USE [master]
GO
/****** Object:  Database [hrmtdb]    Script Date: 16/04/08 09:42:00 ******/
CREATE DATABASE [hrmtdb] ON  PRIMARY 
( NAME = N'hrmtdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\hrmtdb.mdf' , SIZE = 84032KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'hrmtdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\hrmtdb_log.ldf' , SIZE = 7040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hrmtdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hrmtdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hrmtdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hrmtdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hrmtdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hrmtdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [hrmtdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [hrmtdb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [hrmtdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hrmtdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hrmtdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hrmtdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hrmtdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hrmtdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hrmtdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hrmtdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hrmtdb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [hrmtdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hrmtdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hrmtdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hrmtdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hrmtdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hrmtdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hrmtdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hrmtdb] SET RECOVERY FULL 
GO
ALTER DATABASE [hrmtdb] SET  MULTI_USER 
GO
ALTER DATABASE [hrmtdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hrmtdb] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'hrmtdb', N'ON'
GO
USE [hrmtdb]
GO
/****** Object:  Table [dbo].[BaoHiem]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BaoHiem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[SoSo] [varchar](20) NULL,
	[NgayCap] [varchar](20) NULL,
	[NoiDKKCB] [nvarchar](max) NULL,
	[LaBHXH] [bit] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_BaoHiem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BoMonBoPhan]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BoMonBoPhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaBMBP] [varchar](20) NOT NULL,
	[MaPK] [varchar](20) NOT NULL,
	[TenBMBP] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](10) NULL,
	[ThuTuBaoCao] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_BoMonBoPhan] PRIMARY KEY CLUSTERED 
(
	[MaBMBP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChucVu]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChucVu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaChucVu] [varchar](20) NOT NULL,
	[TenChucVu] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL CONSTRAINT [DF_ChucVu_NgayHieuLuc]  DEFAULT (getdate()),
	[TinhTrang] [int] NULL CONSTRAINT [DF_ChucVu_TinhTrang]  DEFAULT ((0)),
 CONSTRAINT [PK_ChucVu] PRIMARY KEY CLUSTERED 
(
	[MaChucVu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChuyenNganh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChuyenNganh](
	[MaChuyenNganh] [varchar](20) NOT NULL,
	[MaNganh] [varchar](20) NOT NULL,
	[TenChuyenNganh] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ChuyenNganh_1] PRIMARY KEY CLUSTERED 
(
	[MaChuyenNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DangVien]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DangVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[BiDanh] [nvarchar](max) NULL,
	[SoThe] [varchar](20) NULL,
	[NgayChinhThuc] [varchar](20) NULL,
	[NgayVaoDang] [varchar](20) NULL,
	[NgayCapThe] [varchar](20) NULL,
	[ChiBo] [nvarchar](50) NULL,
	[DangBo] [nvarchar](50) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_DangVien] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DanToc]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DanToc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaDanToc] [varchar](20) NOT NULL,
	[TenDanToc] [nvarchar](max) NULL,
	[TenVietTat] [varchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_DanToc] PRIMARY KEY CLUSTERED 
(
	[MaDanToc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DotTuyen]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DotTuyen](
	[MaDot] [varchar](20) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NgayGio] [datetime2](7) NULL,
	[Phong] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayBatDau] [date] NULL,
	[NgayKetThuc] [date] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HeDaoTao]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HeDaoTao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaHeDaoTao] [varchar](20) NOT NULL,
	[TenHeDaoTao] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HeDaoTao] PRIMARY KEY CLUSTERED 
(
	[MaHeDaoTao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HeSoLuong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HeSoLuong](
	[MaNgach] [varchar](20) NOT NULL,
	[Bac] [int] NOT NULL,
	[HeSo] [decimal](3, 2) NOT NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HeSoLuong] PRIMARY KEY CLUSTERED 
(
	[MaNgach] ASC,
	[Bac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HinhAnh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HinhAnh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[HinhAnh] [varbinary](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HinhAnh] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HinhThucDaoTao]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThucDaoTao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiHinhThucDaoTao] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HinhThucDaoTao] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HopDong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HopDong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaHD] [nvarchar](20) NOT NULL,
	[TenHD] [nvarchar](max) NULL,
	[MaLoaiHD] [varchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKy] [varchar](20) NULL,
	[NguoiKy] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HopDong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KiemNhiem]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KiemNhiem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[MaPK] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[CongViec] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[TrangThai] [nchar](10) NULL,
	[STT] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_PhanCong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KyLuat]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KyLuat](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKL] [varchar](20) NULL,
	[LoaiKL] [varchar](20) NULL,
	[LyDoKL] [nvarchar](max) NULL,
	[HinhThucKL] [nvarchar](max) NULL,
	[SoQuyetDinh] [nvarchar](max) NULL,
	[CapQuyetDinh] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_KyLuat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LichSuTrangThaiLamViec]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LichSuTrangThaiLamViec](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NOT NULL,
	[MaTrangThai] [int] NOT NULL,
	[TuNgay] [varchar](20) NOT NULL,
	[DenNgay] [varchar](20) NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[TrangThai] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiBangChuyenMon]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangChuyenMon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangChuyenMon] [nvarchar](max) NULL,
	[DangHoc] [bit] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangChuyenMon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBangLyLuanChinhTri]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangLyLuanChinhTri](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangLyLuanChinhTri] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangLyLuanChinhTri] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBangNgoaiNgu]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiBangNgoaiNgu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangNgoaiNgu] [nvarchar](max) NULL,
	[MaNgoaiNgu] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangNgoaiNgu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiBangTinHoc]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangTinHoc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangTinHoc] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangTinHoc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBCCC]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiBCCC](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiBCCC] [varchar](20) NOT NULL,
	[TenLoai] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBCCC] PRIMARY KEY CLUSTERED 
(
	[MaLoaiBCCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiDaoTao]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDaoTao](
	[Id] [int] IDENTITY(0,1) NOT NULL,
	[TenLoaiDaoTao] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiDaoTao] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiHopDong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiHopDong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiHD] [varchar](20) NOT NULL,
	[TenLoaiHD] [nvarchar](max) NULL,
	[LaTamTuyen] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiHopDong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiNhacNho]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiNhacNho](
	[MaLoaiNhacNho] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiNhacNho] [nvarchar](50) NULL,
	[TinhTrang] [int] NULL,
	[NgayHieuLuc] [date] NULL,
 CONSTRAINT [PK_LoaiNhacNho] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNhacNho] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiNV] [varchar](20) NOT NULL,
	[TenLoaiNV] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiQuyetDinh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiQuyetDinh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiQD] [varchar](20) NOT NULL,
	[TenLoaiQD] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[MaLoaiQD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiTrinhDo]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiTrinhDo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiTrinhDo] [varchar](20) NOT NULL,
	[TenLoaiTrinhDo] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiTrinhDo] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTrinhDo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Luong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Luong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[LuongCB] [decimal](18, 0) NULL,
	[LuongChinh] [decimal](18, 0) NULL,
	[PCChucVu] [decimal](18, 0) NULL,
	[PCThamNien] [decimal](18, 0) NULL,
	[TyLeVuotKhung] [float] NULL,
	[LuongBHXH] [decimal](18, 0) NULL,
	[MaNgach] [varchar](50) NULL,
	[BacLuong] [int] NULL,
	[HeSoLuong] [decimal](3, 2) NULL,
	[HeSoNamDau] [int] NULL,
	[VuotKhung] [int] NULL,
	[PhuCap] [decimal](3, 2) NULL,
	[NgayHuong] [varchar](20) NULL,
	[SoQuyetDinh] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Luong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ngach]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ngach](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNgach] [varchar](20) NOT NULL,
	[TenNgach] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNgach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nganh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nganh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNganh] [varchar](20) NOT NULL,
	[TenNganh] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Nganh] PRIMARY KEY CLUSTERED 
(
	[MaNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhacNho]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhacNho](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiNhacNho] [int] NULL,
	[NoiDung] [nvarchar](max) NULL,
	[ThoiGianNhac] [date] NULL,
	[TinhTrang] [int] NULL,
	[GhiChu] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[NgayHetHan] [date] NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
 CONSTRAINT [PK_NhacNho] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NOT NULL,
	[HinhDaiDien] [nvarchar](max) NULL,
	[MaPK] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[Ho] [nvarchar](max) NULL,
	[Ten] [nvarchar](max) NULL,
	[TenThuongDung] [nvarchar](max) NULL,
	[TenGoiKhac] [nvarchar](max) NULL,
	[NgaySinh] [varchar](20) NULL,
	[GioiTinh] [bit] NULL,
	[Email] [nvarchar](max) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[DienThoaiCoDinh] [nvarchar](50) NULL,
	[QueQuan] [nvarchar](max) NULL,
	[NoiSinh] [nvarchar](max) NULL,
	[HoKhauThuongTruDiaChi] [nvarchar](max) NULL,
	[HoKhauThuongTruMaTinhThanh] [varchar](20) NULL,
	[HoKhauThuongTruMaQuanHuyen] [varchar](20) NULL,
	[DiaChiHienTaiHoKhauThuongTru] [bit] NULL,
	[DiaChiHienTaiDiaChi] [nvarchar](max) NULL,
	[DiaChiHienTaiMaQuanHuyen] [varchar](20) NULL,
	[DiaChiHienTaiMaTinhThanh] [varchar](20) NULL,
	[ChucDanh] [nvarchar](max) NULL,
	[MaChucVu] [varchar](20) NULL,
	[SoQuyetDinhChucVu] [nvarchar](max) NULL,
	[KiemNhiem] [nvarchar](max) NULL,
	[SoQuyetDinhKiemNhiem] [nvarchar](max) NULL,
	[CMND] [varchar](20) NULL,
	[NgayCapCMND] [varchar](20) NULL,
	[MaNoiCapCMND] [varchar](20) NULL,
	[MaDanToc] [varchar](20) NULL,
	[MaTonGiao] [varchar](20) NULL,
	[QuocTich] [nvarchar](max) NULL,
	[ThanhPhanGiaDinhXuatThan] [nvarchar](max) NULL,
	[NgheNghiepTruocTuyenDung] [nvarchar](max) NULL,
	[NgheNghiepTuyenDung] [nvarchar](max) NULL,
	[HangNhanVien] [varchar](50) NULL,
	[BacNhanVien] [varchar](50) NULL,
	[MaNgach] [varchar](20) NULL,
	[NgayVaoLam] [varchar](20) NULL,
	[NgayTuyenDung] [varchar](20) NULL,
	[TrinhDoVanHoa] [nvarchar](20) NULL,
	[QuanLyNhaNuoc] [nvarchar](max) NULL,
	[DangVien] [bit] NULL,
	[NgayVaoDCSVN] [varchar](20) NULL,
	[NgayChinhThucVaoDCSVN] [varchar](20) NULL,
	[QuanNgu] [bit] NULL,
	[NgayNhapNgu] [varchar](20) NULL,
	[NgayXuatNgu] [varchar](20) NULL,
	[QuanHamCaoNhat] [nvarchar](max) NULL,
	[GiaDinhChinhSach] [bit] NULL,
	[ThuongBinh] [bit] NULL,
	[ThuongBinhHang] [nvarchar](max) NULL,
	[TenToChucCTXH] [nvarchar](max) NULL,
	[ChucVuCTXH] [nvarchar](max) NULL,
	[NgayThamGiaCTXH] [varchar](20) NULL,
	[DanhHieuPhongTang] [nvarchar](max) NULL,
	[DanhHieuKhac] [nvarchar](max) NULL,
	[KhenThuong] [nvarchar](max) NULL,
	[KyLuat] [nvarchar](max) NULL,
	[SoTruongCongTac] [nvarchar](max) NULL,
	[TinhTrangSucKhoe] [nvarchar](max) NULL,
	[ChieuCao] [nvarchar](20) NULL,
	[CanNang] [nvarchar](20) NULL,
	[NhomMau] [nvarchar](20) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[SoTaiKhoan] [nvarchar](50) NULL,
	[SoBHXH] [nvarchar](50) NULL,
	[SoATM] [nvarchar](50) NULL,
	[TenNganHang] [nvarchar](max) NULL,
	[TinhTrangHonNhan] [nvarchar](20) NULL,
	[TinhTrangLamViec] [varchar](20) NULL,
	[TuNhanXet] [nvarchar](max) NULL,
	[MaUngVien] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[NguoiSuaCuoiCung] [varchar](50) NULL,
 CONSTRAINT [PK_NhanVien_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhanVienLog]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVienLog](
	[LanSua] [int] NULL,
	[MaNV] [varchar](20) NULL,
	[TenCot] [varchar](250) NULL,
	[GiaTriCu] [nvarchar](max) NULL,
	[GiaTriMoi] [nvarchar](max) NULL,
	[NguoiSua] [varchar](50) NULL,
	[ThoiGianSua] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhongKhoa]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhongKhoa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaPhongKhoa] [varchar](20) NOT NULL,
	[TenPhongKhoa] [nvarchar](max) NULL,
	[TenVietTat] [varchar](50) NULL,
	[ThuTuBaoCao] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_PhongKhoa] PRIMARY KEY CLUSTERED 
(
	[MaPhongKhoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhuCapThamNien]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhuCapThamNien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayHuong] [varchar](20) NULL,
	[TyLe] [decimal](3, 2) NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuanHeThanNhan]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuanHeThanNhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuanHe] [varchar](20) NOT NULL,
	[TenQuanHe] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuanHe] PRIMARY KEY CLUSTERED 
(
	[MaQuanHe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuanHuyen]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuanHuyen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuanHuyen] [varchar](20) NOT NULL,
	[MaTinhThanh] [nvarchar](20) NULL,
	[TenQuanHuyen] [nvarchar](max) NULL,
	[TenDayDu] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[Loai] [tinyint] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuanHuyen] PRIMARY KEY CLUSTERED 
(
	[MaQuanHuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuaTrinhCongTac]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuaTrinhCongTac](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NoiCongTac] [nvarchar](max) NULL,
	[NgheNghiep] [nvarchar](max) NULL,
	[ThanhTich] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[DenNay] [bit] NULL,
	[GiaiDoan] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[MaChucVu] [varchar](20) NULL,
	[ChucVuHienTai] [bit] NULL,
	[ChucDanh] [nvarchar](50) NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
 CONSTRAINT [PK_QuaTrinhCongTac] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuocGia]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuocGia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuocGia] [varchar](20) NOT NULL,
	[TenQuocGia] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuocGia] PRIMARY KEY CLUSTERED 
(
	[MaQuocGia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuyetDinh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuyetDinh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SoQD] [nvarchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[MaLoaiQD] [varchar](20) NULL,
	[NoiDungQD] [nvarchar](max) NULL,
	[NgayKy] [varchar](20) NULL,
	[NguoiKy] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuyetDinh] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SucKhoe]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SucKhoe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKham] [varchar](20) NULL,
	[TinhTrangSK] [nvarchar](max) NULL,
	[ChieuCao] [float] NULL,
	[CanNang] [float] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_SucKhoe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NULL,
	[MaNV] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[LastLogonTime] [datetime] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TaiKhoan_1] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThanNhan]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThanNhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[HoTen] [nvarchar](max) NULL,
	[NamSinh] [int] NULL,
	[NoiSinh] [nvarchar](max) NULL,
	[MaQuanHe] [varchar](20) NULL,
	[DacDiemLichSu] [nvarchar](max) NULL,
	[NgheNghiep] [nvarchar](max) NULL,
	[ThanNhanVoChong] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[MaQuanHuyen] [varchar](20) NULL,
	[MaTinhThanh] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThanNhan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThiDua]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThiDua](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[XepLoai] [nvarchar](max) NULL,
	[LoaiXetTD] [nvarchar](max) NULL,
	[Dot] [int] NULL,
	[NamHoc] [varchar](50) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThiDua] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThiDuaKhenThuong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThiDuaKhenThuong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[Ngay] [varchar](20) NULL,
	[Loai] [nvarchar](max) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[HinhThuc] [nvarchar](max) NULL,
	[SoQuyetDinh] [nvarchar](max) NULL,
	[CapQuyetDinh] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThiDuaKhenThuong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TinhThanh]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TinhThanh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTinhThanh] [varchar](20) NOT NULL,
	[TenTinhThanh] [nvarchar](max) NULL,
	[TenDayDu] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[Loai] [tinyint] NULL,
	[DauSoCMND] [varchar](3) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TinhThanh] PRIMARY KEY CLUSTERED 
(
	[MaTinhThanh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TonGiao]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TonGiao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTonGiao] [varchar](20) NOT NULL,
	[TenTonGiao] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TonGiao] PRIMARY KEY CLUSTERED 
(
	[MaTonGiao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrangThaiLamViec]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrangThaiLamViec](
	[MaTrangThai] [int] NOT NULL,
	[TenTrangThai] [nvarchar](max) NULL,
	[LoaiTrangThai] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [datetime] NOT NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TrangThaiLamViec] PRIMARY KEY CLUSTERED 
(
	[MaTrangThai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrinhDo]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrinhDo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaBCCC] [varchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[LoaiDaoTao] [int] NULL,
	[LoaiTrinhDoChuyenMon] [int] NULL,
	[MaNgoaiNgu] [varchar](20) NULL,
	[LoaiTenBangCapNgoaiNgu] [int] NULL,
	[LoaiTrinhDoNgoaiNgu] [nvarchar](max) NULL,
	[LoaiTrinhDoTinHoc] [int] NULL,
	[TrinhDoTinHocKhac] [nvarchar](max) NULL,
	[LoaiNghiepVu] [nvarchar](max) NULL,
	[LoaiNghiepVuSuPham] [bit] NULL,
	[LoaiLyLuanChinhTri] [int] NULL,
	[HinhThucDaoTao] [int] NULL,
	[MaTruong] [nvarchar](50) NULL,
	[TruongKhac] [nvarchar](max) NULL,
	[TruongKhacNuocNgoai] [bit] NULL,
	[HocBongNhaNuoc] [bit] NULL,
	[TenDeAnHocBong] [nvarchar](max) NULL,
	[MaLoaiBCCC] [varchar](20) NULL,
	[MaNganh] [varchar](20) NULL,
	[MaChuyenNganh] [varchar](20) NULL,
	[ChuyenNganhDaoTao] [nvarchar](max) NULL,
	[MaLoaiTrinhDo] [varchar](20) NULL,
	[MaHeDaoTao] [varchar](20) NULL,
	[ThoiGianDaoTaoTu] [varchar](20) NULL,
	[ThoiGianDaoTaoDen] [varchar](20) NULL,
	[ThoiGianHieuLuc] [nvarchar](50) NULL,
	[NgayTotNghiep] [date] NULL,
	[LoaiTotNghiep] [nvarchar](20) NULL,
	[NoiCapBang] [nvarchar](max) NULL,
	[NgayCapBang] [varchar](20) NULL,
	[MoTaThem] [nvarchar](max) NULL,
	[IsBangCap] [bit] NULL,
	[MaCoSoDaoTao] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[TrinhDoBanDau] [bit] NULL CONSTRAINT [DF_TrinhDo_TrinhDoBanDau]  DEFAULT ((0)),
 CONSTRAINT [PK_TrinhDo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Truong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Truong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTruong] [nvarchar](50) NOT NULL,
	[TenTruong] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[LoaiTruong] [varchar](20) NULL,
	[NuocNgoai] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Truong] PRIMARY KEY CLUSTERED 
(
	[MaTruong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UngVien]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UngVien](
	[MaUV] [varchar](20) NOT NULL,
	[MaDot] [varchar](20) NULL,
	[Ho] [nvarchar](max) NULL,
	[Ten] [nvarchar](max) NULL,
	[NgaySinh] [varchar](20) NULL,
	[GioiTinh] [bit] NULL,
	[CMND] [varchar](20) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[Email] [nvarchar](max) NULL,
	[QueQuan] [nvarchar](50) NULL,
	[NganhHoc] [nvarchar](max) NULL,
	[MaTruong] [nvarchar](50) NULL,
	[TruongKhac] [nvarchar](50) NULL,
	[HinhThucDaoTao] [int] NULL,
	[XepHang] [nvarchar](20) NULL,
	[DiemTN] [nvarchar](10) NULL,
	[SauDaiHoc] [nvarchar](max) NULL,
	[MaPK] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[NganhDuTuyen] [nvarchar](max) NULL,
	[ChungChi] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[YeuCauChuyenMon] [nvarchar](max) NULL,
	[KyNangSuPham] [nvarchar](max) NULL,
 CONSTRAINT [PK_UngVien] PRIMARY KEY CLUSTERED 
(
	[MaUV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[XaPhuong]    Script Date: 16/04/08 09:42:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[XaPhuong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaXaPhuong] [varchar](20) NOT NULL,
	[TenXaPhuong] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[MaQuanHuyen] [varchar](20) NULL,
	[Loai] [tinyint] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_XaPhuong] PRIMARY KEY CLUSTERED 
(
	[MaXaPhuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_NhanVienLog_LanSua]    Script Date: 16/04/08 09:42:00 ******/
CREATE NONCLUSTERED INDEX [IX_NhanVienLog_LanSua] ON [dbo].[NhanVienLog]
(
	[LanSua] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NhanVienLog_MaNV]    Script Date: 16/04/08 09:42:00 ******/
CREATE NONCLUSTERED INDEX [IX_NhanVienLog_MaNV] ON [dbo].[NhanVienLog]
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Trigger [dbo].[NV_NhanVienLog]    Script Date: 16/04/08 09:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[NV_NhanVienLog] ON [dbo].[NhanVien] 
	AFTER UPDATE
AS 

BEGIN
	DECLARE @nguoiSua VARCHAR(MAX);
	SELECT @nguoiSua = 'sys - '+ CURRENT_USER;
	--neu chay script truc tiep trong sql server -> khong update cot NguoiSuaCuoiCung -> log lai duoi ten sys
	IF ( UPDATE (NguoiSuaCuoiCung))
	BEGIN
		SELECT TOP 1 @nguoiSua = [NguoiSuaCuoiCung] FROM inserted;
	END;
	--neu update bang chuong trinh -> update cot NguoiSuaCuoiCung -> log lai ten user dang nhap chuong trinh

	--lay danh sach cot cua bang NhanVien de chay dynamic sql
	DECLARE @colList VARCHAR(MAX) = 
	STUFF((SELECT ',(''' + name+''',CAST(['+name+'] AS nvarchar(max)))
'                    
			FROM [hrmtdb].[sys].[columns]
			WHERE [object_id] = OBJECT_ID('[hrmtdb].[dbo].[NhanVien]')
			ORDER BY [column_id]                             
			FOR XML PATH(''), TYPE)
			.value('.', 'VARCHAR(MAX)') 
			,1,1,'');
	--luu bang tam do dynamic sql khong thay 2 bang ao inserted, deleted
	SELECT * INTO #TempInserted FROM inserted
	SELECT * INTO #TempDeleted FROM deleted
	--[version]: lan sua sau cung cua moi nv, cau truc: (MaNV, LanSua)
	--[old]: du lieu truoc khi cap nhap, lay tu bang ao deleted, cau truc: (MaNV, col/*ten cot*/, val/*gia tri cu*/)
	--[new]: du lieu sau khi cap nhap, lay tu bang ao inserted, cau truc: (MaNV, col/*ten cot*/, val/*gia tri moi*/)
	DECLARE @sql VARCHAR(MAX) = 
	'WITH [version] AS
	(
		SELECT l.MaNV, MAX(l.LanSua) AS ''LanSua''
		FROM [hrmtdb].[dbo].[NhanVienLog] l
		GROUP BY l.MaNV
	),
	[old] AS
	(
		SELECT d.[MaNV], c.[col], c.[val]
		FROM #TempDeleted d
		CROSS APPLY
		(
		VALUES '+@colList+') AS c([col],[val])
	),
	[new] AS
	(
		SELECT i.[MaNV], c.[col], c.[val]
		FROM #TempInserted i
		CROSS APPLY
		(
		VALUES '+@colList+') AS c([col],[val])
	)
	INSERT INTO [dbo].[NhanVienLog] ([LanSua],[MaNV],[TenCot],[GiaTriCu],[GiaTriMoi],[NguoiSua],[ThoiGianSua])
	SELECT COALESCE([version].[LanSua],0)+1 AS ''LanSua'', [change].*, '''+@nguoiSua+''' ''NguoiSua'', GETDATE() ''ThoiGianSua''
	FROM 
	(
		SELECT [new].[MaNV], [new].[col] AS ''TenCot'', [old].[val] AS ''GiaTriCu'', [new].[val] AS ''GiaTriMoi''
		FROM [new] LEFT OUTER JOIN [old] ON [new].[MaNV]=[old].[MaNV] AND [new].[col]=[old].[col]
		WHERE ([old].[val] IS NULL AND [new].[val] IS NOT NULL) OR ([old].[val] IS NOT NULL AND [new].[val] IS NULL) OR ([old].[val]<>[new].[val])
	) [change] LEFT OUTER JOIN [version] ON [change].[MaNV]=[version].[MaNV]
	';
	
	
	EXEC (@sql);


END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nơi đăng ký khám chữa bệnh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'NoiDKKCB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loại bảo hiểm xác định BHXH, BHYT kiểu bool' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'LaBHXH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: binh thuong, 1: xoa, 2: an' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BoMonBoPhan', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nam (4 so) + so thu tu trong nam (2 so)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DotTuyen', @level2type=N'COLUMN',@level2name=N'MaDot'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: binh thuong, 1: xoa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DotTuyen', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chuyen bang nay qua DB khac' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HinhAnh'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'STT theo đơn vị' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'KiemNhiem', @level2type=N'COLUMN',@level2name=N'STT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Luu cac cong viec kiem nhiem, khong luu chuc vu chinh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'KiemNhiem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Luu lai qua trinh thay doi trang thai lam viec' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LichSuTrangThaiLamViec'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'%' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Luong', @level2type=N'COLUMN',@level2name=N'VuotKhung'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lay theo dau thang cua ngay het han quyet dinh, cho nguoi dung dieu chinh + cho chon thoi gian nhac lai sau moi lan nhac' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'ThoiGianNhac'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: chua nhac nho, 1 da nhac' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ngay tao (lay theo ngay tao quyet dinh)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'NgayHieuLuc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lay theo ngay het han cua quyet dinh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'NgayHetHan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'So quyet dinh duoc nhac nho' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'SoQuyetDinh'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nghề nghiệp khi được tuyển dụng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhanVien', @level2type=N'COLUMN',@level2name=N'NgheNghiepTuyenDung'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id tai khoan thuc hien thay doi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhanVienLog', @level2type=N'COLUMN',@level2name=N'NguoiSua'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Luu vet cac thay doi tren bang nhan vien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhanVienLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tam thoi chua dung bang nay' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SucKhoe'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: tháng | 1: học kỳ | 2: năm học' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'LoaiXetTD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nếu tháng -> đợt lưu giá trị tháng | ...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'Dot'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngày thi đua khen thưởng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDuaKhenThuong', @level2type=N'COLUMN',@level2name=N'Ngay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: còn làm việc, 2: tạm nghỉ, 3: nghỉ luôn' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrangThaiLamViec', @level2type=N'COLUMN',@level2name=N'LoaiTrangThai'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mã bằng cấp chứng chỉ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaBCCC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mã loại bằng cấp chứng chỉ --> lưu ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaLoaiBCCC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tam thoi chua dung bang nay' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'XaPhuong'
GO
USE [master]
GO
ALTER DATABASE [hrmtdb] SET  READ_WRITE 
GO
