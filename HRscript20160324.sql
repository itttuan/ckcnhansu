USE [hrmtdb]
GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaLoaiBCCC'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaBCCC'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDuaKhenThuong', @level2type=N'COLUMN',@level2name=N'Ngay'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'Dot'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'LoaiXetTD'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PhanCong', @level2type=N'COLUMN',@level2name=N'STT'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhanVien', @level2type=N'COLUMN',@level2name=N'NgheNghiepTuyenDung'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'TinhTrang'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Luong', @level2type=N'COLUMN',@level2name=N'VuotKhung'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DotTuyen', @level2type=N'COLUMN',@level2name=N'TinhTrang'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BoMonBoPhan', @level2type=N'COLUMN',@level2name=N'TinhTrang'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'LaBHXH'

GO
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'NoiDKKCB'

GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [FK_TrinhDo_Nganh]
GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [FK_TrinhDo_LoaiTrinhDo]
GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [FK_TrinhDo_LoaiBCCC]
GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [FK_TrinhDo_HeDaoTao]
GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [FK_TrinhDo_ChuyenNganh]
GO
ALTER TABLE [dbo].[ThanNhan] DROP CONSTRAINT [FK_ThanNhan_QuanHe]
GO
ALTER TABLE [dbo].[QuyetDinh] DROP CONSTRAINT [FK_QuyetDinh_LoaiQuyetDinh]
GO
ALTER TABLE [dbo].[PhanCong] DROP CONSTRAINT [FK_PhanCong_BoMonBoPhan]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_TonGiao]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_TinhThanh1]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_TinhThanh]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_QuanHuyen1]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_QuanHuyen]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_DanToc]
GO
ALTER TABLE [dbo].[NhanVien] DROP CONSTRAINT [FK_NhanVien_ChucVu]
GO
ALTER TABLE [dbo].[ChuyenNganh] DROP CONSTRAINT [FK_ChuyenNganh_Nganh]
GO
ALTER TABLE [dbo].[BoMonBoPhan] DROP CONSTRAINT [FK_BoMonBoPhan_PhongKhoa]
GO
ALTER TABLE [dbo].[TrinhDo] DROP CONSTRAINT [DF_TrinhDo_TrinhDoBanDau]
GO
ALTER TABLE [dbo].[ChucVu] DROP CONSTRAINT [DF_ChucVu_TinhTrang]
GO
ALTER TABLE [dbo].[ChucVu] DROP CONSTRAINT [DF_ChucVu_NgayHieuLuc]
GO
/****** Object:  Table [dbo].[XaPhuong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[XaPhuong]
GO
/****** Object:  Table [dbo].[UngVien]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[UngVien]
GO
/****** Object:  Table [dbo].[Truong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[Truong]
GO
/****** Object:  Table [dbo].[TrinhDo]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[TrinhDo]
GO
/****** Object:  Table [dbo].[TrangThaiLamViec]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[TrangThaiLamViec]
GO
/****** Object:  Table [dbo].[TonGiao]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[TonGiao]
GO
/****** Object:  Table [dbo].[TinhThanh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[TinhThanh]
GO
/****** Object:  Table [dbo].[ThiDuaKhenThuong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[ThiDuaKhenThuong]
GO
/****** Object:  Table [dbo].[ThiDua]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[ThiDua]
GO
/****** Object:  Table [dbo].[ThanNhan]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[ThanNhan]
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[TaiKhoan]
GO
/****** Object:  Table [dbo].[SucKhoe]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[SucKhoe]
GO
/****** Object:  Table [dbo].[QuyetDinh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[QuyetDinh]
GO
/****** Object:  Table [dbo].[QuocGia]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[QuocGia]
GO
/****** Object:  Table [dbo].[QuaTrinhCongTac]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[QuaTrinhCongTac]
GO
/****** Object:  Table [dbo].[QuanHuyen]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[QuanHuyen]
GO
/****** Object:  Table [dbo].[QuanHeThanNhan]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[QuanHeThanNhan]
GO
/****** Object:  Table [dbo].[PhuCapThamNien]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[PhuCapThamNien]
GO
/****** Object:  Table [dbo].[PhongKhoa]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[PhongKhoa]
GO
/****** Object:  Table [dbo].[PhanCong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[PhanCong]
GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[NhanVien]
GO
/****** Object:  Table [dbo].[NhacNho]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[NhacNho]
GO
/****** Object:  Table [dbo].[Nganh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[Nganh]
GO
/****** Object:  Table [dbo].[Ngach]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[Ngach]
GO
/****** Object:  Table [dbo].[Luong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[Luong]
GO
/****** Object:  Table [dbo].[LoaiTrinhDo]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiTrinhDo]
GO
/****** Object:  Table [dbo].[LoaiQuyetDinh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiQuyetDinh]
GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiNhanVien]
GO
/****** Object:  Table [dbo].[LoaiNhacNho]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiNhacNho]
GO
/****** Object:  Table [dbo].[LoaiHopDong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiHopDong]
GO
/****** Object:  Table [dbo].[LoaiDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiDaoTao]
GO
/****** Object:  Table [dbo].[LoaiBCCC]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiBCCC]
GO
/****** Object:  Table [dbo].[LoaiBangTinHoc]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiBangTinHoc]
GO
/****** Object:  Table [dbo].[LoaiBangNgoaiNgu]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiBangNgoaiNgu]
GO
/****** Object:  Table [dbo].[LoaiBangLyLuanChinhTri]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiBangLyLuanChinhTri]
GO
/****** Object:  Table [dbo].[LoaiBangChuyenMon]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[LoaiBangChuyenMon]
GO
/****** Object:  Table [dbo].[KyLuat]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[KyLuat]
GO
/****** Object:  Table [dbo].[HopDong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[HopDong]
GO
/****** Object:  Table [dbo].[HinhThucDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[HinhThucDaoTao]
GO
/****** Object:  Table [dbo].[HinhAnh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[HinhAnh]
GO
/****** Object:  Table [dbo].[HeSoLuong]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[HeSoLuong]
GO
/****** Object:  Table [dbo].[HeDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[HeDaoTao]
GO
/****** Object:  Table [dbo].[DotTuyen]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[DotTuyen]
GO
/****** Object:  Table [dbo].[DanToc]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[DanToc]
GO
/****** Object:  Table [dbo].[DangVien]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[DangVien]
GO
/****** Object:  Table [dbo].[ChuyenNganh]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[ChuyenNganh]
GO
/****** Object:  Table [dbo].[ChucVu]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[ChucVu]
GO
/****** Object:  Table [dbo].[BoMonBoPhan]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[BoMonBoPhan]
GO
/****** Object:  Table [dbo].[BaoHiem]    Script Date: 3/24/2016 4:34:48 PM ******/
DROP TABLE [dbo].[BaoHiem]
GO
/****** Object:  Table [dbo].[BaoHiem]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BaoHiem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[SoSo] [varchar](20) NULL,
	[NgayCap] [varchar](20) NULL,
	[NoiDKKCB] [nvarchar](max) NULL,
	[LaBHXH] [bit] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_BaoHiem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BoMonBoPhan]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BoMonBoPhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaBMBP] [varchar](20) NOT NULL,
	[MaPK] [varchar](20) NOT NULL,
	[TenBMBP] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](10) NULL,
	[ThuTuBaoCao] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_BoMonBoPhan] PRIMARY KEY CLUSTERED 
(
	[MaBMBP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChucVu]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChucVu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaChucVu] [varchar](20) NOT NULL,
	[TenChucVu] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ChucVu] PRIMARY KEY CLUSTERED 
(
	[MaChucVu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChuyenNganh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChuyenNganh](
	[MaChuyenNganh] [varchar](20) NOT NULL,
	[MaNganh] [varchar](20) NOT NULL,
	[TenChuyenNganh] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ChuyenNganh_1] PRIMARY KEY CLUSTERED 
(
	[MaChuyenNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DangVien]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DangVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[BiDanh] [nvarchar](max) NULL,
	[SoThe] [varchar](20) NULL,
	[NgayChinhThuc] [varchar](20) NULL,
	[NgayVaoDang] [varchar](20) NULL,
	[NgayCapThe] [varchar](20) NULL,
	[ChiBo] [nvarchar](50) NULL,
	[DangBo] [nvarchar](50) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_DangVien] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DanToc]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DanToc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaDanToc] [varchar](20) NOT NULL,
	[TenDanToc] [nvarchar](max) NULL,
	[TenVietTat] [varchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_DanToc] PRIMARY KEY CLUSTERED 
(
	[MaDanToc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DotTuyen]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DotTuyen](
	[MaDot] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NgayGio] [datetime2](7) NULL,
	[Phong] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayBatDau] [date] NULL,
	[NgayKetThuc] [date] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HeDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HeDaoTao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaHeDaoTao] [varchar](20) NOT NULL,
	[TenHeDaoTao] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HeDaoTao] PRIMARY KEY CLUSTERED 
(
	[MaHeDaoTao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HeSoLuong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HeSoLuong](
	[MaNgach] [varchar](20) NOT NULL,
	[Bac] [int] NOT NULL,
	[HeSo] [decimal](3, 2) NOT NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HeSoLuong] PRIMARY KEY CLUSTERED 
(
	[MaNgach] ASC,
	[Bac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HinhAnh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HinhAnh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[HinhAnh] [varbinary](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HinhAnh] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HinhThucDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThucDaoTao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiHinhThucDaoTao] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HinhThucDaoTao] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HopDong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HopDong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaHD] [varchar](20) NOT NULL,
	[TenHD] [nvarchar](max) NULL,
	[MaLoaiHD] [varchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKy] [varchar](20) NULL,
	[NguoiKy] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_HopDong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KyLuat]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KyLuat](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKL] [varchar](20) NULL,
	[LoaiKL] [varchar](20) NULL,
	[LyDoKL] [nvarchar](max) NULL,
	[HinhThucKL] [nvarchar](max) NULL,
	[SoQuyetDinh] [nvarchar](max) NULL,
	[CapQuyetDinh] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_KyLuat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiBangChuyenMon]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangChuyenMon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangChuyenMon] [nvarchar](max) NULL,
	[DangHoc] [bit] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangChuyenMon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBangLyLuanChinhTri]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangLyLuanChinhTri](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangLyLuanChinhTri] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangLyLuanChinhTri] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBangNgoaiNgu]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiBangNgoaiNgu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangNgoaiNgu] [nvarchar](max) NULL,
	[MaNgoaiNgu] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangNgoaiNgu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiBangTinHoc]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiBangTinHoc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiBangTinHoc] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBangTinHoc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiBCCC]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiBCCC](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiBCCC] [varchar](20) NOT NULL,
	[TenLoai] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiBCCC] PRIMARY KEY CLUSTERED 
(
	[MaLoaiBCCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiDaoTao]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDaoTao](
	[Id] [int] IDENTITY(0,1) NOT NULL,
	[TenLoaiDaoTao] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiDaoTao] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiHopDong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiHopDong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiHD] [varchar](20) NOT NULL,
	[TenLoaiHD] [nvarchar](max) NULL,
	[LaTamTuyen] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiHopDong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiNhacNho]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiNhacNho](
	[MaLoaiNhacNho] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiNhacNho] [nvarchar](50) NULL,
	[TinhTrang] [int] NULL,
	[NgayHieuLuc] [date] NULL,
 CONSTRAINT [PK_LoaiNhacNho] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNhacNho] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiNV] [varchar](20) NOT NULL,
	[TenLoaiNV] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiQuyetDinh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiQuyetDinh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiQD] [varchar](20) NOT NULL,
	[TenLoaiQD] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[MaLoaiQD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiTrinhDo]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiTrinhDo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiTrinhDo] [varchar](20) NOT NULL,
	[TenLoaiTrinhDo] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiTrinhDo] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTrinhDo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Luong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Luong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[LuongCB] [decimal](18, 0) NULL,
	[LuongChinh] [decimal](18, 0) NULL,
	[PCChucVu] [decimal](18, 0) NULL,
	[PCThamNien] [decimal](18, 0) NULL,
	[TyLeVuotKhung] [float] NULL,
	[LuongBHXH] [decimal](18, 0) NULL,
	[MaNgach] [varchar](50) NULL,
	[BacLuong] [int] NULL,
	[HeSoLuong] [decimal](3, 2) NULL,
	[HeSoNamDau] [int] NULL,
	[VuotKhung] [int] NULL,
	[PhuCap] [decimal](3, 2) NULL,
	[NgayHuong] [varchar](20) NULL,
	[SoQuyetDinh] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Luong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ngach]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ngach](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNgach] [varchar](20) NOT NULL,
	[TenNgach] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNgach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nganh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nganh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNganh] [varchar](20) NOT NULL,
	[TenNganh] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Nganh] PRIMARY KEY CLUSTERED 
(
	[MaNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhacNho]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhacNho](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiNhacNho] [int] NULL,
	[NoiDung] [nvarchar](max) NULL,
	[ThoiGianNhac] [date] NULL,
	[TinhTrang] [int] NULL,
	[GhiChu] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
 CONSTRAINT [PK_NhacNho] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NOT NULL,
	[HinhDaiDien] [nvarchar](max) NULL,
	[MaPK] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[Ho] [nvarchar](max) NULL,
	[Ten] [nvarchar](max) NULL,
	[TenThuongDung] [nvarchar](max) NULL,
	[TenGoiKhac] [nvarchar](max) NULL,
	[NgaySinh] [varchar](20) NULL,
	[GioiTinh] [bit] NULL,
	[Email] [nvarchar](max) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[DienThoaiCoDinh] [nvarchar](50) NULL,
	[QueQuan] [nvarchar](max) NULL,
	[NoiSinh] [nvarchar](max) NULL,
	[HoKhauThuongTruDiaChi] [nvarchar](max) NULL,
	[HoKhauThuongTruMaTinhThanh] [varchar](20) NULL,
	[HoKhauThuongTruMaQuanHuyen] [varchar](20) NULL,
	[DiaChiHienTaiHoKhauThuongTru] [bit] NULL,
	[DiaChiHienTaiDiaChi] [nvarchar](max) NULL,
	[DiaChiHienTaiMaQuanHuyen] [varchar](20) NULL,
	[DiaChiHienTaiMaTinhThanh] [varchar](20) NULL,
	[ChucDanh] [nvarchar](max) NULL,
	[MaChucVu] [varchar](20) NULL,
	[SoQuyetDinhChucVu] [nvarchar](max) NULL,
	[KiemNhiem] [nvarchar](max) NULL,
	[SoQuyetDinhKiemNhiem] [nvarchar](max) NULL,
	[CMND] [varchar](20) NULL,
	[NgayCapCMND] [varchar](20) NULL,
	[MaNoiCapCMND] [varchar](20) NULL,
	[MaDanToc] [varchar](20) NULL,
	[MaTonGiao] [varchar](20) NULL,
	[QuocTich] [nvarchar](max) NULL,
	[ThanhPhanGiaDinhXuatThan] [nvarchar](max) NULL,
	[NgheNghiepTruocTuyenDung] [nvarchar](max) NULL,
	[NgheNghiepTuyenDung] [text] NULL,
	[HangNhanVien] [varchar](50) NULL,
	[BacNhanVien] [varchar](50) NULL,
	[MaNgach] [varchar](20) NULL,
	[NgayVaoLam] [varchar](20) NULL,
	[NgayTuyenDung] [varchar](20) NULL,
	[TrinhDoVanHoa] [nvarchar](20) NULL,
	[QuanLyNhaNuoc] [nvarchar](max) NULL,
	[DangVien] [bit] NULL,
	[NgayVaoDCSVN] [varchar](20) NULL,
	[NgayChinhThucVaoDCSVN] [varchar](20) NULL,
	[QuanNgu] [bit] NULL,
	[NgayNhapNgu] [varchar](20) NULL,
	[NgayXuatNgu] [varchar](20) NULL,
	[QuanHamCaoNhat] [nvarchar](max) NULL,
	[GiaDinhChinhSach] [bit] NULL,
	[ThuongBinh] [bit] NULL,
	[ThuongBinhHang] [nvarchar](max) NULL,
	[TenToChucCTXH] [nvarchar](max) NULL,
	[ChucVuCTXH] [nvarchar](max) NULL,
	[NgayThamGiaCTXH] [varchar](20) NULL,
	[DanhHieuPhongTang] [nvarchar](max) NULL,
	[DanhHieuKhac] [nvarchar](max) NULL,
	[KhenThuong] [nvarchar](max) NULL,
	[KyLuat] [nvarchar](max) NULL,
	[SoTruongCongTac] [nvarchar](max) NULL,
	[TinhTrangSucKhoe] [nvarchar](max) NULL,
	[ChieuCao] [nvarchar](20) NULL,
	[CanNang] [nvarchar](20) NULL,
	[NhomMau] [nvarchar](20) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[SoTaiKhoan] [nvarchar](50) NULL,
	[SoBHXH] [nvarchar](50) NULL,
	[SoATM] [nvarchar](50) NULL,
	[TenNganHang] [nvarchar](max) NULL,
	[TinhTrangHonNhan] [nvarchar](20) NULL,
	[TinhTrangLamViec] [varchar](20) NULL,
	[TuNhanXet] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_NhanVien_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhanCong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhanCong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[CongViec] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[TrangThai] [nchar](10) NULL,
	[STT] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_PhanCong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhongKhoa]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhongKhoa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaPhongKhoa] [varchar](20) NOT NULL,
	[TenPhongKhoa] [nvarchar](max) NULL,
	[TenVietTat] [varchar](50) NULL,
	[ThuTuBaoCao] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_PhongKhoa] PRIMARY KEY CLUSTERED 
(
	[MaPhongKhoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhuCapThamNien]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhuCapThamNien](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayHuong] [varchar](20) NULL,
	[TyLe] [decimal](3, 2) NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuanHeThanNhan]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuanHeThanNhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuanHe] [varchar](20) NOT NULL,
	[TenQuanHe] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuanHe] PRIMARY KEY CLUSTERED 
(
	[MaQuanHe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuanHuyen]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuanHuyen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuanHuyen] [varchar](20) NOT NULL,
	[MaTinhThanh] [nvarchar](20) NULL,
	[TenQuanHuyen] [nvarchar](max) NULL,
	[TenDayDu] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[Loai] [tinyint] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuanHuyen] PRIMARY KEY CLUSTERED 
(
	[MaQuanHuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuaTrinhCongTac]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuaTrinhCongTac](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NoiCongTac] [nvarchar](max) NULL,
	[NgheNghiep] [nvarchar](max) NULL,
	[ThanhTich] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[DenNay] [bit] NULL,
	[GiaiDoan] [int] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[MaChucVu] [varchar](20) NULL,
	[ChucVuHienTai] [bit] NULL,
	[ChucDanh] [nvarchar](50) NULL,
	[SoQuyetDinh] [nvarchar](20) NULL,
 CONSTRAINT [PK_QuaTrinhCongTac] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuocGia]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuocGia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaQuocGia] [varchar](20) NOT NULL,
	[TenQuocGia] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuocGia] PRIMARY KEY CLUSTERED 
(
	[MaQuocGia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuyetDinh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuyetDinh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SoQD] [varchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[MaLoaiQD] [varchar](20) NULL,
	[NoiDungQD] [nvarchar](max) NULL,
	[NgayKy] [varchar](20) NULL,
	[NguoiKy] [nvarchar](max) NULL,
	[TuNgay] [varchar](20) NULL,
	[DenNgay] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_QuyetDinh] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SucKhoe]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SucKhoe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[NgayKham] [varchar](20) NULL,
	[TinhTrangSK] [nvarchar](max) NULL,
	[ChieuCao] [float] NULL,
	[CanNang] [float] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_SucKhoe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[MaNV] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[LastLogonTime] [datetime] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThanNhan]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThanNhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[HoTen] [nvarchar](max) NULL,
	[NamSinh] [int] NULL,
	[NoiSinh] [nvarchar](max) NULL,
	[MaQuanHe] [varchar](20) NULL,
	[DacDiemLichSu] [nvarchar](max) NULL,
	[NgheNghiep] [nvarchar](max) NULL,
	[ThanNhanVoChong] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[MaQuanHuyen] [varchar](20) NULL,
	[MaTinhThanh] [varchar](20) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThanNhan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThiDua]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThiDua](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[XepLoai] [nvarchar](max) NULL,
	[LoaiXetTD] [nvarchar](max) NULL,
	[Dot] [int] NULL,
	[NamHoc] [varchar](50) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThiDua] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThiDuaKhenThuong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThiDuaKhenThuong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [varchar](20) NULL,
	[Ngay] [varchar](20) NULL,
	[Loai] [nvarchar](max) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[HinhThuc] [nvarchar](max) NULL,
	[SoQuyetDinh] [nvarchar](max) NULL,
	[CapQuyetDinh] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_ThiDuaKhenThuong] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TinhThanh]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TinhThanh](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTinhThanh] [varchar](20) NOT NULL,
	[TenTinhThanh] [nvarchar](max) NULL,
	[TenDayDu] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[Loai] [tinyint] NULL,
	[DauSoCMND] [varchar](3) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TinhThanh] PRIMARY KEY CLUSTERED 
(
	[MaTinhThanh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TonGiao]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TonGiao](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTonGiao] [varchar](20) NOT NULL,
	[TenTonGiao] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_TonGiao] PRIMARY KEY CLUSTERED 
(
	[MaTonGiao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrangThaiLamViec]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrangThaiLamViec](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTrangThai] [varchar](20) NOT NULL,
	[TenTrangThai] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrinhDo]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrinhDo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaBCCC] [varchar](20) NULL,
	[MaNV] [varchar](20) NULL,
	[LoaiDaoTao] [int] NULL,
	[LoaiTrinhDoChuyenMon] [int] NULL,
	[MaNgoaiNgu] [varchar](20) NULL,
	[LoaiTenBangCapNgoaiNgu] [int] NULL,
	[LoaiTrinhDoNgoaiNgu] [nvarchar](max) NULL,
	[LoaiTrinhDoTinHoc] [int] NULL,
	[TrinhDoTinHocKhac] [nvarchar](max) NULL,
	[LoaiNghiepVu] [nvarchar](max) NULL,
	[LoaiNghiepVuSuPham] [bit] NULL,
	[LoaiLyLuanChinhTri] [int] NULL,
	[HinhThucDaoTao] [int] NULL,
	[MaTruong] [nvarchar](50) NULL,
	[TruongKhac] [nvarchar](max) NULL,
	[TruongKhacNuocNgoai] [bit] NULL,
	[HocBongNhaNuoc] [bit] NULL,
	[TenDeAnHocBong] [nvarchar](max) NULL,
	[MaLoaiBCCC] [varchar](20) NULL,
	[MaNganh] [varchar](20) NULL,
	[MaChuyenNganh] [varchar](20) NULL,
	[ChuyenNganhDaoTao] [nvarchar](max) NULL,
	[MaLoaiTrinhDo] [varchar](20) NULL,
	[MaHeDaoTao] [varchar](20) NULL,
	[ThoiGianDaoTaoTu] [varchar](20) NULL,
	[ThoiGianDaoTaoDen] [varchar](20) NULL,
	[ThoiGianHieuLuc] [nvarchar](50) NULL,
	[NgayTotNghiep] [date] NULL,
	[LoaiTotNghiep] [nvarchar](20) NULL,
	[NoiCapBang] [nvarchar](max) NULL,
	[NgayCapBang] [varchar](20) NULL,
	[MoTaThem] [nvarchar](max) NULL,
	[IsBangCap] [bit] NULL,
	[MaCoSoDaoTao] [nchar](10) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
	[TrinhDoBanDau] [bit] NULL,
 CONSTRAINT [PK_TrinhDo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Truong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Truong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaTruong] [nvarchar](50) NOT NULL,
	[TenTruong] [nvarchar](max) NULL,
	[TenVietTat] [nvarchar](max) NULL,
	[LoaiTruong] [varchar](20) NULL,
	[NuocNgoai] [bit] NULL,
	[GhiChu] [nvarchar](max) NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_Truong] PRIMARY KEY CLUSTERED 
(
	[MaTruong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UngVien]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UngVien](
	[MaUV] [int] IDENTITY(1,1) NOT NULL,
	[MaDot] [int] NULL,
	[Ho] [nvarchar](max) NULL,
	[Ten] [nvarchar](max) NULL,
	[NgaySinh] [varchar](20) NULL,
	[GioiTinh] [bit] NULL,
	[DienThoai] [nvarchar](50) NULL,
	[QueQuan] [nvarchar](50) NULL,
	[NganhHoc] [nvarchar](max) NULL,
	[MaTruong] [nvarchar](50) NULL,
	[TruongKhac] [nvarchar](50) NULL,
	[HinhThucDaoTao] [int] NULL,
	[XepHang] [nvarchar](20) NULL,
	[DiemTN] [nvarchar](10) NULL,
	[SauDaiHoc] [nvarchar](max) NULL,
	[MaPK] [varchar](20) NULL,
	[MaBMBP] [varchar](20) NULL,
	[NganhDuTuyen] [nvarchar](max) NULL,
	[GhiChu] [nvarchar](max) NULL,
	[YeuCauChuyenMon] [nvarchar](max) NULL,
	[KyNangSuPham] [nvarchar](max) NULL,
 CONSTRAINT [PK_UngVien] PRIMARY KEY CLUSTERED 
(
	[MaUV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[XaPhuong]    Script Date: 3/24/2016 4:34:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[XaPhuong](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MaXaPhuong] [varchar](20) NOT NULL,
	[TenXaPhuong] [nvarchar](max) NULL,
	[TenVietTat] [varchar](10) NULL,
	[MaQuanHuyen] [varchar](20) NULL,
	[Loai] [tinyint] NULL,
	[NgayHieuLuc] [date] NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_XaPhuong] PRIMARY KEY CLUSTERED 
(
	[MaXaPhuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ChucVu] ADD  CONSTRAINT [DF_ChucVu_NgayHieuLuc]  DEFAULT (getdate()) FOR [NgayHieuLuc]
GO
ALTER TABLE [dbo].[ChucVu] ADD  CONSTRAINT [DF_ChucVu_TinhTrang]  DEFAULT ((0)) FOR [TinhTrang]
GO
ALTER TABLE [dbo].[TrinhDo] ADD  CONSTRAINT [DF_TrinhDo_TrinhDoBanDau]  DEFAULT ((0)) FOR [TrinhDoBanDau]
GO
ALTER TABLE [dbo].[BoMonBoPhan]  WITH CHECK ADD  CONSTRAINT [FK_BoMonBoPhan_PhongKhoa] FOREIGN KEY([MaPK])
REFERENCES [dbo].[PhongKhoa] ([MaPhongKhoa])
GO
ALTER TABLE [dbo].[BoMonBoPhan] CHECK CONSTRAINT [FK_BoMonBoPhan_PhongKhoa]
GO
ALTER TABLE [dbo].[ChuyenNganh]  WITH CHECK ADD  CONSTRAINT [FK_ChuyenNganh_Nganh] FOREIGN KEY([MaNganh])
REFERENCES [dbo].[Nganh] ([MaNganh])
GO
ALTER TABLE [dbo].[ChuyenNganh] CHECK CONSTRAINT [FK_ChuyenNganh_Nganh]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_ChucVu] FOREIGN KEY([MaChucVu])
REFERENCES [dbo].[ChucVu] ([MaChucVu])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_ChucVu]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_DanToc] FOREIGN KEY([MaDanToc])
REFERENCES [dbo].[DanToc] ([MaDanToc])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_DanToc]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_QuanHuyen] FOREIGN KEY([HoKhauThuongTruMaQuanHuyen])
REFERENCES [dbo].[QuanHuyen] ([MaQuanHuyen])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_QuanHuyen]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_QuanHuyen1] FOREIGN KEY([DiaChiHienTaiMaQuanHuyen])
REFERENCES [dbo].[QuanHuyen] ([MaQuanHuyen])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_QuanHuyen1]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_TinhThanh] FOREIGN KEY([DiaChiHienTaiMaTinhThanh])
REFERENCES [dbo].[TinhThanh] ([MaTinhThanh])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_TinhThanh]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_TinhThanh1] FOREIGN KEY([HoKhauThuongTruMaTinhThanh])
REFERENCES [dbo].[TinhThanh] ([MaTinhThanh])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_TinhThanh1]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_TonGiao] FOREIGN KEY([MaTonGiao])
REFERENCES [dbo].[TonGiao] ([MaTonGiao])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_TonGiao]
GO
ALTER TABLE [dbo].[PhanCong]  WITH CHECK ADD  CONSTRAINT [FK_PhanCong_BoMonBoPhan] FOREIGN KEY([MaBMBP])
REFERENCES [dbo].[BoMonBoPhan] ([MaBMBP])
GO
ALTER TABLE [dbo].[PhanCong] CHECK CONSTRAINT [FK_PhanCong_BoMonBoPhan]
GO
ALTER TABLE [dbo].[QuyetDinh]  WITH CHECK ADD  CONSTRAINT [FK_QuyetDinh_LoaiQuyetDinh] FOREIGN KEY([MaLoaiQD])
REFERENCES [dbo].[LoaiQuyetDinh] ([MaLoaiQD])
GO
ALTER TABLE [dbo].[QuyetDinh] CHECK CONSTRAINT [FK_QuyetDinh_LoaiQuyetDinh]
GO
ALTER TABLE [dbo].[ThanNhan]  WITH CHECK ADD  CONSTRAINT [FK_ThanNhan_QuanHe] FOREIGN KEY([MaQuanHe])
REFERENCES [dbo].[QuanHeThanNhan] ([MaQuanHe])
GO
ALTER TABLE [dbo].[ThanNhan] CHECK CONSTRAINT [FK_ThanNhan_QuanHe]
GO
ALTER TABLE [dbo].[TrinhDo]  WITH CHECK ADD  CONSTRAINT [FK_TrinhDo_ChuyenNganh] FOREIGN KEY([MaChuyenNganh])
REFERENCES [dbo].[ChuyenNganh] ([MaChuyenNganh])
GO
ALTER TABLE [dbo].[TrinhDo] CHECK CONSTRAINT [FK_TrinhDo_ChuyenNganh]
GO
ALTER TABLE [dbo].[TrinhDo]  WITH CHECK ADD  CONSTRAINT [FK_TrinhDo_HeDaoTao] FOREIGN KEY([MaHeDaoTao])
REFERENCES [dbo].[HeDaoTao] ([MaHeDaoTao])
GO
ALTER TABLE [dbo].[TrinhDo] CHECK CONSTRAINT [FK_TrinhDo_HeDaoTao]
GO
ALTER TABLE [dbo].[TrinhDo]  WITH CHECK ADD  CONSTRAINT [FK_TrinhDo_LoaiBCCC] FOREIGN KEY([MaLoaiBCCC])
REFERENCES [dbo].[LoaiBCCC] ([MaLoaiBCCC])
GO
ALTER TABLE [dbo].[TrinhDo] CHECK CONSTRAINT [FK_TrinhDo_LoaiBCCC]
GO
ALTER TABLE [dbo].[TrinhDo]  WITH CHECK ADD  CONSTRAINT [FK_TrinhDo_LoaiTrinhDo] FOREIGN KEY([MaLoaiTrinhDo])
REFERENCES [dbo].[LoaiTrinhDo] ([MaLoaiTrinhDo])
GO
ALTER TABLE [dbo].[TrinhDo] CHECK CONSTRAINT [FK_TrinhDo_LoaiTrinhDo]
GO
ALTER TABLE [dbo].[TrinhDo]  WITH CHECK ADD  CONSTRAINT [FK_TrinhDo_Nganh] FOREIGN KEY([MaNganh])
REFERENCES [dbo].[Nganh] ([MaNganh])
GO
ALTER TABLE [dbo].[TrinhDo] CHECK CONSTRAINT [FK_TrinhDo_Nganh]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nơi đăng ký khám chữa bệnh' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'NoiDKKCB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loại bảo hiểm xác định BHXH, BHYT kiểu bool' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BaoHiem', @level2type=N'COLUMN',@level2name=N'LaBHXH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: binh thuong, 1: xoa, 2: an' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BoMonBoPhan', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: binh thuong, 1: xoa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DotTuyen', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'%' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Luong', @level2type=N'COLUMN',@level2name=N'VuotKhung'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: chua nhac nho, 1 da nhac' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhacNho', @level2type=N'COLUMN',@level2name=N'TinhTrang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nghề nghiệp khi được tuyển dụng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NhanVien', @level2type=N'COLUMN',@level2name=N'NgheNghiepTuyenDung'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'STT theo đơn vị' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PhanCong', @level2type=N'COLUMN',@level2name=N'STT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: tháng | 1: học kỳ | 2: năm học' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'LoaiXetTD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nếu tháng -> đợt lưu giá trị tháng | ...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDua', @level2type=N'COLUMN',@level2name=N'Dot'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngày thi đua khen thưởng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ThiDuaKhenThuong', @level2type=N'COLUMN',@level2name=N'Ngay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mã bằng cấp chứng chỉ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaBCCC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mã loại bằng cấp chứng chỉ --> lưu ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrinhDo', @level2type=N'COLUMN',@level2name=N'MaLoaiBCCC'
GO
