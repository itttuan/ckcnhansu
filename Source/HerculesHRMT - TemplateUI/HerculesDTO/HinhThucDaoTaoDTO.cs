﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class HinhThucDaoTaoDTO:BaseDTO
    {
        public string TenLoaiHinhThucDaoTao { get; set; }
    }
}
