﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuanHeGiaDinhDto : BaseDTO
    {
        public string MaNv { get; set; }
        public string HoTen { get; set; }
        public int GioiTinh { get; set; }
        public QuanHeThanNhanDTO LoaiQuanHe { get; set; }
        public string NgheNghiep { get; set; }
        public string NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public QuanHuyenDTO QuanHuyen { get; set; }
        public TinhThanhDTO TinhThanh { get; set; }
        public string DacDiemLichSu { get; set; }
        public string GhiChu { get; set; }
        public bool ThanNhanVoChong { get; set; }
        public string DiaChiTongHop { get; set; } 
        public QuanHeGiaDinhDto()
        {
            Id = -1;
            DiaChiTongHop = "";
            LoaiQuanHe = new QuanHeThanNhanDTO();
            QuanHuyen = new QuanHuyenDTO();
            TinhThanh = new TinhThanhDTO();
        }

        public string MoiQuanHe
        {
            get
            {
                string str = LoaiQuanHe.TenQuanHe;

                if (ThanNhanVoChong)
                {
                    str += " bên vợ/chồng";
                }
                //    if(GioiTinh == 0)
                //        str += "vợ";
                //    else
                //        str += "chồng";
                //}

                return str;
            }
        }

        public string ThôngTinThanNhan         
        {
            get
            {
                string str = "";
                bool isNewLine = false;
                if (!String.IsNullOrEmpty(HoTen))
                {
                    str += "Họ tên: " + HoTen;
                    isNewLine = true;
                }

                if (!String.IsNullOrEmpty(NgaySinh))
                {
                    if (isNewLine)
                        str += Environment.NewLine;                     

                    str += "Ngày sinh: " + NgaySinh;
                    isNewLine = true;
                }

                if (!String.IsNullOrEmpty(NgheNghiep))
                {
                    if (isNewLine)
                        str += Environment.NewLine;

                    str += "Nghề nghiệp: " + NgheNghiep;
                    isNewLine = true;
                }

                if (!String.IsNullOrEmpty(DacDiemLichSu))
                {
                    if (isNewLine)
                        str += Environment.NewLine;

                    str += DacDiemLichSu;
                    isNewLine = true;
                }

                if (!String.IsNullOrEmpty(DiaChi))
                {
                    if (isNewLine)
                        str += Environment.NewLine;

                    str += "Địa chỉ: " + DiaChi;
                    isNewLine = true;                
                }

                if (!String.IsNullOrEmpty(GhiChu))
                {
                    if (isNewLine)
                        str += Environment.NewLine;

                    str += "Ghi chú: " + GhiChu;
                    isNewLine = true;
                }


                return str.Replace("\\r\\n",Environment.NewLine);
            }
        }

    }
}
