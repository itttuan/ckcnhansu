﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class HinhAnhDTO: BaseDTO
    {
        public byte[] HinhAnh { get; set; }
        public string MaNV { get; set; }
        public string GhiChu { get; set; }
    }
}
