﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class BaseDTO
    {
        public int Id { get; set; }
        public DateTime NgayHieuLuc { get; set; }
        public int TinhTrang { get; set; }
    }
}
