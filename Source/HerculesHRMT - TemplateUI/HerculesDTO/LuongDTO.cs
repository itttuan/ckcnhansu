﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LuongDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public double LuongCB { get; set; }
        public double LuongChinh { get; set; }
        public double PCChucVu { get; set; }
        public double PCThamNien { get; set; }
        public double TiLeVuotKhung { get; set; }
        public double LuongBHXH { get; set; }
        public string MaNgach { get; set; }
        public string BacLuong { get; set; }
        public double HeSoLuong { get; set; }
        public string HeSoNamDau { get; set; }
        public string VuotKhung { get; set; }
        public double PhuCap { get; set; }
        public string NgayHuong { get; set; }
        public string SoQuyetDinh { get; set; }
        public bool NamDau { get; set; }
        public LuongDTO()
        {
            Id = -1;
            HeSoNamDau = "0";
        }
    }
}
