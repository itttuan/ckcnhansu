﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuanHeThanNhanDTO:BaseDTO
    {
        public string MaQuanHe { get; set; }
        public string TenQuanHe { get; set; }
        public string GhiChu { get; set; }
    }
}
