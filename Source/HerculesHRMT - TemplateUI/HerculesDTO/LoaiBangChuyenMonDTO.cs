﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiBangChuyenMonDTO:BaseDTO
    {
        public string TenLoaiBangChuyenMon { get; set; }
        public Boolean DangHoc { get; set; }
    }
}
