﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiQuyetDinhDTO:BaseDTO
    {
        public string MaLoaiQD { get; set; }
        public string TenLoaiQD { get; set; }
        public string GhiChu { get; set; }
    }
}
