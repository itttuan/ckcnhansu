﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TrangThaiLamViecDTO :BaseDTO
    {
        public string MaTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string GhiChu { get; set; }
    }
}
