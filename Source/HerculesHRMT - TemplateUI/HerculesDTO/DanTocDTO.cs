﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class DanTocDTO:BaseDTO
    {
        public string MaDanToc { get; set; }
        public string TenDanToc { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }
    }
}
