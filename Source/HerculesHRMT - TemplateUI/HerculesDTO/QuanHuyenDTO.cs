﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuanHuyenDTO:BaseDTO
    {
        public string MaQuanHuyen { get; set; }
        public string TenQuanHuyen { get; set; }
        public string TenDayDu { get; set; }
        public string TenVietTat { get; set; }
        public string MaTinhThanh { get; set; }
        public string TenTinhThanh { get; set; }
        public int Loai { get; set; }
        public string TenLoaiQuanHuyen { get; set; }
        public string GhiChu { get; set; }
    }
}
