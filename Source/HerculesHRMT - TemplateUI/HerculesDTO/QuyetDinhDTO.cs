﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuyetDinhDTO:BaseDTO
    {
        public string SoQD { get; set; }
        public string MaLoaiQD { get; set; }
        public string TenLoaiQD { get; set; }
        public string MaNV { get; set; }
        public string NgayKy { get; set; }
        public string NguoiKy { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public string NoiDungQD { get; set; }
        public QuyetDinhDTO()
        {
            Id = -1;
        }
    }
}
