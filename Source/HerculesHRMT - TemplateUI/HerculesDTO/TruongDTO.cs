﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TruongDTO:BaseDTO
    {
        public string MaTruong { get; set; }
        public string TenTruong { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }
        public string LoaiTruong { get; set; }
        public string TenLoaiTruong { get; set; }
        public bool NuocNgoai { get; set; }
    }
}
