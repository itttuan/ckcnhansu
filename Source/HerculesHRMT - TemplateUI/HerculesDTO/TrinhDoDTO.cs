﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TrinhDoDTO: BaseDTO
    {
        public string MaBCCC { set; get; }
        public string MaNV { set; get; }
        public string LoaiDaoTao { set; get; }
        public string LoaiTrinhDoChuyenMon { set; get; }
        public string MaNgoaiNgu { set; get; }
        public string LoaiTenBangCapNgoaiNgu { set; get; }
        public string LoaiTrinhDoNgoaiNgu { set; get; }
        public string LoaiTrinhDoTinHoc { set; get; }
        public string TrinhDoTinHocKhac { set; get; }
        public string LoaiNghiepVu { set; get; }
        public Boolean LoaiNghiepVuSuPham { set; get; }
        public string LoaiLyLuanChinhTri { set; get; }
        public string HinhThucDaoTao { set; get; }
        public string MaTruong { set; get; }
        public string TruongKhac { set; get; }
        public Boolean TruongKhacNuocNgoai { set; get; }
        public Boolean HocBongNhaNuoc { set; get; }
        public string TenDeAnHocBong { set; get; }
        public string MaLoaiBCCC { set; get; }
        public string MaNganh { set; get; }
        public string MaChuyenNganh { set; get; }
        public string ChuyenNganhDaoTao { set; get; }
        public string MaLoaiTrinhDo { set; get; }
        public string MaHeDaoTao { set; get; }
        public string ThoiGianDaoTaoTu { set; get; }
        public string ThoiGianDaoTaoDen { set; get; }
        public string ThoiGianHieuLuc { set; get; }
        public string NgayTotNghiep { set; get; }
        public string LoaiTotNghiep { set; get; }
        public string NoiCapBang { set; get; }
        public string NgayCapBang { set; get; }
        public string MoTaThem { set; get; }
        public Boolean IsBangCap { set; get; }
        public string MaCoSoDaoTao { set; get; }
        
        public LoaiBangChuyenMonDTO LoaiBangChuyenMon { get; set; }
        public QuocGiaDTO NgoaiNgu { get; set; }
        public LoaiBangNgoaiNguDTO LoaiBangNgoaiNgu { get; set; }
        public LoaiBangTinHocDTO LoaiBangTinHoc { get; set; }
        public LoaiBangLyLuanChinhTriDTO LoaiBangLyLuanChinhTri { get; set; }
        public TruongDTO Truong { get; set; }
        public HinhThucDaoTaoDTO HinhThucDaoTaoOBJ { get; set; }

        public string ChuoiThoiGianDaoTao
        {
            get
            {
                string str = "";
                if (!String.IsNullOrEmpty(ThoiGianDaoTaoTu))
                {
                    //DateTime tuNgay = DateTime.Parse(ThoiGianDaoTaoTu);
                    str += "Từ " + Utils.getDate(ThoiGianDaoTaoTu);

                    if (!String.IsNullOrEmpty(ThoiGianDaoTaoDen))
                    {
                        //DateTime denNgay = DateTime.Parse(ThoiGianDaoTaoDen);
                        str += " đến " + Utils.getDate(ThoiGianDaoTaoDen);
                    }

                }

                return str;
            }
        }

        public string ChuoiChuyenNganhDaoTao
        {
            get
            {
                string str = "";
                str += LoaiNghiepVu + ChuyenNganhDaoTao;

                if (!String.IsNullOrEmpty(LoaiBangNgoaiNgu.TenLoaiBangNgoaiNgu) || !String.IsNullOrEmpty(NgoaiNgu.TenQuocGia))
                    str += "Ngoại ngữ tiếng " + NgoaiNgu.TenQuocGia + " " + LoaiBangNgoaiNgu.TenLoaiBangNgoaiNgu;

                if (!String.IsNullOrEmpty(LoaiBangTinHoc.TenLoaiBangTinHoc))
                    str += "Tin học: " + LoaiBangTinHoc.TenLoaiBangTinHoc;

                if (!String.IsNullOrEmpty(LoaiBangLyLuanChinhTri.TenLoaiBangLyLuanChinhTri))
                    str += "Lý luận chính trị: " + LoaiBangLyLuanChinhTri.TenLoaiBangLyLuanChinhTri;
                return str;
            }
        }
    }
}
