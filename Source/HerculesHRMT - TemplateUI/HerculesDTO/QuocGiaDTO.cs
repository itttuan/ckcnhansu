﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuocGiaDTO:BaseDTO
    {
        public string MaQuocGia { get; set; }
        public string TenQuocGia { get; set; }
    }
}
