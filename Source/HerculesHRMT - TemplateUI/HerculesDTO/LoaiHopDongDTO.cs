﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiHopDongDTO : BaseDTO
    {
        public string MaLoaiHD { get; set; }
        public string TenLoaiHD { get; set; }
        public bool LaTamTuyen { get; set; }
        public string GhiChu { get; set; }
    }
}
