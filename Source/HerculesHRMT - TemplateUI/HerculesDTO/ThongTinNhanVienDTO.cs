﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class ThongTinNhanVienDTO
    {
        public NhanVienDTO lylichsoluoc { get; set; }
        public List<QuaTrinhCongTacDTO> truockhituyendung { get; set; }
        public List<QuaTrinhCongTacDTO> khituyendung { get; set; }
        public List<QuaTrinhCongTacDTO> congtacxahoi { get; set; }
        public List<TrinhDoDTO> trinhdochuyenmon { get; set; }
        public List<KhenThuongKyLuatDto> khenthuong { get; set; }
        public List<KhenThuongKyLuatDto> kyluat { get; set; }
        public List<QuanHeGiaDinhDto> quanhegiadinh { get; set; }
        public string tunhanxet { get; set; }
    }
}
