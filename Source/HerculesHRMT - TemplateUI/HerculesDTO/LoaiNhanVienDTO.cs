﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiNhanVienDTO :BaseDTO
    {
        public string MaLoaiNV { get; set; }
        public string TenLoaiNV { get; set; }        
        public string GhiChu { get; set; }
    }
}
