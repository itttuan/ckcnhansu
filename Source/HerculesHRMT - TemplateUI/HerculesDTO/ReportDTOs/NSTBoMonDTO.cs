﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO.ReportDTOs
{
    public class NSTBoMonDTO : NhanSuThangDTO 
    {
        public BoMonBoPhanDTO BoMon { get; set; }

        public NSTBoMonDTO()
        {
            BoMon = new BoMonBoPhanDTO();
        }
    }
}
