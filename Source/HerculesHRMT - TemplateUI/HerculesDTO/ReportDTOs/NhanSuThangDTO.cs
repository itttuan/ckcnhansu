﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO.ReportDTOs
{
    public class NhanSuThangDTO
    {
        // tong so can bo - vien chuc chinh thuc
        public int TongSoChinhThuc { get; set; }
        // gom ngoai chi tieu bien che duoc Bo giao
        public int GomNCTBC { get; set; }
        //chuyen qua co huu
        public int CQuaCH { get; set; }
        // giam co huu
        public int GiamCH { get; set; }
        // noi dung chuyen qua co huu hoac giam co huu
        public string NoiDungPhatSinhThang { get; set; }

        // tong so tam tuyen
        public int TongSoTamTuyen { get; set; }
        // ten vien chuc hop dong tam tuyen
        public string TenTamTuyen { get; set; }

        public NhanSuThangDTO()
        {
            TongSoChinhThuc = 0;
            TongSoTamTuyen = 0;
            GomNCTBC = 0;
            CQuaCH = 0;
            GiamCH = 0;
        }
    }
}
