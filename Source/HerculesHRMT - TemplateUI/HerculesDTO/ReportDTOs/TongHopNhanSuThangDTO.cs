﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO.ReportDTOs
{
    public class TongHopNhanSuThangDTO
    {
        public List<NSTPhongKhoaDTO> dsPhongKhoa { get; set; }
        public int TongSo { get; set; }

        public TongHopNhanSuThangDTO()
        {
            dsPhongKhoa = new List<NSTPhongKhoaDTO>();
            TongSo = 0;
        }
    }
}
