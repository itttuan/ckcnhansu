﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO.ReportDTOs
{
    public class NSTPhongKhoaDTO : NhanSuThangDTO 
    {
        public PhongKhoaDTO PhongKhoa { get; set; }
        public List<NSTBoMonDTO> DsBoMon { get; set; }

        public NSTPhongKhoaDTO()
        {
            PhongKhoa = new PhongKhoaDTO();
            DsBoMon = new List<NSTBoMonDTO>();
        }
    }
}
