﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiBangLyLuanChinhTriDTO:BaseDTO
    {
        public string TenLoaiBangLyLuanChinhTri { get; set; }
    }
}
