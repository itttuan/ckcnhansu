﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiBCapCChiDTO : BaseDTO
    {
        public string MaLoaiBCCC { get; set; }
        public string TenLoai { get; set; }
        public string GhiChu { get; set; }
    }
}
