﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class HopDongDTO: BaseDTO
    {
        public string MaHD { get; set; }
        public string TenHD { get; set; }
        public string MaLoaiHD { get; set; }
        public string TenLoaiHD { get; set; }
        public string MaNV { get; set; }
        public string NgayKy { get; set; }
        public string NguoiKy { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public string NoiDung { get; set; }
        public HopDongDTO()
        {
            this.Id = -1;
        }
    }
}
