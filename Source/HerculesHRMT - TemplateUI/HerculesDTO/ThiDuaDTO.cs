﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class ThiDuaDTO: BaseDTO
    {
        public string MaNV { get; set; }
        public string XepLoai { get; set; }
        public string LoaiXetTD { get; set; }
        public int Dot { get; set; }
        public string NamHoc { get; set; }
        public ThiDuaDTO()
        {
            Id = -1;
        }
    }
}
