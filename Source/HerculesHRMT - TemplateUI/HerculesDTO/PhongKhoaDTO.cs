﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class PhongKhoaDTO:BaseDTO
    {
        public string MaPhongKhoa { get; set; }
        public string TenPhongKhoa { get; set; }
        public string TenBaoCao { get; set; }
        public string TenVietTat { get; set; }
        public int? ThuTuBaoCao { get; set; }
        public string GhiChu { get; set; }        
    }
}
