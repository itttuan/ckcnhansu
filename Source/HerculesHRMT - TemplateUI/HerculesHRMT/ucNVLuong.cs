﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;
using System.Data;
using HerculesDTO;
using System.Windows.Forms;
using System.Collections.Generic;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    public partial class ucNVLuong : XtraUserControl
    {
        string strMaNV = "";
        LuongCTL luongCTL = new LuongCTL();
        NgachCTL ngachCTL = new NgachCTL();
        HeSoLuongCTL hesoluongCTL = new HeSoLuongCTL();
        LuongDTO luongDTO = new LuongDTO();
        List<LuongDTO> listLuong = new List<LuongDTO>();
        Common.STATUS _status;
        public ucNVLuong()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _status = status;
            luongDTO = new LuongDTO();
            BindingData(luongDTO);
            SetStatusComponents();
            LoadDanhSach();
        }


        private void BindingData(LuongDTO lgDTO)
        {
            txtSoQD.EditValue = lgDTO.SoQuyetDinh;
            cbbPg1MaNgach.EditValue = lgDTO.MaNgach;
            cbbPg1Bac.EditValue = lgDTO.BacLuong;
            txtHeSo.EditValue = lgDTO.HeSoLuong;
            chbNamDau.EditValue = lgDTO.HeSoNamDau;
            txtLuongCB.EditValue = lgDTO.LuongCB;
            txtLuongChinh.EditValue = lgDTO.LuongChinh;
            txtPCChucvu.EditValue = lgDTO.PCChucVu;
            txtPCThamnien.EditValue = lgDTO.PCThamNien;
            txtTileVuot.EditValue = lgDTO.VuotKhung;
            txtDongBHXH.EditValue = lgDTO.LuongBHXH;
            deNgayhuong.EditValue = lgDTO.NgayHuong;
            txtPhuCap.EditValue = lgDTO.PhuCap;
        }

        private void LoadDanhSach()
        {
            listLuong = luongCTL.LichSuLuongTheoNhanVien(strMaNV);
            gridLuong.DataSource = listLuong;
        }

        private void ucNVLuong_Load(object sender, EventArgs e)
        {
            cbbPg1MaNgach.Properties.DataSource = ngachCTL.LayDanhSachNgach();
        }

        private void cbbPg1MaNgach_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1MaNgach.EditValue != null)
            {
                cbbPg1Bac.Properties.DataSource = hesoluongCTL.LayDanhSachHeSOLuongTheoNgach(cbbPg1MaNgach.EditValue.ToString());
            }
            else
            {
                cbbPg1Bac.Properties.DataSource = null;
            }
        }

        private void cbbPg1Bac_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1Bac.EditValue != null){
                HeSoLuongDTO o = ((HeSoLuongDTO)((LookUpEdit)sender).Properties.GetDataSourceRowByKeyValue(cbbPg1Bac.EditValue));
                if (o != null)
                {
                    txtHeSo.EditValue = o.HeSo;
                }
            }
            else
            {
                txtHeSo.EditValue = 0;
            }
        }
        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    BindingData(new LuongDTO());
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    SetReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetReadOnly(true);
                    if (luongDTO.Id > 0)
                    {
                        btnPg3Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg3Sua.Enabled = false;
                    }
                    btnPg3Them.Enabled = false;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetReadOnly(false);
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void SetReadOnly(bool read)
        {
            txtSoQD.Properties.ReadOnly = read;
            cbbPg1MaNgach.Properties.ReadOnly = read;
            cbbPg1Bac.Properties.ReadOnly = read;
            chbNamDau.Properties.ReadOnly = read;
            txtLuongCB.Properties.ReadOnly = read;
            txtLuongChinh.Properties.ReadOnly = read;
            txtPCChucvu.Properties.ReadOnly = read;
            txtPCThamnien.Properties.ReadOnly = read;
            txtTileVuot.Properties.ReadOnly = read;
            txtDongBHXH.Properties.ReadOnly = read;
            deNgayhuong.Properties.ReadOnly = read;
            txtPhuCap.Properties.ReadOnly = read;
        }

        private void btnPg3TaoMoi_Click(object sender, EventArgs e)
        {
            luongDTO = new LuongDTO();
            _status = Common.STATUS.NEW;
            SetStatusComponents();
        }

        private void btnPg3Sua_Click(object sender, EventArgs e)
        {
            if (luongDTO.Id > 0)
            {
                _status = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }

        private bool GetDataOncomponentsAndCheck()
        {
            if (cbbPg1MaNgach.EditValue == null || cbbPg1Bac.EditValue == null)
                return false;
            luongDTO.MaNV = strMaNV;
            luongDTO.SoQuyetDinh = txtSoQD.EditValue as String;
            luongDTO.MaNgach = cbbPg1MaNgach.EditValue as String;
            luongDTO.BacLuong = cbbPg1Bac.EditValue as String;
            if (txtHeSo.EditValue != null)
            luongDTO.HeSoLuong = Double.Parse(txtHeSo.EditValue.ToString());
            if (!String.IsNullOrEmpty(txtLuongCB.Text))
                luongDTO.LuongCB = Double.Parse(txtLuongCB.Text);
            else
                luongDTO.LuongCB = 0;
            if (!String.IsNullOrEmpty(txtLuongChinh.Text))
                luongDTO.LuongChinh = Double.Parse(txtLuongChinh.Text);
            else
                luongDTO.LuongChinh = 0;
            if (!String.IsNullOrEmpty(txtPCChucvu.Text))
                luongDTO.PCChucVu = Double.Parse(txtPCChucvu.Text);
            else
                luongDTO.PCChucVu = 0;
            if (!String.IsNullOrEmpty(txtPCThamnien.Text))
                luongDTO.PCThamNien = Double.Parse(txtPCThamnien.Text);
            else
                luongDTO.PCThamNien = 0;
            if (!String.IsNullOrEmpty(txtTileVuot.Text))
                luongDTO.VuotKhung = txtTileVuot.Text;
            else
                luongDTO.VuotKhung = "0";
            if (!String.IsNullOrEmpty(txtDongBHXH.Text))
                luongDTO.LuongBHXH = Double.Parse(txtDongBHXH.Text);
            else
                luongDTO.LuongBHXH = 0;
            if (!String.IsNullOrEmpty(deNgayhuong.Text))
            {
                DateTime NgayHuong = DateTime.Parse(deNgayhuong.EditValue.ToString());
                luongDTO.NgayHuong = NgayHuong.ToString("yyyy-MM-dd");
            }
            else
                luongDTO.NgayHuong = null;
            if (!String.IsNullOrEmpty(txtPhuCap.Text))
                luongDTO.PhuCap = Double.Parse(txtPhuCap.Text);
            if (chbNamDau.Checked)
                luongDTO.HeSoNamDau = "85";
            else
                luongDTO.HeSoNamDau = "0";
            return true; 
        }

        private void btnPg3Them_Click(object sender, EventArgs e)
        {
            if (GetDataOncomponentsAndCheck())
            {
                bool kq = false;
                if (_status == Common.STATUS.NEW)
                {
                    kq = luongCTL.LuuMoiLuong(luongDTO);
                }
                if (_status == Common.STATUS.EDIT)
                {
                    kq = luongCTL.CapNhatLuong(luongDTO);
                }

                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _status = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusComponents();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg3Xoa_Click(object sender, EventArgs e)
        {
            if (luongDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (luongCTL.XoaLuong(luongDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void gridViewLuong_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                luongDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as LuongDTO;
                if (luongDTO == null) luongDTO = new LuongDTO();
                BindingData(luongDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void gridViewLuong_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            luongDTO = ((GridView)sender).GetRow(e.RowHandle) as LuongDTO;
            if (luongDTO == null) luongDTO = new LuongDTO();
            BindingData(luongDTO);
            _status = Common.STATUS.VIEW;
            SetStatusComponents();
        }

    }
}
