﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System;

namespace HerculesHRMT
{
    public partial class ucPhongKhoa : XtraUserControl
    {
        PhongKhoaCTL pkCTL = new PhongKhoaCTL();
        List<PhongKhoaDTO> listPK = new List<PhongKhoaDTO>();
        PhongKhoaDTO pkSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucPhongKhoa()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listPK = pkCTL.GetAll();

            gcPhongKhoa.DataSource = listPK;
            btnLuu.Enabled = false;

        }

        private void BindingData(PhongKhoaDTO pkDTO)
        {
            if (pkDTO != null)
            {
                txtDonVi.Text = pkDTO.TenPhongKhoa;
                txtTenVT.Text = pkDTO.TenVietTat;
                txtThuTu.Text = pkDTO.ThuTuBaoCao.ToString();
                meGhiChu.Text = pkDTO.GhiChu;
            }
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtDonVi.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtThuTu.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;


                    txtDonVi.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    txtThuTu.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtDonVi.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtThuTu.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(pkSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtDonVi.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    txtThuTu.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(pkSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtDonVi.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private PhongKhoaDTO GetData(PhongKhoaDTO pkDTO)
        {
            string maPK = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexNganh = pkCTL.GetMaxMaPhongKhoa();
                if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexNganh);
                    maPK += (nLastIndexTG + 1).ToString().PadLeft(2,'0');
                }
                else
                    maPK += "01";

                pkDTO.NgayHieuLuc = DateTime.Now;
                pkDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maPK = pkSelect.MaPhongKhoa;
                pkDTO.NgayHieuLuc = pkSelect.NgayHieuLuc;
                pkDTO.TinhTrang = pkSelect.TinhTrang;
            }
            pkDTO.MaPhongKhoa = maPK;
            pkDTO.TenPhongKhoa = txtDonVi.Text;

            int result = 0;
            if (int.TryParse(txtThuTu.Text, out result))
                pkDTO.ThuTuBaoCao = result;
            else
                pkDTO.ThuTuBaoCao = null;            

            pkDTO.TenVietTat = txtTenVT.Text;
            pkDTO.GhiChu = meGhiChu.Text;

            return pkDTO;
        }

        private void ucPhongKhoa_Load(object sender, System.EventArgs e)
        {
            LoadData();
            txtTenVT.Properties.MaxLength = 50;
            SetStatus(VIEW);
        }

        private void gridViewPhongKhoa_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listPK.Count)
            {
                pkSelect = ((GridView)sender).GetRow(_selectedIndex) as PhongKhoaDTO;
            }
            else
                if (listPK.Count != 0)
                    pkSelect = listPK[0];
                else
                    pkSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên đơn vị\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            PhongKhoaDTO pkNew = GetData(new PhongKhoaDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = pkCTL.Save(pkNew);
            else
                isSucess = pkCTL.Update(pkNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listPK.IndexOf(listPK.FirstOrDefault(p => p.MaPhongKhoa == pkNew.MaPhongKhoa));
                gridViewPhongKhoa.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (pkSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa đơn vị " + pkSelect.TenPhongKhoa + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = pkCTL.Delete(pkSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listPK.Count != 0)
                            gridViewPhongKhoa.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }


    }
}
