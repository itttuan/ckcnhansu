﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucPhongKhoa
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPhongKhoa = new DevExpress.XtraGrid.GridControl();
            this.gridViewPhongKhoa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaDonVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDonVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColViettat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThuTuBC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtThuTu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtDonVi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcPhongKhoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhongKhoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThuTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonVi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPhongKhoa
            // 
            this.gcPhongKhoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPhongKhoa.Location = new System.Drawing.Point(2, 2);
            this.gcPhongKhoa.MainView = this.gridViewPhongKhoa;
            this.gcPhongKhoa.Name = "gcPhongKhoa";
            this.gcPhongKhoa.Size = new System.Drawing.Size(781, 296);
            this.gcPhongKhoa.TabIndex = 3;
            this.gcPhongKhoa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPhongKhoa});
            // 
            // gridViewPhongKhoa
            // 
            this.gridViewPhongKhoa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaDonVi,
            this.gridColTenDonVi,
            this.gridColViettat,
            this.gridColGhiChu,
            this.gridColThuTuBC});
            this.gridViewPhongKhoa.GridControl = this.gcPhongKhoa;
            this.gridViewPhongKhoa.GroupPanelText = " ";
            this.gridViewPhongKhoa.Name = "gridViewPhongKhoa";
            this.gridViewPhongKhoa.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewPhongKhoa_FocusedRowChanged);
            // 
            // gridColMaDonVi
            // 
            this.gridColMaDonVi.Caption = "Mã Đơn Vị";
            this.gridColMaDonVi.FieldName = "MaPhongKhoa";
            this.gridColMaDonVi.Name = "gridColMaDonVi";
            // 
            // gridColTenDonVi
            // 
            this.gridColTenDonVi.Caption = "Đơn vị Phòng/Khoa";
            this.gridColTenDonVi.FieldName = "TenPhongKhoa";
            this.gridColTenDonVi.Name = "gridColTenDonVi";
            this.gridColTenDonVi.OptionsColumn.AllowEdit = false;
            this.gridColTenDonVi.OptionsColumn.ReadOnly = true;
            this.gridColTenDonVi.Visible = true;
            this.gridColTenDonVi.VisibleIndex = 0;
            this.gridColTenDonVi.Width = 190;
            // 
            // gridColViettat
            // 
            this.gridColViettat.Caption = "Tên Viết Tắt";
            this.gridColViettat.FieldName = "TenVietTat";
            this.gridColViettat.Name = "gridColViettat";
            this.gridColViettat.OptionsColumn.AllowEdit = false;
            this.gridColViettat.OptionsColumn.ReadOnly = true;
            this.gridColViettat.Visible = true;
            this.gridColViettat.VisibleIndex = 1;
            this.gridColViettat.Width = 190;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 3;
            this.gridColGhiChu.Width = 283;
            // 
            // gridColThuTuBC
            // 
            this.gridColThuTuBC.Caption = "Thứ Tự Báo Cáo";
            this.gridColThuTuBC.FieldName = "ThuTuBaoCao";
            this.gridColThuTuBC.Name = "gridColThuTuBC";
            this.gridColThuTuBC.OptionsColumn.AllowEdit = false;
            this.gridColThuTuBC.OptionsColumn.ReadOnly = true;
            this.gridColThuTuBC.Visible = true;
            this.gridColThuTuBC.VisibleIndex = 2;
            this.gridColThuTuBC.Width = 100;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcPhongKhoa);
            this.panelControl3.Location = new System.Drawing.Point(0, 217);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 300);
            this.panelControl3.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtThuTu);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtDonVi);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 202);
            this.panelControl1.TabIndex = 5;
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(480, 69);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(250, 20);
            this.txtTenVT.TabIndex = 1;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(480, 105);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(250, 61);
            this.meGhiChu.TabIndex = 3;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(408, 72);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Tên viết tắt:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(408, 119);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // txtThuTu
            // 
            this.txtThuTu.Location = new System.Drawing.Point(120, 116);
            this.txtThuTu.Name = "txtThuTu";
            this.txtThuTu.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtThuTu.Properties.Mask.EditMask = "d";
            this.txtThuTu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThuTu.Size = new System.Drawing.Size(240, 20);
            this.txtThuTu.TabIndex = 2;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(30, 119);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(78, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Thứ tự báo cáo:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(646, 172);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(553, 172);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 6;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(460, 172);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(367, 172);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 4;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtDonVi
            // 
            this.txtDonVi.Location = new System.Drawing.Point(120, 69);
            this.txtDonVi.Name = "txtDonVi";
            this.txtDonVi.Size = new System.Drawing.Size(240, 20);
            this.txtDonVi.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Đơn vị:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(250, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(280, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Đơn Vị Phòng - Khoa";
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // ucPhongKhoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucPhongKhoa";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucPhongKhoa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcPhongKhoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhongKhoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThuTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonVi.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControl gcPhongKhoa;
        private GridView gridViewPhongKhoa;
        private GridColumn gridColMaDonVi;
        private GridColumn gridColTenDonVi;
        private GridColumn gridColGhiChu;
        private GridColumn gridColTenDayDu;
        private PanelControl panelControl3;
        private PanelControl panelControl2;
        private PanelControl panelControl1;
        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtDonVi;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenVietTat;
        private TextEdit txtThuTu;
        private LabelControl labelControl5;
        private TextEdit txtTenVT;
        private LabelControl labelControl6;
        private GridColumn gridColViettat;
        private GridColumn gridColThuTuBC;
    }
}
