﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesDTO;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucNVSoLuocLyLich : DevExpress.XtraEditors.XtraUserControl
    {
        NhanVienCTL nhanvienCTL = new NhanVienCTL();
        NhanVienDTO nhanvienDTO = null;
        NhanVienDTO nhanvienNEW = new NhanVienDTO();
        Common.STATUS _status;
        BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
        PhongKhoaCTL pkCTL = new PhongKhoaCTL();
        QuanHuyenCTL quanhuyenCTL = new QuanHuyenCTL();
        HeSoLuongCTL hslDAC = new HeSoLuongCTL();


        #region CAU HINH THEM NHAN VIEN
        // false: opened by add new staff
        // true : opened by edit staff
        private readonly Boolean _openedByEditStaff = true;

        // private string LUU_TAM = "Lưu Tạm";

        //public delegate void SendSoYeuLyLich(NhanVienDTO nhanVienDto);
        //public SendSoYeuLyLich SoYeuLyLichSender;

        public delegate void SendSoYeuLyLich(string maNv);
        public SendSoYeuLyLich SoYeuLyLichSender;

        public delegate void SendMainInfo(string hoLot, string ten, string phongKhoa, string boMonBoPhan);
        public SendMainInfo MainInfoSender;
        #endregion

        public ucNVSoLuocLyLich()
        {
            InitializeComponent();
        }

        public ucNVSoLuocLyLich(Boolean openedByEditStaff = true)
        {
            InitializeComponent();

            if (openedByEditStaff) return;

            _openedByEditStaff = false;

            txtPg1HoDem.EditValueChanged += mainInfo_EditValueChanged;
            txtPg1Ten.EditValueChanged += mainInfo_EditValueChanged;
            cbbPg1PhongKhoa.EditValueChanged += mainInfo_EditValueChanged;
            cbbPg1BMBP.EditValueChanged += mainInfo_EditValueChanged;

            btnXemCacPhienBan.Visible = false;
            btnHieuChinh.Visible = false;
        }
        public delegate void SendInfor(string id);
        public SendInfor lylichSender;
        private void ucNVSoLuocLyLich_Load(object sender, EventArgs e)
        {
            cbbPg1PhongKhoa.Properties.DataSource = Program.listPhongKhoa;
            cbbPg1BMBP.Properties.DataSource = Program.listBMBP;
            cbbPg1ChucVu.Properties.DataSource = Program.listChucVu;
            cbbPg1TinhThanhHT.Properties.DataSource = Program.listTinhThanh;
            //txtPg1NoiSinh.Properties.DataSource = Program.listTinhThanh;
            //txtPg1QueQuan.Properties.DataSource = Program.listTinhThanh;
            cbbPg1NoiCapCMND.Properties.DataSource = Program.listTinhThanh;
            cbbPg1TinhThanhTT.Properties.DataSource = Program.listTinhThanh;
            cbbPg1QuanHuyenTT.Properties.DataSource = Program.listQuanHuyen;
            cbbPg1QuanHuyenHT.Properties.DataSource = Program.listQuanHuyen;
            cbbPg1DanToc.Properties.DataSource = Program.listDanToc;
            cbbPg1TonGiao.Properties.DataSource = (new TonGiaoCTL()).GetAll();
            cbbPg1MaNgach.Properties.DataSource = (new NgachCTL()).LayDanhSachNgach();
            //cbbPg1TinhTrangLamViec.Properties.DataSource = (new Ti)
            //BindingData(nhanvienDTO, _status);
        }


        public void SetNhanVien(NhanVienDTO nvDTO, Common.STATUS status)
        {
            nhanvienDTO = nvDTO;
            nhanvienNEW = nvDTO;
            BindingData(nhanvienDTO);
            _status = status;
            SetStatusComponents();
            if (nvDTO == null)
            {
                SetInvisibleButton();
            }
        }
        public void BindingData(NhanVienDTO nhanvienDTO)
        {
            if (nhanvienDTO != null)
            {
                #region Binding Data To Components
                txtPg1HoDem.Text = nhanvienDTO.Ho;
                txtPg1Ten.Text = nhanvienDTO.Ten;
                txtPg1TenKhac.Text = nhanvienDTO.TenGoiKhac;
                dtPg1NgaySinh.EditValue = nhanvienDTO.NgaySinh;
                cbbPg1BMBP.EditValue = nhanvienDTO.MaBMBP;
                cbbPg1PhongKhoa.EditValue = nhanvienDTO.MaPK;
                dtPg1NgayVaoLam.EditValue = nhanvienDTO.NgayVaoLam;
                cbbPg1ChucVu.EditValue = nhanvienDTO.MaChucVu;
                cbbPg1Bac.EditValue = nhanvienDTO.BacNhanVien;
                cbbPg1DanToc.EditValue = nhanvienDTO.MaDanToc;
                if (nhanvienDTO.Email != null && nhanvienDTO.Email != "")
                {
                    cbbPg1EmailTail.EditValue = nhanvienDTO.Email.Substring(nhanvienDTO.Email.IndexOf("@") + 1);
                    txtPg1Email.EditValue = nhanvienDTO.Email.Substring(0, nhanvienDTO.Email.IndexOf("@"));
                }
                else
                {
                    txtPg1Email.EditValue = nhanvienDTO.Email;
                }
                chbPg1GiaDinhChinhSach.EditValue = nhanvienDTO.GiaDinhChinhSach;
                cbbPg1Hang.EditValue = nhanvienDTO.HangNhanVien;
                cbbPg1MaNgach.EditValue = nhanvienDTO.MaNgach;
                cbbPg1NhomMau.EditValue = nhanvienDTO.NhomMau;
                cbbPg1NoiCapCMND.EditValue = nhanvienDTO.MaNoiCapCMND;
                txtPg1NoiSinh.EditValue = nhanvienDTO.NoiSinh;
                txtPg1QueQuan.EditValue = nhanvienDTO.QueQuan;
                cbbPg1QuocTich.EditValue = nhanvienDTO.QuocTich;
                cbbPg1TinhThanhHT.EditValue = nhanvienDTO.DiaChiHienTaiMaTinhThanh;
                cbbPg1TinhThanhTT.EditValue = nhanvienDTO.HoKhauThuongTruMaTinhThanh;
                cbbPg1QuanHuyenHT.EditValue = nhanvienDTO.DiaChiHienTaiMaQuanHuyen;
                cbbPg1QuanHuyenTT.EditValue = nhanvienDTO.HoKhauThuongTruMaQuanHuyen;
                cbbPg1TinhTrangHonNhan.EditValue = nhanvienDTO.TinhTrangHonNhan;
                cbbPg1TinhTrangLamViec.EditValue = nhanvienDTO.TinhTrangLamViec;
                cbbPg1TonGiao.EditValue = nhanvienDTO.MaTonGiao;
                cbbPg1VanHoa.EditValue = nhanvienDTO.TrinhDoVanHoa;
                txtPg1CanNang.EditValue = nhanvienDTO.CanNang;
                txtPg1ChieuCao.EditValue = nhanvienDTO.ChieuCao;
                txtPg1DiaChiHT.EditValue = nhanvienDTO.DiaChiHienTaiDiaChi;
                txtPg1DiaChiTT.EditValue = nhanvienDTO.HoKhauThuongTruDiaChi;
                txtPg1DienThoai.EditValue = nhanvienDTO.DienThoai;
                txtPg1QuanHamCaoNhat.EditValue = nhanvienDTO.QuanHamCaoNhat;
                txtPg1SoCMND.EditValue = nhanvienDTO.CMND;
                txtPg1TinhTrangSucKhoe.EditValue = nhanvienDTO.TinhTrangSucKhoe;
                dtPg1NgayCapCMND.EditValue = nhanvienDTO.NgayCapCMND;
                dtPg1NgayChinhThucDCSVN.EditValue = nhanvienDTO.NgayChinhThucVaoDCSVN;
                dtPg1NgayVaoDCSVN.EditValue = nhanvienDTO.NgayVaoDCSVN;
                dtPg1NgayNhapNgu.EditValue = nhanvienDTO.NgayNhapNgu;
                dtPg1NgayXuatNgu.EditValue = nhanvienDTO.NgayXuatNgu;
                cbbPg1ChuyenMon.EditValue = nhanvienDTO.NgheNghiepTuyenDung;
                rdbPg1GioiTinh.EditValue = Convert.ToInt32(nhanvienDTO.GioiTinh);
                if (nhanvienDTO.ThuongBinh && nhanvienDTO.ThuongBinhHang !=null && nhanvienDTO.ThuongBinhHang !="")
                {
                    txtPg1ThuongBinhHangSecond.EditValue = nhanvienDTO.ThuongBinhHang.Substring(nhanvienDTO.ThuongBinhHang.IndexOf("/") + 1);
                    txtPg1ThuongBinhHangFirst.EditValue = nhanvienDTO.ThuongBinhHang.Substring(0,nhanvienDTO.ThuongBinhHang.IndexOf("/"));
                }
                chbPg1DangVien.Checked = nhanvienDTO.DangVien;
                chbPg1QuanNgu.Checked = nhanvienDTO.QuanNgu;
                chbPg1ThuongBinh.Checked = nhanvienDTO.ThuongBinh;
                chbPg1GiaDinhChinhSach.Checked = nhanvienDTO.GiaDinhChinhSach;
                #endregion
            }
        }


        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    BindingData(nhanvienNEW);
                    break;
                case Common.STATUS.VIEW:
                    setReadOnly(true);
                    btnHieuChinh.Enabled = true;
                    btnLuu.Enabled = false;
                    btnReset.Enabled = false;
                    break;
                case Common.STATUS.EDIT:
                    setReadOnly(false);
                    btnHieuChinh.Enabled = false;
                    btnLuu.Enabled = true;
                    btnReset.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void cbbPg1TinhThanhTT_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1TinhThanhTT.EditValue != null)
            {
                cbbPg1QuanHuyenTT.Properties.DataSource = quanhuyenCTL.GetListByMaTinhThanh(cbbPg1TinhThanhTT.EditValue.ToString());

            }
        }

        private void cbbPg1TinhThanhHT_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1TinhThanhHT.EditValue != null)
            {
                cbbPg1QuanHuyenHT.Properties.DataSource = quanhuyenCTL.GetListByMaTinhThanh(cbbPg1TinhThanhHT.EditValue.ToString());

            }
        }

        private void setReadOnly(bool read)
        {
            txtPg1CanNang.Properties.ReadOnly = read;
            txtPg1ChieuCao.Properties.ReadOnly = read;
            txtPg1DiaChiHT.Properties.ReadOnly = read;
            txtPg1DiaChiTT.Properties.ReadOnly = read;
            txtPg1DienThoai.Properties.ReadOnly = read;
            txtPg1Email.Properties.ReadOnly = read;
            txtPg1HoDem.Properties.ReadOnly = read;
            txtPg1QuanHamCaoNhat.Properties.ReadOnly = read;
            txtPg1SoCMND.Properties.ReadOnly = read;
            txtPg1Ten.Properties.ReadOnly = read;
            txtPg1TenKhac.Properties.ReadOnly = read;
            txtPg1ThuongBinhHangFirst.Properties.ReadOnly = read;
            txtPg1ThuongBinhHangSecond.Properties.ReadOnly = read;
            txtPg1TinhTrangSucKhoe.Properties.ReadOnly = read;
            cbbPg1Bac.Properties.ReadOnly = read;
            cbbPg1BMBP.Properties.ReadOnly = read;
            cbbPg1ChucVu.Properties.ReadOnly = read;
            cbbPg1ChuyenMon.Properties.ReadOnly = read;
            cbbPg1DanToc.Properties.ReadOnly = read;
            cbbPg1EmailTail.Properties.ReadOnly = read;
            chbPg1GiaDinhChinhSach.Properties.ReadOnly = read;
            cbbPg1Hang.Properties.ReadOnly = read;
            cbbPg1MaNgach.Properties.ReadOnly = read;
            cbbPg1NhomMau.Properties.ReadOnly = read;
            cbbPg1NoiCapCMND.Properties.ReadOnly = read;
            txtPg1NoiSinh.Properties.ReadOnly = read;
            cbbPg1PhongKhoa.Properties.ReadOnly = read;
            cbbPg1QuanHuyenHT.Properties.ReadOnly = read;
            cbbPg1QuanHuyenTT.Properties.ReadOnly = read;
            txtPg1QueQuan.Properties.ReadOnly = read;
            cbbPg1QuocTich.Properties.ReadOnly = read;
            cbbPg1TinhThanhHT.Properties.ReadOnly = read;
            cbbPg1TinhThanhTT.Properties.ReadOnly = read;
            cbbPg1TinhTrangHonNhan.Properties.ReadOnly = read;
            cbbPg1TinhTrangLamViec.Properties.ReadOnly = read;
            cbbPg1TonGiao.Properties.ReadOnly = read;
            cbbPg1VanHoa.Properties.ReadOnly = read;
            dtPg1NgayCapCMND.Properties.ReadOnly = read;
            dtPg1NgayChinhThucDCSVN.Properties.ReadOnly = read;
            dtPg1NgayNhapNgu.Properties.ReadOnly = read;
            dtPg1NgaySinh.Properties.ReadOnly = read;
            dtPg1NgayVaoDCSVN.Properties.ReadOnly = read;
            dtPg1NgayVaoLam.Properties.ReadOnly = read;
            dtPg1NgayXuatNgu.Properties.ReadOnly = read;
            rdbPg1GioiTinh.Properties.ReadOnly = read;
            chbPg1DangVien.Properties.ReadOnly = read;
            chbPg1QuanNgu.Properties.ReadOnly = read;
            chbPg1ThuongBinh.Properties.ReadOnly = read;
        }

        private void btnHieuChinh_Click(object sender, EventArgs e)
        {
            _status = Common.STATUS.EDIT;
            SetStatusComponents();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            nhanvienNEW = nhanvienDTO;
            BindingData(nhanvienNEW);
        }

        private bool GetDataOnComponentsAndCheck()
        {
            var messages = new List<string>();
            // check txtPg1Ten not null

            if (_openedByEditStaff)
            {
                nhanvienNEW.SoTaiKhoan = nhanvienDTO.SoTaiKhoan;
                nhanvienNEW.MaSoThue = nhanvienDTO.MaSoThue;
                nhanvienNEW.SoBHXH = nhanvienDTO.SoBHXH;
                nhanvienNEW.SoATM = nhanvienDTO.SoATM;
                nhanvienNEW.NgheNghiepTruocTuyenDung = nhanvienDTO.NgheNghiepTruocTuyenDung;
            }

            if (string.IsNullOrEmpty(txtPg1Ten.Text))
            {
                messages.Add(Constants.ErrTenNhanVienEmpty);
            }
            else
            {
                nhanvienNEW.Ten = txtPg1Ten.Text;
            }

            // check txtPg1CMND not null
            //if (string.IsNullOrEmpty(txtPg1SoCMND.Text))
            //{
            //    messages.Add(Constants.ErrCmndEmpty);
            //}
            //else
            //{
                nhanvienNEW.CMND = txtPg1SoCMND.Text;
            //}
            //// check cbbPg1NoiCapCMND not null
            //if (string.IsNullOrEmpty(cbbPg1NoiCapCMND.Text))
            //{
            //    messages.Add(Constants.ErrNoiCapCmndEmpty);
            //}
            //else
            //{

            // [Matt] using for add new staff to check null value
                if (cbbPg1NoiCapCMND.EditValue != null)
                {
                    nhanvienNEW.MaNoiCapCMND = cbbPg1NoiCapCMND.EditValue.ToString();
                }
                else
                {
                    nhanvienNEW.MaNoiCapCMND = String.Empty;
                }
            //}
            if (dtPg1NgayCapCMND.EditValue != null && dtPg1NgayCapCMND.Text != "")
                {
                    DateTime NgayCapCMND = DateTime.Parse(dtPg1NgayCapCMND.EditValue.ToString());
                    nhanvienNEW.NgayCapCMND = NgayCapCMND.ToString("yyyy-MM-dd");
                }
                else
                {
                    nhanvienNEW.NgayCapCMND = String.Empty;
                }
            // check cbbPg1PhongKhoa not null
            if (string.IsNullOrEmpty(cbbPg1PhongKhoa.Text))
            {
                messages.Add(Constants.ErrPhongKhoaEmpty);
            }

            // check cbbPg1BMBP not null
            if (string.IsNullOrEmpty(cbbPg1BMBP.Text))
            {
                messages.Add(Constants.ErrBMonBPhanEmpty);
            }
            else
            {
                nhanvienNEW.MaBMBP = cbbPg1BMBP.EditValue.ToString();
            }
            nhanvienNEW.MaPK = cbbPg1PhongKhoa.EditValue as String;
            // check DiaChiThuongTru not null
            if (string.IsNullOrEmpty(txtPg1DiaChiTT.Text) || string.IsNullOrEmpty(cbbPg1TinhThanhTT.Text) || string.IsNullOrEmpty(cbbPg1QuanHuyenTT.Text))
            {
                messages.Add(Constants.ErrDiaChiThuongTruEmpty);
            }
            else
            {
                nhanvienNEW.HoKhauThuongTruDiaChi = txtPg1DiaChiTT.Text;
                nhanvienNEW.HoKhauThuongTruMaTinhThanh = cbbPg1TinhThanhTT.EditValue.ToString();
                nhanvienNEW.HoKhauThuongTruMaQuanHuyen = cbbPg1QuanHuyenTT.EditValue.ToString();
            }

            // check DiaChiThuongTru not null
            if (string.IsNullOrEmpty(txtPg1DiaChiHT.Text) || string.IsNullOrEmpty(cbbPg1TinhThanhHT.Text) || string.IsNullOrEmpty(cbbPg1QuanHuyenHT.Text))
            {
                messages.Add(Constants.ErrDiaChiHienTaiEmpty);
            }
            else
            {
                nhanvienNEW.DiaChiHienTaiDiaChi = txtPg1DiaChiHT.Text;
                nhanvienNEW.DiaChiHienTaiMaTinhThanh = cbbPg1TinhThanhHT.EditValue.ToString();
                nhanvienNEW.DiaChiHienTaiMaQuanHuyen = cbbPg1QuanHuyenHT.EditValue.ToString();
            }

            #region THONG TIN CO BAN
            // [Matt] using for add new staff to check null value
            if (_openedByEditStaff)
            {
                nhanvienNEW.MaNV = nhanvienDTO.MaNV;
            }
            
            nhanvienNEW.Ho = txtPg1HoDem.EditValue as String;
            nhanvienNEW.TenGoiKhac = txtPg1TenKhac.EditValue as String;
            nhanvienNEW.GioiTinh = Convert.ToBoolean(rdbPg1GioiTinh.EditValue);
            nhanvienNEW.MaDanToc = cbbPg1DanToc.EditValue as String;
            nhanvienNEW.MaTonGiao = cbbPg1TonGiao.EditValue as String;
            nhanvienNEW.QuocTich = cbbPg1QuocTich.EditValue as String;
            if (dtPg1NgaySinh.EditValue != null && dtPg1NgaySinh.Text != "")
            {
                DateTime NgaySinh = DateTime.Parse(dtPg1NgaySinh.EditValue.ToString());
                nhanvienNEW.NgaySinh = NgaySinh.ToString("yyyy-MM-dd");
            }
            else
            {
                nhanvienNEW.NgaySinh = String.Empty;
            }

            nhanvienNEW.NoiSinh = txtPg1NoiSinh.EditValue as String;
            nhanvienNEW.QueQuan = txtPg1QueQuan.EditValue as String;
            nhanvienNEW.TrinhDoVanHoa = cbbPg1VanHoa.EditValue as String;
            nhanvienNEW.TinhTrangHonNhan = cbbPg1TinhTrangHonNhan.EditValue as String;

            #endregion

            #region THONG TIN LAM VIEC
            if (dtPg1NgayVaoLam.EditValue != null && dtPg1NgayVaoLam.Text !="")
            {
                DateTime NgayVaoLam = DateTime.Parse(dtPg1NgayVaoLam.EditValue.ToString());
                nhanvienNEW.NgayVaoLam = NgayVaoLam.ToString("yyyy-MM-dd");
            }
            else
            {
                nhanvienNEW.NgayVaoLam = String.Empty;
            }

            nhanvienNEW.MaChucVu = cbbPg1ChucVu.EditValue as String;
            nhanvienNEW.TinhTrangLamViec = cbbPg1TinhTrangLamViec.EditValue as String;
            nhanvienNEW.MaNgach = cbbPg1MaNgach.EditValue as String;
            nhanvienNEW.HangNhanVien = cbbPg1Hang.EditValue as String;
            nhanvienNEW.BacNhanVien = cbbPg1Bac.EditValue as String;
            #endregion

            #region THONG TIN LIEN LAC
            nhanvienNEW.DienThoai = txtPg1DienThoai.EditValue as String;
            nhanvienNEW.Email = !string.IsNullOrEmpty(txtPg1Email.Text) ? string.Format(Constants.EmailFormat, txtPg1Email.Text, cbbPg1EmailTail.Text) : null;
            #endregion

            #region THONG TIN KHAC
            nhanvienNEW.DangVien = chbPg1DangVien.Checked;
            if (chbPg1DangVien.Checked)
            {
                if (dtPg1NgayVaoDCSVN.EditValue != null && dtPg1NgayVaoDCSVN.Text != "")
                {
                    DateTime NgayVaoDCSVN = DateTime.Parse(dtPg1NgayVaoDCSVN.EditValue.ToString());
                    nhanvienNEW.NgayVaoDCSVN = NgayVaoDCSVN.ToString("yyyy-MM-dd");
                }
                else
                {
                    nhanvienNEW.NgayVaoDCSVN = String.Empty;
                }
                if (dtPg1NgayChinhThucDCSVN.EditValue != null && dtPg1NgayChinhThucDCSVN.Text != "")
                {
                    DateTime NgayChinhThuc = DateTime.Parse(dtPg1NgayChinhThucDCSVN.EditValue.ToString());
                    nhanvienNEW.NgayChinhThucVaoDCSVN = NgayChinhThuc.ToString("yyyy-MM-dd");
                }
                else
                {
                    nhanvienNEW.NgayChinhThucVaoDCSVN = String.Empty;
                }
            }
            nhanvienNEW.QuanNgu = chbPg1QuanNgu.Checked;
            if (chbPg1QuanNgu.Checked)
            {
                if (dtPg1NgayNhapNgu.EditValue != null && dtPg1NgayNhapNgu.Text != "")
                {
                    DateTime NgayNhapNgu = DateTime.Parse(dtPg1NgayNhapNgu.EditValue.ToString());
                    nhanvienNEW.NgayNhapNgu = NgayNhapNgu.ToString("yyyy-MM-dd");
                }
                else
                {
                    nhanvienNEW.NgayNhapNgu = String.Empty;
                }
                if (dtPg1NgayXuatNgu.EditValue != null && dtPg1NgayXuatNgu.Text != "")
                {
                    DateTime NgayXuatNgu = DateTime.Parse(dtPg1NgayXuatNgu.EditValue.ToString());
                    nhanvienNEW.NgayXuatNgu = NgayXuatNgu.ToString("yyyy-MM-dd");
                }
                else
                {
                    nhanvienNEW.NgayXuatNgu = String.Empty;
                }


                nhanvienNEW.QuanHamCaoNhat = txtPg1QuanHamCaoNhat.EditValue as String;
            }
            nhanvienNEW.TinhTrangSucKhoe = txtPg1TinhTrangSucKhoe.EditValue as String;
            nhanvienNEW.ChieuCao = txtPg1ChieuCao.EditValue as String;
            nhanvienNEW.CanNang = txtPg1CanNang.EditValue as String;
            nhanvienNEW.NhomMau = cbbPg1NhomMau.EditValue as String;
            nhanvienNEW.ThuongBinh = chbPg1ThuongBinh.Checked;
            if (chbPg1ThuongBinh.Checked)
            {
                nhanvienNEW.ThuongBinhHang = string.Format(Constants.HangThuongBinhFormat, txtPg1ThuongBinhHangFirst.Text,
                    txtPg1ThuongBinhHangSecond.Text);
            }
            nhanvienNEW.GiaDinhChinhSach = chbPg1GiaDinhChinhSach.Checked;
            #endregion

            // [Matt] using for add new staff to check null value
            if (_openedByEditStaff)
            {
                nhanvienNEW.TuNhanXet = nhanvienDTO.TuNhanXet;
            }
            return messages.Count == 0;
        }
        private bool CheckChangeNhanVien()
        {
            if (CheckDifference(nhanvienDTO.BacNhanVien, nhanvienNEW.BacNhanVien))
                return true;
            if (CheckDifference(nhanvienDTO.CanNang, nhanvienNEW.CanNang))
                return true;
            if (CheckDifference(nhanvienDTO.ChieuCao, nhanvienNEW.ChieuCao))
                return true;
            if (CheckDifference(nhanvienDTO.CMND, nhanvienNEW.CMND))
                return true;
            if (CheckDifference(nhanvienDTO.HoKhauThuongTruDiaChi, nhanvienNEW.HoKhauThuongTruDiaChi))
                return true;
            if (CheckDifference(nhanvienDTO.DiaChiHienTaiDiaChi, nhanvienNEW.DiaChiHienTaiDiaChi))
                return true;
            if (CheckDifference(nhanvienDTO.DienThoai, nhanvienNEW.DienThoai))
                return true;
            if (CheckDifference(nhanvienDTO.Email, nhanvienNEW.Email))
                return true;
            if (CheckDifference(nhanvienDTO.GiaDinhChinhSach.ToString(), nhanvienNEW.GiaDinhChinhSach.ToString()))
                return true;
            if (CheckDifference(nhanvienDTO.GioiTinh.ToString(), nhanvienNEW.GioiTinh.ToString()))
                return true;
            if (CheckDifference(nhanvienDTO.HangNhanVien, nhanvienNEW.HangNhanVien))
                return true;
            if (CheckDifference(nhanvienDTO.Ho, nhanvienNEW.Ho))
                return true;
            if (CheckDifference(nhanvienDTO.MaBMBP, nhanvienNEW.MaBMBP))
                return true;
            if (CheckDifference(nhanvienDTO.MaChucVu, nhanvienNEW.MaChucVu))
                return true;
            if (CheckDifference(nhanvienDTO.MaDanToc, nhanvienNEW.MaDanToc))
                return true;
            if (CheckDifference(nhanvienDTO.MaNgach, nhanvienNEW.MaNgach))
                return true;
            if (CheckDifference(nhanvienDTO.HoKhauThuongTruMaQuanHuyen, nhanvienNEW.HoKhauThuongTruMaQuanHuyen))
                return true;
            if (CheckDifference(nhanvienDTO.DiaChiHienTaiMaQuanHuyen, nhanvienNEW.DiaChiHienTaiMaQuanHuyen))
                return true;
            if (CheckDifference(nhanvienDTO.HoKhauThuongTruMaTinhThanh, nhanvienNEW.HoKhauThuongTruMaTinhThanh))
                return true;
            if (CheckDifference(nhanvienDTO.DiaChiHienTaiMaTinhThanh, nhanvienNEW.DiaChiHienTaiMaTinhThanh))
                return true;
            if (CheckDifference(nhanvienDTO.MaTonGiao, nhanvienNEW.MaTonGiao))
                return true;
            if (CheckDifference(nhanvienDTO.NgayCapCMND, nhanvienNEW.NgayCapCMND))
                return true;
            if (CheckDifference(nhanvienDTO.NgayChinhThucVaoDCSVN, nhanvienNEW.NgayChinhThucVaoDCSVN))
                return true;
            if (CheckDifference(nhanvienDTO.NgayNhapNgu, nhanvienNEW.NgayNhapNgu))
                return true;
            if (CheckDifference(nhanvienDTO.NgaySinh, nhanvienNEW.NgaySinh))
                return true;
            if (CheckDifference(nhanvienDTO.NgayTuyenDung, nhanvienNEW.NgayTuyenDung))
                return true;
            if (CheckDifference(nhanvienDTO.NgayVaoDCSVN, nhanvienNEW.NgayVaoDCSVN))
                return true;
            if (CheckDifference(nhanvienDTO.NgayVaoLam, nhanvienNEW.NgayVaoLam))
                return true;
            if (CheckDifference(nhanvienDTO.NgayXuatNgu, nhanvienNEW.NgayXuatNgu))
                return true;
            //if (CheckDifference(nhanvienDTO.NgheNghiepTuyenDung, nhanvienNEW.NgheNghiepTuyenDung))
            //    return true;
            if (CheckDifference(nhanvienDTO.NhomMau, nhanvienNEW.NhomMau))
                return true;
            if (CheckDifference(nhanvienDTO.MaNoiCapCMND, nhanvienNEW.MaNoiCapCMND))
                return true;
            if (CheckDifference(nhanvienDTO.NoiSinh, nhanvienNEW.NoiSinh))
                return true;
            if (CheckDifference(nhanvienDTO.QuanHamCaoNhat, nhanvienNEW.QuanHamCaoNhat))
                return true;
            if (CheckDifference(nhanvienDTO.QueQuan, nhanvienNEW.QueQuan))
                return true;
            if (CheckDifference(nhanvienDTO.QuocTich, nhanvienNEW.QuocTich))
                return true;
            if (CheckDifference(nhanvienDTO.Ten, nhanvienNEW.Ten))
                return true;
            if (CheckDifference(nhanvienDTO.TenGoiKhac, nhanvienNEW.TenGoiKhac))
                return true;
            if (CheckDifference(nhanvienDTO.TenThuongDung, nhanvienNEW.TenThuongDung))
                return true;
            if (CheckDifference(nhanvienDTO.ThanhPhanGiaDinhXuatThan, nhanvienNEW.ThanhPhanGiaDinhXuatThan))
                return true;
            if (CheckDifference(nhanvienDTO.ThuongBinhHang, nhanvienNEW.ThuongBinhHang))
                return true;
            if (CheckDifference(nhanvienDTO.TinhTrangHonNhan, nhanvienNEW.TinhTrangHonNhan))
                return true;
            if (CheckDifference(nhanvienDTO.TinhTrangLamViec, nhanvienNEW.TinhTrangLamViec))
                return true;
            if (CheckDifference(nhanvienDTO.TinhTrangSucKhoe, nhanvienNEW.TinhTrangSucKhoe))
                return true;
            if (CheckDifference(nhanvienDTO.TrinhDoVanHoa, nhanvienNEW.TrinhDoVanHoa))
                return true;
            return false;
        }

        private bool CheckDifference(string oldValue, string newValue)
        {
            if (oldValue == null) oldValue = "";
            if (newValue == null) newValue = "";
            return newValue.Trim() == oldValue.Trim();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (GetDataOnComponentsAndCheck())
            {
                // for add new staff
                if (!_openedByEditStaff)
                {
                    nhanvienNEW.MaNV = Common.GenerateStaffCode(nhanvienNEW.MaPK, nhanvienNEW.MaBMBP);
                    if (nhanvienCTL.LuuNhanVien(nhanvienNEW))
                    {
                        SoYeuLyLichSender(nhanvienNEW.MaNV);
                        _status = Common.STATUS.VIEW;
                        SetStatusComponents();

                        XtraMessageBox.Show("Lưu thành công. Mã nhân viên của bạn là: " + nhanvienNEW.MaNV);
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    return;
                }

                if (CheckChangeNhanVien())
                {
                    int id = nhanvienCTL.LuuPhienBanMoi(nhanvienNEW);
                    if (id != 0)
                    {
                        nhanvienCTL.UpdatePhienBanCu(nhanvienDTO.Id);
                        nhanvienNEW.Id = id;
                        nhanvienDTO = nhanvienNEW;
                        _status = Common.STATUS.VIEW;
                        SetStatusComponents();
                        lylichSender(id.ToString());
                        XtraMessageBox.Show("Lưu thành công");
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    XtraMessageBox.Show("Thông tin nhân viên không thay đổi", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPg1SoCMND_EditValueChanged(object sender, EventArgs e)
        {
            if (txtPg1SoCMND.Text.Length < 2) return;

            var key = txtPg1SoCMND.Text.Substring(0, 2);
            var maTinhThanh =(new TinhThanhCTL()).GetMaTinhThanhBySoCmnd(key);
            cbbPg1NoiCapCMND.EditValue = maTinhThanh;
        }

        private void btnXemCacPhienBan_Click(object sender, EventArgs e)
        {
            if (nhanvienDTO != null)
            {
                if (nhanvienDTO.MaNV != null && nhanvienDTO.MaNV != "")
                {
                    frmNhanVien frm = new frmNhanVien(nhanvienDTO.MaNV);
                    frm.Show();
                }
            }
        }

        public void SetInvisibleButton()
        {
            panelControl2.Visible = false;
        }

        private void chbPg1DangVien_CheckedChanged(object sender, EventArgs e)
        {
            if (_status == Common.STATUS.EDIT)
            {
                if (chbPg1DangVien.Checked)
                {
                    dtPg1NgayVaoDCSVN.Properties.ReadOnly = false;
                    dtPg1NgayChinhThucDCSVN.Properties.ReadOnly = false; ;
                }
                else
                {
                    dtPg1NgayVaoDCSVN.Properties.ReadOnly = true;
                    dtPg1NgayChinhThucDCSVN.Properties.ReadOnly = true;
                }
            }
        }

        private void chbPg1QuanNgu_CheckedChanged(object sender, EventArgs e)
        {
            if (_status == Common.STATUS.EDIT)
            {
                if (chbPg1QuanNgu.Checked)
                {
                    dtPg1NgayNhapNgu.Properties.ReadOnly = false; ;
                    dtPg1NgayXuatNgu.Properties.ReadOnly = false; ;
                    txtPg1QuanHamCaoNhat.Properties.ReadOnly = false; ;
                }
                else
                {
                    dtPg1NgayNhapNgu.Properties.ReadOnly = true;
                    dtPg1NgayXuatNgu.Properties.ReadOnly = true;
                    txtPg1QuanHamCaoNhat.Properties.ReadOnly = true;
                }
            }
        }

        private void chbPg1ThuongBinh_CheckedChanged(object sender, EventArgs e)
        {
            if (_status == Common.STATUS.EDIT)
            {
                if (chbPg1ThuongBinh.Checked)
                {
                    txtPg1ThuongBinhHangFirst.Properties.ReadOnly = false;
                    txtPg1ThuongBinhHangSecond.Properties.ReadOnly = false;
                }
                else
                {
                    txtPg1ThuongBinhHangFirst.Properties.ReadOnly = true;
                    txtPg1ThuongBinhHangSecond.Properties.ReadOnly = true;
                }
            }
        }

        private void cbbPg1MaNgach_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1MaNgach.EditValue == null)
            {
                cbbPg1Bac.Properties.DataSource = null;
            }
            else
            {
                cbbPg1Bac.Properties.DataSource = hslDAC.LayDanhSachHeSOLuongTheoNgach(cbbPg1MaNgach.EditValue.ToString());
            }
        }

        private void mainInfo_EditValueChanged(object sender, EventArgs e)
        {
            if (_openedByEditStaff) return;

            var hoLot = string.Empty;
            var ten = string.Empty;
            var phongKhoa = string.Empty;
            var boMonBoPhan = string.Empty;

        
            if(txtPg1HoDem.EditValue != null)
                hoLot = txtPg1HoDem.EditValue.ToString();
            if(txtPg1Ten.EditValue != null)
                ten = txtPg1Ten.EditValue.ToString();
            if(cbbPg1PhongKhoa.EditValue != null)
                phongKhoa = cbbPg1PhongKhoa.Text;
            if(cbbPg1BMBP.EditValue != null)
                boMonBoPhan = cbbPg1BMBP.Text;

            MainInfoSender(hoLot, ten, phongKhoa, boMonBoPhan);
        }

        public string CheckValue()
        {
            var result = string.Empty;

            if (string.IsNullOrEmpty(txtPg1Ten.EditValue.ToString())
                || string.IsNullOrEmpty(txtPg1DiaChiHT.EditValue.ToString())
                || string.IsNullOrEmpty(txtPg1DiaChiTT.EditValue.ToString())
                || string.IsNullOrEmpty(cbbPg1PhongKhoa.EditValue.ToString())
                || string.IsNullOrEmpty(cbbPg1BMBP.EditValue.ToString()))
            {
                result = "Vui lòng nhập các thông tin bắt buộc (*)";
            }

            return result;
        }

        public NhanVienDTO GetData()
        {
            return nhanvienNEW;
        }

        private void dateTime_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            // set empty to display if value is "01-01-0001" (default value)
            if (string.Compare(e.DisplayText, Constants.EmptyDateTimeType1, StringComparison.Ordinal) == 0 || string.Compare(e.DisplayText, Constants.EmptyDateTimeType2, StringComparison.Ordinal) == 0)
            {
                e.DisplayText = string.Empty;
            }
        }
    }
}
