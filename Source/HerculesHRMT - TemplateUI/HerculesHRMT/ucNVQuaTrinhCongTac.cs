﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucNVQuaTrinhCongTac : XtraUserControl
    {
        Common.STATUS _statusQT;
        Common.STATUS _statusTC;
        public string strMaNV = "";
        QuaTrinhCongTacDTO quatrinhDTO = new QuaTrinhCongTacDTO();
        QuaTrinhCongTacDTO tochucDTO = new QuaTrinhCongTacDTO();
        QuaTrinhCongTacCTL qtctCTL = new QuaTrinhCongTacCTL();

        #region CAU HINH THEM NHAN VIEN
        // false: opened by add new staff
        // true : opened by edit staff
        //private Boolean _openedByEditStaff = false;

        // private string LUU_TAM = "Lưu Tạm";

        //public delegate void SendQuaTrinhCongTac(QuaTrinhCongTacDTO quaTrinhCongTacDto);
        //public SendQuaTrinhCongTac QuaTrinhCongTacSender;

        #endregion

        public ucNVQuaTrinhCongTac()
        {
            InitializeComponent();
        }
        public ucNVQuaTrinhCongTac(Boolean openedByEditStaff = true)
        {
            InitializeComponent();

            //if (openedByEditStaff) return;

            //_openedByEditStaff = openedByEditStaff;
            //btnPg2ThemTG.Text = LUU_TAM;
            //btnXemCacPhienBan.Visible = false;
            //btnHieuChinh.Visible = false;
        }

        public void SetMaNV (string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _statusQT = status;
            _statusTC = status;
            tochucDTO = new QuaTrinhCongTacDTO();
            quatrinhDTO = new QuaTrinhCongTacDTO();
            BindingQuaTrinhData(quatrinhDTO);
            BindingToChucData(tochucDTO);
            SetStatusQuaTrinhComponents();
            SetStatusToChucComponents();
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            List<QuaTrinhCongTacDTO> listQuaTrinhCongTac = qtctCTL.LayDanhSachQuaTrinhCongTacNhanVien(strMaNV);
            List<QuaTrinhCongTacDTO> listToChucXaHoi = qtctCTL.LayDanhSachToChucXaHoiNhanVien(strMaNV);
            gvPg2QuaTrinhCongTac.DataSource = null;
            gvPg2QuaTrinhCongTac.DataSource = listQuaTrinhCongTac;
            gvPg2QuaTrinhThamGia.DataSource = null;
            gvPg2QuaTrinhThamGia.DataSource = listToChucXaHoi;
        }
        private void SetStatusQuaTrinhComponents()
        {
            switch (_statusQT)
            {
                case Common.STATUS.NEW:
                    BindingQuaTrinhData(new QuaTrinhCongTacDTO());
                    btnEditQT.Enabled = false;
                    btnPg2ThemGD.Enabled = true;
                    setQuaTrinhReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    setQuaTrinhReadOnly(true);
                    if (quatrinhDTO.Id > 0)
                    {
                        btnEditQT.Enabled = true;
                    }
                    else
                    {
                        btnEditQT.Enabled = false;
                    }
                    btnPg2ThemGD.Enabled = false;
                    btnPg2TaoMoiGD.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    setQuaTrinhReadOnly(false);
                    btnEditQT.Enabled = false;
                    btnPg2ThemGD.Enabled = true;
                    btnPg2TaoMoiGD.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void SetStatusToChucComponents()
        {
            switch (_statusTC)
            {
                case Common.STATUS.NEW:
                    BindingToChucData(new QuaTrinhCongTacDTO());
                    btnEditTC.Enabled = false;
                    btnPg2ThemTG.Enabled = true;
                    setToChucReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    setToChucReadOnly(true);
                    if (tochucDTO.Id > 0)
                    {
                        btnEditTC.Enabled = true;
                    }
                    else
                    {
                        btnEditTC.Enabled = false;
                    }
                    btnPg2ThemTG.Enabled = false;
                    btnPg2TaoMoiTG.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    setToChucReadOnly(false);
                    btnEditTC.Enabled = false;
                    btnPg2ThemTG.Enabled = true;
                    btnPg2TaoMoiTG.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void setQuaTrinhReadOnly(bool read)
        {
            rdbPg2GiaiDoan.Properties.ReadOnly = read;
            dtPg2GDTu.Properties.ReadOnly = read;
            dtPg2GDDen.Properties.ReadOnly = read;
            txtPg2GDCongTac.Properties.ReadOnly = read;
            txtPg2GDCongViec.Properties.ReadOnly = read;
            txtPg2GDThanhTich.Properties.ReadOnly = read;
            txtPg2GDGhiChu.Properties.ReadOnly = read;
        }
        private void setToChucReadOnly(bool read)
        {
            txtPg2TGChinhTriXaHoi.Properties.ReadOnly = read;
            txtPg2TGCongViec.Properties.ReadOnly = read;
            txtPg2TGGhiChu.Properties.ReadOnly = read;
            txtPg2TGThanhTich.Properties.ReadOnly = read;
            dtPg2TGDen.Properties.ReadOnly = read;
            dtPg2TGTu.Properties.ReadOnly = read;
        }
        private void BindingQuaTrinhData(QuaTrinhCongTacDTO qtctDTO)
        {
            if (qtctDTO == null) qtctDTO = new QuaTrinhCongTacDTO();
            rdbPg2GiaiDoan.EditValue = qtctDTO.GiaiDoan;
            dtPg2GDTu.EditValue = qtctDTO.TuNgay;
            dtPg2GDDen.EditValue = qtctDTO.DenNgay;
            txtPg2GDCongTac.EditValue = qtctDTO.NoiCongTac;
            txtPg2GDCongViec.EditValue = qtctDTO.CongViec;
            txtPg2GDGhiChu.EditValue = qtctDTO.GhiChu;
            txtPg2GDThanhTich.EditValue = qtctDTO.ThanhTich;
        }

        private void BindingToChucData(QuaTrinhCongTacDTO tcDTO)
        {
            if (tcDTO == null) tcDTO = new QuaTrinhCongTacDTO();
            dtPg2TGDen.EditValue = tcDTO.DenNgay;
            dtPg2TGTu.EditValue = tcDTO.TuNgay;
            txtPg2TGChinhTriXaHoi.EditValue = tcDTO.NoiCongTac;
            txtPg2TGCongViec.EditValue = tcDTO.CongViec;
            txtPg2TGGhiChu.EditValue = tcDTO.GhiChu;
            txtPg2TGThanhTich.EditValue = tcDTO.ThanhTich;
        }

        private void btnPg2TaoMoiGD_Click(object sender, System.EventArgs e)
        {
            quatrinhDTO = new QuaTrinhCongTacDTO();
            _statusQT = Common.STATUS.NEW;
            SetStatusQuaTrinhComponents();
            dtPg2GDTu.EditValue = dtPg2GDDen.EditValue = Constants.EmptyDateTimeType1;
        }

        private void btnPg2TaoMoiTG_Click(object sender, System.EventArgs e)
        {
            tochucDTO = new QuaTrinhCongTacDTO();
            _statusTC = Common.STATUS.NEW;
            SetStatusToChucComponents();
            dtPg2TGTu.EditValue = dtPg2TGDen.EditValue = Constants.EmptyDateTimeType1;
        }
        private bool GetDataOnQuaTrinhComponentsAndCheck()
        {
            int error = 0;
            quatrinhDTO.MaNV = strMaNV;
            quatrinhDTO.GiaiDoan = Convert.ToInt32(rdbPg2GiaiDoan.EditValue);
            if (txtPg2GDCongTac.Text.Trim() == "") error = 1;
            quatrinhDTO.NoiCongTac = txtPg2GDCongTac.EditValue as String;
            if (txtPg2GDCongViec.Text.Trim() == "") error = 1;
            quatrinhDTO.CongViec = txtPg2GDCongViec.EditValue as String;
            if (dtPg2GDTu.EditValue != null)
            {
                DateTime TuNgay = DateTime.Parse(dtPg2GDTu.EditValue.ToString());
                quatrinhDTO.TuNgay = TuNgay.ToString("yyyy-MM-dd");
            }
            else
            {
                error = 1;
            }
            if (dtPg2GDDen.Text != "")
            {
                DateTime DenNgay = DateTime.Parse(dtPg2GDDen.EditValue.ToString());
                quatrinhDTO.DenNgay = DenNgay.ToString("yyyy-MM-dd");
            }
            else
            {
                error = 1;
            }
            quatrinhDTO.ThanhTich = txtPg2GDThanhTich.EditValue as String;
            quatrinhDTO.GhiChu = txtPg2GDGhiChu.EditValue as String;
            return error == 0;
        }

        private bool GetDataOnToChucComponentsAndCheck()
        {
            int error = 0;
            tochucDTO.MaNV = strMaNV;
            if (txtPg2TGChinhTriXaHoi.Text.Trim() == "") error = 1;
            tochucDTO.NoiCongTac = txtPg2TGChinhTriXaHoi.EditValue as String;
            if (txtPg2TGCongViec.Text.Trim() == "") error = 1;
            tochucDTO.CongViec = txtPg2TGCongViec.EditValue as String;
            if (dtPg2TGTu.Text != "")
            {
                DateTime TuNgay = DateTime.Parse(dtPg2TGTu.EditValue.ToString());
                tochucDTO.TuNgay = TuNgay.ToString("yyyy-MM-dd");
            }
            else
            {
                error = 1;
            }
            if (dtPg2TGDen.Text != "")
            {
                DateTime DenNgay = DateTime.Parse(dtPg2TGDen.EditValue.ToString());
                tochucDTO.DenNgay = DenNgay.ToString("yyyy-MM-dd");
            }
            else
            {
                error = 1;
            }
            tochucDTO.GiaiDoan = 2;
            tochucDTO.ThanhTich = txtPg2TGThanhTich.EditValue as String;
            tochucDTO.GhiChu = txtPg2TGGhiChu.EditValue as String;
            return error == 0;
        }
        private void btnPg2ThemGD_Click(object sender, System.EventArgs e)
        {
            if (GetDataOnQuaTrinhComponentsAndCheck())
            {
                // for add new staff
                //if (_openedByEditStaff)
                //{
                    //List<QuaTrinhCongTacDTO> listQuaTrinhCongTac = qtctCTL.LayDanhSachQuaTrinhCongTacNhanVien(strMaNV);
                    //List<QuaTrinhCongTacDTO> listToChucXaHoi = qtctCTL.LayDanhSachToChucXaHoiNhanVien(strMaNV);

                    //QuaTrinhCongTacSender(quatrinhDTO);
                    //return;
                //}

                if (_statusQT == Common.STATUS.NEW)
                {
                    if (qtctCTL.LuuQuaTrinhCongTac(quatrinhDTO))
                    {
                        XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _statusQT = Common.STATUS.VIEW;
                        LoadDanhSach();
                        SetStatusQuaTrinhComponents();
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (_statusQT == Common.STATUS.EDIT)
                {
                    if (qtctCTL.CapNhatQuaTrinhCongTac(quatrinhDTO))
                    {
                        XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _statusQT = Common.STATUS.VIEW;
                        LoadDanhSach();
                        SetStatusQuaTrinhComponents();
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg2ThemTG_Click(object sender, System.EventArgs e)
        {
            if (GetDataOnToChucComponentsAndCheck())
            {
                if (_statusTC == Common.STATUS.NEW)
                {
                    if (qtctCTL.LuuQuaTrinhCongTac(tochucDTO))
                    {
                        XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _statusTC = Common.STATUS.VIEW;
                        LoadDanhSach();
                        SetStatusToChucComponents();
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (_statusTC == Common.STATUS.EDIT)
                {
                    if (qtctCTL.CapNhatQuaTrinhCongTac(tochucDTO))
                    {
                        XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _statusTC = Common.STATUS.VIEW;
                        LoadDanhSach();
                        SetStatusToChucComponents();
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridviewQuaTrinhCongTac_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "GiaiDoan")
            {
                if (Convert.ToDecimal(e.Value) == 0) e.DisplayText = "Trước khi được tuyển dụng";
                else e.DisplayText = "Khi được tuyển dụng";
            }
        }

        private void gridviewQuaTrinhCongTac_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                quatrinhDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as QuaTrinhCongTacDTO;
                if (quatrinhDTO == null) quatrinhDTO = new QuaTrinhCongTacDTO();
                BindingQuaTrinhData(quatrinhDTO);
                _statusQT = Common.STATUS.VIEW;
                SetStatusQuaTrinhComponents();
            }
        }

        private void gridviewQuaTrinhThamGia_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                tochucDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as QuaTrinhCongTacDTO;
                if (tochucDTO == null) tochucDTO = new QuaTrinhCongTacDTO();
                BindingToChucData(tochucDTO);
                _statusTC = Common.STATUS.VIEW;
                SetStatusToChucComponents();
            }
        }

        private void btnEditQT_Click(object sender, EventArgs e)
        {
            if (quatrinhDTO.Id  > 0)
            {
                _statusQT = Common.STATUS.EDIT;
                SetStatusQuaTrinhComponents();
            }
        }

        private void btnEditTC_Click(object sender, EventArgs e)
        {
            if (tochucDTO.Id >0)
            {
                _statusTC = Common.STATUS.EDIT;
                SetStatusToChucComponents();
            }
        }

        private void btnPg2XoaGD_Click(object sender, EventArgs e)
        {
            if (quatrinhDTO.Id >0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (qtctCTL.XoaQuaTrinhCongTac(quatrinhDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnPg2XoaTG_Click(object sender, EventArgs e)
        {
            if (tochucDTO.Id >0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (qtctCTL.XoaQuaTrinhCongTac(tochucDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void gridviewQuaTrinhCongTac_RowClick(object sender, RowClickEventArgs e)
        {
            quatrinhDTO = ((GridView)sender).GetRow(e.RowHandle) as QuaTrinhCongTacDTO;
            BindingQuaTrinhData(quatrinhDTO);
            _statusQT = Common.STATUS.VIEW;
            SetStatusQuaTrinhComponents();
        }

        private void gridviewQuaTrinhThamGia_RowClick(object sender, RowClickEventArgs e)
        {
            tochucDTO = ((GridView)sender).GetRow(e.RowHandle) as QuaTrinhCongTacDTO;
            BindingToChucData(tochucDTO);
            _statusTC = Common.STATUS.VIEW;
            SetStatusToChucComponents();
        }

        public QuaTrinhCongTacDTO GetData()
        {
            return quatrinhDTO;
        }

        private void dateTime_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            // set empty to display if value is "01-01-0001" (default value)
            if (string.Compare(e.DisplayText, Constants.EmptyDateTimeType1, StringComparison.Ordinal) == 0 || string.Compare(e.DisplayText, Constants.EmptyDateTimeType2, StringComparison.Ordinal) == 0)
            {
                e.DisplayText = string.Empty;
            }
        }
    }
}
