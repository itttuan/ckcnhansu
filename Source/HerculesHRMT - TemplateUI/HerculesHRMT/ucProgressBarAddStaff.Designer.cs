﻿using System.ComponentModel;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    partial class ucProgressBarAddStaff
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ckbStep1 = new DevExpress.XtraEditors.CheckEdit();
            this.lblStep1 = new DevExpress.XtraEditors.LabelControl();
            this.ckbStep2 = new DevExpress.XtraEditors.CheckEdit();
            this.ckbStep3 = new DevExpress.XtraEditors.CheckEdit();
            this.ckbStep5 = new DevExpress.XtraEditors.CheckEdit();
            this.ckbStep4 = new DevExpress.XtraEditors.CheckEdit();
            this.ckbStep6 = new DevExpress.XtraEditors.CheckEdit();
            this.lblStep2 = new DevExpress.XtraEditors.LabelControl();
            this.lblStep3 = new DevExpress.XtraEditors.LabelControl();
            this.lblStep4 = new DevExpress.XtraEditors.LabelControl();
            this.lblStep5 = new DevExpress.XtraEditors.LabelControl();
            this.lblStep6 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ckbStep1
            // 
            this.ckbStep1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep1.EditValue = 0;
            this.ckbStep1.Location = new System.Drawing.Point(44, 3);
            this.ckbStep1.Name = "ckbStep1";
            this.ckbStep1.Properties.Caption = "";
            this.ckbStep1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep1.Properties.ImageIndexChecked = 0;
            this.ckbStep1.Properties.ImageIndexGrayed = 0;
            this.ckbStep1.Properties.ImageIndexUnchecked = 0;
            this.ckbStep1.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep1.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep1.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep1.Properties.ReadOnly = true;
            this.ckbStep1.Properties.ValueChecked = 1;
            this.ckbStep1.Properties.ValueGrayed = -1;
            this.ckbStep1.Properties.ValueUnchecked = 0;
            this.ckbStep1.Size = new System.Drawing.Size(35, 36);
            this.ckbStep1.TabIndex = 0;
            // 
            // lblStep1
            // 
            this.lblStep1.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep1.Location = new System.Drawing.Point(34, 45);
            this.lblStep1.Name = "lblStep1";
            this.lblStep1.Size = new System.Drawing.Size(57, 30);
            this.lblStep1.TabIndex = 1;
            this.lblStep1.Text = "SƠ LƯỢC\r\n LÝ LỊCH";
            // 
            // ckbStep2
            // 
            this.ckbStep2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep2.EditValue = 0;
            this.ckbStep2.Location = new System.Drawing.Point(172, 3);
            this.ckbStep2.Name = "ckbStep2";
            this.ckbStep2.Properties.Caption = "";
            this.ckbStep2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep2.Properties.ImageIndexChecked = 0;
            this.ckbStep2.Properties.ImageIndexGrayed = 0;
            this.ckbStep2.Properties.ImageIndexUnchecked = 0;
            this.ckbStep2.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep2.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep2.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep2.Properties.ReadOnly = true;
            this.ckbStep2.Properties.ValueChecked = 1;
            this.ckbStep2.Properties.ValueGrayed = "-1";
            this.ckbStep2.Properties.ValueUnchecked = 0;
            this.ckbStep2.Size = new System.Drawing.Size(35, 36);
            this.ckbStep2.TabIndex = 0;
            // 
            // ckbStep3
            // 
            this.ckbStep3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep3.EditValue = 0;
            this.ckbStep3.Location = new System.Drawing.Point(295, 3);
            this.ckbStep3.Name = "ckbStep3";
            this.ckbStep3.Properties.Caption = "";
            this.ckbStep3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep3.Properties.ImageIndexChecked = 0;
            this.ckbStep3.Properties.ImageIndexGrayed = 0;
            this.ckbStep3.Properties.ImageIndexUnchecked = 0;
            this.ckbStep3.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep3.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep3.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep3.Properties.ReadOnly = true;
            this.ckbStep3.Properties.ValueChecked = 1;
            this.ckbStep3.Properties.ValueGrayed = -1;
            this.ckbStep3.Properties.ValueUnchecked = 0;
            this.ckbStep3.Size = new System.Drawing.Size(35, 36);
            this.ckbStep3.TabIndex = 0;
            // 
            // ckbStep5
            // 
            this.ckbStep5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep5.EditValue = 0;
            this.ckbStep5.Location = new System.Drawing.Point(546, 3);
            this.ckbStep5.Name = "ckbStep5";
            this.ckbStep5.Properties.Caption = "";
            this.ckbStep5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep5.Properties.ImageIndexChecked = 0;
            this.ckbStep5.Properties.ImageIndexGrayed = 0;
            this.ckbStep5.Properties.ImageIndexUnchecked = 0;
            this.ckbStep5.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep5.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep5.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep5.Properties.ReadOnly = true;
            this.ckbStep5.Properties.ValueChecked = 1;
            this.ckbStep5.Properties.ValueGrayed = -1;
            this.ckbStep5.Properties.ValueUnchecked = 0;
            this.ckbStep5.Size = new System.Drawing.Size(35, 36);
            this.ckbStep5.TabIndex = 0;
            // 
            // ckbStep4
            // 
            this.ckbStep4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep4.EditValue = 0;
            this.ckbStep4.Location = new System.Drawing.Point(423, 3);
            this.ckbStep4.Name = "ckbStep4";
            this.ckbStep4.Properties.Caption = "";
            this.ckbStep4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep4.Properties.ImageIndexChecked = 0;
            this.ckbStep4.Properties.ImageIndexGrayed = 0;
            this.ckbStep4.Properties.ImageIndexUnchecked = 0;
            this.ckbStep4.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep4.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep4.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep4.Properties.ReadOnly = true;
            this.ckbStep4.Properties.ValueChecked = 1;
            this.ckbStep4.Properties.ValueGrayed = -1;
            this.ckbStep4.Properties.ValueUnchecked = 0;
            this.ckbStep4.Size = new System.Drawing.Size(35, 36);
            this.ckbStep4.TabIndex = 0;
            // 
            // ckbStep6
            // 
            this.ckbStep6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ckbStep6.EditValue = 0;
            this.ckbStep6.Location = new System.Drawing.Point(674, 3);
            this.ckbStep6.Name = "ckbStep6";
            this.ckbStep6.Properties.Caption = "";
            this.ckbStep6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.ckbStep6.Properties.ImageIndexChecked = 0;
            this.ckbStep6.Properties.ImageIndexGrayed = 0;
            this.ckbStep6.Properties.ImageIndexUnchecked = 0;
            this.ckbStep6.Properties.PictureChecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_check;
            this.ckbStep6.Properties.PictureGrayed = global::HerculesHRMT.Properties.Resources.hrmt_shield_warning;
            this.ckbStep6.Properties.PictureUnchecked = global::HerculesHRMT.Properties.Resources.hrmt_sign_info;
            this.ckbStep6.Properties.ReadOnly = true;
            this.ckbStep6.Properties.ValueChecked = 1;
            this.ckbStep6.Properties.ValueGrayed = -1;
            this.ckbStep6.Properties.ValueUnchecked = 0;
            this.ckbStep6.Size = new System.Drawing.Size(35, 36);
            this.ckbStep6.TabIndex = 0;
            // 
            // lblStep2
            // 
            this.lblStep2.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep2.Location = new System.Drawing.Point(160, 45);
            this.lblStep2.Name = "lblStep2";
            this.lblStep2.Size = new System.Drawing.Size(59, 30);
            this.lblStep2.TabIndex = 1;
            this.lblStep2.Text = "  LỊCH SỬ\r\nBẢN THÂN";
            // 
            // lblStep3
            // 
            this.lblStep3.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep3.Location = new System.Drawing.Point(241, 45);
            this.lblStep3.Name = "lblStep3";
            this.lblStep3.Size = new System.Drawing.Size(149, 30);
            this.lblStep3.TabIndex = 1;
            this.lblStep3.Text = "     TRÌNH ĐỘ ĐÀO TẠO\r\nCHUYÊN MÔN NGHIỆP VỤ";
            // 
            // lblStep4
            // 
            this.lblStep4.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep4.Location = new System.Drawing.Point(396, 45);
            this.lblStep4.Name = "lblStep4";
            this.lblStep4.Size = new System.Drawing.Size(91, 30);
            this.lblStep4.TabIndex = 1;
            this.lblStep4.Text = "KHEN THƯỞNG\r\n      KỶ LUẬT";
            // 
            // lblStep5
            // 
            this.lblStep5.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep5.Location = new System.Drawing.Point(503, 45);
            this.lblStep5.Name = "lblStep5";
            this.lblStep5.Size = new System.Drawing.Size(121, 30);
            this.lblStep5.TabIndex = 1;
            this.lblStep5.Text = "          QUAN HỆ\r\nGIA ĐÌNH - THÂN TỘC";
            // 
            // lblStep6
            // 
            this.lblStep6.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.lblStep6.Location = new System.Drawing.Point(653, 45);
            this.lblStep6.Name = "lblStep6";
            this.lblStep6.Size = new System.Drawing.Size(79, 15);
            this.lblStep6.TabIndex = 1;
            this.lblStep6.Text = "TỰ NHẬN XÉT";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::HerculesHRMT.Properties.Resources.hrmt_arrow_right_64;
            this.pictureEdit1.Location = new System.Drawing.Point(85, -10);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(76, 70);
            this.pictureEdit1.TabIndex = 2;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = global::HerculesHRMT.Properties.Resources.hrmt_arrow_right_64;
            this.pictureEdit2.Location = new System.Drawing.Point(213, -10);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Size = new System.Drawing.Size(76, 70);
            this.pictureEdit2.TabIndex = 2;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = global::HerculesHRMT.Properties.Resources.hrmt_arrow_right_64;
            this.pictureEdit3.Location = new System.Drawing.Point(341, -10);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Size = new System.Drawing.Size(76, 70);
            this.pictureEdit3.TabIndex = 2;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = global::HerculesHRMT.Properties.Resources.hrmt_arrow_right_64;
            this.pictureEdit4.Location = new System.Drawing.Point(464, -10);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Size = new System.Drawing.Size(76, 70);
            this.pictureEdit4.TabIndex = 2;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.EditValue = global::HerculesHRMT.Properties.Resources.hrmt_arrow_right_64;
            this.pictureEdit5.Location = new System.Drawing.Point(592, -10);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Size = new System.Drawing.Size(76, 70);
            this.pictureEdit5.TabIndex = 2;
            // 
            // ucProgressBarAddStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblStep6);
            this.Controls.Add(this.lblStep5);
            this.Controls.Add(this.lblStep4);
            this.Controls.Add(this.lblStep3);
            this.Controls.Add(this.lblStep2);
            this.Controls.Add(this.lblStep1);
            this.Controls.Add(this.pictureEdit5);
            this.Controls.Add(this.pictureEdit4);
            this.Controls.Add(this.pictureEdit3);
            this.Controls.Add(this.pictureEdit2);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.ckbStep4);
            this.Controls.Add(this.ckbStep6);
            this.Controls.Add(this.ckbStep1);
            this.Controls.Add(this.ckbStep2);
            this.Controls.Add(this.ckbStep5);
            this.Controls.Add(this.ckbStep3);
            this.Name = "ucProgressBarAddStaff";
            this.Size = new System.Drawing.Size(756, 81);
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckbStep6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CheckEdit ckbStep1;
        private LabelControl lblStep1;
        private CheckEdit ckbStep2;
        private CheckEdit ckbStep3;
        private CheckEdit ckbStep5;
        private CheckEdit ckbStep4;
        private CheckEdit ckbStep6;
        private LabelControl lblStep2;
        private LabelControl lblStep3;
        private LabelControl lblStep4;
        private LabelControl lblStep5;
        private LabelControl lblStep6;
        private PictureEdit pictureEdit1;
        private PictureEdit pictureEdit2;
        private PictureEdit pictureEdit3;
        private PictureEdit pictureEdit4;
        private PictureEdit pictureEdit5;
    }
}
