﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucTonGiao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTonGiao = new DevExpress.XtraGrid.GridControl();
            this.gridViewTongiao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaTonGiao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenTonGiao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtTonGiao = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTonGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTongiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(475, 69);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(269, 62);
            this.meGhiChu.TabIndex = 2;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // gcTonGiao
            // 
            this.gcTonGiao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTonGiao.Location = new System.Drawing.Point(2, 2);
            this.gcTonGiao.MainView = this.gridViewTongiao;
            this.gcTonGiao.Name = "gcTonGiao";
            this.gcTonGiao.Size = new System.Drawing.Size(781, 308);
            this.gcTonGiao.TabIndex = 3;
            this.gcTonGiao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTongiao});
            // 
            // gridViewTongiao
            // 
            this.gridViewTongiao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaTonGiao,
            this.gridColTenTonGiao,
            this.gridColTenVT,
            this.gridColGhiChu});
            this.gridViewTongiao.GridControl = this.gcTonGiao;
            this.gridViewTongiao.GroupPanelText = " ";
            this.gridViewTongiao.Name = "gridViewTongiao";
            this.gridViewTongiao.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTongiao_FocusedRowChanged);
            // 
            // gridColMaTonGiao
            // 
            this.gridColMaTonGiao.Caption = "Mã Tôn Giáo";
            this.gridColMaTonGiao.FieldName = "MaTonGiao";
            this.gridColMaTonGiao.Name = "gridColMaTonGiao";
            // 
            // gridColTenTonGiao
            // 
            this.gridColTenTonGiao.Caption = "Tôn Giáo";
            this.gridColTenTonGiao.FieldName = "TenTonGiao";
            this.gridColTenTonGiao.Name = "gridColTenTonGiao";
            this.gridColTenTonGiao.OptionsColumn.AllowEdit = false;
            this.gridColTenTonGiao.OptionsColumn.ReadOnly = true;
            this.gridColTenTonGiao.Visible = true;
            this.gridColTenTonGiao.VisibleIndex = 0;
            // 
            // gridColTenVT
            // 
            this.gridColTenVT.Caption = "Tên Viết Tắt";
            this.gridColTenVT.FieldName = "TenVietTat";
            this.gridColTenVT.Name = "gridColTenVT";
            this.gridColTenVT.Visible = true;
            this.gridColTenVT.VisibleIndex = 1;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(421, 72);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(688, 162);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(595, 162);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 6;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(502, 162);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(409, 162);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 3;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcTonGiao);
            this.panelControl3.Location = new System.Drawing.Point(0, 231);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 312);
            this.panelControl3.TabIndex = 4;
            // 
            // txtTonGiao
            // 
            this.txtTonGiao.Location = new System.Drawing.Point(101, 69);
            this.txtTonGiao.Name = "txtTonGiao";
            this.txtTonGiao.Size = new System.Drawing.Size(269, 20);
            this.txtTonGiao.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 7;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.txtTonGiao);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 192);
            this.panelControl1.TabIndex = 5;
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(101, 111);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(269, 20);
            this.txtTenVT.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(35, 114);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 13);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "Tên viết tắt:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(35, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tôn giáo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(330, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tôn Giáo";
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // ucTonGiao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucTonGiao";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucTonGiao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTonGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTongiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTonGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MemoEdit meGhiChu;
        private GridColumn gridColTenDayDu;
        private GridControl gcTonGiao;
        private GridView gridViewTongiao;
        private GridColumn gridColMaTonGiao;
        private GridColumn gridColTenTonGiao;
        private GridColumn gridColGhiChu;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private PanelControl panelControl3;
        private TextEdit txtTonGiao;
        private PanelControl panelControl2;
        private PanelControl panelControl1;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenVietTat;
        private TextEdit txtTenVT;
        private LabelControl labelControl4;
        private GridColumn gridColTenVT;
    }
}
