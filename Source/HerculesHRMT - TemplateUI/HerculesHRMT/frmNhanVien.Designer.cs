﻿using System.ComponentModel;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    partial class frmNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnBack = new DevExpress.XtraEditors.PanelControl();
            this.btnBack = new DevExpress.XtraEditors.SimpleButton();
            this.pnNext = new DevExpress.XtraEditors.PanelControl();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.pnUC = new DevExpress.XtraEditors.PanelControl();
            this.lbPageNum = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnBack)).BeginInit();
            this.pnBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnNext)).BeginInit();
            this.pnNext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnUC)).BeginInit();
            this.SuspendLayout();
            // 
            // pnBack
            // 
            this.pnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnBack.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBack.Controls.Add(this.btnBack);
            this.pnBack.Location = new System.Drawing.Point(2, 2);
            this.pnBack.Name = "pnBack";
            this.pnBack.Size = new System.Drawing.Size(38, 487);
            this.pnBack.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btnBack.Appearance.Options.UseForeColor = true;
            this.btnBack.Location = new System.Drawing.Point(5, 220);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(28, 47);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "<<<";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // pnNext
            // 
            this.pnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnNext.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnNext.Controls.Add(this.btnNext);
            this.pnNext.Location = new System.Drawing.Point(1224, 2);
            this.pnNext.Name = "pnNext";
            this.pnNext.Size = new System.Drawing.Size(38, 487);
            this.pnNext.TabIndex = 1;
            // 
            // btnNext
            // 
            this.btnNext.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btnNext.Appearance.Options.UseForeColor = true;
            this.btnNext.Location = new System.Drawing.Point(5, 220);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(28, 47);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = ">>>";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pnUC
            // 
            this.pnUC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnUC.Location = new System.Drawing.Point(42, 2);
            this.pnUC.Name = "pnUC";
            this.pnUC.Size = new System.Drawing.Size(1181, 455);
            this.pnUC.TabIndex = 2;
            // 
            // lbPageNum
            // 
            this.lbPageNum.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPageNum.Location = new System.Drawing.Point(642, 466);
            this.lbPageNum.Name = "lbPageNum";
            this.lbPageNum.Size = new System.Drawing.Size(0, 15);
            this.lbPageNum.TabIndex = 3;
            // 
            // frmNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 492);
            this.Controls.Add(this.lbPageNum);
            this.Controls.Add(this.pnUC);
            this.Controls.Add(this.pnNext);
            this.Controls.Add(this.pnBack);
            this.Name = "frmNhanVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Những Lần Đã Cập Nhật";
            ((System.ComponentModel.ISupportInitialize)(this.pnBack)).EndInit();
            this.pnBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnNext)).EndInit();
            this.pnNext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnUC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PanelControl pnBack;
        private PanelControl pnNext;
        private PanelControl pnUC;
        private SimpleButton btnBack;
        private SimpleButton btnNext;
        private LabelControl lbPageNum;

    }
}