﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace HerculesHRMT
{
    public partial class ucHinh : DevExpress.XtraEditors.XtraUserControl
    {
        public ucHinh()
        {
            InitializeComponent();
        }

        public ucHinh(byte[] imgBytes)
        {
            InitializeComponent();
            if (imgBytes != null)
            {
                MemoryStream stream = new MemoryStream(imgBytes);
                this.imgHinhAnh.Image = Image.FromStream(stream);
            }
        }
        public void setImg(byte[] imgBytes)
        {
            if (imgBytes != null)
            {
                MemoryStream stream = new MemoryStream(imgBytes);
                this.imgHinhAnh.Image = Image.FromStream(stream);
            }
        }
    }
}
