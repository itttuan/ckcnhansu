﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucHocVanBangCap
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColMaBangcap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewBCCC = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTenBangCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcBCCC = new DevExpress.XtraGrid.GridControl();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtVBCC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVBCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridColMaBangcap
            // 
            this.gridColMaBangcap.Caption = "Mã Bằng Cấp";
            this.gridColMaBangcap.FieldName = "MaLoaiBCCC";
            this.gridColMaBangcap.Name = "gridColMaBangcap";
            // 
            // gridViewBCCC
            // 
            this.gridViewBCCC.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaBangcap,
            this.gridColTenBangCap,
            this.gridColGhiChu});
            this.gridViewBCCC.GridControl = this.gcBCCC;
            this.gridViewBCCC.GroupPanelText = " ";
            this.gridViewBCCC.Name = "gridViewBCCC";
            this.gridViewBCCC.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBCCC_FocusedRowChanged);
            // 
            // gridColTenBangCap
            // 
            this.gridColTenBangCap.Caption = "Văn bằng/Chứng chỉ";
            this.gridColTenBangCap.FieldName = "TenLoai";
            this.gridColTenBangCap.Name = "gridColTenBangCap";
            this.gridColTenBangCap.OptionsColumn.AllowEdit = false;
            this.gridColTenBangCap.OptionsColumn.ReadOnly = true;
            this.gridColTenBangCap.Visible = true;
            this.gridColTenBangCap.VisibleIndex = 0;
            // 
            // gcBCCC
            // 
            this.gcBCCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBCCC.Location = new System.Drawing.Point(2, 2);
            this.gcBCCC.MainView = this.gridViewBCCC;
            this.gcBCCC.Name = "gcBCCC";
            this.gcBCCC.Size = new System.Drawing.Size(781, 327);
            this.gcBCCC.TabIndex = 3;
            this.gcBCCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBCCC});
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(664, 151);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(571, 151);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 4;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(478, 151);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 3;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcBCCC);
            this.panelControl3.Location = new System.Drawing.Point(0, 182);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 331);
            this.panelControl3.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 4;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtVBCC);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 181);
            this.panelControl1.TabIndex = 5;
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(385, 151);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 2;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtVBCC
            // 
            this.txtVBCC.Location = new System.Drawing.Point(175, 69);
            this.txtVBCC.Name = "txtVBCC";
            this.txtVBCC.Size = new System.Drawing.Size(195, 20);
            this.txtVBCC.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(101, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Văn bằng/Chứng chỉ:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(243, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(295, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Văn Bằng - Chứng Chỉ";
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(174, 102);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(577, 36);
            this.meGhiChu.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 1;
            // 
            // ucHocVanBangCap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucHocVanBangCap";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucHocVanBangCap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVBCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridColumn gridColMaBangcap;
        private GridView gridViewBCCC;
        private GridColumn gridColTenBangCap;
        private GridControl gcBCCC;
        private GridColumn gridColTenDayDu;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private PanelControl panelControl3;
        private PanelControl panelControl2;
        private PanelControl panelControl1;
        private SimpleButton btnThem;
        private TextEdit txtVBCC;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenVietTat;
        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
        private GridColumn gridColGhiChu;
    }
}
