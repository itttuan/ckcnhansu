﻿using System.ComponentModel;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    partial class frmDanhSachUngVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcDanToc = new DevExpress.XtraGrid.GridControl();
            this.gridViewDantoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColHoTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNoiSinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyenNganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHinhThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNamTotNghiep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtPg1NoiSinh = new DevExpress.XtraEditors.TextEdit();
            this.dtPg1NgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.rdbPg1GioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1HoDem = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.txtCMNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.cbbCMHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.txtCMChuyenNganh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.cbbCMTrinhDo = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbCMTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.btnCMSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcDanToc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDantoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1NoiSinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMChuyenNganh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTruong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcDanToc
            // 
            this.gcDanToc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDanToc.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.gcDanToc.Location = new System.Drawing.Point(0, 323);
            this.gcDanToc.MainView = this.gridViewDantoc;
            this.gcDanToc.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.gcDanToc.Name = "gcDanToc";
            this.gcDanToc.Size = new System.Drawing.Size(2739, 774);
            this.gcDanToc.TabIndex = 1;
            this.gcDanToc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDantoc});
            // 
            // gridViewDantoc
            // 
            this.gridViewDantoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColHoTen,
            this.gridColGioiTinh,
            this.gridColNgaySinh,
            this.gridColNoiSinh,
            this.gridColDiaChi,
            this.gridColPhongKhoa,
            this.gridColBoMon,
            this.gridColTrinhDo,
            this.gridColChuyenNganh,
            this.gridColTruong,
            this.gridColHinhThuc,
            this.gridColNamTotNghiep});
            this.gridViewDantoc.GridControl = this.gcDanToc;
            this.gridViewDantoc.GroupPanelText = " ";
            this.gridViewDantoc.Name = "gridViewDantoc";
            // 
            // gridColHoTen
            // 
            this.gridColHoTen.Caption = "Họ Tên";
            this.gridColHoTen.FieldName = "HoTen";
            this.gridColHoTen.Name = "gridColHoTen";
            this.gridColHoTen.OptionsColumn.AllowEdit = false;
            this.gridColHoTen.OptionsColumn.ReadOnly = true;
            this.gridColHoTen.Visible = true;
            this.gridColHoTen.VisibleIndex = 0;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Giới Tính";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 1;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày Sinh";
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 2;
            // 
            // gridColNoiSinh
            // 
            this.gridColNoiSinh.Caption = "Nơi Sinh";
            this.gridColNoiSinh.FieldName = "NoiSinh";
            this.gridColNoiSinh.Name = "gridColNoiSinh";
            this.gridColNoiSinh.Visible = true;
            this.gridColNoiSinh.VisibleIndex = 3;
            // 
            // gridColDiaChi
            // 
            this.gridColDiaChi.Caption = "Địa Chỉ";
            this.gridColDiaChi.FieldName = "DiaChi";
            this.gridColDiaChi.Name = "gridColDiaChi";
            this.gridColDiaChi.Visible = true;
            this.gridColDiaChi.VisibleIndex = 4;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng/Khoa";
            this.gridColPhongKhoa.FieldName = "PhongKhoa";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 5;
            // 
            // gridColBoMon
            // 
            this.gridColBoMon.Caption = "Bộ Môn";
            this.gridColBoMon.FieldName = "BoMon";
            this.gridColBoMon.Name = "gridColBoMon";
            this.gridColBoMon.Visible = true;
            this.gridColBoMon.VisibleIndex = 6;
            // 
            // gridColTrinhDo
            // 
            this.gridColTrinhDo.Caption = "Trình Độ";
            this.gridColTrinhDo.FieldName = "TrinhDo";
            this.gridColTrinhDo.Name = "gridColTrinhDo";
            this.gridColTrinhDo.Visible = true;
            this.gridColTrinhDo.VisibleIndex = 7;
            // 
            // gridColChuyenNganh
            // 
            this.gridColChuyenNganh.Caption = "Chuyên Ngành";
            this.gridColChuyenNganh.FieldName = "ChuyenNganh";
            this.gridColChuyenNganh.Name = "gridColChuyenNganh";
            this.gridColChuyenNganh.Visible = true;
            this.gridColChuyenNganh.VisibleIndex = 8;
            // 
            // gridColTruong
            // 
            this.gridColTruong.Caption = "Trường";
            this.gridColTruong.FieldName = "Truong";
            this.gridColTruong.Name = "gridColTruong";
            this.gridColTruong.Visible = true;
            this.gridColTruong.VisibleIndex = 9;
            // 
            // gridColHinhThuc
            // 
            this.gridColHinhThuc.Caption = "Hình Thức";
            this.gridColHinhThuc.FieldName = "HinhThuc";
            this.gridColHinhThuc.Name = "gridColHinhThuc";
            this.gridColHinhThuc.Visible = true;
            this.gridColHinhThuc.VisibleIndex = 10;
            // 
            // gridColNamTotNghiep
            // 
            this.gridColNamTotNghiep.Caption = "Năm Tốt Nghiệp";
            this.gridColNamTotNghiep.FieldName = "NamTotNghiep";
            this.gridColNamTotNghiep.Name = "gridColNamTotNghiep";
            this.gridColNamTotNghiep.Visible = true;
            this.gridColNamTotNghiep.VisibleIndex = 11;
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.txtPg1NoiSinh);
            this.groupControl2.Controls.Add(this.dtPg1NgaySinh);
            this.groupControl2.Controls.Add(this.rdbPg1GioiTinh);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.textEdit1);
            this.groupControl2.Controls.Add(this.txtPg1HoDem);
            this.groupControl2.Location = new System.Drawing.Point(0, 2);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(856, 243);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Thông tin cơ bản";
            // 
            // txtPg1NoiSinh
            // 
            this.txtPg1NoiSinh.Location = new System.Drawing.Point(488, 116);
            this.txtPg1NoiSinh.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.txtPg1NoiSinh.Name = "txtPg1NoiSinh";
            this.txtPg1NoiSinh.Properties.AllowFocused = false;
            this.txtPg1NoiSinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1NoiSinh.Properties.Appearance.Options.UseFont = true;
            this.txtPg1NoiSinh.Size = new System.Drawing.Size(355, 38);
            this.txtPg1NoiSinh.TabIndex = 15;
            // 
            // dtPg1NgaySinh
            // 
            this.dtPg1NgaySinh.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgaySinh.Location = new System.Drawing.Point(145, 116);
            this.dtPg1NgaySinh.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.dtPg1NgaySinh.Name = "dtPg1NgaySinh";
            this.dtPg1NgaySinh.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtPg1NgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgaySinh.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgaySinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgaySinh.Properties.NullDate = "1-1-0001";
            this.dtPg1NgaySinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgaySinh.Size = new System.Drawing.Size(215, 38);
            this.dtPg1NgaySinh.TabIndex = 10;
            // 
            // rdbPg1GioiTinh
            // 
            this.rdbPg1GioiTinh.EditValue = 0;
            this.rdbPg1GioiTinh.Location = new System.Drawing.Point(613, 54);
            this.rdbPg1GioiTinh.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.rdbPg1GioiTinh.Name = "rdbPg1GioiTinh";
            this.rdbPg1GioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPg1GioiTinh.Properties.Appearance.Options.UseFont = true;
            this.rdbPg1GioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Nữ")});
            this.rdbPg1GioiTinh.Size = new System.Drawing.Size(230, 49);
            this.rdbPg1GioiTinh.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(488, 60);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(115, 32);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Giới tính";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(13, 185);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 32);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Địa chỉ";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(13, 123);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(132, 32);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Sinh ngày";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(373, 123);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(112, 32);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Nơi sinh";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(13, 60);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(86, 32);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Họ tên";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(145, 178);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.AllowFocused = false;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(698, 38);
            this.textEdit1.TabIndex = 0;
            // 
            // txtPg1HoDem
            // 
            this.txtPg1HoDem.Location = new System.Drawing.Point(145, 54);
            this.txtPg1HoDem.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.txtPg1HoDem.Name = "txtPg1HoDem";
            this.txtPg1HoDem.Properties.AllowFocused = false;
            this.txtPg1HoDem.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1HoDem.Properties.Appearance.Options.UseFont = true;
            this.txtPg1HoDem.Size = new System.Drawing.Size(329, 38);
            this.txtPg1HoDem.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.lookUpEdit1);
            this.groupControl1.Controls.Add(this.lookUpEdit2);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Location = new System.Drawing.Point(869, 2);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(589, 243);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Vị trí ứng tuyển";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EditValue = "";
            this.lookUpEdit1.Location = new System.Drawing.Point(228, 54);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã phòng khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên phòng khoa")});
            this.lookUpEdit1.Properties.DisplayMember = "TenPhongKhoa";
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Properties.ValueMember = "MaPhongKhoa";
            this.lookUpEdit1.Size = new System.Drawing.Size(349, 38);
            this.lookUpEdit1.TabIndex = 22;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EditValue = "";
            this.lookUpEdit2.Location = new System.Drawing.Point(228, 116);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã B.Môn B.Phận", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Tên bộ môn/bộ phận")});
            this.lookUpEdit2.Properties.DisplayMember = "TenBMBP";
            this.lookUpEdit2.Properties.NullText = "";
            this.lookUpEdit2.Properties.ValueMember = "MaBMBP";
            this.lookUpEdit2.Size = new System.Drawing.Size(349, 38);
            this.lookUpEdit2.TabIndex = 23;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(11, 123);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(223, 32);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "B.Môn/B.Phận (*)";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(11, 60);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(200, 32);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Phòng/Khoa (*)";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.txtCMNgayCap);
            this.groupControl3.Controls.Add(this.labelControl60);
            this.groupControl3.Controls.Add(this.labelControl50);
            this.groupControl3.Controls.Add(this.cbbCMHinhThuc);
            this.groupControl3.Controls.Add(this.txtCMChuyenNganh);
            this.groupControl3.Controls.Add(this.labelControl57);
            this.groupControl3.Controls.Add(this.labelControl53);
            this.groupControl3.Controls.Add(this.labelControl54);
            this.groupControl3.Controls.Add(this.cbbCMTrinhDo);
            this.groupControl3.Controls.Add(this.cbbCMTruong);
            this.groupControl3.Location = new System.Drawing.Point(1471, 2);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1268, 243);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "Trình độ chuyên môn";
            // 
            // txtCMNgayCap
            // 
            this.txtCMNgayCap.Location = new System.Drawing.Point(208, 178);
            this.txtCMNgayCap.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.txtCMNgayCap.Name = "txtCMNgayCap";
            this.txtCMNgayCap.Properties.AllowFocused = false;
            this.txtCMNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtCMNgayCap.Size = new System.Drawing.Size(358, 38);
            this.txtCMNgayCap.TabIndex = 18;
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Location = new System.Drawing.Point(11, 185);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(203, 32);
            this.labelControl60.TabIndex = 17;
            this.labelControl60.Text = "Năm tốt nghiệp";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(579, 123);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(131, 32);
            this.labelControl50.TabIndex = 15;
            this.labelControl50.Text = "Hình thức";
            // 
            // cbbCMHinhThuc
            // 
            this.cbbCMHinhThuc.Location = new System.Drawing.Point(711, 116);
            this.cbbCMHinhThuc.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.cbbCMHinhThuc.Name = "cbbCMHinhThuc";
            this.cbbCMHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbCMHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã HTDT", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbCMHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbCMHinhThuc.Properties.NullText = "";
            this.cbbCMHinhThuc.Properties.PopupSizeable = false;
            this.cbbCMHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMHinhThuc.Properties.ValueMember = "Id";
            this.cbbCMHinhThuc.Size = new System.Drawing.Size(358, 38);
            this.cbbCMHinhThuc.TabIndex = 16;
            // 
            // txtCMChuyenNganh
            // 
            this.txtCMChuyenNganh.Location = new System.Drawing.Point(208, 116);
            this.txtCMChuyenNganh.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.txtCMChuyenNganh.Name = "txtCMChuyenNganh";
            this.txtCMChuyenNganh.Properties.AllowFocused = false;
            this.txtCMChuyenNganh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMChuyenNganh.Properties.Appearance.Options.UseFont = true;
            this.txtCMChuyenNganh.Size = new System.Drawing.Size(358, 38);
            this.txtCMChuyenNganh.TabIndex = 14;
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(11, 123);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(192, 32);
            this.labelControl57.TabIndex = 9;
            this.labelControl57.Text = "Chuyên ngành";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(11, 60);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(112, 32);
            this.labelControl53.TabIndex = 10;
            this.labelControl53.Text = "Trình độ";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(579, 60);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(101, 32);
            this.labelControl54.TabIndex = 11;
            this.labelControl54.Text = "Trường";
            // 
            // cbbCMTrinhDo
            // 
            this.cbbCMTrinhDo.Location = new System.Drawing.Point(208, 54);
            this.cbbCMTrinhDo.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.cbbCMTrinhDo.Name = "cbbCMTrinhDo";
            this.cbbCMTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.cbbCMTrinhDo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMTrinhDo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangChuyenMon", "Trình độ")});
            this.cbbCMTrinhDo.Properties.DisplayMember = "TenLoaiBangChuyenMon";
            this.cbbCMTrinhDo.Properties.NullText = "";
            this.cbbCMTrinhDo.Properties.PopupSizeable = false;
            this.cbbCMTrinhDo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMTrinhDo.Properties.ValueMember = "Id";
            this.cbbCMTrinhDo.Size = new System.Drawing.Size(358, 38);
            this.cbbCMTrinhDo.TabIndex = 12;
            // 
            // cbbCMTruong
            // 
            this.cbbCMTruong.Location = new System.Drawing.Point(711, 54);
            this.cbbCMTruong.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.cbbCMTruong.Name = "cbbCMTruong";
            this.cbbCMTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbCMTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbCMTruong.Properties.DisplayMember = "TenTruong";
            this.cbbCMTruong.Properties.NullText = "";
            this.cbbCMTruong.Properties.PopupSizeable = false;
            this.cbbCMTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMTruong.Properties.ValueMember = "MaTruong";
            this.cbbCMTruong.Size = new System.Drawing.Size(546, 38);
            this.cbbCMTruong.TabIndex = 13;
            // 
            // btnCMSua
            // 
            this.btnCMSua.Location = new System.Drawing.Point(377, 259);
            this.btnCMSua.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.btnCMSua.Name = "btnCMSua";
            this.btnCMSua.Size = new System.Drawing.Size(163, 51);
            this.btnCMSua.TabIndex = 11;
            this.btnCMSua.Text = "Hiệu chỉnh";
            // 
            // btnCMXoa
            // 
            this.btnCMXoa.Location = new System.Drawing.Point(553, 259);
            this.btnCMXoa.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.btnCMXoa.Name = "btnCMXoa";
            this.btnCMXoa.Size = new System.Drawing.Size(163, 51);
            this.btnCMXoa.TabIndex = 8;
            this.btnCMXoa.Text = "Xóa";
            // 
            // btnCMThem
            // 
            this.btnCMThem.Location = new System.Drawing.Point(202, 259);
            this.btnCMThem.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.btnCMThem.Name = "btnCMThem";
            this.btnCMThem.Size = new System.Drawing.Size(163, 51);
            this.btnCMThem.TabIndex = 9;
            this.btnCMThem.Text = "Lưu";
            // 
            // btnCMTaoMoi
            // 
            this.btnCMTaoMoi.Location = new System.Drawing.Point(26, 259);
            this.btnCMTaoMoi.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.btnCMTaoMoi.Name = "btnCMTaoMoi";
            this.btnCMTaoMoi.Size = new System.Drawing.Size(163, 51);
            this.btnCMTaoMoi.TabIndex = 10;
            this.btnCMTaoMoi.Text = "Tạo mới";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(2557, 259);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(156, 51);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "In danh sách";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(2388, 259);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(156, 51);
            this.simpleButton2.TabIndex = 8;
            this.simpleButton2.Text = "Tạm tuyển";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // frmDanhSachUngVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2722, 1098);
            this.Controls.Add(this.btnCMSua);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnCMXoa);
            this.Controls.Add(this.btnCMThem);
            this.Controls.Add(this.btnCMTaoMoi);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.gcDanToc);
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "frmDanhSachUngVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh Sách Ứng Viên";
            ((System.ComponentModel.ISupportInitialize)(this.gcDanToc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDantoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1NoiSinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMChuyenNganh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTruong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcDanToc;
        private GroupControl groupControl2;
        private TextEdit txtPg1NoiSinh;
        private DateEdit dtPg1NgaySinh;
        private RadioGroup rdbPg1GioiTinh;
        private LabelControl labelControl7;
        private LabelControl labelControl10;
        private LabelControl labelControl11;
        private LabelControl labelControl5;
        private TextEdit txtPg1HoDem;
        private LabelControl labelControl1;
        private TextEdit textEdit1;
        private GroupControl groupControl1;
        private LookUpEdit lookUpEdit1;
        private LookUpEdit lookUpEdit2;
        private LabelControl labelControl2;
        private LabelControl labelControl3;
        private GroupControl groupControl3;
        private TextEdit txtCMChuyenNganh;
        private LabelControl labelControl57;
        private LabelControl labelControl53;
        private LabelControl labelControl54;
        private LookUpEdit cbbCMTrinhDo;
        private LookUpEdit cbbCMTruong;
        private LabelControl labelControl50;
        private LookUpEdit cbbCMHinhThuc;
        private TextEdit txtCMNgayCap;
        private LabelControl labelControl60;
        private SimpleButton btnCMSua;
        private SimpleButton btnCMXoa;
        private SimpleButton btnCMThem;
        private SimpleButton btnCMTaoMoi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDantoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHoTen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNoiSinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhongKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBoMon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTrinhDo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChuyenNganh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNamTotNghiep;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHinhThuc;
        private SimpleButton simpleButton1;
        private SimpleButton simpleButton2;
    }
}