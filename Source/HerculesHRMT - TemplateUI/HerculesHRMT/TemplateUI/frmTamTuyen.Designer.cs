﻿namespace HerculesHRMT.TemplateUI
{
    partial class frmTamTuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chbPg1DangVien = new DevExpress.XtraEditors.CheckEdit();
            this.btnCMXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMThem = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1DangVien.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(12, 12);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(453, 15);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Ứng viên Nguyễn Văn A sẽ được xét tạm tuyển vào vị trí giảng viên Tin Học thuộc\r\n" +
    "";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EditValue = "";
            this.lookUpEdit1.Location = new System.Drawing.Point(197, 30);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã phòng khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên phòng khoa")});
            this.lookUpEdit1.Properties.DisplayMember = "TenPhongKhoa";
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Properties.ValueMember = "MaPhongKhoa";
            this.lookUpEdit1.Size = new System.Drawing.Size(161, 22);
            this.lookUpEdit1.TabIndex = 26;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EditValue = "";
            this.lookUpEdit2.Location = new System.Drawing.Point(197, 58);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã B.Môn B.Phận", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Tên bộ môn/bộ phận")});
            this.lookUpEdit2.Properties.DisplayMember = "TenBMBP";
            this.lookUpEdit2.Properties.NullText = "";
            this.lookUpEdit2.Properties.ValueMember = "MaBMBP";
            this.lookUpEdit2.Size = new System.Drawing.Size(161, 22);
            this.lookUpEdit2.TabIndex = 27;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(97, 61);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(94, 15);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "B.Môn/B.Phận (*)";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(97, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 15);
            this.labelControl3.TabIndex = 25;
            this.labelControl3.Text = "Phòng/Khoa (*)";
            // 
            // chbPg1DangVien
            // 
            this.chbPg1DangVien.Location = new System.Drawing.Point(195, 83);
            this.chbPg1DangVien.Name = "chbPg1DangVien";
            this.chbPg1DangVien.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbPg1DangVien.Properties.Appearance.Options.UseFont = true;
            this.chbPg1DangVien.Properties.Caption = "Thay đổi";
            this.chbPg1DangVien.Size = new System.Drawing.Size(134, 20);
            this.chbPg1DangVien.TabIndex = 47;
            // 
            // btnCMXoa
            // 
            this.btnCMXoa.Location = new System.Drawing.Point(244, 109);
            this.btnCMXoa.Name = "btnCMXoa";
            this.btnCMXoa.Size = new System.Drawing.Size(75, 23);
            this.btnCMXoa.TabIndex = 48;
            this.btnCMXoa.Text = "Cancel";
            // 
            // btnCMThem
            // 
            this.btnCMThem.Location = new System.Drawing.Point(145, 109);
            this.btnCMThem.Name = "btnCMThem";
            this.btnCMThem.Size = new System.Drawing.Size(75, 23);
            this.btnCMThem.TabIndex = 49;
            this.btnCMThem.Text = "OK";
            // 
            // frmTamTuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 140);
            this.Controls.Add(this.btnCMXoa);
            this.Controls.Add(this.btnCMThem);
            this.Controls.Add(this.chbPg1DangVien);
            this.Controls.Add(this.lookUpEdit1);
            this.Controls.Add(this.lookUpEdit2);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl5);
            this.Name = "frmTamTuyen";
            this.Text = "Tạm Tuyển";
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1DangVien.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit chbPg1DangVien;
        private DevExpress.XtraEditors.SimpleButton btnCMXoa;
        private DevExpress.XtraEditors.SimpleButton btnCMThem;
    }
}