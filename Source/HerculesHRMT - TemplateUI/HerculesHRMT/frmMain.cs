﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class frmMain : RibbonForm
    {
        bool isDangnhap = false;
        public frmMain()
        {
            InitializeComponent();
            InitSkinGallery();
            InitGrid();

        }
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        void InitGrid()
        {
            
        }

        private void iLogout_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Ribbon.Minimized = true;
            if (splitContainerControl.Panel2.Controls.Count > 1)
            {
                splitContainerControl.Panel2.Controls.RemoveAt(0);
                
            }
            LoadDangNhap();
        }

        private void LoadDangNhap()
        {
            this.iLogout.Links[0].Visible = false;
            isDangnhap = false;
            RemoveAllUserControl();
            ucDangNhap ucDN = new ucDangNhap();
            Point p = new Point(splitContainerControl.Panel2.Width / 2 - ucDN.Width / 2, splitContainerControl.Panel2.Height / 4 - ucDN.Height / 4);
            ucDN.Anchor = AnchorStyles.None;
            ucDN.Location = p;
            splitContainerControl.Panel2.Controls.Add(ucDN);
            AcceptButton = ucDN.btnDangNhap;
            ucDN.dnSender = getSender;
        }
        private void itemTinhThanh_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTinhThanh());
        }

        private void RemoveAllUserControl()
        {
            foreach (Control ctl in splitContainerControl.Panel2.Controls)
            {
                splitContainerControl.Panel2.Controls.Remove(ctl);
            }
        }

        private void itemQuanHuyen_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucQuanHuyen());
        }

        private void itemTruong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTruongHoc());
        }

        private void itemHocVan_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucHocVanBangCap());
        }

        private void itemChuVu_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucChucVu());
        }

        private void itemPhongKhoa_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucPhongKhoa());
        }

        private void itemNganh_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucNganh());
        }

        private void itemTrangThaiLV_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTrangThaiLamViec());
        }

        private void itemQuanHe_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucQuanHeThanNhan());
        }

        private void itemLoaiHopDong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiHopDong());
        }

        private void itemNhanVien_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDanhSachNhanVien());
        }

        private void itemBoMon_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucBoPhanBoMon());
        }

        public void getSender(bool isDaDangNhap)
        {
            isDangnhap = isDaDangNhap;
            this.iLogout.Links[0].Visible = true;
            RemoveAllUserControl();
            ucDashBoard ucDB = new ucDashBoard(this);
            //ucDanhSachNhanVien ucDSNV = new ucDanhSachNhanVien();
            Point p = new Point(splitContainerControl.Panel2.Width / 2 - ucDB.Width / 2, splitContainerControl.Panel2.Height / 4 - ucDB.Height / 4);
            ucDB.Anchor = AnchorStyles.None;
            ucDB.Location = p;
            //ucDB.Dock = DockStyle.Fill;
            splitContainerControl.Panel2.Controls.Add(ucDB);
        }

        public void OpenDanhSachNhanVien()
        {
            GanUserControl(new ucDanhSachNhanVien());
        }

        public void OpenChuyenGiaiDoan()
        {
            GanUserControl(new ucChuyenHopDong());
        }

        public void OpenChuyenCongTac()
        {
            GanUserControl(new ucChuyenCongTac());
        }

        public void OpenGiaHanHopDong()
        {
            GanUserControl(new ucGiaHanHopDong());
        }

        public void OpenNghiViec()
        {
            GanUserControl(new ucNghiViec());
        }

        private void itemDantoc_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDanToc());
        }

        private void itemTongiao_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTonGiao());
        }

        private void itemHedaotao_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucHeDaoTao());
        }

        private void GanUserControl(XtraUserControl uc)
        {
            if (isDangnhap)
            {
                RemoveAllUserControl();
                uc.Dock = DockStyle.Fill;
                splitContainerControl.Panel2.Controls.Add(uc);
            }
            else
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng đăng nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void itemLoaitrinhdo_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiTrinhDo());
        }

        private void itemLoaiNV_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiNhanVien());
        }

        private void itemLoaiQD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiQuyetDinh());
        }

        private void iLogout_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            ReportCTL abc = new ReportCTL();
            abc.LayDuLieu();

            //if (XtraMessageBox.Show("Quý thầy cô xác thực đăng xuất.", "Đăng xuất", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    LoadDangNhap();
            //}
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (isDangnhap)
            {
                RemoveAllUserControl();
                ucDashBoard ucDB = new ucDashBoard(this);
                //ucDanhSachNhanVien ucDSNV = new ucDanhSachNhanVien();
                Point p = new Point(splitContainerControl.Panel2.Width / 2 - ucDB.Width / 2, splitContainerControl.Panel2.Height / 4 - ucDB.Height / 4);
                ucDB.Anchor = AnchorStyles.None;
                ucDB.Location = p;
                //ucDB.Dock = DockStyle.Fill;
                splitContainerControl.Panel2.Controls.Add(ucDB);
            }
            else
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng đăng nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}