﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucNVQuanHeGiaDinh : XtraUserControl
    {
        string strMaNV = "";
        QuanHeGiaDinhCTL giadinhCTL = new QuanHeGiaDinhCTL();
        QuanHuyenCTL quanhuyenCTL = new QuanHuyenCTL();
        QuanHeGiaDinhDto qhgdDTO = new QuanHeGiaDinhDto();
        List<QuanHeGiaDinhDto> listThanNhan = new List<QuanHeGiaDinhDto>();
        Common.STATUS _status;

        #region CAU HINH THEM NHAN VIEN
        // false: opened by add new staff
        // true : opened by edit staff
        //private Boolean _openedByEditStaff = false;

        ////
        //private string LUU_TAM = "Lưu Tạm";

        //public delegate void SendQuanHeGiaDinh(QuanHeGiaDinhDto quanHeGiaDinhDto);
        //public SendQuanHeGiaDinh QuanHeGiaDinhSender;

        #endregion

        public ucNVQuanHeGiaDinh()
        {
            InitializeComponent();
        }
        public ucNVQuanHeGiaDinh(Boolean openedByEditStaff = true)
        {
            InitializeComponent();

            //if (openedByEditStaff) return;

            //_openedByEditStaff = openedByEditStaff;
            //btnPg5Them.Text = LUU_TAM;
            //btnPg5Sua.Visible = false;
            //btnPg5Xoa.Visible = false;
        }

        public void SetMaNV(string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _status = status;
            qhgdDTO = new QuanHeGiaDinhDto();
            BindingData(qhgdDTO);
            SetStatusComponents();
            LoadDanhSach();
        }

        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    BindingData(new QuanHeGiaDinhDto());
                    btnPg5Sua.Enabled = false;
                    btnPg5Them.Enabled = true;
                    setReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    setReadOnly(true);
                    if (qhgdDTO.Id > 0)
                    {
                        btnPg5Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg5Sua.Enabled = false;
                    }
                    btnPg5Them.Enabled = false;
                    btnPg5TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    setReadOnly(false);
                    btnPg5Sua.Enabled = false;
                    btnPg5Them.Enabled = true;
                    btnPg5TaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void setReadOnly(bool read)
        {
            txtPg5DacDiemLS.Properties.ReadOnly = read;
            txtPg5DiaChi.Properties.ReadOnly = read;
            txtPg5GhiChu.Properties.ReadOnly = read;
            txtPg5HoTen.Properties.ReadOnly = read;
            txtPg5NgheNghiep.Properties.ReadOnly = read;
            cbbPg5LoaiQH.Properties.ReadOnly = read;
            dtPg5NgaySinh.Properties.ReadOnly = read;
            chbTNBenVoChong.Properties.ReadOnly = read;
        }

        private void LoadDanhSach()
        {
            listThanNhan = giadinhCTL.LayDanhSachThanNhanCuaNhanVien(strMaNV);
            gvPg5QuanHeGiaDinh.DataSource = listThanNhan;
        }

        private void BindingData(QuanHeGiaDinhDto qhgdDTO)
        {
            if (qhgdDTO == null) qhgdDTO = new QuanHeGiaDinhDto();
            txtPg5DacDiemLS.EditValue = qhgdDTO.DacDiemLichSu;
            txtPg5DiaChi.EditValue = qhgdDTO.DiaChi;
            txtPg5GhiChu.EditValue = qhgdDTO.GhiChu;
            txtPg5HoTen.EditValue = qhgdDTO.HoTen;
            txtPg5NgheNghiep.EditValue = qhgdDTO.NgheNghiep;
            dtPg5NgaySinh.EditValue = qhgdDTO.NgaySinh;
            cbbPg5LoaiQH.EditValue = qhgdDTO.LoaiQuanHe.MaQuanHe;
            chbTNBenVoChong.Checked = qhgdDTO.ThanNhanVoChong;
        }

        private void btnPg5TaoMoi_Click(object sender, EventArgs e)
        {
            qhgdDTO = new QuanHeGiaDinhDto();
            _status = Common.STATUS.NEW;
            SetStatusComponents();
            dtPg5NgaySinh.EditValue = "";
        }

        private void btnPg5Sua_Click(object sender, EventArgs e)
        {
            if (qhgdDTO.Id > 0)
            {
                _status = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }

        private bool GetDataOncomponentsAndCheck()
        {
            int error = 0;
            qhgdDTO.MaNv = strMaNV;
            if (txtPg5HoTen.Text.Trim() == "") error = 1;
            qhgdDTO.HoTen = txtPg5HoTen.EditValue as String;
            if (dtPg5NgaySinh.Text == "") error = 1;
            else qhgdDTO.NgaySinh = dtPg5NgaySinh.Text;
            if (cbbPg5LoaiQH.EditValue == null) error = 1;
            QuanHeThanNhanDTO thannhan = new QuanHeThanNhanDTO();
            thannhan.MaQuanHe = cbbPg5LoaiQH.EditValue as String;
            qhgdDTO.LoaiQuanHe = thannhan;
            qhgdDTO.DacDiemLichSu = txtPg5DacDiemLS.EditValue as String;
            qhgdDTO.GhiChu = txtPg5GhiChu.EditValue as String;
            qhgdDTO.NgheNghiep = txtPg5NgheNghiep.EditValue as String;
            TinhThanhDTO tinh = new TinhThanhDTO();
            qhgdDTO.TinhThanh = tinh;
            QuanHuyenDTO quan = new QuanHuyenDTO();
            qhgdDTO.QuanHuyen = quan;
            qhgdDTO.DiaChi = txtPg5DiaChi.EditValue as String;
            qhgdDTO.ThanNhanVoChong = chbTNBenVoChong.Checked;
            return error == 0;
        }


        private void gridviewQuanHeGiaDinh_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                //int id = Convert.ToInt32(((GridView)sender).GetRowCellValue(e.FocusedRowHandle, "Id").ToString());
                qhgdDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as QuanHeGiaDinhDto;
                if (qhgdDTO == null) qhgdDTO = new QuanHeGiaDinhDto();
                BindingData(qhgdDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void btnPg5Xoa_Click(object sender, EventArgs e)
        {
            if (qhgdDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (giadinhCTL.XoaQuanHeGiaDinh(qhgdDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnPg5Them_Click(object sender, EventArgs e)
        {
            if (GetDataOncomponentsAndCheck())
            {
                // for add new staff
                //if (_openedByEditStaff)
                //{
                //    QuanHeGiaDinhSender(qhgdDTO);
                //    return;
                //}

                bool kq = false;
                if (_status == Common.STATUS.NEW)
                {
                    kq = giadinhCTL.LuuMoiQuanHeGiaDinh(qhgdDTO);
                }
                if (_status == Common.STATUS.EDIT)
                {
                    kq = giadinhCTL.CapNhatQuanHeGiaDinh(qhgdDTO);
                }

                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _status = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusComponents();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ucNVQuanHeGiaDinh_Load(object sender, EventArgs e)
        {
            cbbPg5LoaiQH.Properties.DataSource = (new QuanHeThanNhanCTL()).GetAll();
        }

        private void gridviewQuanHeGiaDinh_RowClick(object sender, RowClickEventArgs e)
        {
            //int id = Convert.ToInt32(((GridView)sender).GetRowCellValue(e.RowHandle, "Id").ToString());
            qhgdDTO = qhgdDTO = ((GridView)sender).GetRow(e.RowHandle) as QuanHeGiaDinhDto;
            BindingData(qhgdDTO);
            _status = Common.STATUS.VIEW;
            SetStatusComponents();
        }

        public string CheckValue()
        {
            var result = "Vui lòng nhập các thông tin bắt buộc (*) trong: ";

            if (!GetDataOncomponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Quan Hệ Gia Đình");
            }

            return result;
        }

        public QuanHeGiaDinhDto GetData()
        {
            return qhgdDTO;
        }

    }
}
