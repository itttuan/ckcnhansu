﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVBangCapChungChi
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.gvPg3BCCC = new DevExpress.XtraGrid.GridControl();
            this.gridviewBCCC = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBCID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyenmon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyennganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCoSoDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTgianDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTgHieuluc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayCapBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGHICHUBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckedComboBoxEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.btnPg3Sua = new DevExpress.XtraEditors.SimpleButton();
            this.rdbPg3PhanLoaiBCCC = new DevExpress.XtraEditors.RadioGroup();
            this.dtPg3NgayCapBCCC = new DevExpress.XtraEditors.DateEdit();
            this.txtPg3GhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.lblMaBCCC = new DevExpress.XtraEditors.LabelControl();
            this.lblLoaiBCCC = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg3MaBCCC = new DevExpress.XtraEditors.TextEdit();
            this.txtPg3ThoiGianDT = new DevExpress.XtraEditors.TextEdit();
            this.txtPg3ThoiGianHieuLuc = new DevExpress.XtraEditors.TextEdit();
            this.cbbPg3NganhDT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg3LoaiBCCC = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg3ChuyenNganhDT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg3TrinhDoDT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg3HeDT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg3CoSoDT = new DevExpress.XtraEditors.LookUpEdit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3BCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewBCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEditXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg3PhanLoaiBCCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg3NgayCapBCCC.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg3NgayCapBCCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3GhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3MaBCCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3ThoiGianDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3ThoiGianHieuLuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3NganhDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3LoaiBCCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3ChuyenNganhDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3TrinhDoDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3HeDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3CoSoDT.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gvPg3BCCC);
            this.xtraScrollableControl1.Controls.Add(this.groupControl10);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1280, 510);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // gvPg3BCCC
            // 
            this.gvPg3BCCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPg3BCCC.Location = new System.Drawing.Point(0, 184);
            this.gvPg3BCCC.MainView = this.gridviewBCCC;
            this.gvPg3BCCC.Name = "gvPg3BCCC";
            this.gvPg3BCCC.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckedComboBoxEditXoa});
            this.gvPg3BCCC.Size = new System.Drawing.Size(1280, 326);
            this.gvPg3BCCC.TabIndex = 121;
            this.gvPg3BCCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewBCCC});
            // 
            // gridviewBCCC
            // 
            this.gridviewBCCC.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBCID,
            this.gridColMaBCCC,
            this.gridColLoaiBCCC,
            this.gridColChuyenmon,
            this.gridColTrinhDo,
            this.gridColChuyennganh,
            this.gridColCoSoDT,
            this.gridColTgianDT,
            this.gridColTgHieuluc,
            this.gridColNgayCapBCCC,
            this.gridColHDT,
            this.gridColGHICHUBCCC});
            this.gridviewBCCC.GridControl = this.gvPg3BCCC;
            this.gridviewBCCC.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridviewBCCC.Name = "gridviewBCCC";
            this.gridviewBCCC.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewBCCC_FocusedRowChanged);
            // 
            // gridColBCID
            // 
            this.gridColBCID.Caption = "Mã ID";
            this.gridColBCID.FieldName = "Id";
            this.gridColBCID.Name = "gridColBCID";
            this.gridColBCID.OptionsColumn.AllowEdit = false;
            this.gridColBCID.OptionsColumn.ReadOnly = true;
            // 
            // gridColMaBCCC
            // 
            this.gridColMaBCCC.Caption = "Mã B.Cấp/C.Chỉ";
            this.gridColMaBCCC.FieldName = "MaBangCapChungChi";
            this.gridColMaBCCC.Name = "gridColMaBCCC";
            this.gridColMaBCCC.OptionsColumn.AllowEdit = false;
            this.gridColMaBCCC.OptionsColumn.ReadOnly = true;
            this.gridColMaBCCC.Visible = true;
            this.gridColMaBCCC.VisibleIndex = 0;
            this.gridColMaBCCC.Width = 73;
            // 
            // gridColLoaiBCCC
            // 
            this.gridColLoaiBCCC.Caption = "Loại B.Cấp/C.Chỉ";
            this.gridColLoaiBCCC.FieldName = "LoaiBangCapChungChi.TenLoai";
            this.gridColLoaiBCCC.Name = "gridColLoaiBCCC";
            this.gridColLoaiBCCC.OptionsColumn.AllowEdit = false;
            this.gridColLoaiBCCC.OptionsColumn.ReadOnly = true;
            this.gridColLoaiBCCC.Visible = true;
            this.gridColLoaiBCCC.VisibleIndex = 1;
            this.gridColLoaiBCCC.Width = 73;
            // 
            // gridColChuyenmon
            // 
            this.gridColChuyenmon.Caption = "Chuyên môn";
            this.gridColChuyenmon.Name = "gridColChuyenmon";
            this.gridColChuyenmon.OptionsColumn.AllowEdit = false;
            this.gridColChuyenmon.OptionsColumn.ReadOnly = true;
            this.gridColChuyenmon.Visible = true;
            this.gridColChuyenmon.VisibleIndex = 2;
            this.gridColChuyenmon.Width = 73;
            // 
            // gridColTrinhDo
            // 
            this.gridColTrinhDo.Caption = "Trình độ";
            this.gridColTrinhDo.FieldName = "LoaiTrinhDo.TenLoaiTrinhDo";
            this.gridColTrinhDo.Name = "gridColTrinhDo";
            this.gridColTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColTrinhDo.Visible = true;
            this.gridColTrinhDo.VisibleIndex = 3;
            this.gridColTrinhDo.Width = 73;
            // 
            // gridColChuyennganh
            // 
            this.gridColChuyennganh.Caption = "Chuyên ngành";
            this.gridColChuyennganh.Name = "gridColChuyennganh";
            this.gridColChuyennganh.OptionsColumn.AllowEdit = false;
            this.gridColChuyennganh.OptionsColumn.ReadOnly = true;
            this.gridColChuyennganh.Visible = true;
            this.gridColChuyennganh.VisibleIndex = 4;
            this.gridColChuyennganh.Width = 73;
            // 
            // gridColCoSoDT
            // 
            this.gridColCoSoDT.Caption = "Cơ sở đào tạo";
            this.gridColCoSoDT.FieldName = "CoSoDaotao.TenTruong";
            this.gridColCoSoDT.Name = "gridColCoSoDT";
            this.gridColCoSoDT.OptionsColumn.AllowEdit = false;
            this.gridColCoSoDT.OptionsColumn.ReadOnly = true;
            this.gridColCoSoDT.Visible = true;
            this.gridColCoSoDT.VisibleIndex = 5;
            this.gridColCoSoDT.Width = 73;
            // 
            // gridColTgianDT
            // 
            this.gridColTgianDT.Caption = "T.gian đào tạo";
            this.gridColTgianDT.FieldName = "ThoiGianDaoTao";
            this.gridColTgianDT.Name = "gridColTgianDT";
            this.gridColTgianDT.OptionsColumn.AllowEdit = false;
            this.gridColTgianDT.OptionsColumn.ReadOnly = true;
            this.gridColTgianDT.Visible = true;
            this.gridColTgianDT.VisibleIndex = 6;
            this.gridColTgianDT.Width = 73;
            // 
            // gridColTgHieuluc
            // 
            this.gridColTgHieuluc.Caption = "T.gian hiệu lực";
            this.gridColTgHieuluc.FieldName = "ThoiGianHieuLuc";
            this.gridColTgHieuluc.Name = "gridColTgHieuluc";
            this.gridColTgHieuluc.OptionsColumn.AllowEdit = false;
            this.gridColTgHieuluc.OptionsColumn.ReadOnly = true;
            this.gridColTgHieuluc.Visible = true;
            this.gridColTgHieuluc.VisibleIndex = 7;
            this.gridColTgHieuluc.Width = 73;
            // 
            // gridColNgayCapBCCC
            // 
            this.gridColNgayCapBCCC.Caption = "Ngày cấp";
            this.gridColNgayCapBCCC.FieldName = "NgayCap";
            this.gridColNgayCapBCCC.Name = "gridColNgayCapBCCC";
            this.gridColNgayCapBCCC.OptionsColumn.AllowEdit = false;
            this.gridColNgayCapBCCC.Visible = true;
            this.gridColNgayCapBCCC.VisibleIndex = 8;
            this.gridColNgayCapBCCC.Width = 73;
            // 
            // gridColHDT
            // 
            this.gridColHDT.Caption = "Hệ đào tạo";
            this.gridColHDT.FieldName = "HeDaoTao.TenHeDaoTao";
            this.gridColHDT.Name = "gridColHDT";
            this.gridColHDT.OptionsColumn.AllowEdit = false;
            this.gridColHDT.OptionsColumn.ReadOnly = true;
            this.gridColHDT.Visible = true;
            this.gridColHDT.VisibleIndex = 9;
            this.gridColHDT.Width = 73;
            // 
            // gridColGHICHUBCCC
            // 
            this.gridColGHICHUBCCC.Caption = "Ghi chú";
            this.gridColGHICHUBCCC.FieldName = "GhiChu";
            this.gridColGHICHUBCCC.Name = "gridColGHICHUBCCC";
            this.gridColGHICHUBCCC.OptionsColumn.AllowEdit = false;
            this.gridColGHICHUBCCC.OptionsColumn.ReadOnly = true;
            this.gridColGHICHUBCCC.Visible = true;
            this.gridColGHICHUBCCC.VisibleIndex = 10;
            this.gridColGHICHUBCCC.Width = 98;
            // 
            // repositoryItemCheckedComboBoxEditXoa
            // 
            this.repositoryItemCheckedComboBoxEditXoa.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEditXoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEditXoa.Name = "repositoryItemCheckedComboBoxEditXoa";
            this.repositoryItemCheckedComboBoxEditXoa.NullText = "Xóa";
            // 
            // groupControl10
            // 
            this.groupControl10.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.Appearance.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.btnPg3Sua);
            this.groupControl10.Controls.Add(this.rdbPg3PhanLoaiBCCC);
            this.groupControl10.Controls.Add(this.dtPg3NgayCapBCCC);
            this.groupControl10.Controls.Add(this.txtPg3GhiChu);
            this.groupControl10.Controls.Add(this.btnPg3Xoa);
            this.groupControl10.Controls.Add(this.btnPg3Them);
            this.groupControl10.Controls.Add(this.btnPg3TaoMoi);
            this.groupControl10.Controls.Add(this.labelControl58);
            this.groupControl10.Controls.Add(this.labelControl60);
            this.groupControl10.Controls.Add(this.labelControl57);
            this.groupControl10.Controls.Add(this.labelControl74);
            this.groupControl10.Controls.Add(this.lblMaBCCC);
            this.groupControl10.Controls.Add(this.lblLoaiBCCC);
            this.groupControl10.Controls.Add(this.labelControl61);
            this.groupControl10.Controls.Add(this.labelControl50);
            this.groupControl10.Controls.Add(this.labelControl59);
            this.groupControl10.Controls.Add(this.labelControl51);
            this.groupControl10.Controls.Add(this.labelControl53);
            this.groupControl10.Controls.Add(this.labelControl54);
            this.groupControl10.Controls.Add(this.txtPg3MaBCCC);
            this.groupControl10.Controls.Add(this.txtPg3ThoiGianDT);
            this.groupControl10.Controls.Add(this.txtPg3ThoiGianHieuLuc);
            this.groupControl10.Controls.Add(this.cbbPg3NganhDT);
            this.groupControl10.Controls.Add(this.cbbPg3LoaiBCCC);
            this.groupControl10.Controls.Add(this.cbbPg3ChuyenNganhDT);
            this.groupControl10.Controls.Add(this.cbbPg3TrinhDoDT);
            this.groupControl10.Controls.Add(this.cbbPg3HeDT);
            this.groupControl10.Controls.Add(this.cbbPg3CoSoDT);
            this.groupControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl10.Location = new System.Drawing.Point(0, 0);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(1280, 184);
            this.groupControl10.TabIndex = 120;
            this.groupControl10.Text = "Thông tin bằng cấp - chứng chỉ";
            // 
            // btnPg3Sua
            // 
            this.btnPg3Sua.Location = new System.Drawing.Point(171, 153);
            this.btnPg3Sua.Name = "btnPg3Sua";
            this.btnPg3Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Sua.TabIndex = 7;
            this.btnPg3Sua.Text = "Hiệu chỉnh";
            this.btnPg3Sua.Click += new System.EventHandler(this.btnPg3Sua_Click);
            // 
            // rdbPg3PhanLoaiBCCC
            // 
            this.rdbPg3PhanLoaiBCCC.EditValue = 0;
            this.rdbPg3PhanLoaiBCCC.Location = new System.Drawing.Point(5, 24);
            this.rdbPg3PhanLoaiBCCC.Name = "rdbPg3PhanLoaiBCCC";
            this.rdbPg3PhanLoaiBCCC.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPg3PhanLoaiBCCC.Properties.Appearance.Options.UseFont = true;
            this.rdbPg3PhanLoaiBCCC.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Bằng cấp"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Chứng chỉ")});
            this.rdbPg3PhanLoaiBCCC.Size = new System.Drawing.Size(164, 26);
            this.rdbPg3PhanLoaiBCCC.TabIndex = 6;
            // 
            // dtPg3NgayCapBCCC
            // 
            this.dtPg3NgayCapBCCC.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg3NgayCapBCCC.Location = new System.Drawing.Point(595, 112);
            this.dtPg3NgayCapBCCC.Name = "dtPg3NgayCapBCCC";
            this.dtPg3NgayCapBCCC.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg3NgayCapBCCC.Properties.Appearance.Options.UseFont = true;
            this.dtPg3NgayCapBCCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg3NgayCapBCCC.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg3NgayCapBCCC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg3NgayCapBCCC.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg3NgayCapBCCC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg3NgayCapBCCC.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg3NgayCapBCCC.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg3NgayCapBCCC.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg3NgayCapBCCC.Size = new System.Drawing.Size(99, 22);
            this.dtPg3NgayCapBCCC.TabIndex = 5;
            // 
            // txtPg3GhiChu
            // 
            this.txtPg3GhiChu.Location = new System.Drawing.Point(912, 56);
            this.txtPg3GhiChu.Name = "txtPg3GhiChu";
            this.txtPg3GhiChu.Properties.AllowFocused = false;
            this.txtPg3GhiChu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg3GhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtPg3GhiChu.Size = new System.Drawing.Size(180, 78);
            this.txtPg3GhiChu.TabIndex = 0;
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(252, 153);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 0;
            this.btnPg3Xoa.Text = "Xóa";
            this.btnPg3Xoa.Click += new System.EventHandler(this.btnPg3Xoa_Click);
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(90, 153);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 0;
            this.btnPg3Them.Text = "Lưu";
            this.btnPg3Them.Click += new System.EventHandler(this.btnPg3Them_Click);
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(9, 153);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 0;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            this.btnPg3TaoMoi.Click += new System.EventHandler(this.btnPg3TaoMoi_Click);
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl58.Location = new System.Drawing.Point(248, 87);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(80, 15);
            this.labelControl58.TabIndex = 0;
            this.labelControl58.Text = "Chuyên ngành";
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Location = new System.Drawing.Point(499, 115);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(52, 15);
            this.labelControl60.TabIndex = 0;
            this.labelControl60.Text = "Ngày cấp";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(5, 87);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(81, 15);
            this.labelControl57.TabIndex = 0;
            this.labelControl57.Text = "Ngành đào tạo";
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl74.Location = new System.Drawing.Point(9, 31);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(0, 15);
            this.labelControl74.TabIndex = 0;
            // 
            // lblMaBCCC
            // 
            this.lblMaBCCC.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBCCC.Location = new System.Drawing.Point(5, 59);
            this.lblMaBCCC.Name = "lblMaBCCC";
            this.lblMaBCCC.Size = new System.Drawing.Size(72, 15);
            this.lblMaBCCC.TabIndex = 0;
            this.lblMaBCCC.Text = "Mã bằng cấp";
            // 
            // lblLoaiBCCC
            // 
            this.lblLoaiBCCC.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiBCCC.Location = new System.Drawing.Point(248, 59);
            this.lblLoaiBCCC.Name = "lblLoaiBCCC";
            this.lblLoaiBCCC.Size = new System.Drawing.Size(79, 15);
            this.lblLoaiBCCC.TabIndex = 0;
            this.lblLoaiBCCC.Text = "Loại bằng cấp";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Location = new System.Drawing.Point(864, 59);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(42, 15);
            this.labelControl61.TabIndex = 0;
            this.labelControl61.Text = "Ghi chú";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(700, 59);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(60, 15);
            this.labelControl50.TabIndex = 0;
            this.labelControl50.Text = "Hệ đào tạo";
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Location = new System.Drawing.Point(248, 115);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(78, 15);
            this.labelControl59.TabIndex = 0;
            this.labelControl59.Text = "T.gian đào tạo";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Location = new System.Drawing.Point(5, 115);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(82, 15);
            this.labelControl51.TabIndex = 0;
            this.labelControl51.Text = "T.gian hiệu lực";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(499, 59);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(90, 15);
            this.labelControl53.TabIndex = 0;
            this.labelControl53.Text = "Trình độ đào tạo";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(499, 87);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(81, 15);
            this.labelControl54.TabIndex = 0;
            this.labelControl54.Text = "Cơ sở đào tạo";
            // 
            // txtPg3MaBCCC
            // 
            this.txtPg3MaBCCC.Location = new System.Drawing.Point(96, 56);
            this.txtPg3MaBCCC.Name = "txtPg3MaBCCC";
            this.txtPg3MaBCCC.Properties.AllowFocused = false;
            this.txtPg3MaBCCC.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg3MaBCCC.Properties.Appearance.Options.UseFont = true;
            this.txtPg3MaBCCC.Size = new System.Drawing.Size(146, 22);
            this.txtPg3MaBCCC.TabIndex = 0;
            // 
            // txtPg3ThoiGianDT
            // 
            this.txtPg3ThoiGianDT.Location = new System.Drawing.Point(346, 112);
            this.txtPg3ThoiGianDT.Name = "txtPg3ThoiGianDT";
            this.txtPg3ThoiGianDT.Properties.AllowFocused = false;
            this.txtPg3ThoiGianDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg3ThoiGianDT.Properties.Appearance.Options.UseFont = true;
            this.txtPg3ThoiGianDT.Size = new System.Drawing.Size(146, 22);
            this.txtPg3ThoiGianDT.TabIndex = 0;
            // 
            // txtPg3ThoiGianHieuLuc
            // 
            this.txtPg3ThoiGianHieuLuc.Location = new System.Drawing.Point(96, 112);
            this.txtPg3ThoiGianHieuLuc.Name = "txtPg3ThoiGianHieuLuc";
            this.txtPg3ThoiGianHieuLuc.Properties.AllowFocused = false;
            this.txtPg3ThoiGianHieuLuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg3ThoiGianHieuLuc.Properties.Appearance.Options.UseFont = true;
            this.txtPg3ThoiGianHieuLuc.Size = new System.Drawing.Size(146, 22);
            this.txtPg3ThoiGianHieuLuc.TabIndex = 0;
            // 
            // cbbPg3NganhDT
            // 
            this.cbbPg3NganhDT.Location = new System.Drawing.Point(96, 84);
            this.cbbPg3NganhDT.Name = "cbbPg3NganhDT";
            this.cbbPg3NganhDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3NganhDT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3NganhDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3NganhDT.Properties.NullText = "";
            this.cbbPg3NganhDT.Properties.PopupSizeable = false;
            this.cbbPg3NganhDT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3NganhDT.Size = new System.Drawing.Size(146, 22);
            this.cbbPg3NganhDT.TabIndex = 4;
            // 
            // cbbPg3LoaiBCCC
            // 
            this.cbbPg3LoaiBCCC.Location = new System.Drawing.Point(346, 56);
            this.cbbPg3LoaiBCCC.Name = "cbbPg3LoaiBCCC";
            this.cbbPg3LoaiBCCC.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3LoaiBCCC.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3LoaiBCCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3LoaiBCCC.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiBCCC", "Mã loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoai", "Tên loại")});
            this.cbbPg3LoaiBCCC.Properties.DisplayMember = "TenLoai";
            this.cbbPg3LoaiBCCC.Properties.NullText = "";
            this.cbbPg3LoaiBCCC.Properties.PopupSizeable = false;
            this.cbbPg3LoaiBCCC.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3LoaiBCCC.Properties.ValueMember = "MaLoaiBCCC";
            this.cbbPg3LoaiBCCC.Size = new System.Drawing.Size(146, 22);
            this.cbbPg3LoaiBCCC.TabIndex = 4;
            // 
            // cbbPg3ChuyenNganhDT
            // 
            this.cbbPg3ChuyenNganhDT.Location = new System.Drawing.Point(346, 84);
            this.cbbPg3ChuyenNganhDT.Name = "cbbPg3ChuyenNganhDT";
            this.cbbPg3ChuyenNganhDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3ChuyenNganhDT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3ChuyenNganhDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3ChuyenNganhDT.Properties.NullText = "";
            this.cbbPg3ChuyenNganhDT.Properties.PopupSizeable = false;
            this.cbbPg3ChuyenNganhDT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3ChuyenNganhDT.Size = new System.Drawing.Size(146, 22);
            this.cbbPg3ChuyenNganhDT.TabIndex = 4;
            // 
            // cbbPg3TrinhDoDT
            // 
            this.cbbPg3TrinhDoDT.Location = new System.Drawing.Point(595, 56);
            this.cbbPg3TrinhDoDT.Name = "cbbPg3TrinhDoDT";
            this.cbbPg3TrinhDoDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3TrinhDoDT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3TrinhDoDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3TrinhDoDT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiTrinhDo", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiTrinhDo", "Loại trình độ")});
            this.cbbPg3TrinhDoDT.Properties.DisplayMember = "TenLoaiTrinhDo";
            this.cbbPg3TrinhDoDT.Properties.NullText = "";
            this.cbbPg3TrinhDoDT.Properties.PopupSizeable = false;
            this.cbbPg3TrinhDoDT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3TrinhDoDT.Properties.ValueMember = "MaLoaiTrinhDo";
            this.cbbPg3TrinhDoDT.Size = new System.Drawing.Size(99, 22);
            this.cbbPg3TrinhDoDT.TabIndex = 4;
            // 
            // cbbPg3HeDT
            // 
            this.cbbPg3HeDT.Location = new System.Drawing.Point(766, 56);
            this.cbbPg3HeDT.Name = "cbbPg3HeDT";
            this.cbbPg3HeDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3HeDT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3HeDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3HeDT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaHeDaoTao", "Mã hệ đào tạo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenHeDaoTao", "Hệ đào tạo")});
            this.cbbPg3HeDT.Properties.DisplayMember = "TenHeDaoTao";
            this.cbbPg3HeDT.Properties.NullText = "";
            this.cbbPg3HeDT.Properties.PopupSizeable = false;
            this.cbbPg3HeDT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3HeDT.Properties.ValueMember = "MaHeDaoTao";
            this.cbbPg3HeDT.Size = new System.Drawing.Size(92, 22);
            this.cbbPg3HeDT.TabIndex = 4;
            // 
            // cbbPg3CoSoDT
            // 
            this.cbbPg3CoSoDT.Location = new System.Drawing.Point(595, 84);
            this.cbbPg3CoSoDT.Name = "cbbPg3CoSoDT";
            this.cbbPg3CoSoDT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg3CoSoDT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg3CoSoDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg3CoSoDT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbPg3CoSoDT.Properties.DisplayMember = "TenTruong";
            this.cbbPg3CoSoDT.Properties.NullText = "";
            this.cbbPg3CoSoDT.Properties.PopupSizeable = false;
            this.cbbPg3CoSoDT.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbPg3CoSoDT.Properties.ValueMember = "MaTruong";
            this.cbbPg3CoSoDT.Size = new System.Drawing.Size(263, 22);
            this.cbbPg3CoSoDT.TabIndex = 4;
            // 
            // ucNVBangCapChungChi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ucNVBangCapChungChi";
            this.Size = new System.Drawing.Size(1280, 510);
            this.Load += new System.EventHandler(this.ucNVBangCapChungChi_Load);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3BCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewBCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEditXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg3PhanLoaiBCCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg3NgayCapBCCC.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg3NgayCapBCCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3GhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3MaBCCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3ThoiGianDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg3ThoiGianHieuLuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3NganhDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3LoaiBCCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3ChuyenNganhDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3TrinhDoDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3HeDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg3CoSoDT.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XtraScrollableControl xtraScrollableControl1;
        private GridControl gvPg3BCCC;
        private GridView gridviewBCCC;
        private GridColumn gridColBCID;
        private GridColumn gridColMaBCCC;
        private GridColumn gridColLoaiBCCC;
        private GridColumn gridColChuyenmon;
        private GridColumn gridColTrinhDo;
        private GridColumn gridColChuyennganh;
        private GridColumn gridColCoSoDT;
        private GridColumn gridColTgianDT;
        private GridColumn gridColTgHieuluc;
        private GridColumn gridColNgayCapBCCC;
        private GridColumn gridColHDT;
        private GridColumn gridColGHICHUBCCC;
        private RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEditXoa;
        private GroupControl groupControl10;
        private RadioGroup rdbPg3PhanLoaiBCCC;
        private DateEdit dtPg3NgayCapBCCC;
        private MemoEdit txtPg3GhiChu;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
        private LabelControl labelControl58;
        private LabelControl labelControl60;
        private LabelControl labelControl57;
        private LabelControl labelControl74;
        private LabelControl lblMaBCCC;
        private LabelControl lblLoaiBCCC;
        private LabelControl labelControl61;
        private LabelControl labelControl50;
        private LabelControl labelControl59;
        private LabelControl labelControl51;
        private LabelControl labelControl53;
        private LabelControl labelControl54;
        private TextEdit txtPg3MaBCCC;
        private TextEdit txtPg3ThoiGianDT;
        private TextEdit txtPg3ThoiGianHieuLuc;
        private LookUpEdit cbbPg3NganhDT;
        private LookUpEdit cbbPg3LoaiBCCC;
        private LookUpEdit cbbPg3ChuyenNganhDT;
        private LookUpEdit cbbPg3TrinhDoDT;
        private LookUpEdit cbbPg3HeDT;
        private LookUpEdit cbbPg3CoSoDT;
        private SimpleButton btnPg3Sua;

    }
}
