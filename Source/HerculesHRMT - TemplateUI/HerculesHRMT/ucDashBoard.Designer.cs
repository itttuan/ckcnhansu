﻿namespace HerculesHRMT
{
    partial class ucDashBoard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDashBoard));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.navbarImageList = new System.Windows.Forms.ImageList();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(785, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl2.Controls.Add(this.simpleButton8);
            this.panelControl2.Controls.Add(this.simpleButton7);
            this.panelControl2.Controls.Add(this.simpleButton6);
            this.panelControl2.Controls.Add(this.simpleButton5);
            this.panelControl2.Controls.Add(this.simpleButton4);
            this.panelControl2.Controls.Add(this.simpleButton3);
            this.panelControl2.Controls.Add(this.simpleButton2);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Location = new System.Drawing.Point(33, 34);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(719, 451);
            this.panelControl2.TabIndex = 0;
            // 
            // navbarImageList
            // 
            this.navbarImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("navbarImageList.ImageStream")));
            this.navbarImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.navbarImageList.Images.SetKeyName(0, "dong.png");
            this.navbarImageList.Images.SetKeyName(1, "Inbox_16x16.png");
            this.navbarImageList.Images.SetKeyName(2, "Outbox_16x16.png");
            this.navbarImageList.Images.SetKeyName(3, "Drafts_16x16.png");
            this.navbarImageList.Images.SetKeyName(4, "Trash_16x16.png");
            this.navbarImageList.Images.SetKeyName(5, "Calendar_16x16.png");
            this.navbarImageList.Images.SetKeyName(6, "Tasks_16x16.png");
            this.navbarImageList.Images.SetKeyName(7, "login-16x16.png");
            this.navbarImageList.Images.SetKeyName(8, "re-delete-16x16.png");
            this.navbarImageList.Images.SetKeyName(9, "ghep-lop-16x16.png");
            this.navbarImageList.Images.SetKeyName(10, "ngung-hoc-16x16.png");
            this.navbarImageList.Images.SetKeyName(11, "chu-nhiem-16x16.png");
            this.navbarImageList.Images.SetKeyName(12, "tim-kiem-16x16.png");
            this.navbarImageList.Images.SetKeyName(13, "bao-cao-16x16.png");
            this.navbarImageList.Images.SetKeyName(14, "ke-hoach-16x16.png");
            this.navbarImageList.Images.SetKeyName(15, "audience-512.png");
            this.navbarImageList.Images.SetKeyName(16, "ChuyenGiaiDoan.png");
            this.navbarImageList.Images.SetKeyName(17, "Contract.png");
            this.navbarImageList.Images.SetKeyName(18, "Report.png");
            this.navbarImageList.Images.SetKeyName(19, "UngVien.png");
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Image = global::HerculesHRMT.Properties.Resources.NghiViec;
            this.simpleButton8.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton8.Location = new System.Drawing.Point(294, 300);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(130, 130);
            this.simpleButton8.TabIndex = 7;
            this.simpleButton8.Text = "Nghỉ việc";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Image = global::HerculesHRMT.Properties.Resources.ChuyenCongTac;
            this.simpleButton7.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton7.Location = new System.Drawing.Point(90, 300);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(130, 130);
            this.simpleButton7.TabIndex = 6;
            this.simpleButton7.Text = "Chuyển công tác";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Image = global::HerculesHRMT.Properties.Resources.Reminders;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton6.Location = new System.Drawing.Point(294, 158);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(130, 130);
            this.simpleButton6.TabIndex = 5;
            this.simpleButton6.Text = "Nhắc nhở công việc";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Image = global::HerculesHRMT.Properties.Resources.Contract;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton5.Location = new System.Drawing.Point(90, 158);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(130, 130);
            this.simpleButton5.TabIndex = 4;
            this.simpleButton5.Text = "Gia hạn hợp đồng";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Image = global::HerculesHRMT.Properties.Resources.Report;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton4.Location = new System.Drawing.Point(294, 16);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(130, 130);
            this.simpleButton4.TabIndex = 3;
            this.simpleButton4.Text = "Báo cáo thống kê";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Image = global::HerculesHRMT.Properties.Resources.ChuyenGiaiDoan;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton3.Location = new System.Drawing.Point(498, 16);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(130, 130);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Chuyển giai đoạn";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Image = global::HerculesHRMT.Properties.Resources.UngVien;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton2.Location = new System.Drawing.Point(498, 158);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(130, 130);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Danh sách ứng viên";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = global::HerculesHRMT.Properties.Resources.audience_512;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.simpleButton1.Location = new System.Drawing.Point(90, 16);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(130, 130);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Danh sách nhân viên";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // ucDashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucDashBoard";
            this.Size = new System.Drawing.Size(785, 518);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.ImageList navbarImageList;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
    }
}
