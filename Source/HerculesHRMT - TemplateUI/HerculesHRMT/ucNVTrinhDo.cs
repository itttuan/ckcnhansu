﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    public partial class ucNVTrinhDo : DevExpress.XtraEditors.XtraUserControl
    {
        Common.STATUS _statusCM;
        Common.STATUS _statusNN;
        Common.STATUS _statusTH;
        Common.STATUS _statusNV;
        Common.STATUS _statusLL;
        TrinhDoCTL trinhdoCTL = new TrinhDoCTL();
        TrinhDoDTO chuyenmonDTO = new TrinhDoDTO();
        TrinhDoDTO ngoainguDTO = new TrinhDoDTO();
        TrinhDoDTO tinhocDTO = new TrinhDoDTO();
        TrinhDoDTO nghiepvuDTO = new TrinhDoDTO();
        TrinhDoDTO lyluanctDTO = new TrinhDoDTO();
        public string strMaNV = "";

        #region CAU HINH THEM NHAN VIEN
        // false: opened by add new staff
        // true : opened by edit staff
        //private Boolean _openedByEditStaff = false;

        //
        //private string LUU_TAM = "Lưu Tạm";

        //public delegate void SendTrinhDo(TrinhDoDTO trinhDoDto);
        //public SendTrinhDo TrinhDoSender;

        #endregion

        public ucNVTrinhDo()
        {
            InitializeComponent();
        }
        public ucNVTrinhDo(Boolean openedByEditStaff = true)
        {
            InitializeComponent();

            //if (openedByEditStaff) return;

            //_openedByEditStaff = openedByEditStaff;
            //btnCMThem.Text = LUU_TAM;
            //btnCMXoa.Visible = false;
            //btnCMSua.Visible = false;
        }

        public void SetMaNV(string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            this.gridviewChuyenMon.ClearSelection();
            LoadDanhSach();
            _statusCM = status;
            _statusLL = status;
            _statusNN = status;
            _statusNV = status;
            _statusTH = status;
            chuyenmonDTO = new TrinhDoDTO();
            ngoainguDTO = new TrinhDoDTO();
            tinhocDTO = new TrinhDoDTO();
            nghiepvuDTO = new TrinhDoDTO();
            lyluanctDTO = new TrinhDoDTO();
            BindingChuyenMonData(chuyenmonDTO);
            BindingNgoaiNguData(ngoainguDTO);
            BindingTinHocData(tinhocDTO);
            BindingNghiepVuData(nghiepvuDTO);
            BindingLLCTData(lyluanctDTO);
            SetStatusChuyenMonComponents();
            SetStatusLyLuanCTComponents();
            SetStatusNghiepVuComponents();
            SetStatusNgoaiNguComponents();
            SetStatusTinHocComponents();
        }
        private bool GetDataOnChuyenMonComponentsAndCheck()
        {
            chuyenmonDTO.MaNV = strMaNV;
            chuyenmonDTO.LoaiDaoTao = "1";
            if (cbbCMTrinhDo.Text.Trim() =="") return false;
            if (cbbCMTrinhDo.EditValue != null)
            chuyenmonDTO.LoaiTrinhDoChuyenMon = cbbCMTrinhDo.EditValue.ToString();
            if (cbbCMTruong.Text.Trim() == "") return false;
            chuyenmonDTO.MaTruong = cbbCMTruong.EditValue as String;
            if (cbbCMHinhThuc.EditValue != null)
            chuyenmonDTO.HinhThucDaoTao = cbbCMHinhThuc.EditValue.ToString();
            chuyenmonDTO.ChuyenNganhDaoTao = txtCMChuyenNganh.EditValue as String;
            chuyenmonDTO.ThoiGianDaoTaoDen = txtCMTgianDen.EditValue as String;
            chuyenmonDTO.ThoiGianDaoTaoTu = txtCMTgianTu.EditValue as String;
            chuyenmonDTO.NgayCapBang = txtCMNgayCap.EditValue as String;
            chuyenmonDTO.NoiCapBang = txtCMNoiCap.EditValue as String;
            chuyenmonDTO.HocBongNhaNuoc = chbCMDienHocBong.Checked;
            chuyenmonDTO.TenDeAnHocBong = txtCMTenDeAn.EditValue as String;
            return true;
        }

        private bool GetDataOnNgoaiNguComponentsAndCheck()
        {
            ngoainguDTO.MaNV = strMaNV;
            ngoainguDTO.LoaiDaoTao = "4";
            if (txtNNTenNgoaiNgu.Text.Trim() == "") return false;
            ngoainguDTO.MaNgoaiNgu = txtNNTenNgoaiNgu.EditValue as String;
            if (cbbNNBangCap.EditValue != null)
            ngoainguDTO.LoaiTenBangCapNgoaiNgu = cbbNNBangCap.EditValue.ToString();
            ngoainguDTO.LoaiTrinhDoNgoaiNgu = txtNNTrinhDo.EditValue as String;
            ngoainguDTO.LoaiTrinhDoChuyenMon = cbbNNTrinhDo.EditValue as String;
            ngoainguDTO.MaTruong = cbbNNTruong.EditValue as String;
            if (cbbNNHinhThuc.EditValue !=null)
            ngoainguDTO.HinhThucDaoTao = cbbNNHinhThuc.EditValue.ToString();
            ngoainguDTO.ThoiGianDaoTaoDen = txtNNTgianden.EditValue as String;
            ngoainguDTO.ThoiGianDaoTaoTu = txtNNTgianTu.EditValue as String;
            ngoainguDTO.NgayCapBang = txtNNNgayCap.EditValue as String;
            ngoainguDTO.NoiCapBang = txtNNNoiCap.EditValue as String;
            ngoainguDTO.HocBongNhaNuoc = chbNNHocBong.Checked;
            ngoainguDTO.TenDeAnHocBong = txtNNDeAn.EditValue as String;
            return true;
        }

        private bool GetDataOnTinHocComponentsAndCheck()
        {
            tinhocDTO.MaNV = strMaNV;
            tinhocDTO.LoaiDaoTao = "5";
            if (cbbTHTrinhDo.Text.Trim() == "") return false;
            if (cbbTHTrinhDo.EditValue != null)
            tinhocDTO.LoaiTrinhDoTinHoc = cbbTHTrinhDo.EditValue.ToString();
            if (cbbTHTruong.Text.Trim() == "") return false;
            tinhocDTO.MaTruong = cbbTHTruong.EditValue as String;
            tinhocDTO.HinhThucDaoTao = cbbTHHinhThuc.EditValue as String;
            tinhocDTO.ThoiGianDaoTaoDen = txtTHTgianden.EditValue as String;
            tinhocDTO.ThoiGianDaoTaoTu = txtTHTgiantu.EditValue as String;
            tinhocDTO.NgayCapBang = txtTHNgayCap.EditValue as String;
            tinhocDTO.NoiCapBang = txtTHNoiCap.EditValue as String;
            tinhocDTO.HocBongNhaNuoc = chbTHHocBong.Checked;
            tinhocDTO.TenDeAnHocBong = txtTHDeAn.EditValue as String;
            return true;
        }

        private bool GetDataOnNghiepVuComponentsAndCheck()
        {
            nghiepvuDTO.MaNV = strMaNV;
            nghiepvuDTO.LoaiDaoTao = "2";
            if (txtNVTrinhDo.Text.Trim() == "") return false;
            nghiepvuDTO.LoaiNghiepVu = txtNVTrinhDo.EditValue as String;
            if (cbbNVTruong.Text.Trim() == "") return false;
            nghiepvuDTO.MaTruong = cbbNVTruong.EditValue as String;
            nghiepvuDTO.ChuyenNganhDaoTao = txtNVNganh.EditValue as String;
            if (cbbNVHinhThuc.EditValue !=null)
            nghiepvuDTO.HinhThucDaoTao = cbbNVHinhThuc.EditValue.ToString();
            nghiepvuDTO.ThoiGianDaoTaoDen = txtNVTgianden.EditValue as String;
            nghiepvuDTO.ThoiGianDaoTaoTu = txtNVTgiantu.EditValue as String;
            nghiepvuDTO.NgayCapBang = txtNVNgayCap.EditValue as String;
            nghiepvuDTO.HocBongNhaNuoc = chbNVHocBong.Checked;
            nghiepvuDTO.TenDeAnHocBong = txtNVDeAn.EditValue as String;
            nghiepvuDTO.LoaiNghiepVuSuPham = chbNVSP.Checked;
            return true;
        }

        private bool GetDataOnLLCTComponentsAndCheck()
        {
            lyluanctDTO.MaNV = strMaNV;
            lyluanctDTO.LoaiDaoTao = "3";
            if (cbbLLTrinhDo.Text.Trim() == "") return false;
            if (cbbLLTrinhDo.EditValue != null)
            lyluanctDTO.LoaiLyLuanChinhTri = cbbLLTrinhDo.EditValue.ToString();
            if (cbbLLTruong.Text.Trim() == "") return false;
            lyluanctDTO.MaTruong = cbbLLTruong.EditValue as String;
            if (cbbLLHinhThuc.EditValue != null)
            lyluanctDTO.HinhThucDaoTao = cbbLLHinhThuc.EditValue.ToString();
            lyluanctDTO.ChuyenNganhDaoTao = txtLLNganh.EditValue as String;
            lyluanctDTO.ThoiGianDaoTaoDen = txtLLTgianden.EditValue as String;
            lyluanctDTO.ThoiGianDaoTaoTu = txtLLTgiantu.EditValue as String;
            lyluanctDTO.NgayCapBang = txtLLNgayCap.EditValue as String;
            lyluanctDTO.HocBongNhaNuoc = chbLLHocBong.Checked;
            lyluanctDTO.TenDeAnHocBong = txtLLDeAn.EditValue as String;
            return true;
        }

        private void LoadDanhSach()
        {
            List<TrinhDoDTO> listTrinhDo = new List<TrinhDoDTO>();

            //chuyên môn
            listTrinhDo = trinhdoCTL.LayDanhSachTrinhDo(strMaNV, 0);
            gvPg3ChuyenMon.DataSource = listTrinhDo;

            //Ngoại ngữ
            listTrinhDo = trinhdoCTL.LayDanhSachTrinhDo(strMaNV, 4);
            gvPg3NgoaiNgu.DataSource = listTrinhDo;

            //Tin học
            listTrinhDo = trinhdoCTL.LayDanhSachTrinhDo(strMaNV, 5);
            gvPg3TinHoc.DataSource = listTrinhDo;

            //Nghiệp vụ
            listTrinhDo = trinhdoCTL.LayDanhSachTrinhDo(strMaNV, 2);
            gvPg3NghiepVu.DataSource = listTrinhDo;

            //Lý luận
            listTrinhDo = trinhdoCTL.LayDanhSachTrinhDo(strMaNV, 3);
            gvPg3LLCT.DataSource = listTrinhDo;
        }
        private void ucNVTrinhDo_Load(object sender, EventArgs e)
        {

            cbbCMTrinhDo.Properties.DataSource = (new LoaiBangChuyenMonCTL()).LayDanhSachLoaiBangChuyenMon();
             List<HinhThucDaoTaoDTO> lstHtdt = (new HinhThucDaoTaoCTL()).LayDanhSachHinhThucDaoTao();
             cbbCMHinhThuc.Properties.DataSource = lstHtdt;
             cbbNVHinhThuc.Properties.DataSource = lstHtdt;
             cbbTHHinhThuc.Properties.DataSource = lstHtdt;
             cbbNNHinhThuc.Properties.DataSource = lstHtdt;
             cbbLLHinhThuc.Properties.DataSource = lstHtdt;
             LoadDanhSachTruong();
             cbbNNBangCap.Properties.DataSource = (new LoaiBangNgoaiNguCTL()).LayDanhSachLoaiBangNgoaiNgu();
             cbbTHTrinhDo.Properties.DataSource = (new LoaiBangTinHocCTL()).LayDanhSachLoaiBangTinHoc();
             cbbLLTrinhDo.Properties.DataSource = (new LoaiBangLyLuanChinhTriCTL()).LayDanhSachLoaiBangLyLuanChinhTri();
             txtNNTenNgoaiNgu.Properties.DataSource = (new QuocGiaCTL()).LayDanhSachQuocGia();
        }

        public void LoadDanhSachTruong()
        {
            cbbCMTruong.Properties.DataSource = Program.listTruong;
            cbbNNTruong.Properties.DataSource = Program.listTruong;
            cbbTHTruong.Properties.DataSource = Program.listTruong;
            cbbLLTruong.Properties.DataSource = Program.listTruong;
            cbbNVTruong.Properties.DataSource = Program.listTruong;
        }

        private void txtNNTenNgoaiNgu_EditValueChanged(object sender, EventArgs e)
        {
            if (txtNNTenNgoaiNgu.EditValue != null)
            {
            cbbNNBangCap.Properties.DataSource = (new LoaiBangNgoaiNguCTL()).LayDanhSachLoaiBangNNTheoMaNN(txtNNTenNgoaiNgu.EditValue.ToString());
            }
        }

        private void BindingChuyenMonData(TrinhDoDTO cmdto)
        {
            if (cmdto == null) cmdto = new TrinhDoDTO();
            if (!string.IsNullOrEmpty(cmdto.LoaiTrinhDoChuyenMon))
                cbbCMTrinhDo.EditValue = int.Parse(cmdto.LoaiTrinhDoChuyenMon);
            else cbbCMTrinhDo.EditValue = null;
            cbbCMTruong.EditValue = cmdto.MaTruong;
            if (!string.IsNullOrEmpty(cmdto.HinhThucDaoTao))
                cbbCMHinhThuc.EditValue = int.Parse(cmdto.HinhThucDaoTao);
            else cbbCMHinhThuc.EditValue = null;
            txtCMChuyenNganh.EditValue = cmdto.ChuyenNganhDaoTao;
            txtCMTgianTu.EditValue = cmdto.ThoiGianDaoTaoTu;
            txtCMTgianDen.EditValue = cmdto.ThoiGianDaoTaoDen;
            txtCMNgayCap.EditValue = cmdto.NgayCapBang;
            txtCMNoiCap.EditValue = cmdto.NoiCapBang;
            chbCMDienHocBong.EditValue = cmdto.HocBongNhaNuoc;
            txtCMTenDeAn.EditValue = cmdto.TenDeAnHocBong;
        }

        private void SetChuyenMonReadOnly(bool read)
        {
            cbbCMTrinhDo.Properties.ReadOnly = read;
            cbbCMTruong.Properties.ReadOnly = read;
            cbbCMHinhThuc.Properties.ReadOnly = read;
            txtCMChuyenNganh.Properties.ReadOnly = read;
            txtCMTgianTu.Properties.ReadOnly = read;
            txtCMTgianDen.Properties.ReadOnly = read;
            txtCMNgayCap.Properties.ReadOnly = read;
            txtCMNoiCap.Properties.ReadOnly = read;
            chbCMDienHocBong.Properties.ReadOnly = read;
            txtCMTenDeAn.Properties.ReadOnly = read;
        }


        private void SetNgoaiNguReadOnly(bool read)
        {
            txtNNTenNgoaiNgu.Properties.ReadOnly = read;
            cbbNNBangCap.Properties.ReadOnly = read;
            txtNNTrinhDo.Properties.ReadOnly = read;
            cbbNNTrinhDo.Properties.ReadOnly = read;
            cbbNNTruong.Properties.ReadOnly = read;
            cbbNNHinhThuc.Properties.ReadOnly = read;
            txtNNNoiCap.Properties.ReadOnly = read;
            txtNNNgayCap.Properties.ReadOnly = read;
            txtNNTgianTu.Properties.ReadOnly = read;
            txtNNTgianden.Properties.ReadOnly = read;
            chbNNHocBong.Properties.ReadOnly = read;
            txtNNDeAn.Properties.ReadOnly = read;
        }

        private void BindingNgoaiNguData(TrinhDoDTO nndto)
        {
            if (nndto == null) nndto = new TrinhDoDTO();
            txtNNTenNgoaiNgu.EditValue = nndto.MaNgoaiNgu;
            if (!string.IsNullOrEmpty(nndto.LoaiTenBangCapNgoaiNgu))
                cbbNNBangCap.EditValue = int.Parse(nndto.LoaiTenBangCapNgoaiNgu);
            else cbbNNBangCap.EditValue = null;
            txtNNTrinhDo.EditValue = nndto.LoaiTrinhDoNgoaiNgu;
            cbbNNTrinhDo.EditValue = nndto.LoaiTrinhDoChuyenMon;
            cbbNNTruong.EditValue = nndto.MaTruong;
            if (!string.IsNullOrEmpty(nndto.HinhThucDaoTao))
                cbbNNHinhThuc.EditValue = int.Parse(nndto.HinhThucDaoTao);
            else cbbNNHinhThuc.EditValue = null;
            txtNNNoiCap.EditValue = nndto.NoiCapBang;
            txtNNNgayCap.EditValue = nndto.NgayCapBang;
            txtNNTgianTu.EditValue = nndto.ThoiGianDaoTaoTu;
            txtNNTgianden.EditValue = nndto.ThoiGianDaoTaoDen;
            chbNNHocBong.EditValue = nndto.HocBongNhaNuoc;
            txtNNDeAn.EditValue = nndto.TenDeAnHocBong;
        }

        private void BindingTinHocData(TrinhDoDTO thdto)
        {
            if (thdto == null) thdto = new TrinhDoDTO();
            if (!string.IsNullOrEmpty(thdto.LoaiTrinhDoTinHoc))
                cbbTHTrinhDo.EditValue = int.Parse(thdto.LoaiTrinhDoTinHoc);
            else cbbTHTrinhDo.EditValue = null;
            cbbTHTruong.EditValue = thdto.MaTruong;
            if (!string.IsNullOrEmpty(thdto.HinhThucDaoTao))
                cbbTHHinhThuc.EditValue = int.Parse(thdto.HinhThucDaoTao);
            else cbbTHHinhThuc.EditValue = null;
            txtTHNoiCap.EditValue = thdto.NoiCapBang;
            txtTHNgayCap.EditValue = thdto.NgayCapBang;
            txtTHTgiantu.EditValue = thdto.ThoiGianDaoTaoTu;
            txtTHTgianden.EditValue = thdto.ThoiGianDaoTaoDen;
            chbTHHocBong.EditValue = thdto.HocBongNhaNuoc;
            txtTHDeAn.EditValue = thdto.TenDeAnHocBong;
        }

        private void SetTinHocReadOnly(bool read)
        {
            cbbTHTrinhDo.Properties.ReadOnly = read;
            cbbTHTruong.Properties.ReadOnly = read;
            cbbTHHinhThuc.Properties.ReadOnly = read;
            txtTHNoiCap.Properties.ReadOnly = read;
            txtTHNgayCap.Properties.ReadOnly = read;
            txtTHTgiantu.Properties.ReadOnly = read;
            txtTHTgianden.Properties.ReadOnly = read;
            chbTHHocBong.Properties.ReadOnly = read;
            txtTHDeAn.Properties.ReadOnly = read;
        }
        private void BindingNghiepVuData(TrinhDoDTO nvdto)
        {
            if (nvdto == null) nvdto = new TrinhDoDTO();
            txtNVTrinhDo.EditValue = nvdto.LoaiNghiepVu;
            cbbNVTruong.EditValue = nvdto.MaTruong;
            if (!string.IsNullOrEmpty(nvdto.HinhThucDaoTao))
                cbbNVHinhThuc.EditValue = int.Parse(nvdto.HinhThucDaoTao);
            else cbbNVHinhThuc.EditValue = null;
            txtNVNganh.EditValue = nvdto.ChuyenNganhDaoTao;
            txtNVNgayCap.EditValue = nvdto.NgayCapBang;
            txtNVTgiantu.EditValue = nvdto.ThoiGianDaoTaoTu;
            txtNVTgianden.EditValue = nvdto.ThoiGianDaoTaoDen;
            chbNVHocBong.EditValue = nvdto.HocBongNhaNuoc;
            txtNVDeAn.EditValue = nvdto.TenDeAnHocBong;
            chbNVSP.Checked = nvdto.LoaiNghiepVuSuPham;
        }

        private void SetNghiepVuReadOnly(bool read)
        {
            txtNVTrinhDo.Properties.ReadOnly = read;
            cbbNVTruong.Properties.ReadOnly = read;
            cbbNVHinhThuc.Properties.ReadOnly = read;
            txtNVNganh.Properties.ReadOnly = read;
            txtNVNgayCap.Properties.ReadOnly = read;
            txtNVTgiantu.Properties.ReadOnly = read;
            txtNVTgianden.Properties.ReadOnly = read;
            chbNVHocBong.Properties.ReadOnly = read;
            txtNVDeAn.Properties.ReadOnly = read;
        }

        private void BindingLLCTData(TrinhDoDTO lldto)
        {
            if (lldto == null) lldto = new TrinhDoDTO();
            if (!string.IsNullOrEmpty(lldto.LoaiLyLuanChinhTri))
                cbbLLTrinhDo.EditValue = int.Parse(lldto.LoaiLyLuanChinhTri);
            else cbbLLTrinhDo.EditValue = null;
            cbbLLTruong.EditValue = lldto.MaTruong;
            if (!string.IsNullOrEmpty(lldto.HinhThucDaoTao))
                cbbLLHinhThuc.EditValue = int.Parse(lldto.HinhThucDaoTao);
            else cbbLLHinhThuc.EditValue = null;
            txtLLNganh.EditValue = lldto.ChuyenNganhDaoTao;
            txtLLNgayCap.EditValue = lldto.NgayCapBang;
            txtLLTgiantu.EditValue = lldto.ThoiGianDaoTaoTu;
            txtLLTgianden.EditValue = lldto.ThoiGianDaoTaoDen;
            chbLLHocBong.EditValue = lldto.HocBongNhaNuoc;
            txtLLDeAn.EditValue = lldto.TenDeAnHocBong;
        }

        private void SetLLCTReadOnly(bool read)
        {
            cbbLLTrinhDo.Properties.ReadOnly = read;
            cbbLLTruong.Properties.ReadOnly = read;
            cbbLLHinhThuc.Properties.ReadOnly = read;
            txtLLNganh.Properties.ReadOnly = read;
            txtLLNgayCap.Properties.ReadOnly = read;
            txtLLTgiantu.Properties.ReadOnly = read;
            txtLLTgianden.Properties.ReadOnly = read;
            chbLLHocBong.Properties.ReadOnly = read;
            txtLLDeAn.Properties.ReadOnly = read;
        }

        private void SetStatusChuyenMonComponents()
        {
            switch (_statusCM)
            {
                case Common.STATUS.NEW:
                    BindingChuyenMonData(new TrinhDoDTO());
                    btnCMSua.Enabled = false;
                    btnCMThem.Enabled = true;
                    SetChuyenMonReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetChuyenMonReadOnly(true);
                    if (chuyenmonDTO.Id > 0)
                    {
                        btnCMSua.Enabled = true;
                    }
                    else
                    {
                        btnCMSua.Enabled = false;
                    }
                    btnCMThem.Enabled = false;
                    btnCMTaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetChuyenMonReadOnly(false);
                    btnCMSua.Enabled = false;
                    btnCMThem.Enabled = true;
                    btnCMTaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetStatusNgoaiNguComponents()
        {
            switch (_statusNN)
            {
                case Common.STATUS.NEW:
                    BindingNgoaiNguData(new TrinhDoDTO());
                    btnNNSua.Enabled = false;
                    btnNNThem.Enabled = true;
                    SetNgoaiNguReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetNgoaiNguReadOnly(true);
                    if (ngoainguDTO.Id > 0)
                    {
                        btnNNSua.Enabled = true;
                    }
                    else
                    {
                        btnNNSua.Enabled = false;
                    }
                    btnNNThem.Enabled = false;
                    btnNNTaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetNgoaiNguReadOnly(false);
                    btnNNSua.Enabled = false;
                    btnNNThem.Enabled = true;
                    btnNNTaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetStatusTinHocComponents()
        {
            switch (_statusTH)
            {
                case Common.STATUS.NEW:
                    BindingTinHocData(new TrinhDoDTO());
                    btnTHSua.Enabled = false;
                    btnTHThem.Enabled = true;
                    SetTinHocReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetTinHocReadOnly(true);
                    if (tinhocDTO.Id > 0)
                    {
                        btnTHSua.Enabled = true;
                    }
                    else
                    {
                        btnTHSua.Enabled = false;
                    }
                    btnTHThem.Enabled = false;
                    btnTHTaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetTinHocReadOnly(false);
                    btnTHSua.Enabled = false;
                    btnTHThem.Enabled = true;
                    btnTHTaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetStatusNghiepVuComponents()
        {
            switch (_statusNV)
            {
                case Common.STATUS.NEW:
                    BindingNghiepVuData(new TrinhDoDTO());
                    btnNVSua.Enabled = false;
                    btnNVThem.Enabled = true;
                    SetNghiepVuReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetNghiepVuReadOnly(true);
                    if (chuyenmonDTO.Id > 0)
                    {
                        btnNVSua.Enabled = true;
                    }
                    else
                    {
                        btnNVSua.Enabled = false;
                    }
                    btnNVThem.Enabled = false;
                    btnNVTaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetNghiepVuReadOnly(false);
                    btnNVSua.Enabled = false;
                    btnNVThem.Enabled = true;
                    btnNVTaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetStatusLyLuanCTComponents()
        {
            switch (_statusLL)
            {
                case Common.STATUS.NEW:
                    BindingLLCTData(new TrinhDoDTO());
                    btnLLSua.Enabled = false;
                    btnLLThem.Enabled = true;
                    SetLLCTReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetLLCTReadOnly(true);
                    if (chuyenmonDTO.Id > 0)
                    {
                        btnLLSua.Enabled = true;
                    }
                    else
                    {
                        btnLLSua.Enabled = false;
                    }
                    btnLLThem.Enabled = false;
                    btnLLTaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetLLCTReadOnly(false);
                    btnLLSua.Enabled = false;
                    btnLLThem.Enabled = true;
                    btnLLTaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void btnLLLuu_Click(object sender, EventArgs e)
        {
            if (GetDataOnLLCTComponentsAndCheck())
            {
                // for add new staff
                //if (_openedByEditStaff)
                //{
                //    TrinhDoSender(lyluanctDTO);
                //    return;
                //}

                bool kq = false;
                if (_statusLL == Common.STATUS.EDIT)
                {
                    kq = trinhdoCTL.CapNhatTrinhDo(lyluanctDTO);

                }
                else if (_statusLL == Common.STATUS.NEW)
                {
                    kq = trinhdoCTL.LuuTrinhDo(lyluanctDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusLL = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusLyLuanCTComponents();

                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCMTaoMoi_Click(object sender, EventArgs e)
        {
            chuyenmonDTO = new TrinhDoDTO();
            _statusCM = Common.STATUS.NEW;
            SetStatusChuyenMonComponents();
        }

        private void btnNNTaoMoi_Click(object sender, EventArgs e)
        {
            ngoainguDTO = new TrinhDoDTO();
            _statusNN = Common.STATUS.NEW;
            SetStatusNgoaiNguComponents();
        }

        private void btnTHTaoMoi_Click(object sender, EventArgs e)
        {
            tinhocDTO = new TrinhDoDTO();
            _statusTH = Common.STATUS.NEW;
            SetStatusTinHocComponents();
        }

        private void btnNVTaoMoi_Click(object sender, EventArgs e)
        {
            nghiepvuDTO = new TrinhDoDTO();
            _statusNV = Common.STATUS.NEW;
            SetStatusNghiepVuComponents();
        }

        private void btnLLTaoMoi_Click(object sender, EventArgs e)
        {
            lyluanctDTO = new TrinhDoDTO();
            _statusLL = Common.STATUS.NEW;
            SetStatusLyLuanCTComponents();
        }

        private void btnLLSua_Click(object sender, EventArgs e)
        {
            if (lyluanctDTO.Id > 0)
            {
                _statusLL = Common.STATUS.EDIT;
                SetStatusLyLuanCTComponents();
            }
        }

        private void btnNVSua_Click(object sender, EventArgs e)
        {
            if (nghiepvuDTO.Id > 0)
            {
                _statusNV = Common.STATUS.EDIT;
                SetStatusNghiepVuComponents();
            }
        }

        private void btnTHSua_Click(object sender, EventArgs e)
        {
            if (tinhocDTO.Id > 0)
            {
                _statusTH = Common.STATUS.EDIT;
                SetStatusTinHocComponents();
            }
        }

        private void btnNNSua_Click(object sender, EventArgs e)
        {
            if (ngoainguDTO.Id > 0)
            {
                _statusNN = Common.STATUS.EDIT;
                SetStatusNgoaiNguComponents();
            }
        }

        private void btnCMSua_Click(object sender, EventArgs e)
        {
            if (chuyenmonDTO.Id > 0)
            {
                _statusCM = Common.STATUS.EDIT;
                SetStatusChuyenMonComponents();
            }
        }

        private void btnCMXoa_Click(object sender, EventArgs e)
        {
            if (chuyenmonDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (trinhdoCTL.XoaTrinhDo(chuyenmonDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnNNXoa_Click(object sender, EventArgs e)
        {
            if (ngoainguDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (trinhdoCTL.XoaTrinhDo(ngoainguDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnTHXoa_Click(object sender, EventArgs e)
        {
            if (tinhocDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (trinhdoCTL.XoaTrinhDo(tinhocDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnNVXoa_Click(object sender, EventArgs e)
        {
            if (nghiepvuDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (trinhdoCTL.XoaTrinhDo(nghiepvuDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnLLXoa_Click(object sender, EventArgs e)
        {
            if (lyluanctDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (trinhdoCTL.XoaTrinhDo(lyluanctDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnNVThem_Click(object sender, EventArgs e)
        {
            if (GetDataOnNghiepVuComponentsAndCheck())
            {
                bool kq = false;
                if (_statusNV == Common.STATUS.EDIT)
                {
                    kq = trinhdoCTL.CapNhatTrinhDo(nghiepvuDTO);

                }
                else if (_statusNV == Common.STATUS.NEW)
                {
                    kq = trinhdoCTL.LuuTrinhDo(nghiepvuDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusNV = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusNghiepVuComponents();

                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTHThem_Click(object sender, EventArgs e)
        {
            if (GetDataOnTinHocComponentsAndCheck())
            {
                bool kq = false;
                if (_statusTH== Common.STATUS.EDIT)
                {
                    kq = trinhdoCTL.CapNhatTrinhDo(tinhocDTO);

                }
                else if (_statusTH == Common.STATUS.NEW)
                {
                    kq = trinhdoCTL.LuuTrinhDo(tinhocDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusTH = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusTinHocComponents();

                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNNThem_Click(object sender, EventArgs e)
        {
            if (GetDataOnNgoaiNguComponentsAndCheck())
            {
                bool kq = false;
                if (_statusNN == Common.STATUS.EDIT)
                {
                    kq = trinhdoCTL.CapNhatTrinhDo(ngoainguDTO);

                }
                else if (_statusNN == Common.STATUS.NEW)
                {
                    kq = trinhdoCTL.LuuTrinhDo(ngoainguDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusNN = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusNgoaiNguComponents();

                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCMThem_Click(object sender, EventArgs e)
        {
            if (GetDataOnChuyenMonComponentsAndCheck())
            {
                bool kq = false;
                if (_statusCM == Common.STATUS.EDIT)
                {
                    kq = trinhdoCTL.CapNhatTrinhDo(chuyenmonDTO);

                }
                else if (_statusCM == Common.STATUS.NEW)
                {
                    kq = trinhdoCTL.LuuTrinhDo(chuyenmonDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusCM = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusChuyenMonComponents();

                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gridviewLLCT_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                lyluanctDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as TrinhDoDTO;
                if (lyluanctDTO == null) lyluanctDTO = new TrinhDoDTO();
                BindingLLCTData(lyluanctDTO);
                _statusLL = Common.STATUS.VIEW;
                SetStatusLyLuanCTComponents();
            }
        }

        private void gridviewNghiepVu_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                nghiepvuDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as TrinhDoDTO;
                if (nghiepvuDTO == null) nghiepvuDTO = new TrinhDoDTO();
                BindingNghiepVuData(nghiepvuDTO);
                _statusNV = Common.STATUS.VIEW;
                SetStatusNghiepVuComponents();
            }
        }

        private void gridviewTinHoc_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                tinhocDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as TrinhDoDTO;
                if (tinhocDTO == null) tinhocDTO = new TrinhDoDTO();
                BindingTinHocData(tinhocDTO);
                _statusTH = Common.STATUS.VIEW;
                SetStatusTinHocComponents();
            }
        }

        private void gridviewNgoaiNgu_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                ngoainguDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as TrinhDoDTO;
                if (ngoainguDTO == null) ngoainguDTO = new TrinhDoDTO();
                BindingNgoaiNguData(ngoainguDTO);
                _statusNN = Common.STATUS.VIEW;
                SetStatusNgoaiNguComponents();
            }
        }

        private void gridviewChuyenMon_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                chuyenmonDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as TrinhDoDTO;
                if (chuyenmonDTO == null) chuyenmonDTO = new TrinhDoDTO();
                BindingChuyenMonData(chuyenmonDTO);
                _statusCM = Common.STATUS.VIEW;
                SetStatusChuyenMonComponents();
            }
        }

        private void gridviewChuyenMon_RowClick(object sender, RowClickEventArgs e)
        {
            chuyenmonDTO = ((GridView)sender).GetRow(e.RowHandle) as TrinhDoDTO;
            BindingChuyenMonData(chuyenmonDTO);
            _statusCM = Common.STATUS.VIEW;
            SetStatusChuyenMonComponents();
        }

        private void gridviewNgoaiNgu_RowClick(object sender, RowClickEventArgs e)
        {
            ngoainguDTO = ((GridView)sender).GetRow(e.RowHandle) as TrinhDoDTO;
            BindingNgoaiNguData(ngoainguDTO);
            _statusNN = Common.STATUS.VIEW;
            SetStatusNgoaiNguComponents();
        }

        private void gridviewTinHoc_RowClick(object sender, RowClickEventArgs e)
        {
            tinhocDTO = ((GridView)sender).GetRow(e.RowHandle) as TrinhDoDTO;
            BindingTinHocData(tinhocDTO);
            _statusTH = Common.STATUS.VIEW;
            SetStatusTinHocComponents();
        }

        private void gridviewNghiepVu_RowClick(object sender, RowClickEventArgs e)
        {
            nghiepvuDTO = ((GridView)sender).GetRow(e.RowHandle) as TrinhDoDTO;
            BindingNghiepVuData(nghiepvuDTO);
            _statusNV = Common.STATUS.VIEW;
            SetStatusNghiepVuComponents();
        }

        private void gridviewLLCT_RowClick(object sender, RowClickEventArgs e)
        {
            lyluanctDTO = ((GridView)sender).GetRow(e.RowHandle) as TrinhDoDTO;
            BindingLLCTData(lyluanctDTO);
            _statusLL = Common.STATUS.VIEW;
            SetStatusLyLuanCTComponents();
        }

        private void btnCMThemTruong_Click(object sender, EventArgs e)
        {
            frmThemTruong truong = new frmThemTruong(this);
            truong.ShowDialog();
        }

        private void btnNNTruong_Click(object sender, EventArgs e)
        {
            frmThemTruong truong = new frmThemTruong(this);
            truong.ShowDialog();
        }

        private void btnTHTruong_Click(object sender, EventArgs e)
        {
            frmThemTruong truong = new frmThemTruong(this);
            truong.ShowDialog();
        }

        private void btnNVTruong_Click(object sender, EventArgs e)
        {
            frmThemTruong truong = new frmThemTruong(this);
            truong.ShowDialog();
        }

        private void btnLLTruong_Click(object sender, EventArgs e)
        {
            frmThemTruong truong = new frmThemTruong(this);
            truong.ShowDialog();
        }

        public string CheckValue()
        {
            var result = "Vui lòng nhập các thông tin bắt buộc (*) trong tab: ";

            if (!GetDataOnNgoaiNguComponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Ngoại Ngữ");
            }

            if (!GetDataOnChuyenMonComponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Chuyên Môn");
            }

            if (!GetDataOnLLCTComponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Lý Luận Chính Trị");
            }

            if (!GetDataOnNghiepVuComponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Nghiệp Vụ");
            }

            if (!GetDataOnTinHocComponentsAndCheck())
            {
                result = string.Format("{0}{1}", result, "Tin Học");
            }

            return result;
        } 
    }
}
