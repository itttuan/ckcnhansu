﻿using DevExpress.XtraEditors;
using HerculesDTO;
using System.Collections.Generic;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class frmNhanVien : XtraForm
    {

        string strMaNV= "";
        int current = 0;
        int totalPage = 0;
        List<NhanVienDTO> listPhienBan;
        NhanVienCTL nhanvienCTL = new NhanVienCTL();
        ucNVSoLuocLyLich uc = new ucNVSoLuocLyLich();
        public frmNhanVien()
        {
            InitializeComponent();
        }
        public frmNhanVien(string maNV)
        {
            InitializeComponent();
            strMaNV = maNV;
            listPhienBan = nhanvienCTL.LayCacPhienBanNhanVien(strMaNV);
            totalPage = listPhienBan.Count;
            if (totalPage > 0)
            {
                current = totalPage - 1;
            }
            LoadPage();
        }

        private void LoadPage()
        {
            if (current == 0)
            {
                btnBack.Visible = false;
            }
            else
            {
                btnBack.Visible = true;
            }

            if (current == (totalPage - 1))
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            lbPageNum.Text = (current + 1) + "/" + totalPage;
            uc.SetNhanVien(listPhienBan[current], Common.STATUS.VIEW);
            uc.SetInvisibleButton();
            uc.Dock = System.Windows.Forms.DockStyle.Fill;
            pnUC.Controls.Add(uc);
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            current--;
            LoadPage();
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            current++;
            LoadPage();
        }
    
    }
}