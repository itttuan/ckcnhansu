﻿namespace HerculesHRMT
{
    partial class rptLyLich
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLyLich));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrCheckBox1 = new DevExpress.XtraReports.UI.XRCheckBox();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbChieuCao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbCanNang = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNhomMau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbDanhHieu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayCapCMND = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbSoTruongCongTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.HoTen = new DevExpress.XtraReports.UI.CalculatedField();
            this.ChuoiGioiTinh = new DevExpress.XtraReports.UI.CalculatedField();
            this.DiaChiThuongTru = new DevExpress.XtraReports.UI.CalculatedField();
            this.ChuoiNgaySinh = new DevExpress.XtraReports.UI.CalculatedField();
            this.paNgaySinh = new DevExpress.XtraReports.Parameters.Parameter();
            this.QuaTrinhNgayTruocTuyenDung = new DevExpress.XtraReports.UI.CalculatedField();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbTuNhanXet = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport8 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader9 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.imgHinhAnh = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbDanToc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbGioiTinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNoiSinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenKS = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgaySinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgheTruocTD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbHKTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNOHN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTrinhDoChuyenMonCaoNhat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayVaoDCSVN = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayThamGiaCTXH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayChinhThucVaoDCSVN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenNgach = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbMaNgach = new DevExpress.XtraReports.UI.XRLabel();
            this.lbChucVu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayHuongLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbCongViecChinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbDVTD = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayTD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayNhapNgu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNgayXuatNgu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbQuanHam = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.xrLabel18,
            this.xrLabel22,
            this.xrLabel28,
            this.xrLabel29,
            this.lbCongViecChinh,
            this.xrLabel26,
            this.lbChucVu,
            this.lbMaNgach,
            this.xrLabel21,
            this.xrLabel19,
            this.lbNgayHuongLuong,
            this.xrLabel25,
            this.xrLabel51,
            this.lbNgayNhapNgu,
            this.xrLabel53,
            this.lbQuanHam,
            this.xrLabel54,
            this.lbNgayXuatNgu,
            this.xrLabel24,
            this.lbNgayTD,
            this.lbDVTD,
            this.xrLabel39,
            this.xrLabel17,
            this.xrLabel23,
            this.xrLabel16,
            this.xrLabel41,
            this.xrLabel43,
            this.xrLabel37,
            this.xrLabel30,
            this.xrLabel32,
            this.xrLabel40,
            this.xrLabel36,
            this.lbHKTT,
            this.xrLabel15,
            this.lbNgheTruocTD,
            this.lbNOHN,
            this.xrLabel13,
            this.xrLabel10,
            this.xrLabel42,
            this.lbNgayChinhThucVaoDCSVN,
            this.xrLabel31,
            this.lbTenNgach,
            this.xrLabel47,
            this.xrLabel45,
            this.xrLabel48,
            this.lbTrinhDoChuyenMonCaoNhat,
            this.xrLabel38,
            this.xrLabel33,
            this.lbNgayThamGiaCTXH,
            this.lbNgayVaoDCSVN,
            this.xrLabel34,
            this.xrLabel12,
            this.lbGioiTinh,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel3,
            this.lbTenKS,
            this.lbNgaySinh,
            this.lbNoiSinh,
            this.xrLabel4,
            this.xrLabel2,
            this.xrLabel7,
            this.xrLabel76,
            this.imgHinhAnh,
            this.lbTenKhac,
            this.xrLabel11,
            this.xrLabel8,
            this.xrLabel9,
            this.lbDanToc,
            this.xrCheckBox1,
            this.xrLabel60,
            this.xrLabel61,
            this.lbChieuCao,
            this.xrLabel62,
            this.xrLabel66,
            this.lbCanNang,
            this.lbNhomMau,
            this.xrLabel64,
            this.xrLabel69,
            this.xrLabel68,
            this.lbDanhHieu,
            this.xrLabel56,
            this.lbNgayCapCMND,
            this.xrLabel73,
            this.xrLabel74,
            this.lbSoTruongCongTac,
            this.xrLabel58,
            this.xrLabel72});
            this.Detail.HeightF = 748.524F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCheckBox1
            // 
            this.xrCheckBox1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrCheckBox1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("CheckState", null, "lylichsoluoc.GiaDinhChinhSach")});
            this.xrCheckBox1.LocationFloat = new DevExpress.Utils.PointFloat(342.7714F, 677.7728F);
            this.xrCheckBox1.Name = "xrCheckBox1";
            this.xrCheckBox1.SizeF = new System.Drawing.SizeF(226.0824F, 23F);
            this.xrCheckBox1.StylePriority.UseBorders = false;
            this.xrCheckBox1.Text = "Là con gia đình chính sách";
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(11.24961F, 656.8562F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(149.8657F, 20.91663F);
            this.xrLabel60.StylePriority.UseBorders = false;
            this.xrLabel60.Text = "21) Tình trạng sức khỏe:";
            // 
            // xrLabel61
            // 
            this.xrLabel61.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TinhTrangSucKhoe")});
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 656.8562F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(96.27985F, 20.91663F);
            this.xrLabel61.StylePriority.UseBorderDashStyle = false;
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.Text = "xrLabel61";
            // 
            // lbChieuCao
            // 
            this.lbChieuCao.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbChieuCao.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbChieuCao.LocationFloat = new DevExpress.Utils.PointFloat(339.6661F, 656.8562F);
            this.lbChieuCao.Name = "lbChieuCao";
            this.lbChieuCao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbChieuCao.SizeF = new System.Drawing.SizeF(91.66675F, 20.91663F);
            this.lbChieuCao.StylePriority.UseBorderDashStyle = false;
            this.lbChieuCao.StylePriority.UseBorders = false;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(261.5414F, 656.8562F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(76.06241F, 20.91663F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.Text = "Chiều cao";
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(568.8538F, 656.8562F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(79.18756F, 20.91663F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.Text = "Nhóm máu:";
            // 
            // lbCanNang
            // 
            this.lbCanNang.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbCanNang.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbCanNang.LocationFloat = new DevExpress.Utils.PointFloat(506.3542F, 656.8562F);
            this.lbCanNang.Name = "lbCanNang";
            this.lbCanNang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbCanNang.SizeF = new System.Drawing.SizeF(61.45807F, 20.91663F);
            this.lbCanNang.StylePriority.UseBorderDashStyle = false;
            this.lbCanNang.StylePriority.UseBorders = false;
            // 
            // lbNhomMau
            // 
            this.lbNhomMau.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNhomMau.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNhomMau.LocationFloat = new DevExpress.Utils.PointFloat(648.0413F, 656.8562F);
            this.lbNhomMau.Name = "lbNhomMau";
            this.lbNhomMau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNhomMau.SizeF = new System.Drawing.SizeF(85.37457F, 20.91663F);
            this.lbNhomMau.StylePriority.UseBorderDashStyle = false;
            this.lbNhomMau.StylePriority.UseBorders = false;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(431.3326F, 656.8562F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(74.95856F, 20.91663F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.Text = "Cân nặng:";
            // 
            // xrLabel69
            // 
            this.xrLabel69.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.ThuongBinhHang")});
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 681.7728F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(160.8837F, 20.91663F);
            this.xrLabel69.StylePriority.UseBorderDashStyle = false;
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.Text = "xrLabel69";
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(9.457779F, 681.7728F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(151.6575F, 20.91663F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.Text = "22) Là thương binh hạng:";
            // 
            // lbDanhHieu
            // 
            this.lbDanhHieu.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbDanhHieu.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbDanhHieu.LocationFloat = new DevExpress.Utils.PointFloat(257.3951F, 607.0229F);
            this.lbDanhHieu.Name = "lbDanhHieu";
            this.lbDanhHieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbDanhHieu.SizeF = new System.Drawing.SizeF(477.0627F, 20.91663F);
            this.lbDanhHieu.StylePriority.UseBorderDashStyle = false;
            this.lbDanhHieu.StylePriority.UseBorders = false;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(9.457779F, 607.0229F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(247.9374F, 20.91663F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.Text = "19) Danh hiệu được phong tặng cao nhất:";
            // 
            // lbNgayCapCMND
            // 
            this.lbNgayCapCMND.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayCapCMND.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayCapCMND.LocationFloat = new DevExpress.Utils.PointFloat(443.4071F, 706.6893F);
            this.lbNgayCapCMND.Name = "lbNgayCapCMND";
            this.lbNgayCapCMND.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayCapCMND.SizeF = new System.Drawing.SizeF(291.0509F, 20.91656F);
            this.lbNgayCapCMND.StylePriority.UseBorderDashStyle = false;
            this.lbNgayCapCMND.StylePriority.UseBorders = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.CMND")});
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(191.7681F, 706.6893F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(180.1896F, 20.91656F);
            this.xrLabel73.StylePriority.UseBorderDashStyle = false;
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.Text = "xrLabel73";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(371.9576F, 706.6893F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(71.44952F, 20.91656F);
            this.xrLabel74.StylePriority.UseBorders = false;
            this.xrLabel74.Text = "Ngày cấp:";
            // 
            // lbSoTruongCongTac
            // 
            this.lbSoTruongCongTac.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbSoTruongCongTac.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbSoTruongCongTac.LocationFloat = new DevExpress.Utils.PointFloat(153.2081F, 635.9396F);
            this.lbSoTruongCongTac.Name = "lbSoTruongCongTac";
            this.lbSoTruongCongTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbSoTruongCongTac.SizeF = new System.Drawing.SizeF(580.2078F, 20.91663F);
            this.lbSoTruongCongTac.StylePriority.UseBorderDashStyle = false;
            this.lbSoTruongCongTac.StylePriority.UseBorders = false;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(9.457779F, 631.9395F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(143.7503F, 20.91656F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.Text = "20) Sở trường công tác:";
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(9.457779F, 706.6893F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(182.3104F, 20.91656F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.Text = "23) Số chứng minh nhân dân:";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 45.41874F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 28.125F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1});
            this.DetailReport.DataMember = "truockhituyendung";
            this.DetailReport.DataSource = this.bindingSource1;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(7.00029F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(727.8541F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UsePadding = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "truockhituyendung.ChuoiNgayCongTac")});
            this.xrTableCell1.KeepTogether = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.Weight = 1.2314599732407054D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "truockhituyendung.ChuoiCongViec")});
            this.xrTableCell2.KeepTogether = true;
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "xrTableCell2";
            this.xrTableCell2.Weight = 6.0470808287612474D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel78,
            this.xrLabel79,
            this.xrLabel1,
            this.xrLabel77});
            this.GroupHeader1.HeightF = 191.1371F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel78.KeepTogether = true;
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 118.9052F);
            this.xrLabel78.Multiline = true;
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(123.146F, 72.23187F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "Từ tháng, năm\r\nđến tháng, năm";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel79.KeepTogether = true;
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(130.1463F, 118.9051F);
            this.xrLabel79.Multiline = true;
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(604.7082F, 72.23187F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.StylePriority.UsePadding = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = resources.GetString("xrLabel79.Text");
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 15.75003F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(720.9583F, 34.50006F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "II. ĐẶC ĐIỂM LỊCH SỬ BẢN THÂN\t";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(8.792114F, 74F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(722.1661F, 23.00001F);
            this.xrLabel77.StylePriority.UseBorders = false;
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.Text = "II.A - Trước khi được tuyển dụng";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(HerculesDTO.ThongTinNhanVienDTO);
            // 
            // HoTen
            // 
            this.HoTen.Expression = "Concat([lylichsoluoc.Ho],\' \',[lylichsoluoc.Ten])";
            this.HoTen.Name = "HoTen";
            // 
            // ChuoiGioiTinh
            // 
            this.ChuoiGioiTinh.Expression = "Iif([lylichsoluoc.GioiTinh]=True,\'Nữ\' ,\'Nam\')";
            this.ChuoiGioiTinh.Name = "ChuoiGioiTinh";
            // 
            // DiaChiThuongTru
            // 
            this.DiaChiThuongTru.Expression = "concat([lylichsoluoc.DiaChiHienTaiDiaChi], \', \',[lylichsoluoc.DiaChiHienTaiMaQuan" +
    "Huyen],\', \',[lylichsoluoc.DiaChiHienTaiMaTinhThanh])";
            this.DiaChiThuongTru.Name = "DiaChiThuongTru";
            // 
            // ChuoiNgaySinh
            // 
            this.ChuoiNgaySinh.Expression = "GetDate([lylichsoluoc.NgaySinh])";
            this.ChuoiNgaySinh.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
            this.ChuoiNgaySinh.Name = "ChuoiNgaySinh";
            // 
            // paNgaySinh
            // 
            this.paNgaySinh.Name = "paNgaySinh";
            this.paNgaySinh.Type = typeof(System.DateTime);
            this.paNgaySinh.Visible = false;
            // 
            // QuaTrinhNgayTruocTuyenDung
            // 
            this.QuaTrinhNgayTruocTuyenDung.Name = "QuaTrinhNgayTruocTuyenDung";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader2});
            this.DetailReport1.DataMember = "khituyendung";
            this.DetailReport1.DataSource = this.bindingSource1;
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail2.HeightF = 25F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.KeepTogether = true;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(7.000286F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(727.8542F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "khituyendung.ChuoiNgayCongTac")});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.Weight = 0.8986088814747405D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "khituyendung.ChuoiCongViec")});
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 3.953752373329499D;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel84,
            this.xrLabel83,
            this.xrLabel82});
            this.GroupHeader2.HeightF = 135.3699F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel84.KeepTogether = true;
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(141.3532F, 62.1884F);
            this.xrLabel84.Multiline = true;
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(593.0627F, 73.18152F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UsePadding = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = resources.GetString("xrLabel84.Text");
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel83
            // 
            this.xrLabel83.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel83.KeepTogether = true;
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(6.561779F, 62.1884F);
            this.xrLabel83.Multiline = true;
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(134.7913F, 73.18152F);
            this.xrLabel83.StylePriority.UseBorders = false;
            this.xrLabel83.StylePriority.UseTextAlignment = false;
            this.xrLabel83.Text = "Từ tháng, năm\r\nđến tháng, năm";
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 18.00001F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(271.8961F, 23F);
            this.xrLabel82.StylePriority.UseBorders = false;
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.Text = "II.B - Khi được tuyển dụng";
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3});
            this.DetailReport2.DataMember = "congtacxahoi";
            this.DetailReport2.DataSource = this.bindingSource1;
            this.DetailReport2.Level = 2;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail3.HeightF = 25F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.KeepTogether = true;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(7.00029F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(723.9584F, 25F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UsePadding = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "congtacxahoi.ChuoiNgayCongTac")});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.Weight = 0.82097347714875768D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "congtacxahoi.ChuoiCongViec")});
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 4.0054160816085052D;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel90,
            this.xrLabel88,
            this.xrLabel87});
            this.GroupHeader3.HeightF = 155.5534F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrLabel90
            // 
            this.xrLabel90.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel90.KeepTogether = true;
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(130.1463F, 74.37172F);
            this.xrLabel90.Multiline = true;
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(601.8542F, 81.18164F);
            this.xrLabel90.StylePriority.UseBorders = false;
            this.xrLabel90.StylePriority.UsePadding = false;
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.Text = resources.GetString("xrLabel90.Text");
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel88.KeepTogether = true;
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 74.37172F);
            this.xrLabel88.Multiline = true;
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(123.146F, 81.18164F);
            this.xrLabel88.StylePriority.UseBorders = false;
            this.xrLabel88.StylePriority.UseTextAlignment = false;
            this.xrLabel88.Text = "Từ tháng, năm\r\nđến tháng, năm";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel87.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 22.49996F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(727.8542F, 34.5F);
            this.xrLabel87.StylePriority.UseBorders = false;
            this.xrLabel87.StylePriority.UseFont = false;
            this.xrLabel87.StylePriority.UseTextAlignment = false;
            this.xrLabel87.Text = "III. THAM GIA TỔ CHỨC CHÍNH TRỊ - XÃ HỘI, HỘI NGHỀ NGHIỆP...\t";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader4});
            this.DetailReport3.DataMember = "trinhdochuyenmon";
            this.DetailReport3.DataSource = this.bindingSource1;
            this.DetailReport3.Level = 3;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail4.HeightF = 25F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.KeepTogether = true;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(723.9579F, 25F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UsePadding = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "trinhdochuyenmon.Truong.TenTruong")});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 1.84767822265625D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "trinhdochuyenmon.ChuoiChuyenNganhDaoTao")});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 1.7185449218749997D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "trinhdochuyenmon.ChuoiThoiGianDaoTao")});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 1.5629135462122075D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "trinhdochuyenmon.HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao")});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 0.87377744165745985D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 1.2366650301967388D;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel95,
            this.xrLabel96,
            this.xrLabel97,
            this.xrLabel92,
            this.xrLabel93,
            this.xrLabel94});
            this.GroupHeader4.HeightF = 163.3506F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel95.KeepTogether = true;
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(363.6227F, 92.16602F);
            this.xrLabel95.Multiline = true;
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(156.2912F, 71.18457F);
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "Từ tháng, năm\r\nĐến tháng, năm";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel96.KeepTogether = true;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(519.9139F, 92.16602F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(87.37775F, 71.18445F);
            this.xrLabel96.StylePriority.UseBorders = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "Hình thức đào tạo, \r\nbồi dưỡng";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel97.KeepTogether = true;
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(607.2917F, 92.16601F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(123.6665F, 71.18458F);
            this.xrLabel97.StylePriority.UseBorders = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = "Văn bằng, chứng chỉ";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 13.13375F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(723.9579F, 62.78222F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = "IV. QUÁ TRÌNH ĐÀO TẠO, BỒI DƯỠNG VỀ CHUYÊN MÔN, NGHIỆP VỤ, LÝ LUẬN CHÍNH TRỊ, NGO" +
    "ẠI NGỮ TIN HỌC";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel93.KeepTogether = true;
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 92.16601F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(184.7678F, 71.18458F);
            this.xrLabel93.StylePriority.UseBorders = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.Text = "Tên trường hoặc cơ sở đào tạo, bồi dưỡng";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel94.KeepTogether = true;
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(191.7681F, 92.16602F);
            this.xrLabel94.Multiline = true;
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(171.8545F, 71.18457F);
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "Chuyên ngành\r\nđào tạo, bồi dưỡng";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader5});
            this.DetailReport4.DataMember = "khenthuong";
            this.DetailReport4.DataSource = this.bindingSource1;
            this.DetailReport4.Level = 4;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail5.HeightF = 25F;
            this.Detail5.Name = "Detail5";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.KeepTogether = true;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(722F, 25F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UsePadding = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "khenthuong.ChuoiThoiGianHieuLuc")});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.Weight = 0.51602770003876786D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "khenthuong.NoiDung")});
            this.xrTableCell13.Multiline = true;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "xrTableCell13";
            this.xrTableCell13.Weight = 1.6567766320212638D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "khenthuong.CapQuyetDinh")});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.Weight = 0.82650569393109663D;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel105,
            this.xrLabel106,
            this.xrLabel103,
            this.xrLabel104});
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // xrLabel105
            // 
            this.xrLabel105.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel105.KeepTogether = true;
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(134.2192F, 62.33521F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(399.7914F, 37.66479F);
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "Nội dung và hình thức khen thưởng";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel106
            // 
            this.xrLabel106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel106.KeepTogether = true;
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(534.0106F, 62.33521F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(197.9164F, 37.66479F);
            this.xrLabel106.StylePriority.UseBorders = false;
            this.xrLabel106.StylePriority.UseTextAlignment = false;
            this.xrLabel106.Text = "Cấp quyết định";
            this.xrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(10.07299F, 17.00003F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(720.8852F, 27.18994F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "V. KHEN THƯỞNG";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel104.KeepTogether = true;
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(10.073F, 62.33521F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(124.1461F, 37.66479F);
            this.xrLabel104.StylePriority.UseBorders = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.Text = "Tháng, năm";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport5
            // 
            this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader6});
            this.DetailReport5.DataMember = "kyluat";
            this.DetailReport5.DataSource = this.bindingSource1;
            this.DetailReport5.Level = 5;
            this.DetailReport5.Name = "DetailReport5";
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail6.HeightF = 25F;
            this.Detail6.Name = "Detail6";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.KeepTogether = true;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(7.000282F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(723.9579F, 25F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UsePadding = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "kyluat.ChuoiThoiGianHieuLuc")});
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "xrTableCell15";
            this.xrTableCell15.Weight = 0.52848916144050073D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "kyluat.NoiDung")});
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 1.656776568633674D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "kyluat.CapQuyetDinh")});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.Weight = 0.82217784244257741D;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel111,
            this.xrLabel115,
            this.xrLabel112,
            this.xrLabel110});
            this.GroupHeader6.HeightF = 95.6319F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel111.KeepTogether = true;
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(7.000288F, 57.96711F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(127.219F, 37.66479F);
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "Tháng, năm";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel115.KeepTogether = true;
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(534.0106F, 57.96711F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(196.9476F, 37.66479F);
            this.xrLabel115.StylePriority.UseBorders = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "Cấp quyết định";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel112.KeepTogether = true;
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(134.2192F, 57.96711F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(399.7913F, 37.66479F);
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.StylePriority.UseTextAlignment = false;
            this.xrLabel112.Text = "Lý do và hình thức kỷ luật";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel110
            // 
            this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel110.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(9.99999F, 12.52213F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(722.0007F, 27.18994F);
            this.xrLabel110.StylePriority.UseBorders = false;
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.Text = "VI. KỶ LUẬT";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport6
            // 
            this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupHeader7});
            this.DetailReport6.DataMember = "quanhegiadinh";
            this.DetailReport6.DataSource = this.bindingSource1;
            this.DetailReport6.Level = 6;
            this.DetailReport6.Name = "DetailReport6";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.Detail7.HeightF = 25F;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(718.4582F, 25F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UsePadding = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "quanhegiadinh.MoiQuanHe")});
            this.xrTableCell18.KeepTogether = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 0.44519593362671916D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "quanhegiadinh.ThôngTinThanNhan")});
            this.xrTableCell19.KeepTogether = true;
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "xrTableCell19";
            this.xrTableCell19.Weight = 2.028703548557675D;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel117,
            this.xrLabel119,
            this.xrLabel118});
            this.GroupHeader7.HeightF = 144.8766F;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // xrLabel117
            // 
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 14.54201F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(718.4583F, 34.5F);
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "VII. QUAN HỆ GIA ĐÌNH";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel119.KeepTogether = true;
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(141.7917F, 63.69489F);
            this.xrLabel119.Multiline = true;
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(589.1665F, 81.18166F);
            this.xrLabel119.StylePriority.UseBorders = false;
            this.xrLabel119.StylePriority.UsePadding = false;
            this.xrLabel119.StylePriority.UseTextAlignment = false;
            this.xrLabel119.Text = resources.GetString("xrLabel119.Text");
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel118.KeepTogether = true;
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 63.69489F);
            this.xrLabel118.Multiline = true;
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(129.2917F, 81.18166F);
            this.xrLabel118.StylePriority.UseBorders = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = "Mối quan hệ";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable8
            // 
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(18.68779F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(713.3129F, 35.41666F);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbTuNhanXet});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // lbTuNhanXet
            // 
            this.lbTuNhanXet.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbTuNhanXet.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "tunhanxet")});
            this.lbTuNhanXet.KeepTogether = true;
            this.lbTuNhanXet.Multiline = true;
            this.lbTuNhanXet.Name = "lbTuNhanXet";
            this.lbTuNhanXet.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100F);
            this.lbTuNhanXet.StylePriority.UseBorders = false;
            this.lbTuNhanXet.StylePriority.UsePadding = false;
            this.lbTuNhanXet.Weight = 1D;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(18.68779F, 14.21765F);
            this.xrLabel122.Multiline = true;
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(713.3127F, 62.78235F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.StylePriority.UseTextAlignment = false;
            this.xrLabel122.Text = "VIII. TỰ NHẬN XÉT\r\n(Về phẩm chất chính trị, đạo đức, lối sống, ý thức kỷ luật, nă" +
    "ng lực và sở trường công tác)";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 23F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(718.4582F, 23F);
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport8
            // 
            this.DetailReport8.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader9});
            this.DetailReport8.Level = 7;
            this.DetailReport8.Name = "DetailReport8";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.Detail9.HeightF = 35.41666F;
            this.Detail9.Name = "Detail9";
            // 
            // GroupHeader9
            // 
            this.GroupHeader9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel122});
            this.GroupHeader9.HeightF = 85.125F;
            this.GroupHeader9.Name = "GroupHeader9";
            // 
            // xrLabel76
            // 
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(11.11514F, 22.00001F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(723.9579F, 23F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "I. SƠ LƯỢC LÝ LỊCH";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // imgHinhAnh
            // 
            this.imgHinhAnh.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.imgHinhAnh.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Image")));
            this.imgHinhAnh.LocationFloat = new DevExpress.Utils.PointFloat(22.80264F, 66.07517F);
            this.imgHinhAnh.Name = "imgHinhAnh";
            this.imgHinhAnh.SizeF = new System.Drawing.SizeF(123.146F, 152.9167F);
            this.imgHinhAnh.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.imgHinhAnh.StylePriority.UseBorders = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 69.15872F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(142.1136F, 20.91667F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.Text = "1) Họ và tên khai sinh:";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 147.1586F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(84.82208F, 20.91666F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.Text = "4) Nơi sinh:";
            // 
            // lbTenKhac
            // 
            this.lbTenKhac.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbTenKhac.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbTenKhac.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TenGoiKhac")});
            this.lbTenKhac.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbTenKhac.LocationFloat = new DevExpress.Utils.PointFloat(263.1989F, 95.15868F);
            this.lbTenKhac.Name = "lbTenKhac";
            this.lbTenKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenKhac.SizeF = new System.Drawing.SizeF(472.9163F, 20.91666F);
            this.lbTenKhac.StylePriority.UseBorderDashStyle = false;
            this.lbTenKhac.StylePriority.UseBorders = false;
            this.lbTenKhac.StylePriority.UseFont = false;
            this.lbTenKhac.Text = "lbTenKhac";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 198.0755F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(84.82202F, 20.91666F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.Text = "6) Dân tộc:";
            // 
            // lbDanToc
            // 
            this.lbDanToc.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbDanToc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbDanToc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TenDanToc")});
            this.lbDanToc.LocationFloat = new DevExpress.Utils.PointFloat(245.9373F, 198.0753F);
            this.lbDanToc.Name = "lbDanToc";
            this.lbDanToc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbDanToc.SizeF = new System.Drawing.SizeF(197.4698F, 20.91666F);
            this.lbDanToc.StylePriority.UseBorderDashStyle = false;
            this.lbDanToc.StylePriority.UseBorders = false;
            this.lbDanToc.Text = "lbDanToc";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(443.4071F, 198.0753F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(76.50681F, 20.91666F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.Text = "7) Tôn giáo:";
            // 
            // xrLabel8
            // 
            this.xrLabel8.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.QueQuan")});
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(245.9373F, 173.1586F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(490.178F, 20.91666F);
            this.xrLabel8.StylePriority.UseBorderDashStyle = false;
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.Text = "xrLabel8";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 173.1586F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(84.82205F, 20.91666F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.Text = "5) Quê quán:";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(442.365F, 121.1588F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(63.92612F, 20.91666F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.Text = "Giới tính:";
            // 
            // xrLabel12
            // 
            this.xrLabel12.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TenTonGiao")});
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(519.914F, 198.0755F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(216.2011F, 20.91666F);
            this.xrLabel12.StylePriority.UseBorderDashStyle = false;
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.Text = "xrLabel12";
            // 
            // lbGioiTinh
            // 
            this.lbGioiTinh.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbGioiTinh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbGioiTinh.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ChuoiGioiTinh")});
            this.lbGioiTinh.LocationFloat = new DevExpress.Utils.PointFloat(506.3543F, 121.1588F);
            this.lbGioiTinh.Name = "lbGioiTinh";
            this.lbGioiTinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbGioiTinh.SizeF = new System.Drawing.SizeF(228.7188F, 20.91666F);
            this.lbGioiTinh.StylePriority.UseBorderDashStyle = false;
            this.lbGioiTinh.StylePriority.UseBorders = false;
            this.lbGioiTinh.Text = "lbGioiTinh";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 121.1588F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(84.8221F, 20.91666F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.Text = "3) Sinh ngày:";
            // 
            // lbNoiSinh
            // 
            this.lbNoiSinh.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNoiSinh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNoiSinh.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.NoiSinh")});
            this.lbNoiSinh.LocationFloat = new DevExpress.Utils.PointFloat(245.9373F, 147.1586F);
            this.lbNoiSinh.Name = "lbNoiSinh";
            this.lbNoiSinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNoiSinh.SizeF = new System.Drawing.SizeF(490.178F, 20.91666F);
            this.lbNoiSinh.StylePriority.UseBorderDashStyle = false;
            this.lbNoiSinh.StylePriority.UseBorders = false;
            this.lbNoiSinh.Text = "lbNoiSinh";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(161.1153F, 95.15868F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(102.0833F, 20.91666F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.Text = "2) Tên gọi khác:";
            // 
            // lbTenKS
            // 
            this.lbTenKS.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbTenKS.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbTenKS.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "HoTen")});
            this.lbTenKS.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbTenKS.LocationFloat = new DevExpress.Utils.PointFloat(303.2289F, 69.15872F);
            this.lbTenKS.Name = "lbTenKS";
            this.lbTenKS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenKS.SizeF = new System.Drawing.SizeF(431.8444F, 20.91665F);
            this.lbTenKS.StylePriority.UseBorderDashStyle = false;
            this.lbTenKS.StylePriority.UseBorders = false;
            this.lbTenKS.StylePriority.UseFont = false;
            this.lbTenKS.Text = "[HoTen]";
            // 
            // lbNgaySinh
            // 
            this.lbNgaySinh.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgaySinh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgaySinh.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lbNgaySinh.LocationFloat = new DevExpress.Utils.PointFloat(245.9373F, 121.1588F);
            this.lbNgaySinh.Name = "lbNgaySinh";
            this.lbNgaySinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgaySinh.SizeF = new System.Drawing.SizeF(187.0528F, 20.91666F);
            this.lbNgaySinh.StylePriority.UseBorderDashStyle = false;
            this.lbNgaySinh.StylePriority.UseBorders = false;
            this.lbNgaySinh.StylePriority.UseFont = false;
            // 
            // lbNgheTruocTD
            // 
            this.lbNgheTruocTD.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgheTruocTD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgheTruocTD.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.NgheNghiepTruocTuyenDung")});
            this.lbNgheTruocTD.LocationFloat = new DevExpress.Utils.PointFloat(235.5211F, 276.7464F);
            this.lbNgheTruocTD.Name = "lbNgheTruocTD";
            this.lbNgheTruocTD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgheTruocTD.SizeF = new System.Drawing.SizeF(499.9789F, 20.91669F);
            this.lbNgheTruocTD.StylePriority.UseBorderDashStyle = false;
            this.lbNgheTruocTD.StylePriority.UseBorders = false;
            this.lbNgheTruocTD.Text = "lbNgheTruocTD";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(10.50022F, 276.7464F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(225.0208F, 20.91669F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.Text = "10) Nghề nghiệp khi được tuyển dung:";
            // 
            // lbHKTT
            // 
            this.lbHKTT.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbHKTT.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbHKTT.LocationFloat = new DevExpress.Utils.PointFloat(214.7076F, 226.9129F);
            this.lbHKTT.Name = "lbHKTT";
            this.lbHKTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbHKTT.SizeF = new System.Drawing.SizeF(521.4076F, 20.91667F);
            this.lbHKTT.StylePriority.UseBorderDashStyle = false;
            this.lbHKTT.StylePriority.UseBorders = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(10.50022F, 226.9129F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(204.2074F, 20.91667F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.Text = "8) Nơi đăng ký hộ khẩu thường trú:";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(10.50022F, 251.8297F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(119.6461F, 20.91664F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.Text = "9) Nơi ở hiện nay:";
            // 
            // lbNOHN
            // 
            this.lbNOHN.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNOHN.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNOHN.LocationFloat = new DevExpress.Utils.PointFloat(130.1463F, 251.8297F);
            this.lbNOHN.Name = "lbNOHN";
            this.lbNOHN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNOHN.SizeF = new System.Drawing.SizeF(605.3537F, 20.91664F);
            this.lbNOHN.StylePriority.UseBorderDashStyle = false;
            this.lbNOHN.StylePriority.UseBorders = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 479.2127F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(139.6041F, 20.91666F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.Text = "15.3- Lý luận chính trị:";
            // 
            // xrLabel37
            // 
            this.xrLabel37.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(150.1038F, 479.2127F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(222.8956F, 20.91663F);
            this.xrLabel37.StylePriority.UseBorderDashStyle = false;
            this.xrLabel37.StylePriority.UseBorders = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 504.1294F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(104.396F, 20.91669F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.Text = "15.5- Ngoại ngữ:";
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(372.9995F, 504.1294F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(89.58371F, 20.91663F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.Text = "15.6- Tin học:";
            // 
            // xrLabel40
            // 
            this.xrLabel40.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(114.8958F, 504.1294F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(258.1038F, 20.91663F);
            this.xrLabel40.StylePriority.UseBorderDashStyle = false;
            this.xrLabel40.StylePriority.UseBorders = false;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 429.3794F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(204.2079F, 20.91663F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.Text = "15.1- Trình độ giáo dục phổ thông:";
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(621.9791F, 404.4626F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(44.81256F, 20.91666F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.Text = "Khác";
            // 
            // xrLabel33
            // 
            this.xrLabel33.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TrinhDoVanHoa")});
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(214.7076F, 429.3794F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(519.7504F, 20.91663F);
            this.xrLabel33.StylePriority.UseBorderDashStyle = false;
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.Text = "xrLabel33";
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(372.9996F, 479.2127F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(146.9143F, 20.91663F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.Text = "15.4- Quản lý nhà nước:";
            // 
            // lbTrinhDoChuyenMonCaoNhat
            // 
            this.lbTrinhDoChuyenMonCaoNhat.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbTrinhDoChuyenMonCaoNhat.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbTrinhDoChuyenMonCaoNhat.LocationFloat = new DevExpress.Utils.PointFloat(235.5211F, 454.2959F);
            this.lbTrinhDoChuyenMonCaoNhat.Name = "lbTrinhDoChuyenMonCaoNhat";
            this.lbTrinhDoChuyenMonCaoNhat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTrinhDoChuyenMonCaoNhat.SizeF = new System.Drawing.SizeF(498.9369F, 20.91663F);
            this.lbTrinhDoChuyenMonCaoNhat.StylePriority.UseBorderDashStyle = false;
            this.lbTrinhDoChuyenMonCaoNhat.StylePriority.UseBorders = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 454.2959F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(225.0213F, 20.91663F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.Text = "15.2- Trình độ chuyên môn cao nhất:";
            // 
            // lbNgayVaoDCSVN
            // 
            this.lbNgayVaoDCSVN.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayVaoDCSVN.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayVaoDCSVN.LocationFloat = new DevExpress.Utils.PointFloat(245.9373F, 529.0459F);
            this.lbNgayVaoDCSVN.Name = "lbNgayVaoDCSVN";
            this.lbNgayVaoDCSVN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayVaoDCSVN.SizeF = new System.Drawing.SizeF(168.7288F, 20.91663F);
            this.lbNgayVaoDCSVN.StylePriority.UseBorderDashStyle = false;
            this.lbNgayVaoDCSVN.StylePriority.UseBorders = false;
            // 
            // lbNgayThamGiaCTXH
            // 
            this.lbNgayThamGiaCTXH.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayThamGiaCTXH.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayThamGiaCTXH.LocationFloat = new DevExpress.Utils.PointFloat(263.1989F, 554.9625F);
            this.lbNgayThamGiaCTXH.Name = "lbNgayThamGiaCTXH";
            this.lbNgayThamGiaCTXH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayThamGiaCTXH.SizeF = new System.Drawing.SizeF(471.259F, 20.91669F);
            this.lbNgayThamGiaCTXH.StylePriority.UseBorderDashStyle = false;
            this.lbNgayThamGiaCTXH.StylePriority.UseBorders = false;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 554.9625F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(252.6988F, 20.91669F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.Text = "17) Ngày tham gia tổ chức chính trị -xã hội:";
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(666.7916F, 404.4626F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(67.66632F, 20.91666F);
            this.xrLabel31.StylePriority.UseBorderDashStyle = false;
            this.xrLabel31.StylePriority.UseBorders = false;
            // 
            // lbNgayChinhThucVaoDCSVN
            // 
            this.lbNgayChinhThucVaoDCSVN.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayChinhThucVaoDCSVN.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayChinhThucVaoDCSVN.LocationFloat = new DevExpress.Utils.PointFloat(519.9139F, 529.0459F);
            this.lbNgayChinhThucVaoDCSVN.Name = "lbNgayChinhThucVaoDCSVN";
            this.lbNgayChinhThucVaoDCSVN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayChinhThucVaoDCSVN.SizeF = new System.Drawing.SizeF(215.5859F, 20.91663F);
            this.lbNgayChinhThucVaoDCSVN.StylePriority.UseBorderDashStyle = false;
            this.lbNgayChinhThucVaoDCSVN.StylePriority.UseBorders = false;
            // 
            // xrLabel42
            // 
            this.xrLabel42.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(462.5832F, 504.1294F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(272.9166F, 20.91663F);
            this.xrLabel42.StylePriority.UseBorderDashStyle = false;
            this.xrLabel42.StylePriority.UseBorders = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(414.6662F, 529.0459F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(105.2477F, 20.91663F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.Text = "Ngày chính thức:";
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 529.0459F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(235.4375F, 20.91669F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.Text = "16) Ngày vào Đảng cộng sản Việt Nam:";
            // 
            // lbTenNgach
            // 
            this.lbTenNgach.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbTenNgach.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbTenNgach.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.TenNgach")});
            this.lbTenNgach.LocationFloat = new DevExpress.Utils.PointFloat(145.9487F, 379.546F);
            this.lbTenNgach.Name = "lbTenNgach";
            this.lbTenNgach.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenNgach.SizeF = new System.Drawing.SizeF(286.4258F, 20.91666F);
            this.lbTenNgach.StylePriority.UseBorderDashStyle = false;
            this.lbTenNgach.StylePriority.UseBorders = false;
            this.lbTenNgach.Text = "lbTenNgach";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(432.3745F, 379.546F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(80.22928F, 20.91666F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.Text = "Mã ngạch:";
            // 
            // lbMaNgach
            // 
            this.lbMaNgach.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbMaNgach.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbMaNgach.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.MaNgach")});
            this.lbMaNgach.LocationFloat = new DevExpress.Utils.PointFloat(512.6041F, 379.546F);
            this.lbMaNgach.Name = "lbMaNgach";
            this.lbMaNgach.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbMaNgach.SizeF = new System.Drawing.SizeF(221.8538F, 20.91666F);
            this.lbMaNgach.StylePriority.UseBorderDashStyle = false;
            this.lbMaNgach.StylePriority.UseBorders = false;
            this.lbMaNgach.Text = "lbMaNgach";
            // 
            // lbChucVu
            // 
            this.lbChucVu.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbChucVu.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbChucVu.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.ChucVu")});
            this.lbChucVu.LocationFloat = new DevExpress.Utils.PointFloat(134.2192F, 328.7127F);
            this.lbChucVu.Name = "lbChucVu";
            this.lbChucVu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbChucVu.SizeF = new System.Drawing.SizeF(601.2802F, 20.91666F);
            this.lbChucVu.StylePriority.UseBorderDashStyle = false;
            this.lbChucVu.StylePriority.UseBorders = false;
            this.lbChucVu.Text = "lbChucVu";
            // 
            // xrLabel25
            // 
            this.xrLabel25.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.HeSoLuong")});
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(205.3123F, 404.4626F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(53.12483F, 20.91666F);
            this.xrLabel25.StylePriority.UseBorderDashStyle = false;
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.Text = "xrLabel25";
            // 
            // lbNgayHuongLuong
            // 
            this.lbNgayHuongLuong.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayHuongLuong.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayHuongLuong.LocationFloat = new DevExpress.Utils.PointFloat(337.6038F, 404.4626F);
            this.lbNgayHuongLuong.Name = "lbNgayHuongLuong";
            this.lbNgayHuongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayHuongLuong.SizeF = new System.Drawing.SizeF(94.77103F, 20.91666F);
            this.lbNgayHuongLuong.StylePriority.UseBorderDashStyle = false;
            this.lbNgayHuongLuong.StylePriority.UseBorders = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 379.546F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(135.4489F, 20.91666F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.Text = "14) Ngạch công chức:";
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(258.4373F, 404.4626F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(79.16653F, 20.91666F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.Text = "Ngày hưởng:";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 404.4626F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(69.81259F, 20.91666F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.Text = "Bậc lương:";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(296.9373F, 303.7961F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(134.3955F, 20.91666F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.Text = "Cơ quan tuyển dụng:";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 354.6294F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(181.2683F, 20.91663F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.Text = "13) Công việc chính được giao:";
            // 
            // lbCongViecChinh
            // 
            this.lbCongViecChinh.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbCongViecChinh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbCongViecChinh.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.NgheNghiepTuyenDung")});
            this.lbCongViecChinh.LocationFloat = new DevExpress.Utils.PointFloat(191.7681F, 354.6294F);
            this.lbCongViecChinh.Name = "lbCongViecChinh";
            this.lbCongViecChinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbCongViecChinh.SizeF = new System.Drawing.SizeF(543.7315F, 20.91663F);
            this.lbCongViecChinh.StylePriority.UseBorderDashStyle = false;
            this.lbCongViecChinh.StylePriority.UseBorders = false;
            this.lbCongViecChinh.Text = "lbCongViecChinh";
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(547.9996F, 404.4626F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(73.97925F, 20.91666F);
            this.xrLabel29.StylePriority.UseBorderDashStyle = false;
            this.xrLabel29.StylePriority.UseBorders = false;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(442.7916F, 404.4626F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(105.2079F, 20.91666F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.Text = "Phụ cấp chức vụ";
            // 
            // xrLabel39
            // 
            this.xrLabel39.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.QuanLyNhaNuoc")});
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(519.914F, 479.2127F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(215.5858F, 20.91663F);
            this.xrLabel39.StylePriority.UseBorderDashStyle = false;
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.Text = "xrLabel39";
            // 
            // lbDVTD
            // 
            this.lbDVTD.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbDVTD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbDVTD.LocationFloat = new DevExpress.Utils.PointFloat(431.3326F, 303.7961F);
            this.lbDVTD.Name = "lbDVTD";
            this.lbDVTD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbDVTD.SizeF = new System.Drawing.SizeF(303.1255F, 20.91666F);
            this.lbDVTD.StylePriority.UseBorderDashStyle = false;
            this.lbDVTD.StylePriority.UseBorders = false;
            this.lbDVTD.Text = "Trường Cao Đẳng Kỹ Thuật Cao Thắng";
            // 
            // lbNgayTD
            // 
            this.lbNgayTD.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayTD.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayTD.LocationFloat = new DevExpress.Utils.PointFloat(145.9487F, 303.7961F);
            this.lbNgayTD.Name = "lbNgayTD";
            this.lbNgayTD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayTD.SizeF = new System.Drawing.SizeF(149.9888F, 20.91666F);
            this.lbNgayTD.StylePriority.UseBorderDashStyle = false;
            this.lbNgayTD.StylePriority.UseBorders = false;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 303.7961F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(135.4489F, 20.91663F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.Text = "11) Ngày Tuyển dụng:";
            // 
            // xrLabel23
            // 
            this.xrLabel23.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.BacNhanVien")});
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(80.31238F, 404.4626F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(72.89572F, 20.91666F);
            this.xrLabel23.StylePriority.UseBorderDashStyle = false;
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.Text = "xrLabel23";
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 328.7127F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(123.7195F, 20.91666F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.Text = "12) Chức vụ hiện tại:";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(153.2081F, 404.4626F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(52.10426F, 20.91666F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.Text = "Hệ số";
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(10.49979F, 579.8794F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(123.7195F, 20.91663F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.Text = "18) Ngày nhập ngũ:";
            // 
            // lbNgayNhapNgu
            // 
            this.lbNgayNhapNgu.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayNhapNgu.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayNhapNgu.LocationFloat = new DevExpress.Utils.PointFloat(134.2193F, 579.8794F);
            this.lbNgayNhapNgu.Name = "lbNgayNhapNgu";
            this.lbNgayNhapNgu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayNhapNgu.SizeF = new System.Drawing.SizeF(124.2177F, 20.91663F);
            this.lbNgayNhapNgu.StylePriority.UseBorderDashStyle = false;
            this.lbNgayNhapNgu.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(262.5833F, 579.8794F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(101.0392F, 20.91663F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.Text = "Ngày xuất ngũ:";
            // 
            // lbNgayXuatNgu
            // 
            this.lbNgayXuatNgu.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbNgayXuatNgu.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNgayXuatNgu.LocationFloat = new DevExpress.Utils.PointFloat(363.6227F, 579.8794F);
            this.lbNgayXuatNgu.Name = "lbNgayXuatNgu";
            this.lbNgayXuatNgu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNgayXuatNgu.SizeF = new System.Drawing.SizeF(111.4605F, 20.91663F);
            this.lbNgayXuatNgu.StylePriority.UseBorderDashStyle = false;
            this.lbNgayXuatNgu.StylePriority.UseBorders = false;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(475.0832F, 579.8794F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(120.8542F, 20.91663F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.Text = "Quân hàm cao nhất:";
            // 
            // lbQuanHam
            // 
            this.lbQuanHam.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.lbQuanHam.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbQuanHam.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "lylichsoluoc.QuanHamCaoNhat")});
            this.lbQuanHam.LocationFloat = new DevExpress.Utils.PointFloat(595.9374F, 579.8794F);
            this.lbQuanHam.Name = "lbQuanHam";
            this.lbQuanHam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbQuanHam.SizeF = new System.Drawing.SizeF(138.5206F, 20.91663F);
            this.lbQuanHam.StylePriority.UseBorderDashStyle = false;
            this.lbQuanHam.StylePriority.UseBorders = false;
            this.lbQuanHam.Text = "lbQuanHam";
            // 
            // rptLyLich
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.DetailReport1,
            this.DetailReport2,
            this.DetailReport3,
            this.DetailReport4,
            this.DetailReport5,
            this.DetailReport6,
            this.PageFooter,
            this.DetailReport8});
            this.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.HoTen,
            this.ChuoiGioiTinh,
            this.DiaChiThuongTru,
            this.ChuoiNgaySinh,
            this.QuaTrinhNgayTruocTuyenDung});
            this.DataSource = this.bindingSource1;
            this.Margins = new System.Drawing.Printing.Margins(47, 61, 45, 28);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.paNgaySinh});
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private System.Windows.Forms.BindingSource bindingSource1;
        //private DevExpress.XtraReports.UI.XRLabel lbNgayXuatNgu;
        //private DevExpress.XtraReports.UI.XRLabel lbNgayXuatNgu;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.CalculatedField HoTen;
        private DevExpress.XtraReports.UI.CalculatedField ChuoiGioiTinh;
        private DevExpress.XtraReports.UI.CalculatedField DiaChiThuongTru;
        private DevExpress.XtraReports.UI.CalculatedField ChuoiNgaySinh;
        private DevExpress.XtraReports.Parameters.Parameter paNgaySinh;
        private DevExpress.XtraReports.UI.CalculatedField QuaTrinhNgayTruocTuyenDung;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport6;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lbTuNhanXet;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport8;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader9;
        private DevExpress.XtraReports.UI.XRCheckBox xrCheckBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel lbChieuCao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel lbCanNang;
        private DevExpress.XtraReports.UI.XRLabel lbNhomMau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel lbDanhHieu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel lbNgayCapCMND;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel lbSoTruongCongTac;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel lbCongViecChinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lbChucVu;
        private DevExpress.XtraReports.UI.XRLabel lbMaNgach;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel lbNgayHuongLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel lbNgayNhapNgu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel lbQuanHam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel lbNgayXuatNgu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lbNgayTD;
        private DevExpress.XtraReports.UI.XRLabel lbDVTD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel lbHKTT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lbNgheTruocTD;
        private DevExpress.XtraReports.UI.XRLabel lbNOHN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel lbNgayChinhThucVaoDCSVN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lbTenNgach;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lbTrinhDoChuyenMonCaoNhat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lbNgayThamGiaCTXH;
        private DevExpress.XtraReports.UI.XRLabel lbNgayVaoDCSVN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lbGioiTinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lbTenKS;
        private DevExpress.XtraReports.UI.XRLabel lbNgaySinh;
        private DevExpress.XtraReports.UI.XRLabel lbNoiSinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRPictureBox imgHinhAnh;
        private DevExpress.XtraReports.UI.XRLabel lbTenKhac;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lbDanToc;
    }
}
