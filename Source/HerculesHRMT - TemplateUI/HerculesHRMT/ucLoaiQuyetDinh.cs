﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucLoaiQuyetDinh : XtraUserControl
    {
        LoaiQuyetDinhCTL lqdCTL = new LoaiQuyetDinhCTL();
        List<LoaiQuyetDinhDTO> listLoaiQD = new List<LoaiQuyetDinhDTO>();
        LoaiQuyetDinhDTO lqdSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucLoaiQuyetDinh()
        {
            InitializeComponent();
        }
        
        private void LoadData()
        {
            listLoaiQD = lqdCTL.GetAll();

            gcLoaiQD.DataSource = listLoaiQD;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(LoaiQuyetDinhDTO lqdDTO)
        {
            if (lqdDTO != null)
            {
                txtLoaiQD.Text = lqdDTO.TenLoaiQD;                
                meGhiChu.Text = lqdDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtLoaiQD.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    txtLoaiQD.Text = string.Empty;                    
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtLoaiQD.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(lqdSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtLoaiQD.Properties.ReadOnly = true;                    
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(lqdSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtLoaiQD.Text == string.Empty)
                return false;

            return true;
        }

        private LoaiQuyetDinhDTO GetData(LoaiQuyetDinhDTO lqdDTO)
        {
            string maLQD = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexLQD = lqdCTL.GetMaxMaLoaiQuyetDinh();
                if (sLastIndexLQD.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexLQD = int.Parse(sLastIndexLQD);
                    maLQD += (nLastIndexLQD + 1).ToString().PadLeft(2,'0');
                }
                else
                    maLQD += "01";

                lqdDTO.NgayHieuLuc = DateTime.Now;
                lqdDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maLQD = lqdSelect.MaLoaiQD;
                lqdDTO.NgayHieuLuc = lqdSelect.NgayHieuLuc;
                lqdDTO.TinhTrang = lqdSelect.TinhTrang;
            }
            lqdDTO.MaLoaiQD = maLQD;
            lqdDTO.TenLoaiQD = txtLoaiQD.Text;            
            lqdDTO.GhiChu = meGhiChu.Text;

            return lqdDTO;
        }

        private void gridViewLoaiQD_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listLoaiQD.Count)
            {
                lqdSelect = ((GridView)sender).GetRow(_selectedIndex) as LoaiQuyetDinhDTO;
            }
            else
                if (listLoaiQD.Count != 0)
                    lqdSelect = listLoaiQD[0];
                else
                    lqdSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên loại quyết định ", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoaiQuyetDinhDTO lqdNew = GetData(new LoaiQuyetDinhDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = lqdCTL.Save(lqdNew);
            else
                isSucess = lqdCTL.Update(lqdNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listLoaiQD.IndexOf(listLoaiQD.FirstOrDefault(p => p.MaLoaiQD == lqdNew.MaLoaiQD));
                gridViewLoaiQD.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (lqdSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa loại quyết định " + lqdSelect.TenLoaiQD + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = lqdCTL.Delete(lqdSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listLoaiQD.Count != 0)
                            gridViewLoaiQD.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void ucLoaiQuyetDinh_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }
    }
}
