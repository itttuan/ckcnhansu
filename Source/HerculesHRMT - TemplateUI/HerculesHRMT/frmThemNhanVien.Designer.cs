﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;

namespace HerculesHRMT
{
    partial class FrmThemNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmThemNhanVien));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtTenBMBP = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1MaNV = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.txtTenPhongKhoa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.imgHinhAnh = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPreview = new DevExpress.XtraEditors.SimpleButton();
            this.btnBack = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageSoLuocLyLich = new DevExpress.XtraTab.XtraTabPage();
            this.pnl_NVSoLuocLyLich = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPageLyLichBanThan = new DevExpress.XtraTab.XtraTabPage();
            this.pnl_NVQuaTrinhCongTac = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPageTrinhDoChuyenMon = new DevExpress.XtraTab.XtraTabPage();
            this.pnl_NVTrinhDo = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPageKhenThuongKyLuat = new DevExpress.XtraTab.XtraTabPage();
            this.pnl_NVKhenThuongKyLuat = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPageQuanHeGiaDinh = new DevExpress.XtraTab.XtraTabPage();
            this.pnl_NVQuanHeGiaDinh = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.btnPg6Luu = new DevExpress.XtraEditors.SimpleButton();
            this.txtPg6TuNhanXet = new DevExpress.XtraEditors.MemoEdit();
            this.ucProgressBarAddStaff = new HerculesHRMT.ucProgressBarAddStaff();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenBMBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1MaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageSoLuocLyLich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVSoLuocLyLich)).BeginInit();
            this.xtraTabPageLyLichBanThan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVQuaTrinhCongTac)).BeginInit();
            this.xtraTabPageTrinhDoChuyenMon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVTrinhDo)).BeginInit();
            this.xtraTabPageKhenThuongKyLuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVKhenThuongKyLuat)).BeginInit();
            this.xtraTabPageQuanHeGiaDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVQuanHeGiaDinh)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg6TuNhanXet.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.imgHinhAnh);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.xtraTabControl);
            this.panelControl1.Controls.Add(this.ucProgressBarAddStaff);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1264, 492);
            this.panelControl1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtTenBMBP);
            this.groupControl1.Controls.Add(this.txtPg1MaNV);
            this.groupControl1.Controls.Add(this.txtHoTen);
            this.groupControl1.Controls.Add(this.txtTenPhongKhoa);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(90, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(556, 79);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Thông Tin";
            // 
            // txtTenBMBP
            // 
            this.txtTenBMBP.Location = new System.Drawing.Point(363, 50);
            this.txtTenBMBP.Name = "txtTenBMBP";
            this.txtTenBMBP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenBMBP.Properties.Appearance.Options.UseFont = true;
            this.txtTenBMBP.Properties.ReadOnly = true;
            this.txtTenBMBP.Size = new System.Drawing.Size(185, 22);
            this.txtTenBMBP.TabIndex = 0;
            // 
            // txtPg1MaNV
            // 
            this.txtPg1MaNV.Location = new System.Drawing.Point(88, 22);
            this.txtPg1MaNV.Name = "txtPg1MaNV";
            this.txtPg1MaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1MaNV.Properties.Appearance.Options.UseFont = true;
            this.txtPg1MaNV.Properties.ReadOnly = true;
            this.txtPg1MaNV.Size = new System.Drawing.Size(185, 22);
            this.txtPg1MaNV.TabIndex = 0;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(363, 22);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(185, 22);
            this.txtHoTen.TabIndex = 0;
            // 
            // txtTenPhongKhoa
            // 
            this.txtTenPhongKhoa.Location = new System.Drawing.Point(88, 50);
            this.txtTenPhongKhoa.Name = "txtTenPhongKhoa";
            this.txtTenPhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.txtTenPhongKhoa.Properties.ReadOnly = true;
            this.txtTenPhongKhoa.Size = new System.Drawing.Size(185, 22);
            this.txtTenPhongKhoa.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(279, 53);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(78, 15);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "B.Môn/B.Phận";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(5, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(68, 15);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Phòng/Khoa";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(5, 25);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(74, 15);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Mã nhân viên";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(279, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 15);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Họ Tên";
            // 
            // imgHinhAnh
            // 
            this.imgHinhAnh.EditValue = ((object)(resources.GetObject("imgHinhAnh.EditValue")));
            this.imgHinhAnh.Location = new System.Drawing.Point(12, 5);
            this.imgHinhAnh.Name = "imgHinhAnh";
            this.imgHinhAnh.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Properties.Appearance.Image")));
            this.imgHinhAnh.Properties.Appearance.Options.UseImage = true;
            this.imgHinhAnh.Properties.InitialImage = null;
            this.imgHinhAnh.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.imgHinhAnh.Size = new System.Drawing.Size(72, 79);
            this.imgHinhAnh.TabIndex = 3;
            this.imgHinhAnh.ToolTip = "Click để thay đổi ảnh đại diện";
            this.imgHinhAnh.Click += new System.EventHandler(this.imgHinhAnh_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.btnPreview);
            this.panelControl2.Controls.Add(this.btnBack);
            this.panelControl2.Controls.Add(this.simpleButton2);
            this.panelControl2.Controls.Add(this.btnNext);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(2, 457);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1260, 33);
            this.panelControl2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(92, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lưu ý: chỉ xem được thông tin trên báo cáo khi đã chọn nút lưu!";
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Location = new System.Drawing.Point(10, 5);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 0;
            this.btnPreview.Text = "Preview";
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(1015, 5);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "Trở lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Location = new System.Drawing.Point(1096, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "Làm mới";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(1177, 5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "Kế tiếp";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl.Font = new System.Drawing.Font("Arial", 9F);
            this.xtraTabControl.Location = new System.Drawing.Point(2, 90);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageSoLuocLyLich;
            this.xtraTabControl.Size = new System.Drawing.Size(1257, 400);
            this.xtraTabControl.TabIndex = 1;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageSoLuocLyLich,
            this.xtraTabPageLyLichBanThan,
            this.xtraTabPageTrinhDoChuyenMon,
            this.xtraTabPageKhenThuongKyLuat,
            this.xtraTabPageQuanHeGiaDinh,
            this.xtraTabPage6});
            this.xtraTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl_SelectedPageChanged);
            this.xtraTabControl.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl_SelectedPageChanging);
            // 
            // xtraTabPageSoLuocLyLich
            // 
            this.xtraTabPageSoLuocLyLich.Controls.Add(this.pnl_NVSoLuocLyLich);
            this.xtraTabPageSoLuocLyLich.Name = "xtraTabPageSoLuocLyLich";
            this.xtraTabPageSoLuocLyLich.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPageSoLuocLyLich.Text = "Sơ Lược Lý Lịch";
            // 
            // pnl_NVSoLuocLyLich
            // 
            this.pnl_NVSoLuocLyLich.Appearance.BackColor = System.Drawing.Color.White;
            this.pnl_NVSoLuocLyLich.Appearance.Options.UseBackColor = true;
            this.pnl_NVSoLuocLyLich.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnl_NVSoLuocLyLich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_NVSoLuocLyLich.Location = new System.Drawing.Point(0, 0);
            this.pnl_NVSoLuocLyLich.Name = "pnl_NVSoLuocLyLich";
            this.pnl_NVSoLuocLyLich.Size = new System.Drawing.Size(1251, 372);
            this.pnl_NVSoLuocLyLich.TabIndex = 0;
            // 
            // xtraTabPageLyLichBanThan
            // 
            this.xtraTabPageLyLichBanThan.Controls.Add(this.pnl_NVQuaTrinhCongTac);
            this.xtraTabPageLyLichBanThan.Name = "xtraTabPageLyLichBanThan";
            this.xtraTabPageLyLichBanThan.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPageLyLichBanThan.Text = "Lý Lịch Bản Thân";
            // 
            // pnl_NVQuaTrinhCongTac
            // 
            this.pnl_NVQuaTrinhCongTac.Appearance.BackColor = System.Drawing.Color.White;
            this.pnl_NVQuaTrinhCongTac.Appearance.Options.UseBackColor = true;
            this.pnl_NVQuaTrinhCongTac.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnl_NVQuaTrinhCongTac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_NVQuaTrinhCongTac.Location = new System.Drawing.Point(0, 0);
            this.pnl_NVQuaTrinhCongTac.Name = "pnl_NVQuaTrinhCongTac";
            this.pnl_NVQuaTrinhCongTac.Size = new System.Drawing.Size(1251, 372);
            this.pnl_NVQuaTrinhCongTac.TabIndex = 0;
            // 
            // xtraTabPageTrinhDoChuyenMon
            // 
            this.xtraTabPageTrinhDoChuyenMon.Controls.Add(this.pnl_NVTrinhDo);
            this.xtraTabPageTrinhDoChuyenMon.Name = "xtraTabPageTrinhDoChuyenMon";
            this.xtraTabPageTrinhDoChuyenMon.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPageTrinhDoChuyenMon.Text = "Trình Độ Chuyên Môn";
            // 
            // pnl_NVTrinhDo
            // 
            this.pnl_NVTrinhDo.Appearance.BackColor = System.Drawing.Color.White;
            this.pnl_NVTrinhDo.Appearance.Options.UseBackColor = true;
            this.pnl_NVTrinhDo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnl_NVTrinhDo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_NVTrinhDo.Location = new System.Drawing.Point(0, 0);
            this.pnl_NVTrinhDo.Name = "pnl_NVTrinhDo";
            this.pnl_NVTrinhDo.Size = new System.Drawing.Size(1251, 372);
            this.pnl_NVTrinhDo.TabIndex = 1;
            // 
            // xtraTabPageKhenThuongKyLuat
            // 
            this.xtraTabPageKhenThuongKyLuat.Controls.Add(this.pnl_NVKhenThuongKyLuat);
            this.xtraTabPageKhenThuongKyLuat.Name = "xtraTabPageKhenThuongKyLuat";
            this.xtraTabPageKhenThuongKyLuat.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPageKhenThuongKyLuat.Text = "Khen Thưởng Kỷ Luật";
            // 
            // pnl_NVKhenThuongKyLuat
            // 
            this.pnl_NVKhenThuongKyLuat.Appearance.BackColor = System.Drawing.Color.White;
            this.pnl_NVKhenThuongKyLuat.Appearance.Options.UseBackColor = true;
            this.pnl_NVKhenThuongKyLuat.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnl_NVKhenThuongKyLuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_NVKhenThuongKyLuat.Location = new System.Drawing.Point(0, 0);
            this.pnl_NVKhenThuongKyLuat.Name = "pnl_NVKhenThuongKyLuat";
            this.pnl_NVKhenThuongKyLuat.Size = new System.Drawing.Size(1251, 372);
            this.pnl_NVKhenThuongKyLuat.TabIndex = 1;
            // 
            // xtraTabPageQuanHeGiaDinh
            // 
            this.xtraTabPageQuanHeGiaDinh.Controls.Add(this.pnl_NVQuanHeGiaDinh);
            this.xtraTabPageQuanHeGiaDinh.Name = "xtraTabPageQuanHeGiaDinh";
            this.xtraTabPageQuanHeGiaDinh.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPageQuanHeGiaDinh.Text = "Quan Hệ Gia Đình";
            // 
            // pnl_NVQuanHeGiaDinh
            // 
            this.pnl_NVQuanHeGiaDinh.Appearance.BackColor = System.Drawing.Color.White;
            this.pnl_NVQuanHeGiaDinh.Appearance.Options.UseBackColor = true;
            this.pnl_NVQuanHeGiaDinh.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnl_NVQuanHeGiaDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_NVQuanHeGiaDinh.Location = new System.Drawing.Point(0, 0);
            this.pnl_NVQuanHeGiaDinh.Name = "pnl_NVQuanHeGiaDinh";
            this.pnl_NVQuanHeGiaDinh.Size = new System.Drawing.Size(1251, 372);
            this.pnl_NVQuanHeGiaDinh.TabIndex = 1;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.panelControl8);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1251, 372);
            this.xtraTabPage6.Text = "Tự Nhận Xét";
            // 
            // panelControl8
            // 
            this.panelControl8.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl8.Appearance.Options.UseBackColor = true;
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.groupControl15);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1251, 372);
            this.panelControl8.TabIndex = 1;
            // 
            // groupControl15
            // 
            this.groupControl15.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl15.Appearance.Options.UseFont = true;
            this.groupControl15.Controls.Add(this.btnPg6Luu);
            this.groupControl15.Controls.Add(this.txtPg6TuNhanXet);
            this.groupControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl15.Location = new System.Drawing.Point(0, 0);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(1251, 372);
            this.groupControl15.TabIndex = 1;
            this.groupControl15.Text = "Về phẩm chất chính trị, đạo đức, lối sống, ý thức kỷ luật, năng lực và sở trường " +
    "khác...";
            // 
            // btnPg6Luu
            // 
            this.btnPg6Luu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPg6Luu.Location = new System.Drawing.Point(1171, 315);
            this.btnPg6Luu.Name = "btnPg6Luu";
            this.btnPg6Luu.Size = new System.Drawing.Size(75, 23);
            this.btnPg6Luu.TabIndex = 23;
            this.btnPg6Luu.Text = "Lưu";
            this.btnPg6Luu.Click += new System.EventHandler(this.btnPg6Luu_Click);
            // 
            // txtPg6TuNhanXet
            // 
            this.txtPg6TuNhanXet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPg6TuNhanXet.EditValue = "- Về phẩm chất:\r\n\r\n\r\n\r\n- Về đạo đức:\r\n\r\n\r\n\r\n- Về lối sống:\r\n\r\n\r\n\r\n- Về ý thức/kỷ " +
    "luật:\r\n\r\n\r\n\r\n- Về năng lực bản thân:\r\n\r\n\r\n\r\n- Sở trường khác:";
            this.txtPg6TuNhanXet.Location = new System.Drawing.Point(0, 24);
            this.txtPg6TuNhanXet.Name = "txtPg6TuNhanXet";
            this.txtPg6TuNhanXet.Properties.AllowFocused = false;
            this.txtPg6TuNhanXet.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg6TuNhanXet.Properties.Appearance.Options.UseFont = true;
            this.txtPg6TuNhanXet.Size = new System.Drawing.Size(1251, 285);
            this.txtPg6TuNhanXet.TabIndex = 9;
            // 
            // ucProgressBarAddStaff
            // 
            this.ucProgressBarAddStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ucProgressBarAddStaff.Location = new System.Drawing.Point(514, 5);
            this.ucProgressBarAddStaff.Name = "ucProgressBarAddStaff";
            this.ucProgressBarAddStaff.Size = new System.Drawing.Size(740, 84);
            this.ucProgressBarAddStaff.TabIndex = 0;
            // 
            // FrmThemNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 492);
            this.Controls.Add(this.panelControl1);
            this.Name = "FrmThemNhanVien";
            this.Text = "Thêm Nhân Viên - Sơ Lược Lý Lịch";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmThemNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenBMBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1MaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageSoLuocLyLich.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVSoLuocLyLich)).EndInit();
            this.xtraTabPageLyLichBanThan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVQuaTrinhCongTac)).EndInit();
            this.xtraTabPageTrinhDoChuyenMon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVTrinhDo)).EndInit();
            this.xtraTabPageKhenThuongKyLuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVKhenThuongKyLuat)).EndInit();
            this.xtraTabPageQuanHeGiaDinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_NVQuanHeGiaDinh)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPg6TuNhanXet.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelControl panelControl1;
        private XtraTabControl xtraTabControl;
        private XtraTabPage xtraTabPageSoLuocLyLich;
        private XtraTabPage xtraTabPageLyLichBanThan;
        private PanelControl panelControl2;
        private XtraTabPage xtraTabPageTrinhDoChuyenMon;
        private XtraTabPage xtraTabPageKhenThuongKyLuat;
        private XtraTabPage xtraTabPageQuanHeGiaDinh;
        private XtraTabPage xtraTabPage6;
        private SimpleButton btnNext;
        private SimpleButton btnBack;
        private SimpleButton simpleButton2;
        private PanelControl pnl_NVSoLuocLyLich;
        private PictureEdit imgHinhAnh;
        private GroupControl groupControl1;
        private LabelControl labelControl1;
        private LabelControl labelControl4;
        private LabelControl labelControl3;
        private TextEdit txtTenBMBP;
        private TextEdit txtHoTen;
        private TextEdit txtTenPhongKhoa;
        private PanelControl pnl_NVQuaTrinhCongTac;
        private PanelControl pnl_NVTrinhDo;
        private PanelControl pnl_NVKhenThuongKyLuat;
        private PanelControl pnl_NVQuanHeGiaDinh;
        private PanelControl panelControl8;
        private GroupControl groupControl15;
        private ucProgressBarAddStaff ucProgressBarAddStaff;
        private SimpleButton btnPreview;
        private TextEdit txtPg1MaNV;
        private LabelControl labelControl2;
        private MemoEdit txtPg6TuNhanXet;
        private SimpleButton btnPg6Luu;
        private System.Windows.Forms.Label label1;

    }
}