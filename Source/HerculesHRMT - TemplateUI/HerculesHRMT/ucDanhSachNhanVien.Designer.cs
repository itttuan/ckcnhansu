﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;

namespace HerculesHRMT
{
    partial class ucDanhSachNhanVien
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDanhSachNhanVien));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tabNhanVien = new DevExpress.XtraTab.XtraTabControl();
            this.tabDSNhanVien = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.grcTimKiem = new DevExpress.XtraEditors.GroupControl();
            this.btnTimKiemCMND = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtTKCMND = new DevExpress.XtraEditors.TextEdit();
            this.btnTimKiemMaNV = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTKMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKBPBM = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKPhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnXem = new DevExpress.XtraEditors.SimpleButton();
            this.gcDSNhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridViewDSNhanVien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCNND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChucvu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTinhTrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDEDSNgayS = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.tabHieuchinhNV = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.btnViewImg = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenBMBP = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.txtTenPhongKhoa = new DevExpress.XtraEditors.TextEdit();
            this.txtMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.imgHinhAnh = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabControl3 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageSoLuocLL = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageQuaTrinhCT = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVQuaTrinhCongTac1 = new HerculesHRMT.ucNVQuaTrinhCongTac();
            this.tabPageTrinhDo = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageKhenThuongKyLuat = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVKhenThuongKyLuat1 = new HerculesHRMT.ucNVKhenThuongKyLuat();
            this.tabPageQuanHeGiaDinh = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVQuanHeGiaDinh1 = new HerculesHRMT.ucNVQuanHeGiaDinh();
            this.tabPageLuong = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageHopDong = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVHopDong1 = new HerculesHRMT.ucNVHopDong();
            this.tabPageQuyetDinh = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVQuyetDinh1 = new HerculesHRMT.ucNVQuyetDinh();
            this.tabPageThiDua = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVThiDua1 = new HerculesHRMT.ucNVThiDua();
            this.tabPagePhanCong = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVPhanCong1 = new HerculesHRMT.ucNVPhanCong();
            this.tabPageBaoHiem = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVBaoHiem1 = new HerculesHRMT.ucNVBaoHiem();
            this.tabPageTuNhanXet = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.btnPg6Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg6Them = new DevExpress.XtraEditors.SimpleButton();
            this.txtPg6TuNhanXet = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabLyLichCB = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.cbbPg1QuanHuyenHT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1EmailTail = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1TinhThanhHT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TinhThanhTT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.cbbPg1QuanHuyenTT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1Email = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DiaChiTT = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DiaChiHT = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dtPg1NgayXuatNgu = new DevExpress.XtraEditors.DateEdit();
            this.cbbPg1GiaDinhChinhSach = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1NhomMau = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtPg1NgayChinhThucDCSVN = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgayNhapNgu = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgayVaoDCSVN = new DevExpress.XtraEditors.DateEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1ThuongBinhHangSecond = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1ThuongBinhHangFirst = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1CanNang = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1ChieuCao = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1QuanHamCaoNhat = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1TinhTrangSucKhoe = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.cbbPg1MaNgach = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1Hang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1Bac = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1PhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1BMBP = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1ChuyenMon = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1ChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TinhTrangLamViec = new DevExpress.XtraEditors.LookUpEdit();
            this.dtPg1NgayVaoLam = new DevExpress.XtraEditors.DateEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.cbbPg1QuocTich = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1VanHoa = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1TinhTrangHonNhan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1NoiCapCMND = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1NoiSinh = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1QueQuan = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TonGiao = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1DanToc = new DevExpress.XtraEditors.LookUpEdit();
            this.dtPg1NgayCapCMND = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.rdbPg1GioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1Ten = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1SoCMND = new DevExpress.XtraEditors.TextEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1TenKhac = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1HoDem = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.gridQTCT = new DevExpress.XtraGrid.GridControl();
            this.gridViewQTCT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNoiCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNghenghiep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTuNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDenNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThanhTich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit5 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.gridBCCC = new DevExpress.XtraGrid.GridControl();
            this.gridViewBCCC = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBCID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyenmon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyennganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCoSoDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTgianDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTgHieuluc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayCapBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGHICHUBCCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit6 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit21 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit20 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit19 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit18 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit17 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.gridLuong = new DevExpress.XtraGrid.GridControl();
            this.gridViewLuong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColLuongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongSQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongNgach = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongBac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongHeSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongCB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongChinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCCV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCTN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongVuot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongBHXH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongNgayhuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit7 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton22 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit10 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit9 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl99 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl97 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit8 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl96 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl95 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl94 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.gridQHGD = new DevExpress.XtraGrid.GridControl();
            this.gridViewQHGD = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColQHGDID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDHoTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDQuanhe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDNgheNghiep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDNoiO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDDacDiem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memoEdit4 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.lookUpEdit24 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit23 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl106 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl105 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit11 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit22 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.gridHopdong = new DevExpress.XtraGrid.GridControl();
            this.gridViewHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongMaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongLoaiHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNgayky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNguoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton28 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton29 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton30 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton31 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton32 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit5 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit14 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit13 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit12 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit25 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage8 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.gridQD = new DevExpress.XtraGrid.GridControl();
            this.gridViewQuyetDinh = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColQDID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDSoQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDLoaiQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDNgayky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDNguoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCoQDNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton33 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton34 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton35 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton36 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton37 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit6 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl118 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit15 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl119 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit16 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl120 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl121 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit17 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl122 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit26 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage9 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.gridPhancong = new DevExpress.XtraGrid.GridControl();
            this.gridViewPhanCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPhancongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongDonvi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongBomon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongCongviec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongDamnhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditSTT = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.simpleButton38 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton39 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton40 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton41 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton42 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit18 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit29 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit28 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit27 = new DevExpress.XtraEditors.LookUpEdit();
            this.xtraTabPage10 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.gridBHXH = new DevExpress.XtraGrid.GridControl();
            this.gridViewBHXH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBHXHID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHXHSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHXHNgaycap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton48 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton49 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton50 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton51 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton52 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl130 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit19 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.gridBHYT = new DevExpress.XtraGrid.GridControl();
            this.gridViewBHYT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBHYTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTSothe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTNoiDK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTNgaycap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton43 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton44 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton45 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton46 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton47 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit20 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage11 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.gridSuckhoe = new DevExpress.XtraGrid.GridControl();
            this.gridViewSuckhoe = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColSKID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColSKNgaykham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColSKTinhtrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColSKChieucao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColSKCannang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton53 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton54 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton55 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton56 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton57 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit49 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl137 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl136 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit21 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl134 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage12 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage13 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.gridThang = new DevExpress.XtraGrid.GridControl();
            this.gridViewThiduathang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDThangID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThangNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThangXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton63 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton64 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton65 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton66 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton67 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl145 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.gridTDHK = new DevExpress.XtraGrid.GridControl();
            this.gridViewTDHocKy = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDHKID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKHocKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton68 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton69 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton70 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton71 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton72 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit6 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl146 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit7 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl147 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit8 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl148 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.gridTDNam = new DevExpress.XtraGrid.GridControl();
            this.gridViewTDNam = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDNamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDNamNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDNamXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton73 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton74 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton75 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton76 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton77 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit9 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl149 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit10 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl150 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage14 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton58 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton59 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton60 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton61 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton62 = new DevExpress.XtraEditors.SimpleButton();
            this.gridKhenthuong = new DevExpress.XtraGrid.GridControl();
            this.gridViewKhenthuong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColKhenthuongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKhenthuongNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKhenthuongLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKhenthuongNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKhenthuongHinhthuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKhenthuongCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookUpEdit32 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl142 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit7 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl141 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit31 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl140 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit30 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl139 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit22 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl138 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage15 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton78 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton79 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton80 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton81 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton82 = new DevExpress.XtraEditors.SimpleButton();
            this.gridKyluat = new DevExpress.XtraGrid.GridControl();
            this.gridViewKyluat = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColKyluatID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKyluatNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKyluatLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKyluatNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKyluatHinhthuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lookUpEdit33 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl151 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit8 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl152 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit34 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl153 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit35 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl154 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit23 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl155 = new DevExpress.XtraEditors.LabelControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.ucNVSoLuocLyLich1 = new HerculesHRMT.ucNVSoLuocLyLich();
            this.ucNVTrinhDo1 = new HerculesHRMT.ucNVTrinhDo();
            this.ucNVLuong1 = new HerculesHRMT.ucNVLuong();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabNhanVien)).BeginInit();
            this.tabNhanVien.SuspendLayout();
            this.tabDSNhanVien.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).BeginInit();
            this.grcTimKiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).BeginInit();
            this.tabHieuchinhNV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenBMBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).BeginInit();
            this.xtraTabControl3.SuspendLayout();
            this.tabPageSoLuocLL.SuspendLayout();
            this.tabPageQuaTrinhCT.SuspendLayout();
            this.tabPageTrinhDo.SuspendLayout();
            this.tabPageKhenThuongKyLuat.SuspendLayout();
            this.tabPageQuanHeGiaDinh.SuspendLayout();
            this.tabPageLuong.SuspendLayout();
            this.tabPageHopDong.SuspendLayout();
            this.tabPageQuyetDinh.SuspendLayout();
            this.tabPageThiDua.SuspendLayout();
            this.tabPagePhanCong.SuspendLayout();
            this.tabPageBaoHiem.SuspendLayout();
            this.tabPageTuNhanXet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg6TuNhanXet.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabLyLichCB.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1EmailTail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1GiaDinhChinhSach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NhomMau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangSecond.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangFirst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1CanNang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ChieuCao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QuanHamCaoNhat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TinhTrangSucKhoe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Hang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1PhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1BMBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChuyenMon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangLamViec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuocTich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1VanHoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangHonNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiCapCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiSinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QueQuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TonGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1DanToc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Ten.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1SoCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TenKhac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQTCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQTCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBCCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQHGD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQHGD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit14.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit13.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            this.xtraTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit15.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit16.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit17.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            this.xtraTabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPhancong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhanCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit18.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).BeginInit();
            this.xtraTabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHXH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHXH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit19.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHYT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHYT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit20.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            this.xtraTabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuckhoe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSuckhoe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit21.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit21.Properties)).BeginInit();
            this.xtraTabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThiduathang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDHK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDHocKy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDNam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDNam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).BeginInit();
            this.xtraTabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKhenthuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKhenthuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit22.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit22.Properties)).BeginInit();
            this.xtraTabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKyluat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKyluat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit23.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit23.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tabNhanVien);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1270, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // tabNhanVien
            // 
            this.tabNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabNhanVien.Location = new System.Drawing.Point(2, 2);
            this.tabNhanVien.Name = "tabNhanVien";
            this.tabNhanVien.SelectedTabPage = this.tabDSNhanVien;
            this.tabNhanVien.Size = new System.Drawing.Size(1266, 514);
            this.tabNhanVien.TabIndex = 0;
            this.tabNhanVien.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabDSNhanVien,
            this.tabHieuchinhNV,
            this.xtraTabPage1});
            this.tabNhanVien.Selected += new DevExpress.XtraTab.TabPageEventHandler(this.tabNhanVien_Selected);
            // 
            // tabDSNhanVien
            // 
            this.tabDSNhanVien.Controls.Add(this.splitContainerControl4);
            this.tabDSNhanVien.Name = "tabDSNhanVien";
            this.tabDSNhanVien.Size = new System.Drawing.Size(1260, 486);
            this.tabDSNhanVien.Text = "Danh Sách Nhân Viên";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.grcTimKiem);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1260, 486);
            this.splitContainerControl4.SplitterPosition = 126;
            this.splitContainerControl4.TabIndex = 0;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // grcTimKiem
            // 
            this.grcTimKiem.Controls.Add(this.btnTimKiemCMND);
            this.grcTimKiem.Controls.Add(this.labelControl5);
            this.grcTimKiem.Controls.Add(this.txtTKCMND);
            this.grcTimKiem.Controls.Add(this.btnTimKiemMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl4);
            this.grcTimKiem.Controls.Add(this.txtTKMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl2);
            this.grcTimKiem.Controls.Add(this.lueTKBPBM);
            this.grcTimKiem.Controls.Add(this.labelControl1);
            this.grcTimKiem.Controls.Add(this.lueTKPhongKhoa);
            this.grcTimKiem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grcTimKiem.Location = new System.Drawing.Point(0, 0);
            this.grcTimKiem.Name = "grcTimKiem";
            this.grcTimKiem.Size = new System.Drawing.Size(1260, 126);
            this.grcTimKiem.TabIndex = 2;
            this.grcTimKiem.Text = "Tìm Kiếm";
            // 
            // btnTimKiemCMND
            // 
            this.btnTimKiemCMND.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemCMND.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemCMND.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemCMND.Location = new System.Drawing.Point(667, 67);
            this.btnTimKiemCMND.Name = "btnTimKiemCMND";
            this.btnTimKiemCMND.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemCMND.TabIndex = 11;
            this.btnTimKiemCMND.ToolTip = "Tìm theo số CMND";
            this.btnTimKiemCMND.Click += new System.EventHandler(this.btnTimKiemCMND_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(396, 70);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(37, 15);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "CMND:";
            // 
            // txtTKCMND
            // 
            this.txtTKCMND.Location = new System.Drawing.Point(486, 67);
            this.txtTKCMND.Name = "txtTKCMND";
            this.txtTKCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKCMND.Properties.Appearance.Options.UseFont = true;
            this.txtTKCMND.Size = new System.Drawing.Size(175, 22);
            this.txtTKCMND.TabIndex = 9;
            // 
            // btnTimKiemMaNV
            // 
            this.btnTimKiemMaNV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemMaNV.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemMaNV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemMaNV.Location = new System.Drawing.Point(667, 41);
            this.btnTimKiemMaNV.Name = "btnTimKiemMaNV";
            this.btnTimKiemMaNV.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemMaNV.TabIndex = 8;
            this.btnTimKiemMaNV.ToolTip = "Tìm theo Mã Nhân viên";
            this.btnTimKiemMaNV.Click += new System.EventHandler(this.btnTimKiemMaNV_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(396, 44);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(78, 15);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Mã Nhân viên:";
            // 
            // txtTKMaNV
            // 
            this.txtTKMaNV.Location = new System.Drawing.Point(486, 41);
            this.txtTKMaNV.Name = "txtTKMaNV";
            this.txtTKMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtTKMaNV.Size = new System.Drawing.Size(175, 22);
            this.txtTKMaNV.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(35, 70);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(95, 15);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Bộ phận/Bộ môn:";
            // 
            // lueTKBPBM
            // 
            this.lueTKBPBM.Location = new System.Drawing.Point(145, 67);
            this.lueTKBPBM.Name = "lueTKBPBM";
            this.lueTKBPBM.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKBPBM.Properties.Appearance.Options.UseFont = true;
            this.lueTKBPBM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKBPBM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueTKBPBM.Properties.DisplayMember = "TenBMBP";
            this.lueTKBPBM.Properties.NullText = "";
            this.lueTKBPBM.Properties.ValueMember = "MaBMBP";
            this.lueTKBPBM.Size = new System.Drawing.Size(230, 22);
            this.lueTKBPBM.TabIndex = 2;
            this.lueTKBPBM.EditValueChanged += new System.EventHandler(this.lueTKBPBM_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(35, 44);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(71, 15);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Phòng/Khoa:";
            // 
            // lueTKPhongKhoa
            // 
            this.lueTKPhongKhoa.Location = new System.Drawing.Point(145, 41);
            this.lueTKPhongKhoa.Name = "lueTKPhongKhoa";
            this.lueTKPhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKPhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.lueTKPhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKPhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.lueTKPhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.lueTKPhongKhoa.Properties.NullText = "";
            this.lueTKPhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.lueTKPhongKhoa.Size = new System.Drawing.Size(230, 22);
            this.lueTKPhongKhoa.TabIndex = 0;
            this.lueTKPhongKhoa.EditValueChanged += new System.EventHandler(this.lueTKPhongKhoa_EditValueChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.btnThem);
            this.panelControl2.Controls.Add(this.btnXem);
            this.panelControl2.Controls.Add(this.gcDSNhanVien);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1260, 355);
            this.panelControl2.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Location = new System.Drawing.Point(890, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "Preview";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // btnThem
            // 
            this.btnThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThem.Location = new System.Drawing.Point(1111, 5);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(91, 23);
            this.btnThem.TabIndex = 12;
            this.btnThem.Text = "Thêm Nhân Viên";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXem
            // 
            this.btnXem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXem.Location = new System.Drawing.Point(987, 5);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(101, 23);
            this.btnXem.TabIndex = 14;
            this.btnXem.Text = "Xem Thông Tin";
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // gcDSNhanVien
            // 
            this.gcDSNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDSNhanVien.Location = new System.Drawing.Point(2, 2);
            this.gcDSNhanVien.MainView = this.gridViewDSNhanVien;
            this.gcDSNhanVien.Name = "gcDSNhanVien";
            this.gcDSNhanVien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDEDSNgayS});
            this.gcDSNhanVien.Size = new System.Drawing.Size(1256, 351);
            this.gcDSNhanVien.TabIndex = 3;
            this.gcDSNhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDSNhanVien});
            // 
            // gridViewDSNhanVien
            // 
            this.gridViewDSNhanVien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNVID,
            this.gridColMaNV,
            this.gridColHo,
            this.gridColTen,
            this.gridColNgaySinh,
            this.gridColGioiTinh,
            this.gridColCNND,
            this.gridColEmail,
            this.gridColChucvu,
            this.gridColPhongKhoa,
            this.gridColBoMon,
            this.gridColTinhTrang});
            this.gridViewDSNhanVien.GridControl = this.gcDSNhanVien;
            this.gridViewDSNhanVien.GroupPanelText = " ";
            this.gridViewDSNhanVien.Name = "gridViewDSNhanVien";
            this.gridViewDSNhanVien.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDSNhanVien.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDSNhanVien_FocusedRowChanged);
            this.gridViewDSNhanVien.DoubleClick += new System.EventHandler(this.gridViewDSNhanVien_DoubleClick);
            // 
            // gridColNVID
            // 
            this.gridColNVID.Caption = "ID";
            this.gridColNVID.FieldName = "Id";
            this.gridColNVID.Name = "gridColNVID";
            // 
            // gridColMaNV
            // 
            this.gridColMaNV.Caption = "Mã NV";
            this.gridColMaNV.FieldName = "MaNV";
            this.gridColMaNV.Name = "gridColMaNV";
            this.gridColMaNV.OptionsColumn.AllowEdit = false;
            this.gridColMaNV.OptionsColumn.ReadOnly = true;
            this.gridColMaNV.Visible = true;
            this.gridColMaNV.VisibleIndex = 0;
            // 
            // gridColHo
            // 
            this.gridColHo.Caption = "Họ";
            this.gridColHo.FieldName = "Ho";
            this.gridColHo.Name = "gridColHo";
            this.gridColHo.OptionsColumn.AllowEdit = false;
            this.gridColHo.OptionsColumn.ReadOnly = true;
            this.gridColHo.Visible = true;
            this.gridColHo.VisibleIndex = 1;
            // 
            // gridColTen
            // 
            this.gridColTen.Caption = "Tên";
            this.gridColTen.FieldName = "Ten";
            this.gridColTen.Name = "gridColTen";
            this.gridColTen.OptionsColumn.AllowEdit = false;
            this.gridColTen.OptionsColumn.ReadOnly = true;
            this.gridColTen.Visible = true;
            this.gridColTen.VisibleIndex = 2;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày sinh";
            this.gridColNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 3;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Nữ";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.OptionsColumn.AllowEdit = false;
            this.gridColGioiTinh.OptionsColumn.ReadOnly = true;
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 5;
            // 
            // gridColCNND
            // 
            this.gridColCNND.Caption = "CMND";
            this.gridColCNND.FieldName = "CMND";
            this.gridColCNND.Name = "gridColCNND";
            this.gridColCNND.Visible = true;
            this.gridColCNND.VisibleIndex = 4;
            // 
            // gridColEmail
            // 
            this.gridColEmail.Caption = "Email";
            this.gridColEmail.FieldName = "Email";
            this.gridColEmail.Name = "gridColEmail";
            this.gridColEmail.Visible = true;
            this.gridColEmail.VisibleIndex = 6;
            // 
            // gridColChucvu
            // 
            this.gridColChucvu.Caption = "Chức vụ";
            this.gridColChucvu.FieldName = "TenChucVu";
            this.gridColChucvu.Name = "gridColChucvu";
            this.gridColChucvu.OptionsColumn.AllowEdit = false;
            this.gridColChucvu.OptionsColumn.ReadOnly = true;
            this.gridColChucvu.Visible = true;
            this.gridColChucvu.VisibleIndex = 7;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng/Khoa";
            this.gridColPhongKhoa.FieldName = "TenPhongKhoa";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.OptionsColumn.AllowEdit = false;
            this.gridColPhongKhoa.OptionsColumn.ReadOnly = true;
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 8;
            // 
            // gridColBoMon
            // 
            this.gridColBoMon.Caption = "Bộ phận/Bộ môn";
            this.gridColBoMon.FieldName = "TenBMBP";
            this.gridColBoMon.Name = "gridColBoMon";
            this.gridColBoMon.OptionsColumn.AllowEdit = false;
            this.gridColBoMon.OptionsColumn.ReadOnly = true;
            this.gridColBoMon.Visible = true;
            this.gridColBoMon.VisibleIndex = 9;
            // 
            // gridColTinhTrang
            // 
            this.gridColTinhTrang.Caption = "Tình trạng làm việc";
            this.gridColTinhTrang.FieldName = "TinhTrangLamViec";
            this.gridColTinhTrang.Name = "gridColTinhTrang";
            this.gridColTinhTrang.OptionsColumn.AllowEdit = false;
            this.gridColTinhTrang.OptionsColumn.ReadOnly = true;
            this.gridColTinhTrang.Visible = true;
            this.gridColTinhTrang.VisibleIndex = 10;
            // 
            // repositoryItemDEDSNgayS
            // 
            this.repositoryItemDEDSNgayS.AutoHeight = false;
            this.repositoryItemDEDSNgayS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDEDSNgayS.Name = "repositoryItemDEDSNgayS";
            this.repositoryItemDEDSNgayS.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // tabHieuchinhNV
            // 
            this.tabHieuchinhNV.Controls.Add(this.splitContainerControl5);
            this.tabHieuchinhNV.Name = "tabHieuchinhNV";
            this.tabHieuchinhNV.Size = new System.Drawing.Size(1260, 486);
            this.tabHieuchinhNV.Text = "Thông Tin Nhân Viên";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.panelControl7);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.xtraTabControl3);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1260, 486);
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.panelControl9);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(1260, 100);
            this.panelControl7.TabIndex = 1;
            // 
            // panelControl9
            // 
            this.panelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl9.Controls.Add(this.btnViewImg);
            this.panelControl9.Controls.Add(this.groupControl3);
            this.panelControl9.Controls.Add(this.imgHinhAnh);
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(1256, 93);
            this.panelControl9.TabIndex = 2;
            // 
            // btnViewImg
            // 
            this.btnViewImg.Location = new System.Drawing.Point(84, 8);
            this.btnViewImg.Name = "btnViewImg";
            this.btnViewImg.Size = new System.Drawing.Size(54, 79);
            this.btnViewImg.TabIndex = 97;
            this.btnViewImg.Text = "Xem ảnh";
            this.btnViewImg.Click += new System.EventHandler(this.btnViewImg_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.txtTenBMBP);
            this.groupControl3.Controls.Add(this.txtHoTen);
            this.groupControl3.Controls.Add(this.txtTenPhongKhoa);
            this.groupControl3.Controls.Add(this.txtMaNV);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Location = new System.Drawing.Point(144, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1095, 84);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Nhân viên";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(62, 27);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(77, 15);
            this.labelControl6.TabIndex = 97;
            this.labelControl6.Text = "Mã Nhân Viên";
            // 
            // txtTenBMBP
            // 
            this.txtTenBMBP.Location = new System.Drawing.Point(559, 50);
            this.txtTenBMBP.Name = "txtTenBMBP";
            this.txtTenBMBP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenBMBP.Properties.Appearance.Options.UseFont = true;
            this.txtTenBMBP.Properties.ReadOnly = true;
            this.txtTenBMBP.Size = new System.Drawing.Size(285, 22);
            this.txtTenBMBP.TabIndex = 98;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(559, 24);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(285, 22);
            this.txtHoTen.TabIndex = 99;
            // 
            // txtTenPhongKhoa
            // 
            this.txtTenPhongKhoa.Location = new System.Drawing.Point(145, 50);
            this.txtTenPhongKhoa.Name = "txtTenPhongKhoa";
            this.txtTenPhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.txtTenPhongKhoa.Properties.ReadOnly = true;
            this.txtTenPhongKhoa.Size = new System.Drawing.Size(285, 22);
            this.txtTenPhongKhoa.TabIndex = 100;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(145, 24);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Properties.AllowFocused = false;
            this.txtMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtMaNV.Properties.ReadOnly = true;
            this.txtMaNV.Size = new System.Drawing.Size(285, 22);
            this.txtMaNV.TabIndex = 101;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(475, 53);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(78, 15);
            this.labelControl7.TabIndex = 102;
            this.labelControl7.Text = "B.Môn/B.Phận";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(62, 53);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(68, 15);
            this.labelControl8.TabIndex = 103;
            this.labelControl8.Text = "Phòng/Khoa";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(475, 27);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 15);
            this.labelControl9.TabIndex = 104;
            this.labelControl9.Text = "Họ Tên";
            // 
            // imgHinhAnh
            // 
            this.imgHinhAnh.EditValue = ((object)(resources.GetObject("imgHinhAnh.EditValue")));
            this.imgHinhAnh.Location = new System.Drawing.Point(5, 8);
            this.imgHinhAnh.Name = "imgHinhAnh";
            this.imgHinhAnh.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Properties.Appearance.Image")));
            this.imgHinhAnh.Properties.Appearance.Options.UseImage = true;
            this.imgHinhAnh.Properties.InitialImage = null;
            this.imgHinhAnh.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.imgHinhAnh.Size = new System.Drawing.Size(72, 79);
            this.imgHinhAnh.TabIndex = 96;
            this.imgHinhAnh.ToolTip = "Click để thay đổi ảnh đại diện";
            this.imgHinhAnh.Click += new System.EventHandler(this.imgHinhAnh_Click);
            // 
            // xtraTabControl3
            // 
            this.xtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl3.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl3.Name = "xtraTabControl3";
            this.xtraTabControl3.SelectedTabPage = this.tabPageSoLuocLL;
            this.xtraTabControl3.Size = new System.Drawing.Size(1260, 381);
            this.xtraTabControl3.TabIndex = 0;
            this.xtraTabControl3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageSoLuocLL,
            this.tabPageQuaTrinhCT,
            this.tabPageTrinhDo,
            this.tabPageKhenThuongKyLuat,
            this.tabPageQuanHeGiaDinh,
            this.tabPageLuong,
            this.tabPageHopDong,
            this.tabPageQuyetDinh,
            this.tabPageThiDua,
            this.tabPagePhanCong,
            this.tabPageBaoHiem,
            this.tabPageTuNhanXet});
            this.xtraTabControl3.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl3_SelectedPageChanged);
            this.xtraTabControl3.Selected += new DevExpress.XtraTab.TabPageEventHandler(this.xtraTabControl3_Selected);
            // 
            // tabPageSoLuocLL
            // 
            this.tabPageSoLuocLL.AutoScroll = true;
            this.tabPageSoLuocLL.Controls.Add(this.ucNVSoLuocLyLich1);
            this.tabPageSoLuocLL.Name = "tabPageSoLuocLL";
            this.tabPageSoLuocLL.Size = new System.Drawing.Size(1254, 353);
            this.tabPageSoLuocLL.Text = "Sơ Lược Lý Lịch";
            // 
            // tabPageQuaTrinhCT
            // 
            this.tabPageQuaTrinhCT.AutoScroll = true;
            this.tabPageQuaTrinhCT.Controls.Add(this.ucNVQuaTrinhCongTac1);
            this.tabPageQuaTrinhCT.Name = "tabPageQuaTrinhCT";
            this.tabPageQuaTrinhCT.Size = new System.Drawing.Size(1254, 353);
            this.tabPageQuaTrinhCT.Text = "Lý Lịch Bản Thân";
            // 
            // ucNVQuaTrinhCongTac1
            // 
            this.ucNVQuaTrinhCongTac1.AutoScroll = true;
            this.ucNVQuaTrinhCongTac1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVQuaTrinhCongTac1.Location = new System.Drawing.Point(0, 0);
            this.ucNVQuaTrinhCongTac1.Name = "ucNVQuaTrinhCongTac1";
            this.ucNVQuaTrinhCongTac1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVQuaTrinhCongTac1.TabIndex = 0;
            // 
            // tabPageTrinhDo
            // 
            this.tabPageTrinhDo.AutoScroll = true;
            this.tabPageTrinhDo.Controls.Add(this.ucNVTrinhDo1);
            this.tabPageTrinhDo.Name = "tabPageTrinhDo";
            this.tabPageTrinhDo.Size = new System.Drawing.Size(1254, 353);
            this.tabPageTrinhDo.Text = "Trình Độ Chuyên Môn";
            // 
            // tabPageKhenThuongKyLuat
            // 
            this.tabPageKhenThuongKyLuat.AutoScroll = true;
            this.tabPageKhenThuongKyLuat.Controls.Add(this.ucNVKhenThuongKyLuat1);
            this.tabPageKhenThuongKyLuat.Name = "tabPageKhenThuongKyLuat";
            this.tabPageKhenThuongKyLuat.Size = new System.Drawing.Size(1254, 353);
            this.tabPageKhenThuongKyLuat.Text = "Khen Thưởng Kỷ Luật";
            // 
            // ucNVKhenThuongKyLuat1
            // 
            this.ucNVKhenThuongKyLuat1.AutoScroll = true;
            this.ucNVKhenThuongKyLuat1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVKhenThuongKyLuat1.Location = new System.Drawing.Point(0, 0);
            this.ucNVKhenThuongKyLuat1.Name = "ucNVKhenThuongKyLuat1";
            this.ucNVKhenThuongKyLuat1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVKhenThuongKyLuat1.TabIndex = 0;
            // 
            // tabPageQuanHeGiaDinh
            // 
            this.tabPageQuanHeGiaDinh.AutoScroll = true;
            this.tabPageQuanHeGiaDinh.Controls.Add(this.ucNVQuanHeGiaDinh1);
            this.tabPageQuanHeGiaDinh.Name = "tabPageQuanHeGiaDinh";
            this.tabPageQuanHeGiaDinh.Size = new System.Drawing.Size(1254, 353);
            this.tabPageQuanHeGiaDinh.Text = "Quan Hệ Gia Đình";
            // 
            // ucNVQuanHeGiaDinh1
            // 
            this.ucNVQuanHeGiaDinh1.AutoScroll = true;
            this.ucNVQuanHeGiaDinh1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVQuanHeGiaDinh1.Location = new System.Drawing.Point(0, 0);
            this.ucNVQuanHeGiaDinh1.Name = "ucNVQuanHeGiaDinh1";
            this.ucNVQuanHeGiaDinh1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVQuanHeGiaDinh1.TabIndex = 0;
            // 
            // tabPageLuong
            // 
            this.tabPageLuong.AutoScroll = true;
            this.tabPageLuong.Controls.Add(this.ucNVLuong1);
            this.tabPageLuong.Name = "tabPageLuong";
            this.tabPageLuong.Size = new System.Drawing.Size(1254, 353);
            this.tabPageLuong.Text = "Lương";
            // 
            // tabPageHopDong
            // 
            this.tabPageHopDong.AutoScroll = true;
            this.tabPageHopDong.Controls.Add(this.ucNVHopDong1);
            this.tabPageHopDong.Name = "tabPageHopDong";
            this.tabPageHopDong.Size = new System.Drawing.Size(1254, 353);
            this.tabPageHopDong.Text = "Hợp Đồng";
            // 
            // ucNVHopDong1
            // 
            this.ucNVHopDong1.AutoScroll = true;
            this.ucNVHopDong1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVHopDong1.Location = new System.Drawing.Point(0, 0);
            this.ucNVHopDong1.Name = "ucNVHopDong1";
            this.ucNVHopDong1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVHopDong1.TabIndex = 0;
            // 
            // tabPageQuyetDinh
            // 
            this.tabPageQuyetDinh.AutoScroll = true;
            this.tabPageQuyetDinh.Controls.Add(this.ucNVQuyetDinh1);
            this.tabPageQuyetDinh.Name = "tabPageQuyetDinh";
            this.tabPageQuyetDinh.Size = new System.Drawing.Size(1254, 353);
            this.tabPageQuyetDinh.Text = "Quyết Định";
            // 
            // ucNVQuyetDinh1
            // 
            this.ucNVQuyetDinh1.AutoScroll = true;
            this.ucNVQuyetDinh1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVQuyetDinh1.Location = new System.Drawing.Point(0, 0);
            this.ucNVQuyetDinh1.Name = "ucNVQuyetDinh1";
            this.ucNVQuyetDinh1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVQuyetDinh1.TabIndex = 0;
            // 
            // tabPageThiDua
            // 
            this.tabPageThiDua.Controls.Add(this.ucNVThiDua1);
            this.tabPageThiDua.Name = "tabPageThiDua";
            this.tabPageThiDua.Size = new System.Drawing.Size(1254, 353);
            this.tabPageThiDua.Text = "Thi Đua";
            // 
            // ucNVThiDua1
            // 
            this.ucNVThiDua1.AutoScroll = true;
            this.ucNVThiDua1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVThiDua1.Location = new System.Drawing.Point(0, 0);
            this.ucNVThiDua1.Name = "ucNVThiDua1";
            this.ucNVThiDua1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVThiDua1.TabIndex = 0;
            // 
            // tabPagePhanCong
            // 
            this.tabPagePhanCong.AutoScroll = true;
            this.tabPagePhanCong.Controls.Add(this.ucNVPhanCong1);
            this.tabPagePhanCong.Name = "tabPagePhanCong";
            this.tabPagePhanCong.Size = new System.Drawing.Size(1254, 353);
            this.tabPagePhanCong.Text = "Phân Công";
            // 
            // ucNVPhanCong1
            // 
            this.ucNVPhanCong1.AutoScroll = true;
            this.ucNVPhanCong1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVPhanCong1.Location = new System.Drawing.Point(0, 0);
            this.ucNVPhanCong1.Name = "ucNVPhanCong1";
            this.ucNVPhanCong1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVPhanCong1.TabIndex = 0;
            // 
            // tabPageBaoHiem
            // 
            this.tabPageBaoHiem.AutoScroll = true;
            this.tabPageBaoHiem.Controls.Add(this.ucNVBaoHiem1);
            this.tabPageBaoHiem.Name = "tabPageBaoHiem";
            this.tabPageBaoHiem.Size = new System.Drawing.Size(1254, 353);
            this.tabPageBaoHiem.Text = "BHXH - BHYT";
            // 
            // ucNVBaoHiem1
            // 
            this.ucNVBaoHiem1.AutoScroll = true;
            this.ucNVBaoHiem1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVBaoHiem1.Location = new System.Drawing.Point(0, 0);
            this.ucNVBaoHiem1.Name = "ucNVBaoHiem1";
            this.ucNVBaoHiem1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVBaoHiem1.TabIndex = 0;
            // 
            // tabPageTuNhanXet
            // 
            this.tabPageTuNhanXet.AutoScroll = true;
            this.tabPageTuNhanXet.Controls.Add(this.groupControl15);
            this.tabPageTuNhanXet.Name = "tabPageTuNhanXet";
            this.tabPageTuNhanXet.Size = new System.Drawing.Size(1254, 353);
            this.tabPageTuNhanXet.Text = "Tự Nhận Xét";
            // 
            // groupControl15
            // 
            this.groupControl15.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl15.Appearance.Options.UseFont = true;
            this.groupControl15.Controls.Add(this.btnPg6Sua);
            this.groupControl15.Controls.Add(this.btnPg6Them);
            this.groupControl15.Controls.Add(this.txtPg6TuNhanXet);
            this.groupControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl15.Location = new System.Drawing.Point(0, 0);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(1254, 353);
            this.groupControl15.TabIndex = 2;
            this.groupControl15.Text = "Về phẩm chất chính trị, đạo đức, lối sống, ý thức kỷ luật, năng lực và sở trường " +
    "khác...";
            // 
            // btnPg6Sua
            // 
            this.btnPg6Sua.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPg6Sua.Location = new System.Drawing.Point(1048, 319);
            this.btnPg6Sua.Name = "btnPg6Sua";
            this.btnPg6Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg6Sua.TabIndex = 22;
            this.btnPg6Sua.Text = "Hiệu chỉnh";
            this.btnPg6Sua.Click += new System.EventHandler(this.btnPg6Sua_Click);
            // 
            // btnPg6Them
            // 
            this.btnPg6Them.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPg6Them.Location = new System.Drawing.Point(1147, 319);
            this.btnPg6Them.Name = "btnPg6Them";
            this.btnPg6Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg6Them.TabIndex = 21;
            this.btnPg6Them.Text = "Lưu";
            this.btnPg6Them.Click += new System.EventHandler(this.btnPg6Them_Click);
            // 
            // txtPg6TuNhanXet
            // 
            this.txtPg6TuNhanXet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPg6TuNhanXet.EditValue = "- Về phẩm chất:\r\n\r\n\r\n\r\n- Về đạo đức:\r\n\r\n\r\n\r\n- Về lối sống:\r\n\r\n\r\n\r\n- Về ý thức/kỷ " +
    "luật:\r\n\r\n\r\n\r\n- Về năng lực bản thân:\r\n\r\n\r\n\r\n- Sở trường khác:";
            this.txtPg6TuNhanXet.Location = new System.Drawing.Point(2, 21);
            this.txtPg6TuNhanXet.Name = "txtPg6TuNhanXet";
            this.txtPg6TuNhanXet.Properties.AllowFocused = false;
            this.txtPg6TuNhanXet.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg6TuNhanXet.Properties.Appearance.Options.UseFont = true;
            this.txtPg6TuNhanXet.Properties.ReadOnly = true;
            this.txtPg6TuNhanXet.Size = new System.Drawing.Size(1250, 289);
            this.txtPg6TuNhanXet.TabIndex = 8;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.panelControl8);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.PageVisible = false;
            this.xtraTabPage1.Size = new System.Drawing.Size(1260, 486);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // panelControl8
            // 
            this.panelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl8.Controls.Add(this.xtraTabControl1);
            this.panelControl8.Location = new System.Drawing.Point(6, 3);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(958, 525);
            this.panelControl8.TabIndex = 2;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabLyLichCB;
            this.xtraTabControl1.Size = new System.Drawing.Size(954, 521);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabLyLichCB,
            this.xtraTabPage3,
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7,
            this.xtraTabPage8,
            this.xtraTabPage9,
            this.xtraTabPage10,
            this.xtraTabPage11,
            this.xtraTabPage12,
            this.xtraTabPage15});
            this.xtraTabControl1.Visible = false;
            // 
            // tabLyLichCB
            // 
            this.tabLyLichCB.Controls.Add(this.xtraScrollableControl1);
            this.tabLyLichCB.Name = "tabLyLichCB";
            this.tabLyLichCB.Size = new System.Drawing.Size(948, 493);
            this.tabLyLichCB.Text = "Sơ Lược Lý Lịch";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.panelControl4);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(948, 493);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.groupControl4);
            this.panelControl4.Controls.Add(this.groupControl1);
            this.panelControl4.Controls.Add(this.groupControl2);
            this.panelControl4.Controls.Add(this.groupControl10);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(948, 493);
            this.panelControl4.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.cbbPg1QuanHuyenHT);
            this.groupControl4.Controls.Add(this.cbbPg1EmailTail);
            this.groupControl4.Controls.Add(this.cbbPg1TinhThanhHT);
            this.groupControl4.Controls.Add(this.cbbPg1TinhThanhTT);
            this.groupControl4.Controls.Add(this.labelControl25);
            this.groupControl4.Controls.Add(this.labelControl30);
            this.groupControl4.Controls.Add(this.cbbPg1QuanHuyenTT);
            this.groupControl4.Controls.Add(this.labelControl33);
            this.groupControl4.Controls.Add(this.labelControl27);
            this.groupControl4.Controls.Add(this.labelControl28);
            this.groupControl4.Controls.Add(this.labelControl24);
            this.groupControl4.Controls.Add(this.labelControl35);
            this.groupControl4.Controls.Add(this.labelControl36);
            this.groupControl4.Controls.Add(this.txtPg1Email);
            this.groupControl4.Controls.Add(this.txtPg1DienThoai);
            this.groupControl4.Controls.Add(this.txtPg1DiaChiTT);
            this.groupControl4.Controls.Add(this.txtPg1DiaChiHT);
            this.groupControl4.Location = new System.Drawing.Point(3, 206);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(942, 101);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = "Thông tin liên lạc";
            // 
            // cbbPg1QuanHuyenHT
            // 
            this.cbbPg1QuanHuyenHT.Location = new System.Drawing.Point(598, 73);
            this.cbbPg1QuanHuyenHT.Name = "cbbPg1QuanHuyenHT";
            this.cbbPg1QuanHuyenHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuanHuyenHT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuanHuyenHT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuanHuyenHT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuanHuyen", "Mã quận huyện", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuanHuyen", "Tên quận huyện")});
            this.cbbPg1QuanHuyenHT.Properties.NullText = "";
            this.cbbPg1QuanHuyenHT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1QuanHuyenHT.TabIndex = 4;
            // 
            // cbbPg1EmailTail
            // 
            this.cbbPg1EmailTail.EditValue = "gmail.com";
            this.cbbPg1EmailTail.Location = new System.Drawing.Point(988, 73);
            this.cbbPg1EmailTail.Name = "cbbPg1EmailTail";
            this.cbbPg1EmailTail.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1EmailTail.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1EmailTail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1EmailTail.Properties.Items.AddRange(new object[] {
            "gmail.com",
            "yahoo.com",
            "yahoo.com.vn",
            "outlook.com",
            "live.com",
            "facebook.com",
            "hotmail.com",
            "yopmail.com",
            "ymail.com",
            "rocketmail.com",
            "msn.com",
            "zing.vn",
            "ovi.com"});
            this.cbbPg1EmailTail.Size = new System.Drawing.Size(106, 22);
            this.cbbPg1EmailTail.TabIndex = 5;
            // 
            // cbbPg1TinhThanhHT
            // 
            this.cbbPg1TinhThanhHT.Location = new System.Drawing.Point(447, 73);
            this.cbbPg1TinhThanhHT.Name = "cbbPg1TinhThanhHT";
            this.cbbPg1TinhThanhHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhThanhHT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhThanhHT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhThanhHT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1TinhThanhHT.Properties.NullText = "";
            this.cbbPg1TinhThanhHT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1TinhThanhHT.TabIndex = 4;
            // 
            // cbbPg1TinhThanhTT
            // 
            this.cbbPg1TinhThanhTT.Location = new System.Drawing.Point(447, 45);
            this.cbbPg1TinhThanhTT.Name = "cbbPg1TinhThanhTT";
            this.cbbPg1TinhThanhTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhThanhTT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhThanhTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhThanhTT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1TinhThanhTT.Properties.NullText = "";
            this.cbbPg1TinhThanhTT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1TinhThanhTT.TabIndex = 4;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(447, 24);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(62, 15);
            this.labelControl25.TabIndex = 0;
            this.labelControl25.Text = "Tỉnh/Thành";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(106, 24);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(39, 15);
            this.labelControl30.TabIndex = 0;
            this.labelControl30.Text = "Địa chỉ";
            // 
            // cbbPg1QuanHuyenTT
            // 
            this.cbbPg1QuanHuyenTT.Location = new System.Drawing.Point(598, 45);
            this.cbbPg1QuanHuyenTT.Name = "cbbPg1QuanHuyenTT";
            this.cbbPg1QuanHuyenTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuanHuyenTT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuanHuyenTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuanHuyenTT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuanHuyen", "Mã quận huyện", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuanHuyen", "Tên quận huyện")});
            this.cbbPg1QuanHuyenTT.Properties.NullText = "";
            this.cbbPg1QuanHuyenTT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1QuanHuyenTT.TabIndex = 4;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(970, 76);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(12, 15);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "@";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(749, 76);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(31, 15);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "Email";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(598, 24);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(68, 15);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Quận/Huyện";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(749, 48);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(57, 15);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "Điện thoại";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(6, 76);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(94, 15);
            this.labelControl35.TabIndex = 0;
            this.labelControl35.Text = "Chổ ở hiện tại (*)";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(7, 48);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(81, 15);
            this.labelControl36.TabIndex = 0;
            this.labelControl36.Text = "Thường trú (*)";
            // 
            // txtPg1Email
            // 
            this.txtPg1Email.Location = new System.Drawing.Point(812, 73);
            this.txtPg1Email.Name = "txtPg1Email";
            this.txtPg1Email.Properties.AllowFocused = false;
            this.txtPg1Email.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1Email.Properties.Appearance.Options.UseFont = true;
            this.txtPg1Email.Size = new System.Drawing.Size(152, 22);
            this.txtPg1Email.TabIndex = 0;
            // 
            // txtPg1DienThoai
            // 
            this.txtPg1DienThoai.Location = new System.Drawing.Point(812, 45);
            this.txtPg1DienThoai.Name = "txtPg1DienThoai";
            this.txtPg1DienThoai.Properties.AllowFocused = false;
            this.txtPg1DienThoai.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DienThoai.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DienThoai.Size = new System.Drawing.Size(152, 22);
            this.txtPg1DienThoai.TabIndex = 0;
            // 
            // txtPg1DiaChiTT
            // 
            this.txtPg1DiaChiTT.Location = new System.Drawing.Point(106, 45);
            this.txtPg1DiaChiTT.Name = "txtPg1DiaChiTT";
            this.txtPg1DiaChiTT.Properties.AllowFocused = false;
            this.txtPg1DiaChiTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DiaChiTT.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DiaChiTT.Size = new System.Drawing.Size(335, 22);
            this.txtPg1DiaChiTT.TabIndex = 0;
            // 
            // txtPg1DiaChiHT
            // 
            this.txtPg1DiaChiHT.Location = new System.Drawing.Point(106, 73);
            this.txtPg1DiaChiHT.Name = "txtPg1DiaChiHT";
            this.txtPg1DiaChiHT.Properties.AllowFocused = false;
            this.txtPg1DiaChiHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DiaChiHT.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DiaChiHT.Size = new System.Drawing.Size(335, 22);
            this.txtPg1DiaChiHT.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.dtPg1NgayXuatNgu);
            this.groupControl1.Controls.Add(this.cbbPg1GiaDinhChinhSach);
            this.groupControl1.Controls.Add(this.cbbPg1NhomMau);
            this.groupControl1.Controls.Add(this.dtPg1NgayChinhThucDCSVN);
            this.groupControl1.Controls.Add(this.dtPg1NgayNhapNgu);
            this.groupControl1.Controls.Add(this.dtPg1NgayVaoDCSVN);
            this.groupControl1.Controls.Add(this.labelControl34);
            this.groupControl1.Controls.Add(this.labelControl45);
            this.groupControl1.Controls.Add(this.labelControl47);
            this.groupControl1.Controls.Add(this.labelControl46);
            this.groupControl1.Controls.Add(this.labelControl38);
            this.groupControl1.Controls.Add(this.labelControl40);
            this.groupControl1.Controls.Add(this.labelControl42);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.labelControl12);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.labelControl14);
            this.groupControl1.Controls.Add(this.labelControl43);
            this.groupControl1.Controls.Add(this.labelControl44);
            this.groupControl1.Controls.Add(this.txtPg1ThuongBinhHangSecond);
            this.groupControl1.Controls.Add(this.txtPg1ThuongBinhHangFirst);
            this.groupControl1.Controls.Add(this.txtPg1CanNang);
            this.groupControl1.Controls.Add(this.txtPg1ChieuCao);
            this.groupControl1.Controls.Add(this.txtPg1QuanHamCaoNhat);
            this.groupControl1.Controls.Add(this.txtPg1TinhTrangSucKhoe);
            this.groupControl1.Location = new System.Drawing.Point(4, 313);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(942, 137);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin khác";
            // 
            // dtPg1NgayXuatNgu
            // 
            this.dtPg1NgayXuatNgu.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayXuatNgu.Location = new System.Drawing.Point(332, 52);
            this.dtPg1NgayXuatNgu.Name = "dtPg1NgayXuatNgu";
            this.dtPg1NgayXuatNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayXuatNgu.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayXuatNgu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayXuatNgu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayXuatNgu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayXuatNgu.Size = new System.Drawing.Size(85, 20);
            this.dtPg1NgayXuatNgu.TabIndex = 4;
            // 
            // cbbPg1GiaDinhChinhSach
            // 
            this.cbbPg1GiaDinhChinhSach.Location = new System.Drawing.Point(390, 108);
            this.cbbPg1GiaDinhChinhSach.Name = "cbbPg1GiaDinhChinhSach";
            this.cbbPg1GiaDinhChinhSach.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1GiaDinhChinhSach.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1GiaDinhChinhSach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1GiaDinhChinhSach.Properties.Items.AddRange(new object[] {
            "",
            "Con thương binh",
            "Con liệt sĩ",
            "Người nhiễm chất độc da cam Dioxin"});
            this.cbbPg1GiaDinhChinhSach.Size = new System.Drawing.Size(234, 22);
            this.cbbPg1GiaDinhChinhSach.TabIndex = 5;
            // 
            // cbbPg1NhomMau
            // 
            this.cbbPg1NhomMau.EditValue = "O";
            this.cbbPg1NhomMau.Location = new System.Drawing.Point(719, 80);
            this.cbbPg1NhomMau.Name = "cbbPg1NhomMau";
            this.cbbPg1NhomMau.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1NhomMau.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1NhomMau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1NhomMau.Properties.Items.AddRange(new object[] {
            "O",
            "A",
            "B",
            "AB"});
            this.cbbPg1NhomMau.Size = new System.Drawing.Size(55, 22);
            this.cbbPg1NhomMau.TabIndex = 5;
            // 
            // dtPg1NgayChinhThucDCSVN
            // 
            this.dtPg1NgayChinhThucDCSVN.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayChinhThucDCSVN.Location = new System.Drawing.Point(332, 24);
            this.dtPg1NgayChinhThucDCSVN.Name = "dtPg1NgayChinhThucDCSVN";
            this.dtPg1NgayChinhThucDCSVN.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayChinhThucDCSVN.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayChinhThucDCSVN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayChinhThucDCSVN.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayChinhThucDCSVN.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayChinhThucDCSVN.Size = new System.Drawing.Size(85, 20);
            this.dtPg1NgayChinhThucDCSVN.TabIndex = 4;
            // 
            // dtPg1NgayNhapNgu
            // 
            this.dtPg1NgayNhapNgu.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayNhapNgu.Location = new System.Drawing.Point(143, 52);
            this.dtPg1NgayNhapNgu.Name = "dtPg1NgayNhapNgu";
            this.dtPg1NgayNhapNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayNhapNgu.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayNhapNgu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayNhapNgu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayNhapNgu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayNhapNgu.Size = new System.Drawing.Size(85, 20);
            this.dtPg1NgayNhapNgu.TabIndex = 4;
            // 
            // dtPg1NgayVaoDCSVN
            // 
            this.dtPg1NgayVaoDCSVN.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayVaoDCSVN.Location = new System.Drawing.Point(143, 24);
            this.dtPg1NgayVaoDCSVN.Name = "dtPg1NgayVaoDCSVN";
            this.dtPg1NgayVaoDCSVN.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoDCSVN.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoDCSVN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoDCSVN.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoDCSVN.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoDCSVN.Size = new System.Drawing.Size(85, 20);
            this.dtPg1NgayVaoDCSVN.TabIndex = 4;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(270, 83);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(56, 15);
            this.labelControl34.TabIndex = 0;
            this.labelControl34.Text = "Chiều cao";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Location = new System.Drawing.Point(184, 111);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(3, 15);
            this.labelControl45.TabIndex = 0;
            this.labelControl45.Text = "/";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Location = new System.Drawing.Point(630, 83);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(17, 15);
            this.labelControl47.TabIndex = 0;
            this.labelControl47.Text = "kg,";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Location = new System.Drawing.Point(423, 83);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(21, 15);
            this.labelControl46.TabIndex = 0;
            this.labelControl46.Text = "cm,";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(234, 111);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(150, 15);
            this.labelControl38.TabIndex = 0;
            this.labelControl38.Text = "Là con gia đình chính sách";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(652, 83);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(61, 15);
            this.labelControl40.TabIndex = 0;
            this.labelControl40.Text = "Nhóm máu";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(480, 83);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(53, 15);
            this.labelControl42.TabIndex = 0;
            this.labelControl42.Text = "Cân nặng";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(234, 27);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(92, 15);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Ngày chính thức";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(423, 55);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(110, 15);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Quân hàm cao nhất";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(246, 55);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(80, 15);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Ngày xuất ngũ";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(6, 55);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(83, 15);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Ngày nhập ngũ";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(6, 27);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(133, 15);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "Ngày vào ĐCS Việt Nam";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Location = new System.Drawing.Point(5, 111);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(118, 15);
            this.labelControl43.TabIndex = 0;
            this.labelControl43.Text = "Là thương binh hạng";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Location = new System.Drawing.Point(5, 83);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(114, 15);
            this.labelControl44.TabIndex = 0;
            this.labelControl44.Text = "Tình trạng sức khỏe";
            // 
            // txtPg1ThuongBinhHangSecond
            // 
            this.txtPg1ThuongBinhHangSecond.Location = new System.Drawing.Point(193, 108);
            this.txtPg1ThuongBinhHangSecond.Name = "txtPg1ThuongBinhHangSecond";
            this.txtPg1ThuongBinhHangSecond.Properties.AllowFocused = false;
            this.txtPg1ThuongBinhHangSecond.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ThuongBinhHangSecond.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ThuongBinhHangSecond.Size = new System.Drawing.Size(35, 22);
            this.txtPg1ThuongBinhHangSecond.TabIndex = 0;
            // 
            // txtPg1ThuongBinhHangFirst
            // 
            this.txtPg1ThuongBinhHangFirst.Location = new System.Drawing.Point(143, 108);
            this.txtPg1ThuongBinhHangFirst.Name = "txtPg1ThuongBinhHangFirst";
            this.txtPg1ThuongBinhHangFirst.Properties.AllowFocused = false;
            this.txtPg1ThuongBinhHangFirst.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ThuongBinhHangFirst.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ThuongBinhHangFirst.Size = new System.Drawing.Size(35, 22);
            this.txtPg1ThuongBinhHangFirst.TabIndex = 0;
            // 
            // txtPg1CanNang
            // 
            this.txtPg1CanNang.Location = new System.Drawing.Point(539, 80);
            this.txtPg1CanNang.Name = "txtPg1CanNang";
            this.txtPg1CanNang.Properties.AllowFocused = false;
            this.txtPg1CanNang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1CanNang.Properties.Appearance.Options.UseFont = true;
            this.txtPg1CanNang.Size = new System.Drawing.Size(85, 22);
            this.txtPg1CanNang.TabIndex = 0;
            // 
            // txtPg1ChieuCao
            // 
            this.txtPg1ChieuCao.Location = new System.Drawing.Point(332, 80);
            this.txtPg1ChieuCao.Name = "txtPg1ChieuCao";
            this.txtPg1ChieuCao.Properties.AllowFocused = false;
            this.txtPg1ChieuCao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ChieuCao.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ChieuCao.Size = new System.Drawing.Size(85, 22);
            this.txtPg1ChieuCao.TabIndex = 0;
            // 
            // txtPg1QuanHamCaoNhat
            // 
            this.txtPg1QuanHamCaoNhat.Location = new System.Drawing.Point(539, 52);
            this.txtPg1QuanHamCaoNhat.Name = "txtPg1QuanHamCaoNhat";
            this.txtPg1QuanHamCaoNhat.Properties.AllowFocused = false;
            this.txtPg1QuanHamCaoNhat.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1QuanHamCaoNhat.Properties.Appearance.Options.UseFont = true;
            this.txtPg1QuanHamCaoNhat.Size = new System.Drawing.Size(235, 22);
            this.txtPg1QuanHamCaoNhat.TabIndex = 0;
            // 
            // txtPg1TinhTrangSucKhoe
            // 
            this.txtPg1TinhTrangSucKhoe.Location = new System.Drawing.Point(143, 80);
            this.txtPg1TinhTrangSucKhoe.Name = "txtPg1TinhTrangSucKhoe";
            this.txtPg1TinhTrangSucKhoe.Properties.AllowFocused = false;
            this.txtPg1TinhTrangSucKhoe.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1TinhTrangSucKhoe.Properties.Appearance.Options.UseFont = true;
            this.txtPg1TinhTrangSucKhoe.Size = new System.Drawing.Size(85, 22);
            this.txtPg1TinhTrangSucKhoe.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.cbbPg1MaNgach);
            this.groupControl2.Controls.Add(this.cbbPg1Hang);
            this.groupControl2.Controls.Add(this.cbbPg1Bac);
            this.groupControl2.Controls.Add(this.cbbPg1PhongKhoa);
            this.groupControl2.Controls.Add(this.cbbPg1BMBP);
            this.groupControl2.Controls.Add(this.cbbPg1ChuyenMon);
            this.groupControl2.Controls.Add(this.cbbPg1ChucVu);
            this.groupControl2.Controls.Add(this.cbbPg1TinhTrangLamViec);
            this.groupControl2.Controls.Add(this.dtPg1NgayVaoLam);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.labelControl23);
            this.groupControl2.Controls.Add(this.labelControl21);
            this.groupControl2.Controls.Add(this.labelControl20);
            this.groupControl2.Controls.Add(this.labelControl26);
            this.groupControl2.Controls.Add(this.labelControl22);
            this.groupControl2.Controls.Add(this.labelControl29);
            this.groupControl2.Controls.Add(this.labelControl31);
            this.groupControl2.Controls.Add(this.labelControl32);
            this.groupControl2.Location = new System.Drawing.Point(4, 118);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(942, 82);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Thông tin làm việc";
            // 
            // cbbPg1MaNgach
            // 
            this.cbbPg1MaNgach.EditValue = "15.110";
            this.cbbPg1MaNgach.Location = new System.Drawing.Point(541, 52);
            this.cbbPg1MaNgach.Name = "cbbPg1MaNgach";
            this.cbbPg1MaNgach.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1MaNgach.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1MaNgach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1MaNgach.Properties.Items.AddRange(new object[] {
            "15.110",
            "15.111"});
            this.cbbPg1MaNgach.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1MaNgach.TabIndex = 5;
            // 
            // cbbPg1Hang
            // 
            this.cbbPg1Hang.EditValue = "I";
            this.cbbPg1Hang.Location = new System.Drawing.Point(732, 52);
            this.cbbPg1Hang.Name = "cbbPg1Hang";
            this.cbbPg1Hang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Hang.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Hang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Hang.Properties.Items.AddRange(new object[] {
            "I",
            "II",
            "III"});
            this.cbbPg1Hang.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1Hang.TabIndex = 5;
            // 
            // cbbPg1Bac
            // 
            this.cbbPg1Bac.EditValue = "1";
            this.cbbPg1Bac.Location = new System.Drawing.Point(886, 52);
            this.cbbPg1Bac.Name = "cbbPg1Bac";
            this.cbbPg1Bac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Bac.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Bac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Bac.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbbPg1Bac.Size = new System.Drawing.Size(44, 22);
            this.cbbPg1Bac.TabIndex = 5;
            // 
            // cbbPg1PhongKhoa
            // 
            this.cbbPg1PhongKhoa.Location = new System.Drawing.Point(106, 24);
            this.cbbPg1PhongKhoa.Name = "cbbPg1PhongKhoa";
            this.cbbPg1PhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1PhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1PhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1PhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã phòng khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên phòng khoa")});
            this.cbbPg1PhongKhoa.Properties.NullText = "";
            this.cbbPg1PhongKhoa.Size = new System.Drawing.Size(161, 22);
            this.cbbPg1PhongKhoa.TabIndex = 4;
            // 
            // cbbPg1BMBP
            // 
            this.cbbPg1BMBP.Location = new System.Drawing.Point(106, 52);
            this.cbbPg1BMBP.Name = "cbbPg1BMBP";
            this.cbbPg1BMBP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1BMBP.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1BMBP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1BMBP.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã B.Môn B.Phận", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Tên bộ môn/bộ phận")});
            this.cbbPg1BMBP.Properties.NullText = "";
            this.cbbPg1BMBP.Size = new System.Drawing.Size(161, 22);
            this.cbbPg1BMBP.TabIndex = 4;
            // 
            // cbbPg1ChuyenMon
            // 
            this.cbbPg1ChuyenMon.Location = new System.Drawing.Point(354, 52);
            this.cbbPg1ChuyenMon.Name = "cbbPg1ChuyenMon";
            this.cbbPg1ChuyenMon.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChuyenMon.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChuyenMon.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChuyenMon.Properties.NullText = "";
            this.cbbPg1ChuyenMon.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChuyenMon.TabIndex = 4;
            // 
            // cbbPg1ChucVu
            // 
            this.cbbPg1ChucVu.Location = new System.Drawing.Point(541, 24);
            this.cbbPg1ChucVu.Name = "cbbPg1ChucVu";
            this.cbbPg1ChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChucVu.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mã chức vụ", "MaChucVu", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenChucVu", "Tên chức vụ")});
            this.cbbPg1ChucVu.Properties.NullText = "";
            this.cbbPg1ChucVu.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChucVu.TabIndex = 4;
            // 
            // cbbPg1TinhTrangLamViec
            // 
            this.cbbPg1TinhTrangLamViec.EditValue = "1";
            this.cbbPg1TinhTrangLamViec.Location = new System.Drawing.Point(732, 24);
            this.cbbPg1TinhTrangLamViec.Name = "cbbPg1TinhTrangLamViec";
            this.cbbPg1TinhTrangLamViec.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhTrangLamViec.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhTrangLamViec.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhTrangLamViec.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTrangThai", "Mã trạng thái", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTrangThai", "Tên trạng thái")});
            this.cbbPg1TinhTrangLamViec.Properties.NullText = "";
            this.cbbPg1TinhTrangLamViec.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1TinhTrangLamViec.TabIndex = 4;
            // 
            // dtPg1NgayVaoLam
            // 
            this.dtPg1NgayVaoLam.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayVaoLam.Location = new System.Drawing.Point(354, 24);
            this.dtPg1NgayVaoLam.Name = "dtPg1NgayVaoLam";
            this.dtPg1NgayVaoLam.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoLam.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoLam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoLam.Size = new System.Drawing.Size(120, 20);
            this.dtPg1NgayVaoLam.TabIndex = 2;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(273, 27);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(75, 15);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Ngày vào làm";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(858, 55);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(22, 15);
            this.labelControl23.TabIndex = 0;
            this.labelControl23.Text = "Bậc";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(273, 55);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(70, 15);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "Chuyên môn";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(670, 55);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(29, 15);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "Hạng";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Location = new System.Drawing.Point(666, 27);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(56, 15);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Trạng thái";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(480, 55);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(55, 15);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "Mã ngạch";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(480, 27);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(47, 15);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "Chức vụ";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(6, 55);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(94, 15);
            this.labelControl31.TabIndex = 0;
            this.labelControl31.Text = "B.Môn/B.Phận (*)";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(6, 27);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(84, 15);
            this.labelControl32.TabIndex = 0;
            this.labelControl32.Text = "Phòng/Khoa (*)";
            // 
            // groupControl10
            // 
            this.groupControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl10.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.Appearance.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.cbbPg1QuocTich);
            this.groupControl10.Controls.Add(this.cbbPg1VanHoa);
            this.groupControl10.Controls.Add(this.cbbPg1TinhTrangHonNhan);
            this.groupControl10.Controls.Add(this.cbbPg1NoiCapCMND);
            this.groupControl10.Controls.Add(this.cbbPg1NoiSinh);
            this.groupControl10.Controls.Add(this.cbbPg1QueQuan);
            this.groupControl10.Controls.Add(this.cbbPg1TonGiao);
            this.groupControl10.Controls.Add(this.cbbPg1DanToc);
            this.groupControl10.Controls.Add(this.dtPg1NgayCapCMND);
            this.groupControl10.Controls.Add(this.dtPg1NgaySinh);
            this.groupControl10.Controls.Add(this.rdbPg1GioiTinh);
            this.groupControl10.Controls.Add(this.labelControl15);
            this.groupControl10.Controls.Add(this.txtPg1Ten);
            this.groupControl10.Controls.Add(this.labelControl16);
            this.groupControl10.Controls.Add(this.labelControl17);
            this.groupControl10.Controls.Add(this.labelControl18);
            this.groupControl10.Controls.Add(this.labelControl37);
            this.groupControl10.Controls.Add(this.labelControl39);
            this.groupControl10.Controls.Add(this.labelControl41);
            this.groupControl10.Controls.Add(this.labelControl48);
            this.groupControl10.Controls.Add(this.labelControl49);
            this.groupControl10.Controls.Add(this.labelControl50);
            this.groupControl10.Controls.Add(this.txtPg1SoCMND);
            this.groupControl10.Controls.Add(this.labelControl51);
            this.groupControl10.Controls.Add(this.labelControl52);
            this.groupControl10.Controls.Add(this.labelControl53);
            this.groupControl10.Controls.Add(this.labelControl54);
            this.groupControl10.Controls.Add(this.txtPg1TenKhac);
            this.groupControl10.Controls.Add(this.labelControl55);
            this.groupControl10.Controls.Add(this.txtPg1HoDem);
            this.groupControl10.Location = new System.Drawing.Point(3, 3);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(942, 109);
            this.groupControl10.TabIndex = 0;
            this.groupControl10.Text = "Thông tin cơ bản";
            // 
            // cbbPg1QuocTich
            // 
            this.cbbPg1QuocTich.EditValue = "Việt Nam";
            this.cbbPg1QuocTich.Location = new System.Drawing.Point(995, 24);
            this.cbbPg1QuocTich.Name = "cbbPg1QuocTich";
            this.cbbPg1QuocTich.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuocTich.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuocTich.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuocTich.Properties.Items.AddRange(new object[] {
            "Việt Nam"});
            this.cbbPg1QuocTich.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1QuocTich.TabIndex = 5;
            // 
            // cbbPg1VanHoa
            // 
            this.cbbPg1VanHoa.EditValue = "12/12";
            this.cbbPg1VanHoa.Location = new System.Drawing.Point(669, 80);
            this.cbbPg1VanHoa.Name = "cbbPg1VanHoa";
            this.cbbPg1VanHoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1VanHoa.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1VanHoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1VanHoa.Properties.Items.AddRange(new object[] {
            "10/10",
            "12/12"});
            this.cbbPg1VanHoa.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1VanHoa.TabIndex = 5;
            // 
            // cbbPg1TinhTrangHonNhan
            // 
            this.cbbPg1TinhTrangHonNhan.EditValue = "Độc thân";
            this.cbbPg1TinhTrangHonNhan.Location = new System.Drawing.Point(887, 80);
            this.cbbPg1TinhTrangHonNhan.Name = "cbbPg1TinhTrangHonNhan";
            this.cbbPg1TinhTrangHonNhan.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhTrangHonNhan.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhTrangHonNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhTrangHonNhan.Properties.Items.AddRange(new object[] {
            "Độc thân",
            "Có gia đình"});
            this.cbbPg1TinhTrangHonNhan.Size = new System.Drawing.Size(205, 22);
            this.cbbPg1TinhTrangHonNhan.TabIndex = 5;
            // 
            // cbbPg1NoiCapCMND
            // 
            this.cbbPg1NoiCapCMND.Location = new System.Drawing.Point(669, 52);
            this.cbbPg1NoiCapCMND.Name = "cbbPg1NoiCapCMND";
            this.cbbPg1NoiCapCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1NoiCapCMND.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1NoiCapCMND.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1NoiCapCMND.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1NoiCapCMND.Properties.NullText = "";
            this.cbbPg1NoiCapCMND.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1NoiCapCMND.TabIndex = 4;
            // 
            // cbbPg1NoiSinh
            // 
            this.cbbPg1NoiSinh.Location = new System.Drawing.Point(225, 80);
            this.cbbPg1NoiSinh.Name = "cbbPg1NoiSinh";
            this.cbbPg1NoiSinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1NoiSinh.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1NoiSinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1NoiSinh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1NoiSinh.Properties.NullText = "";
            this.cbbPg1NoiSinh.Size = new System.Drawing.Size(158, 22);
            this.cbbPg1NoiSinh.TabIndex = 4;
            // 
            // cbbPg1QueQuan
            // 
            this.cbbPg1QueQuan.Location = new System.Drawing.Point(447, 80);
            this.cbbPg1QueQuan.Name = "cbbPg1QueQuan";
            this.cbbPg1QueQuan.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QueQuan.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QueQuan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QueQuan.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1QueQuan.Properties.NullText = "";
            this.cbbPg1QueQuan.Size = new System.Drawing.Size(150, 22);
            this.cbbPg1QueQuan.TabIndex = 4;
            // 
            // cbbPg1TonGiao
            // 
            this.cbbPg1TonGiao.Location = new System.Drawing.Point(832, 24);
            this.cbbPg1TonGiao.Name = "cbbPg1TonGiao";
            this.cbbPg1TonGiao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TonGiao.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TonGiao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TonGiao.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTonGiao", "Mã tôn giáo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTonGiao", "Tên tôn giáo")});
            this.cbbPg1TonGiao.Properties.NullText = "";
            this.cbbPg1TonGiao.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1TonGiao.TabIndex = 4;
            // 
            // cbbPg1DanToc
            // 
            this.cbbPg1DanToc.Location = new System.Drawing.Point(669, 24);
            this.cbbPg1DanToc.Name = "cbbPg1DanToc";
            this.cbbPg1DanToc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1DanToc.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1DanToc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1DanToc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenDanToc", "Dân tộc"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaDanToc", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cbbPg1DanToc.Properties.NullText = "";
            this.cbbPg1DanToc.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1DanToc.TabIndex = 4;
            // 
            // dtPg1NgayCapCMND
            // 
            this.dtPg1NgayCapCMND.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgayCapCMND.Location = new System.Drawing.Point(833, 52);
            this.dtPg1NgayCapCMND.Name = "dtPg1NgayCapCMND";
            this.dtPg1NgayCapCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayCapCMND.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayCapCMND.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayCapCMND.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayCapCMND.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayCapCMND.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayCapCMND.Size = new System.Drawing.Size(96, 20);
            this.dtPg1NgayCapCMND.TabIndex = 2;
            // 
            // dtPg1NgaySinh
            // 
            this.dtPg1NgaySinh.EditValue = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtPg1NgaySinh.Location = new System.Drawing.Point(67, 80);
            this.dtPg1NgaySinh.Name = "dtPg1NgaySinh";
            this.dtPg1NgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgaySinh.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgaySinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgaySinh.Size = new System.Drawing.Size(99, 20);
            this.dtPg1NgaySinh.TabIndex = 2;
            // 
            // rdbPg1GioiTinh
            // 
            this.rdbPg1GioiTinh.EditValue = 0;
            this.rdbPg1GioiTinh.Location = new System.Drawing.Point(447, 24);
            this.rdbPg1GioiTinh.Name = "rdbPg1GioiTinh";
            this.rdbPg1GioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPg1GioiTinh.Properties.Appearance.Options.UseFont = true;
            this.rdbPg1GioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Nữ")});
            this.rdbPg1GioiTinh.Size = new System.Drawing.Size(150, 22);
            this.rdbPg1GioiTinh.TabIndex = 1;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(243, 27);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(37, 15);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Tên (*)";
            // 
            // txtPg1Ten
            // 
            this.txtPg1Ten.Location = new System.Drawing.Point(286, 24);
            this.txtPg1Ten.Name = "txtPg1Ten";
            this.txtPg1Ten.Properties.AllowFocused = false;
            this.txtPg1Ten.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1Ten.Properties.Appearance.Options.UseFont = true;
            this.txtPg1Ten.Size = new System.Drawing.Size(97, 22);
            this.txtPg1Ten.TabIndex = 0;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(389, 27);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(47, 15);
            this.labelControl16.TabIndex = 0;
            this.labelControl16.Text = "Giới tính";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(6, 83);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(55, 15);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "Sinh ngày";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(935, 27);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(54, 15);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "Quốc tịch";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(389, 83);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(54, 15);
            this.labelControl37.TabIndex = 0;
            this.labelControl37.Text = "Quê quán";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(603, 83);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(46, 15);
            this.labelControl39.TabIndex = 0;
            this.labelControl39.Text = "Văn hóa";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(172, 83);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(47, 15);
            this.labelControl41.TabIndex = 0;
            this.labelControl41.Text = "Nơi sinh";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Location = new System.Drawing.Point(603, 55);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(60, 15);
            this.labelControl48.TabIndex = 0;
            this.labelControl48.Text = "Nơi cấp (*)";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Location = new System.Drawing.Point(772, 27);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(48, 15);
            this.labelControl49.TabIndex = 0;
            this.labelControl49.Text = "Tôn giáo";
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(603, 27);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(43, 15);
            this.labelControl50.TabIndex = 0;
            this.labelControl50.Text = "Dân tộc";
            // 
            // txtPg1SoCMND
            // 
            this.txtPg1SoCMND.Location = new System.Drawing.Point(447, 52);
            this.txtPg1SoCMND.Name = "txtPg1SoCMND";
            this.txtPg1SoCMND.Properties.AllowFocused = false;
            this.txtPg1SoCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1SoCMND.Properties.Appearance.Options.UseFont = true;
            this.txtPg1SoCMND.Size = new System.Drawing.Size(150, 22);
            this.txtPg1SoCMND.TabIndex = 0;
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Location = new System.Drawing.Point(389, 55);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(50, 15);
            this.labelControl51.TabIndex = 0;
            this.labelControl51.Text = "CMND (*)";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Location = new System.Drawing.Point(772, 83);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(112, 15);
            this.labelControl52.TabIndex = 0;
            this.labelControl52.Text = "Tình trạng hôn nhân";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(772, 55);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(52, 15);
            this.labelControl53.TabIndex = 0;
            this.labelControl53.Text = "Ngày cấp";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(6, 55);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(52, 15);
            this.labelControl54.TabIndex = 0;
            this.labelControl54.Text = "Tên khác";
            // 
            // txtPg1TenKhac
            // 
            this.txtPg1TenKhac.Location = new System.Drawing.Point(67, 52);
            this.txtPg1TenKhac.Name = "txtPg1TenKhac";
            this.txtPg1TenKhac.Properties.AllowFocused = false;
            this.txtPg1TenKhac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1TenKhac.Properties.Appearance.Options.UseFont = true;
            this.txtPg1TenKhac.Size = new System.Drawing.Size(316, 22);
            this.txtPg1TenKhac.TabIndex = 0;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Location = new System.Drawing.Point(6, 27);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(32, 15);
            this.labelControl55.TabIndex = 0;
            this.labelControl55.Text = "Họ lót";
            // 
            // txtPg1HoDem
            // 
            this.txtPg1HoDem.Location = new System.Drawing.Point(67, 24);
            this.txtPg1HoDem.Name = "txtPg1HoDem";
            this.txtPg1HoDem.Properties.AllowFocused = false;
            this.txtPg1HoDem.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1HoDem.Properties.Appearance.Options.UseFont = true;
            this.txtPg1HoDem.Size = new System.Drawing.Size(170, 22);
            this.txtPg1HoDem.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.panelControl11);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage3.Text = "Quá trình công tác";
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.memoEdit1);
            this.panelControl11.Controls.Add(this.labelControl71);
            this.panelControl11.Controls.Add(this.textEdit18);
            this.panelControl11.Controls.Add(this.labelControl70);
            this.panelControl11.Controls.Add(this.gridQTCT);
            this.panelControl11.Controls.Add(this.simpleButton8);
            this.panelControl11.Controls.Add(this.simpleButton7);
            this.panelControl11.Controls.Add(this.simpleButton6);
            this.panelControl11.Controls.Add(this.simpleButton5);
            this.panelControl11.Controls.Add(this.simpleButton4);
            this.panelControl11.Controls.Add(this.labelControl69);
            this.panelControl11.Controls.Add(this.dateEdit5);
            this.panelControl11.Controls.Add(this.labelControl68);
            this.panelControl11.Controls.Add(this.dateEdit4);
            this.panelControl11.Controls.Add(this.textEdit17);
            this.panelControl11.Controls.Add(this.labelControl67);
            this.panelControl11.Controls.Add(this.textEdit16);
            this.panelControl11.Controls.Add(this.labelControl66);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl11.Location = new System.Drawing.Point(0, 0);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(948, 493);
            this.panelControl11.TabIndex = 0;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(611, 41);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(310, 31);
            this.memoEdit1.TabIndex = 19;
            // 
            // labelControl71
            // 
            this.labelControl71.Location = new System.Drawing.Point(551, 55);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(39, 13);
            this.labelControl71.TabIndex = 18;
            this.labelControl71.Text = "Ghi chú:";
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(612, 15);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Size = new System.Drawing.Size(309, 20);
            this.textEdit18.TabIndex = 17;
            // 
            // labelControl70
            // 
            this.labelControl70.Location = new System.Drawing.Point(552, 18);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(54, 13);
            this.labelControl70.TabIndex = 16;
            this.labelControl70.Text = "Thành tích:";
            // 
            // gridQTCT
            // 
            this.gridQTCT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQTCT.Location = new System.Drawing.Point(24, 112);
            this.gridQTCT.MainView = this.gridViewQTCT;
            this.gridQTCT.Name = "gridQTCT";
            this.gridQTCT.Size = new System.Drawing.Size(897, 239);
            this.gridQTCT.TabIndex = 15;
            this.gridQTCT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQTCT});
            // 
            // gridViewQTCT
            // 
            this.gridViewQTCT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColID,
            this.gridColNoiCT,
            this.gridColNghenghiep,
            this.gridColTuNgay,
            this.gridColDenNgay,
            this.gridColThanhTich,
            this.gridColGhiChu});
            this.gridViewQTCT.GridControl = this.gridQTCT;
            this.gridViewQTCT.GroupPanelText = "Quá trình học tập, làm việc, công tác";
            this.gridViewQTCT.Name = "gridViewQTCT";
            // 
            // gridColID
            // 
            this.gridColID.Caption = "Mã ID";
            this.gridColID.FieldName = "ID";
            this.gridColID.Name = "gridColID";
            this.gridColID.OptionsColumn.AllowEdit = false;
            this.gridColID.OptionsColumn.ReadOnly = true;
            // 
            // gridColNoiCT
            // 
            this.gridColNoiCT.Caption = "Nơi Học tập/Công tác";
            this.gridColNoiCT.FieldName = "NOICONGTAC";
            this.gridColNoiCT.Name = "gridColNoiCT";
            this.gridColNoiCT.OptionsColumn.AllowEdit = false;
            this.gridColNoiCT.OptionsColumn.ReadOnly = true;
            this.gridColNoiCT.Visible = true;
            this.gridColNoiCT.VisibleIndex = 0;
            // 
            // gridColNghenghiep
            // 
            this.gridColNghenghiep.Caption = "Nghề nghiệp";
            this.gridColNghenghiep.FieldName = "NGHENGHIEP";
            this.gridColNghenghiep.Name = "gridColNghenghiep";
            this.gridColNghenghiep.OptionsColumn.AllowEdit = false;
            this.gridColNghenghiep.OptionsColumn.ReadOnly = true;
            this.gridColNghenghiep.Visible = true;
            this.gridColNghenghiep.VisibleIndex = 1;
            // 
            // gridColTuNgay
            // 
            this.gridColTuNgay.Caption = "Từ ngày";
            this.gridColTuNgay.FieldName = "TUNGAY";
            this.gridColTuNgay.Name = "gridColTuNgay";
            this.gridColTuNgay.OptionsColumn.AllowEdit = false;
            this.gridColTuNgay.OptionsColumn.ReadOnly = true;
            this.gridColTuNgay.Visible = true;
            this.gridColTuNgay.VisibleIndex = 2;
            // 
            // gridColDenNgay
            // 
            this.gridColDenNgay.Caption = "Đến ngày";
            this.gridColDenNgay.FieldName = "DENNGAY";
            this.gridColDenNgay.Name = "gridColDenNgay";
            this.gridColDenNgay.OptionsColumn.AllowEdit = false;
            this.gridColDenNgay.OptionsColumn.ReadOnly = true;
            this.gridColDenNgay.Visible = true;
            this.gridColDenNgay.VisibleIndex = 3;
            // 
            // gridColThanhTich
            // 
            this.gridColThanhTich.Caption = "Thành tích";
            this.gridColThanhTich.FieldName = "THANHTICH";
            this.gridColThanhTich.Name = "gridColThanhTich";
            this.gridColThanhTich.OptionsColumn.AllowEdit = false;
            this.gridColThanhTich.OptionsColumn.ReadOnly = true;
            this.gridColThanhTich.Visible = true;
            this.gridColThanhTich.VisibleIndex = 4;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi chú";
            this.gridColGhiChu.FieldName = "GHICHU";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 5;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(846, 83);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(75, 23);
            this.simpleButton8.TabIndex = 14;
            this.simpleButton8.Text = "Xóa";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(765, 83);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(75, 23);
            this.simpleButton7.TabIndex = 13;
            this.simpleButton7.Text = "Hiệu chỉnh";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(684, 83);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(75, 23);
            this.simpleButton6.TabIndex = 12;
            this.simpleButton6.Text = "Thêm";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(603, 83);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(75, 23);
            this.simpleButton5.TabIndex = 11;
            this.simpleButton5.Text = "Làm mới";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(522, 83);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(75, 23);
            this.simpleButton4.TabIndex = 10;
            this.simpleButton4.Text = "Lưu";
            // 
            // labelControl69
            // 
            this.labelControl69.Location = new System.Drawing.Point(348, 55);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(51, 13);
            this.labelControl69.TabIndex = 7;
            this.labelControl69.Text = "Đến ngày:";
            // 
            // dateEdit5
            // 
            this.dateEdit5.EditValue = null;
            this.dateEdit5.Location = new System.Drawing.Point(408, 52);
            this.dateEdit5.Name = "dateEdit5";
            this.dateEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit5.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit5.Size = new System.Drawing.Size(100, 20);
            this.dateEdit5.TabIndex = 6;
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(348, 18);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(44, 13);
            this.labelControl68.TabIndex = 5;
            this.labelControl68.Text = "Từ ngày:";
            // 
            // dateEdit4
            // 
            this.dateEdit4.EditValue = null;
            this.dateEdit4.Location = new System.Drawing.Point(408, 15);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(100, 20);
            this.dateEdit4.TabIndex = 4;
            // 
            // textEdit17
            // 
            this.textEdit17.Location = new System.Drawing.Point(135, 52);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Size = new System.Drawing.Size(175, 20);
            this.textEdit17.TabIndex = 3;
            // 
            // labelControl67
            // 
            this.labelControl67.Location = new System.Drawing.Point(24, 55);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(64, 13);
            this.labelControl67.TabIndex = 2;
            this.labelControl67.Text = "Nghề nghiệp:";
            // 
            // textEdit16
            // 
            this.textEdit16.Location = new System.Drawing.Point(135, 15);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Size = new System.Drawing.Size(175, 20);
            this.textEdit16.TabIndex = 1;
            // 
            // labelControl66
            // 
            this.labelControl66.Location = new System.Drawing.Point(24, 18);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(105, 13);
            this.labelControl66.TabIndex = 0;
            this.labelControl66.Text = "Nơi học tập/Công tác:\r\n";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl12);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage2.Text = "Bằng cấp - Chứng chỉ";
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.gridBCCC);
            this.panelControl12.Controls.Add(this.simpleButton9);
            this.panelControl12.Controls.Add(this.simpleButton10);
            this.panelControl12.Controls.Add(this.simpleButton11);
            this.panelControl12.Controls.Add(this.simpleButton12);
            this.panelControl12.Controls.Add(this.simpleButton13);
            this.panelControl12.Controls.Add(this.memoEdit2);
            this.panelControl12.Controls.Add(this.labelControl82);
            this.panelControl12.Controls.Add(this.labelControl81);
            this.panelControl12.Controls.Add(this.dateEdit6);
            this.panelControl12.Controls.Add(this.textEdit22);
            this.panelControl12.Controls.Add(this.labelControl80);
            this.panelControl12.Controls.Add(this.textEdit21);
            this.panelControl12.Controls.Add(this.labelControl79);
            this.panelControl12.Controls.Add(this.lookUpEdit21);
            this.panelControl12.Controls.Add(this.labelControl78);
            this.panelControl12.Controls.Add(this.lookUpEdit20);
            this.panelControl12.Controls.Add(this.labelControl77);
            this.panelControl12.Controls.Add(this.lookUpEdit19);
            this.panelControl12.Controls.Add(this.labelControl76);
            this.panelControl12.Controls.Add(this.lookUpEdit18);
            this.panelControl12.Controls.Add(this.labelControl75);
            this.panelControl12.Controls.Add(this.textEdit20);
            this.panelControl12.Controls.Add(this.labelControl74);
            this.panelControl12.Controls.Add(this.lookUpEdit17);
            this.panelControl12.Controls.Add(this.labelControl73);
            this.panelControl12.Controls.Add(this.textEdit19);
            this.panelControl12.Controls.Add(this.labelControl72);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(0, 0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(948, 493);
            this.panelControl12.TabIndex = 0;
            // 
            // gridBCCC
            // 
            this.gridBCCC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBCCC.Location = new System.Drawing.Point(26, 137);
            this.gridBCCC.MainView = this.gridViewBCCC;
            this.gridBCCC.Name = "gridBCCC";
            this.gridBCCC.Size = new System.Drawing.Size(897, 217);
            this.gridBCCC.TabIndex = 101;
            this.gridBCCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBCCC});
            // 
            // gridViewBCCC
            // 
            this.gridViewBCCC.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBCID,
            this.gridColMaBCCC,
            this.gridColLoaiBCCC,
            this.gridColChuyenmon,
            this.gridColTrinhDo,
            this.gridColChuyennganh,
            this.gridColCoSoDT,
            this.gridColTgianDT,
            this.gridColTgHieuluc,
            this.gridColNgayCapBCCC,
            this.gridColHDT,
            this.gridColGHICHUBCCC});
            this.gridViewBCCC.GridControl = this.gridBCCC;
            this.gridViewBCCC.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridViewBCCC.Name = "gridViewBCCC";
            // 
            // gridColBCID
            // 
            this.gridColBCID.Caption = "Mã ID";
            this.gridColBCID.FieldName = "ID";
            this.gridColBCID.Name = "gridColBCID";
            this.gridColBCID.OptionsColumn.AllowEdit = false;
            this.gridColBCID.OptionsColumn.ReadOnly = true;
            // 
            // gridColMaBCCC
            // 
            this.gridColMaBCCC.Caption = "Mã số";
            this.gridColMaBCCC.FieldName = "MaBCCC";
            this.gridColMaBCCC.Name = "gridColMaBCCC";
            this.gridColMaBCCC.OptionsColumn.AllowEdit = false;
            this.gridColMaBCCC.OptionsColumn.ReadOnly = true;
            this.gridColMaBCCC.Visible = true;
            this.gridColMaBCCC.VisibleIndex = 0;
            // 
            // gridColLoaiBCCC
            // 
            this.gridColLoaiBCCC.Caption = "Loại";
            this.gridColLoaiBCCC.FieldName = "LOAIBCCC";
            this.gridColLoaiBCCC.Name = "gridColLoaiBCCC";
            this.gridColLoaiBCCC.OptionsColumn.AllowEdit = false;
            this.gridColLoaiBCCC.OptionsColumn.ReadOnly = true;
            this.gridColLoaiBCCC.Visible = true;
            this.gridColLoaiBCCC.VisibleIndex = 1;
            // 
            // gridColChuyenmon
            // 
            this.gridColChuyenmon.Caption = "Chuyên môn";
            this.gridColChuyenmon.FieldName = "CHUYENMON";
            this.gridColChuyenmon.Name = "gridColChuyenmon";
            this.gridColChuyenmon.OptionsColumn.AllowEdit = false;
            this.gridColChuyenmon.OptionsColumn.ReadOnly = true;
            this.gridColChuyenmon.Visible = true;
            this.gridColChuyenmon.VisibleIndex = 2;
            // 
            // gridColTrinhDo
            // 
            this.gridColTrinhDo.Caption = "Trình độ";
            this.gridColTrinhDo.FieldName = "TRINHDO";
            this.gridColTrinhDo.Name = "gridColTrinhDo";
            this.gridColTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColTrinhDo.Visible = true;
            this.gridColTrinhDo.VisibleIndex = 3;
            // 
            // gridColChuyennganh
            // 
            this.gridColChuyennganh.Caption = "Chuyên ngành";
            this.gridColChuyennganh.FieldName = "TENNGANH";
            this.gridColChuyennganh.Name = "gridColChuyennganh";
            this.gridColChuyennganh.OptionsColumn.AllowEdit = false;
            this.gridColChuyennganh.OptionsColumn.ReadOnly = true;
            this.gridColChuyennganh.Visible = true;
            this.gridColChuyennganh.VisibleIndex = 4;
            // 
            // gridColCoSoDT
            // 
            this.gridColCoSoDT.Caption = "Cơ sở đào tạo";
            this.gridColCoSoDT.FieldName = "TENTRUONG";
            this.gridColCoSoDT.Name = "gridColCoSoDT";
            this.gridColCoSoDT.OptionsColumn.AllowEdit = false;
            this.gridColCoSoDT.OptionsColumn.ReadOnly = true;
            this.gridColCoSoDT.Visible = true;
            this.gridColCoSoDT.VisibleIndex = 5;
            // 
            // gridColTgianDT
            // 
            this.gridColTgianDT.Caption = "Thời gian đào tạo";
            this.gridColTgianDT.FieldName = "THOIGIANDT";
            this.gridColTgianDT.Name = "gridColTgianDT";
            this.gridColTgianDT.OptionsColumn.AllowEdit = false;
            this.gridColTgianDT.OptionsColumn.ReadOnly = true;
            this.gridColTgianDT.Visible = true;
            this.gridColTgianDT.VisibleIndex = 6;
            // 
            // gridColTgHieuluc
            // 
            this.gridColTgHieuluc.Caption = "Hiệu lực";
            this.gridColTgHieuluc.FieldName = "TGHieuLuc";
            this.gridColTgHieuluc.Name = "gridColTgHieuluc";
            this.gridColTgHieuluc.OptionsColumn.AllowEdit = false;
            this.gridColTgHieuluc.OptionsColumn.ReadOnly = true;
            this.gridColTgHieuluc.Visible = true;
            this.gridColTgHieuluc.VisibleIndex = 7;
            // 
            // gridColNgayCapBCCC
            // 
            this.gridColNgayCapBCCC.Caption = "Ngày cấp";
            this.gridColNgayCapBCCC.FieldName = "NGAYCAP";
            this.gridColNgayCapBCCC.Name = "gridColNgayCapBCCC";
            this.gridColNgayCapBCCC.OptionsColumn.AllowEdit = false;
            this.gridColNgayCapBCCC.Visible = true;
            this.gridColNgayCapBCCC.VisibleIndex = 8;
            // 
            // gridColHDT
            // 
            this.gridColHDT.Caption = "Hệ đào tạo";
            this.gridColHDT.FieldName = "HEDAOTAO";
            this.gridColHDT.Name = "gridColHDT";
            this.gridColHDT.OptionsColumn.AllowEdit = false;
            this.gridColHDT.OptionsColumn.ReadOnly = true;
            this.gridColHDT.Visible = true;
            this.gridColHDT.VisibleIndex = 9;
            // 
            // gridColGHICHUBCCC
            // 
            this.gridColGHICHUBCCC.Caption = "Ghi chú";
            this.gridColGHICHUBCCC.FieldName = "GHICHU";
            this.gridColGHICHUBCCC.Name = "gridColGHICHUBCCC";
            this.gridColGHICHUBCCC.OptionsColumn.AllowEdit = false;
            this.gridColGHICHUBCCC.OptionsColumn.ReadOnly = true;
            this.gridColGHICHUBCCC.Visible = true;
            this.gridColGHICHUBCCC.VisibleIndex = 10;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(846, 108);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(75, 23);
            this.simpleButton9.TabIndex = 100;
            this.simpleButton9.Text = "Xóa";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(765, 108);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(75, 23);
            this.simpleButton10.TabIndex = 99;
            this.simpleButton10.Text = "Hiệu chỉnh";
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(684, 108);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(75, 23);
            this.simpleButton11.TabIndex = 98;
            this.simpleButton11.Text = "Thêm";
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(603, 108);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(75, 23);
            this.simpleButton12.TabIndex = 97;
            this.simpleButton12.Text = "Làm mới";
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(522, 108);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(75, 23);
            this.simpleButton13.TabIndex = 96;
            this.simpleButton13.Text = "Lưu";
            // 
            // memoEdit2
            // 
            this.memoEdit2.Location = new System.Drawing.Point(594, 71);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Size = new System.Drawing.Size(326, 31);
            this.memoEdit2.TabIndex = 95;
            // 
            // labelControl82
            // 
            this.labelControl82.Location = new System.Drawing.Point(519, 78);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(39, 13);
            this.labelControl82.TabIndex = 94;
            this.labelControl82.Text = "Ghi chú:";
            // 
            // labelControl81
            // 
            this.labelControl81.Location = new System.Drawing.Point(280, 78);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(49, 13);
            this.labelControl81.TabIndex = 93;
            this.labelControl81.Text = "Ngày cấp:";
            // 
            // dateEdit6
            // 
            this.dateEdit6.EditValue = null;
            this.dateEdit6.Location = new System.Drawing.Point(368, 75);
            this.dateEdit6.Name = "dateEdit6";
            this.dateEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit6.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit6.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit6.Size = new System.Drawing.Size(138, 20);
            this.dateEdit6.TabIndex = 92;
            // 
            // textEdit22
            // 
            this.textEdit22.Location = new System.Drawing.Point(137, 75);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Size = new System.Drawing.Size(131, 20);
            this.textEdit22.TabIndex = 77;
            // 
            // labelControl80
            // 
            this.labelControl80.Location = new System.Drawing.Point(14, 78);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(87, 13);
            this.labelControl80.TabIndex = 76;
            this.labelControl80.Text = "Thời gian hiệu lực:";
            // 
            // textEdit21
            // 
            this.textEdit21.Location = new System.Drawing.Point(811, 45);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Size = new System.Drawing.Size(109, 20);
            this.textEdit21.TabIndex = 75;
            // 
            // labelControl79
            // 
            this.labelControl79.Location = new System.Drawing.Point(721, 48);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(87, 13);
            this.labelControl79.TabIndex = 74;
            this.labelControl79.Text = "Thời gian đào tạo:";
            // 
            // lookUpEdit21
            // 
            this.lookUpEdit21.Location = new System.Drawing.Point(594, 45);
            this.lookUpEdit21.Name = "lookUpEdit21";
            this.lookUpEdit21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit21.Size = new System.Drawing.Size(115, 20);
            this.lookUpEdit21.TabIndex = 73;
            // 
            // labelControl78
            // 
            this.labelControl78.Location = new System.Drawing.Point(519, 48);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(71, 13);
            this.labelControl78.TabIndex = 72;
            this.labelControl78.Text = "Cơ sở đào tạo:";
            // 
            // lookUpEdit20
            // 
            this.lookUpEdit20.Location = new System.Drawing.Point(341, 45);
            this.lookUpEdit20.Name = "lookUpEdit20";
            this.lookUpEdit20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit20.Size = new System.Drawing.Size(165, 20);
            this.lookUpEdit20.TabIndex = 71;
            // 
            // labelControl77
            // 
            this.labelControl77.Location = new System.Drawing.Point(278, 48);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(57, 13);
            this.labelControl77.TabIndex = 70;
            this.labelControl77.Text = "Hệ đào tạo:";
            // 
            // lookUpEdit19
            // 
            this.lookUpEdit19.Location = new System.Drawing.Point(137, 45);
            this.lookUpEdit19.Name = "lookUpEdit19";
            this.lookUpEdit19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit19.Size = new System.Drawing.Size(131, 20);
            this.lookUpEdit19.TabIndex = 69;
            // 
            // labelControl76
            // 
            this.labelControl76.Location = new System.Drawing.Point(14, 48);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(114, 13);
            this.labelControl76.TabIndex = 68;
            this.labelControl76.Text = "Chuyên ngành đào tạo:";
            // 
            // lookUpEdit18
            // 
            this.lookUpEdit18.Location = new System.Drawing.Point(810, 15);
            this.lookUpEdit18.Name = "lookUpEdit18";
            this.lookUpEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit18.Size = new System.Drawing.Size(110, 20);
            this.lookUpEdit18.TabIndex = 67;
            // 
            // labelControl75
            // 
            this.labelControl75.Location = new System.Drawing.Point(721, 18);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(83, 13);
            this.labelControl75.TabIndex = 66;
            this.labelControl75.Text = "Trình độ đào tạo:";
            // 
            // textEdit20
            // 
            this.textEdit20.Location = new System.Drawing.Point(594, 15);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Size = new System.Drawing.Size(115, 20);
            this.textEdit20.TabIndex = 65;
            // 
            // labelControl74
            // 
            this.labelControl74.Location = new System.Drawing.Point(518, 18);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(64, 13);
            this.labelControl74.TabIndex = 64;
            this.labelControl74.Text = "Chuyên môn:";
            // 
            // lookUpEdit17
            // 
            this.lookUpEdit17.Location = new System.Drawing.Point(408, 15);
            this.lookUpEdit17.Name = "lookUpEdit17";
            this.lookUpEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit17.Size = new System.Drawing.Size(98, 20);
            this.lookUpEdit17.TabIndex = 63;
            // 
            // labelControl73
            // 
            this.labelControl73.Location = new System.Drawing.Point(280, 18);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(122, 13);
            this.labelControl73.TabIndex = 2;
            this.labelControl73.Text = "Loại Bằng cấp/Chứng chỉ:";
            // 
            // textEdit19
            // 
            this.textEdit19.Location = new System.Drawing.Point(137, 15);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Size = new System.Drawing.Size(131, 20);
            this.textEdit19.TabIndex = 1;
            // 
            // labelControl72
            // 
            this.labelControl72.Location = new System.Drawing.Point(14, 18);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(117, 13);
            this.labelControl72.TabIndex = 0;
            this.labelControl72.Text = "Mã Bằng cấp/Chứng chỉ:";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.panelControl13);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage4.Text = "Lương";
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.gridLuong);
            this.panelControl13.Controls.Add(this.simpleButton14);
            this.panelControl13.Controls.Add(this.simpleButton15);
            this.panelControl13.Controls.Add(this.simpleButton16);
            this.panelControl13.Controls.Add(this.simpleButton17);
            this.panelControl13.Controls.Add(this.simpleButton18);
            this.panelControl13.Controls.Add(this.dateEdit7);
            this.panelControl13.Controls.Add(this.labelControl93);
            this.panelControl13.Controls.Add(this.textEdit32);
            this.panelControl13.Controls.Add(this.labelControl92);
            this.panelControl13.Controls.Add(this.textEdit31);
            this.panelControl13.Controls.Add(this.labelControl91);
            this.panelControl13.Controls.Add(this.textEdit30);
            this.panelControl13.Controls.Add(this.labelControl90);
            this.panelControl13.Controls.Add(this.textEdit29);
            this.panelControl13.Controls.Add(this.labelControl89);
            this.panelControl13.Controls.Add(this.textEdit28);
            this.panelControl13.Controls.Add(this.labelControl88);
            this.panelControl13.Controls.Add(this.textEdit27);
            this.panelControl13.Controls.Add(this.labelControl87);
            this.panelControl13.Controls.Add(this.textEdit26);
            this.panelControl13.Controls.Add(this.labelControl86);
            this.panelControl13.Controls.Add(this.textEdit25);
            this.panelControl13.Controls.Add(this.labelControl85);
            this.panelControl13.Controls.Add(this.textEdit24);
            this.panelControl13.Controls.Add(this.labelControl84);
            this.panelControl13.Controls.Add(this.textEdit23);
            this.panelControl13.Controls.Add(this.labelControl83);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl13.Location = new System.Drawing.Point(0, 0);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(948, 493);
            this.panelControl13.TabIndex = 0;
            // 
            // gridLuong
            // 
            this.gridLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLuong.Location = new System.Drawing.Point(24, 116);
            this.gridLuong.MainView = this.gridViewLuong;
            this.gridLuong.Name = "gridLuong";
            this.gridLuong.Size = new System.Drawing.Size(897, 243);
            this.gridLuong.TabIndex = 99;
            this.gridLuong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLuong});
            // 
            // gridViewLuong
            // 
            this.gridViewLuong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColLuongID,
            this.gridColLuongSQD,
            this.gridColLuongNgach,
            this.gridColLuongBac,
            this.gridColLuongHeSo,
            this.gridColLuongCB,
            this.gridColLuongChinh,
            this.gridColLuongPCCV,
            this.gridColLuongPCTN,
            this.gridColLuongVuot,
            this.gridColLuongBHXH,
            this.gridColLuongNgayhuong});
            this.gridViewLuong.GridControl = this.gridLuong;
            this.gridViewLuong.GroupPanelText = "Lịch sử Lương";
            this.gridViewLuong.Name = "gridViewLuong";
            // 
            // gridColLuongID
            // 
            this.gridColLuongID.Caption = "Mã ID";
            this.gridColLuongID.FieldName = "ID";
            this.gridColLuongID.Name = "gridColLuongID";
            this.gridColLuongID.OptionsColumn.AllowEdit = false;
            this.gridColLuongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColLuongSQD
            // 
            this.gridColLuongSQD.Caption = "Số QĐ";
            this.gridColLuongSQD.FieldName = "SOQuyetDinh";
            this.gridColLuongSQD.Name = "gridColLuongSQD";
            this.gridColLuongSQD.OptionsColumn.AllowEdit = false;
            this.gridColLuongSQD.OptionsColumn.ReadOnly = true;
            this.gridColLuongSQD.Visible = true;
            this.gridColLuongSQD.VisibleIndex = 0;
            // 
            // gridColLuongNgach
            // 
            this.gridColLuongNgach.Caption = "Ngạch";
            this.gridColLuongNgach.FieldName = "MANGACH";
            this.gridColLuongNgach.Name = "gridColLuongNgach";
            this.gridColLuongNgach.OptionsColumn.AllowEdit = false;
            this.gridColLuongNgach.OptionsColumn.ReadOnly = true;
            this.gridColLuongNgach.Visible = true;
            this.gridColLuongNgach.VisibleIndex = 1;
            // 
            // gridColLuongBac
            // 
            this.gridColLuongBac.Caption = "Bậc";
            this.gridColLuongBac.FieldName = "BACLUONG";
            this.gridColLuongBac.Name = "gridColLuongBac";
            this.gridColLuongBac.OptionsColumn.AllowEdit = false;
            this.gridColLuongBac.OptionsColumn.ReadOnly = true;
            this.gridColLuongBac.Visible = true;
            this.gridColLuongBac.VisibleIndex = 2;
            // 
            // gridColLuongHeSo
            // 
            this.gridColLuongHeSo.Caption = "Hệ số";
            this.gridColLuongHeSo.FieldName = "HESOLUONG";
            this.gridColLuongHeSo.Name = "gridColLuongHeSo";
            this.gridColLuongHeSo.OptionsColumn.AllowEdit = false;
            this.gridColLuongHeSo.OptionsColumn.ReadOnly = true;
            this.gridColLuongHeSo.Visible = true;
            this.gridColLuongHeSo.VisibleIndex = 3;
            // 
            // gridColLuongCB
            // 
            this.gridColLuongCB.Caption = "Lương cơ bản";
            this.gridColLuongCB.FieldName = "LuongCB";
            this.gridColLuongCB.Name = "gridColLuongCB";
            this.gridColLuongCB.OptionsColumn.AllowEdit = false;
            this.gridColLuongCB.OptionsColumn.ReadOnly = true;
            this.gridColLuongCB.Visible = true;
            this.gridColLuongCB.VisibleIndex = 4;
            // 
            // gridColLuongChinh
            // 
            this.gridColLuongChinh.Caption = "Lương chính";
            this.gridColLuongChinh.FieldName = "LUONGCHINH";
            this.gridColLuongChinh.Name = "gridColLuongChinh";
            this.gridColLuongChinh.OptionsColumn.AllowEdit = false;
            this.gridColLuongChinh.OptionsColumn.ReadOnly = true;
            this.gridColLuongChinh.Visible = true;
            this.gridColLuongChinh.VisibleIndex = 5;
            // 
            // gridColLuongPCCV
            // 
            this.gridColLuongPCCV.Caption = "Phụ cấp CV";
            this.gridColLuongPCCV.FieldName = "PCCHUCVU";
            this.gridColLuongPCCV.Name = "gridColLuongPCCV";
            this.gridColLuongPCCV.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCCV.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCCV.Visible = true;
            this.gridColLuongPCCV.VisibleIndex = 6;
            // 
            // gridColLuongPCTN
            // 
            this.gridColLuongPCTN.Caption = "Phụ cấp thâm niên";
            this.gridColLuongPCTN.FieldName = "PCTHAMNIEN";
            this.gridColLuongPCTN.Name = "gridColLuongPCTN";
            this.gridColLuongPCTN.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCTN.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCTN.Visible = true;
            this.gridColLuongPCTN.VisibleIndex = 7;
            // 
            // gridColLuongVuot
            // 
            this.gridColLuongVuot.Caption = "Tỉ lệ vượt khung";
            this.gridColLuongVuot.FieldName = "TILEVUOTKHUNG";
            this.gridColLuongVuot.Name = "gridColLuongVuot";
            this.gridColLuongVuot.OptionsColumn.AllowEdit = false;
            this.gridColLuongVuot.OptionsColumn.ReadOnly = true;
            this.gridColLuongVuot.Visible = true;
            this.gridColLuongVuot.VisibleIndex = 8;
            // 
            // gridColLuongBHXH
            // 
            this.gridColLuongBHXH.Caption = "Đóng BHXH";
            this.gridColLuongBHXH.FieldName = "LUONGBHXH";
            this.gridColLuongBHXH.Name = "gridColLuongBHXH";
            this.gridColLuongBHXH.OptionsColumn.AllowEdit = false;
            this.gridColLuongBHXH.OptionsColumn.ReadOnly = true;
            this.gridColLuongBHXH.Visible = true;
            this.gridColLuongBHXH.VisibleIndex = 9;
            // 
            // gridColLuongNgayhuong
            // 
            this.gridColLuongNgayhuong.Caption = "Ngày hưởng";
            this.gridColLuongNgayhuong.FieldName = "MGAYHUONG";
            this.gridColLuongNgayhuong.Name = "gridColLuongNgayhuong";
            this.gridColLuongNgayhuong.OptionsColumn.AllowEdit = false;
            this.gridColLuongNgayhuong.OptionsColumn.ReadOnly = true;
            this.gridColLuongNgayhuong.Visible = true;
            this.gridColLuongNgayhuong.VisibleIndex = 10;
            // 
            // simpleButton14
            // 
            this.simpleButton14.Location = new System.Drawing.Point(846, 87);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(75, 23);
            this.simpleButton14.TabIndex = 98;
            this.simpleButton14.Text = "Xóa";
            // 
            // simpleButton15
            // 
            this.simpleButton15.Location = new System.Drawing.Point(765, 87);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(75, 23);
            this.simpleButton15.TabIndex = 97;
            this.simpleButton15.Text = "Hiệu chỉnh";
            // 
            // simpleButton16
            // 
            this.simpleButton16.Location = new System.Drawing.Point(684, 87);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(75, 23);
            this.simpleButton16.TabIndex = 96;
            this.simpleButton16.Text = "Cập nhật";
            // 
            // simpleButton17
            // 
            this.simpleButton17.Location = new System.Drawing.Point(603, 87);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(75, 23);
            this.simpleButton17.TabIndex = 95;
            this.simpleButton17.Text = "Làm mới";
            // 
            // simpleButton18
            // 
            this.simpleButton18.Location = new System.Drawing.Point(522, 87);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(75, 23);
            this.simpleButton18.TabIndex = 94;
            this.simpleButton18.Text = "Lưu";
            // 
            // dateEdit7
            // 
            this.dateEdit7.EditValue = null;
            this.dateEdit7.Location = new System.Drawing.Point(846, 52);
            this.dateEdit7.Name = "dateEdit7";
            this.dateEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit7.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit7.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit7.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit7.Size = new System.Drawing.Size(75, 20);
            this.dateEdit7.TabIndex = 93;
            // 
            // labelControl93
            // 
            this.labelControl93.Location = new System.Drawing.Point(777, 55);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(63, 13);
            this.labelControl93.TabIndex = 20;
            this.labelControl93.Text = "Ngày hưởng:";
            // 
            // textEdit32
            // 
            this.textEdit32.Location = new System.Drawing.Point(671, 52);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Size = new System.Drawing.Size(92, 20);
            this.textEdit32.TabIndex = 19;
            // 
            // labelControl92
            // 
            this.labelControl92.Location = new System.Drawing.Point(606, 55);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(59, 13);
            this.labelControl92.TabIndex = 18;
            this.labelControl92.Text = "Đóng BHXH:";
            // 
            // textEdit31
            // 
            this.textEdit31.Location = new System.Drawing.Point(497, 52);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Size = new System.Drawing.Size(92, 20);
            this.textEdit31.TabIndex = 17;
            // 
            // labelControl91
            // 
            this.labelControl91.Location = new System.Drawing.Point(410, 55);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(81, 13);
            this.labelControl91.TabIndex = 16;
            this.labelControl91.Text = "Tỉ lệ vượt khung:";
            // 
            // textEdit30
            // 
            this.textEdit30.Location = new System.Drawing.Point(304, 52);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Size = new System.Drawing.Size(92, 20);
            this.textEdit30.TabIndex = 15;
            // 
            // labelControl90
            // 
            this.labelControl90.Location = new System.Drawing.Point(206, 55);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(92, 13);
            this.labelControl90.TabIndex = 14;
            this.labelControl90.Text = "Phụ cấp thâm niên:";
            // 
            // textEdit29
            // 
            this.textEdit29.Location = new System.Drawing.Point(103, 52);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Size = new System.Drawing.Size(92, 20);
            this.textEdit29.TabIndex = 13;
            // 
            // labelControl89
            // 
            this.labelControl89.Location = new System.Drawing.Point(14, 55);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(83, 13);
            this.labelControl89.TabIndex = 12;
            this.labelControl89.Text = "Phụ cấp chức vụ:";
            // 
            // textEdit28
            // 
            this.textEdit28.Location = new System.Drawing.Point(811, 15);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Size = new System.Drawing.Size(110, 20);
            this.textEdit28.TabIndex = 11;
            // 
            // labelControl88
            // 
            this.labelControl88.Location = new System.Drawing.Point(746, 18);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(62, 13);
            this.labelControl88.TabIndex = 10;
            this.labelControl88.Text = "Lương chính:";
            // 
            // textEdit27
            // 
            this.textEdit27.Location = new System.Drawing.Point(632, 15);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Size = new System.Drawing.Size(96, 20);
            this.textEdit27.TabIndex = 9;
            // 
            // labelControl87
            // 
            this.labelControl87.Location = new System.Drawing.Point(556, 18);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(69, 13);
            this.labelControl87.TabIndex = 8;
            this.labelControl87.Text = "Lương cơ bản:";
            // 
            // textEdit26
            // 
            this.textEdit26.Location = new System.Drawing.Point(481, 15);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Size = new System.Drawing.Size(53, 20);
            this.textEdit26.TabIndex = 7;
            // 
            // labelControl86
            // 
            this.labelControl86.Location = new System.Drawing.Point(440, 18);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(31, 13);
            this.labelControl86.TabIndex = 6;
            this.labelControl86.Text = "Hệ số:";
            // 
            // textEdit25
            // 
            this.textEdit25.Location = new System.Drawing.Point(378, 15);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Size = new System.Drawing.Size(53, 20);
            this.textEdit25.TabIndex = 5;
            // 
            // labelControl85
            // 
            this.labelControl85.Location = new System.Drawing.Point(351, 18);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(21, 13);
            this.labelControl85.TabIndex = 4;
            this.labelControl85.Text = "Bậc:";
            // 
            // textEdit24
            // 
            this.textEdit24.Location = new System.Drawing.Point(252, 15);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Size = new System.Drawing.Size(85, 20);
            this.textEdit24.TabIndex = 3;
            // 
            // labelControl84
            // 
            this.labelControl84.Location = new System.Drawing.Point(212, 18);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(34, 13);
            this.labelControl84.TabIndex = 2;
            this.labelControl84.Text = "Ngạch:";
            // 
            // textEdit23
            // 
            this.textEdit23.Location = new System.Drawing.Point(91, 15);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Size = new System.Drawing.Size(104, 20);
            this.textEdit23.TabIndex = 1;
            // 
            // labelControl83
            // 
            this.labelControl83.Location = new System.Drawing.Point(14, 18);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(70, 13);
            this.labelControl83.TabIndex = 0;
            this.labelControl83.Text = "Số quyết định:";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.panelControl14);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage5.Text = "Đảng viên";
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.simpleButton19);
            this.panelControl14.Controls.Add(this.simpleButton20);
            this.panelControl14.Controls.Add(this.simpleButton21);
            this.panelControl14.Controls.Add(this.simpleButton22);
            this.panelControl14.Controls.Add(this.dateEdit10);
            this.panelControl14.Controls.Add(this.labelControl101);
            this.panelControl14.Controls.Add(this.dateEdit9);
            this.panelControl14.Controls.Add(this.labelControl100);
            this.panelControl14.Controls.Add(this.textEdit37);
            this.panelControl14.Controls.Add(this.labelControl99);
            this.panelControl14.Controls.Add(this.textEdit36);
            this.panelControl14.Controls.Add(this.labelControl98);
            this.panelControl14.Controls.Add(this.textEdit35);
            this.panelControl14.Controls.Add(this.labelControl97);
            this.panelControl14.Controls.Add(this.dateEdit8);
            this.panelControl14.Controls.Add(this.labelControl96);
            this.panelControl14.Controls.Add(this.textEdit34);
            this.panelControl14.Controls.Add(this.labelControl95);
            this.panelControl14.Controls.Add(this.textEdit33);
            this.panelControl14.Controls.Add(this.labelControl94);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(948, 493);
            this.panelControl14.TabIndex = 0;
            // 
            // simpleButton19
            // 
            this.simpleButton19.Location = new System.Drawing.Point(764, 187);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(75, 23);
            this.simpleButton19.TabIndex = 102;
            this.simpleButton19.Text = "Xóa";
            // 
            // simpleButton20
            // 
            this.simpleButton20.Location = new System.Drawing.Point(664, 187);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(75, 23);
            this.simpleButton20.TabIndex = 101;
            this.simpleButton20.Text = "Hiệu chỉnh";
            // 
            // simpleButton21
            // 
            this.simpleButton21.Location = new System.Drawing.Point(564, 187);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(75, 23);
            this.simpleButton21.TabIndex = 100;
            this.simpleButton21.Text = "Thêm mới";
            // 
            // simpleButton22
            // 
            this.simpleButton22.Location = new System.Drawing.Point(464, 187);
            this.simpleButton22.Name = "simpleButton22";
            this.simpleButton22.Size = new System.Drawing.Size(75, 23);
            this.simpleButton22.TabIndex = 99;
            this.simpleButton22.Text = "Lưu";
            // 
            // dateEdit10
            // 
            this.dateEdit10.EditValue = null;
            this.dateEdit10.Location = new System.Drawing.Point(442, 134);
            this.dateEdit10.Name = "dateEdit10";
            this.dateEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit10.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit10.Size = new System.Drawing.Size(138, 20);
            this.dateEdit10.TabIndex = 15;
            // 
            // labelControl101
            // 
            this.labelControl101.Location = new System.Drawing.Point(354, 137);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(82, 13);
            this.labelControl101.TabIndex = 14;
            this.labelControl101.Text = "Ngày chính thức:";
            // 
            // dateEdit9
            // 
            this.dateEdit9.EditValue = null;
            this.dateEdit9.Location = new System.Drawing.Point(168, 134);
            this.dateEdit9.Name = "dateEdit9";
            this.dateEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit9.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit9.Size = new System.Drawing.Size(156, 20);
            this.dateEdit9.TabIndex = 13;
            // 
            // labelControl100
            // 
            this.labelControl100.Location = new System.Drawing.Point(98, 137);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(68, 13);
            this.labelControl100.TabIndex = 12;
            this.labelControl100.Text = "Ngày kết nạp:";
            // 
            // textEdit37
            // 
            this.textEdit37.Location = new System.Drawing.Point(708, 85);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Size = new System.Drawing.Size(132, 20);
            this.textEdit37.TabIndex = 11;
            // 
            // labelControl99
            // 
            this.labelControl99.Location = new System.Drawing.Point(614, 88);
            this.labelControl99.Name = "labelControl99";
            this.labelControl99.Size = new System.Drawing.Size(88, 13);
            this.labelControl99.TabIndex = 10;
            this.labelControl99.Text = "Chức vụ Đảng ủy:";
            // 
            // textEdit36
            // 
            this.textEdit36.Location = new System.Drawing.Point(424, 85);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Size = new System.Drawing.Size(156, 20);
            this.textEdit36.TabIndex = 9;
            // 
            // labelControl98
            // 
            this.labelControl98.Location = new System.Drawing.Point(354, 88);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(45, 13);
            this.labelControl98.TabIndex = 8;
            this.labelControl98.Text = "Đảng bộ:";
            // 
            // textEdit35
            // 
            this.textEdit35.Location = new System.Drawing.Point(168, 85);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Size = new System.Drawing.Size(156, 20);
            this.textEdit35.TabIndex = 7;
            // 
            // labelControl97
            // 
            this.labelControl97.Location = new System.Drawing.Point(98, 88);
            this.labelControl97.Name = "labelControl97";
            this.labelControl97.Size = new System.Drawing.Size(34, 13);
            this.labelControl97.TabIndex = 6;
            this.labelControl97.Text = "Chi bộ:";
            // 
            // dateEdit8
            // 
            this.dateEdit8.EditValue = null;
            this.dateEdit8.Location = new System.Drawing.Point(424, 34);
            this.dateEdit8.Name = "dateEdit8";
            this.dateEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit8.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit8.Size = new System.Drawing.Size(156, 20);
            this.dateEdit8.TabIndex = 5;
            // 
            // labelControl96
            // 
            this.labelControl96.Location = new System.Drawing.Point(354, 37);
            this.labelControl96.Name = "labelControl96";
            this.labelControl96.Size = new System.Drawing.Size(49, 13);
            this.labelControl96.TabIndex = 4;
            this.labelControl96.Text = "Ngày cấp:";
            // 
            // textEdit34
            // 
            this.textEdit34.Location = new System.Drawing.Point(684, 30);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Size = new System.Drawing.Size(156, 20);
            this.textEdit34.TabIndex = 3;
            // 
            // labelControl95
            // 
            this.labelControl95.Location = new System.Drawing.Point(611, 37);
            this.labelControl95.Name = "labelControl95";
            this.labelControl95.Size = new System.Drawing.Size(39, 13);
            this.labelControl95.TabIndex = 2;
            this.labelControl95.Text = "Bí danh:";
            // 
            // textEdit33
            // 
            this.textEdit33.Location = new System.Drawing.Point(168, 34);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Size = new System.Drawing.Size(156, 20);
            this.textEdit33.TabIndex = 1;
            // 
            // labelControl94
            // 
            this.labelControl94.Location = new System.Drawing.Point(98, 37);
            this.labelControl94.Name = "labelControl94";
            this.labelControl94.Size = new System.Drawing.Size(64, 13);
            this.labelControl94.TabIndex = 0;
            this.labelControl94.Text = "Số thẻ Đảng:";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.panelControl15);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage6.Text = "Quan hệ gia đình";
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.simpleButton23);
            this.panelControl15.Controls.Add(this.simpleButton24);
            this.panelControl15.Controls.Add(this.simpleButton25);
            this.panelControl15.Controls.Add(this.simpleButton26);
            this.panelControl15.Controls.Add(this.simpleButton27);
            this.panelControl15.Controls.Add(this.gridQHGD);
            this.panelControl15.Controls.Add(this.memoEdit4);
            this.panelControl15.Controls.Add(this.labelControl110);
            this.panelControl15.Controls.Add(this.memoEdit3);
            this.panelControl15.Controls.Add(this.labelControl109);
            this.panelControl15.Controls.Add(this.groupBox16);
            this.panelControl15.Controls.Add(this.textEdit39);
            this.panelControl15.Controls.Add(this.labelControl105);
            this.panelControl15.Controls.Add(this.dateEdit11);
            this.panelControl15.Controls.Add(this.labelControl104);
            this.panelControl15.Controls.Add(this.textEdit38);
            this.panelControl15.Controls.Add(this.labelControl103);
            this.panelControl15.Controls.Add(this.lookUpEdit22);
            this.panelControl15.Controls.Add(this.labelControl102);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl15.Location = new System.Drawing.Point(0, 0);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(948, 493);
            this.panelControl15.TabIndex = 0;
            // 
            // simpleButton23
            // 
            this.simpleButton23.Location = new System.Drawing.Point(846, 126);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(75, 23);
            this.simpleButton23.TabIndex = 105;
            this.simpleButton23.Text = "Xóa";
            // 
            // simpleButton24
            // 
            this.simpleButton24.Location = new System.Drawing.Point(765, 126);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(75, 23);
            this.simpleButton24.TabIndex = 104;
            this.simpleButton24.Text = "Hiệu chỉnh";
            // 
            // simpleButton25
            // 
            this.simpleButton25.Location = new System.Drawing.Point(684, 126);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(75, 23);
            this.simpleButton25.TabIndex = 103;
            this.simpleButton25.Text = "Thêm";
            // 
            // simpleButton26
            // 
            this.simpleButton26.Location = new System.Drawing.Point(603, 126);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(75, 23);
            this.simpleButton26.TabIndex = 102;
            this.simpleButton26.Text = "Làm mới";
            // 
            // simpleButton27
            // 
            this.simpleButton27.Location = new System.Drawing.Point(522, 126);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(75, 23);
            this.simpleButton27.TabIndex = 101;
            this.simpleButton27.Text = "Lưu";
            // 
            // gridQHGD
            // 
            this.gridQHGD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQHGD.Location = new System.Drawing.Point(26, 155);
            this.gridQHGD.MainView = this.gridViewQHGD;
            this.gridQHGD.Name = "gridQHGD";
            this.gridQHGD.Size = new System.Drawing.Size(897, 199);
            this.gridQHGD.TabIndex = 16;
            this.gridQHGD.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQHGD});
            // 
            // gridViewQHGD
            // 
            this.gridViewQHGD.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColQHGDID,
            this.gridColQHGDHoTen,
            this.gridColQHGDQuanhe,
            this.gridColQHGDNgaySinh,
            this.gridColQHGDNgheNghiep,
            this.gridColQHGDNoiO,
            this.gridColQHGDDacDiem,
            this.gridColQHGDGhiChu});
            this.gridViewQHGD.GridControl = this.gridQHGD;
            this.gridViewQHGD.GroupPanelText = "Quan hệ gia đình";
            this.gridViewQHGD.Name = "gridViewQHGD";
            // 
            // gridColQHGDID
            // 
            this.gridColQHGDID.Caption = "Mã ID";
            this.gridColQHGDID.FieldName = "ID";
            this.gridColQHGDID.Name = "gridColQHGDID";
            this.gridColQHGDID.OptionsColumn.AllowEdit = false;
            this.gridColQHGDID.OptionsColumn.ReadOnly = true;
            // 
            // gridColQHGDHoTen
            // 
            this.gridColQHGDHoTen.Caption = "Họ tên";
            this.gridColQHGDHoTen.FieldName = "HOTEN";
            this.gridColQHGDHoTen.Name = "gridColQHGDHoTen";
            this.gridColQHGDHoTen.OptionsColumn.AllowEdit = false;
            this.gridColQHGDHoTen.OptionsColumn.ReadOnly = true;
            this.gridColQHGDHoTen.Visible = true;
            this.gridColQHGDHoTen.VisibleIndex = 0;
            // 
            // gridColQHGDQuanhe
            // 
            this.gridColQHGDQuanhe.Caption = "Quan hệ thân nhân";
            this.gridColQHGDQuanhe.FieldName = "TENQUANHE";
            this.gridColQHGDQuanhe.Name = "gridColQHGDQuanhe";
            this.gridColQHGDQuanhe.OptionsColumn.AllowEdit = false;
            this.gridColQHGDQuanhe.OptionsColumn.ReadOnly = true;
            this.gridColQHGDQuanhe.Visible = true;
            this.gridColQHGDQuanhe.VisibleIndex = 1;
            // 
            // gridColQHGDNgaySinh
            // 
            this.gridColQHGDNgaySinh.Caption = "Ngày sinh";
            this.gridColQHGDNgaySinh.FieldName = "NGAYSINH";
            this.gridColQHGDNgaySinh.Name = "gridColQHGDNgaySinh";
            this.gridColQHGDNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColQHGDNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColQHGDNgaySinh.Visible = true;
            this.gridColQHGDNgaySinh.VisibleIndex = 2;
            // 
            // gridColQHGDNgheNghiep
            // 
            this.gridColQHGDNgheNghiep.Caption = "Nghề nghiệp";
            this.gridColQHGDNgheNghiep.FieldName = "NGHENGHIEP";
            this.gridColQHGDNgheNghiep.Name = "gridColQHGDNgheNghiep";
            this.gridColQHGDNgheNghiep.OptionsColumn.AllowEdit = false;
            this.gridColQHGDNgheNghiep.OptionsColumn.ReadOnly = true;
            this.gridColQHGDNgheNghiep.Visible = true;
            this.gridColQHGDNgheNghiep.VisibleIndex = 3;
            // 
            // gridColQHGDNoiO
            // 
            this.gridColQHGDNoiO.Caption = "Nơi ở";
            this.gridColQHGDNoiO.FieldName = "NOIO";
            this.gridColQHGDNoiO.Name = "gridColQHGDNoiO";
            this.gridColQHGDNoiO.OptionsColumn.AllowEdit = false;
            this.gridColQHGDNoiO.OptionsColumn.ReadOnly = true;
            this.gridColQHGDNoiO.Visible = true;
            this.gridColQHGDNoiO.VisibleIndex = 4;
            // 
            // gridColQHGDDacDiem
            // 
            this.gridColQHGDDacDiem.Caption = "Đặc điểm lịch sử";
            this.gridColQHGDDacDiem.FieldName = "DACDIEMLICHSU";
            this.gridColQHGDDacDiem.Name = "gridColQHGDDacDiem";
            this.gridColQHGDDacDiem.OptionsColumn.AllowEdit = false;
            this.gridColQHGDDacDiem.OptionsColumn.ReadOnly = true;
            this.gridColQHGDDacDiem.Visible = true;
            this.gridColQHGDDacDiem.VisibleIndex = 5;
            // 
            // gridColQHGDGhiChu
            // 
            this.gridColQHGDGhiChu.Caption = "Ghi chú";
            this.gridColQHGDGhiChu.FieldName = "GHICHU";
            this.gridColQHGDGhiChu.Name = "gridColQHGDGhiChu";
            this.gridColQHGDGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColQHGDGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColQHGDGhiChu.Visible = true;
            this.gridColQHGDGhiChu.VisibleIndex = 6;
            // 
            // memoEdit4
            // 
            this.memoEdit4.Location = new System.Drawing.Point(656, 72);
            this.memoEdit4.Name = "memoEdit4";
            this.memoEdit4.Size = new System.Drawing.Size(265, 48);
            this.memoEdit4.TabIndex = 12;
            // 
            // labelControl110
            // 
            this.labelControl110.Location = new System.Drawing.Point(611, 78);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(39, 13);
            this.labelControl110.TabIndex = 11;
            this.labelControl110.Text = "Ghi chú:";
            // 
            // memoEdit3
            // 
            this.memoEdit3.Location = new System.Drawing.Point(129, 75);
            this.memoEdit3.Name = "memoEdit3";
            this.memoEdit3.Size = new System.Drawing.Size(428, 45);
            this.memoEdit3.TabIndex = 10;
            // 
            // labelControl109
            // 
            this.labelControl109.Location = new System.Drawing.Point(26, 75);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(81, 13);
            this.labelControl109.TabIndex = 9;
            this.labelControl109.Text = "Đặc điểm lịch sử:";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.lookUpEdit24);
            this.groupBox16.Controls.Add(this.labelControl108);
            this.groupBox16.Controls.Add(this.lookUpEdit23);
            this.groupBox16.Controls.Add(this.labelControl107);
            this.groupBox16.Controls.Add(this.textEdit40);
            this.groupBox16.Controls.Add(this.labelControl106);
            this.groupBox16.Location = new System.Drawing.Point(26, 29);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(895, 41);
            this.groupBox16.TabIndex = 8;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Nơi ở";
            // 
            // lookUpEdit24
            // 
            this.lookUpEdit24.Location = new System.Drawing.Point(673, 14);
            this.lookUpEdit24.Name = "lookUpEdit24";
            this.lookUpEdit24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit24.Size = new System.Drawing.Size(153, 20);
            this.lookUpEdit24.TabIndex = 12;
            // 
            // labelControl108
            // 
            this.labelControl108.Location = new System.Drawing.Point(602, 17);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(65, 13);
            this.labelControl108.TabIndex = 11;
            this.labelControl108.Text = "Quận/Huyện:";
            // 
            // lookUpEdit23
            // 
            this.lookUpEdit23.Location = new System.Drawing.Point(404, 14);
            this.lookUpEdit23.Name = "lookUpEdit23";
            this.lookUpEdit23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit23.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit23.TabIndex = 10;
            // 
            // labelControl107
            // 
            this.labelControl107.Location = new System.Drawing.Point(319, 17);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(79, 13);
            this.labelControl107.TabIndex = 9;
            this.labelControl107.Text = "Tỉnh/Thành phố:";
            // 
            // textEdit40
            // 
            this.textEdit40.Location = new System.Drawing.Point(59, 14);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Size = new System.Drawing.Size(213, 20);
            this.textEdit40.TabIndex = 5;
            // 
            // labelControl106
            // 
            this.labelControl106.Location = new System.Drawing.Point(17, 17);
            this.labelControl106.Name = "labelControl106";
            this.labelControl106.Size = new System.Drawing.Size(36, 13);
            this.labelControl106.TabIndex = 4;
            this.labelControl106.Text = "Địa chỉ:";
            // 
            // textEdit39
            // 
            this.textEdit39.Location = new System.Drawing.Point(739, 7);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Size = new System.Drawing.Size(182, 20);
            this.textEdit39.TabIndex = 7;
            // 
            // labelControl105
            // 
            this.labelControl105.Location = new System.Drawing.Point(669, 10);
            this.labelControl105.Name = "labelControl105";
            this.labelControl105.Size = new System.Drawing.Size(64, 13);
            this.labelControl105.TabIndex = 6;
            this.labelControl105.Text = "Nghề nghiệp:";
            // 
            // dateEdit11
            // 
            this.dateEdit11.EditValue = null;
            this.dateEdit11.Location = new System.Drawing.Point(536, 7);
            this.dateEdit11.Name = "dateEdit11";
            this.dateEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit11.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit11.Size = new System.Drawing.Size(100, 20);
            this.dateEdit11.TabIndex = 5;
            // 
            // labelControl104
            // 
            this.labelControl104.Location = new System.Drawing.Point(479, 10);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(51, 13);
            this.labelControl104.TabIndex = 4;
            this.labelControl104.Text = "Ngày sinh:";
            // 
            // textEdit38
            // 
            this.textEdit38.Location = new System.Drawing.Point(294, 7);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Size = new System.Drawing.Size(161, 20);
            this.textEdit38.TabIndex = 3;
            // 
            // labelControl103
            // 
            this.labelControl103.Location = new System.Drawing.Point(252, 10);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(36, 13);
            this.labelControl103.TabIndex = 2;
            this.labelControl103.Text = "Họ tên:";
            // 
            // lookUpEdit22
            // 
            this.lookUpEdit22.Location = new System.Drawing.Point(129, 7);
            this.lookUpEdit22.Name = "lookUpEdit22";
            this.lookUpEdit22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit22.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit22.TabIndex = 1;
            // 
            // labelControl102
            // 
            this.labelControl102.Location = new System.Drawing.Point(26, 10);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(97, 13);
            this.labelControl102.TabIndex = 0;
            this.labelControl102.Text = "Quan hệ thân nhân:";
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.panelControl16);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage7.Text = "Hợp đồng";
            // 
            // panelControl16
            // 
            this.panelControl16.Controls.Add(this.gridHopdong);
            this.panelControl16.Controls.Add(this.simpleButton28);
            this.panelControl16.Controls.Add(this.simpleButton29);
            this.panelControl16.Controls.Add(this.simpleButton30);
            this.panelControl16.Controls.Add(this.simpleButton31);
            this.panelControl16.Controls.Add(this.simpleButton32);
            this.panelControl16.Controls.Add(this.memoEdit5);
            this.panelControl16.Controls.Add(this.labelControl117);
            this.panelControl16.Controls.Add(this.dateEdit14);
            this.panelControl16.Controls.Add(this.labelControl116);
            this.panelControl16.Controls.Add(this.dateEdit13);
            this.panelControl16.Controls.Add(this.labelControl115);
            this.panelControl16.Controls.Add(this.textEdit42);
            this.panelControl16.Controls.Add(this.labelControl114);
            this.panelControl16.Controls.Add(this.dateEdit12);
            this.panelControl16.Controls.Add(this.labelControl113);
            this.panelControl16.Controls.Add(this.lookUpEdit25);
            this.panelControl16.Controls.Add(this.labelControl112);
            this.panelControl16.Controls.Add(this.textEdit41);
            this.panelControl16.Controls.Add(this.labelControl111);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(0, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(948, 493);
            this.panelControl16.TabIndex = 0;
            // 
            // gridHopdong
            // 
            this.gridHopdong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHopdong.Location = new System.Drawing.Point(26, 132);
            this.gridHopdong.MainView = this.gridViewHopDong;
            this.gridHopdong.Name = "gridHopdong";
            this.gridHopdong.Size = new System.Drawing.Size(897, 222);
            this.gridHopdong.TabIndex = 106;
            this.gridHopdong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHopDong});
            // 
            // gridViewHopDong
            // 
            this.gridViewHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColHopDongMaHD,
            this.gridColHopDongLoaiHD,
            this.gridColHopDongNgayky,
            this.gridColHopDongNguoiky,
            this.gridColHopDongTungay,
            this.gridColHopDongDenngay,
            this.gridColHopDongNoidung});
            this.gridViewHopDong.GridControl = this.gridHopdong;
            this.gridViewHopDong.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewHopDong.Name = "gridViewHopDong";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColHopDongMaHD
            // 
            this.gridColHopDongMaHD.Caption = "Mã Hợp đồng";
            this.gridColHopDongMaHD.FieldName = "MAHD";
            this.gridColHopDongMaHD.Name = "gridColHopDongMaHD";
            this.gridColHopDongMaHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongMaHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongMaHD.Visible = true;
            this.gridColHopDongMaHD.VisibleIndex = 0;
            // 
            // gridColHopDongLoaiHD
            // 
            this.gridColHopDongLoaiHD.Caption = "Loại hợp đồng";
            this.gridColHopDongLoaiHD.FieldName = "TENLOAIHD";
            this.gridColHopDongLoaiHD.Name = "gridColHopDongLoaiHD";
            this.gridColHopDongLoaiHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongLoaiHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongLoaiHD.Visible = true;
            this.gridColHopDongLoaiHD.VisibleIndex = 1;
            // 
            // gridColHopDongNgayky
            // 
            this.gridColHopDongNgayky.Caption = "Ngày ký";
            this.gridColHopDongNgayky.FieldName = "NGAYKY";
            this.gridColHopDongNgayky.Name = "gridColHopDongNgayky";
            this.gridColHopDongNgayky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNgayky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNgayky.Visible = true;
            this.gridColHopDongNgayky.VisibleIndex = 2;
            // 
            // gridColHopDongNguoiky
            // 
            this.gridColHopDongNguoiky.Caption = "Người ký";
            this.gridColHopDongNguoiky.FieldName = "NGUOIKY";
            this.gridColHopDongNguoiky.Name = "gridColHopDongNguoiky";
            this.gridColHopDongNguoiky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNguoiky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNguoiky.Visible = true;
            this.gridColHopDongNguoiky.VisibleIndex = 3;
            // 
            // gridColHopDongTungay
            // 
            this.gridColHopDongTungay.Caption = "Từ ngày";
            this.gridColHopDongTungay.FieldName = "TUNGAY";
            this.gridColHopDongTungay.Name = "gridColHopDongTungay";
            this.gridColHopDongTungay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongTungay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongTungay.Visible = true;
            this.gridColHopDongTungay.VisibleIndex = 4;
            // 
            // gridColHopDongDenngay
            // 
            this.gridColHopDongDenngay.Caption = "Đến ngày";
            this.gridColHopDongDenngay.FieldName = "DENNGAY";
            this.gridColHopDongDenngay.Name = "gridColHopDongDenngay";
            this.gridColHopDongDenngay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongDenngay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongDenngay.Visible = true;
            this.gridColHopDongDenngay.VisibleIndex = 5;
            // 
            // gridColHopDongNoidung
            // 
            this.gridColHopDongNoidung.Caption = "Nội dung";
            this.gridColHopDongNoidung.FieldName = "NOIDUNG";
            this.gridColHopDongNoidung.Name = "gridColHopDongNoidung";
            this.gridColHopDongNoidung.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNoidung.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNoidung.Visible = true;
            this.gridColHopDongNoidung.VisibleIndex = 6;
            // 
            // simpleButton28
            // 
            this.simpleButton28.Location = new System.Drawing.Point(846, 103);
            this.simpleButton28.Name = "simpleButton28";
            this.simpleButton28.Size = new System.Drawing.Size(75, 23);
            this.simpleButton28.TabIndex = 105;
            this.simpleButton28.Text = "Xóa";
            // 
            // simpleButton29
            // 
            this.simpleButton29.Location = new System.Drawing.Point(765, 103);
            this.simpleButton29.Name = "simpleButton29";
            this.simpleButton29.Size = new System.Drawing.Size(75, 23);
            this.simpleButton29.TabIndex = 104;
            this.simpleButton29.Text = "Hiệu chỉnh";
            // 
            // simpleButton30
            // 
            this.simpleButton30.Location = new System.Drawing.Point(684, 103);
            this.simpleButton30.Name = "simpleButton30";
            this.simpleButton30.Size = new System.Drawing.Size(75, 23);
            this.simpleButton30.TabIndex = 103;
            this.simpleButton30.Text = "Thêm";
            // 
            // simpleButton31
            // 
            this.simpleButton31.Location = new System.Drawing.Point(603, 103);
            this.simpleButton31.Name = "simpleButton31";
            this.simpleButton31.Size = new System.Drawing.Size(75, 23);
            this.simpleButton31.TabIndex = 102;
            this.simpleButton31.Text = "Làm mới";
            // 
            // simpleButton32
            // 
            this.simpleButton32.Location = new System.Drawing.Point(522, 103);
            this.simpleButton32.Name = "simpleButton32";
            this.simpleButton32.Size = new System.Drawing.Size(75, 23);
            this.simpleButton32.TabIndex = 101;
            this.simpleButton32.Text = "Lưu";
            // 
            // memoEdit5
            // 
            this.memoEdit5.Location = new System.Drawing.Point(526, 42);
            this.memoEdit5.Name = "memoEdit5";
            this.memoEdit5.Size = new System.Drawing.Size(395, 55);
            this.memoEdit5.TabIndex = 73;
            // 
            // labelControl117
            // 
            this.labelControl117.Location = new System.Drawing.Point(474, 59);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(46, 13);
            this.labelControl117.TabIndex = 72;
            this.labelControl117.Text = "Nội dung:";
            // 
            // dateEdit14
            // 
            this.dateEdit14.EditValue = null;
            this.dateEdit14.Location = new System.Drawing.Point(307, 56);
            this.dateEdit14.Name = "dateEdit14";
            this.dateEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit14.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit14.Size = new System.Drawing.Size(131, 20);
            this.dateEdit14.TabIndex = 71;
            // 
            // labelControl116
            // 
            this.labelControl116.Location = new System.Drawing.Point(236, 59);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(51, 13);
            this.labelControl116.TabIndex = 70;
            this.labelControl116.Text = "Đến ngày:";
            // 
            // dateEdit13
            // 
            this.dateEdit13.EditValue = null;
            this.dateEdit13.Location = new System.Drawing.Point(85, 56);
            this.dateEdit13.Name = "dateEdit13";
            this.dateEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit13.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit13.Size = new System.Drawing.Size(117, 20);
            this.dateEdit13.TabIndex = 69;
            // 
            // labelControl115
            // 
            this.labelControl115.Location = new System.Drawing.Point(14, 59);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(44, 13);
            this.labelControl115.TabIndex = 68;
            this.labelControl115.Text = "Từ ngày:";
            // 
            // textEdit42
            // 
            this.textEdit42.Location = new System.Drawing.Point(730, 16);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Size = new System.Drawing.Size(191, 20);
            this.textEdit42.TabIndex = 67;
            // 
            // labelControl114
            // 
            this.labelControl114.Location = new System.Drawing.Point(678, 19);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(46, 13);
            this.labelControl114.TabIndex = 66;
            this.labelControl114.Text = "Người ký:";
            // 
            // dateEdit12
            // 
            this.dateEdit12.EditValue = null;
            this.dateEdit12.Location = new System.Drawing.Point(523, 16);
            this.dateEdit12.Name = "dateEdit12";
            this.dateEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit12.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit12.Size = new System.Drawing.Size(117, 20);
            this.dateEdit12.TabIndex = 65;
            // 
            // labelControl113
            // 
            this.labelControl113.Location = new System.Drawing.Point(474, 19);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(43, 13);
            this.labelControl113.TabIndex = 64;
            this.labelControl113.Text = "Ngày ký:";
            // 
            // lookUpEdit25
            // 
            this.lookUpEdit25.Location = new System.Drawing.Point(283, 16);
            this.lookUpEdit25.Name = "lookUpEdit25";
            this.lookUpEdit25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit25.Size = new System.Drawing.Size(155, 20);
            this.lookUpEdit25.TabIndex = 63;
            // 
            // labelControl112
            // 
            this.labelControl112.Location = new System.Drawing.Point(236, 19);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(41, 13);
            this.labelControl112.TabIndex = 2;
            this.labelControl112.Text = "Loại HĐ:";
            // 
            // textEdit41
            // 
            this.textEdit41.Location = new System.Drawing.Point(56, 16);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Size = new System.Drawing.Size(146, 20);
            this.textEdit41.TabIndex = 1;
            // 
            // labelControl111
            // 
            this.labelControl111.Location = new System.Drawing.Point(14, 19);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(36, 13);
            this.labelControl111.TabIndex = 0;
            this.labelControl111.Text = "Mã HĐ:";
            // 
            // xtraTabPage8
            // 
            this.xtraTabPage8.Controls.Add(this.panelControl17);
            this.xtraTabPage8.Name = "xtraTabPage8";
            this.xtraTabPage8.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage8.Text = "Quyết định";
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.gridQD);
            this.panelControl17.Controls.Add(this.simpleButton33);
            this.panelControl17.Controls.Add(this.simpleButton34);
            this.panelControl17.Controls.Add(this.simpleButton35);
            this.panelControl17.Controls.Add(this.simpleButton36);
            this.panelControl17.Controls.Add(this.simpleButton37);
            this.panelControl17.Controls.Add(this.memoEdit6);
            this.panelControl17.Controls.Add(this.labelControl118);
            this.panelControl17.Controls.Add(this.dateEdit15);
            this.panelControl17.Controls.Add(this.labelControl119);
            this.panelControl17.Controls.Add(this.dateEdit16);
            this.panelControl17.Controls.Add(this.labelControl120);
            this.panelControl17.Controls.Add(this.textEdit43);
            this.panelControl17.Controls.Add(this.labelControl121);
            this.panelControl17.Controls.Add(this.dateEdit17);
            this.panelControl17.Controls.Add(this.labelControl122);
            this.panelControl17.Controls.Add(this.lookUpEdit26);
            this.panelControl17.Controls.Add(this.labelControl123);
            this.panelControl17.Controls.Add(this.textEdit44);
            this.panelControl17.Controls.Add(this.labelControl124);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl17.Location = new System.Drawing.Point(0, 0);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(948, 493);
            this.panelControl17.TabIndex = 0;
            // 
            // gridQD
            // 
            this.gridQD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQD.Location = new System.Drawing.Point(26, 135);
            this.gridQD.MainView = this.gridViewQuyetDinh;
            this.gridQD.Name = "gridQD";
            this.gridQD.Size = new System.Drawing.Size(897, 222);
            this.gridQD.TabIndex = 125;
            this.gridQD.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuyetDinh});
            // 
            // gridViewQuyetDinh
            // 
            this.gridViewQuyetDinh.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColQDID,
            this.gridColQDSoQD,
            this.gridColQDLoaiQD,
            this.gridColQDNgayky,
            this.gridColQDNguoiky,
            this.gridColQDTungay,
            this.gridColQDDenngay,
            this.gridCoQDNoidung});
            this.gridViewQuyetDinh.GridControl = this.gridQD;
            this.gridViewQuyetDinh.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewQuyetDinh.Name = "gridViewQuyetDinh";
            // 
            // gridColQDID
            // 
            this.gridColQDID.Caption = "Mã ID";
            this.gridColQDID.FieldName = "ID";
            this.gridColQDID.Name = "gridColQDID";
            this.gridColQDID.OptionsColumn.AllowEdit = false;
            this.gridColQDID.OptionsColumn.ReadOnly = true;
            // 
            // gridColQDSoQD
            // 
            this.gridColQDSoQD.Caption = "Số QĐ";
            this.gridColQDSoQD.FieldName = "SOQD";
            this.gridColQDSoQD.Name = "gridColQDSoQD";
            this.gridColQDSoQD.OptionsColumn.AllowEdit = false;
            this.gridColQDSoQD.OptionsColumn.ReadOnly = true;
            this.gridColQDSoQD.Visible = true;
            this.gridColQDSoQD.VisibleIndex = 0;
            // 
            // gridColQDLoaiQD
            // 
            this.gridColQDLoaiQD.Caption = "Loại QĐ";
            this.gridColQDLoaiQD.FieldName = "TENLOAIQD";
            this.gridColQDLoaiQD.Name = "gridColQDLoaiQD";
            this.gridColQDLoaiQD.OptionsColumn.AllowEdit = false;
            this.gridColQDLoaiQD.OptionsColumn.ReadOnly = true;
            this.gridColQDLoaiQD.Visible = true;
            this.gridColQDLoaiQD.VisibleIndex = 1;
            // 
            // gridColQDNgayky
            // 
            this.gridColQDNgayky.Caption = "Ngày ký";
            this.gridColQDNgayky.FieldName = "NGAYKY";
            this.gridColQDNgayky.Name = "gridColQDNgayky";
            this.gridColQDNgayky.OptionsColumn.AllowEdit = false;
            this.gridColQDNgayky.OptionsColumn.ReadOnly = true;
            this.gridColQDNgayky.Visible = true;
            this.gridColQDNgayky.VisibleIndex = 2;
            // 
            // gridColQDNguoiky
            // 
            this.gridColQDNguoiky.Caption = "Người ký";
            this.gridColQDNguoiky.FieldName = "NGUOIKY";
            this.gridColQDNguoiky.Name = "gridColQDNguoiky";
            this.gridColQDNguoiky.OptionsColumn.AllowEdit = false;
            this.gridColQDNguoiky.OptionsColumn.ReadOnly = true;
            this.gridColQDNguoiky.Visible = true;
            this.gridColQDNguoiky.VisibleIndex = 3;
            // 
            // gridColQDTungay
            // 
            this.gridColQDTungay.Caption = "Hiệu lực từ";
            this.gridColQDTungay.FieldName = "TUNGAY";
            this.gridColQDTungay.Name = "gridColQDTungay";
            this.gridColQDTungay.OptionsColumn.AllowEdit = false;
            this.gridColQDTungay.OptionsColumn.ReadOnly = true;
            this.gridColQDTungay.Visible = true;
            this.gridColQDTungay.VisibleIndex = 4;
            // 
            // gridColQDDenngay
            // 
            this.gridColQDDenngay.Caption = "Hiệu lực đến";
            this.gridColQDDenngay.FieldName = "DENNGAY";
            this.gridColQDDenngay.Name = "gridColQDDenngay";
            this.gridColQDDenngay.OptionsColumn.AllowEdit = false;
            this.gridColQDDenngay.OptionsColumn.ReadOnly = true;
            this.gridColQDDenngay.Visible = true;
            this.gridColQDDenngay.VisibleIndex = 5;
            // 
            // gridCoQDNoidung
            // 
            this.gridCoQDNoidung.Caption = "Nội dung";
            this.gridCoQDNoidung.FieldName = "NOIDUNG";
            this.gridCoQDNoidung.Name = "gridCoQDNoidung";
            this.gridCoQDNoidung.OptionsColumn.AllowEdit = false;
            this.gridCoQDNoidung.OptionsColumn.ReadOnly = true;
            this.gridCoQDNoidung.Visible = true;
            this.gridCoQDNoidung.VisibleIndex = 6;
            // 
            // simpleButton33
            // 
            this.simpleButton33.Location = new System.Drawing.Point(846, 101);
            this.simpleButton33.Name = "simpleButton33";
            this.simpleButton33.Size = new System.Drawing.Size(75, 23);
            this.simpleButton33.TabIndex = 124;
            this.simpleButton33.Text = "Xóa";
            // 
            // simpleButton34
            // 
            this.simpleButton34.Location = new System.Drawing.Point(765, 101);
            this.simpleButton34.Name = "simpleButton34";
            this.simpleButton34.Size = new System.Drawing.Size(75, 23);
            this.simpleButton34.TabIndex = 123;
            this.simpleButton34.Text = "Hiệu chỉnh";
            // 
            // simpleButton35
            // 
            this.simpleButton35.Location = new System.Drawing.Point(684, 101);
            this.simpleButton35.Name = "simpleButton35";
            this.simpleButton35.Size = new System.Drawing.Size(75, 23);
            this.simpleButton35.TabIndex = 122;
            this.simpleButton35.Text = "Thêm";
            // 
            // simpleButton36
            // 
            this.simpleButton36.Location = new System.Drawing.Point(603, 101);
            this.simpleButton36.Name = "simpleButton36";
            this.simpleButton36.Size = new System.Drawing.Size(75, 23);
            this.simpleButton36.TabIndex = 121;
            this.simpleButton36.Text = "Làm mới";
            // 
            // simpleButton37
            // 
            this.simpleButton37.Location = new System.Drawing.Point(522, 101);
            this.simpleButton37.Name = "simpleButton37";
            this.simpleButton37.Size = new System.Drawing.Size(75, 23);
            this.simpleButton37.TabIndex = 120;
            this.simpleButton37.Text = "Lưu";
            // 
            // memoEdit6
            // 
            this.memoEdit6.Location = new System.Drawing.Point(526, 40);
            this.memoEdit6.Name = "memoEdit6";
            this.memoEdit6.Size = new System.Drawing.Size(395, 55);
            this.memoEdit6.TabIndex = 119;
            // 
            // labelControl118
            // 
            this.labelControl118.Location = new System.Drawing.Point(474, 57);
            this.labelControl118.Name = "labelControl118";
            this.labelControl118.Size = new System.Drawing.Size(46, 13);
            this.labelControl118.TabIndex = 118;
            this.labelControl118.Text = "Nội dung:";
            // 
            // dateEdit15
            // 
            this.dateEdit15.EditValue = null;
            this.dateEdit15.Location = new System.Drawing.Point(307, 54);
            this.dateEdit15.Name = "dateEdit15";
            this.dateEdit15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit15.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit15.Size = new System.Drawing.Size(131, 20);
            this.dateEdit15.TabIndex = 117;
            // 
            // labelControl119
            // 
            this.labelControl119.Location = new System.Drawing.Point(236, 57);
            this.labelControl119.Name = "labelControl119";
            this.labelControl119.Size = new System.Drawing.Size(63, 13);
            this.labelControl119.TabIndex = 116;
            this.labelControl119.Text = "Hiệu lực đến:";
            // 
            // dateEdit16
            // 
            this.dateEdit16.EditValue = null;
            this.dateEdit16.Location = new System.Drawing.Point(85, 54);
            this.dateEdit16.Name = "dateEdit16";
            this.dateEdit16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit16.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit16.Size = new System.Drawing.Size(117, 20);
            this.dateEdit16.TabIndex = 115;
            // 
            // labelControl120
            // 
            this.labelControl120.Location = new System.Drawing.Point(14, 57);
            this.labelControl120.Name = "labelControl120";
            this.labelControl120.Size = new System.Drawing.Size(56, 13);
            this.labelControl120.TabIndex = 114;
            this.labelControl120.Text = "Hiệu lực từ:";
            // 
            // textEdit43
            // 
            this.textEdit43.Location = new System.Drawing.Point(730, 14);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Size = new System.Drawing.Size(191, 20);
            this.textEdit43.TabIndex = 113;
            // 
            // labelControl121
            // 
            this.labelControl121.Location = new System.Drawing.Point(678, 17);
            this.labelControl121.Name = "labelControl121";
            this.labelControl121.Size = new System.Drawing.Size(46, 13);
            this.labelControl121.TabIndex = 112;
            this.labelControl121.Text = "Người ký:";
            // 
            // dateEdit17
            // 
            this.dateEdit17.EditValue = null;
            this.dateEdit17.Location = new System.Drawing.Point(523, 14);
            this.dateEdit17.Name = "dateEdit17";
            this.dateEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit17.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit17.Size = new System.Drawing.Size(117, 20);
            this.dateEdit17.TabIndex = 111;
            // 
            // labelControl122
            // 
            this.labelControl122.Location = new System.Drawing.Point(474, 17);
            this.labelControl122.Name = "labelControl122";
            this.labelControl122.Size = new System.Drawing.Size(43, 13);
            this.labelControl122.TabIndex = 110;
            this.labelControl122.Text = "Ngày ký:";
            // 
            // lookUpEdit26
            // 
            this.lookUpEdit26.Location = new System.Drawing.Point(283, 14);
            this.lookUpEdit26.Name = "lookUpEdit26";
            this.lookUpEdit26.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit26.Size = new System.Drawing.Size(155, 20);
            this.lookUpEdit26.TabIndex = 109;
            // 
            // labelControl123
            // 
            this.labelControl123.Location = new System.Drawing.Point(236, 17);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(42, 13);
            this.labelControl123.TabIndex = 108;
            this.labelControl123.Text = "Loại QĐ:";
            // 
            // textEdit44
            // 
            this.textEdit44.Location = new System.Drawing.Point(56, 14);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Size = new System.Drawing.Size(146, 20);
            this.textEdit44.TabIndex = 107;
            // 
            // labelControl124
            // 
            this.labelControl124.Location = new System.Drawing.Point(14, 17);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(35, 13);
            this.labelControl124.TabIndex = 106;
            this.labelControl124.Text = "Số QĐ:";
            // 
            // xtraTabPage9
            // 
            this.xtraTabPage9.Controls.Add(this.panelControl18);
            this.xtraTabPage9.Name = "xtraTabPage9";
            this.xtraTabPage9.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage9.Text = "Phân công nhiệm vụ";
            // 
            // panelControl18
            // 
            this.panelControl18.Controls.Add(this.gridPhancong);
            this.panelControl18.Controls.Add(this.simpleButton38);
            this.panelControl18.Controls.Add(this.simpleButton39);
            this.panelControl18.Controls.Add(this.simpleButton40);
            this.panelControl18.Controls.Add(this.simpleButton41);
            this.panelControl18.Controls.Add(this.simpleButton42);
            this.panelControl18.Controls.Add(this.checkEdit1);
            this.panelControl18.Controls.Add(this.labelControl128);
            this.panelControl18.Controls.Add(this.dateEdit18);
            this.panelControl18.Controls.Add(this.lookUpEdit29);
            this.panelControl18.Controls.Add(this.labelControl127);
            this.panelControl18.Controls.Add(this.lookUpEdit28);
            this.panelControl18.Controls.Add(this.labelControl125);
            this.panelControl18.Controls.Add(this.labelControl126);
            this.panelControl18.Controls.Add(this.lookUpEdit27);
            this.panelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl18.Location = new System.Drawing.Point(0, 0);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(948, 493);
            this.panelControl18.TabIndex = 0;
            // 
            // gridPhancong
            // 
            this.gridPhancong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPhancong.Location = new System.Drawing.Point(26, 135);
            this.gridPhancong.MainView = this.gridViewPhanCong;
            this.gridPhancong.Name = "gridPhancong";
            this.gridPhancong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditSTT});
            this.gridPhancong.Size = new System.Drawing.Size(897, 219);
            this.gridPhancong.TabIndex = 126;
            this.gridPhancong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPhanCong});
            // 
            // gridViewPhanCong
            // 
            this.gridViewPhanCong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPhancongID,
            this.gridColPhancongDonvi,
            this.gridColPhancongBomon,
            this.gridColPhancongCongviec,
            this.gridColPhancongTungay,
            this.gridColPhancongDamnhan});
            this.gridViewPhanCong.GridControl = this.gridPhancong;
            this.gridViewPhanCong.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewPhanCong.Name = "gridViewPhanCong";
            // 
            // gridColPhancongID
            // 
            this.gridColPhancongID.Caption = "Mã ID";
            this.gridColPhancongID.FieldName = "ID";
            this.gridColPhancongID.Name = "gridColPhancongID";
            this.gridColPhancongID.OptionsColumn.AllowEdit = false;
            this.gridColPhancongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColPhancongDonvi
            // 
            this.gridColPhancongDonvi.Caption = "Phòng/Khoa";
            this.gridColPhancongDonvi.FieldName = "TENPHONGKHOA";
            this.gridColPhancongDonvi.Name = "gridColPhancongDonvi";
            this.gridColPhancongDonvi.OptionsColumn.AllowEdit = false;
            this.gridColPhancongDonvi.OptionsColumn.ReadOnly = true;
            this.gridColPhancongDonvi.Visible = true;
            this.gridColPhancongDonvi.VisibleIndex = 0;
            // 
            // gridColPhancongBomon
            // 
            this.gridColPhancongBomon.Caption = "Bộ phận/Bộ môn";
            this.gridColPhancongBomon.FieldName = "TENBPBM";
            this.gridColPhancongBomon.Name = "gridColPhancongBomon";
            this.gridColPhancongBomon.OptionsColumn.AllowEdit = false;
            this.gridColPhancongBomon.OptionsColumn.ReadOnly = true;
            this.gridColPhancongBomon.Visible = true;
            this.gridColPhancongBomon.VisibleIndex = 1;
            // 
            // gridColPhancongCongviec
            // 
            this.gridColPhancongCongviec.Caption = "Công việc";
            this.gridColPhancongCongviec.FieldName = "TENCHUCVU";
            this.gridColPhancongCongviec.Name = "gridColPhancongCongviec";
            this.gridColPhancongCongviec.OptionsColumn.AllowEdit = false;
            this.gridColPhancongCongviec.OptionsColumn.ReadOnly = true;
            this.gridColPhancongCongviec.Visible = true;
            this.gridColPhancongCongviec.VisibleIndex = 2;
            // 
            // gridColPhancongTungay
            // 
            this.gridColPhancongTungay.Caption = "Từ ngày";
            this.gridColPhancongTungay.FieldName = "TUNGAY";
            this.gridColPhancongTungay.Name = "gridColPhancongTungay";
            this.gridColPhancongTungay.OptionsColumn.AllowEdit = false;
            this.gridColPhancongTungay.OptionsColumn.ReadOnly = true;
            this.gridColPhancongTungay.Visible = true;
            this.gridColPhancongTungay.VisibleIndex = 3;
            // 
            // gridColPhancongDamnhan
            // 
            this.gridColPhancongDamnhan.Caption = "Đang đảm nhận";
            this.gridColPhancongDamnhan.ColumnEdit = this.repositoryItemCheckEditSTT;
            this.gridColPhancongDamnhan.FieldName = "TRANGTHAI";
            this.gridColPhancongDamnhan.Name = "gridColPhancongDamnhan";
            this.gridColPhancongDamnhan.OptionsColumn.AllowEdit = false;
            this.gridColPhancongDamnhan.OptionsColumn.ReadOnly = true;
            this.gridColPhancongDamnhan.Visible = true;
            this.gridColPhancongDamnhan.VisibleIndex = 4;
            // 
            // repositoryItemCheckEditSTT
            // 
            this.repositoryItemCheckEditSTT.AutoHeight = false;
            this.repositoryItemCheckEditSTT.Name = "repositoryItemCheckEditSTT";
            // 
            // simpleButton38
            // 
            this.simpleButton38.Location = new System.Drawing.Point(675, 102);
            this.simpleButton38.Name = "simpleButton38";
            this.simpleButton38.Size = new System.Drawing.Size(75, 23);
            this.simpleButton38.TabIndex = 105;
            this.simpleButton38.Text = "Xóa";
            // 
            // simpleButton39
            // 
            this.simpleButton39.Location = new System.Drawing.Point(594, 102);
            this.simpleButton39.Name = "simpleButton39";
            this.simpleButton39.Size = new System.Drawing.Size(75, 23);
            this.simpleButton39.TabIndex = 104;
            this.simpleButton39.Text = "Hiệu chỉnh";
            // 
            // simpleButton40
            // 
            this.simpleButton40.Location = new System.Drawing.Point(513, 102);
            this.simpleButton40.Name = "simpleButton40";
            this.simpleButton40.Size = new System.Drawing.Size(75, 23);
            this.simpleButton40.TabIndex = 103;
            this.simpleButton40.Text = "Thêm";
            // 
            // simpleButton41
            // 
            this.simpleButton41.Location = new System.Drawing.Point(432, 102);
            this.simpleButton41.Name = "simpleButton41";
            this.simpleButton41.Size = new System.Drawing.Size(75, 23);
            this.simpleButton41.TabIndex = 102;
            this.simpleButton41.Text = "Làm mới";
            // 
            // simpleButton42
            // 
            this.simpleButton42.Location = new System.Drawing.Point(351, 102);
            this.simpleButton42.Name = "simpleButton42";
            this.simpleButton42.Size = new System.Drawing.Size(75, 23);
            this.simpleButton42.TabIndex = 101;
            this.simpleButton42.Text = "Lưu";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(655, 60);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Đang đảm nhận";
            this.checkEdit1.Size = new System.Drawing.Size(100, 19);
            this.checkEdit1.TabIndex = 94;
            // 
            // labelControl128
            // 
            this.labelControl128.Location = new System.Drawing.Point(456, 62);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(44, 13);
            this.labelControl128.TabIndex = 93;
            this.labelControl128.Text = "Từ ngày:";
            // 
            // dateEdit18
            // 
            this.dateEdit18.EditValue = null;
            this.dateEdit18.Location = new System.Drawing.Point(512, 59);
            this.dateEdit18.Name = "dateEdit18";
            this.dateEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit18.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit18.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit18.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit18.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit18.Size = new System.Drawing.Size(113, 20);
            this.dateEdit18.TabIndex = 92;
            // 
            // lookUpEdit29
            // 
            this.lookUpEdit29.Location = new System.Drawing.Point(258, 59);
            this.lookUpEdit29.Name = "lookUpEdit29";
            this.lookUpEdit29.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit29.Size = new System.Drawing.Size(155, 20);
            this.lookUpEdit29.TabIndex = 73;
            // 
            // labelControl127
            // 
            this.labelControl127.Location = new System.Drawing.Point(184, 62);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(51, 13);
            this.labelControl127.TabIndex = 72;
            this.labelControl127.Text = "Công việc:";
            // 
            // lookUpEdit28
            // 
            this.lookUpEdit28.Location = new System.Drawing.Point(544, 21);
            this.lookUpEdit28.Name = "lookUpEdit28";
            this.lookUpEdit28.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit28.Size = new System.Drawing.Size(186, 20);
            this.lookUpEdit28.TabIndex = 70;
            // 
            // labelControl125
            // 
            this.labelControl125.Location = new System.Drawing.Point(456, 24);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(82, 13);
            this.labelControl125.TabIndex = 71;
            this.labelControl125.Text = "Bộ phận/Bộ môn:";
            // 
            // labelControl126
            // 
            this.labelControl126.Location = new System.Drawing.Point(184, 24);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(62, 13);
            this.labelControl126.TabIndex = 68;
            this.labelControl126.Text = "Phòng/Khoa:";
            // 
            // lookUpEdit27
            // 
            this.lookUpEdit27.Location = new System.Drawing.Point(258, 21);
            this.lookUpEdit27.Name = "lookUpEdit27";
            this.lookUpEdit27.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit27.Size = new System.Drawing.Size(155, 20);
            this.lookUpEdit27.TabIndex = 63;
            // 
            // xtraTabPage10
            // 
            this.xtraTabPage10.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage10.Name = "xtraTabPage10";
            this.xtraTabPage10.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage10.Text = "BHXH-BHYT";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl5);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl6);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(948, 493);
            this.splitContainerControl1.SplitterPosition = 475;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.gridBHXH);
            this.groupControl5.Controls.Add(this.simpleButton48);
            this.groupControl5.Controls.Add(this.simpleButton49);
            this.groupControl5.Controls.Add(this.simpleButton50);
            this.groupControl5.Controls.Add(this.simpleButton51);
            this.groupControl5.Controls.Add(this.simpleButton52);
            this.groupControl5.Controls.Add(this.labelControl130);
            this.groupControl5.Controls.Add(this.dateEdit19);
            this.groupControl5.Controls.Add(this.textEdit45);
            this.groupControl5.Controls.Add(this.labelControl129);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl5.Location = new System.Drawing.Point(0, 0);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(475, 493);
            this.groupControl5.TabIndex = 0;
            this.groupControl5.Text = "BHXH";
            // 
            // gridBHXH
            // 
            this.gridBHXH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBHXH.Location = new System.Drawing.Point(14, 128);
            this.gridBHXH.MainView = this.gridViewBHXH;
            this.gridBHXH.Name = "gridBHXH";
            this.gridBHXH.Size = new System.Drawing.Size(447, 223);
            this.gridBHXH.TabIndex = 106;
            this.gridBHXH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBHXH});
            // 
            // gridViewBHXH
            // 
            this.gridViewBHXH.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBHXHID,
            this.gridColBHXHSo,
            this.gridColBHXHNgaycap});
            this.gridViewBHXH.GridControl = this.gridBHXH;
            this.gridViewBHXH.GroupPanelText = " ";
            this.gridViewBHXH.Name = "gridViewBHXH";
            // 
            // gridColBHXHID
            // 
            this.gridColBHXHID.Caption = "Mã";
            this.gridColBHXHID.FieldName = "ID";
            this.gridColBHXHID.Name = "gridColBHXHID";
            this.gridColBHXHID.OptionsColumn.AllowEdit = false;
            this.gridColBHXHID.OptionsColumn.ReadOnly = true;
            // 
            // gridColBHXHSo
            // 
            this.gridColBHXHSo.Caption = "Số sổ BHXH";
            this.gridColBHXHSo.FieldName = "SOSO";
            this.gridColBHXHSo.Name = "gridColBHXHSo";
            this.gridColBHXHSo.OptionsColumn.AllowEdit = false;
            this.gridColBHXHSo.OptionsColumn.ReadOnly = true;
            this.gridColBHXHSo.Visible = true;
            this.gridColBHXHSo.VisibleIndex = 0;
            // 
            // gridColBHXHNgaycap
            // 
            this.gridColBHXHNgaycap.Caption = "Ngày cấp";
            this.gridColBHXHNgaycap.FieldName = "NGAYCAP";
            this.gridColBHXHNgaycap.Name = "gridColBHXHNgaycap";
            this.gridColBHXHNgaycap.OptionsColumn.AllowEdit = false;
            this.gridColBHXHNgaycap.OptionsColumn.ReadOnly = true;
            this.gridColBHXHNgaycap.Visible = true;
            this.gridColBHXHNgaycap.VisibleIndex = 1;
            // 
            // simpleButton48
            // 
            this.simpleButton48.Location = new System.Drawing.Point(376, 92);
            this.simpleButton48.Name = "simpleButton48";
            this.simpleButton48.Size = new System.Drawing.Size(75, 23);
            this.simpleButton48.TabIndex = 105;
            this.simpleButton48.Text = "Xóa";
            // 
            // simpleButton49
            // 
            this.simpleButton49.Location = new System.Drawing.Point(295, 92);
            this.simpleButton49.Name = "simpleButton49";
            this.simpleButton49.Size = new System.Drawing.Size(75, 23);
            this.simpleButton49.TabIndex = 104;
            this.simpleButton49.Text = "Hiệu chỉnh";
            // 
            // simpleButton50
            // 
            this.simpleButton50.Location = new System.Drawing.Point(214, 92);
            this.simpleButton50.Name = "simpleButton50";
            this.simpleButton50.Size = new System.Drawing.Size(75, 23);
            this.simpleButton50.TabIndex = 103;
            this.simpleButton50.Text = "Thêm";
            // 
            // simpleButton51
            // 
            this.simpleButton51.Location = new System.Drawing.Point(133, 92);
            this.simpleButton51.Name = "simpleButton51";
            this.simpleButton51.Size = new System.Drawing.Size(75, 23);
            this.simpleButton51.TabIndex = 102;
            this.simpleButton51.Text = "Làm mới";
            // 
            // simpleButton52
            // 
            this.simpleButton52.Location = new System.Drawing.Point(52, 92);
            this.simpleButton52.Name = "simpleButton52";
            this.simpleButton52.Size = new System.Drawing.Size(75, 23);
            this.simpleButton52.TabIndex = 101;
            this.simpleButton52.Text = "Lưu";
            // 
            // labelControl130
            // 
            this.labelControl130.Location = new System.Drawing.Point(237, 35);
            this.labelControl130.Name = "labelControl130";
            this.labelControl130.Size = new System.Drawing.Size(78, 13);
            this.labelControl130.TabIndex = 93;
            this.labelControl130.Text = "Ngày cấp BHXH:";
            // 
            // dateEdit19
            // 
            this.dateEdit19.EditValue = null;
            this.dateEdit19.Location = new System.Drawing.Point(321, 32);
            this.dateEdit19.Name = "dateEdit19";
            this.dateEdit19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit19.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit19.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit19.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit19.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit19.Size = new System.Drawing.Size(121, 20);
            this.dateEdit19.TabIndex = 92;
            // 
            // textEdit45
            // 
            this.textEdit45.Location = new System.Drawing.Point(79, 32);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Size = new System.Drawing.Size(139, 20);
            this.textEdit45.TabIndex = 1;
            // 
            // labelControl129
            // 
            this.labelControl129.Location = new System.Drawing.Point(14, 35);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(59, 13);
            this.labelControl129.TabIndex = 0;
            this.labelControl129.Text = "Số sổ BHXH:";
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.gridBHYT);
            this.groupControl6.Controls.Add(this.simpleButton43);
            this.groupControl6.Controls.Add(this.textEdit47);
            this.groupControl6.Controls.Add(this.simpleButton44);
            this.groupControl6.Controls.Add(this.simpleButton45);
            this.groupControl6.Controls.Add(this.labelControl133);
            this.groupControl6.Controls.Add(this.simpleButton46);
            this.groupControl6.Controls.Add(this.labelControl131);
            this.groupControl6.Controls.Add(this.simpleButton47);
            this.groupControl6.Controls.Add(this.dateEdit20);
            this.groupControl6.Controls.Add(this.textEdit46);
            this.groupControl6.Controls.Add(this.labelControl132);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl6.Location = new System.Drawing.Point(0, 0);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(468, 493);
            this.groupControl6.TabIndex = 0;
            this.groupControl6.Text = "BHYT";
            // 
            // gridBHYT
            // 
            this.gridBHYT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBHYT.Location = new System.Drawing.Point(24, 126);
            this.gridBHYT.MainView = this.gridViewBHYT;
            this.gridBHYT.Name = "gridBHYT";
            this.gridBHYT.Size = new System.Drawing.Size(427, 228);
            this.gridBHYT.TabIndex = 107;
            this.gridBHYT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBHYT});
            // 
            // gridViewBHYT
            // 
            this.gridViewBHYT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBHYTID,
            this.gridColBHYTSothe,
            this.gridColBHYTNoiDK,
            this.gridColBHYTNgaycap});
            this.gridViewBHYT.GridControl = this.gridBHYT;
            this.gridViewBHYT.GroupPanelText = " ";
            this.gridViewBHYT.Name = "gridViewBHYT";
            // 
            // gridColBHYTID
            // 
            this.gridColBHYTID.Caption = "Mã";
            this.gridColBHYTID.FieldName = "ID";
            this.gridColBHYTID.Name = "gridColBHYTID";
            // 
            // gridColBHYTSothe
            // 
            this.gridColBHYTSothe.Caption = "Số thẻ BHYT";
            this.gridColBHYTSothe.FieldName = "SOSO";
            this.gridColBHYTSothe.Name = "gridColBHYTSothe";
            this.gridColBHYTSothe.OptionsColumn.AllowEdit = false;
            this.gridColBHYTSothe.OptionsColumn.ReadOnly = true;
            this.gridColBHYTSothe.Visible = true;
            this.gridColBHYTSothe.VisibleIndex = 0;
            // 
            // gridColBHYTNoiDK
            // 
            this.gridColBHYTNoiDK.Caption = "Nơi ĐK KCB";
            this.gridColBHYTNoiDK.FieldName = "NOIDKKCB";
            this.gridColBHYTNoiDK.Name = "gridColBHYTNoiDK";
            this.gridColBHYTNoiDK.OptionsColumn.AllowEdit = false;
            this.gridColBHYTNoiDK.OptionsColumn.ReadOnly = true;
            this.gridColBHYTNoiDK.Visible = true;
            this.gridColBHYTNoiDK.VisibleIndex = 1;
            // 
            // gridColBHYTNgaycap
            // 
            this.gridColBHYTNgaycap.Caption = "Ngày cấp";
            this.gridColBHYTNgaycap.FieldName = "NGAYCAP";
            this.gridColBHYTNgaycap.Name = "gridColBHYTNgaycap";
            this.gridColBHYTNgaycap.OptionsColumn.AllowEdit = false;
            this.gridColBHYTNgaycap.OptionsColumn.ReadOnly = true;
            this.gridColBHYTNgaycap.Visible = true;
            this.gridColBHYTNgaycap.VisibleIndex = 2;
            // 
            // simpleButton43
            // 
            this.simpleButton43.Location = new System.Drawing.Point(376, 92);
            this.simpleButton43.Name = "simpleButton43";
            this.simpleButton43.Size = new System.Drawing.Size(75, 23);
            this.simpleButton43.TabIndex = 105;
            this.simpleButton43.Text = "Xóa";
            // 
            // textEdit47
            // 
            this.textEdit47.Location = new System.Drawing.Point(312, 32);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Size = new System.Drawing.Size(139, 20);
            this.textEdit47.TabIndex = 99;
            // 
            // simpleButton44
            // 
            this.simpleButton44.Location = new System.Drawing.Point(295, 92);
            this.simpleButton44.Name = "simpleButton44";
            this.simpleButton44.Size = new System.Drawing.Size(75, 23);
            this.simpleButton44.TabIndex = 104;
            this.simpleButton44.Text = "Hiệu chỉnh";
            // 
            // simpleButton45
            // 
            this.simpleButton45.Location = new System.Drawing.Point(214, 92);
            this.simpleButton45.Name = "simpleButton45";
            this.simpleButton45.Size = new System.Drawing.Size(75, 23);
            this.simpleButton45.TabIndex = 103;
            this.simpleButton45.Text = "Thêm";
            // 
            // labelControl133
            // 
            this.labelControl133.Location = new System.Drawing.Point(247, 35);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(55, 13);
            this.labelControl133.TabIndex = 98;
            this.labelControl133.Text = "Nơi ĐKKCB:";
            // 
            // simpleButton46
            // 
            this.simpleButton46.Location = new System.Drawing.Point(133, 92);
            this.simpleButton46.Name = "simpleButton46";
            this.simpleButton46.Size = new System.Drawing.Size(75, 23);
            this.simpleButton46.TabIndex = 102;
            this.simpleButton46.Text = "Làm mới";
            // 
            // labelControl131
            // 
            this.labelControl131.Location = new System.Drawing.Point(24, 67);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(68, 13);
            this.labelControl131.TabIndex = 97;
            this.labelControl131.Text = "Ngày cấp thẻ:";
            // 
            // simpleButton47
            // 
            this.simpleButton47.Location = new System.Drawing.Point(52, 92);
            this.simpleButton47.Name = "simpleButton47";
            this.simpleButton47.Size = new System.Drawing.Size(75, 23);
            this.simpleButton47.TabIndex = 101;
            this.simpleButton47.Text = "Lưu";
            // 
            // dateEdit20
            // 
            this.dateEdit20.EditValue = null;
            this.dateEdit20.Location = new System.Drawing.Point(107, 64);
            this.dateEdit20.Name = "dateEdit20";
            this.dateEdit20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit20.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit20.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit20.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit20.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit20.Size = new System.Drawing.Size(121, 20);
            this.dateEdit20.TabIndex = 96;
            // 
            // textEdit46
            // 
            this.textEdit46.Location = new System.Drawing.Point(89, 32);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Size = new System.Drawing.Size(139, 20);
            this.textEdit46.TabIndex = 95;
            // 
            // labelControl132
            // 
            this.labelControl132.Location = new System.Drawing.Point(24, 35);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(63, 13);
            this.labelControl132.TabIndex = 94;
            this.labelControl132.Text = "Số thẻ BHYT:";
            // 
            // xtraTabPage11
            // 
            this.xtraTabPage11.Controls.Add(this.panelControl19);
            this.xtraTabPage11.Name = "xtraTabPage11";
            this.xtraTabPage11.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage11.Text = "Sức khỏe";
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.gridSuckhoe);
            this.panelControl19.Controls.Add(this.simpleButton53);
            this.panelControl19.Controls.Add(this.simpleButton54);
            this.panelControl19.Controls.Add(this.simpleButton55);
            this.panelControl19.Controls.Add(this.simpleButton56);
            this.panelControl19.Controls.Add(this.simpleButton57);
            this.panelControl19.Controls.Add(this.textEdit49);
            this.panelControl19.Controls.Add(this.labelControl137);
            this.panelControl19.Controls.Add(this.textEdit48);
            this.panelControl19.Controls.Add(this.labelControl136);
            this.panelControl19.Controls.Add(this.comboBoxEdit2);
            this.panelControl19.Controls.Add(this.labelControl135);
            this.panelControl19.Controls.Add(this.dateEdit21);
            this.panelControl19.Controls.Add(this.labelControl134);
            this.panelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl19.Location = new System.Drawing.Point(0, 0);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(948, 493);
            this.panelControl19.TabIndex = 0;
            // 
            // gridSuckhoe
            // 
            this.gridSuckhoe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSuckhoe.Location = new System.Drawing.Point(26, 127);
            this.gridSuckhoe.MainView = this.gridViewSuckhoe;
            this.gridSuckhoe.Name = "gridSuckhoe";
            this.gridSuckhoe.Size = new System.Drawing.Size(897, 227);
            this.gridSuckhoe.TabIndex = 111;
            this.gridSuckhoe.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSuckhoe});
            // 
            // gridViewSuckhoe
            // 
            this.gridViewSuckhoe.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewSuckhoe.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewSuckhoe.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColSKID,
            this.gridColSKNgaykham,
            this.gridColSKTinhtrang,
            this.gridColSKChieucao,
            this.gridColSKCannang});
            this.gridViewSuckhoe.GridControl = this.gridSuckhoe;
            this.gridViewSuckhoe.GroupPanelText = "Lịch sử sức khỏe";
            this.gridViewSuckhoe.Name = "gridViewSuckhoe";
            // 
            // gridColSKID
            // 
            this.gridColSKID.Caption = "Mã ID";
            this.gridColSKID.FieldName = "ID";
            this.gridColSKID.Name = "gridColSKID";
            this.gridColSKID.OptionsColumn.AllowEdit = false;
            this.gridColSKID.OptionsColumn.ReadOnly = true;
            // 
            // gridColSKNgaykham
            // 
            this.gridColSKNgaykham.Caption = "Ngày khám";
            this.gridColSKNgaykham.FieldName = "NGAYKHAM";
            this.gridColSKNgaykham.Name = "gridColSKNgaykham";
            this.gridColSKNgaykham.OptionsColumn.AllowEdit = false;
            this.gridColSKNgaykham.OptionsColumn.ReadOnly = true;
            this.gridColSKNgaykham.Visible = true;
            this.gridColSKNgaykham.VisibleIndex = 0;
            // 
            // gridColSKTinhtrang
            // 
            this.gridColSKTinhtrang.Caption = "Tình trạng sức khỏe";
            this.gridColSKTinhtrang.FieldName = "TINHTRANGSK";
            this.gridColSKTinhtrang.Name = "gridColSKTinhtrang";
            this.gridColSKTinhtrang.OptionsColumn.AllowEdit = false;
            this.gridColSKTinhtrang.OptionsColumn.ReadOnly = true;
            this.gridColSKTinhtrang.Visible = true;
            this.gridColSKTinhtrang.VisibleIndex = 1;
            // 
            // gridColSKChieucao
            // 
            this.gridColSKChieucao.Caption = "Chiều cao";
            this.gridColSKChieucao.FieldName = "CHIEUCAO";
            this.gridColSKChieucao.Name = "gridColSKChieucao";
            this.gridColSKChieucao.OptionsColumn.AllowEdit = false;
            this.gridColSKChieucao.OptionsColumn.ReadOnly = true;
            this.gridColSKChieucao.Visible = true;
            this.gridColSKChieucao.VisibleIndex = 2;
            // 
            // gridColSKCannang
            // 
            this.gridColSKCannang.Caption = "Cân nặng";
            this.gridColSKCannang.FieldName = "CANNANG";
            this.gridColSKCannang.Name = "gridColSKCannang";
            this.gridColSKCannang.OptionsColumn.AllowEdit = false;
            this.gridColSKCannang.OptionsColumn.ReadOnly = true;
            this.gridColSKCannang.Visible = true;
            this.gridColSKCannang.VisibleIndex = 3;
            // 
            // simpleButton53
            // 
            this.simpleButton53.Location = new System.Drawing.Point(686, 91);
            this.simpleButton53.Name = "simpleButton53";
            this.simpleButton53.Size = new System.Drawing.Size(75, 23);
            this.simpleButton53.TabIndex = 110;
            this.simpleButton53.Text = "Xóa";
            // 
            // simpleButton54
            // 
            this.simpleButton54.Location = new System.Drawing.Point(604, 91);
            this.simpleButton54.Name = "simpleButton54";
            this.simpleButton54.Size = new System.Drawing.Size(75, 23);
            this.simpleButton54.TabIndex = 109;
            this.simpleButton54.Text = "Hiệu chỉnh";
            // 
            // simpleButton55
            // 
            this.simpleButton55.Location = new System.Drawing.Point(522, 91);
            this.simpleButton55.Name = "simpleButton55";
            this.simpleButton55.Size = new System.Drawing.Size(75, 23);
            this.simpleButton55.TabIndex = 108;
            this.simpleButton55.Text = "Thêm";
            // 
            // simpleButton56
            // 
            this.simpleButton56.Location = new System.Drawing.Point(440, 91);
            this.simpleButton56.Name = "simpleButton56";
            this.simpleButton56.Size = new System.Drawing.Size(75, 23);
            this.simpleButton56.TabIndex = 107;
            this.simpleButton56.Text = "Làm mới";
            // 
            // simpleButton57
            // 
            this.simpleButton57.Location = new System.Drawing.Point(358, 91);
            this.simpleButton57.Name = "simpleButton57";
            this.simpleButton57.Size = new System.Drawing.Size(75, 23);
            this.simpleButton57.TabIndex = 106;
            this.simpleButton57.Text = "Lưu";
            // 
            // textEdit49
            // 
            this.textEdit49.Location = new System.Drawing.Point(611, 62);
            this.textEdit49.Name = "textEdit49";
            this.textEdit49.Size = new System.Drawing.Size(150, 20);
            this.textEdit49.TabIndex = 72;
            // 
            // labelControl137
            // 
            this.labelControl137.Location = new System.Drawing.Point(478, 65);
            this.labelControl137.Name = "labelControl137";
            this.labelControl137.Size = new System.Drawing.Size(50, 13);
            this.labelControl137.TabIndex = 71;
            this.labelControl137.Text = "Cân nặng:";
            // 
            // textEdit48
            // 
            this.textEdit48.Location = new System.Drawing.Point(252, 62);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Size = new System.Drawing.Size(150, 20);
            this.textEdit48.TabIndex = 70;
            // 
            // labelControl136
            // 
            this.labelControl136.Location = new System.Drawing.Point(161, 65);
            this.labelControl136.Name = "labelControl136";
            this.labelControl136.Size = new System.Drawing.Size(51, 13);
            this.labelControl136.TabIndex = 69;
            this.labelControl136.Text = "Chiều cao:";
            // 
            // comboBoxEdit2
            // 
            this.comboBoxEdit2.Location = new System.Drawing.Point(611, 28);
            this.comboBoxEdit2.Name = "comboBoxEdit2";
            this.comboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit2.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.comboBoxEdit2.Size = new System.Drawing.Size(150, 20);
            this.comboBoxEdit2.TabIndex = 68;
            // 
            // labelControl135
            // 
            this.labelControl135.Location = new System.Drawing.Point(478, 28);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(99, 13);
            this.labelControl135.TabIndex = 67;
            this.labelControl135.Text = "Tình trạng sức khỏe:";
            // 
            // dateEdit21
            // 
            this.dateEdit21.EditValue = null;
            this.dateEdit21.Location = new System.Drawing.Point(253, 25);
            this.dateEdit21.Name = "dateEdit21";
            this.dateEdit21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit21.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit21.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit21.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit21.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit21.Size = new System.Drawing.Size(149, 20);
            this.dateEdit21.TabIndex = 66;
            // 
            // labelControl134
            // 
            this.labelControl134.Location = new System.Drawing.Point(161, 28);
            this.labelControl134.Name = "labelControl134";
            this.labelControl134.Size = new System.Drawing.Size(57, 13);
            this.labelControl134.TabIndex = 0;
            this.labelControl134.Text = "Ngày khám:";
            // 
            // xtraTabPage12
            // 
            this.xtraTabPage12.Controls.Add(this.xtraTabControl2);
            this.xtraTabPage12.Name = "xtraTabPage12";
            this.xtraTabPage12.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage12.Text = "Thi đua - Khen thưởng";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage13;
            this.xtraTabControl2.Size = new System.Drawing.Size(948, 493);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage13,
            this.xtraTabPage14});
            // 
            // xtraTabPage13
            // 
            this.xtraTabPage13.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage13.Name = "xtraTabPage13";
            this.xtraTabPage13.Size = new System.Drawing.Size(942, 465);
            this.xtraTabPage13.Text = "Thi đua";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.groupControl7);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(942, 465);
            this.splitContainerControl2.SplitterPosition = 315;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.gridThang);
            this.groupControl7.Controls.Add(this.simpleButton63);
            this.groupControl7.Controls.Add(this.simpleButton64);
            this.groupControl7.Controls.Add(this.simpleButton65);
            this.groupControl7.Controls.Add(this.simpleButton66);
            this.groupControl7.Controls.Add(this.simpleButton67);
            this.groupControl7.Controls.Add(this.comboBoxEdit5);
            this.groupControl7.Controls.Add(this.labelControl145);
            this.groupControl7.Controls.Add(this.comboBoxEdit4);
            this.groupControl7.Controls.Add(this.labelControl144);
            this.groupControl7.Controls.Add(this.comboBoxEdit3);
            this.groupControl7.Controls.Add(this.labelControl143);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl7.Location = new System.Drawing.Point(0, 0);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(315, 465);
            this.groupControl7.TabIndex = 0;
            this.groupControl7.Text = "Thi đua tháng";
            // 
            // gridThang
            // 
            this.gridThang.Location = new System.Drawing.Point(13, 128);
            this.gridThang.MainView = this.gridViewThiduathang;
            this.gridThang.Name = "gridThang";
            this.gridThang.Size = new System.Drawing.Size(293, 134);
            this.gridThang.TabIndex = 116;
            this.gridThang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewThiduathang,
            this.gridView3});
            // 
            // gridViewThiduathang
            // 
            this.gridViewThiduathang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDThangID,
            this.gridColTDThang,
            this.gridColTDThangNamhoc,
            this.gridColTDThangXeploai});
            this.gridViewThiduathang.GridControl = this.gridThang;
            this.gridViewThiduathang.Name = "gridViewThiduathang";
            this.gridViewThiduathang.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDThangID
            // 
            this.gridColTDThangID.Caption = "Mã";
            this.gridColTDThangID.FieldName = "ID";
            this.gridColTDThangID.Name = "gridColTDThangID";
            // 
            // gridColTDThang
            // 
            this.gridColTDThang.Caption = "Tháng";
            this.gridColTDThang.FieldName = "DOT";
            this.gridColTDThang.Name = "gridColTDThang";
            this.gridColTDThang.OptionsColumn.AllowEdit = false;
            this.gridColTDThang.OptionsColumn.ReadOnly = true;
            this.gridColTDThang.Visible = true;
            this.gridColTDThang.VisibleIndex = 0;
            // 
            // gridColTDThangNamhoc
            // 
            this.gridColTDThangNamhoc.Caption = "Năm học";
            this.gridColTDThangNamhoc.FieldName = "NAMHOC";
            this.gridColTDThangNamhoc.Name = "gridColTDThangNamhoc";
            this.gridColTDThangNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDThangNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDThangNamhoc.Visible = true;
            this.gridColTDThangNamhoc.VisibleIndex = 1;
            // 
            // gridColTDThangXeploai
            // 
            this.gridColTDThangXeploai.Caption = "Xếp loại";
            this.gridColTDThangXeploai.FieldName = "XEPLOAI";
            this.gridColTDThangXeploai.Name = "gridColTDThangXeploai";
            this.gridColTDThangXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDThangXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDThangXeploai.Visible = true;
            this.gridColTDThangXeploai.VisibleIndex = 2;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridThang;
            this.gridView3.Name = "gridView3";
            // 
            // simpleButton63
            // 
            this.simpleButton63.Location = new System.Drawing.Point(231, 90);
            this.simpleButton63.Name = "simpleButton63";
            this.simpleButton63.Size = new System.Drawing.Size(75, 23);
            this.simpleButton63.TabIndex = 115;
            this.simpleButton63.Text = "Xóa";
            // 
            // simpleButton64
            // 
            this.simpleButton64.Location = new System.Drawing.Point(138, 90);
            this.simpleButton64.Name = "simpleButton64";
            this.simpleButton64.Size = new System.Drawing.Size(75, 23);
            this.simpleButton64.TabIndex = 114;
            this.simpleButton64.Text = "Hiệu chỉnh";
            // 
            // simpleButton65
            // 
            this.simpleButton65.Location = new System.Drawing.Point(51, 90);
            this.simpleButton65.Name = "simpleButton65";
            this.simpleButton65.Size = new System.Drawing.Size(75, 23);
            this.simpleButton65.TabIndex = 113;
            this.simpleButton65.Text = "Thêm";
            // 
            // simpleButton66
            // 
            this.simpleButton66.Location = new System.Drawing.Point(231, 57);
            this.simpleButton66.Name = "simpleButton66";
            this.simpleButton66.Size = new System.Drawing.Size(75, 23);
            this.simpleButton66.TabIndex = 112;
            this.simpleButton66.Text = "Làm mới";
            // 
            // simpleButton67
            // 
            this.simpleButton67.Location = new System.Drawing.Point(138, 57);
            this.simpleButton67.Name = "simpleButton67";
            this.simpleButton67.Size = new System.Drawing.Size(75, 23);
            this.simpleButton67.TabIndex = 111;
            this.simpleButton67.Text = "Lưu";
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.Location = new System.Drawing.Point(60, 60);
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit5.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit5.Size = new System.Drawing.Size(64, 20);
            this.comboBoxEdit5.TabIndex = 5;
            // 
            // labelControl145
            // 
            this.labelControl145.Location = new System.Drawing.Point(13, 63);
            this.labelControl145.Name = "labelControl145";
            this.labelControl145.Size = new System.Drawing.Size(41, 13);
            this.labelControl145.TabIndex = 4;
            this.labelControl145.Text = "Xếp loại:";
            // 
            // comboBoxEdit4
            // 
            this.comboBoxEdit4.Location = new System.Drawing.Point(189, 34);
            this.comboBoxEdit4.Name = "comboBoxEdit4";
            this.comboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit4.Size = new System.Drawing.Size(117, 20);
            this.comboBoxEdit4.TabIndex = 3;
            // 
            // labelControl144
            // 
            this.labelControl144.Location = new System.Drawing.Point(138, 37);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(45, 13);
            this.labelControl144.TabIndex = 2;
            this.labelControl144.Text = "Năm học:";
            // 
            // comboBoxEdit3
            // 
            this.comboBoxEdit3.Location = new System.Drawing.Point(53, 34);
            this.comboBoxEdit3.Name = "comboBoxEdit3";
            this.comboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit3.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxEdit3.Size = new System.Drawing.Size(71, 20);
            this.comboBoxEdit3.TabIndex = 1;
            // 
            // labelControl143
            // 
            this.labelControl143.Location = new System.Drawing.Point(13, 37);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(34, 13);
            this.labelControl143.TabIndex = 0;
            this.labelControl143.Text = "Tháng:";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.groupControl8);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.groupControl9);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(622, 465);
            this.splitContainerControl3.SplitterPosition = 320;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.gridTDHK);
            this.groupControl8.Controls.Add(this.simpleButton68);
            this.groupControl8.Controls.Add(this.simpleButton69);
            this.groupControl8.Controls.Add(this.simpleButton70);
            this.groupControl8.Controls.Add(this.simpleButton71);
            this.groupControl8.Controls.Add(this.simpleButton72);
            this.groupControl8.Controls.Add(this.comboBoxEdit6);
            this.groupControl8.Controls.Add(this.labelControl146);
            this.groupControl8.Controls.Add(this.comboBoxEdit7);
            this.groupControl8.Controls.Add(this.labelControl147);
            this.groupControl8.Controls.Add(this.comboBoxEdit8);
            this.groupControl8.Controls.Add(this.labelControl148);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl8.Location = new System.Drawing.Point(0, 0);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(320, 465);
            this.groupControl8.TabIndex = 0;
            this.groupControl8.Text = "Thi đua học kỳ";
            // 
            // gridTDHK
            // 
            this.gridTDHK.Location = new System.Drawing.Point(16, 128);
            this.gridTDHK.MainView = this.gridViewTDHocKy;
            this.gridTDHK.Name = "gridTDHK";
            this.gridTDHK.Size = new System.Drawing.Size(293, 134);
            this.gridTDHK.TabIndex = 128;
            this.gridTDHK.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTDHocKy,
            this.gridView2});
            // 
            // gridViewTDHocKy
            // 
            this.gridViewTDHocKy.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDHKID,
            this.gridColTDHKHocKy,
            this.gridColTDHKNamhoc,
            this.gridColTDHKXeploai});
            this.gridViewTDHocKy.GridControl = this.gridTDHK;
            this.gridViewTDHocKy.Name = "gridViewTDHocKy";
            this.gridViewTDHocKy.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDHKID
            // 
            this.gridColTDHKID.Caption = "Mã";
            this.gridColTDHKID.FieldName = "ID";
            this.gridColTDHKID.Name = "gridColTDHKID";
            // 
            // gridColTDHKHocKy
            // 
            this.gridColTDHKHocKy.Caption = "Học kỳ";
            this.gridColTDHKHocKy.FieldName = "DOT";
            this.gridColTDHKHocKy.Name = "gridColTDHKHocKy";
            this.gridColTDHKHocKy.OptionsColumn.AllowEdit = false;
            this.gridColTDHKHocKy.OptionsColumn.ReadOnly = true;
            this.gridColTDHKHocKy.Visible = true;
            this.gridColTDHKHocKy.VisibleIndex = 0;
            // 
            // gridColTDHKNamhoc
            // 
            this.gridColTDHKNamhoc.Caption = "Năm học";
            this.gridColTDHKNamhoc.FieldName = "NAMHOC";
            this.gridColTDHKNamhoc.Name = "gridColTDHKNamhoc";
            this.gridColTDHKNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDHKNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDHKNamhoc.Visible = true;
            this.gridColTDHKNamhoc.VisibleIndex = 1;
            // 
            // gridColTDHKXeploai
            // 
            this.gridColTDHKXeploai.Caption = "Xếp loại";
            this.gridColTDHKXeploai.FieldName = "XEPLOAI";
            this.gridColTDHKXeploai.Name = "gridColTDHKXeploai";
            this.gridColTDHKXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDHKXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDHKXeploai.Visible = true;
            this.gridColTDHKXeploai.VisibleIndex = 2;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridTDHK;
            this.gridView2.Name = "gridView2";
            // 
            // simpleButton68
            // 
            this.simpleButton68.Location = new System.Drawing.Point(234, 90);
            this.simpleButton68.Name = "simpleButton68";
            this.simpleButton68.Size = new System.Drawing.Size(75, 23);
            this.simpleButton68.TabIndex = 127;
            this.simpleButton68.Text = "Xóa";
            // 
            // simpleButton69
            // 
            this.simpleButton69.Location = new System.Drawing.Point(141, 90);
            this.simpleButton69.Name = "simpleButton69";
            this.simpleButton69.Size = new System.Drawing.Size(75, 23);
            this.simpleButton69.TabIndex = 126;
            this.simpleButton69.Text = "Hiệu chỉnh";
            // 
            // simpleButton70
            // 
            this.simpleButton70.Location = new System.Drawing.Point(54, 90);
            this.simpleButton70.Name = "simpleButton70";
            this.simpleButton70.Size = new System.Drawing.Size(75, 23);
            this.simpleButton70.TabIndex = 125;
            this.simpleButton70.Text = "Thêm";
            // 
            // simpleButton71
            // 
            this.simpleButton71.Location = new System.Drawing.Point(234, 57);
            this.simpleButton71.Name = "simpleButton71";
            this.simpleButton71.Size = new System.Drawing.Size(75, 23);
            this.simpleButton71.TabIndex = 124;
            this.simpleButton71.Text = "Làm mới";
            // 
            // simpleButton72
            // 
            this.simpleButton72.Location = new System.Drawing.Point(141, 57);
            this.simpleButton72.Name = "simpleButton72";
            this.simpleButton72.Size = new System.Drawing.Size(75, 23);
            this.simpleButton72.TabIndex = 123;
            this.simpleButton72.Text = "Lưu";
            // 
            // comboBoxEdit6
            // 
            this.comboBoxEdit6.Location = new System.Drawing.Point(63, 60);
            this.comboBoxEdit6.Name = "comboBoxEdit6";
            this.comboBoxEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit6.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit6.Size = new System.Drawing.Size(64, 20);
            this.comboBoxEdit6.TabIndex = 122;
            // 
            // labelControl146
            // 
            this.labelControl146.Location = new System.Drawing.Point(16, 63);
            this.labelControl146.Name = "labelControl146";
            this.labelControl146.Size = new System.Drawing.Size(41, 13);
            this.labelControl146.TabIndex = 121;
            this.labelControl146.Text = "Xếp loại:";
            // 
            // comboBoxEdit7
            // 
            this.comboBoxEdit7.Location = new System.Drawing.Point(192, 34);
            this.comboBoxEdit7.Name = "comboBoxEdit7";
            this.comboBoxEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit7.Size = new System.Drawing.Size(117, 20);
            this.comboBoxEdit7.TabIndex = 120;
            // 
            // labelControl147
            // 
            this.labelControl147.Location = new System.Drawing.Point(141, 37);
            this.labelControl147.Name = "labelControl147";
            this.labelControl147.Size = new System.Drawing.Size(45, 13);
            this.labelControl147.TabIndex = 119;
            this.labelControl147.Text = "Năm học:";
            // 
            // comboBoxEdit8
            // 
            this.comboBoxEdit8.Location = new System.Drawing.Point(56, 34);
            this.comboBoxEdit8.Name = "comboBoxEdit8";
            this.comboBoxEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit8.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxEdit8.Size = new System.Drawing.Size(71, 20);
            this.comboBoxEdit8.TabIndex = 118;
            // 
            // labelControl148
            // 
            this.labelControl148.Location = new System.Drawing.Point(16, 37);
            this.labelControl148.Name = "labelControl148";
            this.labelControl148.Size = new System.Drawing.Size(36, 13);
            this.labelControl148.TabIndex = 117;
            this.labelControl148.Text = "Học kỳ:";
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.gridTDNam);
            this.groupControl9.Controls.Add(this.simpleButton73);
            this.groupControl9.Controls.Add(this.simpleButton74);
            this.groupControl9.Controls.Add(this.simpleButton75);
            this.groupControl9.Controls.Add(this.simpleButton76);
            this.groupControl9.Controls.Add(this.simpleButton77);
            this.groupControl9.Controls.Add(this.comboBoxEdit9);
            this.groupControl9.Controls.Add(this.labelControl149);
            this.groupControl9.Controls.Add(this.comboBoxEdit10);
            this.groupControl9.Controls.Add(this.labelControl150);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl9.Location = new System.Drawing.Point(0, 0);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(297, 465);
            this.groupControl9.TabIndex = 0;
            this.groupControl9.Text = "Thi đua năm học";
            // 
            // gridTDNam
            // 
            this.gridTDNam.Location = new System.Drawing.Point(17, 128);
            this.gridTDNam.MainView = this.gridViewTDNam;
            this.gridTDNam.Name = "gridTDNam";
            this.gridTDNam.Size = new System.Drawing.Size(275, 137);
            this.gridTDNam.TabIndex = 138;
            this.gridTDNam.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTDNam,
            this.gridView1});
            // 
            // gridViewTDNam
            // 
            this.gridViewTDNam.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDNamID,
            this.gridColTDNamNamhoc,
            this.gridColTDNamXeploai});
            this.gridViewTDNam.GridControl = this.gridTDNam;
            this.gridViewTDNam.Name = "gridViewTDNam";
            this.gridViewTDNam.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDNamID
            // 
            this.gridColTDNamID.Caption = "Mã";
            this.gridColTDNamID.FieldName = "ID";
            this.gridColTDNamID.Name = "gridColTDNamID";
            // 
            // gridColTDNamNamhoc
            // 
            this.gridColTDNamNamhoc.Caption = "Năm học";
            this.gridColTDNamNamhoc.FieldName = "NAMHOC";
            this.gridColTDNamNamhoc.Name = "gridColTDNamNamhoc";
            this.gridColTDNamNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDNamNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDNamNamhoc.Visible = true;
            this.gridColTDNamNamhoc.VisibleIndex = 0;
            // 
            // gridColTDNamXeploai
            // 
            this.gridColTDNamXeploai.Caption = "Xếp loại";
            this.gridColTDNamXeploai.FieldName = "XEPLOAI";
            this.gridColTDNamXeploai.Name = "gridColTDNamXeploai";
            this.gridColTDNamXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDNamXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDNamXeploai.Visible = true;
            this.gridColTDNamXeploai.VisibleIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridTDNam;
            this.gridView1.Name = "gridView1";
            // 
            // simpleButton73
            // 
            this.simpleButton73.Location = new System.Drawing.Point(174, 89);
            this.simpleButton73.Name = "simpleButton73";
            this.simpleButton73.Size = new System.Drawing.Size(75, 23);
            this.simpleButton73.TabIndex = 137;
            this.simpleButton73.Text = "Xóa";
            // 
            // simpleButton74
            // 
            this.simpleButton74.Location = new System.Drawing.Point(93, 89);
            this.simpleButton74.Name = "simpleButton74";
            this.simpleButton74.Size = new System.Drawing.Size(75, 23);
            this.simpleButton74.TabIndex = 136;
            this.simpleButton74.Text = "Hiệu chỉnh";
            // 
            // simpleButton75
            // 
            this.simpleButton75.Location = new System.Drawing.Point(132, 60);
            this.simpleButton75.Name = "simpleButton75";
            this.simpleButton75.Size = new System.Drawing.Size(75, 23);
            this.simpleButton75.TabIndex = 135;
            this.simpleButton75.Text = "Thêm";
            // 
            // simpleButton76
            // 
            this.simpleButton76.Location = new System.Drawing.Point(213, 60);
            this.simpleButton76.Name = "simpleButton76";
            this.simpleButton76.Size = new System.Drawing.Size(75, 23);
            this.simpleButton76.TabIndex = 134;
            this.simpleButton76.Text = "Làm mới";
            // 
            // simpleButton77
            // 
            this.simpleButton77.Location = new System.Drawing.Point(51, 60);
            this.simpleButton77.Name = "simpleButton77";
            this.simpleButton77.Size = new System.Drawing.Size(75, 23);
            this.simpleButton77.TabIndex = 133;
            this.simpleButton77.Text = "Lưu";
            // 
            // comboBoxEdit9
            // 
            this.comboBoxEdit9.Location = new System.Drawing.Point(220, 34);
            this.comboBoxEdit9.Name = "comboBoxEdit9";
            this.comboBoxEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit9.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit9.Size = new System.Drawing.Size(68, 20);
            this.comboBoxEdit9.TabIndex = 132;
            // 
            // labelControl149
            // 
            this.labelControl149.Location = new System.Drawing.Point(173, 38);
            this.labelControl149.Name = "labelControl149";
            this.labelControl149.Size = new System.Drawing.Size(41, 13);
            this.labelControl149.TabIndex = 131;
            this.labelControl149.Text = "Xếp loại:";
            // 
            // comboBoxEdit10
            // 
            this.comboBoxEdit10.Location = new System.Drawing.Point(58, 35);
            this.comboBoxEdit10.Name = "comboBoxEdit10";
            this.comboBoxEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit10.Size = new System.Drawing.Size(101, 20);
            this.comboBoxEdit10.TabIndex = 130;
            // 
            // labelControl150
            // 
            this.labelControl150.Location = new System.Drawing.Point(7, 38);
            this.labelControl150.Name = "labelControl150";
            this.labelControl150.Size = new System.Drawing.Size(45, 13);
            this.labelControl150.TabIndex = 129;
            this.labelControl150.Text = "Năm học:";
            // 
            // xtraTabPage14
            // 
            this.xtraTabPage14.Controls.Add(this.panelControl20);
            this.xtraTabPage14.Name = "xtraTabPage14";
            this.xtraTabPage14.Size = new System.Drawing.Size(942, 465);
            this.xtraTabPage14.Text = "Khen thưởng";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.simpleButton58);
            this.panelControl20.Controls.Add(this.simpleButton59);
            this.panelControl20.Controls.Add(this.simpleButton60);
            this.panelControl20.Controls.Add(this.simpleButton61);
            this.panelControl20.Controls.Add(this.simpleButton62);
            this.panelControl20.Controls.Add(this.gridKhenthuong);
            this.panelControl20.Controls.Add(this.lookUpEdit32);
            this.panelControl20.Controls.Add(this.labelControl142);
            this.panelControl20.Controls.Add(this.memoEdit7);
            this.panelControl20.Controls.Add(this.labelControl141);
            this.panelControl20.Controls.Add(this.lookUpEdit31);
            this.panelControl20.Controls.Add(this.labelControl140);
            this.panelControl20.Controls.Add(this.lookUpEdit30);
            this.panelControl20.Controls.Add(this.labelControl139);
            this.panelControl20.Controls.Add(this.dateEdit22);
            this.panelControl20.Controls.Add(this.labelControl138);
            this.panelControl20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl20.Location = new System.Drawing.Point(0, 0);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(942, 465);
            this.panelControl20.TabIndex = 0;
            // 
            // simpleButton58
            // 
            this.simpleButton58.Location = new System.Drawing.Point(704, 93);
            this.simpleButton58.Name = "simpleButton58";
            this.simpleButton58.Size = new System.Drawing.Size(75, 23);
            this.simpleButton58.TabIndex = 117;
            this.simpleButton58.Text = "Xóa";
            // 
            // simpleButton59
            // 
            this.simpleButton59.Location = new System.Drawing.Point(622, 93);
            this.simpleButton59.Name = "simpleButton59";
            this.simpleButton59.Size = new System.Drawing.Size(75, 23);
            this.simpleButton59.TabIndex = 116;
            this.simpleButton59.Text = "Hiệu chỉnh";
            // 
            // simpleButton60
            // 
            this.simpleButton60.Location = new System.Drawing.Point(540, 93);
            this.simpleButton60.Name = "simpleButton60";
            this.simpleButton60.Size = new System.Drawing.Size(75, 23);
            this.simpleButton60.TabIndex = 115;
            this.simpleButton60.Text = "Thêm";
            // 
            // simpleButton61
            // 
            this.simpleButton61.Location = new System.Drawing.Point(458, 93);
            this.simpleButton61.Name = "simpleButton61";
            this.simpleButton61.Size = new System.Drawing.Size(75, 23);
            this.simpleButton61.TabIndex = 114;
            this.simpleButton61.Text = "Làm mới";
            // 
            // simpleButton62
            // 
            this.simpleButton62.Location = new System.Drawing.Point(376, 93);
            this.simpleButton62.Name = "simpleButton62";
            this.simpleButton62.Size = new System.Drawing.Size(75, 23);
            this.simpleButton62.TabIndex = 113;
            this.simpleButton62.Text = "Lưu";
            // 
            // gridKhenthuong
            // 
            this.gridKhenthuong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridKhenthuong.Location = new System.Drawing.Point(23, 122);
            this.gridKhenthuong.MainView = this.gridViewKhenthuong;
            this.gridKhenthuong.Name = "gridKhenthuong";
            this.gridKhenthuong.Size = new System.Drawing.Size(897, 211);
            this.gridKhenthuong.TabIndex = 112;
            this.gridKhenthuong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewKhenthuong});
            // 
            // gridViewKhenthuong
            // 
            this.gridViewKhenthuong.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewKhenthuong.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewKhenthuong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColKhenthuongID,
            this.gridColKhenthuongNgay,
            this.gridColKhenthuongLoai,
            this.gridColKhenthuongNoidung,
            this.gridColKhenthuongHinhthuc,
            this.gridColKhenthuongCap});
            this.gridViewKhenthuong.GridControl = this.gridKhenthuong;
            this.gridViewKhenthuong.GroupPanelText = "Danh sách khen thưởng";
            this.gridViewKhenthuong.Name = "gridViewKhenthuong";
            // 
            // gridColKhenthuongID
            // 
            this.gridColKhenthuongID.Caption = "Mã ID";
            this.gridColKhenthuongID.FieldName = "ID";
            this.gridColKhenthuongID.Name = "gridColKhenthuongID";
            this.gridColKhenthuongID.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColKhenthuongNgay
            // 
            this.gridColKhenthuongNgay.Caption = "Ngày khen thưởng";
            this.gridColKhenthuongNgay.FieldName = "NGAY";
            this.gridColKhenthuongNgay.Name = "gridColKhenthuongNgay";
            this.gridColKhenthuongNgay.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongNgay.OptionsColumn.ReadOnly = true;
            this.gridColKhenthuongNgay.Visible = true;
            this.gridColKhenthuongNgay.VisibleIndex = 0;
            // 
            // gridColKhenthuongLoai
            // 
            this.gridColKhenthuongLoai.Caption = "Loại khen thưởng";
            this.gridColKhenthuongLoai.FieldName = "LOAI";
            this.gridColKhenthuongLoai.Name = "gridColKhenthuongLoai";
            this.gridColKhenthuongLoai.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongLoai.OptionsColumn.ReadOnly = true;
            this.gridColKhenthuongLoai.Visible = true;
            this.gridColKhenthuongLoai.VisibleIndex = 1;
            // 
            // gridColKhenthuongNoidung
            // 
            this.gridColKhenthuongNoidung.Caption = "Nội dung khen thưởng";
            this.gridColKhenthuongNoidung.FieldName = "NOIDUNG";
            this.gridColKhenthuongNoidung.Name = "gridColKhenthuongNoidung";
            this.gridColKhenthuongNoidung.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongNoidung.OptionsColumn.ReadOnly = true;
            this.gridColKhenthuongNoidung.Visible = true;
            this.gridColKhenthuongNoidung.VisibleIndex = 2;
            // 
            // gridColKhenthuongHinhthuc
            // 
            this.gridColKhenthuongHinhthuc.Caption = "Hình thức khen thưởng";
            this.gridColKhenthuongHinhthuc.FieldName = "HINHTHUC";
            this.gridColKhenthuongHinhthuc.Name = "gridColKhenthuongHinhthuc";
            this.gridColKhenthuongHinhthuc.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongHinhthuc.OptionsColumn.ReadOnly = true;
            this.gridColKhenthuongHinhthuc.Visible = true;
            this.gridColKhenthuongHinhthuc.VisibleIndex = 3;
            // 
            // gridColKhenthuongCap
            // 
            this.gridColKhenthuongCap.Caption = "Cấp QĐ khen thưởng";
            this.gridColKhenthuongCap.FieldName = "CAPQUYETDINH";
            this.gridColKhenthuongCap.Name = "gridColKhenthuongCap";
            this.gridColKhenthuongCap.OptionsColumn.AllowEdit = false;
            this.gridColKhenthuongCap.OptionsColumn.ReadOnly = true;
            this.gridColKhenthuongCap.Visible = true;
            this.gridColKhenthuongCap.VisibleIndex = 4;
            // 
            // lookUpEdit32
            // 
            this.lookUpEdit32.Location = new System.Drawing.Point(637, 54);
            this.lookUpEdit32.Name = "lookUpEdit32";
            this.lookUpEdit32.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit32.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit32.TabIndex = 74;
            // 
            // labelControl142
            // 
            this.labelControl142.Location = new System.Drawing.Point(517, 57);
            this.labelControl142.Name = "labelControl142";
            this.labelControl142.Size = new System.Drawing.Size(77, 13);
            this.labelControl142.TabIndex = 75;
            this.labelControl142.Text = "Cấp quyết định:";
            // 
            // memoEdit7
            // 
            this.memoEdit7.Location = new System.Drawing.Point(117, 44);
            this.memoEdit7.Name = "memoEdit7";
            this.memoEdit7.Size = new System.Drawing.Size(370, 41);
            this.memoEdit7.TabIndex = 73;
            // 
            // labelControl141
            // 
            this.labelControl141.Location = new System.Drawing.Point(13, 57);
            this.labelControl141.Name = "labelControl141";
            this.labelControl141.Size = new System.Drawing.Size(46, 13);
            this.labelControl141.TabIndex = 72;
            this.labelControl141.Text = "Nội dung:";
            // 
            // lookUpEdit31
            // 
            this.lookUpEdit31.Location = new System.Drawing.Point(637, 18);
            this.lookUpEdit31.Name = "lookUpEdit31";
            this.lookUpEdit31.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit31.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit31.TabIndex = 70;
            // 
            // labelControl140
            // 
            this.labelControl140.Location = new System.Drawing.Point(517, 21);
            this.labelControl140.Name = "labelControl140";
            this.labelControl140.Size = new System.Drawing.Size(114, 13);
            this.labelControl140.TabIndex = 71;
            this.labelControl140.Text = "Hình thức khen thưởng:";
            // 
            // lookUpEdit30
            // 
            this.lookUpEdit30.Location = new System.Drawing.Point(347, 18);
            this.lookUpEdit30.Name = "lookUpEdit30";
            this.lookUpEdit30.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit30.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit30.TabIndex = 68;
            // 
            // labelControl139
            // 
            this.labelControl139.Location = new System.Drawing.Point(254, 21);
            this.labelControl139.Name = "labelControl139";
            this.labelControl139.Size = new System.Drawing.Size(87, 13);
            this.labelControl139.TabIndex = 69;
            this.labelControl139.Text = "Loại khen thưởng:";
            // 
            // dateEdit22
            // 
            this.dateEdit22.EditValue = null;
            this.dateEdit22.Location = new System.Drawing.Point(112, 18);
            this.dateEdit22.Name = "dateEdit22";
            this.dateEdit22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit22.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit22.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit22.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit22.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit22.Size = new System.Drawing.Size(113, 20);
            this.dateEdit22.TabIndex = 66;
            // 
            // labelControl138
            // 
            this.labelControl138.Location = new System.Drawing.Point(13, 21);
            this.labelControl138.Name = "labelControl138";
            this.labelControl138.Size = new System.Drawing.Size(93, 13);
            this.labelControl138.TabIndex = 0;
            this.labelControl138.Text = "Ngày khen thưởng:";
            // 
            // xtraTabPage15
            // 
            this.xtraTabPage15.Controls.Add(this.panelControl21);
            this.xtraTabPage15.Name = "xtraTabPage15";
            this.xtraTabPage15.Size = new System.Drawing.Size(948, 493);
            this.xtraTabPage15.Text = "Kỷ luật";
            // 
            // panelControl21
            // 
            this.panelControl21.Controls.Add(this.simpleButton78);
            this.panelControl21.Controls.Add(this.simpleButton79);
            this.panelControl21.Controls.Add(this.simpleButton80);
            this.panelControl21.Controls.Add(this.simpleButton81);
            this.panelControl21.Controls.Add(this.simpleButton82);
            this.panelControl21.Controls.Add(this.gridKyluat);
            this.panelControl21.Controls.Add(this.lookUpEdit33);
            this.panelControl21.Controls.Add(this.labelControl151);
            this.panelControl21.Controls.Add(this.memoEdit8);
            this.panelControl21.Controls.Add(this.labelControl152);
            this.panelControl21.Controls.Add(this.lookUpEdit34);
            this.panelControl21.Controls.Add(this.labelControl153);
            this.panelControl21.Controls.Add(this.lookUpEdit35);
            this.panelControl21.Controls.Add(this.labelControl154);
            this.panelControl21.Controls.Add(this.dateEdit23);
            this.panelControl21.Controls.Add(this.labelControl155);
            this.panelControl21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl21.Location = new System.Drawing.Point(0, 0);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(948, 493);
            this.panelControl21.TabIndex = 1;
            // 
            // simpleButton78
            // 
            this.simpleButton78.Location = new System.Drawing.Point(775, 87);
            this.simpleButton78.Name = "simpleButton78";
            this.simpleButton78.Size = new System.Drawing.Size(75, 23);
            this.simpleButton78.TabIndex = 117;
            this.simpleButton78.Text = "Xóa";
            // 
            // simpleButton79
            // 
            this.simpleButton79.Location = new System.Drawing.Point(693, 87);
            this.simpleButton79.Name = "simpleButton79";
            this.simpleButton79.Size = new System.Drawing.Size(75, 23);
            this.simpleButton79.TabIndex = 116;
            this.simpleButton79.Text = "Hiệu chỉnh";
            // 
            // simpleButton80
            // 
            this.simpleButton80.Location = new System.Drawing.Point(611, 87);
            this.simpleButton80.Name = "simpleButton80";
            this.simpleButton80.Size = new System.Drawing.Size(75, 23);
            this.simpleButton80.TabIndex = 115;
            this.simpleButton80.Text = "Thêm";
            // 
            // simpleButton81
            // 
            this.simpleButton81.Location = new System.Drawing.Point(529, 87);
            this.simpleButton81.Name = "simpleButton81";
            this.simpleButton81.Size = new System.Drawing.Size(75, 23);
            this.simpleButton81.TabIndex = 114;
            this.simpleButton81.Text = "Làm mới";
            // 
            // simpleButton82
            // 
            this.simpleButton82.Location = new System.Drawing.Point(447, 87);
            this.simpleButton82.Name = "simpleButton82";
            this.simpleButton82.Size = new System.Drawing.Size(75, 23);
            this.simpleButton82.TabIndex = 113;
            this.simpleButton82.Text = "Lưu";
            // 
            // gridKyluat
            // 
            this.gridKyluat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridKyluat.Location = new System.Drawing.Point(23, 122);
            this.gridKyluat.MainView = this.gridViewKyluat;
            this.gridKyluat.Name = "gridKyluat";
            this.gridKyluat.Size = new System.Drawing.Size(903, 232);
            this.gridKyluat.TabIndex = 112;
            this.gridKyluat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewKyluat});
            // 
            // gridViewKyluat
            // 
            this.gridViewKyluat.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewKyluat.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewKyluat.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColKyluatID,
            this.gridColKyluatNgay,
            this.gridColKyluatLoai,
            this.gridColKyluatNoidung,
            this.gridColKyluatHinhthuc,
            this.gridColumn7});
            this.gridViewKyluat.GridControl = this.gridKyluat;
            this.gridViewKyluat.GroupPanelText = "Danh sách Kỷ luật";
            this.gridViewKyluat.Name = "gridViewKyluat";
            // 
            // gridColKyluatID
            // 
            this.gridColKyluatID.Caption = "Mã ID";
            this.gridColKyluatID.FieldName = "ID";
            this.gridColKyluatID.Name = "gridColKyluatID";
            this.gridColKyluatID.OptionsColumn.AllowEdit = false;
            this.gridColKyluatID.OptionsColumn.ReadOnly = true;
            // 
            // gridColKyluatNgay
            // 
            this.gridColKyluatNgay.Caption = "Ngày kỷ luật";
            this.gridColKyluatNgay.FieldName = "NGAYKL";
            this.gridColKyluatNgay.Name = "gridColKyluatNgay";
            this.gridColKyluatNgay.OptionsColumn.AllowEdit = false;
            this.gridColKyluatNgay.OptionsColumn.ReadOnly = true;
            this.gridColKyluatNgay.Visible = true;
            this.gridColKyluatNgay.VisibleIndex = 0;
            // 
            // gridColKyluatLoai
            // 
            this.gridColKyluatLoai.Caption = "Loại kỷ luật";
            this.gridColKyluatLoai.FieldName = "LOAIKL";
            this.gridColKyluatLoai.Name = "gridColKyluatLoai";
            this.gridColKyluatLoai.OptionsColumn.AllowEdit = false;
            this.gridColKyluatLoai.OptionsColumn.ReadOnly = true;
            this.gridColKyluatLoai.Visible = true;
            this.gridColKyluatLoai.VisibleIndex = 1;
            // 
            // gridColKyluatNoidung
            // 
            this.gridColKyluatNoidung.Caption = "Lý do kỷ luật";
            this.gridColKyluatNoidung.FieldName = "LYDOKL";
            this.gridColKyluatNoidung.Name = "gridColKyluatNoidung";
            this.gridColKyluatNoidung.OptionsColumn.AllowEdit = false;
            this.gridColKyluatNoidung.OptionsColumn.ReadOnly = true;
            this.gridColKyluatNoidung.Visible = true;
            this.gridColKyluatNoidung.VisibleIndex = 2;
            // 
            // gridColKyluatHinhthuc
            // 
            this.gridColKyluatHinhthuc.Caption = "Hình thức kỷ luật";
            this.gridColKyluatHinhthuc.FieldName = "HINHTHUCKL";
            this.gridColKyluatHinhthuc.Name = "gridColKyluatHinhthuc";
            this.gridColKyluatHinhthuc.OptionsColumn.AllowEdit = false;
            this.gridColKyluatHinhthuc.OptionsColumn.ReadOnly = true;
            this.gridColKyluatHinhthuc.Visible = true;
            this.gridColKyluatHinhthuc.VisibleIndex = 3;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Cấp QĐ khen thưởng";
            this.gridColumn7.FieldName = "CAPQUYETDINH";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // lookUpEdit33
            // 
            this.lookUpEdit33.Location = new System.Drawing.Point(709, 47);
            this.lookUpEdit33.Name = "lookUpEdit33";
            this.lookUpEdit33.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit33.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit33.TabIndex = 74;
            // 
            // labelControl151
            // 
            this.labelControl151.Location = new System.Drawing.Point(589, 50);
            this.labelControl151.Name = "labelControl151";
            this.labelControl151.Size = new System.Drawing.Size(77, 13);
            this.labelControl151.TabIndex = 75;
            this.labelControl151.Text = "Cấp quyết định:";
            // 
            // memoEdit8
            // 
            this.memoEdit8.Location = new System.Drawing.Point(184, 37);
            this.memoEdit8.Name = "memoEdit8";
            this.memoEdit8.Size = new System.Drawing.Size(370, 41);
            this.memoEdit8.TabIndex = 73;
            // 
            // labelControl152
            // 
            this.labelControl152.Location = new System.Drawing.Point(85, 50);
            this.labelControl152.Name = "labelControl152";
            this.labelControl152.Size = new System.Drawing.Size(66, 13);
            this.labelControl152.TabIndex = 72;
            this.labelControl152.Text = "Lý do Kỷ luật:";
            // 
            // lookUpEdit34
            // 
            this.lookUpEdit34.Location = new System.Drawing.Point(709, 11);
            this.lookUpEdit34.Name = "lookUpEdit34";
            this.lookUpEdit34.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit34.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit34.TabIndex = 70;
            // 
            // labelControl153
            // 
            this.labelControl153.Location = new System.Drawing.Point(589, 14);
            this.labelControl153.Name = "labelControl153";
            this.labelControl153.Size = new System.Drawing.Size(82, 13);
            this.labelControl153.TabIndex = 71;
            this.labelControl153.Text = "Hình thứckỷ luật:";
            // 
            // lookUpEdit35
            // 
            this.lookUpEdit35.Location = new System.Drawing.Point(419, 11);
            this.lookUpEdit35.Name = "lookUpEdit35";
            this.lookUpEdit35.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit35.Size = new System.Drawing.Size(140, 20);
            this.lookUpEdit35.TabIndex = 68;
            // 
            // labelControl154
            // 
            this.labelControl154.Location = new System.Drawing.Point(326, 14);
            this.labelControl154.Name = "labelControl154";
            this.labelControl154.Size = new System.Drawing.Size(58, 13);
            this.labelControl154.TabIndex = 69;
            this.labelControl154.Text = "Loại kỷ luật:";
            // 
            // dateEdit23
            // 
            this.dateEdit23.EditValue = null;
            this.dateEdit23.Location = new System.Drawing.Point(184, 11);
            this.dateEdit23.Name = "dateEdit23";
            this.dateEdit23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit23.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit23.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit23.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit23.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit23.Size = new System.Drawing.Size(113, 20);
            this.dateEdit23.TabIndex = 66;
            // 
            // labelControl155
            // 
            this.labelControl155.Location = new System.Drawing.Point(85, 14);
            this.labelControl155.Name = "labelControl155";
            this.labelControl155.Size = new System.Drawing.Size(64, 13);
            this.labelControl155.TabIndex = 0;
            this.labelControl155.Text = "Ngày kỷ luật:";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitterControl1.Location = new System.Drawing.Point(1, 23);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 230);
            this.splitterControl1.TabIndex = 12;
            this.splitterControl1.TabStop = false;
            // 
            // ucNVSoLuocLyLich1
            // 
            this.ucNVSoLuocLyLich1.AutoScroll = true;
            this.ucNVSoLuocLyLich1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVSoLuocLyLich1.Location = new System.Drawing.Point(0, 0);
            this.ucNVSoLuocLyLich1.Name = "ucNVSoLuocLyLich1";
            this.ucNVSoLuocLyLich1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVSoLuocLyLich1.TabIndex = 0;
            // 
            // ucNVTrinhDo1
            // 
            this.ucNVTrinhDo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVTrinhDo1.Location = new System.Drawing.Point(0, 0);
            this.ucNVTrinhDo1.Name = "ucNVTrinhDo1";
            this.ucNVTrinhDo1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVTrinhDo1.TabIndex = 0;
            // 
            // ucNVLuong1
            // 
            this.ucNVLuong1.AutoScroll = true;
            this.ucNVLuong1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNVLuong1.Location = new System.Drawing.Point(0, 0);
            this.ucNVLuong1.Name = "ucNVLuong1";
            this.ucNVLuong1.Size = new System.Drawing.Size(1254, 353);
            this.ucNVLuong1.TabIndex = 0;
            // 
            // ucDanhSachNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucDanhSachNhanVien";
            this.Size = new System.Drawing.Size(1270, 518);
            this.Load += new System.EventHandler(this.ucDanhSachNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabNhanVien)).EndInit();
            this.tabNhanVien.ResumeLayout(false);
            this.tabDSNhanVien.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).EndInit();
            this.grcTimKiem.ResumeLayout(false);
            this.grcTimKiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).EndInit();
            this.tabHieuchinhNV.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenBMBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).EndInit();
            this.xtraTabControl3.ResumeLayout(false);
            this.tabPageSoLuocLL.ResumeLayout(false);
            this.tabPageQuaTrinhCT.ResumeLayout(false);
            this.tabPageTrinhDo.ResumeLayout(false);
            this.tabPageKhenThuongKyLuat.ResumeLayout(false);
            this.tabPageQuanHeGiaDinh.ResumeLayout(false);
            this.tabPageLuong.ResumeLayout(false);
            this.tabPageHopDong.ResumeLayout(false);
            this.tabPageQuyetDinh.ResumeLayout(false);
            this.tabPageThiDua.ResumeLayout(false);
            this.tabPagePhanCong.ResumeLayout(false);
            this.tabPageBaoHiem.ResumeLayout(false);
            this.tabPageTuNhanXet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPg6TuNhanXet.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabLyLichCB.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1EmailTail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1GiaDinhChinhSach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NhomMau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangSecond.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangFirst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1CanNang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ChieuCao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QuanHamCaoNhat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TinhTrangSucKhoe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Hang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1PhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1BMBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChuyenMon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangLamViec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuocTich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1VanHoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangHonNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiCapCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiSinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QueQuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TonGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1DanToc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Ten.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1SoCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TenKhac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQTCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQTCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBCCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.panelControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQHGD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQHGD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit14.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit13.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            this.xtraTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            this.panelControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit15.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit16.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit17.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            this.xtraTabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPhancong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhanCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit18.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).EndInit();
            this.xtraTabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHXH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHXH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit19.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHYT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHYT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit20.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            this.xtraTabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            this.panelControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSuckhoe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSuckhoe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit21.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit21.Properties)).EndInit();
            this.xtraTabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThiduathang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDHK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDHocKy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDNam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDNam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).EndInit();
            this.xtraTabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKhenthuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKhenthuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit22.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit22.Properties)).EndInit();
            this.xtraTabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            this.panelControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKyluat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKyluat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit23.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit23.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelControl panelControl1;
        private XtraTabControl tabNhanVien;
        private XtraTabPage tabDSNhanVien;
        private SplitterControl splitterControl1;
        private XtraTabPage tabHieuchinhNV;
        private SplitContainerControl splitContainerControl4;
        private GroupControl grcTimKiem;
        private SimpleButton btnTimKiemCMND;
        private LabelControl labelControl5;
        private TextEdit txtTKCMND;
        private SimpleButton btnTimKiemMaNV;
        private LabelControl labelControl4;
        private TextEdit txtTKMaNV;
        private LabelControl labelControl2;
        private LookUpEdit lueTKBPBM;
        private LabelControl labelControl1;
        private LookUpEdit lueTKPhongKhoa;
        private PanelControl panelControl2;
        private SimpleButton btnThem;
        private SimpleButton btnXem;
        private GridControl gcDSNhanVien;
        private GridView gridViewDSNhanVien;
        private GridColumn gridColNVID;
        private GridColumn gridColMaNV;
        private GridColumn gridColHo;
        private GridColumn gridColTen;
        private GridColumn gridColNgaySinh;
        private RepositoryItemDateEdit repositoryItemDEDSNgayS;
        private GridColumn gridColGioiTinh;
        private GridColumn gridColCNND;
        private GridColumn gridColEmail;
        private GridColumn gridColChucvu;
        private GridColumn gridColPhongKhoa;
        private GridColumn gridColBoMon;
        private GridColumn gridColTinhTrang;
        private SplitContainerControl splitContainerControl5;
        private PanelControl panelControl7;
        private PanelControl panelControl9;
        private GroupControl groupControl3;
        private LabelControl labelControl6;
        private TextEdit txtTenBMBP;
        private TextEdit txtHoTen;
        private TextEdit txtTenPhongKhoa;
        private TextEdit txtMaNV;
        private LabelControl labelControl7;
        private LabelControl labelControl8;
        private LabelControl labelControl9;
        private PictureEdit imgHinhAnh;
        private XtraTabControl xtraTabControl3;
        private XtraTabPage tabPageSoLuocLL;
        private XtraTabPage tabPageQuaTrinhCT;
        private XtraTabPage xtraTabPage1;
        private PanelControl panelControl8;
        private XtraTabControl xtraTabControl1;
        private XtraTabPage tabLyLichCB;
        private XtraScrollableControl xtraScrollableControl1;
        private PanelControl panelControl4;
        private XtraTabPage xtraTabPage3;
        private PanelControl panelControl11;
        private MemoEdit memoEdit1;
        private LabelControl labelControl71;
        private TextEdit textEdit18;
        private LabelControl labelControl70;
        private GridControl gridQTCT;
        private GridView gridViewQTCT;
        private GridColumn gridColID;
        private GridColumn gridColNoiCT;
        private GridColumn gridColNghenghiep;
        private GridColumn gridColTuNgay;
        private GridColumn gridColDenNgay;
        private GridColumn gridColThanhTich;
        private GridColumn gridColGhiChu;
        private SimpleButton simpleButton8;
        private SimpleButton simpleButton7;
        private SimpleButton simpleButton6;
        private SimpleButton simpleButton5;
        private SimpleButton simpleButton4;
        private LabelControl labelControl69;
        private DateEdit dateEdit5;
        private LabelControl labelControl68;
        private DateEdit dateEdit4;
        private TextEdit textEdit17;
        private LabelControl labelControl67;
        private TextEdit textEdit16;
        private LabelControl labelControl66;
        private XtraTabPage xtraTabPage2;
        private PanelControl panelControl12;
        private GridControl gridBCCC;
        private GridView gridViewBCCC;
        private GridColumn gridColBCID;
        private GridColumn gridColMaBCCC;
        private GridColumn gridColLoaiBCCC;
        private GridColumn gridColChuyenmon;
        private GridColumn gridColTrinhDo;
        private GridColumn gridColChuyennganh;
        private GridColumn gridColCoSoDT;
        private GridColumn gridColTgianDT;
        private GridColumn gridColTgHieuluc;
        private GridColumn gridColNgayCapBCCC;
        private GridColumn gridColHDT;
        private GridColumn gridColGHICHUBCCC;
        private SimpleButton simpleButton9;
        private SimpleButton simpleButton10;
        private SimpleButton simpleButton11;
        private SimpleButton simpleButton12;
        private SimpleButton simpleButton13;
        private MemoEdit memoEdit2;
        private LabelControl labelControl82;
        private LabelControl labelControl81;
        private DateEdit dateEdit6;
        private TextEdit textEdit22;
        private LabelControl labelControl80;
        private TextEdit textEdit21;
        private LabelControl labelControl79;
        private LookUpEdit lookUpEdit21;
        private LabelControl labelControl78;
        private LookUpEdit lookUpEdit20;
        private LabelControl labelControl77;
        private LookUpEdit lookUpEdit19;
        private LabelControl labelControl76;
        private LookUpEdit lookUpEdit18;
        private LabelControl labelControl75;
        private TextEdit textEdit20;
        private LabelControl labelControl74;
        private LookUpEdit lookUpEdit17;
        private LabelControl labelControl73;
        private TextEdit textEdit19;
        private LabelControl labelControl72;
        private XtraTabPage xtraTabPage4;
        private PanelControl panelControl13;
        private GridControl gridLuong;
        private GridView gridViewLuong;
        private GridColumn gridColLuongID;
        private GridColumn gridColLuongSQD;
        private GridColumn gridColLuongNgach;
        private GridColumn gridColLuongBac;
        private GridColumn gridColLuongHeSo;
        private GridColumn gridColLuongCB;
        private GridColumn gridColLuongChinh;
        private GridColumn gridColLuongPCCV;
        private GridColumn gridColLuongPCTN;
        private GridColumn gridColLuongVuot;
        private GridColumn gridColLuongBHXH;
        private GridColumn gridColLuongNgayhuong;
        private SimpleButton simpleButton14;
        private SimpleButton simpleButton15;
        private SimpleButton simpleButton16;
        private SimpleButton simpleButton17;
        private SimpleButton simpleButton18;
        private DateEdit dateEdit7;
        private LabelControl labelControl93;
        private TextEdit textEdit32;
        private LabelControl labelControl92;
        private TextEdit textEdit31;
        private LabelControl labelControl91;
        private TextEdit textEdit30;
        private LabelControl labelControl90;
        private TextEdit textEdit29;
        private LabelControl labelControl89;
        private TextEdit textEdit28;
        private LabelControl labelControl88;
        private TextEdit textEdit27;
        private LabelControl labelControl87;
        private TextEdit textEdit26;
        private LabelControl labelControl86;
        private TextEdit textEdit25;
        private LabelControl labelControl85;
        private TextEdit textEdit24;
        private LabelControl labelControl84;
        private TextEdit textEdit23;
        private LabelControl labelControl83;
        private XtraTabPage xtraTabPage5;
        private PanelControl panelControl14;
        private SimpleButton simpleButton19;
        private SimpleButton simpleButton20;
        private SimpleButton simpleButton21;
        private SimpleButton simpleButton22;
        private DateEdit dateEdit10;
        private LabelControl labelControl101;
        private DateEdit dateEdit9;
        private LabelControl labelControl100;
        private TextEdit textEdit37;
        private LabelControl labelControl99;
        private TextEdit textEdit36;
        private LabelControl labelControl98;
        private TextEdit textEdit35;
        private LabelControl labelControl97;
        private DateEdit dateEdit8;
        private LabelControl labelControl96;
        private TextEdit textEdit34;
        private LabelControl labelControl95;
        private TextEdit textEdit33;
        private LabelControl labelControl94;
        private XtraTabPage xtraTabPage6;
        private PanelControl panelControl15;
        private SimpleButton simpleButton23;
        private SimpleButton simpleButton24;
        private SimpleButton simpleButton25;
        private SimpleButton simpleButton26;
        private SimpleButton simpleButton27;
        private GridControl gridQHGD;
        private GridView gridViewQHGD;
        private GridColumn gridColQHGDID;
        private GridColumn gridColQHGDHoTen;
        private GridColumn gridColQHGDQuanhe;
        private GridColumn gridColQHGDNgaySinh;
        private GridColumn gridColQHGDNgheNghiep;
        private GridColumn gridColQHGDNoiO;
        private GridColumn gridColQHGDDacDiem;
        private GridColumn gridColQHGDGhiChu;
        private MemoEdit memoEdit4;
        private LabelControl labelControl110;
        private MemoEdit memoEdit3;
        private LabelControl labelControl109;
        private GroupBox groupBox16;
        private LookUpEdit lookUpEdit24;
        private LabelControl labelControl108;
        private LookUpEdit lookUpEdit23;
        private LabelControl labelControl107;
        private TextEdit textEdit40;
        private LabelControl labelControl106;
        private TextEdit textEdit39;
        private LabelControl labelControl105;
        private DateEdit dateEdit11;
        private LabelControl labelControl104;
        private TextEdit textEdit38;
        private LabelControl labelControl103;
        private LookUpEdit lookUpEdit22;
        private LabelControl labelControl102;
        private XtraTabPage xtraTabPage7;
        private PanelControl panelControl16;
        private GridControl gridHopdong;
        private GridView gridViewHopDong;
        private GridColumn gridColumn1;
        private GridColumn gridColHopDongMaHD;
        private GridColumn gridColHopDongLoaiHD;
        private GridColumn gridColHopDongNgayky;
        private GridColumn gridColHopDongNguoiky;
        private GridColumn gridColHopDongTungay;
        private GridColumn gridColHopDongDenngay;
        private GridColumn gridColHopDongNoidung;
        private SimpleButton simpleButton28;
        private SimpleButton simpleButton29;
        private SimpleButton simpleButton30;
        private SimpleButton simpleButton31;
        private SimpleButton simpleButton32;
        private MemoEdit memoEdit5;
        private LabelControl labelControl117;
        private DateEdit dateEdit14;
        private LabelControl labelControl116;
        private DateEdit dateEdit13;
        private LabelControl labelControl115;
        private TextEdit textEdit42;
        private LabelControl labelControl114;
        private DateEdit dateEdit12;
        private LabelControl labelControl113;
        private LookUpEdit lookUpEdit25;
        private LabelControl labelControl112;
        private TextEdit textEdit41;
        private LabelControl labelControl111;
        private XtraTabPage xtraTabPage8;
        private PanelControl panelControl17;
        private GridControl gridQD;
        private GridView gridViewQuyetDinh;
        private GridColumn gridColQDID;
        private GridColumn gridColQDSoQD;
        private GridColumn gridColQDLoaiQD;
        private GridColumn gridColQDNgayky;
        private GridColumn gridColQDNguoiky;
        private GridColumn gridColQDTungay;
        private GridColumn gridColQDDenngay;
        private GridColumn gridCoQDNoidung;
        private SimpleButton simpleButton33;
        private SimpleButton simpleButton34;
        private SimpleButton simpleButton35;
        private SimpleButton simpleButton36;
        private SimpleButton simpleButton37;
        private MemoEdit memoEdit6;
        private LabelControl labelControl118;
        private DateEdit dateEdit15;
        private LabelControl labelControl119;
        private DateEdit dateEdit16;
        private LabelControl labelControl120;
        private TextEdit textEdit43;
        private LabelControl labelControl121;
        private DateEdit dateEdit17;
        private LabelControl labelControl122;
        private LookUpEdit lookUpEdit26;
        private LabelControl labelControl123;
        private TextEdit textEdit44;
        private LabelControl labelControl124;
        private XtraTabPage xtraTabPage9;
        private PanelControl panelControl18;
        private GridControl gridPhancong;
        private GridView gridViewPhanCong;
        private GridColumn gridColPhancongID;
        private GridColumn gridColPhancongDonvi;
        private GridColumn gridColPhancongBomon;
        private GridColumn gridColPhancongCongviec;
        private GridColumn gridColPhancongTungay;
        private GridColumn gridColPhancongDamnhan;
        private RepositoryItemCheckEdit repositoryItemCheckEditSTT;
        private SimpleButton simpleButton38;
        private SimpleButton simpleButton39;
        private SimpleButton simpleButton40;
        private SimpleButton simpleButton41;
        private SimpleButton simpleButton42;
        private CheckEdit checkEdit1;
        private LabelControl labelControl128;
        private DateEdit dateEdit18;
        private LookUpEdit lookUpEdit29;
        private LabelControl labelControl127;
        private LookUpEdit lookUpEdit28;
        private LabelControl labelControl125;
        private LabelControl labelControl126;
        private LookUpEdit lookUpEdit27;
        private XtraTabPage xtraTabPage10;
        private SplitContainerControl splitContainerControl1;
        private GroupControl groupControl5;
        private GridControl gridBHXH;
        private GridView gridViewBHXH;
        private GridColumn gridColBHXHID;
        private GridColumn gridColBHXHSo;
        private GridColumn gridColBHXHNgaycap;
        private SimpleButton simpleButton48;
        private SimpleButton simpleButton49;
        private SimpleButton simpleButton50;
        private SimpleButton simpleButton51;
        private SimpleButton simpleButton52;
        private LabelControl labelControl130;
        private DateEdit dateEdit19;
        private TextEdit textEdit45;
        private LabelControl labelControl129;
        private GroupControl groupControl6;
        private GridControl gridBHYT;
        private GridView gridViewBHYT;
        private GridColumn gridColBHYTID;
        private GridColumn gridColBHYTSothe;
        private GridColumn gridColBHYTNoiDK;
        private GridColumn gridColBHYTNgaycap;
        private SimpleButton simpleButton43;
        private TextEdit textEdit47;
        private SimpleButton simpleButton44;
        private SimpleButton simpleButton45;
        private LabelControl labelControl133;
        private SimpleButton simpleButton46;
        private LabelControl labelControl131;
        private SimpleButton simpleButton47;
        private DateEdit dateEdit20;
        private TextEdit textEdit46;
        private LabelControl labelControl132;
        private XtraTabPage xtraTabPage11;
        private PanelControl panelControl19;
        private GridControl gridSuckhoe;
        private GridView gridViewSuckhoe;
        private GridColumn gridColSKID;
        private GridColumn gridColSKNgaykham;
        private GridColumn gridColSKTinhtrang;
        private GridColumn gridColSKChieucao;
        private GridColumn gridColSKCannang;
        private SimpleButton simpleButton53;
        private SimpleButton simpleButton54;
        private SimpleButton simpleButton55;
        private SimpleButton simpleButton56;
        private SimpleButton simpleButton57;
        private TextEdit textEdit49;
        private LabelControl labelControl137;
        private TextEdit textEdit48;
        private LabelControl labelControl136;
        private ComboBoxEdit comboBoxEdit2;
        private LabelControl labelControl135;
        private DateEdit dateEdit21;
        private LabelControl labelControl134;
        private XtraTabPage xtraTabPage12;
        private XtraTabControl xtraTabControl2;
        private XtraTabPage xtraTabPage13;
        private XtraTabPage xtraTabPage14;
        private PanelControl panelControl20;
        private SimpleButton simpleButton58;
        private SimpleButton simpleButton59;
        private SimpleButton simpleButton60;
        private SimpleButton simpleButton61;
        private SimpleButton simpleButton62;
        private GridControl gridKhenthuong;
        private GridView gridViewKhenthuong;
        private GridColumn gridColKhenthuongID;
        private GridColumn gridColKhenthuongNgay;
        private GridColumn gridColKhenthuongLoai;
        private GridColumn gridColKhenthuongNoidung;
        private GridColumn gridColKhenthuongHinhthuc;
        private GridColumn gridColKhenthuongCap;
        private LookUpEdit lookUpEdit32;
        private LabelControl labelControl142;
        private MemoEdit memoEdit7;
        private LabelControl labelControl141;
        private LookUpEdit lookUpEdit31;
        private LabelControl labelControl140;
        private LookUpEdit lookUpEdit30;
        private LabelControl labelControl139;
        private DateEdit dateEdit22;
        private LabelControl labelControl138;
        private XtraTabPage xtraTabPage15;
        private PanelControl panelControl21;
        private SimpleButton simpleButton78;
        private SimpleButton simpleButton79;
        private SimpleButton simpleButton80;
        private SimpleButton simpleButton81;
        private SimpleButton simpleButton82;
        private GridControl gridKyluat;
        private GridView gridViewKyluat;
        private GridColumn gridColKyluatID;
        private GridColumn gridColKyluatNgay;
        private GridColumn gridColKyluatLoai;
        private GridColumn gridColKyluatNoidung;
        private GridColumn gridColKyluatHinhthuc;
        private GridColumn gridColumn7;
        private LookUpEdit lookUpEdit33;
        private LabelControl labelControl151;
        private MemoEdit memoEdit8;
        private LabelControl labelControl152;
        private LookUpEdit lookUpEdit34;
        private LabelControl labelControl153;
        private LookUpEdit lookUpEdit35;
        private LabelControl labelControl154;
        private DateEdit dateEdit23;
        private LabelControl labelControl155;
        private GroupControl groupControl4;
        private LookUpEdit cbbPg1QuanHuyenHT;
        private ComboBoxEdit cbbPg1EmailTail;
        private LookUpEdit cbbPg1TinhThanhHT;
        private LookUpEdit cbbPg1TinhThanhTT;
        private LabelControl labelControl25;
        private LabelControl labelControl30;
        private LookUpEdit cbbPg1QuanHuyenTT;
        private LabelControl labelControl33;
        private LabelControl labelControl27;
        private LabelControl labelControl28;
        private LabelControl labelControl24;
        private LabelControl labelControl35;
        private LabelControl labelControl36;
        private TextEdit txtPg1Email;
        private TextEdit txtPg1DienThoai;
        private TextEdit txtPg1DiaChiTT;
        private TextEdit txtPg1DiaChiHT;
        private GroupControl groupControl1;
        private DateEdit dtPg1NgayXuatNgu;
        private ComboBoxEdit cbbPg1GiaDinhChinhSach;
        private ComboBoxEdit cbbPg1NhomMau;
        private DateEdit dtPg1NgayChinhThucDCSVN;
        private DateEdit dtPg1NgayNhapNgu;
        private DateEdit dtPg1NgayVaoDCSVN;
        private LabelControl labelControl34;
        private LabelControl labelControl45;
        private LabelControl labelControl47;
        private LabelControl labelControl46;
        private LabelControl labelControl38;
        private LabelControl labelControl40;
        private LabelControl labelControl42;
        private LabelControl labelControl10;
        private LabelControl labelControl11;
        private LabelControl labelControl12;
        private LabelControl labelControl13;
        private LabelControl labelControl14;
        private LabelControl labelControl43;
        private LabelControl labelControl44;
        private TextEdit txtPg1ThuongBinhHangSecond;
        private TextEdit txtPg1ThuongBinhHangFirst;
        private TextEdit txtPg1CanNang;
        private TextEdit txtPg1ChieuCao;
        private TextEdit txtPg1QuanHamCaoNhat;
        private TextEdit txtPg1TinhTrangSucKhoe;
        private GroupControl groupControl2;
        private ComboBoxEdit cbbPg1MaNgach;
        private ComboBoxEdit cbbPg1Hang;
        private ComboBoxEdit cbbPg1Bac;
        private LookUpEdit cbbPg1PhongKhoa;
        private LookUpEdit cbbPg1BMBP;
        private LookUpEdit cbbPg1ChuyenMon;
        private LookUpEdit cbbPg1ChucVu;
        private LookUpEdit cbbPg1TinhTrangLamViec;
        private DateEdit dtPg1NgayVaoLam;
        private LabelControl labelControl19;
        private LabelControl labelControl23;
        private LabelControl labelControl21;
        private LabelControl labelControl20;
        private LabelControl labelControl26;
        private LabelControl labelControl22;
        private LabelControl labelControl29;
        private LabelControl labelControl31;
        private LabelControl labelControl32;
        private GroupControl groupControl10;
        private ComboBoxEdit cbbPg1QuocTich;
        private ComboBoxEdit cbbPg1VanHoa;
        private ComboBoxEdit cbbPg1TinhTrangHonNhan;
        private LookUpEdit cbbPg1NoiCapCMND;
        private LookUpEdit cbbPg1NoiSinh;
        private LookUpEdit cbbPg1QueQuan;
        private LookUpEdit cbbPg1TonGiao;
        private LookUpEdit cbbPg1DanToc;
        private DateEdit dtPg1NgayCapCMND;
        private DateEdit dtPg1NgaySinh;
        private RadioGroup rdbPg1GioiTinh;
        private LabelControl labelControl15;
        private TextEdit txtPg1Ten;
        private LabelControl labelControl16;
        private LabelControl labelControl17;
        private LabelControl labelControl18;
        private LabelControl labelControl37;
        private LabelControl labelControl39;
        private LabelControl labelControl41;
        private LabelControl labelControl48;
        private LabelControl labelControl49;
        private LabelControl labelControl50;
        private TextEdit txtPg1SoCMND;
        private LabelControl labelControl51;
        private LabelControl labelControl52;
        private LabelControl labelControl53;
        private LabelControl labelControl54;
        private TextEdit txtPg1TenKhac;
        private LabelControl labelControl55;
        private TextEdit txtPg1HoDem;
        private ucNVQuaTrinhCongTac ucNVQuaTrinhCongTac1;
        private XtraTabPage tabPageTrinhDo;
        private XtraTabPage tabPageKhenThuongKyLuat;
        private ucNVKhenThuongKyLuat ucNVKhenThuongKyLuat1;
        private XtraTabPage tabPageQuanHeGiaDinh;
        private ucNVQuanHeGiaDinh ucNVQuanHeGiaDinh1;
        private XtraTabPage tabPageThiDua;
        private SplitContainerControl splitContainerControl2;
        private GroupControl groupControl7;
        private GridControl gridThang;
        private GridView gridViewThiduathang;
        private GridColumn gridColTDThangID;
        private GridColumn gridColTDThang;
        private GridColumn gridColTDThangNamhoc;
        private GridColumn gridColTDThangXeploai;
        private SimpleButton simpleButton63;
        private SimpleButton simpleButton64;
        private SimpleButton simpleButton65;
        private SimpleButton simpleButton66;
        private SimpleButton simpleButton67;
        private ComboBoxEdit comboBoxEdit5;
        private LabelControl labelControl145;
        private ComboBoxEdit comboBoxEdit4;
        private LabelControl labelControl144;
        private ComboBoxEdit comboBoxEdit3;
        private LabelControl labelControl143;
        private SplitContainerControl splitContainerControl3;
        private GroupControl groupControl8;
        private GridControl gridTDHK;
        private GridView gridViewTDHocKy;
        private GridColumn gridColTDHKID;
        private GridColumn gridColTDHKHocKy;
        private GridColumn gridColTDHKNamhoc;
        private GridColumn gridColTDHKXeploai;
        private SimpleButton simpleButton68;
        private SimpleButton simpleButton69;
        private SimpleButton simpleButton70;
        private SimpleButton simpleButton71;
        private SimpleButton simpleButton72;
        private ComboBoxEdit comboBoxEdit6;
        private LabelControl labelControl146;
        private ComboBoxEdit comboBoxEdit7;
        private LabelControl labelControl147;
        private ComboBoxEdit comboBoxEdit8;
        private LabelControl labelControl148;
        private GroupControl groupControl9;
        private GridControl gridTDNam;
        private GridView gridViewTDNam;
        private GridColumn gridColTDNamID;
        private GridColumn gridColTDNamNamhoc;
        private GridColumn gridColTDNamXeploai;
        private SimpleButton simpleButton73;
        private SimpleButton simpleButton74;
        private SimpleButton simpleButton75;
        private SimpleButton simpleButton76;
        private SimpleButton simpleButton77;
        private ComboBoxEdit comboBoxEdit9;
        private LabelControl labelControl149;
        private ComboBoxEdit comboBoxEdit10;
        private LabelControl labelControl150;
        private GridView gridView1;
        private GridView gridView2;
        private GridView gridView3;
        private ucNVThiDua ucNVThiDua1;
        private XtraTabPage tabPageLuong;
        private XtraTabPage tabPageHopDong;
        private ucNVHopDong ucNVHopDong1;
        private XtraTabPage tabPageQuyetDinh;
        private ucNVQuyetDinh ucNVQuyetDinh1;
        private XtraTabPage tabPagePhanCong;
        private ucNVPhanCong ucNVPhanCong1;
        private XtraTabPage tabPageBaoHiem;
        private ucNVBaoHiem ucNVBaoHiem1;
        private XtraTabPage tabPageTuNhanXet;
        private GroupControl groupControl15;
        private MemoEdit txtPg6TuNhanXet;
        private SimpleButton btnPg6Sua;
        private SimpleButton btnPg6Them;
        private SimpleButton btnViewImg;
        private SimpleButton simpleButton1;
        private ucNVSoLuocLyLich ucNVSoLuocLyLich1;
        private ucNVTrinhDo ucNVTrinhDo1;
        private ucNVLuong ucNVLuong1;
    }
}
