﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    public partial class ucDashBoard : DevExpress.XtraEditors.XtraUserControl
    {
        public ucDashBoard()
        {
            InitializeComponent();
        }
        frmMain formMain;
        public ucDashBoard(frmMain frm)
        {
            InitializeComponent();
            formMain = frm;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (formMain != null)
            {
                formMain.OpenDanhSachNhanVien();
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            formMain.OpenChuyenGiaiDoan();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            formMain.OpenGiaHanHopDong();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            frmDanhSachUngVien frm = new frmDanhSachUngVien();
            frm.ShowDialog();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            formMain.OpenChuyenCongTac();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            formMain.OpenNghiViec();
        }
    }
}
