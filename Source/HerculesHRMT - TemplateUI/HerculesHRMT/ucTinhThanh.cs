﻿using DevExpress.XtraEditors;
using HerculesCTL;
using System.Collections.Generic;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Windows.Forms;
using System.Linq;

namespace HerculesHRMT
{
    public partial class ucTinhThanh : XtraUserControl
    {
        TinhThanhCTL ttCTL = new TinhThanhCTL();
        List<TinhThanhDTO> listTinhThanh = new List<TinhThanhDTO>();
        TinhThanhDTO ttSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucTinhThanh()
        {
            InitializeComponent();
        }

        private void ucTinhThanh_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void LoadData()
        {
            listTinhThanh = ttCTL.GetAll();
            gcTinhThanh.DataSource = listTinhThanh;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(TinhThanhDTO tinhThanhDTO)
        {
            if (tinhThanhDTO != null)
            {
                txtTinhThanh.Text = tinhThanhDTO.TenTinhThanh;
                txtTenDD.Text = tinhThanhDTO.TenDayDu;
                txtTenVT.Text = tinhThanhDTO.TenVietTat;
                radLoaiTinhThanh.EditValue = tinhThanhDTO.Loai;
                txtDauSoCMND.Text = tinhThanhDTO.DauSoCMND;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtTinhThanh.Properties.ReadOnly = false;
                    txtTenDD.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    radLoaiTinhThanh.Properties.ReadOnly = false;
                    txtDauSoCMND.Properties.ReadOnly = false;

                    txtTinhThanh.Text = string.Empty;
                    txtTenDD.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    radLoaiTinhThanh.EditValue = 0;
                    txtDauSoCMND.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtTinhThanh.Properties.ReadOnly = false;
                    txtTenDD.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    radLoaiTinhThanh.Properties.ReadOnly = false;
                    txtDauSoCMND.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(ttSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtTinhThanh.Properties.ReadOnly = true;
                    txtTenDD.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    radLoaiTinhThanh.Properties.ReadOnly = true;
                    txtDauSoCMND.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(ttSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private TinhThanhDTO GetData(TinhThanhDTO _ttDTO)
        {
            string maTT = "TT";
            if (_selectedStatus == NEW)
            {
                string sLastIndexTT = ttCTL.GetMaxMaTinhThanh();
                if (sLastIndexTT.CompareTo(string.Empty) != 0)
                {                    
                    int nLastIndexTT = int.Parse(sLastIndexTT.Split(new string[] { "TT" }, System.StringSplitOptions.RemoveEmptyEntries)[0]);
                    maTT += (nLastIndexTT + 1).ToString().PadLeft(3, '0');
                }
                else
                    maTT += "001";

                _ttDTO.NgayHieuLuc = DateTime.Now;
                _ttDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maTT = ttSelect.MaTinhThanh;
                _ttDTO.NgayHieuLuc = ttSelect.NgayHieuLuc;
                _ttDTO.TinhTrang = ttSelect.TinhTrang;
            }
            _ttDTO.MaTinhThanh = maTT;
            _ttDTO.TenTinhThanh = txtTinhThanh.Text;
            _ttDTO.TenDayDu = txtTenDD.Text;
            _ttDTO.TenVietTat = txtTenVT.Text;
            _ttDTO.Loai = (int)radLoaiTinhThanh.EditValue;
            _ttDTO.DauSoCMND = txtDauSoCMND.Text;

            return _ttDTO;
        }

        private void gridViewTinhThanh_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listTinhThanh.Count)
                ttSelect = ((GridView)sender).GetRow(_selectedIndex) as TinhThanhDTO;
            else
                if (listTinhThanh.Count != 0)
                    ttSelect = listTinhThanh[0];
                else
                    ttSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private bool CheckInputData()
        {
            if (txtTinhThanh.Text == string.Empty || txtTenDD.Text == string.Empty || txtTenVT.Text == string.Empty || txtDauSoCMND.Text == string.Empty)
                return false;

            return true;
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên Tỉnh/Thành\n Tên đầy đủ\n Tên viết tắt\n Đầu số CMND\n Loại", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TinhThanhDTO ttNew = GetData(new TinhThanhDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = ttCTL.Save(ttNew);
            else
                isSucess = ttCTL.Update(ttNew);

            if (isSucess)
            {                
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listTinhThanh.IndexOf(listTinhThanh.FirstOrDefault(p => p.MaTinhThanh == ttNew.MaTinhThanh));
                gridViewTinhThanh.FocusedRowHandle = _selectedIndex;                
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;
            
            SetStatus(VIEW);
        }

        private void txtDauSoCMND_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            int temp = -1;
            if (_selectedStatus == NEW || _selectedStatus == EDIT)
                if ((!int.TryParse((string)e.NewValue, out temp) && e.NewValue.ToString().CompareTo(string.Empty) != 0) || e.NewValue.ToString().Length > 2)
                    e.Cancel = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ttSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa tỉnh " + ttSelect.TenTinhThanh + " hay không?";
                
                if(XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = ttCTL.Delete(ttSelect);

                    if (isSucess)
                    {                        
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if(listTinhThanh.Count != null)
                            gridViewTinhThanh.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
