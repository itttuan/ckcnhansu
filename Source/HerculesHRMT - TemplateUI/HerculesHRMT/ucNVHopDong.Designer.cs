﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVHopDong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcHopDong = new DevExpress.XtraEditors.GroupControl();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.meNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucTu = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.txtNguoiKy = new DevExpress.XtraEditors.TextEdit();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.deNgayKy = new DevExpress.XtraEditors.DateEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoaiHD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaHD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.gridHopdong = new DevExpress.XtraGrid.GridControl();
            this.gridViewHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongMaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongLoaiHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNgayky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNguoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHDXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcHopDong)).BeginInit();
            this.gcHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            this.SuspendLayout();
            // 
            // gcHopDong
            // 
            this.gcHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcHopDong.Controls.Add(this.btnPg3Xoa);
            this.gcHopDong.Controls.Add(this.btnPg3Them);
            this.gcHopDong.Controls.Add(this.btnPg3TaoMoi);
            this.gcHopDong.Controls.Add(this.meNoiDung);
            this.gcHopDong.Controls.Add(this.labelControl117);
            this.gcHopDong.Controls.Add(this.deHieuLucDen);
            this.gcHopDong.Controls.Add(this.labelControl116);
            this.gcHopDong.Controls.Add(this.deHieuLucTu);
            this.gcHopDong.Controls.Add(this.labelControl115);
            this.gcHopDong.Controls.Add(this.txtNguoiKy);
            this.gcHopDong.Controls.Add(this.labelControl114);
            this.gcHopDong.Controls.Add(this.deNgayKy);
            this.gcHopDong.Controls.Add(this.labelControl113);
            this.gcHopDong.Controls.Add(this.lueLoaiHD);
            this.gcHopDong.Controls.Add(this.labelControl112);
            this.gcHopDong.Controls.Add(this.txtMaHD);
            this.gcHopDong.Controls.Add(this.labelControl111);
            this.gcHopDong.Location = new System.Drawing.Point(15, 14);
            this.gcHopDong.Name = "gcHopDong";
            this.gcHopDong.Size = new System.Drawing.Size(1240, 210);
            this.gcHopDong.TabIndex = 0;
            this.gcHopDong.Text = "Hợp đồng làm việc";
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(238, 168);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 105;
            this.btnPg3Xoa.Text = "Xóa";
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(129, 168);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 106;
            this.btnPg3Them.Text = "Thêm";
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(20, 168);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 107;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            // 
            // meNoiDung
            // 
            this.meNoiDung.Location = new System.Drawing.Point(654, 53);
            this.meNoiDung.Name = "meNoiDung";
            this.meNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meNoiDung.Properties.Appearance.Options.UseFont = true;
            this.meNoiDung.Size = new System.Drawing.Size(249, 99);
            this.meNoiDung.TabIndex = 77;
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl117.Location = new System.Drawing.Point(585, 57);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(52, 15);
            this.labelControl117.TabIndex = 76;
            this.labelControl117.Text = "Nội dung:";
            // 
            // deHieuLucDen
            // 
            this.deHieuLucDen.EditValue = null;
            this.deHieuLucDen.Location = new System.Drawing.Point(378, 126);
            this.deHieuLucDen.Name = "deHieuLucDen";
            this.deHieuLucDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucDen.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucDen.Size = new System.Drawing.Size(155, 22);
            this.deHieuLucDen.TabIndex = 75;
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl116.Location = new System.Drawing.Point(308, 129);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(56, 15);
            this.labelControl116.TabIndex = 74;
            this.labelControl116.Text = "Đến ngày:";
            // 
            // deHieuLucTu
            // 
            this.deHieuLucTu.EditValue = null;
            this.deHieuLucTu.Location = new System.Drawing.Point(105, 126);
            this.deHieuLucTu.Name = "deHieuLucTu";
            this.deHieuLucTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucTu.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucTu.Size = new System.Drawing.Size(159, 22);
            this.deHieuLucTu.TabIndex = 73;
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl115.Location = new System.Drawing.Point(22, 129);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(49, 15);
            this.labelControl115.TabIndex = 72;
            this.labelControl115.Text = "Từ ngày:";
            // 
            // txtNguoiKy
            // 
            this.txtNguoiKy.Location = new System.Drawing.Point(378, 90);
            this.txtNguoiKy.Name = "txtNguoiKy";
            this.txtNguoiKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKy.Properties.Appearance.Options.UseFont = true;
            this.txtNguoiKy.Size = new System.Drawing.Size(155, 22);
            this.txtNguoiKy.TabIndex = 71;
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl114.Location = new System.Drawing.Point(308, 93);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(55, 15);
            this.labelControl114.TabIndex = 70;
            this.labelControl114.Text = "Người ký:";
            // 
            // deNgayKy
            // 
            this.deNgayKy.EditValue = null;
            this.deNgayKy.Location = new System.Drawing.Point(105, 90);
            this.deNgayKy.Name = "deNgayKy";
            this.deNgayKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgayKy.Properties.Appearance.Options.UseFont = true;
            this.deNgayKy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayKy.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayKy.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayKy.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayKy.Size = new System.Drawing.Size(159, 22);
            this.deNgayKy.TabIndex = 69;
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl113.Location = new System.Drawing.Point(22, 93);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(47, 15);
            this.labelControl113.TabIndex = 68;
            this.labelControl113.Text = "Ngày ký:";
            // 
            // lueLoaiHD
            // 
            this.lueLoaiHD.Location = new System.Drawing.Point(378, 54);
            this.lueLoaiHD.Name = "lueLoaiHD";
            this.lueLoaiHD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueLoaiHD.Properties.Appearance.Options.UseFont = true;
            this.lueLoaiHD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiHD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiHD", "Mã Loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHD", "Loại Hợp Đồng")});
            this.lueLoaiHD.Properties.DisplayMember = "TenLoaiHD";
            this.lueLoaiHD.Properties.NullText = "";
            this.lueLoaiHD.Properties.ValueMember = "MaLoaiHD";
            this.lueLoaiHD.Size = new System.Drawing.Size(155, 22);
            this.lueLoaiHD.TabIndex = 67;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(308, 57);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(47, 15);
            this.labelControl112.TabIndex = 66;
            this.labelControl112.Text = "Loại HĐ:";
            // 
            // txtMaHD
            // 
            this.txtMaHD.Location = new System.Drawing.Point(105, 54);
            this.txtMaHD.Name = "txtMaHD";
            this.txtMaHD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHD.Properties.Appearance.Options.UseFont = true;
            this.txtMaHD.Size = new System.Drawing.Size(159, 22);
            this.txtMaHD.TabIndex = 65;
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl111.Location = new System.Drawing.Point(22, 57);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(40, 15);
            this.labelControl111.TabIndex = 64;
            this.labelControl111.Text = "Mã HĐ:";
            // 
            // gridHopdong
            // 
            this.gridHopdong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHopdong.Location = new System.Drawing.Point(15, 225);
            this.gridHopdong.MainView = this.gridViewHopDong;
            this.gridHopdong.Name = "gridHopdong";
            this.gridHopdong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa});
            this.gridHopdong.Size = new System.Drawing.Size(1240, 282);
            this.gridHopdong.TabIndex = 126;
            this.gridHopdong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHopDong});
            // 
            // gridViewHopDong
            // 
            this.gridViewHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColHopDongMaHD,
            this.gridColHopDongLoaiHD,
            this.gridColHopDongNgayky,
            this.gridColHopDongNguoiky,
            this.gridColHopDongTungay,
            this.gridColHopDongDenngay,
            this.gridColHopDongNoidung,
            this.gridColHDXoa});
            this.gridViewHopDong.GridControl = this.gridHopdong;
            this.gridViewHopDong.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewHopDong.Name = "gridViewHopDong";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColHopDongMaHD
            // 
            this.gridColHopDongMaHD.Caption = "Mã Hợp đồng";
            this.gridColHopDongMaHD.FieldName = "MAHD";
            this.gridColHopDongMaHD.Name = "gridColHopDongMaHD";
            this.gridColHopDongMaHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongMaHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongMaHD.Visible = true;
            this.gridColHopDongMaHD.VisibleIndex = 0;
            // 
            // gridColHopDongLoaiHD
            // 
            this.gridColHopDongLoaiHD.Caption = "Loại hợp đồng";
            this.gridColHopDongLoaiHD.FieldName = "TENLOAIHD";
            this.gridColHopDongLoaiHD.Name = "gridColHopDongLoaiHD";
            this.gridColHopDongLoaiHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongLoaiHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongLoaiHD.Visible = true;
            this.gridColHopDongLoaiHD.VisibleIndex = 1;
            // 
            // gridColHopDongNgayky
            // 
            this.gridColHopDongNgayky.Caption = "Ngày ký";
            this.gridColHopDongNgayky.FieldName = "NGAYKY";
            this.gridColHopDongNgayky.Name = "gridColHopDongNgayky";
            this.gridColHopDongNgayky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNgayky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNgayky.Visible = true;
            this.gridColHopDongNgayky.VisibleIndex = 2;
            // 
            // gridColHopDongNguoiky
            // 
            this.gridColHopDongNguoiky.Caption = "Người ký";
            this.gridColHopDongNguoiky.FieldName = "NGUOIKY";
            this.gridColHopDongNguoiky.Name = "gridColHopDongNguoiky";
            this.gridColHopDongNguoiky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNguoiky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNguoiky.Visible = true;
            this.gridColHopDongNguoiky.VisibleIndex = 3;
            // 
            // gridColHopDongTungay
            // 
            this.gridColHopDongTungay.Caption = "Từ ngày";
            this.gridColHopDongTungay.FieldName = "TUNGAY";
            this.gridColHopDongTungay.Name = "gridColHopDongTungay";
            this.gridColHopDongTungay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongTungay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongTungay.Visible = true;
            this.gridColHopDongTungay.VisibleIndex = 4;
            // 
            // gridColHopDongDenngay
            // 
            this.gridColHopDongDenngay.Caption = "Đến ngày";
            this.gridColHopDongDenngay.FieldName = "DENNGAY";
            this.gridColHopDongDenngay.Name = "gridColHopDongDenngay";
            this.gridColHopDongDenngay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongDenngay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongDenngay.Visible = true;
            this.gridColHopDongDenngay.VisibleIndex = 5;
            // 
            // gridColHopDongNoidung
            // 
            this.gridColHopDongNoidung.Caption = "Nội dung";
            this.gridColHopDongNoidung.FieldName = "NOIDUNG";
            this.gridColHopDongNoidung.Name = "gridColHopDongNoidung";
            this.gridColHopDongNoidung.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNoidung.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNoidung.Visible = true;
            this.gridColHopDongNoidung.VisibleIndex = 6;
            // 
            // gridColHDXoa
            // 
            this.gridColHDXoa.Caption = "Xóa";
            this.gridColHDXoa.ColumnEdit = this.repositoryItemHyperLinkEditXoa;
            this.gridColHDXoa.Name = "gridColHDXoa";
            this.gridColHDXoa.Visible = true;
            this.gridColHDXoa.VisibleIndex = 7;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // ucNVHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridHopdong);
            this.Controls.Add(this.gcHopDong);
            this.Name = "ucNVHopDong";
            this.Size = new System.Drawing.Size(1270, 510);
            this.Load += new System.EventHandler(this.ucNVHopDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcHopDong)).EndInit();
            this.gcHopDong.ResumeLayout(false);
            this.gcHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcHopDong;
        private LookUpEdit lueLoaiHD;
        private LabelControl labelControl112;
        private TextEdit txtMaHD;
        private LabelControl labelControl111;
        private TextEdit txtNguoiKy;
        private LabelControl labelControl114;
        private DateEdit deNgayKy;
        private LabelControl labelControl113;
        private DateEdit deHieuLucDen;
        private LabelControl labelControl116;
        private DateEdit deHieuLucTu;
        private LabelControl labelControl115;
        private MemoEdit meNoiDung;
        private LabelControl labelControl117;
        private GridControl gridHopdong;
        private GridView gridViewHopDong;
        private GridColumn gridColumn1;
        private GridColumn gridColHopDongMaHD;
        private GridColumn gridColHopDongLoaiHD;
        private GridColumn gridColHopDongNgayky;
        private GridColumn gridColHopDongNguoiky;
        private GridColumn gridColHopDongTungay;
        private GridColumn gridColHopDongDenngay;
        private GridColumn gridColHopDongNoidung;
        private GridColumn gridColHDXoa;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
    }
}
