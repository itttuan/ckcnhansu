﻿namespace HerculesHRMT
{
    partial class ucChuyenCongTac
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnChuyen = new DevExpress.XtraEditors.SimpleButton();
            this.dtPg1NgayVaoLam = new DevExpress.XtraEditors.DateEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lueQD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.cbbPg1ChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lueBPBMDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.luePhongKhoaDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcDSNhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridViewDSNhanVien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCNND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChucvu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDEDSNgayS = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueBPBMTu = new DevExpress.XtraEditors.LookUpEdit();
            this.luePhongKhoaTu = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaTu.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(785, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.btnIn);
            this.groupControl3.Controls.Add(this.btnChuyen);
            this.groupControl3.Controls.Add(this.dtPg1NgayVaoLam);
            this.groupControl3.Controls.Add(this.labelControl19);
            this.groupControl3.Controls.Add(this.lueQD);
            this.groupControl3.Controls.Add(this.labelControl112);
            this.groupControl3.Controls.Add(this.cbbPg1ChucVu);
            this.groupControl3.Controls.Add(this.labelControl29);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.lueBPBMDen);
            this.groupControl3.Controls.Add(this.txtHoTen);
            this.groupControl3.Controls.Add(this.luePhongKhoaDen);
            this.groupControl3.Controls.Add(this.txtMaNV);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Location = new System.Drawing.Point(8, 304);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(770, 209);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Đơn vị chuyển đến";
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(106, 172);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(75, 23);
            this.btnIn.TabIndex = 134;
            this.btnIn.Text = "In";
            // 
            // btnChuyen
            // 
            this.btnChuyen.Location = new System.Drawing.Point(12, 172);
            this.btnChuyen.Name = "btnChuyen";
            this.btnChuyen.Size = new System.Drawing.Size(75, 23);
            this.btnChuyen.TabIndex = 116;
            this.btnChuyen.Text = "Chuyển";
            // 
            // dtPg1NgayVaoLam
            // 
            this.dtPg1NgayVaoLam.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayVaoLam.Location = new System.Drawing.Point(603, 108);
            this.dtPg1NgayVaoLam.Name = "dtPg1NgayVaoLam";
            this.dtPg1NgayVaoLam.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoLam.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoLam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayVaoLam.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoLam.Size = new System.Drawing.Size(120, 22);
            this.dtPg1NgayVaoLam.TabIndex = 115;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(512, 111);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(77, 15);
            this.labelControl19.TabIndex = 114;
            this.labelControl19.Text = "Ngày hiệu lực";
            // 
            // lueQD
            // 
            this.lueQD.Location = new System.Drawing.Point(338, 108);
            this.lueQD.Name = "lueQD";
            this.lueQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueQD.Properties.Appearance.Options.UseFont = true;
            this.lueQD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQD.Properties.NullText = "";
            this.lueQD.Size = new System.Drawing.Size(141, 22);
            this.lueQD.TabIndex = 113;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(261, 111);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(60, 15);
            this.labelControl112.TabIndex = 112;
            this.labelControl112.Text = "Quyết định";
            // 
            // cbbPg1ChucVu
            // 
            this.cbbPg1ChucVu.Location = new System.Drawing.Point(120, 108);
            this.cbbPg1ChucVu.Name = "cbbPg1ChucVu";
            this.cbbPg1ChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChucVu.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mã chức vụ", "MaChucVu", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenChucVu", "Tên chức vụ")});
            this.cbbPg1ChucVu.Properties.DisplayMember = "TenChucVu";
            this.cbbPg1ChucVu.Properties.NullText = "";
            this.cbbPg1ChucVu.Properties.ValueMember = "MaChucVu";
            this.cbbPg1ChucVu.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChucVu.TabIndex = 110;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(10, 111);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(47, 15);
            this.labelControl29.TabIndex = 109;
            this.labelControl29.Text = "Chức vụ";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(383, 73);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(92, 15);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Bộ phận/Bộ môn";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(10, 73);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 15);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Phòng/Khoa";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(10, 37);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(77, 15);
            this.labelControl6.TabIndex = 105;
            this.labelControl6.Text = "Mã Nhân Viên";
            // 
            // lueBPBMDen
            // 
            this.lueBPBMDen.Location = new System.Drawing.Point(493, 70);
            this.lueBPBMDen.Name = "lueBPBMDen";
            this.lueBPBMDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBPBMDen.Properties.Appearance.Options.UseFont = true;
            this.lueBPBMDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBPBMDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueBPBMDen.Properties.DisplayMember = "TenBMBP";
            this.lueBPBMDen.Properties.NullText = "";
            this.lueBPBMDen.Properties.ValueMember = "MaBMBP";
            this.lueBPBMDen.Size = new System.Drawing.Size(230, 22);
            this.lueBPBMDen.TabIndex = 8;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(492, 34);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(231, 22);
            this.txtHoTen.TabIndex = 106;
            // 
            // luePhongKhoaDen
            // 
            this.luePhongKhoaDen.Location = new System.Drawing.Point(120, 70);
            this.luePhongKhoaDen.Name = "luePhongKhoaDen";
            this.luePhongKhoaDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoaDen.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoaDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoaDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.luePhongKhoaDen.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoaDen.Properties.NullText = "";
            this.luePhongKhoaDen.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoaDen.Size = new System.Drawing.Size(230, 22);
            this.luePhongKhoaDen.TabIndex = 6;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(120, 34);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Properties.AllowFocused = false;
            this.txtMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtMaNV.Properties.ReadOnly = true;
            this.txtMaNV.Size = new System.Drawing.Size(141, 22);
            this.txtMaNV.TabIndex = 107;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(383, 37);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 15);
            this.labelControl9.TabIndex = 108;
            this.labelControl9.Text = "Họ Tên";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.gcDSNhanVien);
            this.groupControl2.Location = new System.Drawing.Point(6, 98);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(774, 199);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Chọn Cán bộ - Viên chức chuyển công tác";
            // 
            // gcDSNhanVien
            // 
            this.gcDSNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDSNhanVien.Location = new System.Drawing.Point(2, 21);
            this.gcDSNhanVien.MainView = this.gridViewDSNhanVien;
            this.gcDSNhanVien.Name = "gcDSNhanVien";
            this.gcDSNhanVien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDEDSNgayS});
            this.gcDSNhanVien.Size = new System.Drawing.Size(770, 176);
            this.gcDSNhanVien.TabIndex = 4;
            this.gcDSNhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDSNhanVien});
            // 
            // gridViewDSNhanVien
            // 
            this.gridViewDSNhanVien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNVID,
            this.gridColMaNV,
            this.gridColHo,
            this.gridColTen,
            this.gridColNgaySinh,
            this.gridColGioiTinh,
            this.gridColCNND,
            this.gridColEmail,
            this.gridColChucvu,
            this.gridColPhongKhoa,
            this.gridColBoMon});
            this.gridViewDSNhanVien.GridControl = this.gcDSNhanVien;
            this.gridViewDSNhanVien.GroupPanelText = " ";
            this.gridViewDSNhanVien.Name = "gridViewDSNhanVien";
            // 
            // gridColNVID
            // 
            this.gridColNVID.Caption = "ID";
            this.gridColNVID.FieldName = "Id";
            this.gridColNVID.Name = "gridColNVID";
            // 
            // gridColMaNV
            // 
            this.gridColMaNV.Caption = "Mã NV";
            this.gridColMaNV.FieldName = "MaNV";
            this.gridColMaNV.Name = "gridColMaNV";
            this.gridColMaNV.OptionsColumn.AllowEdit = false;
            this.gridColMaNV.OptionsColumn.ReadOnly = true;
            this.gridColMaNV.Visible = true;
            this.gridColMaNV.VisibleIndex = 0;
            // 
            // gridColHo
            // 
            this.gridColHo.Caption = "Họ";
            this.gridColHo.FieldName = "Ho";
            this.gridColHo.Name = "gridColHo";
            this.gridColHo.OptionsColumn.AllowEdit = false;
            this.gridColHo.OptionsColumn.ReadOnly = true;
            this.gridColHo.Visible = true;
            this.gridColHo.VisibleIndex = 1;
            // 
            // gridColTen
            // 
            this.gridColTen.Caption = "Tên";
            this.gridColTen.FieldName = "Ten";
            this.gridColTen.Name = "gridColTen";
            this.gridColTen.OptionsColumn.AllowEdit = false;
            this.gridColTen.OptionsColumn.ReadOnly = true;
            this.gridColTen.Visible = true;
            this.gridColTen.VisibleIndex = 2;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày sinh";
            this.gridColNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 3;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Nữ";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.OptionsColumn.AllowEdit = false;
            this.gridColGioiTinh.OptionsColumn.ReadOnly = true;
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 5;
            // 
            // gridColCNND
            // 
            this.gridColCNND.Caption = "CMND";
            this.gridColCNND.FieldName = "CMND";
            this.gridColCNND.Name = "gridColCNND";
            this.gridColCNND.Visible = true;
            this.gridColCNND.VisibleIndex = 4;
            // 
            // gridColEmail
            // 
            this.gridColEmail.Caption = "Email";
            this.gridColEmail.FieldName = "Email";
            this.gridColEmail.Name = "gridColEmail";
            this.gridColEmail.Visible = true;
            this.gridColEmail.VisibleIndex = 6;
            // 
            // gridColChucvu
            // 
            this.gridColChucvu.Caption = "Chức vụ";
            this.gridColChucvu.FieldName = "TenChucVu";
            this.gridColChucvu.Name = "gridColChucvu";
            this.gridColChucvu.OptionsColumn.AllowEdit = false;
            this.gridColChucvu.OptionsColumn.ReadOnly = true;
            this.gridColChucvu.Visible = true;
            this.gridColChucvu.VisibleIndex = 7;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng/Khoa";
            this.gridColPhongKhoa.FieldName = "TenPhongKhoa";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.OptionsColumn.AllowEdit = false;
            this.gridColPhongKhoa.OptionsColumn.ReadOnly = true;
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 8;
            // 
            // gridColBoMon
            // 
            this.gridColBoMon.Caption = "Bộ phận/Bộ môn";
            this.gridColBoMon.FieldName = "TenBMBP";
            this.gridColBoMon.Name = "gridColBoMon";
            this.gridColBoMon.OptionsColumn.AllowEdit = false;
            this.gridColBoMon.OptionsColumn.ReadOnly = true;
            this.gridColBoMon.Visible = true;
            this.gridColBoMon.VisibleIndex = 9;
            // 
            // repositoryItemDEDSNgayS
            // 
            this.repositoryItemDEDSNgayS.AutoHeight = false;
            this.repositoryItemDEDSNgayS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDEDSNgayS.Name = "repositoryItemDEDSNgayS";
            this.repositoryItemDEDSNgayS.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(186, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(412, 33);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Chuyển công tác - Kiêm nhiệm";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.lueBPBMTu);
            this.groupControl1.Controls.Add(this.luePhongKhoaTu);
            this.groupControl1.Location = new System.Drawing.Point(6, 41);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(774, 51);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Đơn vị đang công tác";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(385, 26);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(92, 15);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Bộ phận/Bộ môn";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(12, 26);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(68, 15);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Phòng/Khoa";
            // 
            // lueBPBMTu
            // 
            this.lueBPBMTu.Location = new System.Drawing.Point(495, 23);
            this.lueBPBMTu.Name = "lueBPBMTu";
            this.lueBPBMTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBPBMTu.Properties.Appearance.Options.UseFont = true;
            this.lueBPBMTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBPBMTu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueBPBMTu.Properties.DisplayMember = "TenBMBP";
            this.lueBPBMTu.Properties.NullText = "";
            this.lueBPBMTu.Properties.ValueMember = "MaBMBP";
            this.lueBPBMTu.Size = new System.Drawing.Size(230, 22);
            this.lueBPBMTu.TabIndex = 4;
            // 
            // luePhongKhoaTu
            // 
            this.luePhongKhoaTu.Location = new System.Drawing.Point(122, 23);
            this.luePhongKhoaTu.Name = "luePhongKhoaTu";
            this.luePhongKhoaTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoaTu.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoaTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoaTu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.luePhongKhoaTu.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoaTu.Properties.NullText = "";
            this.luePhongKhoaTu.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoaTu.Size = new System.Drawing.Size(230, 22);
            this.luePhongKhoaTu.TabIndex = 2;
            // 
            // ucChuyenCongTac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucChuyenCongTac";
            this.Size = new System.Drawing.Size(785, 518);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaTu.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit luePhongKhoaTu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lueBPBMTu;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.TextEdit txtMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit lueBPBMDen;
        private DevExpress.XtraEditors.LookUpEdit luePhongKhoaDen;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1ChucVu;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LookUpEdit lueQD;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayVaoLam;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraGrid.GridControl gcDSNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDSNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaNV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCNND;
        private DevExpress.XtraGrid.Columns.GridColumn gridColEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChucvu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhongKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBoMon;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDEDSNgayS;
        private DevExpress.XtraEditors.SimpleButton btnChuyen;
        private DevExpress.XtraEditors.SimpleButton btnIn;
    }
}
