﻿namespace HerculesHRMT
{
    partial class ucNVKhenThuongKyLuat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.gvPg4KyLuat = new DevExpress.XtraGrid.GridControl();
            this.gridviewKyLuat = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckedComboBoxEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.gvPg4KhenThuong = new DevExpress.XtraGrid.GridControl();
            this.gridviewKhenThuong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.btnPg4Sua = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.rdbPg4HinhThuc = new DevExpress.XtraEditors.RadioGroup();
            this.dtPg4TGianHieuLuc = new DevExpress.XtraEditors.DateEdit();
            this.txtPg4NoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.btnPg4Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg4Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg4TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg4SoQuyetDinh = new DevExpress.XtraEditors.TextEdit();
            this.txtPg4CapQuyetDinh = new DevExpress.XtraEditors.TextEdit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg4KyLuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewKyLuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg4KhenThuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewKhenThuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg4HinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg4TGianHieuLuc.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg4TGianHieuLuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4NoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4SoQuyetDinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4CapQuyetDinh.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gvPg4KyLuat);
            this.xtraScrollableControl1.Controls.Add(this.gvPg4KhenThuong);
            this.xtraScrollableControl1.Controls.Add(this.groupControl11);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1282, 510);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // gvPg4KyLuat
            // 
            this.gvPg4KyLuat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg4KyLuat.Location = new System.Drawing.Point(0, 317);
            this.gvPg4KyLuat.MainView = this.gridviewKyLuat;
            this.gvPg4KyLuat.Name = "gvPg4KyLuat";
            this.gvPg4KyLuat.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckedComboBoxEdit2});
            this.gvPg4KyLuat.Size = new System.Drawing.Size(1282, 193);
            this.gvPg4KyLuat.TabIndex = 123;
            this.gvPg4KyLuat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewKyLuat});
            // 
            // gridviewKyLuat
            // 
            this.gridviewKyLuat.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn2,
            this.gridColumn30,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn4});
            this.gridviewKyLuat.GridControl = this.gvPg4KyLuat;
            this.gridviewKyLuat.GroupPanelText = "Danh sách kỷ luật";
            this.gridviewKyLuat.Name = "gridviewKyLuat";
            this.gridviewKyLuat.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewKyLuat_RowClick);
            this.gridviewKyLuat.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewKyLuat_FocusedRowChanged);
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Mã ID";
            this.gridColumn27.FieldName = "Id";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Số quyết định";
            this.gridColumn2.FieldName = "SoQuyetDinh";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Cấp quyết định";
            this.gridColumn30.FieldName = "CapQuyetDinh";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 2;
            this.gridColumn30.Width = 73;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "T.gian hiệu lực";
            this.gridColumn28.FieldName = "ThoiGianHieuLuc";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 1;
            this.gridColumn28.Width = 73;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Lý do & hình thức";
            this.gridColumn29.FieldName = "NoiDung";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 3;
            this.gridColumn29.Width = 73;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Loại KTKL";
            this.gridColumn4.FieldName = "IsKhenThuong";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // repositoryItemCheckedComboBoxEdit2
            // 
            this.repositoryItemCheckedComboBoxEdit2.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit2.Name = "repositoryItemCheckedComboBoxEdit2";
            this.repositoryItemCheckedComboBoxEdit2.NullText = "Xóa";
            // 
            // gvPg4KhenThuong
            // 
            this.gvPg4KhenThuong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg4KhenThuong.Location = new System.Drawing.Point(0, 145);
            this.gvPg4KhenThuong.MainView = this.gridviewKhenThuong;
            this.gvPg4KhenThuong.Name = "gvPg4KhenThuong";
            this.gvPg4KhenThuong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckedComboBoxEdit1});
            this.gvPg4KhenThuong.Size = new System.Drawing.Size(1282, 172);
            this.gvPg4KhenThuong.TabIndex = 122;
            this.gvPg4KhenThuong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewKhenThuong});
            // 
            // gridviewKhenThuong
            // 
            this.gridviewKhenThuong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn3,
            this.gridColumn18,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn1});
            this.gridviewKhenThuong.GridControl = this.gvPg4KhenThuong;
            this.gridviewKhenThuong.GroupPanelText = "Danh sách khen thưởng";
            this.gridviewKhenThuong.Name = "gridviewKhenThuong";
            this.gridviewKhenThuong.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewKhenThuong_RowClick);
            this.gridviewKhenThuong.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewKhenThuong_FocusedRowChanged);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Mã ID";
            this.gridColumn8.FieldName = "Id";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Số quyết định";
            this.gridColumn3.FieldName = "SoQuyetDinh";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Cấp quyết định";
            this.gridColumn18.FieldName = "CapQuyetDinh";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            this.gridColumn18.Width = 73;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "T.gian hiệu lực";
            this.gridColumn16.FieldName = "ThoiGianHieuLuc";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 73;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Nội dung & hình thức";
            this.gridColumn17.FieldName = "NoiDung";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            this.gridColumn17.Width = 73;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Loại KTKL";
            this.gridColumn1.FieldName = "IsKhenThuong";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            this.repositoryItemCheckedComboBoxEdit1.NullText = "Xóa";
            // 
            // groupControl11
            // 
            this.groupControl11.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl11.Appearance.Options.UseFont = true;
            this.groupControl11.Controls.Add(this.btnPg4Sua);
            this.groupControl11.Controls.Add(this.groupControl12);
            this.groupControl11.Controls.Add(this.dtPg4TGianHieuLuc);
            this.groupControl11.Controls.Add(this.txtPg4NoiDung);
            this.groupControl11.Controls.Add(this.btnPg4Xoa);
            this.groupControl11.Controls.Add(this.btnPg4Them);
            this.groupControl11.Controls.Add(this.btnPg4TaoMoi);
            this.groupControl11.Controls.Add(this.labelControl63);
            this.groupControl11.Controls.Add(this.labelControl62);
            this.groupControl11.Controls.Add(this.labelControl39);
            this.groupControl11.Controls.Add(this.labelControl64);
            this.groupControl11.Controls.Add(this.txtPg4SoQuyetDinh);
            this.groupControl11.Controls.Add(this.txtPg4CapQuyetDinh);
            this.groupControl11.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl11.Location = new System.Drawing.Point(0, 0);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(1282, 145);
            this.groupControl11.TabIndex = 121;
            this.groupControl11.Text = "Thông tin khen thưởng & kỷ luật";
            // 
            // btnPg4Sua
            // 
            this.btnPg4Sua.Location = new System.Drawing.Point(187, 112);
            this.btnPg4Sua.Name = "btnPg4Sua";
            this.btnPg4Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg4Sua.TabIndex = 7;
            this.btnPg4Sua.Text = "Hiệu chỉnh";
            this.btnPg4Sua.Click += new System.EventHandler(this.btnPg4Sua_Click);
            // 
            // groupControl12
            // 
            this.groupControl12.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl12.Appearance.Options.UseFont = true;
            this.groupControl12.Controls.Add(this.rdbPg4HinhThuc);
            this.groupControl12.Location = new System.Drawing.Point(9, 24);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(100, 73);
            this.groupControl12.TabIndex = 6;
            this.groupControl12.Text = "Hình thức";
            // 
            // rdbPg4HinhThuc
            // 
            this.rdbPg4HinhThuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbPg4HinhThuc.EditValue = 0;
            this.rdbPg4HinhThuc.Location = new System.Drawing.Point(2, 21);
            this.rdbPg4HinhThuc.Name = "rdbPg4HinhThuc";
            this.rdbPg4HinhThuc.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Khen thưởng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Kỷ luật")});
            this.rdbPg4HinhThuc.Size = new System.Drawing.Size(96, 50);
            this.rdbPg4HinhThuc.TabIndex = 4;
            // 
            // dtPg4TGianHieuLuc
            // 
            this.dtPg4TGianHieuLuc.EditValue = new System.DateTime(((long)(0)));
            this.dtPg4TGianHieuLuc.Location = new System.Drawing.Point(206, 52);
            this.dtPg4TGianHieuLuc.Name = "dtPg4TGianHieuLuc";
            this.dtPg4TGianHieuLuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg4TGianHieuLuc.Properties.Appearance.Options.UseFont = true;
            this.dtPg4TGianHieuLuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg4TGianHieuLuc.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg4TGianHieuLuc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg4TGianHieuLuc.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg4TGianHieuLuc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg4TGianHieuLuc.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg4TGianHieuLuc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg4TGianHieuLuc.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg4TGianHieuLuc.Size = new System.Drawing.Size(146, 22);
            this.dtPg4TGianHieuLuc.TabIndex = 5;
            this.dtPg4TGianHieuLuc.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // txtPg4NoiDung
            // 
            this.txtPg4NoiDung.Location = new System.Drawing.Point(508, 26);
            this.txtPg4NoiDung.Name = "txtPg4NoiDung";
            this.txtPg4NoiDung.Properties.AllowFocused = false;
            this.txtPg4NoiDung.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg4NoiDung.Properties.Appearance.Options.UseFont = true;
            this.txtPg4NoiDung.Size = new System.Drawing.Size(252, 71);
            this.txtPg4NoiDung.TabIndex = 0;
            // 
            // btnPg4Xoa
            // 
            this.btnPg4Xoa.Location = new System.Drawing.Point(276, 112);
            this.btnPg4Xoa.Name = "btnPg4Xoa";
            this.btnPg4Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg4Xoa.TabIndex = 0;
            this.btnPg4Xoa.Text = "Xóa";
            this.btnPg4Xoa.Click += new System.EventHandler(this.btnPg4Xoa_Click);
            // 
            // btnPg4Them
            // 
            this.btnPg4Them.Location = new System.Drawing.Point(98, 112);
            this.btnPg4Them.Name = "btnPg4Them";
            this.btnPg4Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg4Them.TabIndex = 0;
            this.btnPg4Them.Text = "Lưu";
            this.btnPg4Them.Click += new System.EventHandler(this.btnPg4Them_Click);
            // 
            // btnPg4TaoMoi
            // 
            this.btnPg4TaoMoi.Location = new System.Drawing.Point(9, 112);
            this.btnPg4TaoMoi.Name = "btnPg4TaoMoi";
            this.btnPg4TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg4TaoMoi.TabIndex = 0;
            this.btnPg4TaoMoi.Text = "Tạo mới";
            this.btnPg4TaoMoi.Click += new System.EventHandler(this.btnPg4TaoMoi_Click);
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Location = new System.Drawing.Point(113, 55);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(82, 15);
            this.labelControl63.TabIndex = 0;
            this.labelControl63.Text = "T.gian hiệu lực";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl62.Location = new System.Drawing.Point(360, 27);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(142, 15);
            this.labelControl62.TabIndex = 0;
            this.labelControl62.Text = "Lý do/Nội dung & hình thức";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(115, 27);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(77, 15);
            this.labelControl39.TabIndex = 0;
            this.labelControl39.Text = "Quyết định số";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Location = new System.Drawing.Point(113, 82);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(83, 15);
            this.labelControl64.TabIndex = 0;
            this.labelControl64.Text = "Cấp quyết định";
            // 
            // txtPg4SoQuyetDinh
            // 
            this.txtPg4SoQuyetDinh.Location = new System.Drawing.Point(206, 24);
            this.txtPg4SoQuyetDinh.Name = "txtPg4SoQuyetDinh";
            this.txtPg4SoQuyetDinh.Properties.AllowFocused = false;
            this.txtPg4SoQuyetDinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg4SoQuyetDinh.Properties.Appearance.Options.UseFont = true;
            this.txtPg4SoQuyetDinh.Size = new System.Drawing.Size(146, 22);
            this.txtPg4SoQuyetDinh.TabIndex = 0;
            // 
            // txtPg4CapQuyetDinh
            // 
            this.txtPg4CapQuyetDinh.Location = new System.Drawing.Point(206, 80);
            this.txtPg4CapQuyetDinh.Name = "txtPg4CapQuyetDinh";
            this.txtPg4CapQuyetDinh.Properties.AllowFocused = false;
            this.txtPg4CapQuyetDinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg4CapQuyetDinh.Properties.Appearance.Options.UseFont = true;
            this.txtPg4CapQuyetDinh.Size = new System.Drawing.Size(146, 22);
            this.txtPg4CapQuyetDinh.TabIndex = 0;
            // 
            // ucNVKhenThuongKyLuat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ucNVKhenThuongKyLuat";
            this.Size = new System.Drawing.Size(1282, 510);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg4KyLuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewKyLuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg4KhenThuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewKhenThuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg4HinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg4TGianHieuLuc.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg4TGianHieuLuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4NoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4SoQuyetDinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg4CapQuyetDinh.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraGrid.GridControl gvPg4KyLuat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewKyLuat;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit2;
        private DevExpress.XtraGrid.GridControl gvPg4KhenThuong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewKhenThuong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private DevExpress.XtraEditors.RadioGroup rdbPg4HinhThuc;
        private DevExpress.XtraEditors.DateEdit dtPg4TGianHieuLuc;
        private DevExpress.XtraEditors.MemoEdit txtPg4NoiDung;
        private DevExpress.XtraEditors.SimpleButton btnPg4Xoa;
        private DevExpress.XtraEditors.SimpleButton btnPg4Them;
        private DevExpress.XtraEditors.SimpleButton btnPg4TaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.TextEdit txtPg4SoQuyetDinh;
        private DevExpress.XtraEditors.TextEdit txtPg4CapQuyetDinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SimpleButton btnPg4Sua;
    }
}
