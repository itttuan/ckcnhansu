﻿namespace HerculesHRMT
{
    partial class ucGiaHanHopDong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiaHan = new DevExpress.XtraEditors.SimpleButton();
            this.deHieuLucDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucTu = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.cbbPg1ChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lueBPBMDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.luePhongKhoaDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridHopdong = new DevExpress.XtraGrid.GridControl();
            this.gridViewHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongLoaiHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueLoaiHD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(785, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(274, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(237, 33);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Gia hạn hợp đồng";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.btnIn);
            this.groupControl1.Controls.Add(this.btnGiaHan);
            this.groupControl1.Controls.Add(this.deHieuLucDen);
            this.groupControl1.Controls.Add(this.labelControl116);
            this.groupControl1.Controls.Add(this.deHieuLucTu);
            this.groupControl1.Controls.Add(this.labelControl115);
            this.groupControl1.Controls.Add(this.cbbPg1ChucVu);
            this.groupControl1.Controls.Add(this.labelControl29);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.lueBPBMDen);
            this.groupControl1.Controls.Add(this.txtHoTen);
            this.groupControl1.Controls.Add(this.luePhongKhoaDen);
            this.groupControl1.Controls.Add(this.txtMaNV);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.gridHopdong);
            this.groupControl1.Controls.Add(this.lueLoaiHD);
            this.groupControl1.Controls.Add(this.labelControl112);
            this.groupControl1.Location = new System.Drawing.Point(0, 54);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(785, 464);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Hợp đồng hiện tại";
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(109, 384);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(75, 23);
            this.btnIn.TabIndex = 143;
            this.btnIn.Text = "In";
            // 
            // btnGiaHan
            // 
            this.btnGiaHan.Location = new System.Drawing.Point(16, 384);
            this.btnGiaHan.Name = "btnGiaHan";
            this.btnGiaHan.Size = new System.Drawing.Size(75, 23);
            this.btnGiaHan.TabIndex = 142;
            this.btnGiaHan.Text = "Gia hạn";
            // 
            // deHieuLucDen
            // 
            this.deHieuLucDen.EditValue = null;
            this.deHieuLucDen.Location = new System.Drawing.Point(383, 327);
            this.deHieuLucDen.Name = "deHieuLucDen";
            this.deHieuLucDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucDen.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucDen.Size = new System.Drawing.Size(140, 22);
            this.deHieuLucDen.TabIndex = 141;
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl116.Location = new System.Drawing.Point(274, 330);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(56, 15);
            this.labelControl116.TabIndex = 140;
            this.labelControl116.Text = "Đến ngày:";
            // 
            // deHieuLucTu
            // 
            this.deHieuLucTu.EditValue = null;
            this.deHieuLucTu.Location = new System.Drawing.Point(110, 327);
            this.deHieuLucTu.Name = "deHieuLucTu";
            this.deHieuLucTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucTu.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucTu.Size = new System.Drawing.Size(141, 22);
            this.deHieuLucTu.TabIndex = 139;
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl115.Location = new System.Drawing.Point(23, 330);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(49, 15);
            this.labelControl115.TabIndex = 138;
            this.labelControl115.Text = "Từ ngày:";
            // 
            // cbbPg1ChucVu
            // 
            this.cbbPg1ChucVu.Location = new System.Drawing.Point(637, 299);
            this.cbbPg1ChucVu.Name = "cbbPg1ChucVu";
            this.cbbPg1ChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChucVu.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mã chức vụ", "MaChucVu", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenChucVu", "Tên chức vụ")});
            this.cbbPg1ChucVu.Properties.DisplayMember = "TenChucVu";
            this.cbbPg1ChucVu.Properties.NullText = "";
            this.cbbPg1ChucVu.Properties.ReadOnly = true;
            this.cbbPg1ChucVu.Properties.ValueMember = "MaChucVu";
            this.cbbPg1ChucVu.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChucVu.TabIndex = 137;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(541, 302);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(47, 15);
            this.labelControl29.TabIndex = 136;
            this.labelControl29.Text = "Chức vụ";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(273, 302);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(92, 15);
            this.labelControl5.TabIndex = 131;
            this.labelControl5.Text = "Bộ phận/Bộ môn";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(23, 302);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(68, 15);
            this.labelControl6.TabIndex = 129;
            this.labelControl6.Text = "Phòng/Khoa";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(23, 275);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(77, 15);
            this.labelControl7.TabIndex = 132;
            this.labelControl7.Text = "Mã Nhân Viên";
            // 
            // lueBPBMDen
            // 
            this.lueBPBMDen.Location = new System.Drawing.Point(382, 299);
            this.lueBPBMDen.Name = "lueBPBMDen";
            this.lueBPBMDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBPBMDen.Properties.Appearance.Options.UseFont = true;
            this.lueBPBMDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBPBMDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueBPBMDen.Properties.DisplayMember = "TenBMBP";
            this.lueBPBMDen.Properties.NullText = "";
            this.lueBPBMDen.Properties.ReadOnly = true;
            this.lueBPBMDen.Properties.ValueMember = "MaBMBP";
            this.lueBPBMDen.Size = new System.Drawing.Size(141, 22);
            this.lueBPBMDen.TabIndex = 130;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(382, 272);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(241, 22);
            this.txtHoTen.TabIndex = 133;
            // 
            // luePhongKhoaDen
            // 
            this.luePhongKhoaDen.Location = new System.Drawing.Point(110, 299);
            this.luePhongKhoaDen.Name = "luePhongKhoaDen";
            this.luePhongKhoaDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoaDen.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoaDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoaDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.luePhongKhoaDen.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoaDen.Properties.NullText = "";
            this.luePhongKhoaDen.Properties.ReadOnly = true;
            this.luePhongKhoaDen.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoaDen.Size = new System.Drawing.Size(141, 22);
            this.luePhongKhoaDen.TabIndex = 128;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(110, 272);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Properties.AllowFocused = false;
            this.txtMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtMaNV.Properties.ReadOnly = true;
            this.txtMaNV.Size = new System.Drawing.Size(141, 22);
            this.txtMaNV.TabIndex = 134;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(273, 275);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 15);
            this.labelControl9.TabIndex = 135;
            this.labelControl9.Text = "Họ Tên";
            // 
            // gridHopdong
            // 
            this.gridHopdong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHopdong.Location = new System.Drawing.Point(5, 65);
            this.gridHopdong.MainView = this.gridViewHopDong;
            this.gridHopdong.Name = "gridHopdong";
            this.gridHopdong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa});
            this.gridHopdong.Size = new System.Drawing.Size(775, 186);
            this.gridHopdong.TabIndex = 127;
            this.gridHopdong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHopDong});
            // 
            // gridViewHopDong
            // 
            this.gridViewHopDong.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewHopDong.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn2,
            this.gridColHopDongLoaiHD,
            this.gridColHopDongTungay,
            this.gridColHopDongDenngay});
            this.gridViewHopDong.GridControl = this.gridHopdong;
            this.gridViewHopDong.GroupPanelText = "Chọn Cán bộ - Viên chức theo hợp đồng sẽ gia hạn";
            this.gridViewHopDong.Name = "gridViewHopDong";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Mã Nhân viên";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Họ";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Tên";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Mã Hợp đồng";
            this.gridColumn2.FieldName = "MAHD";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColHopDongLoaiHD
            // 
            this.gridColHopDongLoaiHD.Caption = "Loại hợp đồng";
            this.gridColHopDongLoaiHD.FieldName = "TENLOAIHD";
            this.gridColHopDongLoaiHD.Name = "gridColHopDongLoaiHD";
            this.gridColHopDongLoaiHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongLoaiHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongLoaiHD.Visible = true;
            this.gridColHopDongLoaiHD.VisibleIndex = 4;
            // 
            // gridColHopDongTungay
            // 
            this.gridColHopDongTungay.Caption = "Từ ngày";
            this.gridColHopDongTungay.FieldName = "TUNGAY";
            this.gridColHopDongTungay.Name = "gridColHopDongTungay";
            this.gridColHopDongTungay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongTungay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongTungay.Visible = true;
            this.gridColHopDongTungay.VisibleIndex = 5;
            // 
            // gridColHopDongDenngay
            // 
            this.gridColHopDongDenngay.Caption = "Đến ngày";
            this.gridColHopDongDenngay.FieldName = "DENNGAY";
            this.gridColHopDongDenngay.Name = "gridColHopDongDenngay";
            this.gridColHopDongDenngay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongDenngay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongDenngay.Visible = true;
            this.gridColHopDongDenngay.VisibleIndex = 6;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // lueLoaiHD
            // 
            this.lueLoaiHD.Location = new System.Drawing.Point(74, 37);
            this.lueLoaiHD.Name = "lueLoaiHD";
            this.lueLoaiHD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueLoaiHD.Properties.Appearance.Options.UseFont = true;
            this.lueLoaiHD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiHD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiHD", "Mã Loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHD", "Loại Hợp Đồng")});
            this.lueLoaiHD.Properties.DisplayMember = "TenLoaiHD";
            this.lueLoaiHD.Properties.NullText = "";
            this.lueLoaiHD.Properties.ValueMember = "MaLoaiHD";
            this.lueLoaiHD.Size = new System.Drawing.Size(155, 22);
            this.lueLoaiHD.TabIndex = 69;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(10, 40);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(47, 15);
            this.labelControl112.TabIndex = 68;
            this.labelControl112.Text = "Loại HĐ:";
            // 
            // ucGiaHanHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucGiaHanHopDong";
            this.Size = new System.Drawing.Size(785, 518);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridHopdong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongLoaiHD;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongTungay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongDenngay;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiHD;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1ChucVu;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit lueBPBMDen;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.LookUpEdit luePhongKhoaDen;
        private DevExpress.XtraEditors.TextEdit txtMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit deHieuLucDen;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.DateEdit deHieuLucTu;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.SimpleButton btnIn;
        private DevExpress.XtraEditors.SimpleButton btnGiaHan;
    }
}
