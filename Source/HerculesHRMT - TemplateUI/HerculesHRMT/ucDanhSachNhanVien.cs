﻿using System;
using DevExpress.XtraEditors;
using System.Collections.Generic;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Collections;

namespace HerculesHRMT
{
    public partial class ucDanhSachNhanVien : XtraUserControl
    {
        PhongKhoaCTL phongkhoaCTL = new PhongKhoaCTL();
        BoMonBoPhanCTL bomonbophanCTL = new BoMonBoPhanCTL();
        NhanVienCTL nhanvienCTL = new NhanVienCTL();
        HinhAnhCTL hinhanhCTL = new HinhAnhCTL();
        List<byte[]> listImgNV = new List<byte[]>();
        List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
        string id = "";
        string strMaNV = "";
        Common.STATUS _statusTNX;
        public ucDanhSachNhanVien()
        {
            InitializeComponent();
        }

        private void ucDanhSachNhanVien_Load(object sender, EventArgs e)
        {
            LoadData();
            LoadDanhSachNhanVien();
        }
        private void LoadData()
        {
            List<PhongKhoaDTO> listPhongKhoa = Program.listPhongKhoa;
            listPhongKhoa.Insert(0, new PhongKhoaDTO());
            List<BoMonBoPhanDTO> listBPBM = Program.listBMBP;
            listBPBM.Insert(0, new BoMonBoPhanDTO());
            lueTKPhongKhoa.Properties.DataSource = listPhongKhoa;
            lueTKBPBM.Properties.DataSource = listBPBM;
            if (listPhongKhoa.Count > 0)
            {
                lueTKPhongKhoa.EditValue = listPhongKhoa[0].MaPhongKhoa;
            }
        }
        private void LoadDanhSachNhanVien()
        {
            
            if (lueTKPhongKhoa.EditValue == null && lueTKBPBM.EditValue == null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachTatCaNhanVien();
            }
            else if (lueTKPhongKhoa.EditValue != null && lueTKBPBM.EditValue == null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
            }
            else if (lueTKBPBM.EditValue != null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoBoMon(lueTKBPBM.EditValue.ToString().Trim());
            }
            gcDSNhanVien.DataSource = listNhanVien;
        }

        private void lueTKPhongKhoa_EditValueChanged(object sender, EventArgs e)
        {
            if (lueTKPhongKhoa.EditValue != null)
            {
                List<BoMonBoPhanDTO> listBPBM = bomonbophanCTL.GetListByMaPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
                listBPBM.Insert(0, new BoMonBoPhanDTO());
                lueTKBPBM.Properties.DataSource = listBPBM;
                if (listBPBM.Count > 0)
                {
                    lueTKBPBM.EditValue = listBPBM[0].MaBMBP;
                }
            }
            LoadDanhSachNhanVien();
        }

        private void lueTKBPBM_EditValueChanged(object sender, EventArgs e)
        {
            LoadDanhSachNhanVien();
        }

        private void btnTimKiemMaNV_Click(object sender, EventArgs e)
        {
            if (txtTKMaNV.Text.Trim() != "")
            {
                listNhanVien = nhanvienCTL.TimNhanVienTheoMaNV(txtTKMaNV.Text.Trim());
                gcDSNhanVien.DataSource = listNhanVien;
            }
        }

        private void btnTimKiemCMND_Click(object sender, EventArgs e)
        {
            if (txtTKCMND.Text.Trim() != "")
            {
                listNhanVien = nhanvienCTL.TimNhanVienTheoCMND(txtTKCMND.Text.Trim());
                gcDSNhanVien.DataSource = listNhanVien;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            FrmThemNhanVien frm = new FrmThemNhanVien();
            frm.ShowDialog();
        }

        private void gridViewDSNhanVien_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0 && e.FocusedRowHandle< gridViewDSNhanVien.RowCount)
            {
                id = ((GridView)sender).GetRowCellValue(e.FocusedRowHandle, "Id").ToString();
                strMaNV = ((GridView)sender).GetRowCellValue(e.FocusedRowHandle, "MaNV").ToString();
                NhanVienDTO obj = ((GridView)sender).GetRow(e.FocusedRowHandle) as NhanVienDTO;
                
                
                txtHoTen.Text = obj.Ho + " " + obj.Ten;
                txtMaNV.Text = obj.MaNV;
                txtTenPhongKhoa.Text = obj.TenPhongKhoa;
                txtTenBMBP.Text = obj.TenBMBP;

                _statusTNX = Common.STATUS.VIEW;
                SetStatusTNX();
                #region Load Dữ Liệu Của Nhân Viên
                LoadImageNV();
                this.ucNVThiDua1.SetMaNV(strMaNV);
                this.ucNVLuong1.SetMaNV(strMaNV, Common.STATUS.VIEW);
                this.ucNVHopDong1.SetMaNV(strMaNV);
                this.ucNVQuyetDinh1.SetMaNV(strMaNV);
                this.ucNVPhanCong1.SetMaNV(strMaNV);
                this.ucNVBaoHiem1.SetIDNhanVien(id,Common.STATUS.VIEW);
                #endregion
            }

        }

        private void LoadImageNV()
        {
            try
            {
                listImgNV = hinhanhCTL.LayTatCaHinhTheoNhanVien(strMaNV);
                if (listImgNV.Count == 0)
                {
                    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDanhSachNhanVien));
                    this.imgHinhAnh.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Properties.Appearance.Image")));

                }
                else
                {
                    MemoryStream stream = new MemoryStream(listImgNV[listImgNV.Count-1]);
                    this.imgHinhAnh.Image = Image.FromStream(stream);
                }
            }
            catch
            {
            }
        }

        private void tabNhanVien_Selected(object sender, DevExpress.XtraTab.TabPageEventArgs e)
        {
            if (e.Page == tabDSNhanVien)
            {
                LoadDanhSachNhanVien();
            }
            else if (e.Page == tabHieuchinhNV)
            {
                LoadLyLichSoLuoc();
                xtraTabControl3.SelectedTabPage = tabPageSoLuocLL;
            }
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            tabNhanVien.SelectedTabPageIndex = 1;
        }

        private void gridViewDSNhanVien_DoubleClick(object sender, EventArgs e)
        {
            tabNhanVien.SelectedTabPageIndex = 1;
        }

        private void LoadLyLichSoLuoc()
        {
            NhanVienDTO obj = nhanvienCTL.TimNhanVienTheoId(id);

            txtPg6TuNhanXet.EditValue = obj.TuNhanXet;
            this.ucNVSoLuocLyLich1.SetNhanVien(obj, Common.STATUS.VIEW);
            ucNVSoLuocLyLich1.lylichSender = getNVIDSender;
        }
        private void xtraTabControl3_Selected(object sender, DevExpress.XtraTab.TabPageEventArgs e)
        {

            if (e.Page == tabPageQuaTrinhCT)
            {
                this.ucNVQuaTrinhCongTac1.SetMaNV(strMaNV, Common.STATUS.VIEW);
            }
            else if (e.Page == tabPageTrinhDo)
            {
                //this.ucNVBangCapChungChi1.SetMaNV(strMaNV,Common.STATUS.VIEW);
                this.ucNVTrinhDo1.SetMaNV(strMaNV, Common.STATUS.VIEW);
            }
            else if (e.Page == tabPageKhenThuongKyLuat)
            {
                this.ucNVKhenThuongKyLuat1.setMaNV(strMaNV, Common.STATUS.VIEW);
            }
            else if (e.Page == tabPageQuanHeGiaDinh)
            {
                this.ucNVQuanHeGiaDinh1.SetMaNV(strMaNV, Common.STATUS.VIEW);
            }
            else if (e.Page == tabPageTuNhanXet)
            {
                NhanVienDTO nv = nhanvienCTL.TimNhanVienTheoId(id);
                txtPg6TuNhanXet.EditValue = nv.TuNhanXet;
                _statusTNX = Common.STATUS.VIEW;
            }
            else if (e.Page == tabPageBaoHiem)
            {
                this.ucNVBaoHiem1.SetIDNhanVien(id, Common.STATUS.VIEW);
            }
            else if (e.Page == tabPageLuong)
            {
                this.ucNVLuong1.SetMaNV(strMaNV, Common.STATUS.VIEW);
            }
        }
        private void getNVIDSender(string id)
        {
            this.id = id;
        }
        private void xtraTabControl3_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (true)
            {

            }
        }

        private void SetStatusTNX()
        {
            switch (_statusTNX)
            {
                case Common.STATUS.VIEW:
                    btnPg6Them.Enabled = false;
                    txtPg6TuNhanXet.Properties.ReadOnly = true;
                    break;
                case Common.STATUS.EDIT:
                    btnPg6Them.Enabled = true;
                    txtPg6TuNhanXet.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void btnPg6Sua_Click(object sender, EventArgs e)
        {
            _statusTNX = Common.STATUS.EDIT;
            SetStatusTNX();
        }

        private void btnPg6Them_Click(object sender, EventArgs e)
        {
            string strNhanXet = txtPg6TuNhanXet.EditValue as String;
            if (id != null && id != "")
            {
                if (nhanvienCTL.CapNhatTuNhanXet(strNhanXet, id))
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusTNX = Common.STATUS.VIEW;
                    SetStatusTNX();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void imgHinhAnh_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                Filter = Constants.ImageFilter
            };

            if (open.ShowDialog() != DialogResult.OK) return;

            var img = (Bitmap)Image.FromFile(open.FileName);
            {
                var stream = new MemoryStream();

                ((Image)img).Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

                var imageBytes = stream.ToArray();

                HinhAnhDTO haDTO = new HinhAnhDTO();
                haDTO.MaNV = strMaNV;
                haDTO.HinhAnh = imageBytes;
                if (hinhanhCTL.InsertImage(haDTO))
                {
                    imgHinhAnh.Image = img;
                    listImgNV = hinhanhCTL.LayTatCaHinhTheoNhanVien(strMaNV);
                }
                else
                {
                    XtraMessageBox.Show("Lưu ảnh không thành công.", "Thông báo.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void btnViewImg_Click(object sender, EventArgs e)
        {
            frmHinhAnh frmHA = new frmHinhAnh(strMaNV,listImgNV);
            frmHA.ShowDialog();
        }


        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            if (id != "" && strMaNV != "")
            {
                //NhanVienDTO nv = nhanvienCTL.TimNhanVienTheoMaNV(strMaNV)[0];
                NhanVienDTO nv = nhanvienCTL.TimNhanVienTheoId(id);
                ThongTinNhanVienDTO ttnv = new ThongTinNhanVienDTO();
                ttnv.lylichsoluoc = nv;
                List<QuaTrinhCongTacDTO> qtcc = (new QuaTrinhCongTacCTL()).LayDanhSachQuaTrinhCongTacNhanVien(strMaNV);

                ttnv.truockhituyendung = qtcc.FindAll(delegate(QuaTrinhCongTacDTO q) { return q.GiaiDoan == 0; });
                ttnv.khituyendung = qtcc.FindAll(delegate(QuaTrinhCongTacDTO q) { return q.GiaiDoan == 1; });
                ttnv.congtacxahoi = qtcc.FindAll(delegate(QuaTrinhCongTacDTO q) { return q.GiaiDoan == 2; });

                TrinhDoCTL trinhdo = new TrinhDoCTL();
                ttnv.trinhdochuyenmon = trinhdo.LayDanhSachTrinhDo(strMaNV, 0);
                ttnv.trinhdochuyenmon.AddRange(trinhdo.LayDanhSachTrinhDo(strMaNV, 2));
                ttnv.trinhdochuyenmon.AddRange(trinhdo.LayDanhSachTrinhDo(strMaNV, 3));
                ttnv.trinhdochuyenmon.AddRange(trinhdo.LayDanhSachTrinhDo(strMaNV, 4));
                ttnv.trinhdochuyenmon.AddRange(trinhdo.LayDanhSachTrinhDo(strMaNV, 5));
                ttnv.khenthuong = (new KhenThuongKyLuatCTL()).LayDanhSachKhenThuongNhanVien(strMaNV);
                ttnv.kyluat = (new KhenThuongKyLuatCTL()).LayDanhSachKyLuatNhanVien(strMaNV);

                QuanHeGiaDinhCTL qhgd = new QuanHeGiaDinhCTL();
                ttnv.quanhegiadinh = qhgd.LayDanhSachThanNhanCuaNhanVien(strMaNV);

                rptLyLich ll = new rptLyLich(ttnv);
                ll.ShowPreview();
            }
        }

    }
}
