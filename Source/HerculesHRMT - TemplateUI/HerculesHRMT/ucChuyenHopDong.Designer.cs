﻿namespace HerculesHRMT
{
    partial class ucChuyenHopDong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnChuyen = new DevExpress.XtraEditors.SimpleButton();
            this.deHieuLucDen = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucTu = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridHopdong = new DevExpress.XtraGrid.GridControl();
            this.gridViewHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongLoaiHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueLoaiHD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColHopDongMaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNguoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHopDongNgayky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHDXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(848, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAdd.Location = new System.Drawing.Point(417, 340);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(16, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = ">";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupControl2.Controls.Add(this.btnIn);
            this.groupControl2.Controls.Add(this.btnChuyen);
            this.groupControl2.Controls.Add(this.deHieuLucDen);
            this.groupControl2.Controls.Add(this.lookUpEdit1);
            this.groupControl2.Controls.Add(this.labelControl116);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.deHieuLucTu);
            this.groupControl2.Controls.Add(this.labelControl115);
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(434, 54);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(409, 459);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Hợp đồng chuẩn bị";
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(110, 122);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(75, 23);
            this.btnIn.TabIndex = 133;
            this.btnIn.Text = "In";
            // 
            // btnChuyen
            // 
            this.btnChuyen.Location = new System.Drawing.Point(17, 122);
            this.btnChuyen.Name = "btnChuyen";
            this.btnChuyen.Size = new System.Drawing.Size(75, 23);
            this.btnChuyen.TabIndex = 132;
            this.btnChuyen.Text = "Chuyển";
            // 
            // deHieuLucDen
            // 
            this.deHieuLucDen.EditValue = null;
            this.deHieuLucDen.Location = new System.Drawing.Point(87, 89);
            this.deHieuLucDen.Name = "deHieuLucDen";
            this.deHieuLucDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucDen.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucDen.Size = new System.Drawing.Size(155, 22);
            this.deHieuLucDen.TabIndex = 131;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(87, 33);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiHD", "Mã Loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHD", "Loại Hợp Đồng")});
            this.lookUpEdit1.Properties.DisplayMember = "TenLoaiHD";
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Properties.ValueMember = "MaLoaiHD";
            this.lookUpEdit1.Size = new System.Drawing.Size(155, 22);
            this.lookUpEdit1.TabIndex = 129;
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl116.Location = new System.Drawing.Point(15, 92);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(56, 15);
            this.labelControl116.TabIndex = 130;
            this.labelControl116.Text = "Đến ngày:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(17, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 15);
            this.labelControl2.TabIndex = 128;
            this.labelControl2.Text = "Loại HĐ:";
            // 
            // deHieuLucTu
            // 
            this.deHieuLucTu.EditValue = null;
            this.deHieuLucTu.Location = new System.Drawing.Point(87, 61);
            this.deHieuLucTu.Name = "deHieuLucTu";
            this.deHieuLucTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucTu.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucTu.Size = new System.Drawing.Size(155, 22);
            this.deHieuLucTu.TabIndex = 129;
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl115.Location = new System.Drawing.Point(15, 64);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(49, 15);
            this.labelControl115.TabIndex = 128;
            this.labelControl115.Text = "Từ ngày:";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(5, 151);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(399, 303);
            this.gridControl1.TabIndex = 128;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = "Danh sách Cán bộ - Viên Chức chuyển giai đoạn";
            this.gridView1.Name = "gridView1";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Mã ID";
            this.gridColumn5.FieldName = "ID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Mã Nhân viên";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Họ";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Tên";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Mã Hợp đồng";
            this.gridColumn10.FieldName = "MAHD";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Số Quyết định";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Xóa";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.NullText = "Xóa";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupControl1.Controls.Add(this.gridHopdong);
            this.groupControl1.Controls.Add(this.lueLoaiHD);
            this.groupControl1.Controls.Add(this.labelControl112);
            this.groupControl1.Location = new System.Drawing.Point(6, 54);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(409, 459);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Hợp đồng hiện tại";
            // 
            // gridHopdong
            // 
            this.gridHopdong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHopdong.Location = new System.Drawing.Point(5, 151);
            this.gridHopdong.MainView = this.gridViewHopDong;
            this.gridHopdong.Name = "gridHopdong";
            this.gridHopdong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa});
            this.gridHopdong.Size = new System.Drawing.Size(399, 303);
            this.gridHopdong.TabIndex = 127;
            this.gridHopdong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHopDong});
            // 
            // gridViewHopDong
            // 
            this.gridViewHopDong.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewHopDong.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn2,
            this.gridColHopDongLoaiHD,
            this.gridColHopDongTungay,
            this.gridColHopDongDenngay});
            this.gridViewHopDong.GridControl = this.gridHopdong;
            this.gridViewHopDong.GroupPanelText = "Danh sách Cán bộ - Viên chức theo hợp đồng hiện tại";
            this.gridViewHopDong.Name = "gridViewHopDong";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Mã Nhân viên";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Họ";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Tên";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Mã Hợp đồng";
            this.gridColumn2.FieldName = "MAHD";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColHopDongLoaiHD
            // 
            this.gridColHopDongLoaiHD.Caption = "Loại hợp đồng";
            this.gridColHopDongLoaiHD.FieldName = "TENLOAIHD";
            this.gridColHopDongLoaiHD.Name = "gridColHopDongLoaiHD";
            this.gridColHopDongLoaiHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongLoaiHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongLoaiHD.Visible = true;
            this.gridColHopDongLoaiHD.VisibleIndex = 4;
            // 
            // gridColHopDongTungay
            // 
            this.gridColHopDongTungay.Caption = "Từ ngày";
            this.gridColHopDongTungay.FieldName = "TUNGAY";
            this.gridColHopDongTungay.Name = "gridColHopDongTungay";
            this.gridColHopDongTungay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongTungay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongTungay.Visible = true;
            this.gridColHopDongTungay.VisibleIndex = 5;
            // 
            // gridColHopDongDenngay
            // 
            this.gridColHopDongDenngay.Caption = "Đến ngày";
            this.gridColHopDongDenngay.FieldName = "DENNGAY";
            this.gridColHopDongDenngay.Name = "gridColHopDongDenngay";
            this.gridColHopDongDenngay.OptionsColumn.AllowEdit = false;
            this.gridColHopDongDenngay.OptionsColumn.ReadOnly = true;
            this.gridColHopDongDenngay.Visible = true;
            this.gridColHopDongDenngay.VisibleIndex = 6;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // lueLoaiHD
            // 
            this.lueLoaiHD.Location = new System.Drawing.Point(103, 33);
            this.lueLoaiHD.Name = "lueLoaiHD";
            this.lueLoaiHD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueLoaiHD.Properties.Appearance.Options.UseFont = true;
            this.lueLoaiHD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiHD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiHD", "Mã Loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHD", "Loại Hợp Đồng")});
            this.lueLoaiHD.Properties.DisplayMember = "TenLoaiHD";
            this.lueLoaiHD.Properties.NullText = "";
            this.lueLoaiHD.Properties.ValueMember = "MaLoaiHD";
            this.lueLoaiHD.Size = new System.Drawing.Size(155, 22);
            this.lueLoaiHD.TabIndex = 69;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(33, 36);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(47, 15);
            this.labelControl112.TabIndex = 68;
            this.labelControl112.Text = "Loại HĐ:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(240, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(368, 33);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Chuyển giai đoạn hợp đồng";
            // 
            // gridColHopDongMaHD
            // 
            this.gridColHopDongMaHD.Caption = "Mã Hợp đồng";
            this.gridColHopDongMaHD.FieldName = "MAHD";
            this.gridColHopDongMaHD.Name = "gridColHopDongMaHD";
            this.gridColHopDongMaHD.OptionsColumn.AllowEdit = false;
            this.gridColHopDongMaHD.OptionsColumn.ReadOnly = true;
            this.gridColHopDongMaHD.Visible = true;
            this.gridColHopDongMaHD.VisibleIndex = 0;
            // 
            // gridColHopDongNoidung
            // 
            this.gridColHopDongNoidung.Caption = "Nội dung";
            this.gridColHopDongNoidung.FieldName = "NOIDUNG";
            this.gridColHopDongNoidung.Name = "gridColHopDongNoidung";
            this.gridColHopDongNoidung.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNoidung.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNoidung.Visible = true;
            this.gridColHopDongNoidung.VisibleIndex = 5;
            // 
            // gridColHopDongNguoiky
            // 
            this.gridColHopDongNguoiky.Caption = "Người ký";
            this.gridColHopDongNguoiky.FieldName = "NGUOIKY";
            this.gridColHopDongNguoiky.Name = "gridColHopDongNguoiky";
            this.gridColHopDongNguoiky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNguoiky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNguoiky.Visible = true;
            this.gridColHopDongNguoiky.VisibleIndex = 3;
            // 
            // gridColHopDongNgayky
            // 
            this.gridColHopDongNgayky.Caption = "Ngày ký";
            this.gridColHopDongNgayky.FieldName = "NGAYKY";
            this.gridColHopDongNgayky.Name = "gridColHopDongNgayky";
            this.gridColHopDongNgayky.OptionsColumn.AllowEdit = false;
            this.gridColHopDongNgayky.OptionsColumn.ReadOnly = true;
            this.gridColHopDongNgayky.Visible = true;
            this.gridColHopDongNgayky.VisibleIndex = 3;
            // 
            // gridColHDXoa
            // 
            this.gridColHDXoa.Caption = "Xóa";
            this.gridColHDXoa.Name = "gridColHDXoa";
            this.gridColHDXoa.Visible = true;
            this.gridColHDXoa.VisibleIndex = 7;
            // 
            // ucChuyenHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucChuyenHopDong";
            this.Size = new System.Drawing.Size(848, 518);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHopdong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiHD.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LookUpEdit lueLoaiHD;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongMaHD;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongNoidung;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongNguoiky;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongNgayky;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHDXoa;
        private DevExpress.XtraGrid.GridControl gridHopdong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHopDong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongLoaiHD;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongTungay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHopDongDenngay;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.DateEdit deHieuLucDen;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.DateEdit deHieuLucTu;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.SimpleButton btnChuyen;
        private DevExpress.XtraEditors.SimpleButton btnIn;
    }
}
