﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System;
using System.Linq;

namespace HerculesHRMT
{
    public partial class ucNganh : XtraUserControl
    {
        NganhCTL ngCTL = new NganhCTL();
        List<NganhDTO> listNganh = new List<NganhDTO>();
        NganhDTO ngSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucNganh()
        {
            InitializeComponent();
            txtTenVT.Properties.MaxLength = 10;
        }

        public void LoadData()
        {
            listNganh = ngCTL.GetAll();

            gcNganh.DataSource = listNganh;
            btnLuu.Enabled = false;
            
        }

        private void BindingData(NganhDTO ngDTO)
        {
            if (ngDTO != null)
            {
                txtNganh.Text = ngDTO.TenNganh;
                txtTenVT.Text = ngDTO.TenVietTat;
                meGhiChu.Text = ngDTO.GhiChu;
            }
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtNganh.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtNganh.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtNganh.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(ngSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtNganh.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(ngSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtNganh.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private NganhDTO GetData(NganhDTO nganhDTO)
        {
            string maNganh = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexNganh = ngCTL.GetMaxMaNganh();
                if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexNganh);
                    maNganh += (nLastIndexTG + 1).ToString();
                }
                else
                    maNganh += "1";

                nganhDTO.NgayHieuLuc = DateTime.Now;
                nganhDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maNganh = ngSelect.MaNganh;
                nganhDTO.NgayHieuLuc = ngSelect.NgayHieuLuc;
                nganhDTO.TinhTrang = ngSelect.TinhTrang;
            }
            nganhDTO.MaNganh = maNganh;
            nganhDTO.TenNganh = txtNganh.Text;
            nganhDTO.TenVietTat = txtTenVT.Text;
            nganhDTO.GhiChu = meGhiChu.Text;

            return nganhDTO;
        }

        private void ucNganh_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void gridViewNganh_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listNganh.Count)
            {
                ngSelect = ((GridView)sender).GetRow(_selectedIndex) as NganhDTO;
            }
            else
                if (listNganh.Count != 0)
                    ngSelect = listNganh[0];
                else
                    ngSelect = null;

            SetStatus(VIEW);
        }
               

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên ngành\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            NganhDTO ngNew = GetData(new NganhDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = ngCTL.Save(ngNew);
            else
                isSucess = ngCTL.Update(ngNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listNganh.IndexOf(listNganh.FirstOrDefault(p => p.MaNganh == ngNew.MaNganh));
                gridViewNganh.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (ngSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa ngành " + ngSelect.TenNganh + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = ngCTL.Delete(ngSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listNganh.Count != 0)
                            gridViewNganh.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        
    }
}
