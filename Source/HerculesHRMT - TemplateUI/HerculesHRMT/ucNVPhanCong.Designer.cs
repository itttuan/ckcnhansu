﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVPhanCong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPhanCong = new DevExpress.XtraEditors.GroupControl();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxTrangThai = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.deTuNgay = new DevExpress.XtraEditors.DateEdit();
            this.lueChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.lueBPBM = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.luePhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.gridPhancong = new DevExpress.XtraGrid.GridControl();
            this.gridViewPhanCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPhancongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongDonvi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongBomon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongCongviec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhancongDamnhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditSTT = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColPhanCongXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPhanCong)).BeginInit();
            this.gcPhanCong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxTrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTuNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPhancong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhanCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPhanCong
            // 
            this.gcPhanCong.Controls.Add(this.btnPg3Xoa);
            this.gcPhanCong.Controls.Add(this.btnPg3Them);
            this.gcPhanCong.Controls.Add(this.btnPg3TaoMoi);
            this.gcPhanCong.Controls.Add(this.checkBoxTrangThai);
            this.gcPhanCong.Controls.Add(this.labelControl128);
            this.gcPhanCong.Controls.Add(this.deTuNgay);
            this.gcPhanCong.Controls.Add(this.lueChucVu);
            this.gcPhanCong.Controls.Add(this.labelControl127);
            this.gcPhanCong.Controls.Add(this.lueBPBM);
            this.gcPhanCong.Controls.Add(this.labelControl125);
            this.gcPhanCong.Controls.Add(this.labelControl126);
            this.gcPhanCong.Controls.Add(this.luePhongKhoa);
            this.gcPhanCong.Location = new System.Drawing.Point(12, 4);
            this.gcPhanCong.Name = "gcPhanCong";
            this.gcPhanCong.Size = new System.Drawing.Size(1243, 160);
            this.gcPhanCong.TabIndex = 0;
            this.gcPhanCong.Text = "Phân công nhiệm vụ";
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(258, 119);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 125;
            this.btnPg3Xoa.Text = "Xóa";
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(149, 119);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 126;
            this.btnPg3Them.Text = "Thêm";
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(40, 119);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 127;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            // 
            // checkBoxTrangThai
            // 
            this.checkBoxTrangThai.Location = new System.Drawing.Point(657, 77);
            this.checkBoxTrangThai.Name = "checkBoxTrangThai";
            this.checkBoxTrangThai.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTrangThai.Properties.Appearance.Options.UseFont = true;
            this.checkBoxTrangThai.Properties.Caption = "Đang đảm nhận";
            this.checkBoxTrangThai.Size = new System.Drawing.Size(112, 20);
            this.checkBoxTrangThai.TabIndex = 103;
            // 
            // labelControl128
            // 
            this.labelControl128.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl128.Location = new System.Drawing.Point(409, 81);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(49, 15);
            this.labelControl128.TabIndex = 102;
            this.labelControl128.Text = "Từ ngày:";
            // 
            // deTuNgay
            // 
            this.deTuNgay.EditValue = null;
            this.deTuNgay.Location = new System.Drawing.Point(512, 77);
            this.deTuNgay.Name = "deTuNgay";
            this.deTuNgay.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deTuNgay.Properties.Appearance.Options.UseFont = true;
            this.deTuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTuNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deTuNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTuNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deTuNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deTuNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deTuNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deTuNgay.Size = new System.Drawing.Size(113, 22);
            this.deTuNgay.TabIndex = 101;
            // 
            // lueChucVu
            // 
            this.lueChucVu.Location = new System.Drawing.Point(119, 78);
            this.lueChucVu.Name = "lueChucVu";
            this.lueChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueChucVu.Properties.Appearance.Options.UseFont = true;
            this.lueChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueChucVu.Properties.NullText = "";
            this.lueChucVu.Size = new System.Drawing.Size(248, 22);
            this.lueChucVu.TabIndex = 100;
            // 
            // labelControl127
            // 
            this.labelControl127.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl127.Location = new System.Drawing.Point(38, 81);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(58, 15);
            this.labelControl127.TabIndex = 99;
            this.labelControl127.Text = "Công việc:";
            // 
            // lueBPBM
            // 
            this.lueBPBM.Location = new System.Drawing.Point(512, 38);
            this.lueBPBM.Name = "lueBPBM";
            this.lueBPBM.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBPBM.Properties.Appearance.Options.UseFont = true;
            this.lueBPBM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBPBM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã BM", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueBPBM.Properties.DisplayMember = "TenBMBP";
            this.lueBPBM.Properties.NullText = "";
            this.lueBPBM.Properties.ValueMember = "MaBMBP";
            this.lueBPBM.Size = new System.Drawing.Size(261, 22);
            this.lueBPBM.TabIndex = 97;
            // 
            // labelControl125
            // 
            this.labelControl125.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl125.Location = new System.Drawing.Point(409, 41);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(95, 15);
            this.labelControl125.TabIndex = 98;
            this.labelControl125.Text = "Bộ phận/Bộ môn:";
            // 
            // labelControl126
            // 
            this.labelControl126.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl126.Location = new System.Drawing.Point(38, 41);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(71, 15);
            this.labelControl126.TabIndex = 96;
            this.labelControl126.Text = "Phòng/Khoa:";
            // 
            // luePhongKhoa
            // 
            this.luePhongKhoa.Location = new System.Drawing.Point(119, 38);
            this.luePhongKhoa.Name = "luePhongKhoa";
            this.luePhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.luePhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoa.Properties.NullText = "";
            this.luePhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoa.Size = new System.Drawing.Size(248, 22);
            this.luePhongKhoa.TabIndex = 95;
            this.luePhongKhoa.EditValueChanged += new System.EventHandler(this.luePhongKhoa_EditValueChanged);
            // 
            // gridPhancong
            // 
            this.gridPhancong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPhancong.Location = new System.Drawing.Point(12, 166);
            this.gridPhancong.MainView = this.gridViewPhanCong;
            this.gridPhancong.Name = "gridPhancong";
            this.gridPhancong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditSTT,
            this.repositoryItemHyperLinkEditXoa});
            this.gridPhancong.Size = new System.Drawing.Size(1243, 341);
            this.gridPhancong.TabIndex = 128;
            this.gridPhancong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPhanCong});
            // 
            // gridViewPhanCong
            // 
            this.gridViewPhanCong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColPhancongID,
            this.gridColPhancongDonvi,
            this.gridColPhancongBomon,
            this.gridColPhancongCongviec,
            this.gridColPhancongTungay,
            this.gridColPhancongDamnhan,
            this.gridColPhanCongXoa});
            this.gridViewPhanCong.GridControl = this.gridPhancong;
            this.gridViewPhanCong.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewPhanCong.Name = "gridViewPhanCong";
            // 
            // gridColPhancongID
            // 
            this.gridColPhancongID.Caption = "Mã ID";
            this.gridColPhancongID.FieldName = "ID";
            this.gridColPhancongID.Name = "gridColPhancongID";
            this.gridColPhancongID.OptionsColumn.AllowEdit = false;
            this.gridColPhancongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColPhancongDonvi
            // 
            this.gridColPhancongDonvi.Caption = "Phòng/Khoa";
            this.gridColPhancongDonvi.FieldName = "TENPHONGKHOA";
            this.gridColPhancongDonvi.Name = "gridColPhancongDonvi";
            this.gridColPhancongDonvi.OptionsColumn.AllowEdit = false;
            this.gridColPhancongDonvi.OptionsColumn.ReadOnly = true;
            this.gridColPhancongDonvi.Visible = true;
            this.gridColPhancongDonvi.VisibleIndex = 0;
            // 
            // gridColPhancongBomon
            // 
            this.gridColPhancongBomon.Caption = "Bộ phận/Bộ môn";
            this.gridColPhancongBomon.FieldName = "TENBPBM";
            this.gridColPhancongBomon.Name = "gridColPhancongBomon";
            this.gridColPhancongBomon.OptionsColumn.AllowEdit = false;
            this.gridColPhancongBomon.OptionsColumn.ReadOnly = true;
            this.gridColPhancongBomon.Visible = true;
            this.gridColPhancongBomon.VisibleIndex = 1;
            // 
            // gridColPhancongCongviec
            // 
            this.gridColPhancongCongviec.Caption = "Công việc";
            this.gridColPhancongCongviec.FieldName = "TENCHUCVU";
            this.gridColPhancongCongviec.Name = "gridColPhancongCongviec";
            this.gridColPhancongCongviec.OptionsColumn.AllowEdit = false;
            this.gridColPhancongCongviec.OptionsColumn.ReadOnly = true;
            this.gridColPhancongCongviec.Visible = true;
            this.gridColPhancongCongviec.VisibleIndex = 2;
            // 
            // gridColPhancongTungay
            // 
            this.gridColPhancongTungay.Caption = "Từ ngày";
            this.gridColPhancongTungay.FieldName = "TUNGAY";
            this.gridColPhancongTungay.Name = "gridColPhancongTungay";
            this.gridColPhancongTungay.OptionsColumn.AllowEdit = false;
            this.gridColPhancongTungay.OptionsColumn.ReadOnly = true;
            this.gridColPhancongTungay.Visible = true;
            this.gridColPhancongTungay.VisibleIndex = 3;
            // 
            // gridColPhancongDamnhan
            // 
            this.gridColPhancongDamnhan.Caption = "Đang đảm nhận";
            this.gridColPhancongDamnhan.ColumnEdit = this.repositoryItemCheckEditSTT;
            this.gridColPhancongDamnhan.FieldName = "TRANGTHAI";
            this.gridColPhancongDamnhan.Name = "gridColPhancongDamnhan";
            this.gridColPhancongDamnhan.OptionsColumn.AllowEdit = false;
            this.gridColPhancongDamnhan.OptionsColumn.ReadOnly = true;
            this.gridColPhancongDamnhan.Visible = true;
            this.gridColPhancongDamnhan.VisibleIndex = 4;
            // 
            // repositoryItemCheckEditSTT
            // 
            this.repositoryItemCheckEditSTT.AutoHeight = false;
            this.repositoryItemCheckEditSTT.Name = "repositoryItemCheckEditSTT";
            // 
            // gridColPhanCongXoa
            // 
            this.gridColPhanCongXoa.Caption = "Xóa";
            this.gridColPhanCongXoa.ColumnEdit = this.repositoryItemHyperLinkEditXoa;
            this.gridColPhanCongXoa.Name = "gridColPhanCongXoa";
            this.gridColPhanCongXoa.Visible = true;
            this.gridColPhanCongXoa.VisibleIndex = 5;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // ucNVPhanCong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridPhancong);
            this.Controls.Add(this.gcPhanCong);
            this.Name = "ucNVPhanCong";
            this.Size = new System.Drawing.Size(1270, 510);
            this.Load += new System.EventHandler(this.ucNVPhanCong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcPhanCong)).EndInit();
            this.gcPhanCong.ResumeLayout(false);
            this.gcPhanCong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxTrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTuNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPhancong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPhanCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcPhanCong;
        private CheckEdit checkBoxTrangThai;
        private LabelControl labelControl128;
        private DateEdit deTuNgay;
        private LookUpEdit lueChucVu;
        private LabelControl labelControl127;
        private LookUpEdit lueBPBM;
        private LabelControl labelControl125;
        private LabelControl labelControl126;
        private LookUpEdit luePhongKhoa;
        private GridControl gridPhancong;
        private GridView gridViewPhanCong;
        private GridColumn gridColPhancongID;
        private GridColumn gridColPhancongDonvi;
        private GridColumn gridColPhancongBomon;
        private GridColumn gridColPhancongCongviec;
        private GridColumn gridColPhancongTungay;
        private GridColumn gridColPhancongDamnhan;
        private RepositoryItemCheckEdit repositoryItemCheckEditSTT;
        private GridColumn gridColPhanCongXoa;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
    }
}
