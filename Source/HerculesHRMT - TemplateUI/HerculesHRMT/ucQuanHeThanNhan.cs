﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucQuanHeThanNhan : XtraUserControl
    {
        QuanHeThanNhanCTL qhCTL = new QuanHeThanNhanCTL();
        List<QuanHeThanNhanDTO> listQH = new List<QuanHeThanNhanDTO>();
        QuanHeThanNhanDTO qhSelect = null;
        
        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucQuanHeThanNhan()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            listQH = qhCTL.GetAll();

            gcQuanHeThanNhan.DataSource = listQH;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(QuanHeThanNhanDTO qhDTO)
        {
            if (qhDTO != null)
            {
                txtQuanHe.Text = qhDTO.TenQuanHe;
                meGhiChu.Text = qhDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtQuanHe.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtQuanHe.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtQuanHe.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(qhSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtQuanHe.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(qhSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtQuanHe.Text == string.Empty)
                return false;

            return true;
        }

        private QuanHeThanNhanDTO GetData(QuanHeThanNhanDTO qhDTO)
        {
            string maQH = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexQH = qhCTL.GetMaxMaLoaiQuanHe();
                if (sLastIndexQH.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexQH = int.Parse(sLastIndexQH);
                    maQH += (nLastIndexQH + 1).ToString().PadLeft(2, '0');
                }
                else
                    maQH += "01";

                qhDTO.NgayHieuLuc = DateTime.Now;
                qhDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maQH = qhSelect.MaQuanHe;
                qhDTO.NgayHieuLuc = qhSelect.NgayHieuLuc;
                qhDTO.TinhTrang = qhSelect.TinhTrang;
            }
            qhDTO.MaQuanHe = maQH;
            qhDTO.TenQuanHe = txtQuanHe.Text;
            qhDTO.GhiChu = meGhiChu.Text;

            return qhDTO;
        }

        private void gridViewQuanHeThanNhan_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listQH.Count)
            {
                qhSelect = ((GridView)sender).GetRow(_selectedIndex) as QuanHeThanNhanDTO;
            }
            else
                if (listQH.Count != 0)
                    qhSelect = listQH[0];
                else
                    qhSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên quan hệ", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            QuanHeThanNhanDTO qhNew = GetData(new QuanHeThanNhanDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = qhCTL.Save(qhNew);
            else
                isSucess = qhCTL.Update(qhNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listQH.IndexOf(listQH.FirstOrDefault(p => p.MaQuanHe == qhNew.MaQuanHe));
                gridViewQuanHeThanNhan.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (qhSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa quan hệ thân nhân " + qhSelect.TenQuanHe + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = qhCTL.Delete(qhSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listQH.Count != 0)
                            gridViewQuanHeThanNhan.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void ucQuanHeThanNhan_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }


    }
}
