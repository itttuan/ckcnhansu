﻿namespace HerculesHRMT
{
    partial class ucNVThiDua
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.gridThang = new DevExpress.XtraGrid.GridControl();
            this.gridViewThiduathang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDThangID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThangNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDThangXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton63 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton65 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton66 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl145 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.cbbThang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.gridTDHK = new DevExpress.XtraGrid.GridControl();
            this.gridViewTDHocKy = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDHKID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKHocKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDHKXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton68 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton70 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton71 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit6 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl146 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit7 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl147 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit8 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl148 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.gridTDNam = new DevExpress.XtraGrid.GridControl();
            this.gridViewTDNam = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTDNamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDNamNamhoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTDNamXeploai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton73 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton75 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton76 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit9 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl149 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit10 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl150 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThiduathang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDHK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDHocKy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDNam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDNam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.groupControl7);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1280, 510);
            this.splitContainerControl2.SplitterPosition = 423;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // groupControl7
            // 
            this.groupControl7.Controls.Add(this.gridThang);
            this.groupControl7.Controls.Add(this.simpleButton63);
            this.groupControl7.Controls.Add(this.simpleButton65);
            this.groupControl7.Controls.Add(this.simpleButton66);
            this.groupControl7.Controls.Add(this.comboBoxEdit5);
            this.groupControl7.Controls.Add(this.labelControl145);
            this.groupControl7.Controls.Add(this.comboBoxEdit4);
            this.groupControl7.Controls.Add(this.labelControl144);
            this.groupControl7.Controls.Add(this.cbbThang);
            this.groupControl7.Controls.Add(this.labelControl143);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl7.Location = new System.Drawing.Point(0, 0);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(423, 510);
            this.groupControl7.TabIndex = 0;
            this.groupControl7.Text = "Thi đua tháng";
            // 
            // gridThang
            // 
            this.gridThang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridThang.Location = new System.Drawing.Point(5, 168);
            this.gridThang.MainView = this.gridViewThiduathang;
            this.gridThang.Name = "gridThang";
            this.gridThang.Size = new System.Drawing.Size(413, 326);
            this.gridThang.TabIndex = 116;
            this.gridThang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewThiduathang});
            // 
            // gridViewThiduathang
            // 
            this.gridViewThiduathang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDThangID,
            this.gridColTDThang,
            this.gridColTDThangNamhoc,
            this.gridColTDThangXeploai});
            this.gridViewThiduathang.GridControl = this.gridThang;
            this.gridViewThiduathang.Name = "gridViewThiduathang";
            this.gridViewThiduathang.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDThangID
            // 
            this.gridColTDThangID.Caption = "Mã";
            this.gridColTDThangID.FieldName = "ID";
            this.gridColTDThangID.Name = "gridColTDThangID";
            // 
            // gridColTDThang
            // 
            this.gridColTDThang.Caption = "Tháng";
            this.gridColTDThang.FieldName = "DOT";
            this.gridColTDThang.Name = "gridColTDThang";
            this.gridColTDThang.OptionsColumn.AllowEdit = false;
            this.gridColTDThang.OptionsColumn.ReadOnly = true;
            this.gridColTDThang.Visible = true;
            this.gridColTDThang.VisibleIndex = 0;
            // 
            // gridColTDThangNamhoc
            // 
            this.gridColTDThangNamhoc.Caption = "Năm học";
            this.gridColTDThangNamhoc.FieldName = "NAMHOC";
            this.gridColTDThangNamhoc.Name = "gridColTDThangNamhoc";
            this.gridColTDThangNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDThangNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDThangNamhoc.Visible = true;
            this.gridColTDThangNamhoc.VisibleIndex = 1;
            // 
            // gridColTDThangXeploai
            // 
            this.gridColTDThangXeploai.Caption = "Xếp loại";
            this.gridColTDThangXeploai.FieldName = "XEPLOAI";
            this.gridColTDThangXeploai.Name = "gridColTDThangXeploai";
            this.gridColTDThangXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDThangXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDThangXeploai.Visible = true;
            this.gridColTDThangXeploai.VisibleIndex = 2;
            // 
            // simpleButton63
            // 
            this.simpleButton63.Location = new System.Drawing.Point(175, 123);
            this.simpleButton63.Name = "simpleButton63";
            this.simpleButton63.Size = new System.Drawing.Size(75, 23);
            this.simpleButton63.TabIndex = 115;
            this.simpleButton63.Text = "Xóa";
            // 
            // simpleButton65
            // 
            this.simpleButton65.Location = new System.Drawing.Point(94, 123);
            this.simpleButton65.Name = "simpleButton65";
            this.simpleButton65.Size = new System.Drawing.Size(75, 23);
            this.simpleButton65.TabIndex = 113;
            this.simpleButton65.Text = "Thêm";
            // 
            // simpleButton66
            // 
            this.simpleButton66.Location = new System.Drawing.Point(13, 123);
            this.simpleButton66.Name = "simpleButton66";
            this.simpleButton66.Size = new System.Drawing.Size(75, 23);
            this.simpleButton66.TabIndex = 112;
            this.simpleButton66.Text = "Tạo mới";
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.EditValue = "A";
            this.comboBoxEdit5.Location = new System.Drawing.Point(118, 83);
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit5.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit5.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit5.Size = new System.Drawing.Size(132, 22);
            this.comboBoxEdit5.TabIndex = 5;
            // 
            // labelControl145
            // 
            this.labelControl145.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl145.Location = new System.Drawing.Point(13, 86);
            this.labelControl145.Name = "labelControl145";
            this.labelControl145.Size = new System.Drawing.Size(48, 15);
            this.labelControl145.TabIndex = 4;
            this.labelControl145.Text = "Xếp loại:";
            // 
            // comboBoxEdit4
            // 
            this.comboBoxEdit4.Location = new System.Drawing.Point(118, 55);
            this.comboBoxEdit4.Name = "comboBoxEdit4";
            this.comboBoxEdit4.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit4.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit4.Size = new System.Drawing.Size(132, 22);
            this.comboBoxEdit4.TabIndex = 3;
            // 
            // labelControl144
            // 
            this.labelControl144.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl144.Location = new System.Drawing.Point(13, 58);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(53, 15);
            this.labelControl144.TabIndex = 2;
            this.labelControl144.Text = "Năm học:";
            // 
            // cbbThang
            // 
            this.cbbThang.Location = new System.Drawing.Point(118, 27);
            this.cbbThang.Name = "cbbThang";
            this.cbbThang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbThang.Properties.Appearance.Options.UseFont = true;
            this.cbbThang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbThang.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cbbThang.Size = new System.Drawing.Size(132, 22);
            this.cbbThang.TabIndex = 1;
            // 
            // labelControl143
            // 
            this.labelControl143.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl143.Location = new System.Drawing.Point(13, 31);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(38, 15);
            this.labelControl143.TabIndex = 0;
            this.labelControl143.Text = "Tháng:";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.groupControl8);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.groupControl9);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(852, 510);
            this.splitContainerControl3.SplitterPosition = 431;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // groupControl8
            // 
            this.groupControl8.Controls.Add(this.gridTDHK);
            this.groupControl8.Controls.Add(this.simpleButton68);
            this.groupControl8.Controls.Add(this.simpleButton70);
            this.groupControl8.Controls.Add(this.simpleButton71);
            this.groupControl8.Controls.Add(this.comboBoxEdit6);
            this.groupControl8.Controls.Add(this.labelControl146);
            this.groupControl8.Controls.Add(this.comboBoxEdit7);
            this.groupControl8.Controls.Add(this.labelControl147);
            this.groupControl8.Controls.Add(this.comboBoxEdit8);
            this.groupControl8.Controls.Add(this.labelControl148);
            this.groupControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl8.Location = new System.Drawing.Point(0, 0);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(431, 510);
            this.groupControl8.TabIndex = 0;
            this.groupControl8.Text = "Thi đua học kỳ";
            // 
            // gridTDHK
            // 
            this.gridTDHK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridTDHK.Location = new System.Drawing.Point(16, 168);
            this.gridTDHK.MainView = this.gridViewTDHocKy;
            this.gridTDHK.Name = "gridTDHK";
            this.gridTDHK.Size = new System.Drawing.Size(397, 326);
            this.gridTDHK.TabIndex = 128;
            this.gridTDHK.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTDHocKy});
            // 
            // gridViewTDHocKy
            // 
            this.gridViewTDHocKy.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDHKID,
            this.gridColTDHKHocKy,
            this.gridColTDHKNamhoc,
            this.gridColTDHKXeploai});
            this.gridViewTDHocKy.GridControl = this.gridTDHK;
            this.gridViewTDHocKy.Name = "gridViewTDHocKy";
            this.gridViewTDHocKy.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDHKID
            // 
            this.gridColTDHKID.Caption = "Mã";
            this.gridColTDHKID.FieldName = "ID";
            this.gridColTDHKID.Name = "gridColTDHKID";
            // 
            // gridColTDHKHocKy
            // 
            this.gridColTDHKHocKy.Caption = "Học kỳ";
            this.gridColTDHKHocKy.FieldName = "DOT";
            this.gridColTDHKHocKy.Name = "gridColTDHKHocKy";
            this.gridColTDHKHocKy.OptionsColumn.AllowEdit = false;
            this.gridColTDHKHocKy.OptionsColumn.ReadOnly = true;
            this.gridColTDHKHocKy.Visible = true;
            this.gridColTDHKHocKy.VisibleIndex = 0;
            // 
            // gridColTDHKNamhoc
            // 
            this.gridColTDHKNamhoc.Caption = "Năm học";
            this.gridColTDHKNamhoc.FieldName = "NAMHOC";
            this.gridColTDHKNamhoc.Name = "gridColTDHKNamhoc";
            this.gridColTDHKNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDHKNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDHKNamhoc.Visible = true;
            this.gridColTDHKNamhoc.VisibleIndex = 1;
            // 
            // gridColTDHKXeploai
            // 
            this.gridColTDHKXeploai.Caption = "Xếp loại";
            this.gridColTDHKXeploai.FieldName = "XEPLOAI";
            this.gridColTDHKXeploai.Name = "gridColTDHKXeploai";
            this.gridColTDHKXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDHKXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDHKXeploai.Visible = true;
            this.gridColTDHKXeploai.VisibleIndex = 2;
            // 
            // simpleButton68
            // 
            this.simpleButton68.Location = new System.Drawing.Point(178, 123);
            this.simpleButton68.Name = "simpleButton68";
            this.simpleButton68.Size = new System.Drawing.Size(75, 23);
            this.simpleButton68.TabIndex = 127;
            this.simpleButton68.Text = "Xóa";
            // 
            // simpleButton70
            // 
            this.simpleButton70.Location = new System.Drawing.Point(97, 123);
            this.simpleButton70.Name = "simpleButton70";
            this.simpleButton70.Size = new System.Drawing.Size(75, 23);
            this.simpleButton70.TabIndex = 125;
            this.simpleButton70.Text = "Thêm";
            // 
            // simpleButton71
            // 
            this.simpleButton71.Location = new System.Drawing.Point(16, 123);
            this.simpleButton71.Name = "simpleButton71";
            this.simpleButton71.Size = new System.Drawing.Size(75, 23);
            this.simpleButton71.TabIndex = 124;
            this.simpleButton71.Text = "Tạo mới";
            // 
            // comboBoxEdit6
            // 
            this.comboBoxEdit6.EditValue = "A";
            this.comboBoxEdit6.Location = new System.Drawing.Point(113, 83);
            this.comboBoxEdit6.Name = "comboBoxEdit6";
            this.comboBoxEdit6.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit6.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit6.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit6.Size = new System.Drawing.Size(140, 22);
            this.comboBoxEdit6.TabIndex = 122;
            // 
            // labelControl146
            // 
            this.labelControl146.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl146.Location = new System.Drawing.Point(23, 87);
            this.labelControl146.Name = "labelControl146";
            this.labelControl146.Size = new System.Drawing.Size(48, 15);
            this.labelControl146.TabIndex = 121;
            this.labelControl146.Text = "Xếp loại:";
            // 
            // comboBoxEdit7
            // 
            this.comboBoxEdit7.Location = new System.Drawing.Point(113, 55);
            this.comboBoxEdit7.Name = "comboBoxEdit7";
            this.comboBoxEdit7.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit7.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit7.Size = new System.Drawing.Size(140, 22);
            this.comboBoxEdit7.TabIndex = 120;
            // 
            // labelControl147
            // 
            this.labelControl147.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl147.Location = new System.Drawing.Point(23, 58);
            this.labelControl147.Name = "labelControl147";
            this.labelControl147.Size = new System.Drawing.Size(53, 15);
            this.labelControl147.TabIndex = 119;
            this.labelControl147.Text = "Năm học:";
            // 
            // comboBoxEdit8
            // 
            this.comboBoxEdit8.Location = new System.Drawing.Point(113, 24);
            this.comboBoxEdit8.Name = "comboBoxEdit8";
            this.comboBoxEdit8.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit8.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit8.Properties.Items.AddRange(new object[] {
            "I",
            "II"});
            this.comboBoxEdit8.Size = new System.Drawing.Size(140, 22);
            this.comboBoxEdit8.TabIndex = 118;
            // 
            // labelControl148
            // 
            this.labelControl148.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl148.Location = new System.Drawing.Point(23, 31);
            this.labelControl148.Name = "labelControl148";
            this.labelControl148.Size = new System.Drawing.Size(41, 15);
            this.labelControl148.TabIndex = 117;
            this.labelControl148.Text = "Học kỳ:";
            // 
            // groupControl9
            // 
            this.groupControl9.Controls.Add(this.gridTDNam);
            this.groupControl9.Controls.Add(this.simpleButton73);
            this.groupControl9.Controls.Add(this.simpleButton75);
            this.groupControl9.Controls.Add(this.simpleButton76);
            this.groupControl9.Controls.Add(this.comboBoxEdit9);
            this.groupControl9.Controls.Add(this.labelControl149);
            this.groupControl9.Controls.Add(this.comboBoxEdit10);
            this.groupControl9.Controls.Add(this.labelControl150);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl9.Location = new System.Drawing.Point(0, 0);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(416, 510);
            this.groupControl9.TabIndex = 0;
            this.groupControl9.Text = "Thi đua năm học";
            // 
            // gridTDNam
            // 
            this.gridTDNam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridTDNam.Location = new System.Drawing.Point(17, 168);
            this.gridTDNam.MainView = this.gridViewTDNam;
            this.gridTDNam.Name = "gridTDNam";
            this.gridTDNam.Size = new System.Drawing.Size(394, 326);
            this.gridTDNam.TabIndex = 138;
            this.gridTDNam.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTDNam});
            // 
            // gridViewTDNam
            // 
            this.gridViewTDNam.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTDNamID,
            this.gridColTDNamNamhoc,
            this.gridColTDNamXeploai});
            this.gridViewTDNam.GridControl = this.gridTDNam;
            this.gridViewTDNam.Name = "gridViewTDNam";
            this.gridViewTDNam.OptionsView.ShowGroupPanel = false;
            // 
            // gridColTDNamID
            // 
            this.gridColTDNamID.Caption = "Mã";
            this.gridColTDNamID.FieldName = "ID";
            this.gridColTDNamID.Name = "gridColTDNamID";
            // 
            // gridColTDNamNamhoc
            // 
            this.gridColTDNamNamhoc.Caption = "Năm học";
            this.gridColTDNamNamhoc.FieldName = "NAMHOC";
            this.gridColTDNamNamhoc.Name = "gridColTDNamNamhoc";
            this.gridColTDNamNamhoc.OptionsColumn.AllowEdit = false;
            this.gridColTDNamNamhoc.OptionsColumn.ReadOnly = true;
            this.gridColTDNamNamhoc.Visible = true;
            this.gridColTDNamNamhoc.VisibleIndex = 0;
            // 
            // gridColTDNamXeploai
            // 
            this.gridColTDNamXeploai.Caption = "Xếp loại";
            this.gridColTDNamXeploai.FieldName = "XEPLOAI";
            this.gridColTDNamXeploai.Name = "gridColTDNamXeploai";
            this.gridColTDNamXeploai.OptionsColumn.AllowEdit = false;
            this.gridColTDNamXeploai.OptionsColumn.ReadOnly = true;
            this.gridColTDNamXeploai.Visible = true;
            this.gridColTDNamXeploai.VisibleIndex = 1;
            // 
            // simpleButton73
            // 
            this.simpleButton73.Location = new System.Drawing.Point(183, 123);
            this.simpleButton73.Name = "simpleButton73";
            this.simpleButton73.Size = new System.Drawing.Size(75, 23);
            this.simpleButton73.TabIndex = 137;
            this.simpleButton73.Text = "Xóa";
            // 
            // simpleButton75
            // 
            this.simpleButton75.Location = new System.Drawing.Point(102, 123);
            this.simpleButton75.Name = "simpleButton75";
            this.simpleButton75.Size = new System.Drawing.Size(75, 23);
            this.simpleButton75.TabIndex = 135;
            this.simpleButton75.Text = "Thêm";
            // 
            // simpleButton76
            // 
            this.simpleButton76.Location = new System.Drawing.Point(21, 123);
            this.simpleButton76.Name = "simpleButton76";
            this.simpleButton76.Size = new System.Drawing.Size(75, 23);
            this.simpleButton76.TabIndex = 134;
            this.simpleButton76.Text = "Tạo mới";
            // 
            // comboBoxEdit9
            // 
            this.comboBoxEdit9.EditValue = "A";
            this.comboBoxEdit9.Location = new System.Drawing.Point(116, 82);
            this.comboBoxEdit9.Name = "comboBoxEdit9";
            this.comboBoxEdit9.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit9.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit9.Properties.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Không xét"});
            this.comboBoxEdit9.Size = new System.Drawing.Size(142, 22);
            this.comboBoxEdit9.TabIndex = 132;
            // 
            // labelControl149
            // 
            this.labelControl149.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl149.Location = new System.Drawing.Point(21, 86);
            this.labelControl149.Name = "labelControl149";
            this.labelControl149.Size = new System.Drawing.Size(48, 15);
            this.labelControl149.TabIndex = 131;
            this.labelControl149.Text = "Xếp loại:";
            // 
            // comboBoxEdit10
            // 
            this.comboBoxEdit10.Location = new System.Drawing.Point(116, 54);
            this.comboBoxEdit10.Name = "comboBoxEdit10";
            this.comboBoxEdit10.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit10.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit10.Size = new System.Drawing.Size(142, 22);
            this.comboBoxEdit10.TabIndex = 130;
            // 
            // labelControl150
            // 
            this.labelControl150.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl150.Location = new System.Drawing.Point(21, 58);
            this.labelControl150.Name = "labelControl150";
            this.labelControl150.Size = new System.Drawing.Size(53, 15);
            this.labelControl150.TabIndex = 129;
            this.labelControl150.Text = "Năm học:";
            // 
            // ucNVThiDua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.splitContainerControl2);
            this.Name = "ucNVThiDua";
            this.Size = new System.Drawing.Size(1280, 510);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThiduathang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDHK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDHocKy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTDNam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTDNam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit10.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraGrid.GridControl gridThang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewThiduathang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDThangID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDThang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDThangNamhoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDThangXeploai;
        private DevExpress.XtraEditors.SimpleButton simpleButton63;
        private DevExpress.XtraEditors.SimpleButton simpleButton65;
        private DevExpress.XtraEditors.SimpleButton simpleButton66;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl145;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl144;
        private DevExpress.XtraEditors.ComboBoxEdit cbbThang;
        private DevExpress.XtraEditors.LabelControl labelControl143;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraGrid.GridControl gridTDHK;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTDHocKy;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDHKID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDHKHocKy;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDHKNamhoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDHKXeploai;
        private DevExpress.XtraEditors.SimpleButton simpleButton68;
        private DevExpress.XtraEditors.SimpleButton simpleButton70;
        private DevExpress.XtraEditors.SimpleButton simpleButton71;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl146;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl147;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl148;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraGrid.GridControl gridTDNam;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTDNam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDNamID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDNamNamhoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTDNamXeploai;
        private DevExpress.XtraEditors.SimpleButton simpleButton73;
        private DevExpress.XtraEditors.SimpleButton simpleButton75;
        private DevExpress.XtraEditors.SimpleButton simpleButton76;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl149;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl150;
    }
}
