﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVQuyetDinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcQuyetDinh = new DevExpress.XtraEditors.GroupControl();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.meNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucTu = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.txtNguoiKy = new DevExpress.XtraEditors.TextEdit();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.deNgayKy = new DevExpress.XtraEditors.DateEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoaiQD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaQD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.gridQD = new DevExpress.XtraGrid.GridControl();
            this.gridViewQuyetDinh = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColQDID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDSoQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDLoaiQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDNgayky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDNguoiky = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDTungay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDDenngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCoQDNoidung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQDXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuyetDinh)).BeginInit();
            this.gcQuyetDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            this.SuspendLayout();
            // 
            // gcQuyetDinh
            // 
            this.gcQuyetDinh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcQuyetDinh.Controls.Add(this.btnPg3Xoa);
            this.gcQuyetDinh.Controls.Add(this.btnPg3Them);
            this.gcQuyetDinh.Controls.Add(this.btnPg3TaoMoi);
            this.gcQuyetDinh.Controls.Add(this.meNoiDung);
            this.gcQuyetDinh.Controls.Add(this.labelControl117);
            this.gcQuyetDinh.Controls.Add(this.deHieuLucDen);
            this.gcQuyetDinh.Controls.Add(this.labelControl116);
            this.gcQuyetDinh.Controls.Add(this.deHieuLucTu);
            this.gcQuyetDinh.Controls.Add(this.labelControl115);
            this.gcQuyetDinh.Controls.Add(this.txtNguoiKy);
            this.gcQuyetDinh.Controls.Add(this.labelControl114);
            this.gcQuyetDinh.Controls.Add(this.deNgayKy);
            this.gcQuyetDinh.Controls.Add(this.labelControl113);
            this.gcQuyetDinh.Controls.Add(this.lueLoaiQD);
            this.gcQuyetDinh.Controls.Add(this.labelControl112);
            this.gcQuyetDinh.Controls.Add(this.txtMaQD);
            this.gcQuyetDinh.Controls.Add(this.labelControl111);
            this.gcQuyetDinh.Location = new System.Drawing.Point(13, 3);
            this.gcQuyetDinh.Name = "gcQuyetDinh";
            this.gcQuyetDinh.Size = new System.Drawing.Size(1241, 192);
            this.gcQuyetDinh.TabIndex = 0;
            this.gcQuyetDinh.Text = "Quyết định";
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(247, 154);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 122;
            this.btnPg3Xoa.Text = "Xóa";
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(138, 154);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 123;
            this.btnPg3Them.Text = "Thêm";
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(29, 154);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 124;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            // 
            // meNoiDung
            // 
            this.meNoiDung.Location = new System.Drawing.Point(668, 39);
            this.meNoiDung.Name = "meNoiDung";
            this.meNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meNoiDung.Properties.Appearance.Options.UseFont = true;
            this.meNoiDung.Size = new System.Drawing.Size(249, 99);
            this.meNoiDung.TabIndex = 121;
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl117.Location = new System.Drawing.Point(601, 43);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(52, 15);
            this.labelControl117.TabIndex = 120;
            this.labelControl117.Text = "Nội dung:";
            // 
            // deHieuLucDen
            // 
            this.deHieuLucDen.EditValue = null;
            this.deHieuLucDen.Location = new System.Drawing.Point(403, 112);
            this.deHieuLucDen.Name = "deHieuLucDen";
            this.deHieuLucDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucDen.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucDen.Size = new System.Drawing.Size(155, 22);
            this.deHieuLucDen.TabIndex = 119;
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl116.Location = new System.Drawing.Point(317, 115);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(74, 15);
            this.labelControl116.TabIndex = 118;
            this.labelControl116.Text = "Hiệu lực đến:";
            // 
            // deHieuLucTu
            // 
            this.deHieuLucTu.EditValue = null;
            this.deHieuLucTu.Location = new System.Drawing.Point(114, 112);
            this.deHieuLucTu.Name = "deHieuLucTu";
            this.deHieuLucTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucTu.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucTu.Size = new System.Drawing.Size(159, 22);
            this.deHieuLucTu.TabIndex = 117;
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl115.Location = new System.Drawing.Point(31, 115);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(66, 15);
            this.labelControl115.TabIndex = 116;
            this.labelControl115.Text = "Hiệu lực từ:";
            // 
            // txtNguoiKy
            // 
            this.txtNguoiKy.Location = new System.Drawing.Point(403, 76);
            this.txtNguoiKy.Name = "txtNguoiKy";
            this.txtNguoiKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKy.Properties.Appearance.Options.UseFont = true;
            this.txtNguoiKy.Size = new System.Drawing.Size(155, 22);
            this.txtNguoiKy.TabIndex = 115;
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl114.Location = new System.Drawing.Point(317, 79);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(55, 15);
            this.labelControl114.TabIndex = 114;
            this.labelControl114.Text = "Người ký:";
            // 
            // deNgayKy
            // 
            this.deNgayKy.EditValue = null;
            this.deNgayKy.Location = new System.Drawing.Point(114, 76);
            this.deNgayKy.Name = "deNgayKy";
            this.deNgayKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgayKy.Properties.Appearance.Options.UseFont = true;
            this.deNgayKy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayKy.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayKy.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayKy.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayKy.Size = new System.Drawing.Size(159, 22);
            this.deNgayKy.TabIndex = 113;
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl113.Location = new System.Drawing.Point(31, 79);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(47, 15);
            this.labelControl113.TabIndex = 112;
            this.labelControl113.Text = "Ngày ký:";
            // 
            // lueLoaiQD
            // 
            this.lueLoaiQD.Location = new System.Drawing.Point(403, 40);
            this.lueLoaiQD.Name = "lueLoaiQD";
            this.lueLoaiQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueLoaiQD.Properties.Appearance.Options.UseFont = true;
            this.lueLoaiQD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiQD.Properties.NullText = "";
            this.lueLoaiQD.Size = new System.Drawing.Size(155, 22);
            this.lueLoaiQD.TabIndex = 111;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(317, 43);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(48, 15);
            this.labelControl112.TabIndex = 110;
            this.labelControl112.Text = "Loại QĐ:";
            // 
            // txtMaQD
            // 
            this.txtMaQD.Location = new System.Drawing.Point(114, 40);
            this.txtMaQD.Name = "txtMaQD";
            this.txtMaQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaQD.Properties.Appearance.Options.UseFont = true;
            this.txtMaQD.Size = new System.Drawing.Size(159, 22);
            this.txtMaQD.TabIndex = 109;
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl111.Location = new System.Drawing.Point(31, 43);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(41, 15);
            this.labelControl111.TabIndex = 108;
            this.labelControl111.Text = "Mã QĐ:";
            // 
            // gridQD
            // 
            this.gridQD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQD.Location = new System.Drawing.Point(13, 197);
            this.gridQD.MainView = this.gridViewQuyetDinh;
            this.gridQD.Name = "gridQD";
            this.gridQD.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa});
            this.gridQD.Size = new System.Drawing.Size(1241, 306);
            this.gridQD.TabIndex = 142;
            this.gridQD.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuyetDinh});
            // 
            // gridViewQuyetDinh
            // 
            this.gridViewQuyetDinh.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColQDID,
            this.gridColQDSoQD,
            this.gridColQDLoaiQD,
            this.gridColQDNgayky,
            this.gridColQDNguoiky,
            this.gridColQDTungay,
            this.gridColQDDenngay,
            this.gridCoQDNoidung,
            this.gridColQDXoa});
            this.gridViewQuyetDinh.GridControl = this.gridQD;
            this.gridViewQuyetDinh.GroupPanelText = "Danh sách các Hợp đồng";
            this.gridViewQuyetDinh.Name = "gridViewQuyetDinh";
            // 
            // gridColQDID
            // 
            this.gridColQDID.Caption = "Mã ID";
            this.gridColQDID.FieldName = "ID";
            this.gridColQDID.Name = "gridColQDID";
            this.gridColQDID.OptionsColumn.AllowEdit = false;
            this.gridColQDID.OptionsColumn.ReadOnly = true;
            // 
            // gridColQDSoQD
            // 
            this.gridColQDSoQD.Caption = "Số QĐ";
            this.gridColQDSoQD.FieldName = "SOQD";
            this.gridColQDSoQD.Name = "gridColQDSoQD";
            this.gridColQDSoQD.OptionsColumn.AllowEdit = false;
            this.gridColQDSoQD.OptionsColumn.ReadOnly = true;
            this.gridColQDSoQD.Visible = true;
            this.gridColQDSoQD.VisibleIndex = 0;
            // 
            // gridColQDLoaiQD
            // 
            this.gridColQDLoaiQD.Caption = "Loại QĐ";
            this.gridColQDLoaiQD.FieldName = "TENLOAIQD";
            this.gridColQDLoaiQD.Name = "gridColQDLoaiQD";
            this.gridColQDLoaiQD.OptionsColumn.AllowEdit = false;
            this.gridColQDLoaiQD.OptionsColumn.ReadOnly = true;
            this.gridColQDLoaiQD.Visible = true;
            this.gridColQDLoaiQD.VisibleIndex = 1;
            // 
            // gridColQDNgayky
            // 
            this.gridColQDNgayky.Caption = "Ngày ký";
            this.gridColQDNgayky.FieldName = "NGAYKY";
            this.gridColQDNgayky.Name = "gridColQDNgayky";
            this.gridColQDNgayky.OptionsColumn.AllowEdit = false;
            this.gridColQDNgayky.OptionsColumn.ReadOnly = true;
            this.gridColQDNgayky.Visible = true;
            this.gridColQDNgayky.VisibleIndex = 2;
            // 
            // gridColQDNguoiky
            // 
            this.gridColQDNguoiky.Caption = "Người ký";
            this.gridColQDNguoiky.FieldName = "NGUOIKY";
            this.gridColQDNguoiky.Name = "gridColQDNguoiky";
            this.gridColQDNguoiky.OptionsColumn.AllowEdit = false;
            this.gridColQDNguoiky.OptionsColumn.ReadOnly = true;
            this.gridColQDNguoiky.Visible = true;
            this.gridColQDNguoiky.VisibleIndex = 3;
            // 
            // gridColQDTungay
            // 
            this.gridColQDTungay.Caption = "Hiệu lực từ";
            this.gridColQDTungay.FieldName = "TUNGAY";
            this.gridColQDTungay.Name = "gridColQDTungay";
            this.gridColQDTungay.OptionsColumn.AllowEdit = false;
            this.gridColQDTungay.OptionsColumn.ReadOnly = true;
            this.gridColQDTungay.Visible = true;
            this.gridColQDTungay.VisibleIndex = 4;
            // 
            // gridColQDDenngay
            // 
            this.gridColQDDenngay.Caption = "Hiệu lực đến";
            this.gridColQDDenngay.FieldName = "DENNGAY";
            this.gridColQDDenngay.Name = "gridColQDDenngay";
            this.gridColQDDenngay.OptionsColumn.AllowEdit = false;
            this.gridColQDDenngay.OptionsColumn.ReadOnly = true;
            this.gridColQDDenngay.Visible = true;
            this.gridColQDDenngay.VisibleIndex = 5;
            // 
            // gridCoQDNoidung
            // 
            this.gridCoQDNoidung.Caption = "Nội dung";
            this.gridCoQDNoidung.FieldName = "NOIDUNG";
            this.gridCoQDNoidung.Name = "gridCoQDNoidung";
            this.gridCoQDNoidung.OptionsColumn.AllowEdit = false;
            this.gridCoQDNoidung.OptionsColumn.ReadOnly = true;
            this.gridCoQDNoidung.Visible = true;
            this.gridCoQDNoidung.VisibleIndex = 6;
            // 
            // gridColQDXoa
            // 
            this.gridColQDXoa.Caption = "Xóa";
            this.gridColQDXoa.ColumnEdit = this.repositoryItemHyperLinkEditXoa;
            this.gridColQDXoa.Name = "gridColQDXoa";
            this.gridColQDXoa.Visible = true;
            this.gridColQDXoa.VisibleIndex = 7;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // ucNVQuyetDinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridQD);
            this.Controls.Add(this.gcQuyetDinh);
            this.Name = "ucNVQuyetDinh";
            this.Size = new System.Drawing.Size(1270, 510);
            ((System.ComponentModel.ISupportInitialize)(this.gcQuyetDinh)).EndInit();
            this.gcQuyetDinh.ResumeLayout(false);
            this.gcQuyetDinh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcQuyetDinh;
        private GridControl gridQD;
        private GridView gridViewQuyetDinh;
        private GridColumn gridColQDID;
        private GridColumn gridColQDSoQD;
        private GridColumn gridColQDLoaiQD;
        private GridColumn gridColQDNgayky;
        private GridColumn gridColQDNguoiky;
        private GridColumn gridColQDTungay;
        private GridColumn gridColQDDenngay;
        private GridColumn gridCoQDNoidung;
        private GridColumn gridColQDXoa;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
        private MemoEdit meNoiDung;
        private LabelControl labelControl117;
        private DateEdit deHieuLucDen;
        private LabelControl labelControl116;
        private DateEdit deHieuLucTu;
        private LabelControl labelControl115;
        private TextEdit txtNguoiKy;
        private LabelControl labelControl114;
        private DateEdit deNgayKy;
        private LabelControl labelControl113;
        private LookUpEdit lueLoaiQD;
        private LabelControl labelControl112;
        private TextEdit txtMaQD;
        private LabelControl labelControl111;
    }
}
