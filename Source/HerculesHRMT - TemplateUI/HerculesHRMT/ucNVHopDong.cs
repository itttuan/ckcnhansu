﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucNVHopDong : XtraUserControl
    {
        string strMaNV = "";
        HopDongCTL hopdongCTL = new HopDongCTL();
        public ucNVHopDong()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV)
        {
            strMaNV = maNV;
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gridHopdong.DataSource = hopdongCTL.DanhSachHopDongNhanVien(strMaNV);
        }

        private void ucNVHopDong_Load(object sender, EventArgs e)
        {

        }
    }
}
