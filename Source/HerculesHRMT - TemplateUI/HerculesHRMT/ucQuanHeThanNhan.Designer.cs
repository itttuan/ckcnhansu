﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucQuanHeThanNhan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcQuanHeThanNhan = new DevExpress.XtraGrid.GridControl();
            this.gridViewQuanHeThanNhan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaQuanHe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenQuanHe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhichu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtQuanHe = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuanHeThanNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuanHeThanNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuanHe.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcQuanHeThanNhan
            // 
            this.gcQuanHeThanNhan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcQuanHeThanNhan.Location = new System.Drawing.Point(2, 2);
            this.gcQuanHeThanNhan.MainView = this.gridViewQuanHeThanNhan;
            this.gcQuanHeThanNhan.Name = "gcQuanHeThanNhan";
            this.gcQuanHeThanNhan.Size = new System.Drawing.Size(781, 308);
            this.gcQuanHeThanNhan.TabIndex = 3;
            this.gcQuanHeThanNhan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuanHeThanNhan});
            // 
            // gridViewQuanHeThanNhan
            // 
            this.gridViewQuanHeThanNhan.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaQuanHe,
            this.gridColTenQuanHe,
            this.gridColGhichu});
            this.gridViewQuanHeThanNhan.GridControl = this.gcQuanHeThanNhan;
            this.gridViewQuanHeThanNhan.GroupPanelText = " ";
            this.gridViewQuanHeThanNhan.Name = "gridViewQuanHeThanNhan";
            this.gridViewQuanHeThanNhan.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewQuanHeThanNhan_FocusedRowChanged);
            // 
            // gridColMaQuanHe
            // 
            this.gridColMaQuanHe.Caption = "Mã Quan Hệ";
            this.gridColMaQuanHe.FieldName = "MaQuanHe";
            this.gridColMaQuanHe.Name = "gridColMaQuanHe";
            // 
            // gridColTenQuanHe
            // 
            this.gridColTenQuanHe.Caption = "Tên Quan Hệ";
            this.gridColTenQuanHe.FieldName = "TenQuanHe";
            this.gridColTenQuanHe.Name = "gridColTenQuanHe";
            this.gridColTenQuanHe.OptionsColumn.AllowEdit = false;
            this.gridColTenQuanHe.OptionsColumn.ReadOnly = true;
            this.gridColTenQuanHe.Visible = true;
            this.gridColTenQuanHe.VisibleIndex = 0;
            // 
            // gridColGhichu
            // 
            this.gridColGhichu.Caption = "Ghi Chú";
            this.gridColGhichu.FieldName = "GhiChu";
            this.gridColGhichu.Name = "gridColGhichu";
            this.gridColGhichu.OptionsColumn.AllowEdit = false;
            this.gridColGhichu.OptionsColumn.ReadOnly = true;
            this.gridColGhichu.Visible = true;
            this.gridColGhichu.VisibleIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcQuanHeThanNhan);
            this.panelControl3.Location = new System.Drawing.Point(0, 202);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 312);
            this.panelControl3.TabIndex = 4;
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(620, 171);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 9;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtQuanHe);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 201);
            this.panelControl1.TabIndex = 5;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(135, 91);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(573, 61);
            this.meGhiChu.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(45, 93);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(527, 171);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 4;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(434, 171);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 3;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(343, 171);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 2;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtQuanHe
            // 
            this.txtQuanHe.Location = new System.Drawing.Point(135, 64);
            this.txtQuanHe.Name = "txtQuanHe";
            this.txtQuanHe.Size = new System.Drawing.Size(269, 20);
            this.txtQuanHe.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(45, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Quan Hệ:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(255, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(270, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Quan Hệ Thân Nhân";
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // ucQuanHeThanNhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucQuanHeThanNhan";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucQuanHeThanNhan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcQuanHeThanNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuanHeThanNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuanHe.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControl gcQuanHeThanNhan;
        private GridView gridViewQuanHeThanNhan;
        private GridColumn gridColMaQuanHe;
        private GridColumn gridColTenQuanHe;
        private PanelControl panelControl3;
        private SimpleButton btnXoa;
        private PanelControl panelControl2;
        private PanelControl panelControl1;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtQuanHe;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenDayDu;
        private GridColumn gridColTenVietTat;
        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
        private GridColumn gridColGhichu;
    }
}
