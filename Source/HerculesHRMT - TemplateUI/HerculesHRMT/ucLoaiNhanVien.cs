﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucLoaiNhanVien : XtraUserControl
    {
        LoaiNhanVienCTL lnvCTL = new LoaiNhanVienCTL();
        List<LoaiNhanVienDTO> listLoaiNV = new List<LoaiNhanVienDTO>();
        LoaiNhanVienDTO lnvSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucLoaiNhanVien()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            listLoaiNV = lnvCTL.GetAll();

            gcLoaiNV.DataSource = listLoaiNV;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(LoaiNhanVienDTO lnvDTO)
        {
            if (lnvDTO != null)
            {
                txtLoaiNV.Text = lnvDTO.TenLoaiNV;                
                meGhiChu.Text = lnvDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtLoaiNV.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    txtLoaiNV.Text = string.Empty;                    
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtLoaiNV.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(lnvSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtLoaiNV.Properties.ReadOnly = true;                    
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(lnvSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtLoaiNV.Text == string.Empty)
                return false;

            return true;
        }

        private LoaiNhanVienDTO GetData(LoaiNhanVienDTO lnvDTO)
        {
            string maLNV = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexLNV = lnvCTL.GetMaxMaLoaiNhanVien();
                if (sLastIndexLNV.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexLNV = int.Parse(sLastIndexLNV);
                    maLNV += (nLastIndexLNV + 1).ToString().PadLeft(2,'0');
                }
                else
                    maLNV += "01";

                lnvDTO.NgayHieuLuc = DateTime.Now;
                lnvDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maLNV = lnvSelect.MaLoaiNV;
                lnvDTO.NgayHieuLuc = lnvSelect.NgayHieuLuc;
                lnvDTO.TinhTrang = lnvSelect.TinhTrang;
            }
            lnvDTO.MaLoaiNV = maLNV;
            lnvDTO.TenLoaiNV = txtLoaiNV.Text;            
            lnvDTO.GhiChu = meGhiChu.Text;

            return lnvDTO;
        }

        private void gridViewLoaiNV_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listLoaiNV.Count)
            {
                lnvSelect = ((GridView)sender).GetRow(_selectedIndex) as LoaiNhanVienDTO;
            }
            else
                if (listLoaiNV.Count != 0)
                    lnvSelect = listLoaiNV[0];
                else
                    lnvSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên loại nhân viên", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoaiNhanVienDTO lnvNew = GetData(new LoaiNhanVienDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = lnvCTL.Save(lnvNew);
            else
                isSucess = lnvCTL.Update(lnvNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listLoaiNV.IndexOf(listLoaiNV.FirstOrDefault(p => p.MaLoaiNV == lnvNew.MaLoaiNV));
                gridViewLoaiNV.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
                       

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (lnvSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa loại nhân viên " + lnvSelect.TenLoaiNV + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = lnvCTL.Delete(lnvSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listLoaiNV.Count != 0)
                            gridViewLoaiNV.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void ucLoaiNhanVien_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }


    }
}
