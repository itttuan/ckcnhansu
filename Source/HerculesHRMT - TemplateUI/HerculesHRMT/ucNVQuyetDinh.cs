﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucNVQuyetDinh : XtraUserControl
    {
        string strMaNV = "";
        QuyetDinhCTL quyetdinhCTL = new QuyetDinhCTL();
        public ucNVQuyetDinh()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV)
        {
            strMaNV = maNV;
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gridQD.DataSource = quyetdinhCTL.DanhSachQuyetDinhNhanVien(strMaNV);
        }
        
    }
}
