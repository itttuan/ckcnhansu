﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucTinhThanh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.radLoaiTinhThanh = new DevExpress.XtraEditors.RadioGroup();
            this.txtDauSoCMND = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtTinhThanh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gcTinhThanh = new DevExpress.XtraGrid.GridControl();
            this.gridViewTinhThanh = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaTinhThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenTinhThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCoLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDauSoCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoaiTinhThanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDauSoCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhThanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTinhThanh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTinhThanh)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.radLoaiTinhThanh);
            this.panelControl1.Controls.Add(this.txtDauSoCMND);
            this.panelControl1.Controls.Add(this.txtTenDD);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtTinhThanh);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 174);
            this.panelControl1.TabIndex = 5;
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(465, 104);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(214, 20);
            this.txtTenVT.TabIndex = 4;
            // 
            // labelControl6
            // 
            this.labelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.labelControl6.Location = new System.Drawing.Point(30, 144);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(70, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Đầu số CMND:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(383, 107);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Tên viết tắt:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(383, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(23, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Loại:";
            // 
            // radLoaiTinhThanh
            // 
            this.radLoaiTinhThanh.EditValue = "0";
            this.radLoaiTinhThanh.Location = new System.Drawing.Point(465, 63);
            this.radLoaiTinhThanh.Name = "radLoaiTinhThanh";
            this.radLoaiTinhThanh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Tỉnh"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Thành phố")});
            this.radLoaiTinhThanh.Size = new System.Drawing.Size(214, 26);
            this.radLoaiTinhThanh.TabIndex = 3;
            // 
            // txtDauSoCMND
            // 
            this.txtDauSoCMND.Location = new System.Drawing.Point(136, 141);
            this.txtDauSoCMND.Name = "txtDauSoCMND";
            this.txtDauSoCMND.Size = new System.Drawing.Size(71, 20);
            this.txtDauSoCMND.TabIndex = 2;
            this.txtDauSoCMND.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtDauSoCMND_EditValueChanging);
            // 
            // txtTenDD
            // 
            this.txtTenDD.Location = new System.Drawing.Point(136, 104);
            this.txtTenDD.Name = "txtTenDD";
            this.txtTenDD.Size = new System.Drawing.Size(214, 20);
            this.txtTenDD.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 107);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(58, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Tên đầy đủ:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(592, 144);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(499, 144);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 7;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(406, 144);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 6;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(313, 144);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtTinhThanh
            // 
            this.txtTinhThanh.Location = new System.Drawing.Point(136, 69);
            this.txtTinhThanh.Name = "txtTinhThanh";
            this.txtTinhThanh.Size = new System.Drawing.Size(214, 20);
            this.txtTinhThanh.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(79, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Tỉnh/Thành:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(271, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(239, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tỉnh - Thành Phố ";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcTinhThanh);
            this.panelControl3.Location = new System.Drawing.Point(0, 193);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 322);
            this.panelControl3.TabIndex = 4;
            // 
            // gcTinhThanh
            // 
            this.gcTinhThanh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTinhThanh.Location = new System.Drawing.Point(2, 2);
            this.gcTinhThanh.MainView = this.gridViewTinhThanh;
            this.gcTinhThanh.Name = "gcTinhThanh";
            this.gcTinhThanh.Size = new System.Drawing.Size(781, 318);
            this.gcTinhThanh.TabIndex = 3;
            this.gcTinhThanh.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTinhThanh});
            // 
            // gridViewTinhThanh
            // 
            this.gridViewTinhThanh.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaTinhThanh,
            this.gridColLoai,
            this.gridColTenTinhThanh,
            this.gridColTenDayDu,
            this.gridColTenVietTat,
            this.gridCoLoai,
            this.gridColDauSoCMND});
            this.gridViewTinhThanh.GridControl = this.gcTinhThanh;
            this.gridViewTinhThanh.GroupPanelText = " ";
            this.gridViewTinhThanh.Name = "gridViewTinhThanh";
            this.gridViewTinhThanh.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTinhThanh_FocusedRowChanged);
            // 
            // gridColMaTinhThanh
            // 
            this.gridColMaTinhThanh.Caption = "Mã Tỉnh Thành";
            this.gridColMaTinhThanh.FieldName = "MaTinhThanh";
            this.gridColMaTinhThanh.Name = "gridColMaTinhThanh";
            // 
            // gridColLoai
            // 
            this.gridColLoai.Caption = "gridColumn1";
            this.gridColLoai.FieldName = "Loai";
            this.gridColLoai.Name = "gridColLoai";
            // 
            // gridColTenTinhThanh
            // 
            this.gridColTenTinhThanh.Caption = "Tên Tỉnh/Thành";
            this.gridColTenTinhThanh.FieldName = "TenTinhThanh";
            this.gridColTenTinhThanh.Name = "gridColTenTinhThanh";
            this.gridColTenTinhThanh.OptionsColumn.AllowEdit = false;
            this.gridColTenTinhThanh.OptionsColumn.ReadOnly = true;
            this.gridColTenTinhThanh.Visible = true;
            this.gridColTenTinhThanh.VisibleIndex = 0;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên Đầy Đủ";
            this.gridColTenDayDu.FieldName = "TenDayDu";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 1;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên Viết Tắt";
            this.gridColTenVietTat.FieldName = "TenVietTat";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 2;
            // 
            // gridCoLoai
            // 
            this.gridCoLoai.Caption = "Loại Tỉnh Thành";
            this.gridCoLoai.FieldName = "TenLoaiTinhThanh";
            this.gridCoLoai.Name = "gridCoLoai";
            this.gridCoLoai.OptionsColumn.AllowEdit = false;
            this.gridCoLoai.OptionsColumn.ReadOnly = true;
            this.gridCoLoai.Visible = true;
            this.gridCoLoai.VisibleIndex = 3;
            // 
            // gridColDauSoCMND
            // 
            this.gridColDauSoCMND.Caption = "Đầu số CMND";
            this.gridColDauSoCMND.FieldName = "DauSoCMND";
            this.gridColDauSoCMND.Name = "gridColDauSoCMND";
            this.gridColDauSoCMND.Visible = true;
            this.gridColDauSoCMND.VisibleIndex = 4;
            // 
            // ucTinhThanh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucTinhThanh";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucTinhThanh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoaiTinhThanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDauSoCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinhThanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTinhThanh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTinhThanh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelControl panelControl2;
        private GridControl gcTinhThanh;
        private PanelControl panelControl3;
        private PanelControl panelControl1;
        private TextEdit txtTenVT;
        private LabelControl labelControl5;
        private LabelControl labelControl4;
        private RadioGroup radLoaiTinhThanh;
        private TextEdit txtTenDD;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtTinhThanh;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridView gridViewTinhThanh;
        private GridColumn gridColMaTinhThanh;
        private GridColumn gridColTenTinhThanh;
        private GridColumn gridColTenDayDu;
        private GridColumn gridColTenVietTat;
        private GridColumn gridCoLoai;
        private GridColumn gridColLoai;
        private LabelControl labelControl6;
        private TextEdit txtDauSoCMND;
        private GridColumn gridColDauSoCMND;

    }
}
