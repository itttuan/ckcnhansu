﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVLuong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcLuong = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhuCap = new DevExpress.XtraEditors.TextEdit();
            this.btnPg3Sua = new DevExpress.XtraEditors.SimpleButton();
            this.chbNamDau = new DevExpress.XtraEditors.CheckEdit();
            this.cbbPg1Bac = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1MaNgach = new DevExpress.XtraEditors.LookUpEdit();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.deNgayhuong = new DevExpress.XtraEditors.DateEdit();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.txtDongBHXH = new DevExpress.XtraEditors.TextEdit();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.txtTileVuot = new DevExpress.XtraEditors.TextEdit();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCThamnien = new DevExpress.XtraEditors.TextEdit();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCChucvu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.txtLuongChinh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.txtLuongCB = new DevExpress.XtraEditors.TextEdit();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.txtHeSo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoQD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.gridLuong = new DevExpress.XtraGrid.GridControl();
            this.gridViewLuong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColLuongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongSQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongNgach = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongBac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongHeSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNamDau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColLuongCB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongChinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhuCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCCV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCTN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongVuot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongBHXH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongNgayhuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLuong)).BeginInit();
            this.gcLuong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNamDau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayhuong.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayhuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDongBHXH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTileVuot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCThamnien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCChucvu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongChinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongCB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            this.SuspendLayout();
            // 
            // gcLuong
            // 
            this.gcLuong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcLuong.Controls.Add(this.labelControl2);
            this.gcLuong.Controls.Add(this.labelControl1);
            this.gcLuong.Controls.Add(this.txtPhuCap);
            this.gcLuong.Controls.Add(this.btnPg3Sua);
            this.gcLuong.Controls.Add(this.chbNamDau);
            this.gcLuong.Controls.Add(this.cbbPg1Bac);
            this.gcLuong.Controls.Add(this.cbbPg1MaNgach);
            this.gcLuong.Controls.Add(this.btnPg3Xoa);
            this.gcLuong.Controls.Add(this.btnPg3Them);
            this.gcLuong.Controls.Add(this.btnPg3TaoMoi);
            this.gcLuong.Controls.Add(this.deNgayhuong);
            this.gcLuong.Controls.Add(this.labelControl93);
            this.gcLuong.Controls.Add(this.txtDongBHXH);
            this.gcLuong.Controls.Add(this.labelControl92);
            this.gcLuong.Controls.Add(this.txtTileVuot);
            this.gcLuong.Controls.Add(this.labelControl91);
            this.gcLuong.Controls.Add(this.txtPCThamnien);
            this.gcLuong.Controls.Add(this.labelControl90);
            this.gcLuong.Controls.Add(this.txtPCChucvu);
            this.gcLuong.Controls.Add(this.labelControl89);
            this.gcLuong.Controls.Add(this.txtLuongChinh);
            this.gcLuong.Controls.Add(this.labelControl88);
            this.gcLuong.Controls.Add(this.txtLuongCB);
            this.gcLuong.Controls.Add(this.labelControl87);
            this.gcLuong.Controls.Add(this.txtHeSo);
            this.gcLuong.Controls.Add(this.labelControl86);
            this.gcLuong.Controls.Add(this.labelControl85);
            this.gcLuong.Controls.Add(this.labelControl84);
            this.gcLuong.Controls.Add(this.txtSoQD);
            this.gcLuong.Controls.Add(this.labelControl83);
            this.gcLuong.Location = new System.Drawing.Point(21, 17);
            this.gcLuong.Name = "gcLuong";
            this.gcLuong.Size = new System.Drawing.Size(1167, 212);
            this.gcLuong.TabIndex = 0;
            this.gcLuong.Text = "Lương";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(229, 135);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(9, 15);
            this.labelControl2.TabIndex = 110;
            this.labelControl2.Text = "%";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(769, 135);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(49, 15);
            this.labelControl1.TabIndex = 109;
            this.labelControl1.Text = "Phụ cấp:";
            // 
            // txtPhuCap
            // 
            this.txtPhuCap.Location = new System.Drawing.Point(885, 128);
            this.txtPhuCap.Name = "txtPhuCap";
            this.txtPhuCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuCap.Properties.Appearance.Options.UseFont = true;
            this.txtPhuCap.Properties.DisplayFormat.FormatString = "##0.#0";
            this.txtPhuCap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPhuCap.Properties.EditFormat.FormatString = "##0.#0";
            this.txtPhuCap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPhuCap.Properties.Mask.EditMask = "##0.#0";
            this.txtPhuCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhuCap.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPhuCap.Size = new System.Drawing.Size(105, 22);
            this.txtPhuCap.TabIndex = 108;
            // 
            // btnPg3Sua
            // 
            this.btnPg3Sua.Location = new System.Drawing.Point(205, 170);
            this.btnPg3Sua.Name = "btnPg3Sua";
            this.btnPg3Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Sua.TabIndex = 107;
            this.btnPg3Sua.Text = "Hiệu chỉnh";
            this.btnPg3Sua.Click += new System.EventHandler(this.btnPg3Sua_Click);
            // 
            // chbNamDau
            // 
            this.chbNamDau.Location = new System.Drawing.Point(767, 40);
            this.chbNamDau.Name = "chbNamDau";
            this.chbNamDau.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNamDau.Properties.Appearance.Options.UseFont = true;
            this.chbNamDau.Properties.Caption = "Tập sự (85%)";
            this.chbNamDau.Properties.ValueChecked = "85";
            this.chbNamDau.Properties.ValueUnchecked = "0";
            this.chbNamDau.Size = new System.Drawing.Size(110, 20);
            this.chbNamDau.TabIndex = 106;
            // 
            // cbbPg1Bac
            // 
            this.cbbPg1Bac.Location = new System.Drawing.Point(545, 41);
            this.cbbPg1Bac.Name = "cbbPg1Bac";
            this.cbbPg1Bac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Bac.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Bac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Bac.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bac", "Bậc lương"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HeSo", "Hệ số", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cbbPg1Bac.Properties.DisplayMember = "Bac";
            this.cbbPg1Bac.Properties.NullText = "";
            this.cbbPg1Bac.Properties.ValueMember = "Bac";
            this.cbbPg1Bac.Size = new System.Drawing.Size(76, 22);
            this.cbbPg1Bac.TabIndex = 105;
            this.cbbPg1Bac.EditValueChanged += new System.EventHandler(this.cbbPg1Bac_EditValueChanged);
            // 
            // cbbPg1MaNgach
            // 
            this.cbbPg1MaNgach.Location = new System.Drawing.Point(343, 41);
            this.cbbPg1MaNgach.Name = "cbbPg1MaNgach";
            this.cbbPg1MaNgach.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1MaNgach.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1MaNgach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1MaNgach.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaNgach", "Mã ngạch")});
            this.cbbPg1MaNgach.Properties.DisplayMember = "MaNgach";
            this.cbbPg1MaNgach.Properties.NullText = "[Chọn mã ngạch]";
            this.cbbPg1MaNgach.Properties.ValueMember = "MaNgach";
            this.cbbPg1MaNgach.Size = new System.Drawing.Size(146, 22);
            this.cbbPg1MaNgach.TabIndex = 102;
            this.cbbPg1MaNgach.EditValueChanged += new System.EventHandler(this.cbbPg1MaNgach_EditValueChanged);
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(301, 170);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 102;
            this.btnPg3Xoa.Text = "Xóa";
            this.btnPg3Xoa.Click += new System.EventHandler(this.btnPg3Xoa_Click);
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(109, 170);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 103;
            this.btnPg3Them.Text = "Lưu";
            this.btnPg3Them.Click += new System.EventHandler(this.btnPg3Them_Click);
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(13, 170);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 104;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            this.btnPg3TaoMoi.Click += new System.EventHandler(this.btnPg3TaoMoi_Click);
            // 
            // deNgayhuong
            // 
            this.deNgayhuong.EditValue = null;
            this.deNgayhuong.Location = new System.Drawing.Point(607, 132);
            this.deNgayhuong.Name = "deNgayhuong";
            this.deNgayhuong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgayhuong.Properties.Appearance.Options.UseFont = true;
            this.deNgayhuong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayhuong.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayhuong.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayhuong.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayhuong.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayhuong.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayhuong.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayhuong.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayhuong.Size = new System.Drawing.Size(148, 22);
            this.deNgayhuong.TabIndex = 99;
            // 
            // labelControl93
            // 
            this.labelControl93.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl93.Location = new System.Drawing.Point(500, 135);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(73, 15);
            this.labelControl93.TabIndex = 98;
            this.labelControl93.Text = "Ngày hưởng:";
            // 
            // txtDongBHXH
            // 
            this.txtDongBHXH.Location = new System.Drawing.Point(343, 132);
            this.txtDongBHXH.Name = "txtDongBHXH";
            this.txtDongBHXH.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDongBHXH.Properties.Appearance.Options.UseFont = true;
            this.txtDongBHXH.Size = new System.Drawing.Size(146, 22);
            this.txtDongBHXH.TabIndex = 97;
            // 
            // labelControl92
            // 
            this.labelControl92.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl92.Location = new System.Drawing.Point(258, 135);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(68, 15);
            this.labelControl92.TabIndex = 96;
            this.labelControl92.Text = "Đóng BHXH:";
            // 
            // txtTileVuot
            // 
            this.txtTileVuot.Location = new System.Drawing.Point(113, 132);
            this.txtTileVuot.Name = "txtTileVuot";
            this.txtTileVuot.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTileVuot.Properties.Appearance.Options.UseFont = true;
            this.txtTileVuot.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTileVuot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTileVuot.Properties.DisplayFormat.FormatString = "##0";
            this.txtTileVuot.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTileVuot.Properties.EditFormat.FormatString = "##0";
            this.txtTileVuot.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTileVuot.Properties.Mask.EditMask = "##0";
            this.txtTileVuot.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTileVuot.Properties.NullText = "0";
            this.txtTileVuot.Size = new System.Drawing.Size(110, 22);
            this.txtTileVuot.TabIndex = 95;
            // 
            // labelControl91
            // 
            this.labelControl91.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl91.Location = new System.Drawing.Point(13, 135);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(95, 15);
            this.labelControl91.TabIndex = 94;
            this.labelControl91.Text = "Tỉ lệ vượt khung:";
            // 
            // txtPCThamnien
            // 
            this.txtPCThamnien.Location = new System.Drawing.Point(885, 86);
            this.txtPCThamnien.Name = "txtPCThamnien";
            this.txtPCThamnien.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCThamnien.Properties.Appearance.Options.UseFont = true;
            this.txtPCThamnien.Size = new System.Drawing.Size(105, 22);
            this.txtPCThamnien.TabIndex = 23;
            // 
            // labelControl90
            // 
            this.labelControl90.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl90.Location = new System.Drawing.Point(769, 89);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(108, 15);
            this.labelControl90.TabIndex = 22;
            this.labelControl90.Text = "Phụ cấp thâm niên:";
            // 
            // txtPCChucvu
            // 
            this.txtPCChucvu.Location = new System.Drawing.Point(607, 86);
            this.txtPCChucvu.Name = "txtPCChucvu";
            this.txtPCChucvu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCChucvu.Properties.Appearance.Options.UseFont = true;
            this.txtPCChucvu.Size = new System.Drawing.Size(148, 22);
            this.txtPCChucvu.TabIndex = 21;
            // 
            // labelControl89
            // 
            this.labelControl89.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl89.Location = new System.Drawing.Point(500, 89);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(98, 15);
            this.labelControl89.TabIndex = 20;
            this.labelControl89.Text = "Phụ cấp chức vụ:";
            // 
            // txtLuongChinh
            // 
            this.txtLuongChinh.Location = new System.Drawing.Point(343, 86);
            this.txtLuongChinh.Name = "txtLuongChinh";
            this.txtLuongChinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongChinh.Properties.Appearance.Options.UseFont = true;
            this.txtLuongChinh.Size = new System.Drawing.Size(146, 22);
            this.txtLuongChinh.TabIndex = 19;
            // 
            // labelControl88
            // 
            this.labelControl88.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl88.Location = new System.Drawing.Point(258, 89);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(76, 15);
            this.labelControl88.TabIndex = 18;
            this.labelControl88.Text = "Lương chính:";
            // 
            // txtLuongCB
            // 
            this.txtLuongCB.Location = new System.Drawing.Point(113, 86);
            this.txtLuongCB.Name = "txtLuongCB";
            this.txtLuongCB.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongCB.Properties.Appearance.Options.UseFont = true;
            this.txtLuongCB.Size = new System.Drawing.Size(125, 22);
            this.txtLuongCB.TabIndex = 17;
            // 
            // labelControl87
            // 
            this.labelControl87.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl87.Location = new System.Drawing.Point(13, 89);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(85, 15);
            this.labelControl87.TabIndex = 16;
            this.labelControl87.Text = "Lương cơ bản:";
            // 
            // txtHeSo
            // 
            this.txtHeSo.Location = new System.Drawing.Point(685, 41);
            this.txtHeSo.Name = "txtHeSo";
            this.txtHeSo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeSo.Properties.Appearance.Options.UseFont = true;
            this.txtHeSo.Properties.DisplayFormat.FormatString = "##0.#0";
            this.txtHeSo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHeSo.Properties.ReadOnly = true;
            this.txtHeSo.Size = new System.Drawing.Size(70, 22);
            this.txtHeSo.TabIndex = 15;
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl86.Location = new System.Drawing.Point(644, 44);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(35, 15);
            this.labelControl86.TabIndex = 14;
            this.labelControl86.Text = "Hệ số:";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl85.Location = new System.Drawing.Point(500, 43);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(41, 15);
            this.labelControl85.TabIndex = 12;
            this.labelControl85.Text = "Bậc: (*)";
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl84.Location = new System.Drawing.Point(258, 43);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(55, 15);
            this.labelControl84.TabIndex = 10;
            this.labelControl84.Text = "Ngạch: (*)";
            // 
            // txtSoQD
            // 
            this.txtSoQD.Location = new System.Drawing.Point(113, 40);
            this.txtSoQD.Name = "txtSoQD";
            this.txtSoQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQD.Properties.Appearance.Options.UseFont = true;
            this.txtSoQD.Size = new System.Drawing.Size(125, 22);
            this.txtSoQD.TabIndex = 9;
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl83.Location = new System.Drawing.Point(13, 43);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(79, 15);
            this.labelControl83.TabIndex = 8;
            this.labelControl83.Text = "Số quyết định:";
            // 
            // gridLuong
            // 
            this.gridLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLuong.Location = new System.Drawing.Point(21, 230);
            this.gridLuong.MainView = this.gridViewLuong;
            this.gridLuong.Name = "gridLuong";
            this.gridLuong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa,
            this.repositoryItemCheckEdit1});
            this.gridLuong.Size = new System.Drawing.Size(1167, 277);
            this.gridLuong.TabIndex = 101;
            this.gridLuong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLuong});
            // 
            // gridViewLuong
            // 
            this.gridViewLuong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColLuongID,
            this.gridColLuongSQD,
            this.gridColLuongNgach,
            this.gridColLuongBac,
            this.gridColLuongHeSo,
            this.gridColNamDau,
            this.gridColLuongCB,
            this.gridColLuongChinh,
            this.gridColPhuCap,
            this.gridColLuongPCCV,
            this.gridColLuongPCTN,
            this.gridColLuongVuot,
            this.gridColLuongBHXH,
            this.gridColLuongNgayhuong});
            this.gridViewLuong.GridControl = this.gridLuong;
            this.gridViewLuong.GroupPanelText = "Lịch sử Lương";
            this.gridViewLuong.Name = "gridViewLuong";
            this.gridViewLuong.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewLuong_RowClick);
            this.gridViewLuong.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewLuong_FocusedRowChanged);
            // 
            // gridColLuongID
            // 
            this.gridColLuongID.Caption = "Mã ID";
            this.gridColLuongID.FieldName = "ID";
            this.gridColLuongID.Name = "gridColLuongID";
            this.gridColLuongID.OptionsColumn.AllowEdit = false;
            this.gridColLuongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColLuongSQD
            // 
            this.gridColLuongSQD.Caption = "Số QĐ";
            this.gridColLuongSQD.FieldName = "SoQuyetDinh";
            this.gridColLuongSQD.Name = "gridColLuongSQD";
            this.gridColLuongSQD.OptionsColumn.AllowEdit = false;
            this.gridColLuongSQD.OptionsColumn.ReadOnly = true;
            this.gridColLuongSQD.Visible = true;
            this.gridColLuongSQD.VisibleIndex = 0;
            // 
            // gridColLuongNgach
            // 
            this.gridColLuongNgach.Caption = "Ngạch";
            this.gridColLuongNgach.FieldName = "MaNgach";
            this.gridColLuongNgach.Name = "gridColLuongNgach";
            this.gridColLuongNgach.OptionsColumn.AllowEdit = false;
            this.gridColLuongNgach.OptionsColumn.ReadOnly = true;
            this.gridColLuongNgach.Visible = true;
            this.gridColLuongNgach.VisibleIndex = 1;
            // 
            // gridColLuongBac
            // 
            this.gridColLuongBac.Caption = "Bậc";
            this.gridColLuongBac.FieldName = "BacLuong";
            this.gridColLuongBac.Name = "gridColLuongBac";
            this.gridColLuongBac.OptionsColumn.AllowEdit = false;
            this.gridColLuongBac.OptionsColumn.ReadOnly = true;
            this.gridColLuongBac.Visible = true;
            this.gridColLuongBac.VisibleIndex = 2;
            // 
            // gridColLuongHeSo
            // 
            this.gridColLuongHeSo.Caption = "Hệ số";
            this.gridColLuongHeSo.DisplayFormat.FormatString = "##0.#0";
            this.gridColLuongHeSo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColLuongHeSo.FieldName = "HeSoLuong";
            this.gridColLuongHeSo.Name = "gridColLuongHeSo";
            this.gridColLuongHeSo.OptionsColumn.AllowEdit = false;
            this.gridColLuongHeSo.OptionsColumn.ReadOnly = true;
            this.gridColLuongHeSo.Visible = true;
            this.gridColLuongHeSo.VisibleIndex = 3;
            // 
            // gridColNamDau
            // 
            this.gridColNamDau.Caption = "Tập sự";
            this.gridColNamDau.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColNamDau.FieldName = "HeSoNamDau";
            this.gridColNamDau.Name = "gridColNamDau";
            this.gridColNamDau.OptionsColumn.AllowEdit = false;
            this.gridColNamDau.OptionsColumn.ReadOnly = true;
            this.gridColNamDau.Visible = true;
            this.gridColNamDau.VisibleIndex = 4;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = "85";
            this.repositoryItemCheckEdit1.ValueUnchecked = "0";
            // 
            // gridColLuongCB
            // 
            this.gridColLuongCB.Caption = "Lương cơ bản";
            this.gridColLuongCB.FieldName = "LuongCB";
            this.gridColLuongCB.Name = "gridColLuongCB";
            this.gridColLuongCB.OptionsColumn.AllowEdit = false;
            this.gridColLuongCB.OptionsColumn.ReadOnly = true;
            this.gridColLuongCB.Visible = true;
            this.gridColLuongCB.VisibleIndex = 5;
            // 
            // gridColLuongChinh
            // 
            this.gridColLuongChinh.Caption = "Lương chính";
            this.gridColLuongChinh.FieldName = "LuongChinh";
            this.gridColLuongChinh.Name = "gridColLuongChinh";
            this.gridColLuongChinh.OptionsColumn.AllowEdit = false;
            this.gridColLuongChinh.OptionsColumn.ReadOnly = true;
            this.gridColLuongChinh.Visible = true;
            this.gridColLuongChinh.VisibleIndex = 6;
            // 
            // gridColPhuCap
            // 
            this.gridColPhuCap.Caption = "Phụ cấp";
            this.gridColPhuCap.DisplayFormat.FormatString = "##0.#0";
            this.gridColPhuCap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColPhuCap.FieldName = "PhuCap";
            this.gridColPhuCap.Name = "gridColPhuCap";
            this.gridColPhuCap.OptionsColumn.AllowEdit = false;
            this.gridColPhuCap.OptionsColumn.ReadOnly = true;
            this.gridColPhuCap.Visible = true;
            this.gridColPhuCap.VisibleIndex = 8;
            // 
            // gridColLuongPCCV
            // 
            this.gridColLuongPCCV.Caption = "Phụ cấp CV";
            this.gridColLuongPCCV.FieldName = "PCChucVu";
            this.gridColLuongPCCV.Name = "gridColLuongPCCV";
            this.gridColLuongPCCV.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCCV.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCCV.Visible = true;
            this.gridColLuongPCCV.VisibleIndex = 7;
            // 
            // gridColLuongPCTN
            // 
            this.gridColLuongPCTN.Caption = "Phụ cấp thâm niên";
            this.gridColLuongPCTN.FieldName = "PCThamNien";
            this.gridColLuongPCTN.Name = "gridColLuongPCTN";
            this.gridColLuongPCTN.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCTN.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCTN.Visible = true;
            this.gridColLuongPCTN.VisibleIndex = 9;
            // 
            // gridColLuongVuot
            // 
            this.gridColLuongVuot.Caption = "Tỉ lệ vượt khung";
            this.gridColLuongVuot.FieldName = "VuotKhung";
            this.gridColLuongVuot.Name = "gridColLuongVuot";
            this.gridColLuongVuot.OptionsColumn.AllowEdit = false;
            this.gridColLuongVuot.OptionsColumn.ReadOnly = true;
            this.gridColLuongVuot.Visible = true;
            this.gridColLuongVuot.VisibleIndex = 10;
            // 
            // gridColLuongBHXH
            // 
            this.gridColLuongBHXH.Caption = "Đóng BHXH";
            this.gridColLuongBHXH.FieldName = "LuongBHXH";
            this.gridColLuongBHXH.Name = "gridColLuongBHXH";
            this.gridColLuongBHXH.OptionsColumn.AllowEdit = false;
            this.gridColLuongBHXH.OptionsColumn.ReadOnly = true;
            this.gridColLuongBHXH.Visible = true;
            this.gridColLuongBHXH.VisibleIndex = 11;
            // 
            // gridColLuongNgayhuong
            // 
            this.gridColLuongNgayhuong.Caption = "Ngày hưởng";
            this.gridColLuongNgayhuong.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColLuongNgayhuong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColLuongNgayhuong.FieldName = "NgayHuong";
            this.gridColLuongNgayhuong.Name = "gridColLuongNgayhuong";
            this.gridColLuongNgayhuong.OptionsColumn.AllowEdit = false;
            this.gridColLuongNgayhuong.OptionsColumn.ReadOnly = true;
            this.gridColLuongNgayhuong.Visible = true;
            this.gridColLuongNgayhuong.VisibleIndex = 12;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // ucNVLuong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.gridLuong);
            this.Controls.Add(this.gcLuong);
            this.Name = "ucNVLuong";
            this.Size = new System.Drawing.Size(1200, 510);
            this.Load += new System.EventHandler(this.ucNVLuong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcLuong)).EndInit();
            this.gcLuong.ResumeLayout(false);
            this.gcLuong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNamDau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayhuong.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayhuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDongBHXH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTileVuot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCThamnien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCChucvu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongChinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuongCB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcLuong;
        private TextEdit txtHeSo;
        private LabelControl labelControl86;
        private LabelControl labelControl85;
        private LabelControl labelControl84;
        private TextEdit txtSoQD;
        private LabelControl labelControl83;
        private TextEdit txtPCThamnien;
        private LabelControl labelControl90;
        private TextEdit txtPCChucvu;
        private LabelControl labelControl89;
        private TextEdit txtLuongChinh;
        private LabelControl labelControl88;
        private TextEdit txtLuongCB;
        private LabelControl labelControl87;
        private DateEdit deNgayhuong;
        private LabelControl labelControl93;
        private TextEdit txtDongBHXH;
        private LabelControl labelControl92;
        private TextEdit txtTileVuot;
        private LabelControl labelControl91;
        private GridControl gridLuong;
        private GridView gridViewLuong;
        private GridColumn gridColLuongID;
        private GridColumn gridColLuongSQD;
        private GridColumn gridColLuongNgach;
        private GridColumn gridColLuongBac;
        private GridColumn gridColLuongHeSo;
        private GridColumn gridColLuongCB;
        private GridColumn gridColLuongChinh;
        private GridColumn gridColLuongPCCV;
        private GridColumn gridColLuongPCTN;
        private GridColumn gridColLuongVuot;
        private GridColumn gridColLuongBHXH;
        private GridColumn gridColLuongNgayhuong;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
        private LookUpEdit cbbPg1MaNgach;
        private LookUpEdit cbbPg1Bac;
        private CheckEdit chbNamDau;
        private GridColumn gridColNamDau;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridColumn gridColPhuCap;
        private SimpleButton btnPg3Sua;
        private TextEdit txtPhuCap;
        private LabelControl labelControl1;
        private LabelControl labelControl2;
    }
}
