﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucTrangThaiLamViec : XtraUserControl
    {
        TrangThaiLamViecCTL ttlvCTL = new TrangThaiLamViecCTL();
        List<TrangThaiLamViecDTO> listTTLV = new List<TrangThaiLamViecDTO>();
        TrangThaiLamViecDTO ttlvSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucTrangThaiLamViec()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            listTTLV = ttlvCTL.GetAll();

            gcTranThaiLV.DataSource = listTTLV;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(TrangThaiLamViecDTO ttlvDTO)
        {
            if (ttlvDTO != null)
            {
                txtTrangThai.Text = ttlvDTO.TenTrangThai;
                meGhiChu.Text = ttlvDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtTrangThai.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtTrangThai.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtTrangThai.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(ttlvSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtTrangThai.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(ttlvSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtTrangThai.Text == string.Empty)
                return false;

            return true;
        }

        private TrangThaiLamViecDTO GetData(TrangThaiLamViecDTO ttlvDTO)
        {
            string maTTLV = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexTTLV = ttlvCTL.GetMaxMaTrangThai();
                if (sLastIndexTTLV.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTTLV = int.Parse(sLastIndexTTLV);
                    maTTLV += (nLastIndexTTLV + 1).ToString().PadLeft(2, '0');
                }
                else
                    maTTLV += "01";

                ttlvDTO.NgayHieuLuc = DateTime.Now;
                ttlvDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maTTLV = ttlvSelect.MaTrangThai;
                ttlvDTO.NgayHieuLuc = ttlvSelect.NgayHieuLuc;
                ttlvDTO.TinhTrang = ttlvSelect.TinhTrang;
            }
            ttlvDTO.MaTrangThai = maTTLV;
            ttlvDTO.TenTrangThai = txtTrangThai.Text;
            ttlvDTO.GhiChu = meGhiChu.Text;

            return ttlvDTO;
        }

        private void gridViewTrangThaiLV_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listTTLV.Count)
            {
                ttlvSelect = ((GridView)sender).GetRow(_selectedIndex) as TrangThaiLamViecDTO;
            }
            else
                if (listTTLV.Count != 0)
                    ttlvSelect = listTTLV[0];
                else
                    ttlvSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên trạng thái", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TrangThaiLamViecDTO ttlvNew = GetData(new TrangThaiLamViecDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = ttlvCTL.Save(ttlvNew);
            else
                isSucess = ttlvCTL.Update(ttlvNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listTTLV.IndexOf(listTTLV.FirstOrDefault(p => p.MaTrangThai == ttlvNew.MaTrangThai));
                gridViewTrangThaiLV.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (ttlvSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa trạng thái làm việc " + ttlvSelect.TenTrangThai + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = ttlvCTL.Delete(ttlvSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listTTLV.Count != 0)
                            gridViewTrangThaiLV.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void ucTrangThaiLamViec_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }
    }
}
