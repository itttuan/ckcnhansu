﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class frmThemTruong : DevExpress.XtraEditors.XtraForm
    {
        ucNVTrinhDo ucTrinhdo;
        public frmThemTruong()
        {
            InitializeComponent();
        }
        public frmThemTruong(ucNVTrinhDo trinhdoUC)
        {
            InitializeComponent();
            ucTruongHoc1.truonghocSender = ReceiveInfo;
            ucTrinhdo = trinhdoUC;
        }

        public void ReceiveInfo(bool isSuccess)
        {
            if (isSuccess)
            {
                Program.listTruong = (new TruongCTL()).GetAll();
                if (ucTrinhdo != null)
                {
                    ucTrinhdo.LoadDanhSachTruong();
                }
            }
        }
    }
}