﻿namespace HerculesHRMT
{
    partial class frmThemTruong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucTruongHoc1 = new HerculesHRMT.ucTruongHoc();
            this.SuspendLayout();
            // 
            // ucTruongHoc1
            // 
            this.ucTruongHoc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTruongHoc1.Location = new System.Drawing.Point(0, 0);
            this.ucTruongHoc1.Name = "ucTruongHoc1";
            this.ucTruongHoc1.Size = new System.Drawing.Size(783, 381);
            this.ucTruongHoc1.TabIndex = 0;
            // 
            // frmThemTruong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 381);
            this.Controls.Add(this.ucTruongHoc1);
            this.Name = "frmThemTruong";
            this.Text = "Trường học - Học viện";
            this.ResumeLayout(false);

        }

        #endregion

        private ucTruongHoc ucTruongHoc1;
    }
}