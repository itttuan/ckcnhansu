﻿namespace HerculesHRMT
{
    partial class ucNVSoLuocLyLich
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnHieuChinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnXemCacPhienBan = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.cbbPg1QuanHuyenHT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1EmailTail = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1TinhThanhHT = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TinhThanhTT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.cbbPg1QuanHuyenTT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1Email = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DiaChiTT = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1DiaChiHT = new DevExpress.XtraEditors.TextEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.txtPg1QuanHamCaoNhat = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chbPg1DangVien = new DevExpress.XtraEditors.CheckEdit();
            this.chbPg1QuanNgu = new DevExpress.XtraEditors.CheckEdit();
            this.chbPg1ThuongBinh = new DevExpress.XtraEditors.CheckEdit();
            this.chbPg1GiaDinhChinhSach = new DevExpress.XtraEditors.CheckEdit();
            this.dtPg1NgayXuatNgu = new DevExpress.XtraEditors.DateEdit();
            this.cbbPg1NhomMau = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtPg1NgayChinhThucDCSVN = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgayNhapNgu = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgayVaoDCSVN = new DevExpress.XtraEditors.DateEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1ThuongBinhHangSecond = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1ThuongBinhHangFirst = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1CanNang = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1ChieuCao = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1TinhTrangSucKhoe = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cbbPg1Bac = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1MaNgach = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1Hang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1PhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1BMBP = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1ChuyenMon = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1ChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TinhTrangLamViec = new DevExpress.XtraEditors.LookUpEdit();
            this.dtPg1NgayVaoLam = new DevExpress.XtraEditors.DateEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtPg1QueQuan = new DevExpress.XtraEditors.TextEdit();
            this.txtPg1NoiSinh = new DevExpress.XtraEditors.TextEdit();
            this.cbbPg1QuocTich = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1VanHoa = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1TinhTrangHonNhan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbPg1NoiCapCMND = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1TonGiao = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1DanToc = new DevExpress.XtraEditors.LookUpEdit();
            this.dtPg1NgayCapCMND = new DevExpress.XtraEditors.DateEdit();
            this.dtPg1NgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.rdbPg1GioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1Ten = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1SoCMND = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1TenKhac = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg1HoDem = new DevExpress.XtraEditors.TextEdit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1EmailTail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiHT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QuanHamCaoNhat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1DangVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1QuanNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1ThuongBinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1GiaDinhChinhSach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NhomMau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangSecond.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangFirst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1CanNang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ChieuCao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TinhTrangSucKhoe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Hang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1PhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1BMBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChuyenMon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangLamViec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QueQuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1NoiSinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuocTich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1VanHoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangHonNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiCapCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TonGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1DanToc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Ten.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1SoCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TenKhac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.panelControl2);
            this.xtraScrollableControl1.Controls.Add(this.groupControl4);
            this.xtraScrollableControl1.Controls.Add(this.groupControl5);
            this.xtraScrollableControl1.Controls.Add(this.groupControl3);
            this.xtraScrollableControl1.Controls.Add(this.groupControl2);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1200, 510);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.btnHieuChinh);
            this.panelControl2.Controls.Add(this.btnReset);
            this.panelControl2.Controls.Add(this.btnXemCacPhienBan);
            this.panelControl2.Controls.Add(this.btnLuu);
            this.panelControl2.Location = new System.Drawing.Point(6, 439);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1191, 33);
            this.panelControl2.TabIndex = 5;
            // 
            // btnHieuChinh
            // 
            this.btnHieuChinh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHieuChinh.Location = new System.Drawing.Point(853, 5);
            this.btnHieuChinh.Name = "btnHieuChinh";
            this.btnHieuChinh.Size = new System.Drawing.Size(75, 23);
            this.btnHieuChinh.TabIndex = 45;
            this.btnHieuChinh.Text = "Hiệu chỉnh";
            this.btnHieuChinh.Click += new System.EventHandler(this.btnHieuChinh_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(954, 5);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(97, 23);
            this.btnReset.TabIndex = 46;
            this.btnReset.Text = "Nhập lại";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnXemCacPhienBan
            // 
            this.btnXemCacPhienBan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemCacPhienBan.Location = new System.Drawing.Point(702, 5);
            this.btnXemCacPhienBan.Name = "btnXemCacPhienBan";
            this.btnXemCacPhienBan.Size = new System.Drawing.Size(125, 23);
            this.btnXemCacPhienBan.TabIndex = 44;
            this.btnXemCacPhienBan.Text = "Những Lần thay đổi";
            this.btnXemCacPhienBan.Click += new System.EventHandler(this.btnXemCacPhienBan_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Location = new System.Drawing.Point(1077, 5);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(97, 23);
            this.btnLuu.TabIndex = 47;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.cbbPg1QuanHuyenHT);
            this.groupControl4.Controls.Add(this.cbbPg1EmailTail);
            this.groupControl4.Controls.Add(this.cbbPg1TinhThanhHT);
            this.groupControl4.Controls.Add(this.cbbPg1TinhThanhTT);
            this.groupControl4.Controls.Add(this.labelControl25);
            this.groupControl4.Controls.Add(this.labelControl30);
            this.groupControl4.Controls.Add(this.cbbPg1QuanHuyenTT);
            this.groupControl4.Controls.Add(this.labelControl33);
            this.groupControl4.Controls.Add(this.labelControl27);
            this.groupControl4.Controls.Add(this.labelControl28);
            this.groupControl4.Controls.Add(this.labelControl24);
            this.groupControl4.Controls.Add(this.labelControl35);
            this.groupControl4.Controls.Add(this.labelControl36);
            this.groupControl4.Controls.Add(this.txtPg1Email);
            this.groupControl4.Controls.Add(this.txtPg1DienThoai);
            this.groupControl4.Controls.Add(this.txtPg1DiaChiTT);
            this.groupControl4.Controls.Add(this.txtPg1DiaChiHT);
            this.groupControl4.Location = new System.Drawing.Point(5, 198);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1192, 101);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "Thông tin liên lạc";
            // 
            // cbbPg1QuanHuyenHT
            // 
            this.cbbPg1QuanHuyenHT.Location = new System.Drawing.Point(560, 73);
            this.cbbPg1QuanHuyenHT.Name = "cbbPg1QuanHuyenHT";
            this.cbbPg1QuanHuyenHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuanHuyenHT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuanHuyenHT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuanHuyenHT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuanHuyen", "Mã quận huyện", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuanHuyen", "Tên quận huyện")});
            this.cbbPg1QuanHuyenHT.Properties.DisplayMember = "TenQuanHuyen";
            this.cbbPg1QuanHuyenHT.Properties.NullText = "";
            this.cbbPg1QuanHuyenHT.Properties.ValueMember = "MaQuanHuyen";
            this.cbbPg1QuanHuyenHT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1QuanHuyenHT.TabIndex = 30;
            // 
            // cbbPg1EmailTail
            // 
            this.cbbPg1EmailTail.EditValue = "gmail.com";
            this.cbbPg1EmailTail.Location = new System.Drawing.Point(929, 73);
            this.cbbPg1EmailTail.Name = "cbbPg1EmailTail";
            this.cbbPg1EmailTail.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1EmailTail.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1EmailTail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1EmailTail.Properties.Items.AddRange(new object[] {
            "gmail.com",
            "yahoo.com",
            "yahoo.com.vn",
            "outlook.com",
            "live.com",
            "facebook.com",
            "hotmail.com",
            "yopmail.com",
            "ymail.com",
            "rocketmail.com",
            "msn.com",
            "zing.vn",
            "ovi.com"});
            this.cbbPg1EmailTail.Size = new System.Drawing.Size(106, 22);
            this.cbbPg1EmailTail.TabIndex = 5;
            // 
            // cbbPg1TinhThanhHT
            // 
            this.cbbPg1TinhThanhHT.Location = new System.Drawing.Point(409, 73);
            this.cbbPg1TinhThanhHT.Name = "cbbPg1TinhThanhHT";
            this.cbbPg1TinhThanhHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhThanhHT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhThanhHT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhThanhHT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1TinhThanhHT.Properties.DisplayMember = "TenTinhThanh";
            this.cbbPg1TinhThanhHT.Properties.NullText = "";
            this.cbbPg1TinhThanhHT.Properties.ValueMember = "MaTinhThanh";
            this.cbbPg1TinhThanhHT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1TinhThanhHT.TabIndex = 29;
            this.cbbPg1TinhThanhHT.EditValueChanged += new System.EventHandler(this.cbbPg1TinhThanhHT_EditValueChanged);
            // 
            // cbbPg1TinhThanhTT
            // 
            this.cbbPg1TinhThanhTT.Location = new System.Drawing.Point(409, 45);
            this.cbbPg1TinhThanhTT.Name = "cbbPg1TinhThanhTT";
            this.cbbPg1TinhThanhTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhThanhTT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhThanhTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhThanhTT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1TinhThanhTT.Properties.DisplayMember = "TenTinhThanh";
            this.cbbPg1TinhThanhTT.Properties.NullText = "";
            this.cbbPg1TinhThanhTT.Properties.ValueMember = "MaTinhThanh";
            this.cbbPg1TinhThanhTT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1TinhThanhTT.TabIndex = 25;
            this.cbbPg1TinhThanhTT.EditValueChanged += new System.EventHandler(this.cbbPg1TinhThanhTT_EditValueChanged);
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(409, 24);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(62, 15);
            this.labelControl25.TabIndex = 0;
            this.labelControl25.Text = "Tỉnh/Thành";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(106, 24);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(39, 15);
            this.labelControl30.TabIndex = 0;
            this.labelControl30.Text = "Địa chỉ";
            // 
            // cbbPg1QuanHuyenTT
            // 
            this.cbbPg1QuanHuyenTT.Location = new System.Drawing.Point(560, 45);
            this.cbbPg1QuanHuyenTT.Name = "cbbPg1QuanHuyenTT";
            this.cbbPg1QuanHuyenTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuanHuyenTT.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuanHuyenTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuanHuyenTT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuanHuyen", "Mã quận huyện", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuanHuyen", "Tên quận huyện")});
            this.cbbPg1QuanHuyenTT.Properties.DisplayMember = "TenQuanHuyen";
            this.cbbPg1QuanHuyenTT.Properties.NullText = "";
            this.cbbPg1QuanHuyenTT.Properties.ValueMember = "MaQuanHuyen";
            this.cbbPg1QuanHuyenTT.Size = new System.Drawing.Size(145, 22);
            this.cbbPg1QuanHuyenTT.TabIndex = 26;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(913, 76);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(12, 15);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "@";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(711, 76);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(31, 15);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "Email";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(560, 24);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(68, 15);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Quận/Huyện";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(711, 48);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(57, 15);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "Điện thoại";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(6, 76);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(94, 15);
            this.labelControl35.TabIndex = 0;
            this.labelControl35.Text = "Chổ ở hiện tại (*)";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(7, 48);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(81, 15);
            this.labelControl36.TabIndex = 0;
            this.labelControl36.Text = "Thường trú (*)";
            // 
            // txtPg1Email
            // 
            this.txtPg1Email.Location = new System.Drawing.Point(774, 73);
            this.txtPg1Email.Name = "txtPg1Email";
            this.txtPg1Email.Properties.AllowFocused = false;
            this.txtPg1Email.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1Email.Properties.Appearance.Options.UseFont = true;
            this.txtPg1Email.Size = new System.Drawing.Size(136, 22);
            this.txtPg1Email.TabIndex = 31;
            // 
            // txtPg1DienThoai
            // 
            this.txtPg1DienThoai.Location = new System.Drawing.Point(774, 45);
            this.txtPg1DienThoai.Name = "txtPg1DienThoai";
            this.txtPg1DienThoai.Properties.AllowFocused = false;
            this.txtPg1DienThoai.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DienThoai.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DienThoai.Size = new System.Drawing.Size(136, 22);
            this.txtPg1DienThoai.TabIndex = 27;
            // 
            // txtPg1DiaChiTT
            // 
            this.txtPg1DiaChiTT.EditValue = "";
            this.txtPg1DiaChiTT.Location = new System.Drawing.Point(106, 45);
            this.txtPg1DiaChiTT.Name = "txtPg1DiaChiTT";
            this.txtPg1DiaChiTT.Properties.AllowFocused = false;
            this.txtPg1DiaChiTT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DiaChiTT.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DiaChiTT.Size = new System.Drawing.Size(299, 22);
            this.txtPg1DiaChiTT.TabIndex = 24;
            // 
            // txtPg1DiaChiHT
            // 
            this.txtPg1DiaChiHT.EditValue = "";
            this.txtPg1DiaChiHT.Location = new System.Drawing.Point(106, 73);
            this.txtPg1DiaChiHT.Name = "txtPg1DiaChiHT";
            this.txtPg1DiaChiHT.Properties.AllowFocused = false;
            this.txtPg1DiaChiHT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1DiaChiHT.Properties.Appearance.Options.UseFont = true;
            this.txtPg1DiaChiHT.Size = new System.Drawing.Size(299, 22);
            this.txtPg1DiaChiHT.TabIndex = 28;
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.txtPg1QuanHamCaoNhat);
            this.groupControl5.Controls.Add(this.chbPg1DangVien);
            this.groupControl5.Controls.Add(this.chbPg1QuanNgu);
            this.groupControl5.Controls.Add(this.chbPg1ThuongBinh);
            this.groupControl5.Controls.Add(this.chbPg1GiaDinhChinhSach);
            this.groupControl5.Controls.Add(this.dtPg1NgayXuatNgu);
            this.groupControl5.Controls.Add(this.cbbPg1NhomMau);
            this.groupControl5.Controls.Add(this.dtPg1NgayChinhThucDCSVN);
            this.groupControl5.Controls.Add(this.dtPg1NgayNhapNgu);
            this.groupControl5.Controls.Add(this.dtPg1NgayVaoDCSVN);
            this.groupControl5.Controls.Add(this.labelControl34);
            this.groupControl5.Controls.Add(this.labelControl45);
            this.groupControl5.Controls.Add(this.labelControl47);
            this.groupControl5.Controls.Add(this.labelControl46);
            this.groupControl5.Controls.Add(this.labelControl40);
            this.groupControl5.Controls.Add(this.labelControl42);
            this.groupControl5.Controls.Add(this.labelControl84);
            this.groupControl5.Controls.Add(this.labelControl87);
            this.groupControl5.Controls.Add(this.labelControl86);
            this.groupControl5.Controls.Add(this.labelControl85);
            this.groupControl5.Controls.Add(this.labelControl83);
            this.groupControl5.Controls.Add(this.labelControl43);
            this.groupControl5.Controls.Add(this.labelControl44);
            this.groupControl5.Controls.Add(this.txtPg1ThuongBinhHangSecond);
            this.groupControl5.Controls.Add(this.txtPg1ThuongBinhHangFirst);
            this.groupControl5.Controls.Add(this.txtPg1CanNang);
            this.groupControl5.Controls.Add(this.txtPg1ChieuCao);
            this.groupControl5.Controls.Add(this.txtPg1TinhTrangSucKhoe);
            this.groupControl5.Location = new System.Drawing.Point(6, 301);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(1191, 137);
            this.groupControl5.TabIndex = 2;
            this.groupControl5.Text = "Thông tin khác";
            // 
            // txtPg1QuanHamCaoNhat
            // 
            this.txtPg1QuanHamCaoNhat.Location = new System.Drawing.Point(686, 53);
            this.txtPg1QuanHamCaoNhat.Name = "txtPg1QuanHamCaoNhat";
            this.txtPg1QuanHamCaoNhat.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1QuanHamCaoNhat.Properties.Appearance.Options.UseFont = true;
            this.txtPg1QuanHamCaoNhat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtPg1QuanHamCaoNhat.Properties.Items.AddRange(new object[] {
            "",
            "Binh nhì",
            "Binh nhất",
            "Hạ sĩ",
            "Trung sĩ",
            "Thượng sĩ",
            "Thiếu úy",
            "Trung úy",
            "Thượng úy",
            "Đại úy",
            "Thiếu tá",
            "Trung tá",
            "Thượng tá",
            "Đại tá",
            "Thiếu tướng",
            "Trung tướng",
            "Thượng tướng",
            "Đại tướng"});
            this.txtPg1QuanHamCaoNhat.Properties.NullText = "[Chọn quân hàm cao nhất]";
            this.txtPg1QuanHamCaoNhat.Size = new System.Drawing.Size(223, 22);
            this.txtPg1QuanHamCaoNhat.TabIndex = 47;
            // 
            // chbPg1DangVien
            // 
            this.chbPg1DangVien.Location = new System.Drawing.Point(10, 25);
            this.chbPg1DangVien.Name = "chbPg1DangVien";
            this.chbPg1DangVien.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbPg1DangVien.Properties.Appearance.Options.UseFont = true;
            this.chbPg1DangVien.Properties.Caption = "Là Đảng viên";
            this.chbPg1DangVien.Size = new System.Drawing.Size(134, 20);
            this.chbPg1DangVien.TabIndex = 46;
            this.chbPg1DangVien.CheckedChanged += new System.EventHandler(this.chbPg1DangVien_CheckedChanged);
            // 
            // chbPg1QuanNgu
            // 
            this.chbPg1QuanNgu.Location = new System.Drawing.Point(10, 53);
            this.chbPg1QuanNgu.Name = "chbPg1QuanNgu";
            this.chbPg1QuanNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbPg1QuanNgu.Properties.Appearance.Options.UseFont = true;
            this.chbPg1QuanNgu.Properties.Caption = "Tham gia quân ngũ";
            this.chbPg1QuanNgu.Size = new System.Drawing.Size(134, 20);
            this.chbPg1QuanNgu.TabIndex = 45;
            this.chbPg1QuanNgu.CheckedChanged += new System.EventHandler(this.chbPg1QuanNgu_CheckedChanged);
            // 
            // chbPg1ThuongBinh
            // 
            this.chbPg1ThuongBinh.Location = new System.Drawing.Point(10, 83);
            this.chbPg1ThuongBinh.Name = "chbPg1ThuongBinh";
            this.chbPg1ThuongBinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbPg1ThuongBinh.Properties.Appearance.Options.UseFont = true;
            this.chbPg1ThuongBinh.Properties.Caption = "Là thương binh";
            this.chbPg1ThuongBinh.Size = new System.Drawing.Size(120, 20);
            this.chbPg1ThuongBinh.TabIndex = 44;
            this.chbPg1ThuongBinh.CheckedChanged += new System.EventHandler(this.chbPg1ThuongBinh_CheckedChanged);
            // 
            // chbPg1GiaDinhChinhSach
            // 
            this.chbPg1GiaDinhChinhSach.Location = new System.Drawing.Point(477, 80);
            this.chbPg1GiaDinhChinhSach.Name = "chbPg1GiaDinhChinhSach";
            this.chbPg1GiaDinhChinhSach.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbPg1GiaDinhChinhSach.Properties.Appearance.Options.UseFont = true;
            this.chbPg1GiaDinhChinhSach.Properties.Caption = "Là con gia đình chính sách";
            this.chbPg1GiaDinhChinhSach.Size = new System.Drawing.Size(201, 20);
            this.chbPg1GiaDinhChinhSach.TabIndex = 43;
            // 
            // dtPg1NgayXuatNgu
            // 
            this.dtPg1NgayXuatNgu.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayXuatNgu.Location = new System.Drawing.Point(479, 52);
            this.dtPg1NgayXuatNgu.Name = "dtPg1NgayXuatNgu";
            this.dtPg1NgayXuatNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayXuatNgu.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayXuatNgu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayXuatNgu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayXuatNgu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayXuatNgu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayXuatNgu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayXuatNgu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayXuatNgu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayXuatNgu.Size = new System.Drawing.Size(85, 22);
            this.dtPg1NgayXuatNgu.TabIndex = 35;
            this.dtPg1NgayXuatNgu.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // cbbPg1NhomMau
            // 
            this.cbbPg1NhomMau.EditValue = "O";
            this.cbbPg1NhomMau.Location = new System.Drawing.Point(719, 110);
            this.cbbPg1NhomMau.Name = "cbbPg1NhomMau";
            this.cbbPg1NhomMau.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1NhomMau.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1NhomMau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1NhomMau.Properties.Items.AddRange(new object[] {
            "O",
            "A",
            "B",
            "AB"});
            this.cbbPg1NhomMau.Size = new System.Drawing.Size(55, 22);
            this.cbbPg1NhomMau.TabIndex = 40;
            // 
            // dtPg1NgayChinhThucDCSVN
            // 
            this.dtPg1NgayChinhThucDCSVN.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayChinhThucDCSVN.Location = new System.Drawing.Point(479, 24);
            this.dtPg1NgayChinhThucDCSVN.Name = "dtPg1NgayChinhThucDCSVN";
            this.dtPg1NgayChinhThucDCSVN.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayChinhThucDCSVN.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayChinhThucDCSVN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayChinhThucDCSVN.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayChinhThucDCSVN.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayChinhThucDCSVN.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayChinhThucDCSVN.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayChinhThucDCSVN.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayChinhThucDCSVN.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayChinhThucDCSVN.Size = new System.Drawing.Size(85, 22);
            this.dtPg1NgayChinhThucDCSVN.TabIndex = 33;
            this.dtPg1NgayChinhThucDCSVN.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // dtPg1NgayNhapNgu
            // 
            this.dtPg1NgayNhapNgu.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayNhapNgu.Location = new System.Drawing.Point(290, 52);
            this.dtPg1NgayNhapNgu.Name = "dtPg1NgayNhapNgu";
            this.dtPg1NgayNhapNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayNhapNgu.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayNhapNgu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayNhapNgu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayNhapNgu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayNhapNgu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayNhapNgu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayNhapNgu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayNhapNgu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayNhapNgu.Size = new System.Drawing.Size(85, 22);
            this.dtPg1NgayNhapNgu.TabIndex = 34;
            this.dtPg1NgayNhapNgu.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // dtPg1NgayVaoDCSVN
            // 
            this.dtPg1NgayVaoDCSVN.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayVaoDCSVN.Location = new System.Drawing.Point(290, 24);
            this.dtPg1NgayVaoDCSVN.Name = "dtPg1NgayVaoDCSVN";
            this.dtPg1NgayVaoDCSVN.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoDCSVN.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoDCSVN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoDCSVN.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoDCSVN.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoDCSVN.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoDCSVN.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoDCSVN.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayVaoDCSVN.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoDCSVN.Size = new System.Drawing.Size(85, 22);
            this.dtPg1NgayVaoDCSVN.TabIndex = 32;
            this.dtPg1NgayVaoDCSVN.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(270, 113);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(56, 15);
            this.labelControl34.TabIndex = 0;
            this.labelControl34.Text = "Chiều cao";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Location = new System.Drawing.Point(330, 84);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(3, 15);
            this.labelControl45.TabIndex = 0;
            this.labelControl45.Text = "/";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Location = new System.Drawing.Point(630, 113);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(17, 15);
            this.labelControl47.TabIndex = 0;
            this.labelControl47.Text = "kg,";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Location = new System.Drawing.Point(423, 113);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(21, 15);
            this.labelControl46.TabIndex = 0;
            this.labelControl46.Text = "cm,";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(652, 113);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(61, 15);
            this.labelControl40.TabIndex = 0;
            this.labelControl40.Text = "Nhóm máu";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(480, 113);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(53, 15);
            this.labelControl42.TabIndex = 0;
            this.labelControl42.Text = "Cân nặng";
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl84.Location = new System.Drawing.Point(381, 27);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(92, 15);
            this.labelControl84.TabIndex = 0;
            this.labelControl84.Text = "Ngày chính thức";
            // 
            // labelControl87
            // 
            this.labelControl87.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl87.Location = new System.Drawing.Point(570, 55);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(110, 15);
            this.labelControl87.TabIndex = 0;
            this.labelControl87.Text = "Quân hàm cao nhất";
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl86.Location = new System.Drawing.Point(382, 55);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(80, 15);
            this.labelControl86.TabIndex = 0;
            this.labelControl86.Text = "Ngày xuất ngũ";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl85.Location = new System.Drawing.Point(154, 55);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(83, 15);
            this.labelControl85.TabIndex = 0;
            this.labelControl85.Text = "Ngày nhập ngũ";
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl83.Location = new System.Drawing.Point(154, 27);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(133, 15);
            this.labelControl83.TabIndex = 0;
            this.labelControl83.Text = "Ngày vào ĐCS Việt Nam";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Location = new System.Drawing.Point(154, 84);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(104, 15);
            this.labelControl43.TabIndex = 0;
            this.labelControl43.Text = "Thương binh hạng";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Location = new System.Drawing.Point(5, 113);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(114, 15);
            this.labelControl44.TabIndex = 0;
            this.labelControl44.Text = "Tình trạng sức khỏe";
            // 
            // txtPg1ThuongBinhHangSecond
            // 
            this.txtPg1ThuongBinhHangSecond.Location = new System.Drawing.Point(340, 82);
            this.txtPg1ThuongBinhHangSecond.Name = "txtPg1ThuongBinhHangSecond";
            this.txtPg1ThuongBinhHangSecond.Properties.AllowFocused = false;
            this.txtPg1ThuongBinhHangSecond.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ThuongBinhHangSecond.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ThuongBinhHangSecond.Size = new System.Drawing.Size(35, 22);
            this.txtPg1ThuongBinhHangSecond.TabIndex = 42;
            // 
            // txtPg1ThuongBinhHangFirst
            // 
            this.txtPg1ThuongBinhHangFirst.Location = new System.Drawing.Point(288, 81);
            this.txtPg1ThuongBinhHangFirst.Name = "txtPg1ThuongBinhHangFirst";
            this.txtPg1ThuongBinhHangFirst.Properties.AllowFocused = false;
            this.txtPg1ThuongBinhHangFirst.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ThuongBinhHangFirst.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ThuongBinhHangFirst.Size = new System.Drawing.Size(35, 22);
            this.txtPg1ThuongBinhHangFirst.TabIndex = 41;
            // 
            // txtPg1CanNang
            // 
            this.txtPg1CanNang.Location = new System.Drawing.Point(539, 110);
            this.txtPg1CanNang.Name = "txtPg1CanNang";
            this.txtPg1CanNang.Properties.AllowFocused = false;
            this.txtPg1CanNang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1CanNang.Properties.Appearance.Options.UseFont = true;
            this.txtPg1CanNang.Size = new System.Drawing.Size(85, 22);
            this.txtPg1CanNang.TabIndex = 39;
            // 
            // txtPg1ChieuCao
            // 
            this.txtPg1ChieuCao.Location = new System.Drawing.Point(332, 110);
            this.txtPg1ChieuCao.Name = "txtPg1ChieuCao";
            this.txtPg1ChieuCao.Properties.AllowFocused = false;
            this.txtPg1ChieuCao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1ChieuCao.Properties.Appearance.Options.UseFont = true;
            this.txtPg1ChieuCao.Size = new System.Drawing.Size(85, 22);
            this.txtPg1ChieuCao.TabIndex = 38;
            // 
            // txtPg1TinhTrangSucKhoe
            // 
            this.txtPg1TinhTrangSucKhoe.Location = new System.Drawing.Point(143, 110);
            this.txtPg1TinhTrangSucKhoe.Name = "txtPg1TinhTrangSucKhoe";
            this.txtPg1TinhTrangSucKhoe.Properties.AllowFocused = false;
            this.txtPg1TinhTrangSucKhoe.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1TinhTrangSucKhoe.Properties.Appearance.Options.UseFont = true;
            this.txtPg1TinhTrangSucKhoe.Size = new System.Drawing.Size(85, 22);
            this.txtPg1TinhTrangSucKhoe.TabIndex = 37;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.cbbPg1Bac);
            this.groupControl3.Controls.Add(this.cbbPg1MaNgach);
            this.groupControl3.Controls.Add(this.cbbPg1Hang);
            this.groupControl3.Controls.Add(this.cbbPg1PhongKhoa);
            this.groupControl3.Controls.Add(this.cbbPg1BMBP);
            this.groupControl3.Controls.Add(this.cbbPg1ChuyenMon);
            this.groupControl3.Controls.Add(this.cbbPg1ChucVu);
            this.groupControl3.Controls.Add(this.cbbPg1TinhTrangLamViec);
            this.groupControl3.Controls.Add(this.dtPg1NgayVaoLam);
            this.groupControl3.Controls.Add(this.labelControl19);
            this.groupControl3.Controls.Add(this.labelControl23);
            this.groupControl3.Controls.Add(this.labelControl21);
            this.groupControl3.Controls.Add(this.labelControl20);
            this.groupControl3.Controls.Add(this.labelControl26);
            this.groupControl3.Controls.Add(this.labelControl22);
            this.groupControl3.Controls.Add(this.labelControl29);
            this.groupControl3.Controls.Add(this.labelControl31);
            this.groupControl3.Controls.Add(this.labelControl32);
            this.groupControl3.Location = new System.Drawing.Point(5, 114);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1192, 82);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Thông tin làm việc";
            // 
            // cbbPg1Bac
            // 
            this.cbbPg1Bac.Location = new System.Drawing.Point(889, 53);
            this.cbbPg1Bac.Name = "cbbPg1Bac";
            this.cbbPg1Bac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Bac.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Bac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Bac.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bac", "Bậc lương")});
            this.cbbPg1Bac.Properties.DisplayMember = "Bac";
            this.cbbPg1Bac.Properties.NullText = "";
            this.cbbPg1Bac.Properties.ValueMember = "Bac";
            this.cbbPg1Bac.Size = new System.Drawing.Size(75, 22);
            this.cbbPg1Bac.TabIndex = 25;
            // 
            // cbbPg1MaNgach
            // 
            this.cbbPg1MaNgach.Location = new System.Drawing.Point(541, 52);
            this.cbbPg1MaNgach.Name = "cbbPg1MaNgach";
            this.cbbPg1MaNgach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1MaNgach.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaNgach", "Mã ngạch")});
            this.cbbPg1MaNgach.Properties.DisplayMember = "MaNgach";
            this.cbbPg1MaNgach.Properties.NullText = "[Chọn mã ngạch]";
            this.cbbPg1MaNgach.Properties.ValueMember = "MaNgach";
            this.cbbPg1MaNgach.Size = new System.Drawing.Size(120, 20);
            this.cbbPg1MaNgach.TabIndex = 24;
            this.cbbPg1MaNgach.EditValueChanged += new System.EventHandler(this.cbbPg1MaNgach_EditValueChanged);
            // 
            // cbbPg1Hang
            // 
            this.cbbPg1Hang.EditValue = "I";
            this.cbbPg1Hang.Location = new System.Drawing.Point(732, 52);
            this.cbbPg1Hang.Name = "cbbPg1Hang";
            this.cbbPg1Hang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Hang.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Hang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Hang.Properties.Items.AddRange(new object[] {
            "I",
            "II",
            "III"});
            this.cbbPg1Hang.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1Hang.TabIndex = 22;
            // 
            // cbbPg1PhongKhoa
            // 
            this.cbbPg1PhongKhoa.EditValue = "";
            this.cbbPg1PhongKhoa.Location = new System.Drawing.Point(106, 24);
            this.cbbPg1PhongKhoa.Name = "cbbPg1PhongKhoa";
            this.cbbPg1PhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1PhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1PhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1PhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã phòng khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên phòng khoa")});
            this.cbbPg1PhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.cbbPg1PhongKhoa.Properties.NullText = "";
            this.cbbPg1PhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.cbbPg1PhongKhoa.Size = new System.Drawing.Size(161, 22);
            this.cbbPg1PhongKhoa.TabIndex = 15;
            // 
            // cbbPg1BMBP
            // 
            this.cbbPg1BMBP.EditValue = "";
            this.cbbPg1BMBP.Location = new System.Drawing.Point(106, 52);
            this.cbbPg1BMBP.Name = "cbbPg1BMBP";
            this.cbbPg1BMBP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1BMBP.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1BMBP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1BMBP.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã B.Môn B.Phận", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Tên bộ môn/bộ phận")});
            this.cbbPg1BMBP.Properties.DisplayMember = "TenBMBP";
            this.cbbPg1BMBP.Properties.NullText = "";
            this.cbbPg1BMBP.Properties.ValueMember = "MaBMBP";
            this.cbbPg1BMBP.Size = new System.Drawing.Size(161, 22);
            this.cbbPg1BMBP.TabIndex = 19;
            // 
            // cbbPg1ChuyenMon
            // 
            this.cbbPg1ChuyenMon.Location = new System.Drawing.Point(354, 52);
            this.cbbPg1ChuyenMon.Name = "cbbPg1ChuyenMon";
            this.cbbPg1ChuyenMon.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChuyenMon.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChuyenMon.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChuyenMon.Properties.NullText = "";
            this.cbbPg1ChuyenMon.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChuyenMon.TabIndex = 20;
            // 
            // cbbPg1ChucVu
            // 
            this.cbbPg1ChucVu.Location = new System.Drawing.Point(541, 24);
            this.cbbPg1ChucVu.Name = "cbbPg1ChucVu";
            this.cbbPg1ChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChucVu.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mã chức vụ", "MaChucVu", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenChucVu", "Tên chức vụ")});
            this.cbbPg1ChucVu.Properties.DisplayMember = "TenChucVu";
            this.cbbPg1ChucVu.Properties.NullText = "";
            this.cbbPg1ChucVu.Properties.ValueMember = "MaChucVu";
            this.cbbPg1ChucVu.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChucVu.TabIndex = 17;
            // 
            // cbbPg1TinhTrangLamViec
            // 
            this.cbbPg1TinhTrangLamViec.EditValue = "1";
            this.cbbPg1TinhTrangLamViec.Location = new System.Drawing.Point(732, 24);
            this.cbbPg1TinhTrangLamViec.Name = "cbbPg1TinhTrangLamViec";
            this.cbbPg1TinhTrangLamViec.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhTrangLamViec.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhTrangLamViec.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhTrangLamViec.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTrangThai", "Mã trạng thái", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTrangThai", "Tên trạng thái")});
            this.cbbPg1TinhTrangLamViec.Properties.NullText = "";
            this.cbbPg1TinhTrangLamViec.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1TinhTrangLamViec.TabIndex = 18;
            // 
            // dtPg1NgayVaoLam
            // 
            this.dtPg1NgayVaoLam.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayVaoLam.Location = new System.Drawing.Point(354, 24);
            this.dtPg1NgayVaoLam.Name = "dtPg1NgayVaoLam";
            this.dtPg1NgayVaoLam.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoLam.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoLam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayVaoLam.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoLam.Size = new System.Drawing.Size(120, 22);
            this.dtPg1NgayVaoLam.TabIndex = 16;
            this.dtPg1NgayVaoLam.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(273, 27);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(75, 15);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Ngày vào làm";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(858, 55);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(22, 15);
            this.labelControl23.TabIndex = 0;
            this.labelControl23.Text = "Bậc";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(273, 55);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(70, 15);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "Chuyên môn";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(670, 55);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(29, 15);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "Hạng";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Location = new System.Drawing.Point(666, 27);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(56, 15);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Trạng thái";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(480, 55);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(55, 15);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "Mã ngạch";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(480, 27);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(47, 15);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "Chức vụ";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(6, 55);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(94, 15);
            this.labelControl31.TabIndex = 0;
            this.labelControl31.Text = "B.Môn/B.Phận (*)";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(6, 27);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(84, 15);
            this.labelControl32.TabIndex = 0;
            this.labelControl32.Text = "Phòng/Khoa (*)";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.txtPg1QueQuan);
            this.groupControl2.Controls.Add(this.txtPg1NoiSinh);
            this.groupControl2.Controls.Add(this.cbbPg1QuocTich);
            this.groupControl2.Controls.Add(this.cbbPg1VanHoa);
            this.groupControl2.Controls.Add(this.cbbPg1TinhTrangHonNhan);
            this.groupControl2.Controls.Add(this.cbbPg1NoiCapCMND);
            this.groupControl2.Controls.Add(this.cbbPg1TonGiao);
            this.groupControl2.Controls.Add(this.cbbPg1DanToc);
            this.groupControl2.Controls.Add(this.dtPg1NgayCapCMND);
            this.groupControl2.Controls.Add(this.dtPg1NgaySinh);
            this.groupControl2.Controls.Add(this.rdbPg1GioiTinh);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtPg1Ten);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.txtPg1SoCMND);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.labelControl75);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.txtPg1TenKhac);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.txtPg1HoDem);
            this.groupControl2.Location = new System.Drawing.Point(5, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1192, 109);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Thông tin cơ bản";
            // 
            // txtPg1QueQuan
            // 
            this.txtPg1QueQuan.Location = new System.Drawing.Point(427, 80);
            this.txtPg1QueQuan.Name = "txtPg1QueQuan";
            this.txtPg1QueQuan.Properties.AllowFocused = false;
            this.txtPg1QueQuan.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1QueQuan.Properties.Appearance.Options.UseFont = true;
            this.txtPg1QueQuan.Size = new System.Drawing.Size(106, 22);
            this.txtPg1QueQuan.TabIndex = 16;
            // 
            // txtPg1NoiSinh
            // 
            this.txtPg1NoiSinh.Location = new System.Drawing.Point(225, 80);
            this.txtPg1NoiSinh.Name = "txtPg1NoiSinh";
            this.txtPg1NoiSinh.Properties.AllowFocused = false;
            this.txtPg1NoiSinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1NoiSinh.Properties.Appearance.Options.UseFont = true;
            this.txtPg1NoiSinh.Size = new System.Drawing.Size(136, 22);
            this.txtPg1NoiSinh.TabIndex = 15;
            // 
            // cbbPg1QuocTich
            // 
            this.cbbPg1QuocTich.EditValue = "Việt Nam";
            this.cbbPg1QuocTich.Location = new System.Drawing.Point(922, 24);
            this.cbbPg1QuocTich.Name = "cbbPg1QuocTich";
            this.cbbPg1QuocTich.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1QuocTich.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1QuocTich.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1QuocTich.Properties.Items.AddRange(new object[] {
            "Việt Nam"});
            this.cbbPg1QuocTich.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1QuocTich.TabIndex = 5;
            // 
            // cbbPg1VanHoa
            // 
            this.cbbPg1VanHoa.EditValue = "12/12";
            this.cbbPg1VanHoa.Location = new System.Drawing.Point(595, 80);
            this.cbbPg1VanHoa.Name = "cbbPg1VanHoa";
            this.cbbPg1VanHoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1VanHoa.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1VanHoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1VanHoa.Properties.Items.AddRange(new object[] {
            "10/10",
            "12/12"});
            this.cbbPg1VanHoa.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1VanHoa.TabIndex = 13;
            // 
            // cbbPg1TinhTrangHonNhan
            // 
            this.cbbPg1TinhTrangHonNhan.EditValue = "Độc thân";
            this.cbbPg1TinhTrangHonNhan.Location = new System.Drawing.Point(814, 80);
            this.cbbPg1TinhTrangHonNhan.Name = "cbbPg1TinhTrangHonNhan";
            this.cbbPg1TinhTrangHonNhan.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TinhTrangHonNhan.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TinhTrangHonNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TinhTrangHonNhan.Properties.Items.AddRange(new object[] {
            "Độc thân",
            "Có gia đình"});
            this.cbbPg1TinhTrangHonNhan.Size = new System.Drawing.Size(205, 22);
            this.cbbPg1TinhTrangHonNhan.TabIndex = 14;
            // 
            // cbbPg1NoiCapCMND
            // 
            this.cbbPg1NoiCapCMND.Location = new System.Drawing.Point(595, 52);
            this.cbbPg1NoiCapCMND.Name = "cbbPg1NoiCapCMND";
            this.cbbPg1NoiCapCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1NoiCapCMND.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1NoiCapCMND.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1NoiCapCMND.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Mã tỉnh thành", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tên tỉnh thành")});
            this.cbbPg1NoiCapCMND.Properties.DisplayMember = "TenTinhThanh";
            this.cbbPg1NoiCapCMND.Properties.NullText = "";
            this.cbbPg1NoiCapCMND.Properties.ValueMember = "MaTinhThanh";
            this.cbbPg1NoiCapCMND.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1NoiCapCMND.TabIndex = 8;
            // 
            // cbbPg1TonGiao
            // 
            this.cbbPg1TonGiao.Location = new System.Drawing.Point(759, 24);
            this.cbbPg1TonGiao.Name = "cbbPg1TonGiao";
            this.cbbPg1TonGiao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1TonGiao.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1TonGiao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1TonGiao.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTonGiao", "Mã tôn giáo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTonGiao", "Tên tôn giáo")});
            this.cbbPg1TonGiao.Properties.DisplayMember = "TenTonGiao";
            this.cbbPg1TonGiao.Properties.NullText = "";
            this.cbbPg1TonGiao.Properties.ValueMember = "MaTonGiao";
            this.cbbPg1TonGiao.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1TonGiao.TabIndex = 4;
            // 
            // cbbPg1DanToc
            // 
            this.cbbPg1DanToc.Location = new System.Drawing.Point(595, 24);
            this.cbbPg1DanToc.Name = "cbbPg1DanToc";
            this.cbbPg1DanToc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1DanToc.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1DanToc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1DanToc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenDanToc", "Dân tộc"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaDanToc", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cbbPg1DanToc.Properties.DisplayMember = "TenDanToc";
            this.cbbPg1DanToc.Properties.NullText = "";
            this.cbbPg1DanToc.Properties.ValueMember = "MaDanToc";
            this.cbbPg1DanToc.Size = new System.Drawing.Size(97, 22);
            this.cbbPg1DanToc.TabIndex = 3;
            // 
            // dtPg1NgayCapCMND
            // 
            this.dtPg1NgayCapCMND.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayCapCMND.Location = new System.Drawing.Point(760, 52);
            this.dtPg1NgayCapCMND.Name = "dtPg1NgayCapCMND";
            this.dtPg1NgayCapCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayCapCMND.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayCapCMND.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayCapCMND.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayCapCMND.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayCapCMND.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayCapCMND.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayCapCMND.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayCapCMND.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayCapCMND.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayCapCMND.Size = new System.Drawing.Size(96, 22);
            this.dtPg1NgayCapCMND.TabIndex = 9;
            this.dtPg1NgayCapCMND.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // dtPg1NgaySinh
            // 
            this.dtPg1NgaySinh.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgaySinh.Location = new System.Drawing.Point(67, 80);
            this.dtPg1NgaySinh.Name = "dtPg1NgaySinh";
            this.dtPg1NgaySinh.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtPg1NgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgaySinh.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgaySinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgaySinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgaySinh.Properties.NullDate = "1-1-0001";
            this.dtPg1NgaySinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgaySinh.Size = new System.Drawing.Size(99, 22);
            this.dtPg1NgaySinh.TabIndex = 10;
            this.dtPg1NgaySinh.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.dateTime_CustomDisplayText);
            // 
            // rdbPg1GioiTinh
            // 
            this.rdbPg1GioiTinh.EditValue = 0;
            this.rdbPg1GioiTinh.Location = new System.Drawing.Point(427, 24);
            this.rdbPg1GioiTinh.Name = "rdbPg1GioiTinh";
            this.rdbPg1GioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbPg1GioiTinh.Properties.Appearance.Options.UseFont = true;
            this.rdbPg1GioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Nữ")});
            this.rdbPg1GioiTinh.Size = new System.Drawing.Size(106, 22);
            this.rdbPg1GioiTinh.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(221, 27);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(37, 15);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Tên (*)";
            // 
            // txtPg1Ten
            // 
            this.txtPg1Ten.EditValue = "";
            this.txtPg1Ten.Location = new System.Drawing.Point(264, 24);
            this.txtPg1Ten.Name = "txtPg1Ten";
            this.txtPg1Ten.Properties.AllowFocused = false;
            this.txtPg1Ten.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1Ten.Properties.Appearance.Options.UseFont = true;
            this.txtPg1Ten.Size = new System.Drawing.Size(97, 22);
            this.txtPg1Ten.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(369, 27);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(47, 15);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Giới tính";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(6, 83);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(55, 15);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Sinh ngày";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(862, 27);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(54, 15);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Quốc tịch";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(369, 83);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(54, 15);
            this.labelControl16.TabIndex = 0;
            this.labelControl16.Text = "Quê quán";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(542, 83);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(46, 15);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "Văn hóa";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(172, 83);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 15);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Nơi sinh";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(542, 55);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(44, 15);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "Nơi cấp";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(699, 27);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(48, 15);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "Tôn giáo";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(542, 27);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(43, 15);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "Dân tộc";
            // 
            // txtPg1SoCMND
            // 
            this.txtPg1SoCMND.Location = new System.Drawing.Point(427, 52);
            this.txtPg1SoCMND.Name = "txtPg1SoCMND";
            this.txtPg1SoCMND.Properties.AllowFocused = false;
            this.txtPg1SoCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1SoCMND.Properties.Appearance.Options.UseFont = true;
            this.txtPg1SoCMND.Size = new System.Drawing.Size(106, 22);
            this.txtPg1SoCMND.TabIndex = 7;
            this.txtPg1SoCMND.EditValueChanged += new System.EventHandler(this.txtPg1SoCMND_EditValueChanged);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(369, 55);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(34, 15);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "CMND";
            // 
            // labelControl75
            // 
            this.labelControl75.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl75.Location = new System.Drawing.Point(699, 83);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(112, 15);
            this.labelControl75.TabIndex = 0;
            this.labelControl75.Text = "Tình trạng hôn nhân";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(699, 55);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(52, 15);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Ngày cấp";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(6, 55);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(52, 15);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Tên khác";
            // 
            // txtPg1TenKhac
            // 
            this.txtPg1TenKhac.Location = new System.Drawing.Point(67, 52);
            this.txtPg1TenKhac.Name = "txtPg1TenKhac";
            this.txtPg1TenKhac.Properties.AllowFocused = false;
            this.txtPg1TenKhac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1TenKhac.Properties.Appearance.Options.UseFont = true;
            this.txtPg1TenKhac.Size = new System.Drawing.Size(294, 22);
            this.txtPg1TenKhac.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(6, 27);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(32, 15);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Họ lót";
            // 
            // txtPg1HoDem
            // 
            this.txtPg1HoDem.Location = new System.Drawing.Point(67, 24);
            this.txtPg1HoDem.Name = "txtPg1HoDem";
            this.txtPg1HoDem.Properties.AllowFocused = false;
            this.txtPg1HoDem.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg1HoDem.Properties.Appearance.Options.UseFont = true;
            this.txtPg1HoDem.Size = new System.Drawing.Size(152, 22);
            this.txtPg1HoDem.TabIndex = 0;
            // 
            // ucNVSoLuocLyLich
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ucNVSoLuocLyLich";
            this.Size = new System.Drawing.Size(1200, 510);
            this.Load += new System.EventHandler(this.ucNVSoLuocLyLich_Load);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1EmailTail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhThanhTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuanHuyenTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1DiaChiHT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QuanHamCaoNhat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1DangVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1QuanNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1ThuongBinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbPg1GiaDinhChinhSach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayXuatNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NhomMau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayChinhThucDCSVN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayNhapNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoDCSVN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangSecond.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ThuongBinhHangFirst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1CanNang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1ChieuCao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TinhTrangSucKhoe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Hang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1PhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1BMBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChuyenMon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangLamViec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1QueQuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1NoiSinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1QuocTich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1VanHoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TinhTrangHonNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1NoiCapCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1TonGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1DanToc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayCapCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg1GioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1Ten.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1SoCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1TenKhac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg1HoDem.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnHieuChinh;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraEditors.SimpleButton btnXemCacPhienBan;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1QuanHuyenHT;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1EmailTail;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1TinhThanhHT;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1TinhThanhTT;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1QuanHuyenTT;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txtPg1Email;
        private DevExpress.XtraEditors.TextEdit txtPg1DienThoai;
        private DevExpress.XtraEditors.TextEdit txtPg1DiaChiTT;
        private DevExpress.XtraEditors.TextEdit txtPg1DiaChiHT;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.ComboBoxEdit txtPg1QuanHamCaoNhat;
        private DevExpress.XtraEditors.CheckEdit chbPg1DangVien;
        private DevExpress.XtraEditors.CheckEdit chbPg1QuanNgu;
        private DevExpress.XtraEditors.CheckEdit chbPg1ThuongBinh;
        private DevExpress.XtraEditors.CheckEdit chbPg1GiaDinhChinhSach;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayXuatNgu;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1NhomMau;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayChinhThucDCSVN;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayNhapNgu;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayVaoDCSVN;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.TextEdit txtPg1ThuongBinhHangSecond;
        private DevExpress.XtraEditors.TextEdit txtPg1ThuongBinhHangFirst;
        private DevExpress.XtraEditors.TextEdit txtPg1CanNang;
        private DevExpress.XtraEditors.TextEdit txtPg1ChieuCao;
        private DevExpress.XtraEditors.TextEdit txtPg1TinhTrangSucKhoe;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1Bac;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1MaNgach;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1Hang;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1PhongKhoa;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1BMBP;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1ChuyenMon;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1ChucVu;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1TinhTrangLamViec;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayVaoLam;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtPg1QueQuan;
        private DevExpress.XtraEditors.TextEdit txtPg1NoiSinh;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1QuocTich;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1VanHoa;
        private DevExpress.XtraEditors.ComboBoxEdit cbbPg1TinhTrangHonNhan;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1NoiCapCMND;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1TonGiao;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1DanToc;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayCapCMND;
        private DevExpress.XtraEditors.DateEdit dtPg1NgaySinh;
        private DevExpress.XtraEditors.RadioGroup rdbPg1GioiTinh;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtPg1Ten;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtPg1SoCMND;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtPg1TenKhac;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtPg1HoDem;


    }
}
