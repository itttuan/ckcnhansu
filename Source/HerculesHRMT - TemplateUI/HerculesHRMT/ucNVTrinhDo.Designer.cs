﻿namespace HerculesHRMT
{
    partial class ucNVTrinhDo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabTrinhdoChuyenmon = new System.Windows.Forms.TabPage();
            this.gvPg3ChuyenMon = new DevExpress.XtraGrid.GridControl();
            this.gridviewChuyenMon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColCMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMLoaiTDCMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMLoaiTDCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMTenHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMChuyennganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMNoiCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMNgayCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMHocBong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColCMTenDeAn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.txtCMNoiCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.btnCMThemTruong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCMTenDeAn = new DevExpress.XtraEditors.TextEdit();
            this.chbCMDienHocBong = new DevExpress.XtraEditors.CheckEdit();
            this.txtCMNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCMTgianDen = new DevExpress.XtraEditors.TextEdit();
            this.txtCMChuyenNganh = new DevExpress.XtraEditors.TextEdit();
            this.btnCMSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnCMTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.txtCMTgianTu = new DevExpress.XtraEditors.TextEdit();
            this.cbbCMTrinhDo = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbCMHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbCMTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.tabNgoaiNgu = new System.Windows.Forms.TabPage();
            this.gvPg3NgoaiNgu = new DevExpress.XtraGrid.GridControl();
            this.gridviewNgoaiNgu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNNID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNMaNgoaiNgu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTenNgoaiNgu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNMaLoaiBangNN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTenLoaiBangNN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNLoaiTDCMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNLoaiTDCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTrinhDoDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTenHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNNoiCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNNgayCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNNHocBong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColNNTenDeAn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtNNTrinhDo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.cbbNNBangCap = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.txtNNTenNgoaiNgu = new DevExpress.XtraEditors.LookUpEdit();
            this.btnNNTruong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtNNDeAn = new DevExpress.XtraEditors.TextEdit();
            this.chbNNHocBong = new DevExpress.XtraEditors.CheckEdit();
            this.txtNNNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtNNTgianden = new DevExpress.XtraEditors.TextEdit();
            this.txtNNNoiCap = new DevExpress.XtraEditors.TextEdit();
            this.btnNNSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnNNXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnNNThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnNNTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtNNTgianTu = new DevExpress.XtraEditors.TextEdit();
            this.cbbNNTrinhDo = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbNNHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbNNTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.tabTinHoc = new System.Windows.Forms.TabPage();
            this.gvPg3TinHoc = new DevExpress.XtraGrid.GridControl();
            this.gridviewTinHoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColTHID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHLoaiTDCMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHLoaiTDCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHTenHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHNoiCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHNgayCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTHHocBong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColTHTenDeAn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnTHTruong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtTHDeAn = new DevExpress.XtraEditors.TextEdit();
            this.chbTHHocBong = new DevExpress.XtraEditors.CheckEdit();
            this.txtTHNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtTHTgianden = new DevExpress.XtraEditors.TextEdit();
            this.txtTHNoiCap = new DevExpress.XtraEditors.TextEdit();
            this.btnTHSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnTHXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnTHThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnTHTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtTHTgiantu = new DevExpress.XtraEditors.TextEdit();
            this.cbbTHTrinhDo = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbTHHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbTHTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.tabNghiepVu = new System.Windows.Forms.TabPage();
            this.gvPg3NghiepVu = new DevExpress.XtraGrid.GridControl();
            this.gridviewNghiepVu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVLoaiTDCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiNVSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColNVMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVTenHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVChuyennganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVNoiCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVNgayCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVHocBong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNVTenDeAn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chbNVSP = new DevExpress.XtraEditors.CheckEdit();
            this.txtNVTrinhDo = new DevExpress.XtraEditors.TextEdit();
            this.btnNVTruong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtNVDeAn = new DevExpress.XtraEditors.TextEdit();
            this.chbNVHocBong = new DevExpress.XtraEditors.CheckEdit();
            this.txtNVNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtNVTgianden = new DevExpress.XtraEditors.TextEdit();
            this.txtNVNganh = new DevExpress.XtraEditors.TextEdit();
            this.btnNVSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnNVXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnNVThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnNVTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.txtNVTgiantu = new DevExpress.XtraEditors.TextEdit();
            this.cbbNVHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbNVTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.tabLyLuanCT = new System.Windows.Forms.TabPage();
            this.gvPg3LLCT = new DevExpress.XtraGrid.GridControl();
            this.gridviewLLCT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColLLID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLLoaiTDCMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLLoaiTDCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLTrinhDo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLTenHTDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLChuyennganh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLNoiCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLNgayCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLLHocBong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColLLTenDeAn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btnLLTruong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.txtLLDeAn = new DevExpress.XtraEditors.TextEdit();
            this.chbLLHocBong = new DevExpress.XtraEditors.CheckEdit();
            this.txtLLNgayCap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtLLTgianden = new DevExpress.XtraEditors.TextEdit();
            this.txtLLNganh = new DevExpress.XtraEditors.TextEdit();
            this.btnLLSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnLLXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLLThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnLLTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.txtLLTgiantu = new DevExpress.XtraEditors.TextEdit();
            this.cbbLLTrinhDo = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbLLHinhThuc = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbLLTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.tabControl1.SuspendLayout();
            this.tabTrinhdoChuyenmon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3ChuyenMon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewChuyenMon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNoiCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTenDeAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbCMDienHocBong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTgianDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMChuyenNganh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTgianTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTruong.Properties)).BeginInit();
            this.tabNgoaiNgu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3NgoaiNgu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewNgoaiNgu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNBangCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTenNgoaiNgu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNDeAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNNHocBong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTgianden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNNoiCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTgianTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNTruong.Properties)).BeginInit();
            this.tabTinHoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3TinHoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewTinHoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHDeAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbTHHocBong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHTgianden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHNoiCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHTgiantu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHTruong.Properties)).BeginInit();
            this.tabNghiepVu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3NghiepVu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewNghiepVu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbNVSP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVDeAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNVHocBong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTgianden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVNganh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTgiantu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNVHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNVTruong.Properties)).BeginInit();
            this.tabLyLuanCT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3LLCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewLLCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLDeAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbLLHocBong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLNgayCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTgianden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLNganh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTgiantu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLTrinhDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLHinhThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLTruong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabTrinhdoChuyenmon);
            this.tabControl1.Controls.Add(this.tabNgoaiNgu);
            this.tabControl1.Controls.Add(this.tabTinHoc);
            this.tabControl1.Controls.Add(this.tabNghiepVu);
            this.tabControl1.Controls.Add(this.tabLyLuanCT);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1280, 510);
            this.tabControl1.TabIndex = 0;
            // 
            // tabTrinhdoChuyenmon
            // 
            this.tabTrinhdoChuyenmon.Controls.Add(this.gvPg3ChuyenMon);
            this.tabTrinhdoChuyenmon.Controls.Add(this.groupControl10);
            this.tabTrinhdoChuyenmon.Location = new System.Drawing.Point(4, 22);
            this.tabTrinhdoChuyenmon.Name = "tabTrinhdoChuyenmon";
            this.tabTrinhdoChuyenmon.Padding = new System.Windows.Forms.Padding(3);
            this.tabTrinhdoChuyenmon.Size = new System.Drawing.Size(1272, 484);
            this.tabTrinhdoChuyenmon.TabIndex = 0;
            this.tabTrinhdoChuyenmon.Text = "Trình Độ Chuyên Môn";
            this.tabTrinhdoChuyenmon.UseVisualStyleBackColor = true;
            // 
            // gvPg3ChuyenMon
            // 
            this.gvPg3ChuyenMon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg3ChuyenMon.Location = new System.Drawing.Point(3, 187);
            this.gvPg3ChuyenMon.MainView = this.gridviewChuyenMon;
            this.gvPg3ChuyenMon.Name = "gvPg3ChuyenMon";
            this.gvPg3ChuyenMon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gvPg3ChuyenMon.Size = new System.Drawing.Size(1266, 294);
            this.gvPg3ChuyenMon.TabIndex = 123;
            this.gvPg3ChuyenMon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewChuyenMon});
            // 
            // gridviewChuyenMon
            // 
            this.gridviewChuyenMon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColCMID,
            this.gridColCMLoaiTDCMID,
            this.gridColCMLoaiTDCM,
            this.gridColCMMaTruong,
            this.gridColCMTrinhDo,
            this.gridColCMHTDT,
            this.gridColCMTenHTDT,
            this.gridColCMChuyennganh,
            this.gridColCMTu,
            this.gridColCMden,
            this.gridColCMNoiCap,
            this.gridColCMNgayCap,
            this.gridColCMHocBong,
            this.gridColCMTenDeAn});
            this.gridviewChuyenMon.GridControl = this.gvPg3ChuyenMon;
            this.gridviewChuyenMon.GroupPanelText = "Bằng cấp chuyên môn";
            this.gridviewChuyenMon.Name = "gridviewChuyenMon";
            this.gridviewChuyenMon.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewChuyenMon_RowClick);
            this.gridviewChuyenMon.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewChuyenMon_FocusedRowChanged);
            // 
            // gridColCMID
            // 
            this.gridColCMID.Caption = "Mã ID";
            this.gridColCMID.FieldName = "Id";
            this.gridColCMID.Name = "gridColCMID";
            this.gridColCMID.OptionsColumn.AllowEdit = false;
            this.gridColCMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColCMLoaiTDCMID
            // 
            this.gridColCMLoaiTDCMID.Caption = "Mã B.Cấp/C.Chỉ";
            this.gridColCMLoaiTDCMID.FieldName = " LoaiTrinhDoChuyenMon";
            this.gridColCMLoaiTDCMID.Name = "gridColCMLoaiTDCMID";
            this.gridColCMLoaiTDCMID.OptionsColumn.AllowEdit = false;
            this.gridColCMLoaiTDCMID.OptionsColumn.ReadOnly = true;
            this.gridColCMLoaiTDCMID.Width = 73;
            // 
            // gridColCMLoaiTDCM
            // 
            this.gridColCMLoaiTDCM.Caption = "Trình độ";
            this.gridColCMLoaiTDCM.FieldName = "LoaiBangChuyenMon.TenLoaiBangChuyenMon";
            this.gridColCMLoaiTDCM.Name = "gridColCMLoaiTDCM";
            this.gridColCMLoaiTDCM.OptionsColumn.AllowEdit = false;
            this.gridColCMLoaiTDCM.OptionsColumn.ReadOnly = true;
            this.gridColCMLoaiTDCM.Visible = true;
            this.gridColCMLoaiTDCM.VisibleIndex = 0;
            this.gridColCMLoaiTDCM.Width = 73;
            // 
            // gridColCMMaTruong
            // 
            this.gridColCMMaTruong.Caption = "Chuyên môn";
            this.gridColCMMaTruong.FieldName = "MaTruong";
            this.gridColCMMaTruong.Name = "gridColCMMaTruong";
            this.gridColCMMaTruong.OptionsColumn.AllowEdit = false;
            this.gridColCMMaTruong.OptionsColumn.ReadOnly = true;
            this.gridColCMMaTruong.Width = 73;
            // 
            // gridColCMTrinhDo
            // 
            this.gridColCMTrinhDo.Caption = "Trường đào tạo";
            this.gridColCMTrinhDo.FieldName = "Truong.TenTruong";
            this.gridColCMTrinhDo.Name = "gridColCMTrinhDo";
            this.gridColCMTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColCMTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColCMTrinhDo.Visible = true;
            this.gridColCMTrinhDo.VisibleIndex = 1;
            this.gridColCMTrinhDo.Width = 73;
            // 
            // gridColCMHTDT
            // 
            this.gridColCMHTDT.Caption = "Chuyên ngành";
            this.gridColCMHTDT.FieldName = "HinhThucDaoTao";
            this.gridColCMHTDT.Name = "gridColCMHTDT";
            this.gridColCMHTDT.OptionsColumn.AllowEdit = false;
            this.gridColCMHTDT.OptionsColumn.ReadOnly = true;
            this.gridColCMHTDT.Width = 73;
            // 
            // gridColCMTenHTDT
            // 
            this.gridColCMTenHTDT.Caption = "Hình thức đào tạo";
            this.gridColCMTenHTDT.FieldName = "HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao";
            this.gridColCMTenHTDT.Name = "gridColCMTenHTDT";
            this.gridColCMTenHTDT.OptionsColumn.AllowEdit = false;
            this.gridColCMTenHTDT.OptionsColumn.ReadOnly = true;
            this.gridColCMTenHTDT.Visible = true;
            this.gridColCMTenHTDT.VisibleIndex = 2;
            this.gridColCMTenHTDT.Width = 73;
            // 
            // gridColCMChuyennganh
            // 
            this.gridColCMChuyennganh.Caption = "Chuyên ngành đào tạo";
            this.gridColCMChuyennganh.FieldName = "ChuyenNganhDaoTao";
            this.gridColCMChuyennganh.Name = "gridColCMChuyennganh";
            this.gridColCMChuyennganh.OptionsColumn.AllowEdit = false;
            this.gridColCMChuyennganh.OptionsColumn.ReadOnly = true;
            this.gridColCMChuyennganh.Visible = true;
            this.gridColCMChuyennganh.VisibleIndex = 3;
            this.gridColCMChuyennganh.Width = 73;
            // 
            // gridColCMTu
            // 
            this.gridColCMTu.Caption = "T.gian đào tạo từ";
            this.gridColCMTu.FieldName = "ThoiGianDaoTaoTu";
            this.gridColCMTu.Name = "gridColCMTu";
            this.gridColCMTu.OptionsColumn.AllowEdit = false;
            this.gridColCMTu.OptionsColumn.ReadOnly = true;
            this.gridColCMTu.Visible = true;
            this.gridColCMTu.VisibleIndex = 4;
            this.gridColCMTu.Width = 73;
            // 
            // gridColCMden
            // 
            this.gridColCMden.Caption = "T.gian đào tạo đến";
            this.gridColCMden.FieldName = "ThoiGianDaoTaoDen";
            this.gridColCMden.Name = "gridColCMden";
            this.gridColCMden.OptionsColumn.AllowEdit = false;
            this.gridColCMden.Visible = true;
            this.gridColCMden.VisibleIndex = 5;
            this.gridColCMden.Width = 73;
            // 
            // gridColCMNoiCap
            // 
            this.gridColCMNoiCap.Caption = "Nơi cấp";
            this.gridColCMNoiCap.FieldName = "NoiCapBang";
            this.gridColCMNoiCap.Name = "gridColCMNoiCap";
            this.gridColCMNoiCap.OptionsColumn.AllowEdit = false;
            this.gridColCMNoiCap.OptionsColumn.ReadOnly = true;
            this.gridColCMNoiCap.Visible = true;
            this.gridColCMNoiCap.VisibleIndex = 6;
            this.gridColCMNoiCap.Width = 73;
            // 
            // gridColCMNgayCap
            // 
            this.gridColCMNgayCap.Caption = "Ngày cấp";
            this.gridColCMNgayCap.FieldName = "NgayCapBang";
            this.gridColCMNgayCap.Name = "gridColCMNgayCap";
            this.gridColCMNgayCap.OptionsColumn.AllowEdit = false;
            this.gridColCMNgayCap.OptionsColumn.ReadOnly = true;
            this.gridColCMNgayCap.Visible = true;
            this.gridColCMNgayCap.VisibleIndex = 7;
            this.gridColCMNgayCap.Width = 98;
            // 
            // gridColCMHocBong
            // 
            this.gridColCMHocBong.Caption = "Học bổng nhà nước";
            this.gridColCMHocBong.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColCMHocBong.FieldName = "HocBongNhaNuoc";
            this.gridColCMHocBong.Name = "gridColCMHocBong";
            this.gridColCMHocBong.OptionsColumn.AllowEdit = false;
            this.gridColCMHocBong.OptionsColumn.ReadOnly = true;
            this.gridColCMHocBong.Visible = true;
            this.gridColCMHocBong.VisibleIndex = 8;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColCMTenDeAn
            // 
            this.gridColCMTenDeAn.Caption = "Tên đề án học bổng";
            this.gridColCMTenDeAn.FieldName = "TenDeAnHocBong";
            this.gridColCMTenDeAn.Name = "gridColCMTenDeAn";
            this.gridColCMTenDeAn.OptionsColumn.AllowEdit = false;
            this.gridColCMTenDeAn.OptionsColumn.ReadOnly = true;
            this.gridColCMTenDeAn.Visible = true;
            this.gridColCMTenDeAn.VisibleIndex = 9;
            // 
            // groupControl10
            // 
            this.groupControl10.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.Appearance.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.txtCMNoiCap);
            this.groupControl10.Controls.Add(this.labelControl42);
            this.groupControl10.Controls.Add(this.btnCMThemTruong);
            this.groupControl10.Controls.Add(this.labelControl2);
            this.groupControl10.Controls.Add(this.txtCMTenDeAn);
            this.groupControl10.Controls.Add(this.chbCMDienHocBong);
            this.groupControl10.Controls.Add(this.txtCMNgayCap);
            this.groupControl10.Controls.Add(this.labelControl1);
            this.groupControl10.Controls.Add(this.txtCMTgianDen);
            this.groupControl10.Controls.Add(this.txtCMChuyenNganh);
            this.groupControl10.Controls.Add(this.btnCMSua);
            this.groupControl10.Controls.Add(this.btnCMXoa);
            this.groupControl10.Controls.Add(this.btnCMThem);
            this.groupControl10.Controls.Add(this.btnCMTaoMoi);
            this.groupControl10.Controls.Add(this.labelControl60);
            this.groupControl10.Controls.Add(this.labelControl57);
            this.groupControl10.Controls.Add(this.labelControl74);
            this.groupControl10.Controls.Add(this.labelControl50);
            this.groupControl10.Controls.Add(this.labelControl59);
            this.groupControl10.Controls.Add(this.labelControl53);
            this.groupControl10.Controls.Add(this.labelControl54);
            this.groupControl10.Controls.Add(this.txtCMTgianTu);
            this.groupControl10.Controls.Add(this.cbbCMTrinhDo);
            this.groupControl10.Controls.Add(this.cbbCMHinhThuc);
            this.groupControl10.Controls.Add(this.cbbCMTruong);
            this.groupControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl10.Location = new System.Drawing.Point(3, 3);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(1266, 184);
            this.groupControl10.TabIndex = 122;
            this.groupControl10.Text = "Thông tin trình độ chuyên môn";
            // 
            // txtCMNoiCap
            // 
            this.txtCMNoiCap.Location = new System.Drawing.Point(136, 119);
            this.txtCMNoiCap.Name = "txtCMNoiCap";
            this.txtCMNoiCap.Properties.AllowFocused = false;
            this.txtCMNoiCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMNoiCap.Properties.Appearance.Options.UseFont = true;
            this.txtCMNoiCap.Size = new System.Drawing.Size(170, 22);
            this.txtCMNoiCap.TabIndex = 17;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Location = new System.Drawing.Point(5, 122);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(44, 15);
            this.labelControl42.TabIndex = 16;
            this.labelControl42.Text = "Nơi cấp";
            // 
            // btnCMThemTruong
            // 
            this.btnCMThemTruong.Location = new System.Drawing.Point(703, 31);
            this.btnCMThemTruong.Name = "btnCMThemTruong";
            this.btnCMThemTruong.Size = new System.Drawing.Size(34, 23);
            this.btnCMThemTruong.TabIndex = 15;
            this.btnCMThemTruong.Text = "...";
            this.btnCMThemTruong.ToolTip = "Thêm Trường đào tạo";
            this.btnCMThemTruong.Click += new System.EventHandler(this.btnCMThemTruong_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(590, 122);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(55, 15);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "Tên đề án";
            // 
            // txtCMTenDeAn
            // 
            this.txtCMTenDeAn.Location = new System.Drawing.Point(670, 119);
            this.txtCMTenDeAn.Name = "txtCMTenDeAn";
            this.txtCMTenDeAn.Properties.AllowFocused = false;
            this.txtCMTenDeAn.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMTenDeAn.Properties.Appearance.Options.UseFont = true;
            this.txtCMTenDeAn.Size = new System.Drawing.Size(374, 22);
            this.txtCMTenDeAn.TabIndex = 13;
            // 
            // chbCMDienHocBong
            // 
            this.chbCMDienHocBong.Location = new System.Drawing.Point(344, 119);
            this.chbCMDienHocBong.Name = "chbCMDienHocBong";
            this.chbCMDienHocBong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbCMDienHocBong.Properties.Appearance.Options.UseFont = true;
            this.chbCMDienHocBong.Properties.Caption = "Học bổng nhà nước";
            this.chbCMDienHocBong.Size = new System.Drawing.Size(156, 20);
            this.chbCMDienHocBong.TabIndex = 12;
            // 
            // txtCMNgayCap
            // 
            this.txtCMNgayCap.Location = new System.Drawing.Point(886, 77);
            this.txtCMNgayCap.Name = "txtCMNgayCap";
            this.txtCMNgayCap.Properties.AllowFocused = false;
            this.txtCMNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtCMNgayCap.Size = new System.Drawing.Size(158, 22);
            this.txtCMNgayCap.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(590, 80);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(21, 15);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "đến";
            // 
            // txtCMTgianDen
            // 
            this.txtCMTgianDen.Location = new System.Drawing.Point(632, 77);
            this.txtCMTgianDen.Name = "txtCMTgianDen";
            this.txtCMTgianDen.Properties.AllowFocused = false;
            this.txtCMTgianDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMTgianDen.Properties.Appearance.Options.UseFont = true;
            this.txtCMTgianDen.Size = new System.Drawing.Size(105, 22);
            this.txtCMTgianDen.TabIndex = 9;
            // 
            // txtCMChuyenNganh
            // 
            this.txtCMChuyenNganh.Location = new System.Drawing.Point(136, 77);
            this.txtCMChuyenNganh.Name = "txtCMChuyenNganh";
            this.txtCMChuyenNganh.Properties.AllowFocused = false;
            this.txtCMChuyenNganh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMChuyenNganh.Properties.Appearance.Options.UseFont = true;
            this.txtCMChuyenNganh.Size = new System.Drawing.Size(170, 22);
            this.txtCMChuyenNganh.TabIndex = 8;
            // 
            // btnCMSua
            // 
            this.btnCMSua.Location = new System.Drawing.Point(171, 154);
            this.btnCMSua.Name = "btnCMSua";
            this.btnCMSua.Size = new System.Drawing.Size(75, 23);
            this.btnCMSua.TabIndex = 7;
            this.btnCMSua.Text = "Hiệu chỉnh";
            this.btnCMSua.Click += new System.EventHandler(this.btnCMSua_Click);
            // 
            // btnCMXoa
            // 
            this.btnCMXoa.Location = new System.Drawing.Point(252, 154);
            this.btnCMXoa.Name = "btnCMXoa";
            this.btnCMXoa.Size = new System.Drawing.Size(75, 23);
            this.btnCMXoa.TabIndex = 0;
            this.btnCMXoa.Text = "Xóa";
            this.btnCMXoa.Click += new System.EventHandler(this.btnCMXoa_Click);
            // 
            // btnCMThem
            // 
            this.btnCMThem.Location = new System.Drawing.Point(90, 154);
            this.btnCMThem.Name = "btnCMThem";
            this.btnCMThem.Size = new System.Drawing.Size(75, 23);
            this.btnCMThem.TabIndex = 0;
            this.btnCMThem.Text = "Lưu";
            this.btnCMThem.Click += new System.EventHandler(this.btnCMThem_Click);
            // 
            // btnCMTaoMoi
            // 
            this.btnCMTaoMoi.Location = new System.Drawing.Point(9, 154);
            this.btnCMTaoMoi.Name = "btnCMTaoMoi";
            this.btnCMTaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnCMTaoMoi.TabIndex = 0;
            this.btnCMTaoMoi.Text = "Tạo mới";
            this.btnCMTaoMoi.Click += new System.EventHandler(this.btnCMTaoMoi_Click);
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl60.Location = new System.Drawing.Point(773, 80);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(52, 15);
            this.labelControl60.TabIndex = 0;
            this.labelControl60.Text = "Ngày cấp";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(5, 80);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(125, 15);
            this.labelControl57.TabIndex = 0;
            this.labelControl57.Text = "Chuyên ngành đào tạo";
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl74.Location = new System.Drawing.Point(9, 31);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(0, 15);
            this.labelControl74.TabIndex = 0;
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Location = new System.Drawing.Point(773, 35);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(100, 15);
            this.labelControl50.TabIndex = 0;
            this.labelControl50.Text = "Hình thức đào tạo";
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Location = new System.Drawing.Point(346, 80);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(94, 15);
            this.labelControl59.TabIndex = 0;
            this.labelControl59.Text = "T.gian đào tạo từ";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Location = new System.Drawing.Point(9, 35);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(98, 15);
            this.labelControl53.TabIndex = 0;
            this.labelControl53.Text = "Trình độ đào tạo *";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Location = new System.Drawing.Point(346, 35);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(96, 15);
            this.labelControl54.TabIndex = 0;
            this.labelControl54.Text = "Trường đào tạo *";
            // 
            // txtCMTgianTu
            // 
            this.txtCMTgianTu.Location = new System.Drawing.Point(459, 77);
            this.txtCMTgianTu.Name = "txtCMTgianTu";
            this.txtCMTgianTu.Properties.AllowFocused = false;
            this.txtCMTgianTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMTgianTu.Properties.Appearance.Options.UseFont = true;
            this.txtCMTgianTu.Size = new System.Drawing.Size(104, 22);
            this.txtCMTgianTu.TabIndex = 0;
            // 
            // cbbCMTrinhDo
            // 
            this.cbbCMTrinhDo.Location = new System.Drawing.Point(136, 32);
            this.cbbCMTrinhDo.Name = "cbbCMTrinhDo";
            this.cbbCMTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.cbbCMTrinhDo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMTrinhDo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangChuyenMon", "Trình độ")});
            this.cbbCMTrinhDo.Properties.DisplayMember = "TenLoaiBangChuyenMon";
            this.cbbCMTrinhDo.Properties.NullText = "";
            this.cbbCMTrinhDo.Properties.PopupSizeable = false;
            this.cbbCMTrinhDo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMTrinhDo.Properties.ValueMember = "Id";
            this.cbbCMTrinhDo.Size = new System.Drawing.Size(170, 22);
            this.cbbCMTrinhDo.TabIndex = 4;
            // 
            // cbbCMHinhThuc
            // 
            this.cbbCMHinhThuc.Location = new System.Drawing.Point(886, 32);
            this.cbbCMHinhThuc.Name = "cbbCMHinhThuc";
            this.cbbCMHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbCMHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã HTDT", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbCMHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbCMHinhThuc.Properties.NullText = "";
            this.cbbCMHinhThuc.Properties.PopupSizeable = false;
            this.cbbCMHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMHinhThuc.Properties.ValueMember = "Id";
            this.cbbCMHinhThuc.Size = new System.Drawing.Size(158, 22);
            this.cbbCMHinhThuc.TabIndex = 4;
            // 
            // cbbCMTruong
            // 
            this.cbbCMTruong.Location = new System.Drawing.Point(459, 32);
            this.cbbCMTruong.Name = "cbbCMTruong";
            this.cbbCMTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCMTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbCMTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbCMTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbCMTruong.Properties.DisplayMember = "TenTruong";
            this.cbbCMTruong.Properties.NullText = "";
            this.cbbCMTruong.Properties.PopupSizeable = false;
            this.cbbCMTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbCMTruong.Properties.ValueMember = "MaTruong";
            this.cbbCMTruong.Size = new System.Drawing.Size(241, 22);
            this.cbbCMTruong.TabIndex = 4;
            // 
            // tabNgoaiNgu
            // 
            this.tabNgoaiNgu.Controls.Add(this.gvPg3NgoaiNgu);
            this.tabNgoaiNgu.Controls.Add(this.groupControl1);
            this.tabNgoaiNgu.Location = new System.Drawing.Point(4, 22);
            this.tabNgoaiNgu.Name = "tabNgoaiNgu";
            this.tabNgoaiNgu.Padding = new System.Windows.Forms.Padding(3);
            this.tabNgoaiNgu.Size = new System.Drawing.Size(1272, 484);
            this.tabNgoaiNgu.TabIndex = 1;
            this.tabNgoaiNgu.Text = "Trình Độ Ngoại Ngữ";
            this.tabNgoaiNgu.UseVisualStyleBackColor = true;
            // 
            // gvPg3NgoaiNgu
            // 
            this.gvPg3NgoaiNgu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg3NgoaiNgu.Location = new System.Drawing.Point(3, 235);
            this.gvPg3NgoaiNgu.MainView = this.gridviewNgoaiNgu;
            this.gvPg3NgoaiNgu.Name = "gvPg3NgoaiNgu";
            this.gvPg3NgoaiNgu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.gvPg3NgoaiNgu.Size = new System.Drawing.Size(1266, 246);
            this.gvPg3NgoaiNgu.TabIndex = 125;
            this.gvPg3NgoaiNgu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewNgoaiNgu});
            // 
            // gridviewNgoaiNgu
            // 
            this.gridviewNgoaiNgu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNNID,
            this.gridColNNMaNgoaiNgu,
            this.gridColNNTenNgoaiNgu,
            this.gridColNNMaLoaiBangNN,
            this.gridColNNTenLoaiBangNN,
            this.gridColNNTrinhDo,
            this.gridColNNLoaiTDCMID,
            this.gridColNNLoaiTDCM,
            this.gridColNNMaTruong,
            this.gridColNNTrinhDoDT,
            this.gridColNNHTDT,
            this.gridColNNTenHTDT,
            this.gridColNNTu,
            this.gridColNNden,
            this.gridColNNNoiCap,
            this.gridColNNNgayCap,
            this.gridColNNHocBong,
            this.gridColNNTenDeAn});
            this.gridviewNgoaiNgu.GridControl = this.gvPg3NgoaiNgu;
            this.gridviewNgoaiNgu.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridviewNgoaiNgu.Name = "gridviewNgoaiNgu";
            this.gridviewNgoaiNgu.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewNgoaiNgu_RowClick);
            this.gridviewNgoaiNgu.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewNgoaiNgu_FocusedRowChanged);
            // 
            // gridColNNID
            // 
            this.gridColNNID.Caption = "Mã ID";
            this.gridColNNID.FieldName = "Id";
            this.gridColNNID.Name = "gridColNNID";
            this.gridColNNID.OptionsColumn.AllowEdit = false;
            this.gridColNNID.OptionsColumn.ReadOnly = true;
            // 
            // gridColNNMaNgoaiNgu
            // 
            this.gridColNNMaNgoaiNgu.Caption = "gridColumn1";
            this.gridColNNMaNgoaiNgu.FieldName = "MaNgoaiNgu";
            this.gridColNNMaNgoaiNgu.Name = "gridColNNMaNgoaiNgu";
            // 
            // gridColNNTenNgoaiNgu
            // 
            this.gridColNNTenNgoaiNgu.Caption = "Ngoại ngữ";
            this.gridColNNTenNgoaiNgu.FieldName = "NgoaiNgu.TenQuocGia";
            this.gridColNNTenNgoaiNgu.Name = "gridColNNTenNgoaiNgu";
            this.gridColNNTenNgoaiNgu.OptionsColumn.AllowEdit = false;
            this.gridColNNTenNgoaiNgu.OptionsColumn.ReadOnly = true;
            this.gridColNNTenNgoaiNgu.Visible = true;
            this.gridColNNTenNgoaiNgu.VisibleIndex = 0;
            // 
            // gridColNNMaLoaiBangNN
            // 
            this.gridColNNMaLoaiBangNN.Caption = "gridColumn1";
            this.gridColNNMaLoaiBangNN.FieldName = "LoaiTenBangCapNgoaiNgu";
            this.gridColNNMaLoaiBangNN.Name = "gridColNNMaLoaiBangNN";
            // 
            // gridColNNTenLoaiBangNN
            // 
            this.gridColNNTenLoaiBangNN.Caption = "Bằng cấp";
            this.gridColNNTenLoaiBangNN.FieldName = "LoaiBangNgoaiNgu.TenLoaiBangNgoaiNgu";
            this.gridColNNTenLoaiBangNN.Name = "gridColNNTenLoaiBangNN";
            this.gridColNNTenLoaiBangNN.OptionsColumn.AllowEdit = false;
            this.gridColNNTenLoaiBangNN.OptionsColumn.ReadOnly = true;
            this.gridColNNTenLoaiBangNN.Visible = true;
            this.gridColNNTenLoaiBangNN.VisibleIndex = 1;
            // 
            // gridColNNTrinhDo
            // 
            this.gridColNNTrinhDo.Caption = "Trình độ";
            this.gridColNNTrinhDo.FieldName = "LoaiTrinhDoNgoaiNgu";
            this.gridColNNTrinhDo.Name = "gridColNNTrinhDo";
            this.gridColNNTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColNNTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColNNTrinhDo.Visible = true;
            this.gridColNNTrinhDo.VisibleIndex = 2;
            // 
            // gridColNNLoaiTDCMID
            // 
            this.gridColNNLoaiTDCMID.Caption = "Mã B.Cấp/C.Chỉ";
            this.gridColNNLoaiTDCMID.FieldName = " LoaiTrinhDoChuyenMon";
            this.gridColNNLoaiTDCMID.Name = "gridColNNLoaiTDCMID";
            this.gridColNNLoaiTDCMID.OptionsColumn.AllowEdit = false;
            this.gridColNNLoaiTDCMID.OptionsColumn.ReadOnly = true;
            this.gridColNNLoaiTDCMID.Width = 73;
            // 
            // gridColNNLoaiTDCM
            // 
            this.gridColNNLoaiTDCM.Caption = "Trình độ đào tạo";
            this.gridColNNLoaiTDCM.FieldName = "LoaiBangChuyenMon.TenLoaiBangChuyenMon";
            this.gridColNNLoaiTDCM.Name = "gridColNNLoaiTDCM";
            this.gridColNNLoaiTDCM.OptionsColumn.AllowEdit = false;
            this.gridColNNLoaiTDCM.OptionsColumn.ReadOnly = true;
            this.gridColNNLoaiTDCM.Visible = true;
            this.gridColNNLoaiTDCM.VisibleIndex = 3;
            this.gridColNNLoaiTDCM.Width = 73;
            // 
            // gridColNNMaTruong
            // 
            this.gridColNNMaTruong.Caption = "Chuyên môn";
            this.gridColNNMaTruong.FieldName = "MaTruong";
            this.gridColNNMaTruong.Name = "gridColNNMaTruong";
            this.gridColNNMaTruong.OptionsColumn.AllowEdit = false;
            this.gridColNNMaTruong.OptionsColumn.ReadOnly = true;
            this.gridColNNMaTruong.Width = 73;
            // 
            // gridColNNTrinhDoDT
            // 
            this.gridColNNTrinhDoDT.Caption = "Trường đào tạo";
            this.gridColNNTrinhDoDT.FieldName = "Truong.TenTruong";
            this.gridColNNTrinhDoDT.Name = "gridColNNTrinhDoDT";
            this.gridColNNTrinhDoDT.OptionsColumn.AllowEdit = false;
            this.gridColNNTrinhDoDT.OptionsColumn.ReadOnly = true;
            this.gridColNNTrinhDoDT.Visible = true;
            this.gridColNNTrinhDoDT.VisibleIndex = 4;
            this.gridColNNTrinhDoDT.Width = 73;
            // 
            // gridColNNHTDT
            // 
            this.gridColNNHTDT.Caption = "Chuyên ngành";
            this.gridColNNHTDT.FieldName = "HinhThucDaoTao";
            this.gridColNNHTDT.Name = "gridColNNHTDT";
            this.gridColNNHTDT.OptionsColumn.AllowEdit = false;
            this.gridColNNHTDT.OptionsColumn.ReadOnly = true;
            this.gridColNNHTDT.Width = 73;
            // 
            // gridColNNTenHTDT
            // 
            this.gridColNNTenHTDT.Caption = "Hình thức đào tạo";
            this.gridColNNTenHTDT.FieldName = "HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao";
            this.gridColNNTenHTDT.Name = "gridColNNTenHTDT";
            this.gridColNNTenHTDT.OptionsColumn.AllowEdit = false;
            this.gridColNNTenHTDT.OptionsColumn.ReadOnly = true;
            this.gridColNNTenHTDT.Visible = true;
            this.gridColNNTenHTDT.VisibleIndex = 5;
            this.gridColNNTenHTDT.Width = 73;
            // 
            // gridColNNTu
            // 
            this.gridColNNTu.Caption = "T.gian đào tạo từ";
            this.gridColNNTu.FieldName = "ThoiGianDaoTaoTu";
            this.gridColNNTu.Name = "gridColNNTu";
            this.gridColNNTu.OptionsColumn.AllowEdit = false;
            this.gridColNNTu.OptionsColumn.ReadOnly = true;
            this.gridColNNTu.Visible = true;
            this.gridColNNTu.VisibleIndex = 6;
            this.gridColNNTu.Width = 73;
            // 
            // gridColNNden
            // 
            this.gridColNNden.Caption = "T.gian đào tạo đến";
            this.gridColNNden.FieldName = "ThoiGianDaoTaoDen";
            this.gridColNNden.Name = "gridColNNden";
            this.gridColNNden.OptionsColumn.AllowEdit = false;
            this.gridColNNden.Visible = true;
            this.gridColNNden.VisibleIndex = 7;
            this.gridColNNden.Width = 73;
            // 
            // gridColNNNoiCap
            // 
            this.gridColNNNoiCap.Caption = "Nơi cấp";
            this.gridColNNNoiCap.FieldName = "NoiCapBang";
            this.gridColNNNoiCap.Name = "gridColNNNoiCap";
            this.gridColNNNoiCap.OptionsColumn.AllowEdit = false;
            this.gridColNNNoiCap.OptionsColumn.ReadOnly = true;
            this.gridColNNNoiCap.Visible = true;
            this.gridColNNNoiCap.VisibleIndex = 8;
            this.gridColNNNoiCap.Width = 73;
            // 
            // gridColNNNgayCap
            // 
            this.gridColNNNgayCap.Caption = "Ngày cấp";
            this.gridColNNNgayCap.FieldName = "NgayCapBang";
            this.gridColNNNgayCap.Name = "gridColNNNgayCap";
            this.gridColNNNgayCap.OptionsColumn.AllowEdit = false;
            this.gridColNNNgayCap.OptionsColumn.ReadOnly = true;
            this.gridColNNNgayCap.Visible = true;
            this.gridColNNNgayCap.VisibleIndex = 9;
            this.gridColNNNgayCap.Width = 98;
            // 
            // gridColNNHocBong
            // 
            this.gridColNNHocBong.Caption = "Học bổng nhà nước";
            this.gridColNNHocBong.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColNNHocBong.FieldName = "HocBongNhaNuoc";
            this.gridColNNHocBong.Name = "gridColNNHocBong";
            this.gridColNNHocBong.OptionsColumn.AllowEdit = false;
            this.gridColNNHocBong.OptionsColumn.ReadOnly = true;
            this.gridColNNHocBong.Visible = true;
            this.gridColNNHocBong.VisibleIndex = 10;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // gridColNNTenDeAn
            // 
            this.gridColNNTenDeAn.Caption = "Tên đề án học bổng";
            this.gridColNNTenDeAn.FieldName = "TenDeAnHocBong";
            this.gridColNNTenDeAn.Name = "gridColNNTenDeAn";
            this.gridColNNTenDeAn.OptionsColumn.AllowEdit = false;
            this.gridColNNTenDeAn.OptionsColumn.ReadOnly = true;
            this.gridColNNTenDeAn.Visible = true;
            this.gridColNNTenDeAn.VisibleIndex = 11;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtNNTrinhDo);
            this.groupControl1.Controls.Add(this.labelControl41);
            this.groupControl1.Controls.Add(this.labelControl40);
            this.groupControl1.Controls.Add(this.cbbNNBangCap);
            this.groupControl1.Controls.Add(this.labelControl39);
            this.groupControl1.Controls.Add(this.txtNNTenNgoaiNgu);
            this.groupControl1.Controls.Add(this.btnNNTruong);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtNNDeAn);
            this.groupControl1.Controls.Add(this.chbNNHocBong);
            this.groupControl1.Controls.Add(this.txtNNNgayCap);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtNNTgianden);
            this.groupControl1.Controls.Add(this.txtNNNoiCap);
            this.groupControl1.Controls.Add(this.btnNNSua);
            this.groupControl1.Controls.Add(this.btnNNXoa);
            this.groupControl1.Controls.Add(this.btnNNThem);
            this.groupControl1.Controls.Add(this.btnNNTaoMoi);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.txtNNTgianTu);
            this.groupControl1.Controls.Add(this.cbbNNTrinhDo);
            this.groupControl1.Controls.Add(this.cbbNNHinhThuc);
            this.groupControl1.Controls.Add(this.cbbNNTruong);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1266, 232);
            this.groupControl1.TabIndex = 124;
            this.groupControl1.Text = "Thông tin trình độ ngoại ngữ";
            // 
            // txtNNTrinhDo
            // 
            this.txtNNTrinhDo.Location = new System.Drawing.Point(886, 37);
            this.txtNNTrinhDo.Name = "txtNNTrinhDo";
            this.txtNNTrinhDo.Properties.AllowFocused = false;
            this.txtNNTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.txtNNTrinhDo.Size = new System.Drawing.Size(158, 22);
            this.txtNNTrinhDo.TabIndex = 21;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(773, 40);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(45, 15);
            this.labelControl41.TabIndex = 20;
            this.labelControl41.Text = "Trình độ";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Location = new System.Drawing.Point(346, 40);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(53, 15);
            this.labelControl40.TabIndex = 18;
            this.labelControl40.Text = "Bằng cấp";
            // 
            // cbbNNBangCap
            // 
            this.cbbNNBangCap.Location = new System.Drawing.Point(459, 37);
            this.cbbNNBangCap.Name = "cbbNNBangCap";
            this.cbbNNBangCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNNBangCap.Properties.Appearance.Options.UseFont = true;
            this.cbbNNBangCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNNBangCap.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã Bang", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangNgoaiNgu", "Bằng cấp")});
            this.cbbNNBangCap.Properties.DisplayMember = "TenLoaiBangNgoaiNgu";
            this.cbbNNBangCap.Properties.NullText = "";
            this.cbbNNBangCap.Properties.PopupSizeable = false;
            this.cbbNNBangCap.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNNBangCap.Properties.ValueMember = "Id";
            this.cbbNNBangCap.Size = new System.Drawing.Size(241, 22);
            this.cbbNNBangCap.TabIndex = 19;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Location = new System.Drawing.Point(9, 40);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(89, 15);
            this.labelControl39.TabIndex = 16;
            this.labelControl39.Text = "Tên ngoại ngữ *";
            // 
            // txtNNTenNgoaiNgu
            // 
            this.txtNNTenNgoaiNgu.Location = new System.Drawing.Point(136, 37);
            this.txtNNTenNgoaiNgu.Name = "txtNNTenNgoaiNgu";
            this.txtNNTenNgoaiNgu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNTenNgoaiNgu.Properties.Appearance.Options.UseFont = true;
            this.txtNNTenNgoaiNgu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNNTenNgoaiNgu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuocGia", "Mã QG", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuocGia", "Ngoại ngữ")});
            this.txtNNTenNgoaiNgu.Properties.DisplayMember = "TenQuocGia";
            this.txtNNTenNgoaiNgu.Properties.NullText = "";
            this.txtNNTenNgoaiNgu.Properties.PopupSizeable = false;
            this.txtNNTenNgoaiNgu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txtNNTenNgoaiNgu.Properties.ValueMember = "MaQuocGia";
            this.txtNNTenNgoaiNgu.Size = new System.Drawing.Size(170, 22);
            this.txtNNTenNgoaiNgu.TabIndex = 17;
            this.txtNNTenNgoaiNgu.EditValueChanged += new System.EventHandler(this.txtNNTenNgoaiNgu_EditValueChanged);
            // 
            // btnNNTruong
            // 
            this.btnNNTruong.Location = new System.Drawing.Point(703, 83);
            this.btnNNTruong.Name = "btnNNTruong";
            this.btnNNTruong.Size = new System.Drawing.Size(34, 23);
            this.btnNNTruong.TabIndex = 15;
            this.btnNNTruong.Text = "...";
            this.btnNNTruong.ToolTip = "Thêm Trường đào tạo";
            this.btnNNTruong.Click += new System.EventHandler(this.btnNNTruong_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(346, 173);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 15);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Tên đề án";
            // 
            // txtNNDeAn
            // 
            this.txtNNDeAn.Location = new System.Drawing.Point(459, 170);
            this.txtNNDeAn.Name = "txtNNDeAn";
            this.txtNNDeAn.Properties.AllowFocused = false;
            this.txtNNDeAn.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNDeAn.Properties.Appearance.Options.UseFont = true;
            this.txtNNDeAn.Size = new System.Drawing.Size(271, 22);
            this.txtNNDeAn.TabIndex = 13;
            // 
            // chbNNHocBong
            // 
            this.chbNNHocBong.Location = new System.Drawing.Point(134, 170);
            this.chbNNHocBong.Name = "chbNNHocBong";
            this.chbNNHocBong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNNHocBong.Properties.Appearance.Options.UseFont = true;
            this.chbNNHocBong.Properties.Caption = "Học bổng nhà nước";
            this.chbNNHocBong.Size = new System.Drawing.Size(156, 20);
            this.chbNNHocBong.TabIndex = 12;
            // 
            // txtNNNgayCap
            // 
            this.txtNNNgayCap.Location = new System.Drawing.Point(886, 129);
            this.txtNNNgayCap.Name = "txtNNNgayCap";
            this.txtNNNgayCap.Properties.AllowFocused = false;
            this.txtNNNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtNNNgayCap.Size = new System.Drawing.Size(158, 22);
            this.txtNNNgayCap.TabIndex = 11;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(595, 132);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(21, 15);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "đến";
            // 
            // txtNNTgianden
            // 
            this.txtNNTgianden.Location = new System.Drawing.Point(625, 129);
            this.txtNNTgianden.Name = "txtNNTgianden";
            this.txtNNTgianden.Properties.AllowFocused = false;
            this.txtNNTgianden.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNTgianden.Properties.Appearance.Options.UseFont = true;
            this.txtNNTgianden.Size = new System.Drawing.Size(105, 22);
            this.txtNNTgianden.TabIndex = 9;
            // 
            // txtNNNoiCap
            // 
            this.txtNNNoiCap.Location = new System.Drawing.Point(136, 129);
            this.txtNNNoiCap.Name = "txtNNNoiCap";
            this.txtNNNoiCap.Properties.AllowFocused = false;
            this.txtNNNoiCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNNoiCap.Properties.Appearance.Options.UseFont = true;
            this.txtNNNoiCap.Size = new System.Drawing.Size(170, 22);
            this.txtNNNoiCap.TabIndex = 8;
            // 
            // btnNNSua
            // 
            this.btnNNSua.Location = new System.Drawing.Point(171, 200);
            this.btnNNSua.Name = "btnNNSua";
            this.btnNNSua.Size = new System.Drawing.Size(75, 23);
            this.btnNNSua.TabIndex = 7;
            this.btnNNSua.Text = "Hiệu chỉnh";
            this.btnNNSua.Click += new System.EventHandler(this.btnNNSua_Click);
            // 
            // btnNNXoa
            // 
            this.btnNNXoa.Location = new System.Drawing.Point(252, 200);
            this.btnNNXoa.Name = "btnNNXoa";
            this.btnNNXoa.Size = new System.Drawing.Size(75, 23);
            this.btnNNXoa.TabIndex = 0;
            this.btnNNXoa.Text = "Xóa";
            this.btnNNXoa.Click += new System.EventHandler(this.btnNNXoa_Click);
            // 
            // btnNNThem
            // 
            this.btnNNThem.Location = new System.Drawing.Point(90, 200);
            this.btnNNThem.Name = "btnNNThem";
            this.btnNNThem.Size = new System.Drawing.Size(75, 23);
            this.btnNNThem.TabIndex = 0;
            this.btnNNThem.Text = "Lưu";
            this.btnNNThem.Click += new System.EventHandler(this.btnNNThem_Click);
            // 
            // btnNNTaoMoi
            // 
            this.btnNNTaoMoi.Location = new System.Drawing.Point(9, 200);
            this.btnNNTaoMoi.Name = "btnNNTaoMoi";
            this.btnNNTaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnNNTaoMoi.TabIndex = 0;
            this.btnNNTaoMoi.Text = "Tạo mới";
            this.btnNNTaoMoi.Click += new System.EventHandler(this.btnNNTaoMoi_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(773, 132);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 15);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Ngày cấp";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(9, 132);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(44, 15);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Nơi cấp";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(9, 31);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(0, 15);
            this.labelControl7.TabIndex = 0;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(773, 87);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(100, 15);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Hình thức đào tạo";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(346, 132);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(94, 15);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "T.gian đào tạo từ";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(9, 87);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(90, 15);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Trình độ đào tạo";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(346, 87);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(88, 15);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Trường đào tạo";
            // 
            // txtNNTgianTu
            // 
            this.txtNNTgianTu.Location = new System.Drawing.Point(459, 129);
            this.txtNNTgianTu.Name = "txtNNTgianTu";
            this.txtNNTgianTu.Properties.AllowFocused = false;
            this.txtNNTgianTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNNTgianTu.Properties.Appearance.Options.UseFont = true;
            this.txtNNTgianTu.Size = new System.Drawing.Size(104, 22);
            this.txtNNTgianTu.TabIndex = 0;
            // 
            // cbbNNTrinhDo
            // 
            this.cbbNNTrinhDo.Location = new System.Drawing.Point(136, 84);
            this.cbbNNTrinhDo.Name = "cbbNNTrinhDo";
            this.cbbNNTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNNTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.cbbNNTrinhDo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNNTrinhDo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangChuyenMon", "Trình độ đào tạo")});
            this.cbbNNTrinhDo.Properties.DisplayMember = "TenLoaiBangChuyenMon";
            this.cbbNNTrinhDo.Properties.NullText = "";
            this.cbbNNTrinhDo.Properties.PopupSizeable = false;
            this.cbbNNTrinhDo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNNTrinhDo.Properties.ValueMember = "Id";
            this.cbbNNTrinhDo.Size = new System.Drawing.Size(170, 22);
            this.cbbNNTrinhDo.TabIndex = 4;
            // 
            // cbbNNHinhThuc
            // 
            this.cbbNNHinhThuc.Location = new System.Drawing.Point(886, 84);
            this.cbbNNHinhThuc.Name = "cbbNNHinhThuc";
            this.cbbNNHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNNHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbNNHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNNHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã hệ đào tạo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbNNHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbNNHinhThuc.Properties.NullText = "";
            this.cbbNNHinhThuc.Properties.PopupSizeable = false;
            this.cbbNNHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNNHinhThuc.Properties.ValueMember = "Id";
            this.cbbNNHinhThuc.Size = new System.Drawing.Size(158, 22);
            this.cbbNNHinhThuc.TabIndex = 4;
            // 
            // cbbNNTruong
            // 
            this.cbbNNTruong.Location = new System.Drawing.Point(459, 84);
            this.cbbNNTruong.Name = "cbbNNTruong";
            this.cbbNNTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNNTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbNNTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNNTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbNNTruong.Properties.DisplayMember = "TenTruong";
            this.cbbNNTruong.Properties.NullText = "";
            this.cbbNNTruong.Properties.PopupSizeable = false;
            this.cbbNNTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNNTruong.Properties.ValueMember = "MaTruong";
            this.cbbNNTruong.Size = new System.Drawing.Size(241, 22);
            this.cbbNNTruong.TabIndex = 4;
            // 
            // tabTinHoc
            // 
            this.tabTinHoc.Controls.Add(this.gvPg3TinHoc);
            this.tabTinHoc.Controls.Add(this.groupControl2);
            this.tabTinHoc.Location = new System.Drawing.Point(4, 22);
            this.tabTinHoc.Name = "tabTinHoc";
            this.tabTinHoc.Padding = new System.Windows.Forms.Padding(3);
            this.tabTinHoc.Size = new System.Drawing.Size(1272, 484);
            this.tabTinHoc.TabIndex = 2;
            this.tabTinHoc.Text = "Trình Độ Tin Học";
            this.tabTinHoc.UseVisualStyleBackColor = true;
            // 
            // gvPg3TinHoc
            // 
            this.gvPg3TinHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg3TinHoc.Location = new System.Drawing.Point(3, 187);
            this.gvPg3TinHoc.MainView = this.gridviewTinHoc;
            this.gvPg3TinHoc.Name = "gvPg3TinHoc";
            this.gvPg3TinHoc.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.gvPg3TinHoc.Size = new System.Drawing.Size(1266, 294);
            this.gvPg3TinHoc.TabIndex = 125;
            this.gvPg3TinHoc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewTinHoc});
            // 
            // gridviewTinHoc
            // 
            this.gridviewTinHoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColTHID,
            this.gridColTHLoaiTDCMID,
            this.gridColTHLoaiTDCM,
            this.gridColTHMaTruong,
            this.gridColTHTrinhDo,
            this.gridColTHHTDT,
            this.gridColTHTenHTDT,
            this.gridColTHden,
            this.gridColTHNoiCap,
            this.gridColTHNgayCap,
            this.gridColTHHocBong,
            this.gridColTHTenDeAn});
            this.gridviewTinHoc.GridControl = this.gvPg3TinHoc;
            this.gridviewTinHoc.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridviewTinHoc.Name = "gridviewTinHoc";
            this.gridviewTinHoc.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewTinHoc_RowClick);
            this.gridviewTinHoc.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewTinHoc_FocusedRowChanged);
            // 
            // gridColTHID
            // 
            this.gridColTHID.Caption = "Mã ID";
            this.gridColTHID.FieldName = "Id";
            this.gridColTHID.Name = "gridColTHID";
            this.gridColTHID.OptionsColumn.AllowEdit = false;
            this.gridColTHID.OptionsColumn.ReadOnly = true;
            // 
            // gridColTHLoaiTDCMID
            // 
            this.gridColTHLoaiTDCMID.Caption = "Mã B.Cấp/C.Chỉ";
            this.gridColTHLoaiTDCMID.FieldName = "LoaiTrinhDoTinHoc";
            this.gridColTHLoaiTDCMID.Name = "gridColTHLoaiTDCMID";
            this.gridColTHLoaiTDCMID.OptionsColumn.AllowEdit = false;
            this.gridColTHLoaiTDCMID.OptionsColumn.ReadOnly = true;
            this.gridColTHLoaiTDCMID.Width = 73;
            // 
            // gridColTHLoaiTDCM
            // 
            this.gridColTHLoaiTDCM.Caption = "Trình độ";
            this.gridColTHLoaiTDCM.FieldName = "LoaiBangTinHoc.TenLoaiBangTinHoc";
            this.gridColTHLoaiTDCM.Name = "gridColTHLoaiTDCM";
            this.gridColTHLoaiTDCM.OptionsColumn.AllowEdit = false;
            this.gridColTHLoaiTDCM.OptionsColumn.ReadOnly = true;
            this.gridColTHLoaiTDCM.Visible = true;
            this.gridColTHLoaiTDCM.VisibleIndex = 0;
            this.gridColTHLoaiTDCM.Width = 73;
            // 
            // gridColTHMaTruong
            // 
            this.gridColTHMaTruong.Caption = "Chuyên môn";
            this.gridColTHMaTruong.FieldName = "MaTruong";
            this.gridColTHMaTruong.Name = "gridColTHMaTruong";
            this.gridColTHMaTruong.OptionsColumn.AllowEdit = false;
            this.gridColTHMaTruong.OptionsColumn.ReadOnly = true;
            this.gridColTHMaTruong.Width = 73;
            // 
            // gridColTHTrinhDo
            // 
            this.gridColTHTrinhDo.Caption = "Trường đào tạo";
            this.gridColTHTrinhDo.FieldName = "Truong.TenTruong";
            this.gridColTHTrinhDo.Name = "gridColTHTrinhDo";
            this.gridColTHTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColTHTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColTHTrinhDo.Visible = true;
            this.gridColTHTrinhDo.VisibleIndex = 1;
            this.gridColTHTrinhDo.Width = 73;
            // 
            // gridColTHHTDT
            // 
            this.gridColTHHTDT.Caption = "Chuyên ngành";
            this.gridColTHHTDT.FieldName = "HinhThucDaoTao";
            this.gridColTHHTDT.Name = "gridColTHHTDT";
            this.gridColTHHTDT.OptionsColumn.AllowEdit = false;
            this.gridColTHHTDT.OptionsColumn.ReadOnly = true;
            this.gridColTHHTDT.Width = 73;
            // 
            // gridColTHTenHTDT
            // 
            this.gridColTHTenHTDT.Caption = "Hình thức đào tạo";
            this.gridColTHTenHTDT.FieldName = "HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao";
            this.gridColTHTenHTDT.Name = "gridColTHTenHTDT";
            this.gridColTHTenHTDT.OptionsColumn.AllowEdit = false;
            this.gridColTHTenHTDT.OptionsColumn.ReadOnly = true;
            this.gridColTHTenHTDT.Visible = true;
            this.gridColTHTenHTDT.VisibleIndex = 2;
            this.gridColTHTenHTDT.Width = 73;
            // 
            // gridColTHden
            // 
            this.gridColTHden.Caption = "T.gian đào tạo đến";
            this.gridColTHden.FieldName = "ThoiGianDaoTaoDen";
            this.gridColTHden.Name = "gridColTHden";
            this.gridColTHden.OptionsColumn.AllowEdit = false;
            this.gridColTHden.Visible = true;
            this.gridColTHden.VisibleIndex = 3;
            this.gridColTHden.Width = 73;
            // 
            // gridColTHNoiCap
            // 
            this.gridColTHNoiCap.Caption = "Nơi cấp";
            this.gridColTHNoiCap.FieldName = "NoiCapBang";
            this.gridColTHNoiCap.Name = "gridColTHNoiCap";
            this.gridColTHNoiCap.OptionsColumn.AllowEdit = false;
            this.gridColTHNoiCap.OptionsColumn.ReadOnly = true;
            this.gridColTHNoiCap.Visible = true;
            this.gridColTHNoiCap.VisibleIndex = 4;
            this.gridColTHNoiCap.Width = 73;
            // 
            // gridColTHNgayCap
            // 
            this.gridColTHNgayCap.Caption = "Ngày cấp";
            this.gridColTHNgayCap.FieldName = "NgayCapBang";
            this.gridColTHNgayCap.Name = "gridColTHNgayCap";
            this.gridColTHNgayCap.OptionsColumn.AllowEdit = false;
            this.gridColTHNgayCap.OptionsColumn.ReadOnly = true;
            this.gridColTHNgayCap.Visible = true;
            this.gridColTHNgayCap.VisibleIndex = 5;
            this.gridColTHNgayCap.Width = 98;
            // 
            // gridColTHHocBong
            // 
            this.gridColTHHocBong.Caption = "Học bổng nhà nước";
            this.gridColTHHocBong.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColTHHocBong.FieldName = "HocBongNhaNuoc";
            this.gridColTHHocBong.Name = "gridColTHHocBong";
            this.gridColTHHocBong.OptionsColumn.AllowEdit = false;
            this.gridColTHHocBong.OptionsColumn.ReadOnly = true;
            this.gridColTHHocBong.Visible = true;
            this.gridColTHHocBong.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // gridColTHTenDeAn
            // 
            this.gridColTHTenDeAn.Caption = "Tên đề án học bổng";
            this.gridColTHTenDeAn.FieldName = "TenDeAnHocBong";
            this.gridColTHTenDeAn.Name = "gridColTHTenDeAn";
            this.gridColTHTenDeAn.OptionsColumn.AllowEdit = false;
            this.gridColTHTenDeAn.OptionsColumn.ReadOnly = true;
            this.gridColTHTenDeAn.Visible = true;
            this.gridColTHTenDeAn.VisibleIndex = 7;
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.btnTHTruong);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.txtTHDeAn);
            this.groupControl2.Controls.Add(this.chbTHHocBong);
            this.groupControl2.Controls.Add(this.txtTHNgayCap);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.txtTHTgianden);
            this.groupControl2.Controls.Add(this.txtTHNoiCap);
            this.groupControl2.Controls.Add(this.btnTHSua);
            this.groupControl2.Controls.Add(this.btnTHXoa);
            this.groupControl2.Controls.Add(this.btnTHThem);
            this.groupControl2.Controls.Add(this.btnTHTaoMoi);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.labelControl20);
            this.groupControl2.Controls.Add(this.txtTHTgiantu);
            this.groupControl2.Controls.Add(this.cbbTHTrinhDo);
            this.groupControl2.Controls.Add(this.cbbTHHinhThuc);
            this.groupControl2.Controls.Add(this.cbbTHTruong);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1266, 184);
            this.groupControl2.TabIndex = 124;
            this.groupControl2.Text = "Thông tin trình độ tin học";
            // 
            // btnTHTruong
            // 
            this.btnTHTruong.Location = new System.Drawing.Point(703, 31);
            this.btnTHTruong.Name = "btnTHTruong";
            this.btnTHTruong.Size = new System.Drawing.Size(34, 23);
            this.btnTHTruong.TabIndex = 15;
            this.btnTHTruong.Text = "...";
            this.btnTHTruong.ToolTip = "Thêm Trường đào tạo";
            this.btnTHTruong.Click += new System.EventHandler(this.btnTHTruong_Click);
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(346, 121);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(55, 15);
            this.labelControl12.TabIndex = 14;
            this.labelControl12.Text = "Tên đề án";
            // 
            // txtTHDeAn
            // 
            this.txtTHDeAn.Location = new System.Drawing.Point(459, 118);
            this.txtTHDeAn.Name = "txtTHDeAn";
            this.txtTHDeAn.Properties.AllowFocused = false;
            this.txtTHDeAn.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHDeAn.Properties.Appearance.Options.UseFont = true;
            this.txtTHDeAn.Size = new System.Drawing.Size(271, 22);
            this.txtTHDeAn.TabIndex = 13;
            // 
            // chbTHHocBong
            // 
            this.chbTHHocBong.Location = new System.Drawing.Point(134, 118);
            this.chbTHHocBong.Name = "chbTHHocBong";
            this.chbTHHocBong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTHHocBong.Properties.Appearance.Options.UseFont = true;
            this.chbTHHocBong.Properties.Caption = "Học bổng nhà nước";
            this.chbTHHocBong.Size = new System.Drawing.Size(156, 20);
            this.chbTHHocBong.TabIndex = 12;
            // 
            // txtTHNgayCap
            // 
            this.txtTHNgayCap.Location = new System.Drawing.Point(886, 77);
            this.txtTHNgayCap.Name = "txtTHNgayCap";
            this.txtTHNgayCap.Properties.AllowFocused = false;
            this.txtTHNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtTHNgayCap.Size = new System.Drawing.Size(158, 22);
            this.txtTHNgayCap.TabIndex = 11;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(595, 80);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(21, 15);
            this.labelControl13.TabIndex = 10;
            this.labelControl13.Text = "đến";
            // 
            // txtTHTgianden
            // 
            this.txtTHTgianden.Location = new System.Drawing.Point(625, 77);
            this.txtTHTgianden.Name = "txtTHTgianden";
            this.txtTHTgianden.Properties.AllowFocused = false;
            this.txtTHTgianden.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHTgianden.Properties.Appearance.Options.UseFont = true;
            this.txtTHTgianden.Size = new System.Drawing.Size(105, 22);
            this.txtTHTgianden.TabIndex = 9;
            // 
            // txtTHNoiCap
            // 
            this.txtTHNoiCap.Location = new System.Drawing.Point(136, 77);
            this.txtTHNoiCap.Name = "txtTHNoiCap";
            this.txtTHNoiCap.Properties.AllowFocused = false;
            this.txtTHNoiCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHNoiCap.Properties.Appearance.Options.UseFont = true;
            this.txtTHNoiCap.Size = new System.Drawing.Size(170, 22);
            this.txtTHNoiCap.TabIndex = 8;
            // 
            // btnTHSua
            // 
            this.btnTHSua.Location = new System.Drawing.Point(171, 154);
            this.btnTHSua.Name = "btnTHSua";
            this.btnTHSua.Size = new System.Drawing.Size(75, 23);
            this.btnTHSua.TabIndex = 7;
            this.btnTHSua.Text = "Hiệu chỉnh";
            this.btnTHSua.Click += new System.EventHandler(this.btnTHSua_Click);
            // 
            // btnTHXoa
            // 
            this.btnTHXoa.Location = new System.Drawing.Point(252, 154);
            this.btnTHXoa.Name = "btnTHXoa";
            this.btnTHXoa.Size = new System.Drawing.Size(75, 23);
            this.btnTHXoa.TabIndex = 0;
            this.btnTHXoa.Text = "Xóa";
            this.btnTHXoa.Click += new System.EventHandler(this.btnTHXoa_Click);
            // 
            // btnTHThem
            // 
            this.btnTHThem.Location = new System.Drawing.Point(90, 154);
            this.btnTHThem.Name = "btnTHThem";
            this.btnTHThem.Size = new System.Drawing.Size(75, 23);
            this.btnTHThem.TabIndex = 0;
            this.btnTHThem.Text = "Lưu";
            this.btnTHThem.Click += new System.EventHandler(this.btnTHThem_Click);
            // 
            // btnTHTaoMoi
            // 
            this.btnTHTaoMoi.Location = new System.Drawing.Point(9, 154);
            this.btnTHTaoMoi.Name = "btnTHTaoMoi";
            this.btnTHTaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnTHTaoMoi.TabIndex = 0;
            this.btnTHTaoMoi.Text = "Tạo mới";
            this.btnTHTaoMoi.Click += new System.EventHandler(this.btnTHTaoMoi_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(790, 80);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(52, 15);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "Ngày cấp";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(9, 80);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(44, 15);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Nơi cấp";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(9, 31);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(0, 15);
            this.labelControl16.TabIndex = 0;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(773, 35);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(100, 15);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "Hình thức đào tạo";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(346, 80);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(94, 15);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "T.gian đào tạo từ";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(9, 35);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(94, 15);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Trình độ tin học *";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(346, 35);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(96, 15);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "Trường đào tạo *";
            // 
            // txtTHTgiantu
            // 
            this.txtTHTgiantu.Location = new System.Drawing.Point(459, 77);
            this.txtTHTgiantu.Name = "txtTHTgiantu";
            this.txtTHTgiantu.Properties.AllowFocused = false;
            this.txtTHTgiantu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTHTgiantu.Properties.Appearance.Options.UseFont = true;
            this.txtTHTgiantu.Size = new System.Drawing.Size(104, 22);
            this.txtTHTgiantu.TabIndex = 0;
            // 
            // cbbTHTrinhDo
            // 
            this.cbbTHTrinhDo.Location = new System.Drawing.Point(136, 32);
            this.cbbTHTrinhDo.Name = "cbbTHTrinhDo";
            this.cbbTHTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTHTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.cbbTHTrinhDo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbTHTrinhDo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangTinHoc", "Tin học")});
            this.cbbTHTrinhDo.Properties.DisplayMember = "TenLoaiBangTinHoc";
            this.cbbTHTrinhDo.Properties.NullText = "";
            this.cbbTHTrinhDo.Properties.PopupSizeable = false;
            this.cbbTHTrinhDo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbTHTrinhDo.Properties.ValueMember = "Id";
            this.cbbTHTrinhDo.Size = new System.Drawing.Size(170, 22);
            this.cbbTHTrinhDo.TabIndex = 4;
            // 
            // cbbTHHinhThuc
            // 
            this.cbbTHHinhThuc.Location = new System.Drawing.Point(886, 32);
            this.cbbTHHinhThuc.Name = "cbbTHHinhThuc";
            this.cbbTHHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTHHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbTHHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbTHHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã hệ đào tạo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbTHHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbTHHinhThuc.Properties.NullText = "";
            this.cbbTHHinhThuc.Properties.PopupSizeable = false;
            this.cbbTHHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbTHHinhThuc.Properties.ValueMember = "Id";
            this.cbbTHHinhThuc.Size = new System.Drawing.Size(158, 22);
            this.cbbTHHinhThuc.TabIndex = 4;
            // 
            // cbbTHTruong
            // 
            this.cbbTHTruong.Location = new System.Drawing.Point(459, 32);
            this.cbbTHTruong.Name = "cbbTHTruong";
            this.cbbTHTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTHTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbTHTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbTHTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbTHTruong.Properties.DisplayMember = "TenTruong";
            this.cbbTHTruong.Properties.NullText = "";
            this.cbbTHTruong.Properties.PopupSizeable = false;
            this.cbbTHTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbTHTruong.Properties.ValueMember = "MaTruong";
            this.cbbTHTruong.Size = new System.Drawing.Size(241, 22);
            this.cbbTHTruong.TabIndex = 4;
            // 
            // tabNghiepVu
            // 
            this.tabNghiepVu.Controls.Add(this.gvPg3NghiepVu);
            this.tabNghiepVu.Controls.Add(this.groupControl3);
            this.tabNghiepVu.Location = new System.Drawing.Point(4, 22);
            this.tabNghiepVu.Name = "tabNghiepVu";
            this.tabNghiepVu.Padding = new System.Windows.Forms.Padding(3);
            this.tabNghiepVu.Size = new System.Drawing.Size(1272, 484);
            this.tabNghiepVu.TabIndex = 3;
            this.tabNghiepVu.Text = "Trình Độ Nghiệp Vụ";
            this.tabNghiepVu.UseVisualStyleBackColor = true;
            // 
            // gvPg3NghiepVu
            // 
            this.gvPg3NghiepVu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg3NghiepVu.Location = new System.Drawing.Point(3, 187);
            this.gvPg3NghiepVu.MainView = this.gridviewNghiepVu;
            this.gvPg3NghiepVu.Name = "gvPg3NghiepVu";
            this.gvPg3NghiepVu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            this.gvPg3NghiepVu.Size = new System.Drawing.Size(1266, 294);
            this.gvPg3NghiepVu.TabIndex = 125;
            this.gvPg3NghiepVu.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewNghiepVu});
            // 
            // gridviewNghiepVu
            // 
            this.gridviewNghiepVu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNVID,
            this.gridColNVLoaiTDCM,
            this.gridColLoaiNVSP,
            this.gridColNVMaTruong,
            this.gridColNVTrinhDo,
            this.gridColNVHTDT,
            this.gridColNVTenHTDT,
            this.gridColNVChuyennganh,
            this.gridColNVTu,
            this.gridColNVden,
            this.gridColNVNoiCap,
            this.gridColNVNgayCap,
            this.gridColNVHocBong,
            this.gridColNVTenDeAn});
            this.gridviewNghiepVu.GridControl = this.gvPg3NghiepVu;
            this.gridviewNghiepVu.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridviewNghiepVu.Name = "gridviewNghiepVu";
            this.gridviewNghiepVu.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewNghiepVu_RowClick);
            this.gridviewNghiepVu.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewNghiepVu_FocusedRowChanged);
            // 
            // gridColNVID
            // 
            this.gridColNVID.Caption = "Mã ID";
            this.gridColNVID.FieldName = "Id";
            this.gridColNVID.Name = "gridColNVID";
            this.gridColNVID.OptionsColumn.AllowEdit = false;
            this.gridColNVID.OptionsColumn.ReadOnly = true;
            // 
            // gridColNVLoaiTDCM
            // 
            this.gridColNVLoaiTDCM.Caption = "Nghiệp vụ";
            this.gridColNVLoaiTDCM.FieldName = "LoaiNghiepVu";
            this.gridColNVLoaiTDCM.Name = "gridColNVLoaiTDCM";
            this.gridColNVLoaiTDCM.OptionsColumn.AllowEdit = false;
            this.gridColNVLoaiTDCM.OptionsColumn.ReadOnly = true;
            this.gridColNVLoaiTDCM.Visible = true;
            this.gridColNVLoaiTDCM.VisibleIndex = 0;
            this.gridColNVLoaiTDCM.Width = 73;
            // 
            // gridColLoaiNVSP
            // 
            this.gridColLoaiNVSP.Caption = "Là nghiệp vụ sư phạm";
            this.gridColLoaiNVSP.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColLoaiNVSP.FieldName = "LoaiNghiepVuSuPham";
            this.gridColLoaiNVSP.Name = "gridColLoaiNVSP";
            this.gridColLoaiNVSP.OptionsColumn.AllowEdit = false;
            this.gridColLoaiNVSP.OptionsColumn.ReadOnly = true;
            this.gridColLoaiNVSP.Visible = true;
            this.gridColLoaiNVSP.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // gridColNVMaTruong
            // 
            this.gridColNVMaTruong.Caption = "Chuyên môn";
            this.gridColNVMaTruong.FieldName = "MaTruong";
            this.gridColNVMaTruong.Name = "gridColNVMaTruong";
            this.gridColNVMaTruong.OptionsColumn.AllowEdit = false;
            this.gridColNVMaTruong.OptionsColumn.ReadOnly = true;
            this.gridColNVMaTruong.Width = 73;
            // 
            // gridColNVTrinhDo
            // 
            this.gridColNVTrinhDo.Caption = "Trường đào tạo";
            this.gridColNVTrinhDo.FieldName = "Truong.TenTruong";
            this.gridColNVTrinhDo.Name = "gridColNVTrinhDo";
            this.gridColNVTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColNVTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColNVTrinhDo.Visible = true;
            this.gridColNVTrinhDo.VisibleIndex = 2;
            this.gridColNVTrinhDo.Width = 73;
            // 
            // gridColNVHTDT
            // 
            this.gridColNVHTDT.Caption = "Chuyên ngành";
            this.gridColNVHTDT.FieldName = "HinhThucDaoTao";
            this.gridColNVHTDT.Name = "gridColNVHTDT";
            this.gridColNVHTDT.OptionsColumn.AllowEdit = false;
            this.gridColNVHTDT.OptionsColumn.ReadOnly = true;
            this.gridColNVHTDT.Width = 73;
            // 
            // gridColNVTenHTDT
            // 
            this.gridColNVTenHTDT.Caption = "Hình thức đào tạo";
            this.gridColNVTenHTDT.FieldName = "HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao";
            this.gridColNVTenHTDT.Name = "gridColNVTenHTDT";
            this.gridColNVTenHTDT.OptionsColumn.AllowEdit = false;
            this.gridColNVTenHTDT.OptionsColumn.ReadOnly = true;
            this.gridColNVTenHTDT.Visible = true;
            this.gridColNVTenHTDT.VisibleIndex = 3;
            this.gridColNVTenHTDT.Width = 73;
            // 
            // gridColNVChuyennganh
            // 
            this.gridColNVChuyennganh.Caption = "Chuyên ngành đào tạo";
            this.gridColNVChuyennganh.FieldName = "ChuyenNganhDaoTao";
            this.gridColNVChuyennganh.Name = "gridColNVChuyennganh";
            this.gridColNVChuyennganh.OptionsColumn.AllowEdit = false;
            this.gridColNVChuyennganh.OptionsColumn.ReadOnly = true;
            this.gridColNVChuyennganh.Visible = true;
            this.gridColNVChuyennganh.VisibleIndex = 4;
            this.gridColNVChuyennganh.Width = 73;
            // 
            // gridColNVTu
            // 
            this.gridColNVTu.Caption = "T.gian đào tạo từ";
            this.gridColNVTu.FieldName = "ThoiGianDaoTaoTu";
            this.gridColNVTu.Name = "gridColNVTu";
            this.gridColNVTu.OptionsColumn.AllowEdit = false;
            this.gridColNVTu.OptionsColumn.ReadOnly = true;
            this.gridColNVTu.Visible = true;
            this.gridColNVTu.VisibleIndex = 5;
            this.gridColNVTu.Width = 73;
            // 
            // gridColNVden
            // 
            this.gridColNVden.Caption = "T.gian đào tạo đến";
            this.gridColNVden.FieldName = "ThoiGianDaoTaoDen";
            this.gridColNVden.Name = "gridColNVden";
            this.gridColNVden.OptionsColumn.AllowEdit = false;
            this.gridColNVden.Visible = true;
            this.gridColNVden.VisibleIndex = 6;
            this.gridColNVden.Width = 73;
            // 
            // gridColNVNoiCap
            // 
            this.gridColNVNoiCap.Caption = "Nơi cấp";
            this.gridColNVNoiCap.FieldName = "NoiCapBang";
            this.gridColNVNoiCap.Name = "gridColNVNoiCap";
            this.gridColNVNoiCap.OptionsColumn.AllowEdit = false;
            this.gridColNVNoiCap.OptionsColumn.ReadOnly = true;
            this.gridColNVNoiCap.Visible = true;
            this.gridColNVNoiCap.VisibleIndex = 7;
            this.gridColNVNoiCap.Width = 73;
            // 
            // gridColNVNgayCap
            // 
            this.gridColNVNgayCap.Caption = "Ngày cấp";
            this.gridColNVNgayCap.FieldName = "NgayCapBang";
            this.gridColNVNgayCap.Name = "gridColNVNgayCap";
            this.gridColNVNgayCap.OptionsColumn.AllowEdit = false;
            this.gridColNVNgayCap.OptionsColumn.ReadOnly = true;
            this.gridColNVNgayCap.Visible = true;
            this.gridColNVNgayCap.VisibleIndex = 8;
            this.gridColNVNgayCap.Width = 98;
            // 
            // gridColNVHocBong
            // 
            this.gridColNVHocBong.Caption = "Học bổng nhà nước";
            this.gridColNVHocBong.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColNVHocBong.FieldName = "HocBongNhaNuoc";
            this.gridColNVHocBong.Name = "gridColNVHocBong";
            this.gridColNVHocBong.OptionsColumn.AllowEdit = false;
            this.gridColNVHocBong.OptionsColumn.ReadOnly = true;
            this.gridColNVHocBong.Visible = true;
            this.gridColNVHocBong.VisibleIndex = 9;
            // 
            // gridColNVTenDeAn
            // 
            this.gridColNVTenDeAn.Caption = "Tên đề án học bổng";
            this.gridColNVTenDeAn.FieldName = "TenDeAnHocBong";
            this.gridColNVTenDeAn.Name = "gridColNVTenDeAn";
            this.gridColNVTenDeAn.OptionsColumn.AllowEdit = false;
            this.gridColNVTenDeAn.OptionsColumn.ReadOnly = true;
            this.gridColNVTenDeAn.Visible = true;
            this.gridColNVTenDeAn.VisibleIndex = 10;
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.chbNVSP);
            this.groupControl3.Controls.Add(this.txtNVTrinhDo);
            this.groupControl3.Controls.Add(this.btnNVTruong);
            this.groupControl3.Controls.Add(this.labelControl21);
            this.groupControl3.Controls.Add(this.txtNVDeAn);
            this.groupControl3.Controls.Add(this.chbNVHocBong);
            this.groupControl3.Controls.Add(this.txtNVNgayCap);
            this.groupControl3.Controls.Add(this.labelControl22);
            this.groupControl3.Controls.Add(this.txtNVTgianden);
            this.groupControl3.Controls.Add(this.txtNVNganh);
            this.groupControl3.Controls.Add(this.btnNVSua);
            this.groupControl3.Controls.Add(this.btnNVXoa);
            this.groupControl3.Controls.Add(this.btnNVThem);
            this.groupControl3.Controls.Add(this.btnNVTaoMoi);
            this.groupControl3.Controls.Add(this.labelControl23);
            this.groupControl3.Controls.Add(this.labelControl24);
            this.groupControl3.Controls.Add(this.labelControl25);
            this.groupControl3.Controls.Add(this.labelControl26);
            this.groupControl3.Controls.Add(this.labelControl27);
            this.groupControl3.Controls.Add(this.labelControl28);
            this.groupControl3.Controls.Add(this.labelControl29);
            this.groupControl3.Controls.Add(this.txtNVTgiantu);
            this.groupControl3.Controls.Add(this.cbbNVHinhThuc);
            this.groupControl3.Controls.Add(this.cbbNVTruong);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(3, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1266, 184);
            this.groupControl3.TabIndex = 124;
            this.groupControl3.Text = "Thông tin trình độ nghiệp vụ";
            // 
            // chbNVSP
            // 
            this.chbNVSP.Location = new System.Drawing.Point(771, 120);
            this.chbNVSP.Name = "chbNVSP";
            this.chbNVSP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNVSP.Properties.Appearance.Options.UseFont = true;
            this.chbNVSP.Properties.Caption = "Là chứng chỉ nghiệp vụ sư phạm";
            this.chbNVSP.Size = new System.Drawing.Size(237, 20);
            this.chbNVSP.TabIndex = 17;
            // 
            // txtNVTrinhDo
            // 
            this.txtNVTrinhDo.Location = new System.Drawing.Point(136, 32);
            this.txtNVTrinhDo.Name = "txtNVTrinhDo";
            this.txtNVTrinhDo.Properties.AllowFocused = false;
            this.txtNVTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.txtNVTrinhDo.Size = new System.Drawing.Size(170, 22);
            this.txtNVTrinhDo.TabIndex = 16;
            // 
            // btnNVTruong
            // 
            this.btnNVTruong.Location = new System.Drawing.Point(703, 31);
            this.btnNVTruong.Name = "btnNVTruong";
            this.btnNVTruong.Size = new System.Drawing.Size(34, 23);
            this.btnNVTruong.TabIndex = 15;
            this.btnNVTruong.Text = "...";
            this.btnNVTruong.ToolTip = "Thêm Trường đào tạo";
            this.btnNVTruong.Click += new System.EventHandler(this.btnNVTruong_Click);
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(346, 121);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(55, 15);
            this.labelControl21.TabIndex = 14;
            this.labelControl21.Text = "Tên đề án";
            // 
            // txtNVDeAn
            // 
            this.txtNVDeAn.Location = new System.Drawing.Point(459, 118);
            this.txtNVDeAn.Name = "txtNVDeAn";
            this.txtNVDeAn.Properties.AllowFocused = false;
            this.txtNVDeAn.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVDeAn.Properties.Appearance.Options.UseFont = true;
            this.txtNVDeAn.Size = new System.Drawing.Size(271, 22);
            this.txtNVDeAn.TabIndex = 13;
            // 
            // chbNVHocBong
            // 
            this.chbNVHocBong.Location = new System.Drawing.Point(134, 118);
            this.chbNVHocBong.Name = "chbNVHocBong";
            this.chbNVHocBong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbNVHocBong.Properties.Appearance.Options.UseFont = true;
            this.chbNVHocBong.Properties.Caption = "Học bổng nhà nước";
            this.chbNVHocBong.Size = new System.Drawing.Size(156, 20);
            this.chbNVHocBong.TabIndex = 12;
            // 
            // txtNVNgayCap
            // 
            this.txtNVNgayCap.Location = new System.Drawing.Point(886, 77);
            this.txtNVNgayCap.Name = "txtNVNgayCap";
            this.txtNVNgayCap.Properties.AllowFocused = false;
            this.txtNVNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtNVNgayCap.Size = new System.Drawing.Size(158, 22);
            this.txtNVNgayCap.TabIndex = 11;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(595, 80);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(21, 15);
            this.labelControl22.TabIndex = 10;
            this.labelControl22.Text = "đến";
            // 
            // txtNVTgianden
            // 
            this.txtNVTgianden.Location = new System.Drawing.Point(625, 77);
            this.txtNVTgianden.Name = "txtNVTgianden";
            this.txtNVTgianden.Properties.AllowFocused = false;
            this.txtNVTgianden.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVTgianden.Properties.Appearance.Options.UseFont = true;
            this.txtNVTgianden.Size = new System.Drawing.Size(105, 22);
            this.txtNVTgianden.TabIndex = 9;
            // 
            // txtNVNganh
            // 
            this.txtNVNganh.Location = new System.Drawing.Point(136, 77);
            this.txtNVNganh.Name = "txtNVNganh";
            this.txtNVNganh.Properties.AllowFocused = false;
            this.txtNVNganh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVNganh.Properties.Appearance.Options.UseFont = true;
            this.txtNVNganh.Size = new System.Drawing.Size(170, 22);
            this.txtNVNganh.TabIndex = 8;
            // 
            // btnNVSua
            // 
            this.btnNVSua.Location = new System.Drawing.Point(171, 154);
            this.btnNVSua.Name = "btnNVSua";
            this.btnNVSua.Size = new System.Drawing.Size(75, 23);
            this.btnNVSua.TabIndex = 7;
            this.btnNVSua.Text = "Hiệu chỉnh";
            this.btnNVSua.Click += new System.EventHandler(this.btnNVSua_Click);
            // 
            // btnNVXoa
            // 
            this.btnNVXoa.Location = new System.Drawing.Point(252, 154);
            this.btnNVXoa.Name = "btnNVXoa";
            this.btnNVXoa.Size = new System.Drawing.Size(75, 23);
            this.btnNVXoa.TabIndex = 0;
            this.btnNVXoa.Text = "Xóa";
            this.btnNVXoa.Click += new System.EventHandler(this.btnNVXoa_Click);
            // 
            // btnNVThem
            // 
            this.btnNVThem.Location = new System.Drawing.Point(90, 154);
            this.btnNVThem.Name = "btnNVThem";
            this.btnNVThem.Size = new System.Drawing.Size(75, 23);
            this.btnNVThem.TabIndex = 0;
            this.btnNVThem.Text = "Lưu";
            this.btnNVThem.Click += new System.EventHandler(this.btnNVThem_Click);
            // 
            // btnNVTaoMoi
            // 
            this.btnNVTaoMoi.Location = new System.Drawing.Point(9, 154);
            this.btnNVTaoMoi.Name = "btnNVTaoMoi";
            this.btnNVTaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnNVTaoMoi.TabIndex = 0;
            this.btnNVTaoMoi.Text = "Tạo mới";
            this.btnNVTaoMoi.Click += new System.EventHandler(this.btnNVTaoMoi_Click);
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Location = new System.Drawing.Point(773, 80);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 15);
            this.labelControl23.TabIndex = 0;
            this.labelControl23.Text = "Ngày cấp";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(5, 80);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(125, 15);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "Chuyên ngành đào tạo";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Location = new System.Drawing.Point(9, 31);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(0, 15);
            this.labelControl25.TabIndex = 0;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Location = new System.Drawing.Point(773, 35);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(100, 15);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Hình thức đào tạo";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(346, 80);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(94, 15);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "T.gian đào tạo từ";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Location = new System.Drawing.Point(9, 35);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(63, 15);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Nghiệp vụ *";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(346, 35);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(96, 15);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "Trường đào tạo *";
            // 
            // txtNVTgiantu
            // 
            this.txtNVTgiantu.Location = new System.Drawing.Point(459, 77);
            this.txtNVTgiantu.Name = "txtNVTgiantu";
            this.txtNVTgiantu.Properties.AllowFocused = false;
            this.txtNVTgiantu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNVTgiantu.Properties.Appearance.Options.UseFont = true;
            this.txtNVTgiantu.Size = new System.Drawing.Size(104, 22);
            this.txtNVTgiantu.TabIndex = 0;
            // 
            // cbbNVHinhThuc
            // 
            this.cbbNVHinhThuc.Location = new System.Drawing.Point(886, 32);
            this.cbbNVHinhThuc.Name = "cbbNVHinhThuc";
            this.cbbNVHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNVHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbNVHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNVHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã hệ đào tạo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbNVHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbNVHinhThuc.Properties.NullText = "";
            this.cbbNVHinhThuc.Properties.PopupSizeable = false;
            this.cbbNVHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNVHinhThuc.Properties.ValueMember = "Id";
            this.cbbNVHinhThuc.Size = new System.Drawing.Size(158, 22);
            this.cbbNVHinhThuc.TabIndex = 4;
            // 
            // cbbNVTruong
            // 
            this.cbbNVTruong.Location = new System.Drawing.Point(459, 32);
            this.cbbNVTruong.Name = "cbbNVTruong";
            this.cbbNVTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNVTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbNVTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbNVTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbNVTruong.Properties.DisplayMember = "TenTruong";
            this.cbbNVTruong.Properties.NullText = "";
            this.cbbNVTruong.Properties.PopupSizeable = false;
            this.cbbNVTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbNVTruong.Properties.ValueMember = "MaTruong";
            this.cbbNVTruong.Size = new System.Drawing.Size(241, 22);
            this.cbbNVTruong.TabIndex = 4;
            // 
            // tabLyLuanCT
            // 
            this.tabLyLuanCT.Controls.Add(this.gvPg3LLCT);
            this.tabLyLuanCT.Controls.Add(this.groupControl4);
            this.tabLyLuanCT.Location = new System.Drawing.Point(4, 22);
            this.tabLyLuanCT.Name = "tabLyLuanCT";
            this.tabLyLuanCT.Padding = new System.Windows.Forms.Padding(3);
            this.tabLyLuanCT.Size = new System.Drawing.Size(1272, 484);
            this.tabLyLuanCT.TabIndex = 4;
            this.tabLyLuanCT.Text = "Lý Luận Chính Trị";
            this.tabLyLuanCT.UseVisualStyleBackColor = true;
            // 
            // gvPg3LLCT
            // 
            this.gvPg3LLCT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg3LLCT.Location = new System.Drawing.Point(3, 187);
            this.gvPg3LLCT.MainView = this.gridviewLLCT;
            this.gvPg3LLCT.Name = "gvPg3LLCT";
            this.gvPg3LLCT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit5});
            this.gvPg3LLCT.Size = new System.Drawing.Size(1266, 294);
            this.gvPg3LLCT.TabIndex = 125;
            this.gvPg3LLCT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewLLCT});
            // 
            // gridviewLLCT
            // 
            this.gridviewLLCT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColLLID,
            this.gridColLLLoaiTDCMID,
            this.gridColLLLoaiTDCM,
            this.gridColLLMaTruong,
            this.gridColLLTrinhDo,
            this.gridColLLHTDT,
            this.gridColLLTenHTDT,
            this.gridColLLChuyennganh,
            this.gridColLLTu,
            this.gridColLLden,
            this.gridColLLNoiCap,
            this.gridColLLNgayCap,
            this.gridColLLHocBong,
            this.gridColLLTenDeAn});
            this.gridviewLLCT.GridControl = this.gvPg3LLCT;
            this.gridviewLLCT.GroupPanelText = "Danh sách Bằng cấp - Chứng chỉ";
            this.gridviewLLCT.Name = "gridviewLLCT";
            this.gridviewLLCT.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewLLCT_RowClick);
            this.gridviewLLCT.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewLLCT_FocusedRowChanged);
            // 
            // gridColLLID
            // 
            this.gridColLLID.Caption = "Mã ID";
            this.gridColLLID.FieldName = "Id";
            this.gridColLLID.Name = "gridColLLID";
            this.gridColLLID.OptionsColumn.AllowEdit = false;
            this.gridColLLID.OptionsColumn.ReadOnly = true;
            // 
            // gridColLLLoaiTDCMID
            // 
            this.gridColLLLoaiTDCMID.Caption = "Mã B.Cấp/C.Chỉ";
            this.gridColLLLoaiTDCMID.FieldName = "LoaiLyLuanChinhTri";
            this.gridColLLLoaiTDCMID.Name = "gridColLLLoaiTDCMID";
            this.gridColLLLoaiTDCMID.OptionsColumn.AllowEdit = false;
            this.gridColLLLoaiTDCMID.OptionsColumn.ReadOnly = true;
            this.gridColLLLoaiTDCMID.Width = 73;
            // 
            // gridColLLLoaiTDCM
            // 
            this.gridColLLLoaiTDCM.Caption = "Lý luận chính trị";
            this.gridColLLLoaiTDCM.FieldName = "LoaiBangLyLuanChinhTri.TenLoaiBangLyLuanChinhTri";
            this.gridColLLLoaiTDCM.Name = "gridColLLLoaiTDCM";
            this.gridColLLLoaiTDCM.OptionsColumn.AllowEdit = false;
            this.gridColLLLoaiTDCM.OptionsColumn.ReadOnly = true;
            this.gridColLLLoaiTDCM.Visible = true;
            this.gridColLLLoaiTDCM.VisibleIndex = 0;
            this.gridColLLLoaiTDCM.Width = 73;
            // 
            // gridColLLMaTruong
            // 
            this.gridColLLMaTruong.Caption = "Chuyên môn";
            this.gridColLLMaTruong.FieldName = "MaTruong";
            this.gridColLLMaTruong.Name = "gridColLLMaTruong";
            this.gridColLLMaTruong.OptionsColumn.AllowEdit = false;
            this.gridColLLMaTruong.OptionsColumn.ReadOnly = true;
            this.gridColLLMaTruong.Width = 73;
            // 
            // gridColLLTrinhDo
            // 
            this.gridColLLTrinhDo.Caption = "Trường đào tạo";
            this.gridColLLTrinhDo.FieldName = "Truong.TenTruong";
            this.gridColLLTrinhDo.Name = "gridColLLTrinhDo";
            this.gridColLLTrinhDo.OptionsColumn.AllowEdit = false;
            this.gridColLLTrinhDo.OptionsColumn.ReadOnly = true;
            this.gridColLLTrinhDo.Visible = true;
            this.gridColLLTrinhDo.VisibleIndex = 1;
            this.gridColLLTrinhDo.Width = 73;
            // 
            // gridColLLHTDT
            // 
            this.gridColLLHTDT.Caption = "Chuyên ngành";
            this.gridColLLHTDT.FieldName = "HinhThucDaoTao";
            this.gridColLLHTDT.Name = "gridColLLHTDT";
            this.gridColLLHTDT.OptionsColumn.AllowEdit = false;
            this.gridColLLHTDT.OptionsColumn.ReadOnly = true;
            this.gridColLLHTDT.Width = 73;
            // 
            // gridColLLTenHTDT
            // 
            this.gridColLLTenHTDT.Caption = "Hình thức đào tạo";
            this.gridColLLTenHTDT.FieldName = "HinhThucDaoTaoOBJ.TenLoaiHinhThucDaoTao";
            this.gridColLLTenHTDT.Name = "gridColLLTenHTDT";
            this.gridColLLTenHTDT.OptionsColumn.AllowEdit = false;
            this.gridColLLTenHTDT.OptionsColumn.ReadOnly = true;
            this.gridColLLTenHTDT.Visible = true;
            this.gridColLLTenHTDT.VisibleIndex = 2;
            this.gridColLLTenHTDT.Width = 73;
            // 
            // gridColLLChuyennganh
            // 
            this.gridColLLChuyennganh.Caption = "Chuyên ngành đào tạo";
            this.gridColLLChuyennganh.FieldName = "ChuyenNganhDaoTao";
            this.gridColLLChuyennganh.Name = "gridColLLChuyennganh";
            this.gridColLLChuyennganh.OptionsColumn.AllowEdit = false;
            this.gridColLLChuyennganh.OptionsColumn.ReadOnly = true;
            this.gridColLLChuyennganh.Visible = true;
            this.gridColLLChuyennganh.VisibleIndex = 3;
            this.gridColLLChuyennganh.Width = 73;
            // 
            // gridColLLTu
            // 
            this.gridColLLTu.Caption = "T.gian đào tạo từ";
            this.gridColLLTu.FieldName = "ThoiGianDaoTaoTu";
            this.gridColLLTu.Name = "gridColLLTu";
            this.gridColLLTu.OptionsColumn.AllowEdit = false;
            this.gridColLLTu.OptionsColumn.ReadOnly = true;
            this.gridColLLTu.Visible = true;
            this.gridColLLTu.VisibleIndex = 4;
            this.gridColLLTu.Width = 73;
            // 
            // gridColLLden
            // 
            this.gridColLLden.Caption = "T.gian đào tạo đến";
            this.gridColLLden.FieldName = "ThoiGianDaoTaoDen";
            this.gridColLLden.Name = "gridColLLden";
            this.gridColLLden.OptionsColumn.AllowEdit = false;
            this.gridColLLden.Visible = true;
            this.gridColLLden.VisibleIndex = 5;
            this.gridColLLden.Width = 73;
            // 
            // gridColLLNoiCap
            // 
            this.gridColLLNoiCap.Caption = "Nơi cấp";
            this.gridColLLNoiCap.FieldName = "NoiCapBang";
            this.gridColLLNoiCap.Name = "gridColLLNoiCap";
            this.gridColLLNoiCap.OptionsColumn.AllowEdit = false;
            this.gridColLLNoiCap.OptionsColumn.ReadOnly = true;
            this.gridColLLNoiCap.Visible = true;
            this.gridColLLNoiCap.VisibleIndex = 6;
            this.gridColLLNoiCap.Width = 73;
            // 
            // gridColLLNgayCap
            // 
            this.gridColLLNgayCap.Caption = "Ngày cấp";
            this.gridColLLNgayCap.FieldName = "NgayCapBang";
            this.gridColLLNgayCap.Name = "gridColLLNgayCap";
            this.gridColLLNgayCap.OptionsColumn.AllowEdit = false;
            this.gridColLLNgayCap.OptionsColumn.ReadOnly = true;
            this.gridColLLNgayCap.Visible = true;
            this.gridColLLNgayCap.VisibleIndex = 7;
            this.gridColLLNgayCap.Width = 98;
            // 
            // gridColLLHocBong
            // 
            this.gridColLLHocBong.Caption = "Học bổng nhà nước";
            this.gridColLLHocBong.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColLLHocBong.FieldName = "HocBongNhaNuoc";
            this.gridColLLHocBong.Name = "gridColLLHocBong";
            this.gridColLLHocBong.OptionsColumn.AllowEdit = false;
            this.gridColLLHocBong.OptionsColumn.ReadOnly = true;
            this.gridColLLHocBong.Visible = true;
            this.gridColLLHocBong.VisibleIndex = 8;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // gridColLLTenDeAn
            // 
            this.gridColLLTenDeAn.Caption = "Tên đề án học bổng";
            this.gridColLLTenDeAn.FieldName = "TenDeAnHocBong";
            this.gridColLLTenDeAn.Name = "gridColLLTenDeAn";
            this.gridColLLTenDeAn.OptionsColumn.AllowEdit = false;
            this.gridColLLTenDeAn.OptionsColumn.ReadOnly = true;
            this.gridColLLTenDeAn.Visible = true;
            this.gridColLLTenDeAn.VisibleIndex = 9;
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.btnLLTruong);
            this.groupControl4.Controls.Add(this.labelControl30);
            this.groupControl4.Controls.Add(this.txtLLDeAn);
            this.groupControl4.Controls.Add(this.chbLLHocBong);
            this.groupControl4.Controls.Add(this.txtLLNgayCap);
            this.groupControl4.Controls.Add(this.labelControl31);
            this.groupControl4.Controls.Add(this.txtLLTgianden);
            this.groupControl4.Controls.Add(this.txtLLNganh);
            this.groupControl4.Controls.Add(this.btnLLSua);
            this.groupControl4.Controls.Add(this.btnLLXoa);
            this.groupControl4.Controls.Add(this.btnLLThem);
            this.groupControl4.Controls.Add(this.btnLLTaoMoi);
            this.groupControl4.Controls.Add(this.labelControl32);
            this.groupControl4.Controls.Add(this.labelControl33);
            this.groupControl4.Controls.Add(this.labelControl34);
            this.groupControl4.Controls.Add(this.labelControl35);
            this.groupControl4.Controls.Add(this.labelControl36);
            this.groupControl4.Controls.Add(this.labelControl37);
            this.groupControl4.Controls.Add(this.labelControl38);
            this.groupControl4.Controls.Add(this.txtLLTgiantu);
            this.groupControl4.Controls.Add(this.cbbLLTrinhDo);
            this.groupControl4.Controls.Add(this.cbbLLHinhThuc);
            this.groupControl4.Controls.Add(this.cbbLLTruong);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(3, 3);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1266, 184);
            this.groupControl4.TabIndex = 124;
            this.groupControl4.Text = "Thông tin trình độ lý luận chính trị";
            // 
            // btnLLTruong
            // 
            this.btnLLTruong.Location = new System.Drawing.Point(703, 31);
            this.btnLLTruong.Name = "btnLLTruong";
            this.btnLLTruong.Size = new System.Drawing.Size(34, 23);
            this.btnLLTruong.TabIndex = 15;
            this.btnLLTruong.Text = "...";
            this.btnLLTruong.ToolTip = "Thêm Trường đào tạo";
            this.btnLLTruong.Click += new System.EventHandler(this.btnLLTruong_Click);
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Location = new System.Drawing.Point(346, 121);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(55, 15);
            this.labelControl30.TabIndex = 14;
            this.labelControl30.Text = "Tên đề án";
            // 
            // txtLLDeAn
            // 
            this.txtLLDeAn.Location = new System.Drawing.Point(459, 118);
            this.txtLLDeAn.Name = "txtLLDeAn";
            this.txtLLDeAn.Properties.AllowFocused = false;
            this.txtLLDeAn.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLLDeAn.Properties.Appearance.Options.UseFont = true;
            this.txtLLDeAn.Size = new System.Drawing.Size(271, 22);
            this.txtLLDeAn.TabIndex = 13;
            // 
            // chbLLHocBong
            // 
            this.chbLLHocBong.Location = new System.Drawing.Point(134, 118);
            this.chbLLHocBong.Name = "chbLLHocBong";
            this.chbLLHocBong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbLLHocBong.Properties.Appearance.Options.UseFont = true;
            this.chbLLHocBong.Properties.Caption = "Học bổng nhà nước";
            this.chbLLHocBong.Size = new System.Drawing.Size(156, 20);
            this.chbLLHocBong.TabIndex = 12;
            // 
            // txtLLNgayCap
            // 
            this.txtLLNgayCap.Location = new System.Drawing.Point(886, 77);
            this.txtLLNgayCap.Name = "txtLLNgayCap";
            this.txtLLNgayCap.Properties.AllowFocused = false;
            this.txtLLNgayCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLLNgayCap.Properties.Appearance.Options.UseFont = true;
            this.txtLLNgayCap.Size = new System.Drawing.Size(158, 22);
            this.txtLLNgayCap.TabIndex = 11;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(595, 80);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(21, 15);
            this.labelControl31.TabIndex = 10;
            this.labelControl31.Text = "đến";
            // 
            // txtLLTgianden
            // 
            this.txtLLTgianden.Location = new System.Drawing.Point(625, 77);
            this.txtLLTgianden.Name = "txtLLTgianden";
            this.txtLLTgianden.Properties.AllowFocused = false;
            this.txtLLTgianden.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLLTgianden.Properties.Appearance.Options.UseFont = true;
            this.txtLLTgianden.Size = new System.Drawing.Size(105, 22);
            this.txtLLTgianden.TabIndex = 9;
            // 
            // txtLLNganh
            // 
            this.txtLLNganh.Location = new System.Drawing.Point(136, 77);
            this.txtLLNganh.Name = "txtLLNganh";
            this.txtLLNganh.Properties.AllowFocused = false;
            this.txtLLNganh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLLNganh.Properties.Appearance.Options.UseFont = true;
            this.txtLLNganh.Size = new System.Drawing.Size(170, 22);
            this.txtLLNganh.TabIndex = 8;
            // 
            // btnLLSua
            // 
            this.btnLLSua.Location = new System.Drawing.Point(171, 154);
            this.btnLLSua.Name = "btnLLSua";
            this.btnLLSua.Size = new System.Drawing.Size(75, 23);
            this.btnLLSua.TabIndex = 7;
            this.btnLLSua.Text = "Hiệu chỉnh";
            this.btnLLSua.Click += new System.EventHandler(this.btnLLSua_Click);
            // 
            // btnLLXoa
            // 
            this.btnLLXoa.Location = new System.Drawing.Point(252, 154);
            this.btnLLXoa.Name = "btnLLXoa";
            this.btnLLXoa.Size = new System.Drawing.Size(75, 23);
            this.btnLLXoa.TabIndex = 0;
            this.btnLLXoa.Text = "Xóa";
            this.btnLLXoa.Click += new System.EventHandler(this.btnLLXoa_Click);
            // 
            // btnLLThem
            // 
            this.btnLLThem.Location = new System.Drawing.Point(90, 154);
            this.btnLLThem.Name = "btnLLThem";
            this.btnLLThem.Size = new System.Drawing.Size(75, 23);
            this.btnLLThem.TabIndex = 0;
            this.btnLLThem.Text = "Lưu";
            this.btnLLThem.Click += new System.EventHandler(this.btnLLLuu_Click);
            // 
            // btnLLTaoMoi
            // 
            this.btnLLTaoMoi.Location = new System.Drawing.Point(9, 154);
            this.btnLLTaoMoi.Name = "btnLLTaoMoi";
            this.btnLLTaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnLLTaoMoi.TabIndex = 0;
            this.btnLLTaoMoi.Text = "Tạo mới";
            this.btnLLTaoMoi.Click += new System.EventHandler(this.btnLLTaoMoi_Click);
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(790, 80);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(52, 15);
            this.labelControl32.TabIndex = 0;
            this.labelControl32.Text = "Ngày cấp";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Location = new System.Drawing.Point(5, 80);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(125, 15);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "Chuyên ngành đào tạo";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Location = new System.Drawing.Point(9, 31);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(0, 15);
            this.labelControl34.TabIndex = 0;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Location = new System.Drawing.Point(773, 35);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(100, 15);
            this.labelControl35.TabIndex = 0;
            this.labelControl35.Text = "Hình thức đào tạo";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Location = new System.Drawing.Point(346, 80);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(94, 15);
            this.labelControl36.TabIndex = 0;
            this.labelControl36.Text = "T.gian đào tạo từ";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(9, 35);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(97, 15);
            this.labelControl37.TabIndex = 0;
            this.labelControl37.Text = "Lý luận chính trị *";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Location = new System.Drawing.Point(346, 35);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(96, 15);
            this.labelControl38.TabIndex = 0;
            this.labelControl38.Text = "Trường đào tạo *";
            // 
            // txtLLTgiantu
            // 
            this.txtLLTgiantu.Location = new System.Drawing.Point(459, 77);
            this.txtLLTgiantu.Name = "txtLLTgiantu";
            this.txtLLTgiantu.Properties.AllowFocused = false;
            this.txtLLTgiantu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLLTgiantu.Properties.Appearance.Options.UseFont = true;
            this.txtLLTgiantu.Size = new System.Drawing.Size(104, 22);
            this.txtLLTgiantu.TabIndex = 0;
            // 
            // cbbLLTrinhDo
            // 
            this.cbbLLTrinhDo.Location = new System.Drawing.Point(136, 32);
            this.cbbLLTrinhDo.Name = "cbbLLTrinhDo";
            this.cbbLLTrinhDo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLLTrinhDo.Properties.Appearance.Options.UseFont = true;
            this.cbbLLTrinhDo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbLLTrinhDo.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã loại trình độ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiBangLyLuanChinhTri", "Lý luận chính trị")});
            this.cbbLLTrinhDo.Properties.DisplayMember = "TenLoaiBangLyLuanChinhTri";
            this.cbbLLTrinhDo.Properties.NullText = "";
            this.cbbLLTrinhDo.Properties.PopupSizeable = false;
            this.cbbLLTrinhDo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbLLTrinhDo.Properties.ValueMember = "Id";
            this.cbbLLTrinhDo.Size = new System.Drawing.Size(170, 22);
            this.cbbLLTrinhDo.TabIndex = 4;
            // 
            // cbbLLHinhThuc
            // 
            this.cbbLLHinhThuc.Location = new System.Drawing.Point(886, 32);
            this.cbbLLHinhThuc.Name = "cbbLLHinhThuc";
            this.cbbLLHinhThuc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLLHinhThuc.Properties.Appearance.Options.UseFont = true;
            this.cbbLLHinhThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbLLHinhThuc.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã hệ đào tạo", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.cbbLLHinhThuc.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.cbbLLHinhThuc.Properties.NullText = "";
            this.cbbLLHinhThuc.Properties.PopupSizeable = false;
            this.cbbLLHinhThuc.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbLLHinhThuc.Properties.ValueMember = "Id";
            this.cbbLLHinhThuc.Size = new System.Drawing.Size(158, 22);
            this.cbbLLHinhThuc.TabIndex = 4;
            // 
            // cbbLLTruong
            // 
            this.cbbLLTruong.Location = new System.Drawing.Point(459, 32);
            this.cbbLLTruong.Name = "cbbLLTruong";
            this.cbbLLTruong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLLTruong.Properties.Appearance.Options.UseFont = true;
            this.cbbLLTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbLLTruong.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTruong", "Mã trường"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.cbbLLTruong.Properties.DisplayMember = "TenTruong";
            this.cbbLLTruong.Properties.NullText = "";
            this.cbbLLTruong.Properties.PopupSizeable = false;
            this.cbbLLTruong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbbLLTruong.Properties.ValueMember = "MaTruong";
            this.cbbLLTruong.Size = new System.Drawing.Size(241, 22);
            this.cbbLLTruong.TabIndex = 4;
            // 
            // ucNVTrinhDo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "ucNVTrinhDo";
            this.Size = new System.Drawing.Size(1280, 510);
            this.Load += new System.EventHandler(this.ucNVTrinhDo_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabTrinhdoChuyenmon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3ChuyenMon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewChuyenMon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNoiCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTenDeAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbCMDienHocBong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTgianDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMChuyenNganh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMTgianTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCMTruong.Properties)).EndInit();
            this.tabNgoaiNgu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3NgoaiNgu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewNgoaiNgu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNBangCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTenNgoaiNgu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNDeAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNNHocBong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTgianden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNNoiCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNNTgianTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNNTruong.Properties)).EndInit();
            this.tabTinHoc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3TinHoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewTinHoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHDeAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbTHHocBong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHTgianden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHNoiCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTHTgiantu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTHTruong.Properties)).EndInit();
            this.tabNghiepVu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3NghiepVu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewNghiepVu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbNVSP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVDeAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbNVHocBong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTgianden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVNganh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNVTgiantu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNVHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNVTruong.Properties)).EndInit();
            this.tabLyLuanCT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg3LLCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewLLCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLDeAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbLLHocBong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLNgayCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTgianden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLNganh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTgiantu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLTrinhDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLHinhThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLLTruong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabTrinhdoChuyenmon;
        private System.Windows.Forms.TabPage tabNgoaiNgu;
        private System.Windows.Forms.TabPage tabTinHoc;
        private System.Windows.Forms.TabPage tabNghiepVu;
        private System.Windows.Forms.TabPage tabLyLuanCT;
        private DevExpress.XtraGrid.GridControl gvPg3ChuyenMon;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewChuyenMon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMLoaiTDCMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMLoaiTDCM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMMaTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMTrinhDo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMTenHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMChuyennganh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMTu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMden;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMNoiCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMNgayCap;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.SimpleButton btnCMSua;
        private DevExpress.XtraEditors.SimpleButton btnCMXoa;
        private DevExpress.XtraEditors.SimpleButton btnCMThem;
        private DevExpress.XtraEditors.SimpleButton btnCMTaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.TextEdit txtCMTgianTu;
        private DevExpress.XtraEditors.LookUpEdit cbbCMTrinhDo;
        private DevExpress.XtraEditors.LookUpEdit cbbCMHinhThuc;
        private DevExpress.XtraEditors.LookUpEdit cbbCMTruong;
        private DevExpress.XtraEditors.TextEdit txtCMChuyenNganh;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtCMTgianDen;
        private DevExpress.XtraEditors.TextEdit txtCMNgayCap;
        private DevExpress.XtraEditors.SimpleButton btnCMThemTruong;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtCMTenDeAn;
        private DevExpress.XtraEditors.CheckEdit chbCMDienHocBong;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnNNTruong;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtNNDeAn;
        private DevExpress.XtraEditors.CheckEdit chbNNHocBong;
        private DevExpress.XtraEditors.TextEdit txtNNNgayCap;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtNNTgianden;
        private DevExpress.XtraEditors.TextEdit txtNNNoiCap;
        private DevExpress.XtraEditors.SimpleButton btnNNSua;
        private DevExpress.XtraEditors.SimpleButton btnNNXoa;
        private DevExpress.XtraEditors.SimpleButton btnNNThem;
        private DevExpress.XtraEditors.SimpleButton btnNNTaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtNNTgianTu;
        private DevExpress.XtraEditors.LookUpEdit cbbNNTrinhDo;
        private DevExpress.XtraEditors.LookUpEdit cbbNNHinhThuc;
        private DevExpress.XtraEditors.LookUpEdit cbbNNTruong;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnTHTruong;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtTHDeAn;
        private DevExpress.XtraEditors.CheckEdit chbTHHocBong;
        private DevExpress.XtraEditors.TextEdit txtTHNgayCap;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtTHTgianden;
        private DevExpress.XtraEditors.TextEdit txtTHNoiCap;
        private DevExpress.XtraEditors.SimpleButton btnTHSua;
        private DevExpress.XtraEditors.SimpleButton btnTHXoa;
        private DevExpress.XtraEditors.SimpleButton btnTHThem;
        private DevExpress.XtraEditors.SimpleButton btnTHTaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtTHTgiantu;
        private DevExpress.XtraEditors.LookUpEdit cbbTHTrinhDo;
        private DevExpress.XtraEditors.LookUpEdit cbbTHHinhThuc;
        private DevExpress.XtraEditors.LookUpEdit cbbTHTruong;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnNVTruong;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtNVDeAn;
        private DevExpress.XtraEditors.CheckEdit chbNVHocBong;
        private DevExpress.XtraEditors.TextEdit txtNVNgayCap;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtNVTgianden;
        private DevExpress.XtraEditors.TextEdit txtNVNganh;
        private DevExpress.XtraEditors.SimpleButton btnNVSua;
        private DevExpress.XtraEditors.SimpleButton btnNVXoa;
        private DevExpress.XtraEditors.SimpleButton btnNVThem;
        private DevExpress.XtraEditors.SimpleButton btnNVTaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.TextEdit txtNVTgiantu;
        private DevExpress.XtraEditors.LookUpEdit cbbNVHinhThuc;
        private DevExpress.XtraEditors.LookUpEdit cbbNVTruong;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton btnLLTruong;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit txtLLDeAn;
        private DevExpress.XtraEditors.CheckEdit chbLLHocBong;
        private DevExpress.XtraEditors.TextEdit txtLLNgayCap;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txtLLTgianden;
        private DevExpress.XtraEditors.TextEdit txtLLNganh;
        private DevExpress.XtraEditors.SimpleButton btnLLSua;
        private DevExpress.XtraEditors.SimpleButton btnLLXoa;
        private DevExpress.XtraEditors.SimpleButton btnLLThem;
        private DevExpress.XtraEditors.SimpleButton btnLLTaoMoi;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit txtLLTgiantu;
        private DevExpress.XtraEditors.LookUpEdit cbbLLTrinhDo;
        private DevExpress.XtraEditors.LookUpEdit cbbLLHinhThuc;
        private DevExpress.XtraEditors.LookUpEdit cbbLLTruong;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LookUpEdit txtNNTenNgoaiNgu;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LookUpEdit cbbNNBangCap;
        private DevExpress.XtraEditors.TextEdit txtNNTrinhDo;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit txtNVTrinhDo;
        private DevExpress.XtraEditors.CheckEdit chbNVSP;
        private DevExpress.XtraEditors.TextEdit txtCMNoiCap;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMHocBong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMTenDeAn;
        private DevExpress.XtraGrid.GridControl gvPg3NgoaiNgu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewNgoaiNgu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNLoaiTDCMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNLoaiTDCM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNMaTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTrinhDoDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTenHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNden;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNNoiCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNNgayCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNHocBong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTenDeAn;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNMaNgoaiNgu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTenNgoaiNgu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNMaLoaiBangNN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTenLoaiBangNN;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNNTrinhDo;
        private DevExpress.XtraGrid.GridControl gvPg3TinHoc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewTinHoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHLoaiTDCMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHLoaiTDCM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHMaTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHTrinhDo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHTenHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHden;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHNoiCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHNgayCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHHocBong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTHTenDeAn;
        private DevExpress.XtraGrid.GridControl gvPg3NghiepVu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewNghiepVu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVLoaiTDCM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVMaTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVTrinhDo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVTenHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVChuyennganh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVTu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVden;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVNoiCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVNgayCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVHocBong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVTenDeAn;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLoaiNVSP;
        private DevExpress.XtraGrid.GridControl gvPg3LLCT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewLLCT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLLoaiTDCMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLLoaiTDCM;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLMaTruong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLTrinhDo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLTenHTDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLChuyennganh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLTu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLden;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLNoiCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLNgayCap;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLHocBong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColLLTenDeAn;
    }
}
