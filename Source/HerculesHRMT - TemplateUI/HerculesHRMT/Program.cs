﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.UserSkins;
using HerculesDTO;
using HerculesDAC;
using HerculesCTL;

namespace HerculesHRMT
{
    static class Program
    {
        public static TaiKhoanDTO herculesUser;
        public static bool isKetNoiServer = false;
        public static List<TinhThanhDTO> listTinhThanh = new List<TinhThanhDTO>();
        public static List<QuanHuyenDTO> listQuanHuyen = new List<QuanHuyenDTO>();
        public static List<ChucVuDTO> listChucVu = new List<ChucVuDTO>();
        public static List<PhongKhoaDTO> listPhongKhoa = new List<PhongKhoaDTO>();
        public static List<BoMonBoPhanDTO> listBMBP = new List<BoMonBoPhanDTO>();

        public static List<DanTocDTO> listDanToc = new List<DanTocDTO>();
        public static List<TruongDTO> listTruong = new List<TruongDTO>();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SkinManager.EnableFormSkins();
            BonusSkins.Register();
            UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            #region Load tham so chung
            isKetNoiServer = DataProvider.KiemTraKetNoiSERVER();
            if (isKetNoiServer)
            {
                TinhThanhCTL tinhthanhCTL = new TinhThanhCTL();
                listTinhThanh = tinhthanhCTL.GetAll();

                QuanHuyenCTL quanhuyenCTL = new QuanHuyenCTL();
                listQuanHuyen = quanhuyenCTL.GetAll();

                ChucVuCTL chucvuCTL = new ChucVuCTL();
                listChucVu = chucvuCTL.GetAll();

                PhongKhoaCTL phongkhoaCTL = new PhongKhoaCTL();
                listPhongKhoa = phongkhoaCTL.GetAll();

                BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
                listBMBP = bmbpCTL.GetAll();

                DanTocCTL danTocCtl = new DanTocCTL();
                listDanToc = danTocCtl.GetAll();

                TruongCTL truongCtl = new TruongCTL();
                listTruong = truongCtl.GetAll();
            }
            #endregion

            //Application.Run(new frmDanhSachUngVien());
            Application.Run(new frmMain());
        }
    }
}