﻿using DevExpress.XtraEditors;
using HerculesCTL;
using System.Collections.Generic;
using HerculesDTO;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System;

namespace HerculesHRMT
{
    public partial class ucTonGiao : XtraUserControl
    {
        TonGiaoCTL tgCTL = new TonGiaoCTL();
        List<TonGiaoDTO> listTonGiao = new List<TonGiaoDTO>();
        TonGiaoDTO tgSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;


        public ucTonGiao()
        {
            InitializeComponent();
            txtTenVT.Properties.MaxLength = 10;
        }

        private void LoadData()
        {
            listTonGiao = tgCTL.GetAll();

            gcTonGiao.DataSource = listTonGiao;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(TonGiaoDTO tgDTO)
        {
            if (tgDTO != null)
            {
                txtTonGiao.Text = tgDTO.TenTonGiao;
                txtTenVT.Text = tgDTO.TenVietTat;
                meGhiChu.Text = tgDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtTonGiao.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtTonGiao.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtTonGiao.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(tgSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtTonGiao.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(tgSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtTonGiao.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private TonGiaoDTO GetData(TonGiaoDTO tonGiaoDTO)
        {
            string maTG = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexTG = tgCTL.GetMaxMaTonGiao();
                if (sLastIndexTG.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexTG);
                    maTG += (nLastIndexTG + 1).ToString();
                }
                else
                    maTG += "1";

                tonGiaoDTO.NgayHieuLuc = DateTime.Now;
                tonGiaoDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maTG = tgSelect.MaTonGiao;
                tonGiaoDTO.NgayHieuLuc = tgSelect.NgayHieuLuc;
                tonGiaoDTO.TinhTrang = tgSelect.TinhTrang;
            }
            tonGiaoDTO.MaTonGiao = maTG;
            tonGiaoDTO.TenTonGiao = txtTonGiao.Text;
            tonGiaoDTO.TenVietTat = txtTenVT.Text;
            tonGiaoDTO.GhiChu = meGhiChu.Text;

            return tonGiaoDTO;
        }

        private void ucTonGiao_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void gridViewTongiao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listTonGiao.Count)
            {
                tgSelect = ((GridView)sender).GetRow(_selectedIndex) as TonGiaoDTO;
            }
            else
                if (listTonGiao.Count != 0)
                    tgSelect = listTonGiao[0];
                else
                    tgSelect = null;

            SetStatus(VIEW);
        }

        

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên tôn giáo\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TonGiaoDTO tgNew = GetData(new TonGiaoDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = tgCTL.Save(tgNew);
            else
                isSucess = tgCTL.Update(tgNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listTonGiao.IndexOf(listTonGiao.FirstOrDefault(p => p.MaTonGiao == tgNew.MaTonGiao));
                gridViewTongiao.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(VIEW);
        }



        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (tgSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa tôn giáo " + tgSelect.TenTonGiao + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = tgCTL.Delete(tgSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if(listTonGiao.Count != 0)
                            gridViewTongiao.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
