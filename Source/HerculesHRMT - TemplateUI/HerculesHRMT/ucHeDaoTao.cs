﻿using DevExpress.XtraEditors;
using HerculesCTL;
using System.Linq;
using HerculesDTO;
using System.Collections.Generic;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucHeDaoTao : XtraUserControl
    {
        HeDaoTaoCTL hCTL = new HeDaoTaoCTL();
        List<HeDaoTaoDTO> listHeDT = new List<HeDaoTaoDTO>();
        HeDaoTaoDTO hSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucHeDaoTao()
        {
            InitializeComponent();
            txtTenVT.Properties.MaxLength = 10;
        }

        public void LoadData()
        {
            listHeDT = hCTL.GetAll();

            gcHeDaoTao.DataSource = listHeDT;
            btnLuu.Enabled = false;

        }

        private void BindingData(HeDaoTaoDTO hDTO)
        {
            if (hDTO != null)
            {
                txtHeDaoTao.Text = hDTO.TenHeDaoTao;
                txtTenVT.Text = hDTO.TenVietTat;
                meGhiChu.Text = hDTO.GhiChu;
            }
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtHeDaoTao.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtHeDaoTao.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtHeDaoTao.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(hSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtHeDaoTao.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(hSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtHeDaoTao.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private HeDaoTaoDTO GetData(HeDaoTaoDTO hDTO)
        {
            string maNganh = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexNganh = hCTL.GetMaxMaHeDaoTao();
                if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexNganh);
                    maNganh += (nLastIndexTG + 1).ToString();
                }
                else
                    maNganh += "1";

                hDTO.NgayHieuLuc = DateTime.Now;
                hDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maNganh = hSelect.MaHeDaoTao;
                hDTO.NgayHieuLuc = hSelect.NgayHieuLuc;
                hDTO.TinhTrang = hSelect.TinhTrang;
            }
            hDTO.MaHeDaoTao = maNganh;
            hDTO.TenHeDaoTao = txtHeDaoTao.Text;
            hDTO.TenVietTat = txtTenVT.Text;
            hDTO.GhiChu = meGhiChu.Text;

            return hDTO;
        }

        private void ucHeDaoTao_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void gridViewHeDaoTao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listHeDT.Count)
            {
                hSelect = ((GridView)sender).GetRow(_selectedIndex) as HeDaoTaoDTO;
            }
            else
                if (listHeDT.Count != 0)
                    hSelect = listHeDT[0];
                else
                    hSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên hệ đào tạo\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            HeDaoTaoDTO hNew = GetData(new HeDaoTaoDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = hCTL.Save(hNew);
            else
                isSucess = hCTL.Update(hNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listHeDT.IndexOf(listHeDT.FirstOrDefault(p => p.MaHeDaoTao == hNew.MaHeDaoTao));
                gridViewHeDaoTao.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (hSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa hệ đào tạo" + hSelect.TenHeDaoTao + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = hCTL.Delete(hSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listHeDT.Count != 0)
                            gridViewHeDaoTao.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
