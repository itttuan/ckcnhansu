﻿namespace HerculesHRMT
{
    partial class ucNghiViec
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.dtPg1NgayVaoLam = new DevExpress.XtraEditors.DateEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lueQD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.cbbPg1ChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lueBPBMDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtHoTen = new DevExpress.XtraEditors.TextEdit();
            this.luePhongKhoaDen = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.grcTimKiem = new DevExpress.XtraEditors.GroupControl();
            this.gcDSNhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridViewDSNhanVien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCNND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChucvu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTinhTrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDEDSNgayS = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btnTimKiemMaNV = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTKMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKBPBM = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKPhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).BeginInit();
            this.grcTimKiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.grcTimKiem);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(785, 518);
            this.panelControl1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.lookUpEdit1);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.btnSave);
            this.groupControl1.Controls.Add(this.dtPg1NgayVaoLam);
            this.groupControl1.Controls.Add(this.labelControl19);
            this.groupControl1.Controls.Add(this.lueQD);
            this.groupControl1.Controls.Add(this.labelControl112);
            this.groupControl1.Controls.Add(this.cbbPg1ChucVu);
            this.groupControl1.Controls.Add(this.labelControl29);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.lueBPBMDen);
            this.groupControl1.Controls.Add(this.txtHoTen);
            this.groupControl1.Controls.Add(this.luePhongKhoaDen);
            this.groupControl1.Controls.Add(this.txtMaNV);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(0, 240);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(785, 278);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = " ";
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(102, 81);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.NullText = "";
            this.lookUpEdit1.Size = new System.Drawing.Size(141, 22);
            this.lookUpEdit1.TabIndex = 132;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(15, 87);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(30, 15);
            this.labelControl8.TabIndex = 131;
            this.labelControl8.Text = "Lý do";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 113);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 130;
            this.btnSave.Text = "Lưu";
            // 
            // dtPg1NgayVaoLam
            // 
            this.dtPg1NgayVaoLam.EditValue = new System.DateTime(((long)(0)));
            this.dtPg1NgayVaoLam.Location = new System.Drawing.Point(629, 80);
            this.dtPg1NgayVaoLam.Name = "dtPg1NgayVaoLam";
            this.dtPg1NgayVaoLam.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg1NgayVaoLam.Properties.Appearance.Options.UseFont = true;
            this.dtPg1NgayVaoLam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg1NgayVaoLam.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg1NgayVaoLam.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg1NgayVaoLam.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg1NgayVaoLam.Size = new System.Drawing.Size(120, 22);
            this.dtPg1NgayVaoLam.TabIndex = 129;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(538, 83);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(77, 15);
            this.labelControl19.TabIndex = 128;
            this.labelControl19.Text = "Ngày hiệu lực";
            // 
            // lueQD
            // 
            this.lueQD.Location = new System.Drawing.Point(374, 80);
            this.lueQD.Name = "lueQD";
            this.lueQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueQD.Properties.Appearance.Options.UseFont = true;
            this.lueQD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQD.Properties.NullText = "";
            this.lueQD.Size = new System.Drawing.Size(141, 22);
            this.lueQD.TabIndex = 127;
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(265, 83);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(60, 15);
            this.labelControl112.TabIndex = 126;
            this.labelControl112.Text = "Quyết định";
            // 
            // cbbPg1ChucVu
            // 
            this.cbbPg1ChucVu.Location = new System.Drawing.Point(629, 53);
            this.cbbPg1ChucVu.Name = "cbbPg1ChucVu";
            this.cbbPg1ChucVu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1ChucVu.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1ChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1ChucVu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mã chức vụ", "MaChucVu", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenChucVu", "Tên chức vụ")});
            this.cbbPg1ChucVu.Properties.DisplayMember = "TenChucVu";
            this.cbbPg1ChucVu.Properties.NullText = "";
            this.cbbPg1ChucVu.Properties.ReadOnly = true;
            this.cbbPg1ChucVu.Properties.ValueMember = "MaChucVu";
            this.cbbPg1ChucVu.Size = new System.Drawing.Size(120, 22);
            this.cbbPg1ChucVu.TabIndex = 125;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Location = new System.Drawing.Point(533, 56);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(47, 15);
            this.labelControl29.TabIndex = 124;
            this.labelControl29.Text = "Chức vụ";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(265, 56);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(92, 15);
            this.labelControl5.TabIndex = 119;
            this.labelControl5.Text = "Bộ phận/Bộ môn";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(15, 56);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(68, 15);
            this.labelControl6.TabIndex = 117;
            this.labelControl6.Text = "Phòng/Khoa";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(15, 29);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(77, 15);
            this.labelControl7.TabIndex = 120;
            this.labelControl7.Text = "Mã Nhân Viên";
            // 
            // lueBPBMDen
            // 
            this.lueBPBMDen.Location = new System.Drawing.Point(374, 53);
            this.lueBPBMDen.Name = "lueBPBMDen";
            this.lueBPBMDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBPBMDen.Properties.Appearance.Options.UseFont = true;
            this.lueBPBMDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBPBMDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueBPBMDen.Properties.DisplayMember = "TenBMBP";
            this.lueBPBMDen.Properties.NullText = "";
            this.lueBPBMDen.Properties.ReadOnly = true;
            this.lueBPBMDen.Properties.ValueMember = "MaBMBP";
            this.lueBPBMDen.Size = new System.Drawing.Size(141, 22);
            this.lueBPBMDen.TabIndex = 118;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(374, 26);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Properties.Appearance.Options.UseFont = true;
            this.txtHoTen.Properties.ReadOnly = true;
            this.txtHoTen.Size = new System.Drawing.Size(241, 22);
            this.txtHoTen.TabIndex = 121;
            // 
            // luePhongKhoaDen
            // 
            this.luePhongKhoaDen.Location = new System.Drawing.Point(102, 53);
            this.luePhongKhoaDen.Name = "luePhongKhoaDen";
            this.luePhongKhoaDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoaDen.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoaDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoaDen.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.luePhongKhoaDen.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoaDen.Properties.NullText = "";
            this.luePhongKhoaDen.Properties.ReadOnly = true;
            this.luePhongKhoaDen.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoaDen.Size = new System.Drawing.Size(141, 22);
            this.luePhongKhoaDen.TabIndex = 116;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(102, 26);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Properties.AllowFocused = false;
            this.txtMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtMaNV.Properties.ReadOnly = true;
            this.txtMaNV.Size = new System.Drawing.Size(141, 22);
            this.txtMaNV.TabIndex = 122;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(265, 29);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 15);
            this.labelControl9.TabIndex = 123;
            this.labelControl9.Text = "Họ Tên";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(5, 142);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(775, 136);
            this.gridControl1.TabIndex = 10;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupPanelText = " Danh sách Cán bộ - Viên chức đã nghỉ việc";
            this.gridView1.Name = "gridView1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "Id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Mã NV";
            this.gridColumn2.FieldName = "MaNV";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Họ";
            this.gridColumn3.FieldName = "Ho";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Tên";
            this.gridColumn4.FieldName = "Ten";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Chức vụ";
            this.gridColumn9.FieldName = "TenChucVu";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Phòng/Khoa";
            this.gridColumn10.FieldName = "TenPhongKhoa";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Bộ phận/Bộ môn";
            this.gridColumn11.FieldName = "TenBMBP";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 5;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Lý do nghỉ việc";
            this.gridColumn12.FieldName = "TinhTrangLamViec";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 6;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Số quyết định";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 7;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ngày hiệu lực";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 8;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // grcTimKiem
            // 
            this.grcTimKiem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcTimKiem.Controls.Add(this.gcDSNhanVien);
            this.grcTimKiem.Controls.Add(this.btnTimKiemMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl4);
            this.grcTimKiem.Controls.Add(this.txtTKMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl2);
            this.grcTimKiem.Controls.Add(this.lueTKBPBM);
            this.grcTimKiem.Controls.Add(this.labelControl3);
            this.grcTimKiem.Controls.Add(this.lueTKPhongKhoa);
            this.grcTimKiem.Location = new System.Drawing.Point(0, 44);
            this.grcTimKiem.Name = "grcTimKiem";
            this.grcTimKiem.Size = new System.Drawing.Size(785, 195);
            this.grcTimKiem.TabIndex = 3;
            this.grcTimKiem.Text = "Tìm Kiếm";
            // 
            // gcDSNhanVien
            // 
            this.gcDSNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDSNhanVien.Location = new System.Drawing.Point(5, 58);
            this.gcDSNhanVien.MainView = this.gridViewDSNhanVien;
            this.gcDSNhanVien.Name = "gcDSNhanVien";
            this.gcDSNhanVien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDEDSNgayS});
            this.gcDSNhanVien.Size = new System.Drawing.Size(775, 132);
            this.gcDSNhanVien.TabIndex = 9;
            this.gcDSNhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDSNhanVien});
            // 
            // gridViewDSNhanVien
            // 
            this.gridViewDSNhanVien.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewDSNhanVien.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewDSNhanVien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNVID,
            this.gridColMaNV,
            this.gridColHo,
            this.gridColTen,
            this.gridColNgaySinh,
            this.gridColGioiTinh,
            this.gridColCNND,
            this.gridColEmail,
            this.gridColChucvu,
            this.gridColPhongKhoa,
            this.gridColBoMon,
            this.gridColTinhTrang});
            this.gridViewDSNhanVien.GridControl = this.gcDSNhanVien;
            this.gridViewDSNhanVien.GroupPanelText = " Chọn Cán bộ - Viên chức nghỉ việc";
            this.gridViewDSNhanVien.Name = "gridViewDSNhanVien";
            // 
            // gridColNVID
            // 
            this.gridColNVID.Caption = "ID";
            this.gridColNVID.FieldName = "Id";
            this.gridColNVID.Name = "gridColNVID";
            // 
            // gridColMaNV
            // 
            this.gridColMaNV.Caption = "Mã NV";
            this.gridColMaNV.FieldName = "MaNV";
            this.gridColMaNV.Name = "gridColMaNV";
            this.gridColMaNV.OptionsColumn.AllowEdit = false;
            this.gridColMaNV.OptionsColumn.ReadOnly = true;
            this.gridColMaNV.Visible = true;
            this.gridColMaNV.VisibleIndex = 0;
            // 
            // gridColHo
            // 
            this.gridColHo.Caption = "Họ";
            this.gridColHo.FieldName = "Ho";
            this.gridColHo.Name = "gridColHo";
            this.gridColHo.OptionsColumn.AllowEdit = false;
            this.gridColHo.OptionsColumn.ReadOnly = true;
            this.gridColHo.Visible = true;
            this.gridColHo.VisibleIndex = 1;
            // 
            // gridColTen
            // 
            this.gridColTen.Caption = "Tên";
            this.gridColTen.FieldName = "Ten";
            this.gridColTen.Name = "gridColTen";
            this.gridColTen.OptionsColumn.AllowEdit = false;
            this.gridColTen.OptionsColumn.ReadOnly = true;
            this.gridColTen.Visible = true;
            this.gridColTen.VisibleIndex = 2;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày sinh";
            this.gridColNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 3;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Nữ";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.OptionsColumn.AllowEdit = false;
            this.gridColGioiTinh.OptionsColumn.ReadOnly = true;
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 5;
            // 
            // gridColCNND
            // 
            this.gridColCNND.Caption = "CMND";
            this.gridColCNND.FieldName = "CMND";
            this.gridColCNND.Name = "gridColCNND";
            this.gridColCNND.Visible = true;
            this.gridColCNND.VisibleIndex = 4;
            // 
            // gridColEmail
            // 
            this.gridColEmail.Caption = "Email";
            this.gridColEmail.FieldName = "Email";
            this.gridColEmail.Name = "gridColEmail";
            this.gridColEmail.Visible = true;
            this.gridColEmail.VisibleIndex = 6;
            // 
            // gridColChucvu
            // 
            this.gridColChucvu.Caption = "Chức vụ";
            this.gridColChucvu.FieldName = "TenChucVu";
            this.gridColChucvu.Name = "gridColChucvu";
            this.gridColChucvu.OptionsColumn.AllowEdit = false;
            this.gridColChucvu.OptionsColumn.ReadOnly = true;
            this.gridColChucvu.Visible = true;
            this.gridColChucvu.VisibleIndex = 7;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng/Khoa";
            this.gridColPhongKhoa.FieldName = "TenPhongKhoa";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.OptionsColumn.AllowEdit = false;
            this.gridColPhongKhoa.OptionsColumn.ReadOnly = true;
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 8;
            // 
            // gridColBoMon
            // 
            this.gridColBoMon.Caption = "Bộ phận/Bộ môn";
            this.gridColBoMon.FieldName = "TenBMBP";
            this.gridColBoMon.Name = "gridColBoMon";
            this.gridColBoMon.OptionsColumn.AllowEdit = false;
            this.gridColBoMon.OptionsColumn.ReadOnly = true;
            this.gridColBoMon.Visible = true;
            this.gridColBoMon.VisibleIndex = 9;
            // 
            // gridColTinhTrang
            // 
            this.gridColTinhTrang.Caption = "Tình trạng làm việc";
            this.gridColTinhTrang.FieldName = "TinhTrangLamViec";
            this.gridColTinhTrang.Name = "gridColTinhTrang";
            this.gridColTinhTrang.OptionsColumn.AllowEdit = false;
            this.gridColTinhTrang.OptionsColumn.ReadOnly = true;
            this.gridColTinhTrang.Visible = true;
            this.gridColTinhTrang.VisibleIndex = 10;
            // 
            // repositoryItemDEDSNgayS
            // 
            this.repositoryItemDEDSNgayS.AutoHeight = false;
            this.repositoryItemDEDSNgayS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDEDSNgayS.Name = "repositoryItemDEDSNgayS";
            this.repositoryItemDEDSNgayS.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // btnTimKiemMaNV
            // 
            this.btnTimKiemMaNV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemMaNV.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemMaNV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemMaNV.Location = new System.Drawing.Point(732, 33);
            this.btnTimKiemMaNV.Name = "btnTimKiemMaNV";
            this.btnTimKiemMaNV.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemMaNV.TabIndex = 8;
            this.btnTimKiemMaNV.ToolTip = "Tìm theo Mã Nhân viên";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(533, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(78, 15);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Mã Nhân viên:";
            // 
            // txtTKMaNV
            // 
            this.txtTKMaNV.Location = new System.Drawing.Point(617, 30);
            this.txtTKMaNV.Name = "txtTKMaNV";
            this.txtTKMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtTKMaNV.Size = new System.Drawing.Size(109, 22);
            this.txtTKMaNV.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(262, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(95, 15);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Bộ phận/Bộ môn:";
            // 
            // lueTKBPBM
            // 
            this.lueTKBPBM.Location = new System.Drawing.Point(363, 30);
            this.lueTKBPBM.Name = "lueTKBPBM";
            this.lueTKBPBM.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKBPBM.Properties.Appearance.Options.UseFont = true;
            this.lueTKBPBM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKBPBM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueTKBPBM.Properties.DisplayMember = "TenBMBP";
            this.lueTKBPBM.Properties.NullText = "";
            this.lueTKBPBM.Properties.ValueMember = "MaBMBP";
            this.lueTKBPBM.Size = new System.Drawing.Size(145, 22);
            this.lueTKBPBM.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(15, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 15);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Phòng/Khoa:";
            // 
            // lueTKPhongKhoa
            // 
            this.lueTKPhongKhoa.Location = new System.Drawing.Point(92, 30);
            this.lueTKPhongKhoa.Name = "lueTKPhongKhoa";
            this.lueTKPhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKPhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.lueTKPhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKPhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.lueTKPhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.lueTKPhongKhoa.Properties.NullText = "";
            this.lueTKPhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.lueTKPhongKhoa.Size = new System.Drawing.Size(145, 22);
            this.lueTKPhongKhoa.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(276, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(232, 33);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Quản lý nghỉ việc";
            // 
            // ucNghiViec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucNghiViec";
            this.Size = new System.Drawing.Size(785, 518);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg1NgayVaoLam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1ChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBPBMDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoaDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).EndInit();
            this.grcTimKiem.ResumeLayout(false);
            this.grcTimKiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl grcTimKiem;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtTKMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueTKBPBM;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lueTKPhongKhoa;
        private DevExpress.XtraGrid.GridControl gcDSNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDSNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaNV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCNND;
        private DevExpress.XtraGrid.Columns.GridColumn gridColEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChucvu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhongKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBoMon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTinhTrang;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDEDSNgayS;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.DateEdit dtPg1NgayVaoLam;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LookUpEdit lueQD;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.LookUpEdit cbbPg1ChucVu;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit lueBPBMDen;
        private DevExpress.XtraEditors.TextEdit txtHoTen;
        private DevExpress.XtraEditors.LookUpEdit luePhongKhoaDen;
        private DevExpress.XtraEditors.TextEdit txtMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
    }
}
