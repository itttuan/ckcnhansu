﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using HerculesCTL;
using HerculesDTO;

namespace HerculesHRMT
{
    public partial class ucChucVu : XtraUserControl
    {
        private ChucVuCTL _chucVuCtl = new ChucVuCTL();
        private ChucVuDTO cvSelect = null;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        private int _selectedStatus = -1;
        //private int _selectedIndex = -1;
        private List<ChucVuDTO> listChucVu = new List<ChucVuDTO>(); 
        //private int NEW = 0;

        public ucChucVu()
        {
            InitializeComponent();
            
        }

        #region METHODS
        // Load list of data
        private void LoadData()
        {
            listChucVu = _chucVuCtl.GetAll();

            gridControl1.DataSource = listChucVu;
            btnLuu.Enabled = false;            
        }

        // Display content of row to textboxes
        private void BindingData(ChucVuDTO chucVuDto)
        {
            if (chucVuDto != null)
            {
                txtMaChucVu.Text = chucVuDto.MaChucVu;
                txtChucVu.Text = chucVuDto.TenChucVu;
                meMoTa.Text = chucVuDto.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtMaChucVu.Properties.ReadOnly = false;
                    txtChucVu.Properties.ReadOnly = false;
                    meMoTa.Properties.ReadOnly = false;
                    
                    txtMaChucVu.Text = string.Empty;
                    txtChucVu.Text = string.Empty;
                    meMoTa.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    
                    break;
                case EDIT:
                    txtMaChucVu.Properties.ReadOnly = true;
                    txtChucVu.Properties.ReadOnly = false;
                    meMoTa.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(cvSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtMaChucVu.Properties.ReadOnly = true;
                    txtChucVu.Properties.ReadOnly = true;
                    meMoTa.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(cvSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private ChucVuDTO GetData(ChucVuDTO chucVuDto)
        {
            string maCV = "";
            if (_selectedStatus == NEW)
            {
                maCV = txtMaChucVu.Text;
                chucVuDto.NgayHieuLuc = DateTime.Now;
                chucVuDto.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maCV = cvSelect.MaChucVu;
                chucVuDto.NgayHieuLuc = cvSelect.NgayHieuLuc;
                chucVuDto.TinhTrang = cvSelect.TinhTrang;
            }

            chucVuDto.MaChucVu = maCV;
            chucVuDto.GhiChu = meMoTa.Text;
            chucVuDto.TenChucVu = txtChucVu.Text;

            return chucVuDto;
        }

        private bool CheckInputData()
        {
            if (txtMaChucVu.Text == string.Empty || txtChucVu.Text == string.Empty)
                return false;

            return true;
        }
        #endregion

        #region Button Events
        private void btnThem_Click(object sender, EventArgs e)
        {
            //txtChucVu.Enter -= textbox_Enter;
            //meMoTa.Enter -= textbox_Enter;

            //txtMaChucVu.Text = string.Empty;
            //txtChucVu.Text = string.Empty;
            //meMoTa.Text = string.Empty;
            SetStatus(NEW);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Mã chức vụ\n Chức vụ", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var chucVuDto = GetData(new ChucVuDTO());
            var isSucess = false;
            if(_selectedStatus == NEW)
                isSucess = _chucVuCtl.Save(chucVuDto);
            else
                isSucess = _chucVuCtl.Update(chucVuDto);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                LoadData();
                int _selectedIndex = listChucVu.IndexOf(listChucVu.FirstOrDefault(p => p.MaChucVu == chucVuDto.MaChucVu));
                //BindingData(listChucVu[_selectedIndex]);
                gridViewChucVu.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (cvSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn chức vụ " + cvSelect.TenChucVu + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {

                    var isSucess = _chucVuCtl.Delete(cvSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listChucVu.Count != 0)
                            gridViewChucVu.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SetStatus(VIEW);
                    }

                    //txtChucVu.Enter += textbox_Enter;
                    //meMoTa.Enter += textbox_Enter;
                }
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(EDIT);
        }

        #endregion

        #region GRID EVENTS
        // Get selected row index to display content to textboxes
        private void gridViewChucVu_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listChucVu.Count)
            {
                cvSelect = ((GridView)sender).GetRow(_selectedIndex) as ChucVuDTO;
            }
            else
                if (listChucVu.Count != 0)
                    cvSelect = listChucVu[0];
                else
                    cvSelect = null;
            SetStatus(VIEW);
        }
        #endregion

        #region TEXTBOX EVENTS
        // Update status of buttons if user edit any field
        private void textbox_TextChanged(object sender, EventArgs e)
        {
            //SetStatus(EDIT);
        }

        // Set TextChanged event for textbox to listen user edit any field
        private void textbox_Enter(object sender, EventArgs e)
        {
            //txtChucVu.TextChanged += textbox_TextChanged;
            //meMoTa.TextChanged += textbox_TextChanged;
        }

        // Remove TextChanged event for textbox when lost focus on textbox
        private void textbox_Leave(object sender, EventArgs e)
        {
            //txtChucVu.TextChanged -= textbox_TextChanged;
            //meMoTa.TextChanged -= textbox_TextChanged;
        }
        #endregion

        private void ucChucVu_Load(object sender, EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }
    }
}
