﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class PhanCongDAC
    {
        const string StrSelectByNhanVien = "select pc.*, bm.TenBMBP, bm.MaPK, pk.TenPhongKhoa from PhanCong pc left join BoMonBoPhan bm on pc.MaBMBP = bm.MaBMBP left join PhongKhoa pk on bm.MaPK = pk.MaPhongKhoa where MaNV = '{0}' and pc.TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<PhanCongDTO> LayDanhSachPhanCongTheoNhanVien(string strMaNV)
        {
            List<PhanCongDTO> listResult = new List<PhanCongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<PhanCongDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }
    }
}
