﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuaTrinhCongTacDAC
    {
        const string StrSelectByNhanVien = "select * from QuaTrinhCongTac where MaNV = '{0}' and TinhTrang = 0 and (GiaiDoan = 0 or GiaiDoan = 1)";
        const string StrSelectByNhanVienToChuc = "select * from QuaTrinhCongTac where MaNV = '{0}' and TinhTrang = 0 and GiaiDoan = 2";
        SqlConnection _conn = new SqlConnection();

        public List<QuaTrinhCongTacDTO> LayDanhSachQuaTrinhCongTacCuaNhanVien(string strMaNV)
        {
            List<QuaTrinhCongTacDTO> result = new List<QuaTrinhCongTacDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(string.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                QuaTrinhCongTacDTO dto = Utils.MappingSQLDRToDTO3<QuaTrinhCongTacDTO>(sdr);
                dto.CongViec = sdr["NgheNghiep"] as String;
                if (dto.GiaiDoan == 0)
                {
                    dto.LoaiQuaTrinh = "Trước tuyển dụng";
                }
                else if (dto.GiaiDoan == 1)
                {
                    dto.LoaiQuaTrinh = "Khi được tuyển dụng";
                }
                result.Add(dto);
                
            }
            sdr.Close();
            _conn.Close();
            return result;
        }
        public List<QuaTrinhCongTacDTO> LayDanhSachToChucXaHoiNhanVien(string strMaNV)
        {
            List<QuaTrinhCongTacDTO> result = new List<QuaTrinhCongTacDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(string.Format(StrSelectByNhanVienToChuc, strMaNV), _conn);
            while (sdr.Read())
            {
                QuaTrinhCongTacDTO dto = Utils.MappingSQLDRToDTO3<QuaTrinhCongTacDTO>(sdr);
                dto.CongViec = sdr["NgheNghiep"] as String;
                result.Add(dto);
                
            }
            sdr.Close();
            _conn.Close();
            return result;
        }
        public bool Save(QuaTrinhCongTacDTO qtctDTO)
        {
            string query = "INSERT INTO QuaTrinhCongTac([MaNV],[NoiCongTac],[NgheNghiep],[ThanhTich],[TuNgay],[DenNgay],[GhiChu]"
                + ",[NgayHieuLuc],[TinhTrang],[GiaiDoan])VALUES ('" + qtctDTO.MaNV + "',N'" + qtctDTO.NoiCongTac + "',N'" + qtctDTO.CongViec
                + "'," + (String.IsNullOrEmpty(qtctDTO.ThanhTich)? "null," : ("N'" + qtctDTO.ThanhTich + "',"))
                + (string.IsNullOrEmpty(qtctDTO.TuNgay) ? "null," : "'" + qtctDTO.TuNgay +"',")
                + (string.IsNullOrEmpty(qtctDTO.DenNgay) ? "null," : "'" + qtctDTO.DenNgay + "',")
                //+ "convert(date,'" + qtctDTO.TuNgay.ToString("dd/MM/yyyy") + "',103), convert(date,'" + qtctDTO.DenNgay.ToString("dd/MM/yyyy") + "',103),"
                + (String.IsNullOrEmpty(qtctDTO.GhiChu)? "null," : "N'" + qtctDTO.GhiChu + "',") + "getdate(),0," + qtctDTO.GiaiDoan + ")";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Update(QuaTrinhCongTacDTO qtctDTO)
        {
            //string query = "UPDATE QuaTrinhCongTac SET NoiCongTac = N'" + qtctDTO.NoiCongTac + "', NgheNghiep = N'" + qtctDTO.CongViec
            //    + "', ThanhTich = " + (String.IsNullOrEmpty(qtctDTO.ThanhTich) ? "null, TuNgay = " : "N'" + qtctDTO.ThanhTich + "',TuNgay =")
            //    + "convert(date, '" + qtctDTO.TuNgay.ToString("dd/MM/yyyy") + "',103), DenNgay = convert(date,'" + qtctDTO.DenNgay.ToString("dd/MM/yyyy")
            //    + "',103), GhiChu = " + (String.IsNullOrEmpty(qtctDTO.GhiChu)? "null,":"N'" + qtctDTO.GhiChu + "',")
            //    + "GiaiDoan = " + qtctDTO.GiaiDoan + " where Id = " + qtctDTO.Id;

            string query = "UPDATE QuaTrinhCongTac SET NoiCongTac = N'" + qtctDTO.NoiCongTac + "', NgheNghiep = N'" + qtctDTO.CongViec
                + "', ThanhTich = " + (String.IsNullOrEmpty(qtctDTO.ThanhTich) ? "null, TuNgay = " : "N'" + qtctDTO.ThanhTich + "',TuNgay =")
                + (string.IsNullOrEmpty(qtctDTO.TuNgay) ? "null, DenNgay = " : "'" + qtctDTO.TuNgay + "', DenNgay = ")
                + (string.IsNullOrEmpty(qtctDTO.DenNgay) ? "null," : "'" + qtctDTO.DenNgay + "',")
                + " GhiChu = " + (String.IsNullOrEmpty(qtctDTO.GhiChu) ? "null," : "N'" + qtctDTO.GhiChu + "',")
                + "GiaiDoan = " + qtctDTO.GiaiDoan + " where Id = " + qtctDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(string id)
        {
            string query = "UPDATE QuaTrinhCongTac SET TinhTrang = 1 where Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
