﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuyetDinhDAC
    {
        const string StrSelectByNhanVien = "select qd.*, lqd.TenLoaiQD from QuyetDinh qd left join LoaiQuyetDinh lqd on qd.MaLoaiQD = lqd.MaLoaiQD where MaNV = '{0}' and qd.TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<QuyetDinhDTO> LayDanhSachQuyetDinhTheoNhanVien(string strMaNV)
        {
            List<QuyetDinhDTO> listResult = new List<QuyetDinhDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<QuyetDinhDTO>(sdr));
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }
    }
}
