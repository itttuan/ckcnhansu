﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;


namespace HerculesDAC
{
    public class NhanVienDAC
    {
        SqlConnection _conn = new SqlConnection();

        public bool Save(NhanVienDTO nvObj)
        {
            try
            {
                string query = "Insert Into NhanVien (MaNV, Ho,Ten,NgaySinh,NoiSinh,GioiTinh,MaDanToc,MaTonGiao,QuocTich,DienThoai"
                    + ",MaChucVu,CMND,NgayCapCMND,MaNoiCapCMND,HoKhauThuongTruDiaChi,HoKhauThuongTruMaQuanHuyen,HoKhauThuongTruMaTinhThanh,DiaChiHienTaiDiaChi,DiaChiHienTaiMaQuanHuyen"
                    + ",DiaChiHienTaiMaTinhThanh,NgheNghiepTuyenDung,HangNhanVien,BacNhanVien,MaNgach,Email,TinhTrangHonNhan,NgayVaoLam"
                    + ",NgayHieuLuc,TinhTrang,TinhTrangLamViec) VALUES ('" + nvObj.MaNV + "',N'" + nvObj.Ho + "',N'" + nvObj.Ten
                    + "',CONVERT(date,'" + nvObj.NgaySinh.ToString() + "',103),'" + nvObj.NoiSinh + "','" + nvObj.GioiTinh + "','" + nvObj.MaDanToc
                    + "','" + nvObj.MaTonGiao + "',N'" + nvObj.QuocTich + "','" + nvObj.DienThoai + "','" + nvObj.MaChucVu + "','" + nvObj.CMND
                    + "', CONVERT(date,'" + nvObj.NgayCapCMND.ToString() + "',103),'" + nvObj.MaNoiCapCMND + "',N'" + nvObj.HoKhauThuongTruDiaChi
                    + "','" + nvObj.HoKhauThuongTruMaQuanHuyen + "','" + nvObj.HoKhauThuongTruMaTinhThanh + "',N'" + nvObj.DiaChiHienTaiDiaChi + "','" + nvObj.DiaChiHienTaiMaQuanHuyen
                    + "','" + nvObj.DiaChiHienTaiMaTinhThanh + "',N'" + nvObj.NgheNghiepTuyenDung + "','" + nvObj.HangNhanVien + "','" + nvObj.BacNhanVien
                    + "','" + nvObj.MaNgach + "','" + nvObj.Email + "',N'" + nvObj.TinhTrangHonNhan + "',CONVERT(date,'" + nvObj.NgayVaoLam.ToString()
                    + "',103),GETDATE(), 0,'" + nvObj.TinhTrangLamViec + "')";
                _conn = DataProvider.TaoKetNoiCSDL();
                int result = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (result != 0)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }


        public List<NhanVienDTO> LayDanhSachTatCaNhanVien()
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select NV.Id, NV.MaNV, NV.Ho, NV.Ten, NV.CMND, NV.NgaySinh, NV.CMND, NV.GioiTinh, NV.Email, CV.TenChucVu, "
            + "PK.TenPhongKhoa, BM.TenBMBP, DT.TenDanToc, TG.TenTonGiao from NhanVien NV left join ChucVu CV on NV.MaChucVu = CV.MaChucVu"
            + " left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + "where NV.TinhTrang = 0 order by NV.MaNV";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                // result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;

        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoPhongKhoa(string strMaPK)
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select NV.Id, NV.MaNV, NV.Ho, NV.Ten, NV.CMND, NV.NgaySinh, NV.CMND, NV.GioiTinh, NV.Email, CV.TenChucVu, "
            + "PK.TenPhongKhoa, BM.TenBMBP, DT.TenDanToc, TG.TenTonGiao from NhanVien NV left join ChucVu CV on NV.MaChucVu = CV.MaChucVu"
            + " left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + "where PK.MaPhongKhoa = '" + strMaPK + "' and NV.TinhTrang = 0 order by NV.MaNV";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;

        }
        public List<NhanVienDTO> LayDanhSachNhanVienTheoBoMon(string strMaBM)
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select NV.Id, NV.MaNV, NV.Ho, NV.Ten, NV.CMND, NV.NgaySinh, NV.CMND, NV.GioiTinh, NV.Email, CV.TenChucVu, "
            + "PK.TenPhongKhoa, BM.TenBMBP, DT.TenDanToc, TG.TenTonGiao from NhanVien NV left join ChucVu CV on NV.MaChucVu = CV.MaChucVu"
            + " left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + "where NV.MaBMBP = '" + strMaBM + "' and NV.TinhTrang = 0 order by NV.MaNV";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;

        }

        public List<NhanVienDTO> TimNhanVienTheoCMND(string strCMND)
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select NV.Id, NV.MaNV, NV.Ho, NV.Ten, NV.CMND, NV.NgaySinh, NV.CMND, NV.GioiTinh, NV.Email, CV.TenChucVu, "
            + "PK.TenPhongKhoa, BM.TenBMBP, DT.TenDanToc, TG.TenTonGiao from NhanVien NV left join ChucVu CV on NV.MaChucVu = CV.MaChucVu"
            + " left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + "where NV.TinhTrang = 0 "
            + "and NV.CMND = '" + strCMND + "'";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;

        }

        public List<NhanVienDTO> TimNhanVienTheoMaNV(string strMaNV)
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select NV.Id, NV.MaNV, NV.Ho, NV.Ten, NV.CMND, NV.NgaySinh, NV.CMND, NV.GioiTinh, NV.Email, CV.TenChucVu, "
            + "PK.TenPhongKhoa, BM.TenBMBP, DT.TenDanToc, TG.TenTonGiao from NhanVien NV left join ChucVu CV on NV.MaChucVu = CV.MaChucVu"
            + " left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + "where NV.MaNV = '" + strMaNV + "' and NV.TinhTrang = 0 ";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;

        }
        public NhanVienDTO TimNhanVienTheoId(string id)
        {
            NhanVienDTO result = null;
            string strSelect = "Select TOP 1 NV.*, DT.TenDanToc, TG.TenTonGiao, N.TenNgach, L.HeSoLuong, L.NgayHuong from NhanVien NV "
            + " left join DanToc DT on NV.MaDanToc = DT.MaDanToc left join TonGiao TG on NV.MaTonGiao = TG.MaTonGiao "
            + " left join Ngach N on NV.MaNgach = N.MaNgach left join LUONG L on NV.MaNV = L.MaNV and NV.MaNgach = L.MaNgach and NV.BacNhanVien = L.BacLuong "            
            + " where NV.Id = " + id + " ORDER BY L.ID DESC";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result = Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return result;

        }
        public bool SaveStaff(StaffDto staff)
        {
            using (var connection = new SqlConnection(DataProvider.SqlConnectionString()))
            {
                connection.Open();

                var command = connection.CreateCommand();

                // Start a local transaction.
                var transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = SqlQueries.SqlInsertNhanVien;

                    command.Parameters.AddWithValue("@maNV", staff.MaNv);
                    command.Parameters.AddWithValue("@ho", staff.HoLot);
                    command.Parameters.AddWithValue("@ten", staff.Ten);
                    command.Parameters.AddWithValue("@tenThuongDung", string.Empty); // empty
                    command.Parameters.AddWithValue("@tenGoiKhac", staff.TenKhac);
                    command.Parameters.AddWithValue("@ngaySinh", staff.NgaySinh.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@noiSinh", staff.NoiSinh.MaTinhThanh);
                    command.Parameters.AddWithValue("@gioiTinh", staff.GioiTinh);
                    command.Parameters.AddWithValue("@maDanToc", staff.DanToc.MaDanToc);
                    command.Parameters.AddWithValue("@maTonGiao", staff.TonGiao.MaTonGiao);
                    command.Parameters.AddWithValue("@quocTich", staff.QuocTich);
                    command.Parameters.AddWithValue("@dienThoai", staff.DienThoai);
                    command.Parameters.AddWithValue("@maChucVu", staff.ChucVu.MaChucVu);
                    command.Parameters.AddWithValue("@CMND", staff.Cmnd);
                    command.Parameters.AddWithValue("@ngayCapCMND", staff.NgayCapCmnd.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@noiCapCMND", staff.NoiCapCmnd.MaTinhThanh);
                    command.Parameters.AddWithValue("@diaChiHKTT", staff.DiaChiTt);
                    command.Parameters.AddWithValue("@maQuanHuyenHKTT", staff.QuanHuyenTt.MaQuanHuyen);
                    command.Parameters.AddWithValue("@maTinhThanhHKTT", staff.TinhThanhTt.MaTinhThanh);
                    command.Parameters.AddWithValue("@diaChiHT", staff.DiaChiHt);
                    command.Parameters.AddWithValue("@maQuanHuyenHT", staff.QuanHuyenHt.MaQuanHuyen);
                    command.Parameters.AddWithValue("@maTinhThanhHT", staff.TinhThanhHt.MaTinhThanh);
                    command.Parameters.AddWithValue("@thanhPhanGiaDinhXuatThan", string.Empty); // empty
                    command.Parameters.AddWithValue("@ngheNghiepTuyenDung", string.Empty); // empty
                    command.Parameters.AddWithValue("@hangNhanVien", staff.Hang);
                    command.Parameters.AddWithValue("@bacNhanVien", staff.Bac);
                    command.Parameters.AddWithValue("@maNgach", staff.MaNgach);
                    command.Parameters.AddWithValue("@email", staff.Email);
                    command.Parameters.AddWithValue("@tinhTrangHonNhan", staff.TinhTrangHonNhan);
                    command.Parameters.AddWithValue("@ngayVaoLam", staff.NgayVaoLam.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@ngayTuyenDung", string.Empty); // empty
                    command.Parameters.AddWithValue("@ngayHieuLuc", staff.NgayHieuLuc.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@tinhTrang", staff.TinhTrang);
                    command.Parameters.AddWithValue("@tinhTrangLamViec", staff.TrangThaiLamViec.MaTrangThai);
                    command.Parameters.AddWithValue("@trinhDoVanHoa", staff.TrinhDoVanHoa);
                    command.Parameters.AddWithValue("@queQuan", staff.QueQuan.MaTinhThanh);
                    command.Parameters.AddWithValue("@ngayVaoDCSVN", staff.NgayVaoDcsVn.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@ngayChinhThucVaoDCSVN", staff.NgayChinhThucVaoDcsVn.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@ngayNhapNgu", staff.NgayXuatNgu.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@ngayXuatNgu", staff.NgayNhapNgu.ToString("MM/dd/yyyy"));
                    command.Parameters.AddWithValue("@quanHamCaoNhat", staff.QuanHamCaoNhat);
                    command.Parameters.AddWithValue("@tinhTrangSucKhoe", staff.TinhTrangSucKhoe);
                    command.Parameters.AddWithValue("@chieuCao", staff.ChieuCao);
                    command.Parameters.AddWithValue("@canNang", staff.CanNang);
                    command.Parameters.AddWithValue("@nhomMau", staff.NhomMau);
                    command.Parameters.AddWithValue("@thuongBinhHang", staff.ThuongBinhHang);
                    command.Parameters.AddWithValue("@giaDinhChinhSach", staff.GiaDinhChinhSach);
                    command.Parameters.AddWithValue("@tuNhanXet", staff.TuNhanXet);

                    var nhanVienId = Convert.ToInt32(command.ExecuteScalar());

                    // save data into table: NhanVien
                    var maNhanVien = staff.MaNv;

                    if (staff.DsQuaTrinhCongTac.Count > 0)
                    {
                        command.CommandText = SqlQueries.SqlInsertQuaTrinhCongTac;

                        foreach (var quaTrinhCongTacDto in staff.DsQuaTrinhCongTac)
                        {
                            command.Parameters.Clear();

                            command.Parameters.AddWithValue("@maNV", maNhanVien);
                            command.Parameters.AddWithValue("@noiCongTac", quaTrinhCongTacDto.NoiCongTac);
                            command.Parameters.AddWithValue("@ngheNghiep", quaTrinhCongTacDto.CongViec);
                            command.Parameters.AddWithValue("@thanhTich", quaTrinhCongTacDto.ThanhTich);
                            //command.Parameters.AddWithValue("@tuNgay", quaTrinhCongTacDto.TuNgay.ToString("MM/dd/yyyy"));
                            //command.Parameters.AddWithValue("@denNgay", quaTrinhCongTacDto.DenNgay.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@tuNgay", quaTrinhCongTacDto.TuNgay);
                            command.Parameters.AddWithValue("@denNgay", quaTrinhCongTacDto.DenNgay);
                            command.Parameters.AddWithValue("@ghiChu", quaTrinhCongTacDto.GhiChu);
                            command.Parameters.AddWithValue("@ngayHieuLuc", quaTrinhCongTacDto.NgayHieuLuc.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@tinhTrang", quaTrinhCongTacDto.TinhTrang);
                            command.Parameters.AddWithValue("@giaiDoan", quaTrinhCongTacDto.GiaiDoan);

                            command.ExecuteNonQuery();
                        }
                    }

                    if (staff.DsBangCapChungChi.Count > 0)
                    {
                        command.CommandText = SqlQueries.SqlInsertTrinhDo;

                        foreach (var bangCapChungChiDto in staff.DsBangCapChungChi)
                        {
                            command.Parameters.Clear();

                            command.Parameters.AddWithValue("@maBCCC", bangCapChungChiDto.MaBangCapChungChi);
                            command.Parameters.AddWithValue("@maNV", maNhanVien);
                            command.Parameters.AddWithValue("@maTruong", bangCapChungChiDto.CoSoDaotao.MaTruong);
                            command.Parameters.AddWithValue("@maLoaiBCCC", bangCapChungChiDto.LoaiBangCapChungChi.MaLoaiBCCC);
                            //command.Parameters.AddWithValue("@maNganh", bangCapChungChiDto.NganhDaoTao.Id);
                            //command.Parameters.AddWithValue("@maChuyenNganh", bangCapChungChiDto.ChuyenNganhDaoTao.Id);
                            command.Parameters.AddWithValue("@maLoaiTrinhDo", bangCapChungChiDto.LoaiTrinhDo.MaLoaiTrinhDo);
                            command.Parameters.AddWithValue("@maHeDaoTao", bangCapChungChiDto.HeDaoTao.MaHeDaoTao);
                            command.Parameters.AddWithValue("@thoiGianDaoTaoTu", bangCapChungChiDto.ThoiGianDaoTao);
                            command.Parameters.AddWithValue("@thoiGianDaoTaoDen", bangCapChungChiDto.ThoiGianDaoTao);
                            command.Parameters.AddWithValue("@ngayTotNghiep", bangCapChungChiDto.NgayCapBang.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@loaiTotNghiep", string.Empty);
                            command.Parameters.AddWithValue("@ngayCap", bangCapChungChiDto.NgayCapBang.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@moTaThem", bangCapChungChiDto.GhiChu);
                            command.Parameters.AddWithValue("@isBangCap", bangCapChungChiDto.IsBangCap);
                            command.Parameters.AddWithValue("@maCoSoDaoTao", bangCapChungChiDto.CoSoDaotao.MaTruong);
                            command.Parameters.AddWithValue("@ngayHieuLuc", bangCapChungChiDto.NgayHieuLuc.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@tinhTrang", bangCapChungChiDto.TinhTrang);

                            command.ExecuteNonQuery();
                        }
                    }

                    if (staff.DsKhenThuongKyLuat.Count > 0)
                    {
                        foreach (var khenThuongKyLuatDto in staff.DsKhenThuongKyLuat)
                        {
                            if (khenThuongKyLuatDto.IsKhenThuong)
                            {
                                command.CommandText = SqlQueries.SqlInsertKhenThuong;
                                command.Parameters.Clear();

                                command.Parameters.AddWithValue("@maNV", maNhanVien);
                                command.Parameters.AddWithValue("@ngay", khenThuongKyLuatDto.ThoiGianHieuLuc);
                                command.Parameters.AddWithValue("@loai", string.Empty);
                                command.Parameters.AddWithValue("@noiDung", khenThuongKyLuatDto.NoiDung);
                                command.Parameters.AddWithValue("@hinhThuc", string.Empty);
                                command.Parameters.AddWithValue("@capQuyetDinh", khenThuongKyLuatDto.CapQuyetDinh);
                                command.Parameters.AddWithValue("@soQuyetDinh", khenThuongKyLuatDto.SoQuyetDinh);
                                command.Parameters.AddWithValue("@ngayHieuLuc", khenThuongKyLuatDto.NgayHieuLuc.ToString("MM/dd/yyyy"));
                                command.Parameters.AddWithValue("@tinhTrang", khenThuongKyLuatDto.TinhTrang);
                            }
                            else
                            {
                                command.CommandText = SqlQueries.SqlInsertKyLuat;
                                command.Parameters.Clear();

                                command.Parameters.AddWithValue("@maNV", maNhanVien);
                                command.Parameters.AddWithValue("@ngayKL", khenThuongKyLuatDto.ThoiGianHieuLuc);
                                command.Parameters.AddWithValue("@loaiKL", string.Empty);
                                command.Parameters.AddWithValue("@lyDoKL", khenThuongKyLuatDto.NoiDung);
                                command.Parameters.AddWithValue("@hinhThucKL", string.Empty);
                                command.Parameters.AddWithValue("@capQuyetDinh", khenThuongKyLuatDto.CapQuyetDinh);
                                command.Parameters.AddWithValue("@soQuyetDinh", khenThuongKyLuatDto.SoQuyetDinh);
                                command.Parameters.AddWithValue("@ngayHieuLuc", khenThuongKyLuatDto.NgayHieuLuc.ToString("MM/dd/yyyy"));
                                command.Parameters.AddWithValue("@tinhTrang", khenThuongKyLuatDto.TinhTrang);
                            }
                            command.ExecuteNonQuery();
                        }
                    }

                    if (staff.DsQuanHeGiaDinh.Count > 0)
                    {
                        command.CommandText = SqlQueries.SqlInsertThanNhan;

                        foreach (var quanHeGiaDinhDto in staff.DsQuanHeGiaDinh)
                        {
                            command.Parameters.Clear();

                            command.Parameters.AddWithValue("@maNV", maNhanVien);
                            command.Parameters.AddWithValue("@hoTen", quanHeGiaDinhDto.HoTen);
                            if (String.IsNullOrEmpty(quanHeGiaDinhDto.NgaySinh))
                            {
                                command.Parameters.AddWithValue("@ngaySinh", string.Empty);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@ngaySinh", quanHeGiaDinhDto.NgaySinh);
                            }
                            command.Parameters.AddWithValue("@noiSinh", string.Empty);
                            command.Parameters.AddWithValue("@maQuanHe", quanHeGiaDinhDto.LoaiQuanHe.MaQuanHe);
                            command.Parameters.AddWithValue("@dacDiemLichSu", quanHeGiaDinhDto.DacDiemLichSu);
                            command.Parameters.AddWithValue("@ngheNghiep", quanHeGiaDinhDto.NgheNghiep);
                            command.Parameters.AddWithValue("@ghiChu", quanHeGiaDinhDto.GhiChu);
                            command.Parameters.AddWithValue("@quanHuyen", quanHeGiaDinhDto.QuanHuyen.MaQuanHuyen);
                            command.Parameters.AddWithValue("@tinhThanh", quanHeGiaDinhDto.TinhThanh.MaTinhThanh);
                            command.Parameters.AddWithValue("@ngayHieuLuc", quanHeGiaDinhDto.NgayHieuLuc.ToString("MM/dd/yyyy"));
                            command.Parameters.AddWithValue("@tinhTrang", quanHeGiaDinhDto.TinhTrang);

                            command.ExecuteNonQuery();
                        }
                    }

                    // Upload images
                    if (!UploadImage(staff.HinhAnh, staff.MaNv, string.Empty)) return false;

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Both records are written to database.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);

                    // Attempt to roll back the transaction. 
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }

                    return false;
                }
            }

            return true;
        }

        public static bool UploadImage(byte[] imageBytes, string maNhanVien, string ghiChu)
        {
            try
            {
                var con = DataProvider.TaoKetNoiCSDL();
                var command = new SqlCommand(SqlQueries.SqlInsertHinhAnh, con);

                command.Parameters.AddWithValue("@maNV", maNhanVien);
                command.Parameters.AddWithValue("@hinhAnh", imageBytes);
                command.Parameters.AddWithValue("@ghiChu", ghiChu);
                command.Parameters.AddWithValue("@ngayHieuLuc", DateTime.Now.ToString());
                command.Parameters.AddWithValue("@tinhTrang", 0);

                command.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //Update phiên bản mới
        public int LuuNhanVien(NhanVienDTO nhanvienDTO)
        {
            int id = 0;
            string strInsert = "INSERT INTO NhanVien([MaNV],MaPK,[MaBMBP],[Ho],[Ten],[TenThuongDung],[TenGoiKhac],[NgaySinh] "
                + ",[NoiSinh],[GioiTinh],[MaDanToc],[MaTonGiao],[QuocTich],[DienThoai],[MaChucVu],[CMND],[NgayCapCMND] "
                + ",[MaNoiCapCMND],[HoKhauThuongTruDiaChi],[HoKhauThuongTruMaQuanHuyen],[HoKhauThuongTruMaTinhThanh],[DiaChiHienTaiDiaChi],[DiaChiHienTaiMaQuanHuyen] "
                + ",[DiaChiHienTaiMaTinhThanh],[ThanhPhanGiaDinhXuatThan],[NgheNghiepTuyenDung],[HangNhanVien],[BacNhanVien] "
                + ",[MaNgach],[Email],[TinhTrangHonNhan],[NgayVaoLam],[NgayTuyenDung],[NgayHieuLuc],[TinhTrang] "
                + ",[TinhTrangLamViec],[TrinhDoVanHoa],[QueQuan],[NgayVaoDCSVN],[NgayChinhThucVaoDCSVN],[NgayNhapNgu] "
                + ",[NgayXuatNgu],[QuanHamCaoNhat],[TinhTrangSucKhoe],[ChieuCao],[CanNang],[NhomMau],[ThuongBinhHang] "
                + ",[GiaDinhChinhSach],DangVien, QuanNgu, ThuongBinh,MaSoThue,SoTaiKhoan,SoBHXH,SoATM,NgheNghiepTruocTuyenDung,[TuNhanXet]) VALUES ('" + nhanvienDTO.MaNV + "','" + nhanvienDTO.MaPK
                + "','" + nhanvienDTO.MaBMBP + "',"
                + (String.IsNullOrEmpty(nhanvienDTO.Ho) ? "null,'" : ("N'" + nhanvienDTO.Ho) + "',N'") + nhanvienDTO.Ten
                + "'," + (String.IsNullOrEmpty(nhanvienDTO.TenThuongDung) ? "null," : ("N'" + nhanvienDTO.TenThuongDung + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.TenGoiKhac) ? "null," : ("N'" + nhanvienDTO.TenGoiKhac + "',"))
                //+ (String.IsNullOrEmpty(nhanvienDTO.NgaySinh) ? "null," : ("convert(date,'" + nhanvienDTO.NgaySinh + "',103),"))
                + (String.IsNullOrEmpty(nhanvienDTO.NgaySinh) ? "null," : "'"+ nhanvienDTO.NgaySinh + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.NoiSinh) ? "null," : ("N'" + nhanvienDTO.NoiSinh + "','")) + nhanvienDTO.GioiTinh
                + "'," + (String.IsNullOrEmpty(nhanvienDTO.MaDanToc) ? "null," : ("'" + nhanvienDTO.MaDanToc + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.MaTonGiao) ? "null," : ("'" + nhanvienDTO.MaTonGiao + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.QuocTich) ? "null," : ("N'" + nhanvienDTO.QuocTich + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.DienThoai) ? "null," : ("'" + nhanvienDTO.DienThoai + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.MaChucVu) ? "null," : ("'" + nhanvienDTO.MaChucVu + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.CMND) ? "null," : ("'" + nhanvienDTO.CMND + "',"))
                //+ (String.IsNullOrEmpty(nhanvienDTO.NgayCapCMND) ? "null," : ("convert(date,'" + nhanvienDTO.NgayCapCMND + "',103),"))
                + (String.IsNullOrEmpty(nhanvienDTO.NgayCapCMND) ? "null," : "'" + nhanvienDTO.NgayCapCMND + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.MaNoiCapCMND) ? "null," : ("'" + nhanvienDTO.MaNoiCapCMND + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.HoKhauThuongTruDiaChi) ? "null," : ("N'" + nhanvienDTO.HoKhauThuongTruDiaChi + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.HoKhauThuongTruMaQuanHuyen) ? "null," : ("'" + nhanvienDTO.HoKhauThuongTruMaQuanHuyen + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.HoKhauThuongTruMaTinhThanh) ? "null," : ("'" + nhanvienDTO.HoKhauThuongTruMaTinhThanh + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.DiaChiHienTaiDiaChi) ? "null," : ("N'" + nhanvienDTO.DiaChiHienTaiDiaChi + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.DiaChiHienTaiMaQuanHuyen) ? "null," : ("'" + nhanvienDTO.DiaChiHienTaiMaQuanHuyen + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.DiaChiHienTaiMaTinhThanh) ? "null," : ("'" + nhanvienDTO.DiaChiHienTaiMaTinhThanh + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.ThanhPhanGiaDinhXuatThan) ? "null," : ("N'" + nhanvienDTO.ThanhPhanGiaDinhXuatThan + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.NgheNghiepTuyenDung) ? "null," : ("N'" + nhanvienDTO.NgheNghiepTuyenDung + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.HangNhanVien) ? "null," : ("'" + nhanvienDTO.HangNhanVien + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.BacNhanVien) ? "null," : ("'" + nhanvienDTO.BacNhanVien + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.MaNgach) ? "null," : ("'" + nhanvienDTO.MaNgach + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.Email) ? "null," : ("'" + nhanvienDTO.Email + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.TinhTrangHonNhan) ? "null," : ("N'" + nhanvienDTO.TinhTrangHonNhan + "',"))
                //+ (String.IsNullOrEmpty(nhanvienDTO.NgayVaoLam) ? "null," : ("convert(date,'" + nhanvienDTO.NgayVaoLam + "',103),"))
                //+ (String.IsNullOrEmpty(nhanvienDTO.NgayTuyenDung) ? "null," : ("convert(date,'" + nhanvienDTO.NgayTuyenDung + "',103)"))
                + (String.IsNullOrEmpty(nhanvienDTO.NgayVaoLam) ? "null," : "'" + nhanvienDTO.NgayVaoLam + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.NgayTuyenDung) ? "null," : "'" + nhanvienDTO.NgayTuyenDung + "',")
                + "Getdate(), 0,"
                + (String.IsNullOrEmpty(nhanvienDTO.TinhTrangLamViec) ? "null," : ("'" + nhanvienDTO.TinhTrangLamViec + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.TrinhDoVanHoa) ? "null," : ("'" + nhanvienDTO.TrinhDoVanHoa + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.QueQuan) ? "null," : ("N'" + nhanvienDTO.QueQuan + "',"))
                ////+ (String.IsNullOrEmpty(nhanvienDTO.NgayVaoDCSVN) ? "null," : ("convert(date,'" + nhanvienDTO.NgayVaoDCSVN + "',103),"))
                ////+ (String.IsNullOrEmpty(nhanvienDTO.NgayChinhThucVaoDCSVN) ? "null," : ("convert(date,'" + nhanvienDTO.NgayChinhThucVaoDCSVN + "',103),"))
                ////+ (String.IsNullOrEmpty(nhanvienDTO.NgayNhapNgu) ? "null," : ("convert(date,'" + nhanvienDTO.NgayNhapNgu + "',103),"))
                ////+ (String.IsNullOrEmpty(nhanvienDTO.NgayXuatNgu) ? "null," : ("convert(date,'" + nhanvienDTO.NgayXuatNgu + "',103),"))
                + (String.IsNullOrEmpty(nhanvienDTO.NgayVaoDCSVN) ? "null," : "'" + nhanvienDTO.NgayVaoDCSVN + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.NgayChinhThucVaoDCSVN) ? "null," : "'" + nhanvienDTO.NgayChinhThucVaoDCSVN + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.NgayNhapNgu) ? "null," : "'" + nhanvienDTO.NgayNhapNgu + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.NgayXuatNgu) ? "null," : "'" + nhanvienDTO.NgayXuatNgu + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.QuanHamCaoNhat) ? "null," : ("N'" + nhanvienDTO.QuanHamCaoNhat + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.TinhTrangSucKhoe) ? "null," : ("N'" + nhanvienDTO.TinhTrangSucKhoe + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.ChieuCao) ? "null," : ("'" + nhanvienDTO.ChieuCao + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.CanNang) ? "null," : ("'" + nhanvienDTO.CanNang + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.NhomMau) ? "null," : ("'" + nhanvienDTO.NhomMau + "',"))
                + (String.IsNullOrEmpty(nhanvienDTO.ThuongBinhHang) ? "null," : ("'" + nhanvienDTO.ThuongBinhHang + "',"))
                + ((nhanvienDTO.GiaDinhChinhSach) ? "1," : ("'" + 0 + "',"))
                + ((nhanvienDTO.DangVien) ? "1," : ("'" + 0 + "',"))
                + ((nhanvienDTO.QuanNgu) ? "1," : ("'" + 0 + "',"))
                + ((nhanvienDTO.ThuongBinh) ? "1," : ("'" + 0 + "',"))
                + ((String.IsNullOrEmpty(nhanvienDTO.MaSoThue))? "null,":"N'" + nhanvienDTO.MaSoThue + "',")
                + ((String.IsNullOrEmpty(nhanvienDTO.SoTaiKhoan)) ? "null," : "N'" + nhanvienDTO.SoTaiKhoan + "',")
                + ((String.IsNullOrEmpty(nhanvienDTO.SoBHXH)) ? "null," : "N'" + nhanvienDTO.SoBHXH + "',")
                + ((String.IsNullOrEmpty(nhanvienDTO.SoATM)) ? "null," : "N'" + nhanvienDTO.SoATM + "',")
                + ((String.IsNullOrEmpty(nhanvienDTO.NgheNghiepTruocTuyenDung)) ? "null," : "N'" + nhanvienDTO.NgheNghiepTruocTuyenDung + "',")
                + (String.IsNullOrEmpty(nhanvienDTO.TuNhanXet) ? "null)" : ("N'" + nhanvienDTO.TuNhanXet + "');"))
                + " SELECT SCOPE_IDENTITY();";
            _conn = DataProvider.TaoKetNoiCSDL();
            var command = _conn.CreateCommand();
            var transaction = _conn.BeginTransaction();
            command.Connection = _conn;
            command.CommandText = strInsert;
            command.Transaction = transaction;
            try
            {
                var idReturn = command.ExecuteScalar();
                if (idReturn != null)
                {
                    id = Convert.ToInt32(idReturn);
                }
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }
            _conn.Close();
            return id;
        }

        public void CapNhatTrangThaiPhienBanCu(int Id)
        {
            string strUpdate = "update NhanVien set TinhTrang = 1 where Id = " + Id;
            _conn = DataProvider.TaoKetNoiCSDL();
            DataProvider.ThucThiSQL(strUpdate, _conn);
            _conn.Close();
        }

        public List<NhanVienDTO> LayTatCaCacPhienBanCapNhat(string strMaNV)
        {
            List<NhanVienDTO> result = new List<NhanVienDTO>();
            string strSelect = "Select * from NhanVien where  MaNV = '" + strMaNV + "' order by Id";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result.Add(Utils.MappingSQLDRToDTO2<NhanVienDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public bool UpdateTuNhanXet(string tnx, string id)
        {
            string query = "UPDATE NhanVien set TuNhanXet = " + (String.IsNullOrEmpty(tnx) ? "null " : ("N'" + tnx + "' "))
                + "where Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool UpdateBaoHiemTaiKhoanNganHang(NhanVienDTO nhanvienDTO)
        {
            string query = "UPDATE NhanVien set MaSoThue = " + (String.IsNullOrEmpty(nhanvienDTO.MaSoThue) ? "null, " : ("N'" + nhanvienDTO.MaSoThue + "', "))
                + "SoTaiKhoan = " + (String.IsNullOrEmpty(nhanvienDTO.SoTaiKhoan)? "null, ": "N'" + nhanvienDTO.SoTaiKhoan + "', ")
                + "SoBHXH = " + (String.IsNullOrEmpty(nhanvienDTO.SoBHXH)? "null, " : "N'"+nhanvienDTO.SoBHXH + "', ")
                + "SoATM = " + (String.IsNullOrEmpty(nhanvienDTO.SoATM)? "null " : "N'" + nhanvienDTO.SoATM + "' ")
                + "where Id = " + nhanvienDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public string LayMaNVMax(string maBoMon)
        {
            string result = string.Empty;
            string strSelect = string.Format("SELECT MAX(MaNV) FROM NhanVien WHERE MaBMBP = {0}", maBoMon);
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strSelect, _conn);
            while (sdr.Read())
            {
                //result.Add(Utils.MappingSQLDRToDTO<NhanVienDTO>(sdr));
                result = sdr.GetString(0);
            }
            sdr.Close();
            _conn.Close();
            return result ?? string.Empty;
        }
    }
}
