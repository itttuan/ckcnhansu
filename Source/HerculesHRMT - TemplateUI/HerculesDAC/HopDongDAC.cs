﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class HopDongDAC
    {
        const string StrSelectByNhanVien = "select hd.*, lhd.TenLoaiHD from HopDong hd left join LoaiHopDong lhd on hd.MaLoaiHD = lhd.MaLoaiHD where MaNV = '{0}' and hd.TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<HopDongDTO> LayDanhSachHopDongTheoNhanVien(string strMaNV)
        {
            List<HopDongDTO> listResult = new List<HopDongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while(sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<HopDongDTO>(sdr));
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }
    }
}
