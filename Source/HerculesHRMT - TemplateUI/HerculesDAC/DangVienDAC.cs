﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class DangVienDAC
    {
        SqlConnection _conn = new SqlConnection();

        public bool Save (DangVienDTO dangvienobj)
        {
            try
            {
                string query = "INSERT INTO DangVien(MaNV,BiDanh,SoThe,NgayChinhThuc"
                    + ",NgayVaoDang,NgayCapThe,ChiBo,DangBo,ChucVuDang, NgayHieuLuc,TinhTrang) "
                    + "VALUES('" +dangvienobj.MaNV + "','" + dangvienobj.BiDanh + "','"
                    +  dangvienobj.SoThe + "',"
                    + "CONVERT(date,'" + dangvienobj.NgayChinhThuc.ToString() + "',103),"
                    + "CONVERT(date,'" + dangvienobj.NgayVaoDang.ToString() + "',103),"
                    + "CONVERT(date,'" + dangvienobj.NgayCapThe.ToString() + "',103),'"
                    + dangvienobj.ChiBo + "','" + dangvienobj.DangBo + "','" + dangvienobj.ChucVuDang + 
                    "', GetDate(),0";
                _conn = DataProvider.TaoKetNoiCSDL();
                int result = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (result != 0)
                    return true;
                return false;
            }
            catch
            {
            }
            return false;
        }
    }
}
