﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class TrangThaiLamViecDAC
    {
        const string SelectAll = "SELECT * FROM TrangThaiLamViec TTLV";
        const string SelectByMaTrangThai = "SELECT * FROM TrangThaiLamViec TTLV WHERE TTLV.MaTrangThai = \'{0}\'";
        const string StrInsert = "INSERT INTO TrangThaiLamViec (MaTrangThai, TenTrangThai, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', '{3}', '{4}')";
        const string StrUpdate = "UPDATE TrangThaiLamViec SET TenTrangThai = N'{0}', GhiChu = N'{1}' WHERE MaTrangThai = '{2}'";
        const string StrDelete = "UPDATE TrangThaiLamViec SET TinhTrang = 1 WHERE MaTrangThai = '{0}'";
        const string StrMaxMaTrangThai = "SELECT MAX(MaTrangThai) MaTrangThai FROM TrangThaiLamViec";

        SqlConnection _conn = new SqlConnection();

        private TrangThaiLamViecDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            TrangThaiLamViecDTO ttDTO = Utils.MappingSQLDRToDTO<TrangThaiLamViecDTO>(sdr);

            if (ttDTO.GhiChu != null && ttDTO.GhiChu.CompareTo("NULL") == 0)
                ttDTO.GhiChu = string.Empty;

            return ttDTO;
        }

        public List<TrangThaiLamViecDTO> GetAll()
        {
            var result = new List<TrangThaiLamViecDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(SelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public TrangThaiLamViecDTO GetByMaTrangThai(string maTrangThai)
        {
            var result = new TrangThaiLamViecDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaTrangThai, maTrangThai);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaTrangThai()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaTrangThai);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaTrangThai"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(TrangThaiLamViecDTO ttNew)
        {
            var query = string.Format(StrInsert, ttNew.MaTrangThai, ttNew.TenTrangThai, ttNew.GhiChu, ttNew.NgayHieuLuc.ToString("yyyy-MM-dd"), ttNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(TrangThaiLamViecDTO ttUpdate)
        {
            var query = string.Format(StrUpdate, ttUpdate.TenTrangThai, ttUpdate.GhiChu, ttUpdate.MaTrangThai);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(TrangThaiLamViecDTO ttDel)
        {
            var query = string.Format(StrDelete, ttDel.MaTrangThai);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
