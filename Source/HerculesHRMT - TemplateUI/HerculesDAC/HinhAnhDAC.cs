﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class HinhAnhDAC
    {
        public bool UploadImage(HinhAnhDTO hinhanhDTO)
        {
            try
            {
                var con = DataProvider.TaoKetNoiCSDL();
                var command = new SqlCommand(SqlQueries.SqlInsertHinhAnh, con);

                command.Parameters.AddWithValue("@maNV", hinhanhDTO.MaNV);
                command.Parameters.AddWithValue("@hinhAnh", hinhanhDTO.HinhAnh);
                command.Parameters.AddWithValue("@ghiChu", "");
                //command.Parameters.AddWithValue("@ngayHieuLuc", DateTime.Now.ToString());
                command.Parameters.AddWithValue("@tinhTrang", 0);

                command.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<byte[]> layHinh(string maNV)
        {
            List<byte[]> listImg = new List<byte[]>();
            try
            {
                string strQuery = "SELECT HINHANH from HINHANH where MaNV='" + maNV + "' and TinhTrang = 0 order by Id";
                SqlConnection conn = DataProvider.TaoKetNoiCSDL();
                SqlDataReader dr = DataProvider.TruyVanDuLieu(strQuery, conn);
                while (dr.Read())
                {
                    byte[] ha = (byte[])dr["HINHANH"];
                    if (ha != null)
                    {
                        listImg.Add(ha);
                    }
                }
                dr.Close();
                conn.Close();
                return listImg;
            }
            catch
            {
                return listImg;
            }
        }
    }
}
