﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class PhongKhoaDAC
    {
        const string StrSelectAll = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 order by left(MaPhongKhoa,1) desc";
        const string StrSelectByMaPhongKhoa = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 AND MaPhongKhoa = \'{0}\'";
        const string StrInsert = "INSERT INTO PhongKhoa (MaPhongKhoa, TenPhongKhoa, TenVietTat, ThuTuBaoCao, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', {3}, N'{4}', '{5}', '{6}')";
        const string StrUpdate = "UPDATE PhongKhoa SET TenPhongKhoa = N'{0}', TenVietTat = N'{1}', ThuTuBaoCao = {2}, GhiChu = N'{3}' WHERE MaPhongKhoa = '{4}'";
        const string StrDelete = "UPDATE PhongKhoa SET TinhTrang = 1 WHERE MaLoaiTrinhDo = '{0}'";
        const string StrMaxMaPhongKhoa = "SELECT MAX(MaPhongKhoa) MaPhongKhoa FROM PhongKhoa";

        SqlConnection _conn = new SqlConnection();

        private PhongKhoaDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            PhongKhoaDTO pkDTO = Utils.MappingSQLDRToDTO<PhongKhoaDTO>(sdr);
            if (pkDTO.TenVietTat != null && pkDTO.TenVietTat.CompareTo("NULL") == 0)
                pkDTO.TenVietTat = string.Empty;

            if (pkDTO.GhiChu != null && pkDTO.GhiChu.CompareTo("NULL") == 0)
                pkDTO.GhiChu = string.Empty;

            return pkDTO;
        }

        public List<PhongKhoaDTO> GetAll()
        {
            var result = new List<PhongKhoaDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        {
            var result = new PhongKhoaDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectByMaPhongKhoa, maPhongKhoa);
            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaPhongKhoa()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaPhongKhoa);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaPhongKhoa"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(PhongKhoaDTO pkNew)
        {
            string ttbc = pkNew.ThuTuBaoCao != null ? pkNew.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrInsert, pkNew.MaPhongKhoa, pkNew.TenPhongKhoa, pkNew.TenVietTat, ttbc, pkNew.GhiChu, pkNew.NgayHieuLuc.ToString("yyyy-MM-dd"), pkNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(PhongKhoaDTO pkUpdate)
        {
            string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrUpdate, pkUpdate.TenPhongKhoa, pkUpdate.TenVietTat, ttbc, pkUpdate.GhiChu, pkUpdate.MaPhongKhoa);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(PhongKhoaDTO pkDel)
        {
            var query = string.Format(StrDelete, pkDel.MaPhongKhoa);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
