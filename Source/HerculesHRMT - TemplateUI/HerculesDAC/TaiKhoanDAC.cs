﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class TaiKhoanDAC
    {
        SqlConnection _conn = new SqlConnection();
        public TaiKhoanDTO LayTaiKhoanUserDangNhap(string strUsename, string strPassword)
        {
            TaiKhoanDTO result = null;
            string strTruyVan = "Select TK.Id, Username, Password, TK.MaNV, TK.NgayHieuLuc, TK.TinhTrang, NV.Ho, NV.Ten"
             + " from TaiKhoan TK left join NhanVien NV on TK.MaNV = NV.MaNV where Username = '" + strUsename + "' and Password = '"
             + strPassword + "' and TK.TinhTrang = 0";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strTruyVan, _conn);
            if (sdr.Read())
            {
                result = Utils.MappingSQLDRToDTO<TaiKhoanDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public bool Save(TaiKhoanDTO tkDTO)
        {
            try
            {
                string strInsert = "insert into TaiKhoan (Username, Password, MaNV, NgayHieuLuc, TinhTrang) values ('"
                    + tkDTO.Username + "','" + tkDTO.Password + "','" + tkDTO.MaNV + "', getdate(),0)";
                _conn = DataProvider.TaoKetNoiCSDL();
                int result = DataProvider.ThucThiSQL(strInsert, _conn);
                _conn.Close();
                return (result > 0);
            }
            catch
            {
            }
            return false;
        }
        public void CapNhatThoiGianDangNhap(string strUsername)
        {
            try
            {
                string strUpdate = "Update TaiKhoan set LastLogonTime = getDate() where Username = " + strUsername; 
                _conn = DataProvider.TaoKetNoiCSDL();
                int result = DataProvider.ThucThiSQL(strUpdate, _conn);
                _conn.Close();
            }
            catch
            {
            }
        }
    }
}
