﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiBangChuyenMonDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<LoaiBangChuyenMonDTO> LayDanhSachLoaiBangChuyenMon()
        {
            List<LoaiBangChuyenMonDTO> listRes = new List<LoaiBangChuyenMonDTO>();
            string query = "select * from LoaiBangChuyenMon where TinhTrang = 0";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<LoaiBangChuyenMonDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public LoaiBangChuyenMonDTO TimLoaiBangChuyenMon(string id)
        {
            LoaiBangChuyenMonDTO listRes = new LoaiBangChuyenMonDTO();
            string query = "select * from LoaiBangChuyenMon where TinhTrang = 0 and Id = " + id;
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<LoaiBangChuyenMonDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
