﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class BaoHiemDAC
    {
        const string StrSelectBHXHByNhanVien = "select * from BaoHiem where MaNV = '{0}' and TinhTrang = 0 and LaBHXH = 1";
        const string StrSelectBHYTByNhanVien = "select * from BaoHiem where MaNV = '{0}' and TinhTrang = 0 and LaBHXH = 0";
        SqlConnection _conn = new SqlConnection();

        public List<BaoHiemDTO> LayDanhSachBHXHTheoNhanVien(string strMaNV)
        {
            List<BaoHiemDTO> listResult = new List<BaoHiemDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectBHXHByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<BaoHiemDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<BaoHiemDTO> LayDanhSachBHYTTheoNhanVien(string strMaNV)
        {
            List<BaoHiemDTO> listResult = new List<BaoHiemDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectBHYTByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<BaoHiemDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public bool Save(BaoHiemDTO baohiemDTO)
        {
            string query = "INSERT INTO BaoHiem (MaNV, SoSo, NgayCap, NoiDKKCB, LaBHXH, NgayHieuLuc, TinhTrang) VALUES ('" + baohiemDTO.MaNV + "',"
                + (String.IsNullOrEmpty(baohiemDTO.SoSo) ? "null," : "'" + baohiemDTO.SoSo + "',")
                + (String.IsNullOrEmpty(baohiemDTO.NgayCap) ? "null," : "'" + baohiemDTO.NgayCap + "',")
                + (String.IsNullOrEmpty(baohiemDTO.NoiDKKCB) ? "null," : "N'" + baohiemDTO.NoiDKKCB + "',")
                + (baohiemDTO.LaBHXH ? "1," : "0,")
                + "getDate(),0)";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Update(BaoHiemDTO baohiemDTO)
        {
            string query = "UPDATE BaoHiem SET SoSo = " + (String.IsNullOrEmpty(baohiemDTO.SoSo) ? "null," : "'" + baohiemDTO.SoSo + "',")
                + "NgayCap = " + (String.IsNullOrEmpty(baohiemDTO.NgayCap) ? "null," : "'" + baohiemDTO.NgayCap + "',")
                + "NoiDKKCB = " + (String.IsNullOrEmpty(baohiemDTO.NoiDKKCB) ? "null " : "N'" + baohiemDTO.NoiDKKCB + "' ")
                + "WHERE Id = " + baohiemDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(string id)
        {
            string query = "UPDATE BaoHiem SET TinhTrang = 1" 
                + "WHERE Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
