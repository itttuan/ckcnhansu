﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace HerculesDAC
{
    public class ReportDAC
    {
        const string StrSelectCountNhanVien = "SELECT COUNT(*) FROM NhanVien WHERE MaPK = '{0}' OR MaBMBP = '{1}'";
        SqlConnection _conn = new SqlConnection();

        public int LaySoLuongNhanVien(string maPK, string maBoMon)
        {
            var strQuery = string.Format(StrSelectCountNhanVien, maPK, maBoMon);
            var result = 0;

            _conn = DataProvider.TaoKetNoiCSDL();

            SqlDataReader sdr = DataProvider.TruyVanDuLieu(strQuery, _conn);
            while (sdr.Read())
            {
                result = sdr.GetInt32(0);
            }
            sdr.Close();
            _conn.Close();

            return result;
        }
    }
}
