﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class DanTocDAC
    {
        const string SelectAll = "SELECT * FROM DanToc DT WHERE DT.TinhTrang = 0";
        const string SelectByMaDanToc = "SELECT * FROM DanToc DT WHERE DT.TinhTrang = 0 AND MaDanToc = \'{0}\'";
        const string StrInsert = "INSERT INTO DanToc (MaDanToc, TenDanToc, TenVietTat, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE DanToc SET TenDanToc = N'{0}', TenVietTat = N'{1}', GhiChu = N'{2}' WHERE MaDanToc = '{3}'";
        const string StrDelete = "UPDATE DanToc SET TinhTrang = 1 WHERE MaDanToc = '{0}'";
        const string StrMaxMaDanToc = "SELECT MAX(MaDanToc) MaDanToc FROM DanToc";

        SqlConnection _conn = new SqlConnection();

        private DanTocDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            DanTocDTO dtDTO = Utils.MappingSQLDRToDTO<DanTocDTO>(sdr);

            if (dtDTO.TenVietTat != null && dtDTO.TenVietTat.CompareTo("NULL") == 0)
                dtDTO.TenVietTat = string.Empty;

            if (dtDTO.GhiChu != null && dtDTO.GhiChu.CompareTo("NULL") == 0)
                dtDTO.GhiChu = string.Empty;

            return dtDTO;
        }

        public List<DanTocDTO> GetAll()
        {
            var result = new List<DanTocDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(SelectAll, _conn);

            while (sdr.Read())
            {                
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public DanTocDTO GetByMaDanToc(string maDanToc)
        {
            var result = new DanTocDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaDanToc, maDanToc);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = Utils.MappingSQLDRToDTO<DanTocDTO>(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaDanToc()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaDanToc);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaDanToc"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(DanTocDTO dtNew)
        {
            var query = string.Format(StrInsert, dtNew.MaDanToc, dtNew.TenDanToc, dtNew.TenVietTat, dtNew.GhiChu, dtNew.NgayHieuLuc.ToString("yyyy-MM-dd"), dtNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(DanTocDTO dtUpdate)
        {
            var query = string.Format(StrUpdate, dtUpdate.TenDanToc, dtUpdate.TenVietTat, dtUpdate.GhiChu, dtUpdate.MaDanToc);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(DanTocDTO dtDel)
        {
            var query = string.Format(StrDelete, dtDel.MaDanToc);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
