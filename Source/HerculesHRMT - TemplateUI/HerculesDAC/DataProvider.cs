﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class DataProvider
    {
        public static string strTenServer = ".";

        public static string SqlConnectionString()
        {
            return @"Data Source=" + strTenServer + ";Initial Catalog=hrmtdb;Integrated Security=true;";
        }

        public static SqlConnection TaoKetNoiCSDL()
        {
            try
            {
                string strChuoiKetNoi = @"Data Source=" + strTenServer + ";Initial Catalog=hrmtdb;Integrated Security=true;";
                SqlConnection conn = new SqlConnection(strChuoiKetNoi);
                conn.Open();
                return conn;
            }
            catch
            {
                throw new Exception("Lỗi kết nối Cơ Sở Dữ Liệu...");
            }
        }

        public static SqlDataReader TruyVanDuLieu(string strCauTruyVan, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                SqlDataReader dr = com.ExecuteReader();
                return dr;
            }
            catch
            {
                throw new Exception("Lỗi Truy Vấn Dữ Liệu...");
            }
        }

        public static SqlDataReader TruyVanDuLieu(string strCauTruyVan, SqlParameter[] arrParam, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                com.Parameters.AddRange(arrParam);
                SqlDataReader dr = com.ExecuteReader();
                return dr;
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi Truy Vấn Dữ Liệu..." + ex.Message);
            }
        }

        public static int ThucThiSQL(string strCauTruyVan, SqlParameter[] arrParam, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                com.Parameters.AddRange(arrParam);
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi thực thi SQL..." + ex.Message);
            }
        }

        public static int ThucThiSQL(string strCauTruyVan, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi thực thi SQL..." + ex.Message);
            }
        }

        public static bool KiemTraKetNoiSERVER()
        {
            bool isKQ = false;
            try
            {
                SqlConnection conn = TaoKetNoiCSDL();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return isKQ;
        }
    }
}
