﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiBangNgoaiNguDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<LoaiBangNgoaiNguDTO> LayDanhSachLoaiBangNgoaiNgu()
        {
            List<LoaiBangNgoaiNguDTO> listRes = new List<LoaiBangNgoaiNguDTO>();
            string query = "select * from LoaiBangNgoaiNgu";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<LoaiBangNgoaiNguDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public LoaiBangNgoaiNguDTO TimLoaiBangNgoaiNgu(string id)
        {
            LoaiBangNgoaiNguDTO listRes = new LoaiBangNgoaiNguDTO();
            string query = "select * from LoaiBangNgoaiNgu where Id = " + id;
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<LoaiBangNgoaiNguDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public List<LoaiBangNgoaiNguDTO> LayDanhSachLoaiBangNNTheoMaNN(string strMaNN)
        {
            List<LoaiBangNgoaiNguDTO> listRes = new List<LoaiBangNgoaiNguDTO>();
            string query = "select * from LoaiBangNgoaiNgu where MaNgoaiNgu = '" + strMaNN + "'";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<LoaiBangNgoaiNguDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
