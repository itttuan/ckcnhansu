﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class TinhThanhDAC
    {
        const string StrSelectAll = "SELECT * FROM TinhThanh TT WHERE TT.TinhTrang = 0";
        const string StrSelectMaTinhThanhTheoCmnd = "SELECT * FROM TinhThanh TT WHERE TT.TinhTrang = 0 and TT.DauSoCMND = '{0}'";
        const string StrSelectByMaTinhThanh = "SELECT * FROM TinhThanh TT WHERE TT.TinhTrang = 0 AND TT.MaTinhThanh = \'{0}\'";
        const string StrInsert = "INSERT INTO TinhThanh (MaTinhThanh, TenTinhThanh, TenDayDu, TenVietTat, NgayHieuLuc, TinhTrang, Loai, DauSoCMND) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}', '{6}', '{7}')";
        const string StrUpdate = "UPDATE TinhThanh SET TenTinhThanh = N'{0}', TenDayDu = N'{1}', TenVietTat = N'{2}', Loai = '{3}', DauSoCMND = '{4}' WHERE MaTinhThanh = '{5}'";
        const string StrDelete = "UPDATE TinhThanh SET TinhTrang = 1 WHERE MaTinhThanh = '{0}'";
        const string StrMaxMaTinhThanh = "SELECT MAX(MATINHTHANH) MATINHTHANH FROM TinhThanh";
        SqlConnection _conn = new SqlConnection();

        private TinhThanhDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            TinhThanhDTO ttDTO = Utils.MappingSQLDRToDTO<TinhThanhDTO>(sdr);
            if (ttDTO.Loai == 0)
            {
                ttDTO.TenLoaiTinhThanh = "Tỉnh";
            }
            else
                ttDTO.TenLoaiTinhThanh = "Thành phố";

            return ttDTO;
        }

        public List<TinhThanhDTO> GetAll()
        {
            var result = new List<TinhThanhDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public string GetMaxMaTinhThanh()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaTinhThanh);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaTinhThanh"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaTinhThanhBySoCmnd(string soCmnd)
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectMaTinhThanhTheoCmnd, soCmnd);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaTinhThanh"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public TinhThanhDTO GetByMaTinhThanh(string maTinhThanh)
        {
            var result = new TinhThanhDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectByMaTinhThanh, maTinhThanh);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(TinhThanhDTO ttNew)
        {
            var query = string.Format(StrInsert, ttNew.MaTinhThanh, ttNew.TenTinhThanh, ttNew.TenDayDu, ttNew.TenVietTat, ttNew.NgayHieuLuc.ToString("yyyy-MM-dd"), ttNew.TinhTrang, ttNew.Loai, ttNew.DauSoCMND);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(TinhThanhDTO ttUpdate)
        {
            var query = string.Format(StrUpdate, ttUpdate.TenTinhThanh, ttUpdate.TenDayDu, ttUpdate.TenVietTat, ttUpdate.Loai, ttUpdate.DauSoCMND, ttUpdate.MaTinhThanh);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(TinhThanhDTO ttDel)
        {
            var query = string.Format(StrDelete, ttDel.MaTinhThanh);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
