﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class HeDaoTaoDAC
    {
        const string StrSelectAll = "SELECT * FROM HeDaoTao HDT WHERE HDT.TinhTrang = 0";
        const string SelectByMaHeDaoTao = "SELECT * FROM HeDaoTao HDT WHERE HDT.TinhTrang = 0 AND MaHeDaoTao = {0}";
        const string StrInsert = "INSERT INTO HeDaoTao (MaHeDaoTao, TenHeDaoTao, TenVietTat, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE HeDaoTao SET TenHeDaoTao = N'{0}', TenVietTat = N'{1}', GhiChu = N'{2}' WHERE MaHeDaoTao = '{3}'";
        const string StrDelete = "UPDATE HeDaoTao SET TinhTrang = 1 WHERE MaHeDaoTao = '{0}'";
        const string StrMaxMaHeDaoTao = "SELECT MAX(MaHeDaoTao) MaHeDaoTao FROM HeDaoTao";

        SqlConnection _conn = new SqlConnection();

        private HeDaoTaoDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            HeDaoTaoDTO hDTO = Utils.MappingSQLDRToDTO<HeDaoTaoDTO>(sdr);
            if (hDTO.TenVietTat != null && hDTO.TenVietTat.CompareTo("NULL") == 0)
                hDTO.TenVietTat = string.Empty;

            if (hDTO.GhiChu != null && hDTO.GhiChu.CompareTo("NULL") == 0)
                hDTO.GhiChu = string.Empty;

            return hDTO;
        }

        public List<HeDaoTaoDTO> GetAll()
        {
            List<HeDaoTaoDTO> result = new List<HeDaoTaoDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public HeDaoTaoDTO GetByMaLoaiHeDaoTao(string maLoaiHeDaoTao)
        {
            var result = new HeDaoTaoDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaHeDaoTao, maLoaiHeDaoTao);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaHeDaoTao()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaHeDaoTao);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaHeDaoTao"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(HeDaoTaoDTO hNew)
        {
            var query = string.Format(StrInsert, hNew.MaHeDaoTao, hNew.TenHeDaoTao, hNew.TenVietTat, hNew.GhiChu, hNew.NgayHieuLuc.ToString("yyyy-MM-dd"), hNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(HeDaoTaoDTO hUpdate)
        {
            var query = string.Format(StrUpdate, hUpdate.TenHeDaoTao, hUpdate.TenVietTat, hUpdate.GhiChu, hUpdate.MaHeDaoTao);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(HeDaoTaoDTO hDel)
        {
            var query = string.Format(StrDelete, hDel.MaHeDaoTao);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
