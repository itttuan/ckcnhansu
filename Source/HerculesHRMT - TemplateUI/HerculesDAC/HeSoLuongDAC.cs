﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class HeSoLuongDAC
    {
        SqlConnection _conn = new SqlConnection();

        public List<HeSoLuongDTO> LayDanhSachTatCaHeSoLuong()
        {
            string query = "Select * from HeSoLuong Where TinhTrang = 0";
            List<HeSoLuongDTO> listRes = new List<HeSoLuongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO4<HeSoLuongDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public List<HeSoLuongDTO> LayDanhSachHeSoLuongTheoNgach(string strMaNgach)
        {
            string query = "Select * from HeSoLuong Where TinhTrang = 0 and MaNgach = '" + strMaNgach + "'" ;
            List<HeSoLuongDTO> listRes = new List<HeSoLuongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO4<HeSoLuongDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
        
    }
}
