﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class HinhThucDaoTaoDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<HinhThucDaoTaoDTO> LayDanhSachHinhThucDaoTao()
        {
            List<HinhThucDaoTaoDTO> listRes = new List<HinhThucDaoTaoDTO>();
            string query = "select * from HinhThucDaoTao";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<HinhThucDaoTaoDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public HinhThucDaoTaoDTO TimHinhThucDaoTaoTheoMa(string id)
        {
            HinhThucDaoTaoDTO listRes = new HinhThucDaoTaoDTO();
            string query = "select * from HinhThucDaoTao where Id = " + id;
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<HinhThucDaoTaoDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
