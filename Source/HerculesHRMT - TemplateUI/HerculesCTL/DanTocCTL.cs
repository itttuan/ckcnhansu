﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class DanTocCTL
    {
        readonly DanTocDAC _dantocDac = new DanTocDAC();

        public List<DanTocDTO> GetAll()
        {
            return _dantocDac.GetAll();
        }

        public DanTocDTO GetByMaDanToc(string maDanToc)
        {
            return !string.IsNullOrEmpty(maDanToc) ? _dantocDac.GetByMaDanToc(maDanToc) : new DanTocDTO();
        }

        public string GetMaxMaDanToc()
        {
            return _dantocDac.GetMaxMaDanToc();
        }

        public bool Save(DanTocDTO dtNew)
        {
            return _dantocDac.Save(dtNew);
        }

        public bool Update(DanTocDTO dtUpdate)
        {
            return _dantocDac.Update(dtUpdate);
        }

        public bool Delete(DanTocDTO dtDel)
        {
            return _dantocDac.Delete(dtDel);
        }
    }
}
