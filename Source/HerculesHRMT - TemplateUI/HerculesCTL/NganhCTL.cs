﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class NganhCTL
    {
        readonly NganhDAC _nganhDac = new NganhDAC();

        public List<NganhDTO> GetAll()
        {
            return _nganhDac.GetAll();
        }

        public NganhDTO GetByMaNganh(string maNganh)
        {
            return _nganhDac.GetByMaNganh(maNganh);
        }

        public string GetMaxMaNganh()
        {
            return _nganhDac.GetMaxMaNganh();
        }

        public bool Save(NganhDTO nNew)
        {
            return _nganhDac.Save(nNew);
        }

        public bool Update(NganhDTO nUpdate)
        {
            return _nganhDac.Update(nUpdate);
        }

        public bool Delete(NganhDTO nDel)
        {
            return _nganhDac.Delete(nDel);
        }
    }
}
