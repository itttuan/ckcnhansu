﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiBCapCChiCTL
    {
        readonly LoaiBCapCChiDAC _loaiBCapCChiDac = new LoaiBCapCChiDAC();

        public List<LoaiBCapCChiDTO> GetAll()
        {
            return _loaiBCapCChiDac.GetAll();
        }

        public LoaiBCapCChiDTO GetByLoaiBCapCChi(string maLoaiBCapCChi)
        {
            return _loaiBCapCChiDac.GetByMaLoaiBCapCChi(maLoaiBCapCChi);
        }

        public string GetMaxMaLoaiBCCC()
        {
            return _loaiBCapCChiDac.GetMaxMaLoaiBCCC();
        }

        public bool Save(LoaiBCapCChiDTO bcccNew)
        {
            return _loaiBCapCChiDac.Save(bcccNew);
        }

        public bool Update(LoaiBCapCChiDTO bcccUpdate)
        {
            return _loaiBCapCChiDac.Update(bcccUpdate);
        }

        public bool Delete(LoaiBCapCChiDTO bcccDel)
        {
            return _loaiBCapCChiDac.Delete(bcccDel);
        }
    }
}
