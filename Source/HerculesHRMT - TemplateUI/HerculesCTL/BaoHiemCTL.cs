﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class BaoHiemCTL
    {
        BaoHiemDAC baohiemDAC = new BaoHiemDAC();

        public List<BaoHiemDTO> DanhSachBHXHNhanVien(string maNV)
        {
            return baohiemDAC.LayDanhSachBHXHTheoNhanVien(maNV);
        }

        public List<BaoHiemDTO> DanhSachBHYTNhanVien(string maNV)
        {
            return baohiemDAC.LayDanhSachBHYTTheoNhanVien(maNV);
        }

        public bool LuuBaoHiem(BaoHiemDTO baohiem)
        {
            return baohiemDAC.Save(baohiem);
        }
        public bool CapNhatBaoHiem(BaoHiemDTO baohiem)
        {
            return baohiemDAC.Update(baohiem);
        }

        public bool XoaBaoHiem(string id)
        {
            return baohiemDAC.Delete(id);
        }
    }
}
