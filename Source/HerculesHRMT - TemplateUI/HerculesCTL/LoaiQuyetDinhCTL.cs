﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiQuyetDinhCTL
    {
        LoaiQuyetDinhDAC _loaiquyetdinhDAC = new LoaiQuyetDinhDAC();

        public List<LoaiQuyetDinhDTO> GetAll()
        {
            return _loaiquyetdinhDAC.GetAll();
        }


        public string GetMaxMaLoaiQuyetDinh()
        {
            return _loaiquyetdinhDAC.GetMaxMaLoaiQuyetDinh();
        }

        public bool Save(LoaiQuyetDinhDTO qdNew)
        {
            return _loaiquyetdinhDAC.Save(qdNew);
        }

        public bool Update(LoaiQuyetDinhDTO qdUpdate)
        {
            return _loaiquyetdinhDAC.Update(qdUpdate);
        }

        public bool Delete(LoaiQuyetDinhDTO qdDel)
        {
            return _loaiquyetdinhDAC.Delete(qdDel);
        }
    }
}
