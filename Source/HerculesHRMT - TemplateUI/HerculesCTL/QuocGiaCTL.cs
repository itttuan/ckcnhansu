﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using HerculesDAC;

namespace HerculesCTL
{
    public class QuocGiaCTL
    {
        QuocGiaDAC qgDAC = new QuocGiaDAC();

        public List<QuocGiaDTO> LayDanhSachQuocGia()
        {
            return qgDAC.LayDanhSachQuocGia();
        }

        public QuocGiaDTO TimQuocGiaTheoMa(string maQG)
        {
            return qgDAC.TimQuocGiaTheoMa(maQG);
        }
    }
}
