﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using HerculesDAC;

namespace HerculesCTL
{
    public class HinhThucDaoTaoCTL
    {
        HinhThucDaoTaoDAC htdtDTO = new HinhThucDaoTaoDAC();

        public List<HinhThucDaoTaoDTO> LayDanhSachHinhThucDaoTao()
        {
            return htdtDTO.LayDanhSachHinhThucDaoTao();
        }

        public HinhThucDaoTaoDTO TimHinhThucDaoTao(string id)
        {
            return htdtDTO.TimHinhThucDaoTaoTheoMa(id);
        }
    }
}
