﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TrinhDoCTL
    {
        TrinhDoDAC trinhdoDAC = new TrinhDoDAC();
        HinhThucDaoTaoDAC htdt = new HinhThucDaoTaoDAC();
        LoaiBangChuyenMonDAC lbcm = new LoaiBangChuyenMonDAC();
        QuocGiaDAC quocgia = new QuocGiaDAC();
        LoaiBangNgoaiNguDAC lbnn = new LoaiBangNgoaiNguDAC();
        LoaiBangTinHocDAC lbth = new LoaiBangTinHocDAC();
        LoaiBangLyLuanChinhTriDAC llct = new LoaiBangLyLuanChinhTriDAC();
        TruongDAC truong = new TruongDAC();
        public List<TrinhDoDTO> LayDanhSachTrinhDo(string maNV, int loaiTD)
        {
            List<TrinhDoDTO> result = new List<TrinhDoDTO>();
            List<TrinhDoDTO> lstTrinhDo = trinhdoDAC.LayDanhSachTrinhDo(maNV, loaiTD);
            foreach (TrinhDoDTO trinhdo in lstTrinhDo)
            {
                TrinhDoDTO tdObj = trinhdo;
                tdObj.HinhThucDaoTaoOBJ = htdt.TimHinhThucDaoTaoTheoMa(string.IsNullOrEmpty(trinhdo.HinhThucDaoTao)?"-1":trinhdo.HinhThucDaoTao);
                tdObj.LoaiBangChuyenMon = lbcm.TimLoaiBangChuyenMon(string.IsNullOrEmpty(trinhdo.LoaiTrinhDoChuyenMon) ? "-1" : trinhdo.LoaiTrinhDoChuyenMon);
                tdObj.NgoaiNgu = quocgia.TimQuocGiaTheoMa(string.IsNullOrEmpty(trinhdo.MaNgoaiNgu) ? "-1" : trinhdo.MaNgoaiNgu);
                tdObj.LoaiBangNgoaiNgu = lbnn.TimLoaiBangNgoaiNgu(string.IsNullOrEmpty(trinhdo.LoaiTenBangCapNgoaiNgu) ? "-1" : trinhdo.LoaiTenBangCapNgoaiNgu);
                tdObj.LoaiBangTinHoc = lbth.TimLoaiBangTinHoc(string.IsNullOrEmpty(trinhdo.LoaiTrinhDoTinHoc) ? "-1" : trinhdo.LoaiTrinhDoTinHoc);
                tdObj.LoaiBangLyLuanChinhTri = llct.TimLoaiBangLLChinhTri(string.IsNullOrEmpty(trinhdo.LoaiLyLuanChinhTri) ? "-1" : trinhdo.LoaiLyLuanChinhTri);
                tdObj.Truong = truong.GetByMaTruong(string.IsNullOrEmpty(trinhdo.MaTruong) ? "-1" : trinhdo.MaTruong);
                result.Add(tdObj);
            }
            return result;
        }

        public bool CapNhatTrinhDo(TrinhDoDTO trinhdo)
        {
            return trinhdoDAC.Update(trinhdo);
        }

        public bool XoaTrinhDo(TrinhDoDTO trinhdo)
        {
            return trinhdoDAC.Delete(trinhdo);
        }

        public bool LuuTrinhDo(TrinhDoDTO trinhdo)
        {
            return trinhdoDAC.Save(trinhdo);
        }
    }
}
