﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using HerculesDAC;

namespace HerculesCTL
{
    public class QuaTrinhCongTacCTL
    {
        QuaTrinhCongTacDAC _qtctDAC = new QuaTrinhCongTacDAC();

        public List<QuaTrinhCongTacDTO> LayDanhSachQuaTrinhCongTacNhanVien(string maNV)
        {
            return _qtctDAC.LayDanhSachQuaTrinhCongTacCuaNhanVien(maNV);
        }

        public List<QuaTrinhCongTacDTO> LayDanhSachToChucXaHoiNhanVien(string maNV)
        {
            return _qtctDAC.LayDanhSachToChucXaHoiNhanVien(maNV);
        }
        public bool LuuQuaTrinhCongTac(QuaTrinhCongTacDTO qtctDTO)
        {
            return _qtctDAC.Save(qtctDTO);
        }

        public bool CapNhatQuaTrinhCongTac(QuaTrinhCongTacDTO qtctDTO)
        {
            return _qtctDAC.Update(qtctDTO);
        }

        public bool XoaQuaTrinhCongTac(string id)
        {
            return _qtctDAC.Delete(id);
        }
    }
}
