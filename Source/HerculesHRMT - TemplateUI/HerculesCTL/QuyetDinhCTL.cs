﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class QuyetDinhCTL
    {
        QuyetDinhDAC quyetDinhDAC = new QuyetDinhDAC();

        public List<QuyetDinhDTO> DanhSachQuyetDinhNhanVien(string maNV)
        {
            return quyetDinhDAC.LayDanhSachQuyetDinhTheoNhanVien(maNV);
        }

    }
}
