﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiTrinhDoCTL
    {
        readonly LoaiTrinhDoDAC _loaiTrinhDoDac = new LoaiTrinhDoDAC();

        public List<LoaiTrinhDoDTO> GetAll()
        {
            return _loaiTrinhDoDac.GetAll();
        }

        public LoaiTrinhDoDTO GetByMaLoaiTrinhDo(string maLoaiTrinhDo)
        {
            return _loaiTrinhDoDac.GetByMaLoaiTrinhDo(maLoaiTrinhDo);
        }

        public string GetMaxMaLoaiTrinhDo()
        {
            return _loaiTrinhDoDac.GetMaxMaLoaiTrinhDo();
        }

        public bool Save(LoaiTrinhDoDTO tdNew)
        {
            return _loaiTrinhDoDac.Save(tdNew);
        }

        public bool Update(LoaiTrinhDoDTO tdUpdate)
        {
            return _loaiTrinhDoDac.Update(tdUpdate);
        }

        public bool Delete(LoaiTrinhDoDTO tdDel)
        {
            return _loaiTrinhDoDac.Delete(tdDel);
        }
    }
}
