﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class NhanVienCTL
    {
        private readonly NhanVienDAC _nhanvienDAC = new NhanVienDAC();

        public bool LuuNhanVien(NhanVienDTO nhanvien)
        {
            //return _nhanvienDAC.Save(nhanvien);
            var id = _nhanvienDAC.LuuNhanVien(nhanvien);

            return id > 0;
        }

        public bool SaveStaff(StaffDto staff)
        {
            return _nhanvienDAC.SaveStaff(staff);
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVien()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVien();
        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoPhongKhoa(string strMaPK)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoPhongKhoa(strMaPK);
        }
        public List<NhanVienDTO> LayDanhSachNhanVienTheoBoMon(string strMaBM)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoBoMon(strMaBM);
        }
        public List<NhanVienDTO> TimNhanVienTheoCMND(string strCMND)
        {
            return _nhanvienDAC.TimNhanVienTheoCMND(strCMND);
        }
        public List<NhanVienDTO> TimNhanVienTheoMaNV(string strMaNV)
        {
            return _nhanvienDAC.TimNhanVienTheoMaNV(strMaNV);
        }

        public NhanVienDTO TimNhanVienTheoId(string id)
        {
            return _nhanvienDAC.TimNhanVienTheoId(id);
        }

        public int LuuPhienBanMoi(NhanVienDTO obj)
        {
            return _nhanvienDAC.LuuNhanVien(obj);
        }

        public void UpdatePhienBanCu(int id)
        {
            _nhanvienDAC.CapNhatTrangThaiPhienBanCu(id);
        }

        public List<NhanVienDTO> LayCacPhienBanNhanVien(string maNV)
        {
            return _nhanvienDAC.LayTatCaCacPhienBanCapNhat(maNV);
        }

        public bool CapNhatTuNhanXet(string tunhanxet, string id)
        {
            return _nhanvienDAC.UpdateTuNhanXet(tunhanxet,id);
        }

        public bool CapNhatBHXHTKNganhang(NhanVienDTO nhanvienDTO)
        {
            return _nhanvienDAC.UpdateBaoHiemTaiKhoanNganHang(nhanvienDTO);
        }

        public string GetMaNvMax(string maBoMon)
        {
            return _nhanvienDAC.LayMaNVMax(maBoMon);
        }
    }
}
