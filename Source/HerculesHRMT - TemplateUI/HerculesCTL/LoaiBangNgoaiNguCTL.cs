﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using HerculesDAC;

namespace HerculesCTL
{
    public class LoaiBangNgoaiNguCTL
    {
        LoaiBangNgoaiNguDAC ngoainguDAC = new LoaiBangNgoaiNguDAC();

        public List<LoaiBangNgoaiNguDTO> LayDanhSachLoaiBangNgoaiNgu()
        {
            return ngoainguDAC.LayDanhSachLoaiBangNgoaiNgu();
        }

        public LoaiBangNgoaiNguDTO TimLoaiBangNgoaiNgu(string id)
        {
            return ngoainguDAC.TimLoaiBangNgoaiNgu(id);
        }

        public List<LoaiBangNgoaiNguDTO> LayDanhSachLoaiBangNNTheoMaNN(string strMaNN)
        {
            return ngoainguDAC.LayDanhSachLoaiBangNNTheoMaNN(strMaNN);
        }
    }
}
