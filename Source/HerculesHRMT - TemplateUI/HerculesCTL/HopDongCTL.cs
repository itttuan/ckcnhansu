﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class HopDongCTL
    {
        HopDongDAC hodongDAC = new HopDongDAC();

        public List<HopDongDTO> DanhSachHopDongNhanVien(string maNV)
        {
            return hodongDAC.LayDanhSachHopDongTheoNhanVien(maNV);
        }
    }
}
