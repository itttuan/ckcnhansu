﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TrangThaiLamViecCTL
    {
        TrangThaiLamViecDAC _trangThaiLamViecDac = new TrangThaiLamViecDAC();

        public List<TrangThaiLamViecDTO> GetAll()
        {
            return _trangThaiLamViecDac.GetAll();
        }

        public TrangThaiLamViecDTO GetByMaTrangThai(string maTrangThai)
        {
            return !string.IsNullOrEmpty(maTrangThai) ? _trangThaiLamViecDac.GetByMaTrangThai(maTrangThai) : new TrangThaiLamViecDTO();
        }

        public string GetMaxMaTrangThai()
        {
            return _trangThaiLamViecDac.GetMaxMaTrangThai();
        }

        public bool Save(TrangThaiLamViecDTO ttNew)
        {
            return _trangThaiLamViecDac.Save(ttNew);
        }

        public bool Update(TrangThaiLamViecDTO ttUpdate)
        {
            return _trangThaiLamViecDac.Update(ttUpdate);
        }

        public bool Delete(TrangThaiLamViecDTO ttDel)
        {
            return _trangThaiLamViecDac.Delete(ttDel);
        }
    }
}
