﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class QuanHuyenCTL
    {
        private readonly QuanHuyenDAC _quanHuyenDac = new QuanHuyenDAC();

        public List<QuanHuyenDTO> GetAll()
        {
            return _quanHuyenDac.GetAll();
        }

        public List<QuanHuyenDTO> GetListByMaTinhThanh(string maTinhThanh)
        {
            return _quanHuyenDac.GetListByMaTinhThanh(maTinhThanh);
        }

        public QuanHuyenDTO GetByMaQuanHuyen(string maQuanHuyen)
        {
            return !string.IsNullOrEmpty(maQuanHuyen) ? _quanHuyenDac.GetByMaQuanHuyen(maQuanHuyen) : new QuanHuyenDTO();
        }

        public string GetMaxMaQuanHuyen()
        {
            return _quanHuyenDac.GetMaxMaQuanHuyen();
        }

        public bool Save(QuanHuyenDTO qhNew)
        {
            return _quanHuyenDac.Save(qhNew);
        }

        public bool Update(QuanHuyenDTO qhUpdate)
        {
            return _quanHuyenDac.Update(qhUpdate);
        }

        public bool Delete(QuanHuyenDTO qhDel)
        {
            return _quanHuyenDac.Delete(qhDel);
        }
    }
}
