﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class KhenThuongKyLuatCTL
    {
        KhenThuongKyLuatDAC khenthuongkyluatDAC = new KhenThuongKyLuatDAC();

        public List<KhenThuongKyLuatDto> LayDanhSachKhenThuongNhanVien(string strMaNV)
        {
            return khenthuongkyluatDAC.LayDanhSachKhenThuongNhanVien(strMaNV);
        }

        public List<KhenThuongKyLuatDto> LayDanhSachKyLuatNhanVien(string strMaNV)
        {
            return khenthuongkyluatDAC.LayDanhSachKyLuatNhanVien(strMaNV);
        }

        public bool LuuMoiKhenThuongKyLuat(KhenThuongKyLuatDto ktkl)
        {
            return khenthuongkyluatDAC.Save(ktkl);
        }

        public bool CapNhapKhenThuongKyLuat(KhenThuongKyLuatDto ktkl)
        {
            return khenthuongkyluatDAC.Update(ktkl);
        }

        public bool XoaKhenThuongKyLuat(KhenThuongKyLuatDto ktkl)
        {
            return khenthuongkyluatDAC.Delete(ktkl);
        }
    }
}
