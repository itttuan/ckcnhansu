﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LuongCTL
    {
        LuongDAC luongDAC = new LuongDAC();

        public List<LuongDTO> LichSuLuongTheoNhanVien(string maNV)
        {
            return luongDAC.LayLichSuLuongTheoNhanVien(maNV);
        }

        public bool LuuMoiLuong(LuongDTO luong)
        {
            return luongDAC.Save(luong);
        }
        public bool CapNhatLuong(LuongDTO luong)
        {
            return luongDAC.Update(luong);
        }

        public bool XoaLuong(string id)
        {
            return luongDAC.Delete(id);
        }
    }
}
