﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class ThiDuaCTL
    {
        ThiDuaDAC thiduaDAC = new ThiDuaDAC();

        public List<ThiDuaDTO> DanhSachThiDuaThangTheoNhanVien(string maNV)
        {
            return thiduaDAC.LayDanhSachThiDuaThangTheoNV(maNV);
        }

        public List<ThiDuaDTO> DanhSachThiDuahocKyTheoNhanVien(string maNV)
        {
            return thiduaDAC.LayDanhSachThiDuaHocKyTheoNV(maNV);
        }

        public List<ThiDuaDTO> DanhSachThiDuaNamHocTheoNhanVien(string maNV)
        {
            return thiduaDAC.LayDanhSachThiDuaNamHocTheoNV(maNV);
        }
    }
}
