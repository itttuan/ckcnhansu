﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiHopDongCTL
    {
        LoaiHopDongDAC _loaihopdongDAC = new LoaiHopDongDAC();

        public List<LoaiHopDongDTO> GetAll()
        {
            return _loaihopdongDAC.GetAll();
        }


        public string GetMaxMaLoaiHopDong()
        {
            return _loaihopdongDAC.GetMaxMaLoaiHopDong();
        }

        public bool Save(LoaiHopDongDTO hdNew)
        {
            return _loaihopdongDAC.Save(hdNew);
        }

        public bool Update(LoaiHopDongDTO hdUpdate)
        {
            return _loaihopdongDAC.Update(hdUpdate);
        }

        public bool Delete(LoaiHopDongDTO hdDel)
        {
            return _loaihopdongDAC.Delete(hdDel);
        }
    }
}
