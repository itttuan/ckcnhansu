﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class HeSoLuongCTL
    {
        HeSoLuongDAC hesoluongDAC = new HeSoLuongDAC();

        public List<HeSoLuongDTO> LayDanhSachTatCaHeSoLuong()
        {
            return hesoluongDAC.LayDanhSachTatCaHeSoLuong();
        }

        public List<HeSoLuongDTO> LayDanhSachHeSOLuongTheoNgach(string mangach)
        {
            return hesoluongDAC.LayDanhSachHeSoLuongTheoNgach(mangach);
        }
    }
}
