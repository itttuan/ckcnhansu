﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TruongCTL
    {
        readonly TruongDAC _truongDac = new TruongDAC();

        public List<TruongDTO> GetAll()
        {
            return _truongDac.GetAll();
        }

        public TruongDTO GetByMaTruong(string maTruong)
        {
            return _truongDac.GetByMaTruong(maTruong);
        }

        public string GetMaxMaTruong()
        {
            return _truongDac.GetMaxMaTruong();
        }

        public bool Save(TruongDTO tNew)
        {
            return _truongDac.Save(tNew);
        }

        public bool Update(TruongDTO tUpdate)
        {
            return _truongDac.Update(tUpdate);
        }

        public bool Delete(TruongDTO tDel)
        {
            return _truongDac.Delete(tDel);
        }
    }
}
