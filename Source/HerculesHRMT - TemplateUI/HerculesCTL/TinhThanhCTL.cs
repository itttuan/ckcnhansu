﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TinhThanhCTL
    {
        readonly TinhThanhDAC _tinhthanhDac = new TinhThanhDAC();

        public List<TinhThanhDTO> GetAll()
        {
            return _tinhthanhDac.GetAll();
        }

        public string GetMaTinhThanhBySoCmnd(string soCmnd)
        {
            if (!string.IsNullOrEmpty(soCmnd) && soCmnd.Length >= 2)
            {
                return _tinhthanhDac.GetMaTinhThanhBySoCmnd(soCmnd.Substring(0, 2));
            }

            return string.Empty;
        }

        public string GetMaxMaTinhThanh()
        {
            return _tinhthanhDac.GetMaxMaTinhThanh();
        }

        public TinhThanhDTO GetByMaTinhThanh(string maTinhThanh)
        {
            return !string.IsNullOrEmpty(maTinhThanh) ? _tinhthanhDac.GetByMaTinhThanh(maTinhThanh) : new TinhThanhDTO();
        }

        public bool Save(TinhThanhDTO ttNew)
        {
            return _tinhthanhDac.Save(ttNew);
        }

        public bool Update(TinhThanhDTO ttUpdate)
        {
            return _tinhthanhDac.Update(ttUpdate);
        }

        public bool Delete(TinhThanhDTO ttDel)
        {
            return _tinhthanhDac.Delete(ttDel);
        }
    }
}
