﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class HinhAnhCTL
    {
        HinhAnhDAC hinhanhDAC = new HinhAnhDAC();

        public List<byte[]> LayTatCaHinhTheoNhanVien(string strMaNV)
        {
            return hinhanhDAC.layHinh(strMaNV);
        }
        public bool InsertImage(HinhAnhDTO hinhanhDTO)
        {
            return hinhanhDAC.UploadImage(hinhanhDTO);
        }
    }

}
