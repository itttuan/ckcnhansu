﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class PhongKhoaCTL
    {
        private readonly PhongKhoaDAC _phongKhoaDac = new PhongKhoaDAC();

        public List<PhongKhoaDTO> GetAll()
        {
            return _phongKhoaDac.GetAll();
        }

        public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        {
            return !string.IsNullOrEmpty(maPhongKhoa) ? _phongKhoaDac.GetByMaPhongKhoa(maPhongKhoa) : new PhongKhoaDTO();
        }

        public string GetMaxMaPhongKhoa()
        {
            return _phongKhoaDac.GetMaxMaPhongKhoa();
        }

        public bool Save(PhongKhoaDTO pkNew)
        {
            return _phongKhoaDac.Save(pkNew);
        }

        public bool Update(PhongKhoaDTO pkUpdate)
        {
            return _phongKhoaDac.Update(pkUpdate);
        }

        public bool Delete(PhongKhoaDTO pkDel)
        {
            return _phongKhoaDac.Delete(pkDel);
        }
    }
}
