﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LuongDAC
    {
        const string StrSelectByNhanVien = "select * from Luong where MaNV = '{0}' and TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<LuongDTO> LayLichSuLuongTheoNhanVien(string strMaNV)
        {
            List<LuongDTO> listResult = new List<LuongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO4<LuongDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public bool Save(LuongDTO luongDTO)
        {
            string query = "INSERT INTO Luong([MaNV], [LuongCB], [LuongChinh], [PCChucVu], [PCThamNien],"
                + " [TyLeVuotKhung], [LuongBHXH], [MaNgach], [BacLuong], [HeSoLuong], [HeSoNamDau], [VuotKhung], [PhuCap],"
                + " [NgayHuong], [SoQuyetDinh], [NgayHieuLuc], [TinhTrang]) VALUES('" + luongDTO.MaNV + "',"
                + (Double.IsNaN(luongDTO.LuongCB) ? "null," : luongDTO.LuongCB + ",")
                + (Double.IsNaN(luongDTO.LuongChinh) ? "null," : luongDTO.LuongChinh + ",")
                + (Double.IsNaN(luongDTO.PCChucVu) ? "null," : luongDTO.PCChucVu + ",")
                + (Double.IsNaN(luongDTO.PCThamNien) ? "null," : luongDTO.PCThamNien + ",")
                + (Double.IsNaN(luongDTO.TiLeVuotKhung) ? "null," : luongDTO.TiLeVuotKhung + ",")
                + (Double.IsNaN(luongDTO.LuongBHXH) ? "null," : luongDTO.LuongBHXH + ",")
                + (String.IsNullOrEmpty(luongDTO.MaNgach) ? "null," : "N'" + luongDTO.MaNgach + "',")
                + (String.IsNullOrEmpty(luongDTO.BacLuong) ? "null," : luongDTO.BacLuong + ",")
                + (Double.IsNaN(luongDTO.HeSoLuong) ? "null," : luongDTO.HeSoLuong + ",")
                + (String.IsNullOrEmpty(luongDTO.HeSoNamDau) ? "0," : luongDTO.HeSoNamDau + ",")
                + (String.IsNullOrEmpty(luongDTO.VuotKhung) ? "0," : luongDTO.VuotKhung + ",")
                + (Double.IsNaN(luongDTO.PhuCap) ? "null," : luongDTO.PhuCap + ",")
                + (String.IsNullOrEmpty(luongDTO.NgayHuong) ? "null," : "'" + luongDTO.NgayHuong + "',")
                + (String.IsNullOrEmpty(luongDTO.SoQuyetDinh) ? "null," : "N'" + luongDTO.SoQuyetDinh + "',")
                + "getDate(),0)";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true; 
            }
            catch
            {
            }
            return false;
        }

        public bool Update(LuongDTO luongDTO)
        {
            string query = "UPDATE Luong SET LuongCB = "
                + (Double.IsNaN(luongDTO.LuongCB) ? "null," : luongDTO.LuongCB + ",") + "LuongChinh = "
                + (Double.IsNaN(luongDTO.LuongChinh) ? "null," : luongDTO.LuongChinh + ",") + " PCChucVu = "
                + (Double.IsNaN(luongDTO.PCChucVu) ? "null," : luongDTO.PCChucVu + ",") + " PCThamNien = "
                + (Double.IsNaN(luongDTO.PCThamNien) ? "null," : luongDTO.PCThamNien + ",") + " TyLeVuotKhung = "
                + (Double.IsNaN(luongDTO.TiLeVuotKhung) ? "null," : luongDTO.TiLeVuotKhung + ",") + " LuongBHXH = "
                + (Double.IsNaN(luongDTO.LuongBHXH) ? "null," : luongDTO.LuongBHXH + ",") + " MaNgach = "
                + (String.IsNullOrEmpty(luongDTO.MaNgach) ? "null," : "N'" + luongDTO.MaNgach + "',") + " BacLuong = "
                + (String.IsNullOrEmpty(luongDTO.BacLuong) ? "null," : luongDTO.BacLuong + ",") + " HeSoLuong = "
                + (Double.IsNaN(luongDTO.HeSoLuong) ? "null," : luongDTO.HeSoLuong + ",") + " HeSoNamDau = "
                + (String.IsNullOrEmpty(luongDTO.HeSoNamDau) ? "0," : luongDTO.HeSoNamDau + ",") + " VuotKhung = "
                + (String.IsNullOrEmpty(luongDTO.VuotKhung) ? "0," : luongDTO.VuotKhung + ",") + " PhuCap = "
                + (Double.IsNaN(luongDTO.PhuCap) ? "null," : luongDTO.PhuCap + ",") + " NgayHuong = "
                + (String.IsNullOrEmpty(luongDTO.NgayHuong) ? "null," : "'" + luongDTO.NgayHuong + "',") + " SoQuyetDinh = "
                + (String.IsNullOrEmpty(luongDTO.SoQuyetDinh) ? "null " : "N'" + luongDTO.SoQuyetDinh + "' ")
                + " WHERE Id = " + luongDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(string id)
        {
            string query = "UPDATE Luong SET TinhTrang = 1 WHERE Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
