﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class TonGiaoDAC
    {
        const string StrSelectAll = "SELECT * FROM TonGiao TG WHERE TG.TinhTrang = 0";
        const string SelectByMaTonGiao = "SELECT * FROM TonGiao TG WHERE TG.TinhTrang = 0 AND MaTonGiao = \'{0}\'";
        const string StrInsert = "INSERT INTO TonGiao (MaTonGiao, TenTonGiao, TenVietTat, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE TonGiao SET TenTonGiao = N'{0}', TenVietTat = N'{1}', GhiChu = N'{2}' WHERE MaTonGiao = '{3}'";
        const string StrDelete = "UPDATE TonGiao SET TinhTrang = 1 WHERE MaTonGiao = '{0}'";
        const string StrMaxMaTonGiao = "SELECT MAX(MaTonGiao) MaTonGiao FROM TonGiao";
        SqlConnection _conn = new SqlConnection();

        private TonGiaoDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            TonGiaoDTO ttDTO = Utils.MappingSQLDRToDTO<TonGiaoDTO>(sdr);
            if (ttDTO.TenVietTat.CompareTo("NULL") == 0)
                ttDTO.TenVietTat = string.Empty;

            if (ttDTO.GhiChu.CompareTo("NULL") == 0)
                ttDTO.GhiChu = string.Empty;

            return ttDTO;
        }

        public List<TonGiaoDTO> GetAll()
        {
            List<TonGiaoDTO> result = new List<TonGiaoDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();

            return result;
        }

        public TonGiaoDTO GetByMaTonGiao(string maTonGiao)
        {
            var result = new TonGiaoDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaTonGiao, maTonGiao);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = Utils.MappingSQLDRToDTO<TonGiaoDTO>(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaTonGiao()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaTonGiao);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaTonGiao"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(TonGiaoDTO tgNew)
        {
            var query = string.Format(StrInsert, tgNew.MaTonGiao, tgNew.TenTonGiao, tgNew.TenVietTat, tgNew.GhiChu, tgNew.NgayHieuLuc.ToString("yyyy-MM-dd"), tgNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(TonGiaoDTO tgUpdate)
        {
            var query = string.Format(StrUpdate, tgUpdate.TenTonGiao, tgUpdate.TenVietTat, tgUpdate.GhiChu, tgUpdate.MaTonGiao);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(TonGiaoDTO tgDel)
        {
            var query = string.Format(StrDelete, tgDel.MaTonGiao);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
