﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiHopDongDAC
    {
        const string StrSelectAll = "SELECT * FROM LoaiHopDong WHERE TinhTrang = 0";
        const string SelectByMaLoaiHopDong = "SELECT * FROM LoaiHopDong WHERE TinhTrang = 0 AND MaLoaiHD = \'{0}\'";
        const string StrInsert = "INSERT INTO LoaiHopDong (MaLoaiHD, TenLoaiHD, LaTamTuyen, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', '{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE LoaiHopDong SET TenLoaiHD = N'{0}', LaTamTuyen = '{1}', GhiChu = N'{2}' WHERE MaLoaiHD = '{3}'";
        const string StrDelete = "UPDATE LoaiHopDong SET TinhTrang = 1 WHERE MaTonGiao = '{0}'";
        const string StrMaxMaLoaiHopDong = "SELECT MAX(MaLoaiHD) MaLoaiHD FROM LoaiHopDong";
        SqlConnection _conn = new SqlConnection();

        private LoaiHopDongDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            LoaiHopDongDTO hdDTO = Utils.MappingSQLDRToDTO<LoaiHopDongDTO>(sdr);

            if (hdDTO.GhiChu != null  && hdDTO.GhiChu.CompareTo("NULL") == 0)
                hdDTO.GhiChu = string.Empty;

            return hdDTO;
        }

        public List<LoaiHopDongDTO> GetAll()
        {
            List<LoaiHopDongDTO> result = new List<LoaiHopDongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();

            return result;
        }

        public LoaiHopDongDTO GetByMaLoaiHopDong(string maHD)
        {
            var result = new LoaiHopDongDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaLoaiHopDong, maHD);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiHopDong()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoaiHopDong);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaLoaiHD"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(LoaiHopDongDTO hdNew)
        {
            var query = string.Format(StrInsert, hdNew.MaLoaiHD, hdNew.TenLoaiHD, hdNew.LaTamTuyen, hdNew.GhiChu, hdNew.NgayHieuLuc.ToString("yyyy-MM-dd"), hdNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(LoaiHopDongDTO hdUpdate)
        {
            var query = string.Format(StrUpdate, hdUpdate.TenLoaiHD, hdUpdate.LaTamTuyen, hdUpdate.GhiChu, hdUpdate.MaLoaiHD);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(LoaiHopDongDTO hdDel)
        {
            var query = string.Format(StrDelete, hdDel.MaLoaiHD);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
