﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class NgachDAC
    {
        SqlConnection _conn = new SqlConnection();

        public List<NgachDTO> LayDanhSachNgach()
        {
            string query = "Select * from Ngach where TinhTrang = 0";
            List<NgachDTO> listRes = new List<NgachDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query,_conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO4<NgachDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
