﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiBangTinHocDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<LoaiBangTinHocDTO> LayDanhSachLoaiBangTinHoc()
        {
            List<LoaiBangTinHocDTO> listRes = new List<LoaiBangTinHocDTO>();
            string query = "select * from LoaiBangTinHoc";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<LoaiBangTinHocDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public LoaiBangTinHocDTO TimLoaiBangTinHoc(string id)
        {
            LoaiBangTinHocDTO listRes = new LoaiBangTinHocDTO();
            string query = "select * from LoaiBangTinHoc where Id = " + id;
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<LoaiBangTinHocDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
