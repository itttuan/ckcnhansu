﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class TrangThaiLamViecDAC
    {
        const string SelectAll = "SELECT * FROM TrangThaiLamViec TTLV WHERE TTLV.TinhTrang = 0";
        const string SelectByMaTrangThai = "SELECT * FROM TrangThaiLamViec TTLV WHERE TTLV.MaTrangThai = \'{0}\'";
        const string StrInsert = "INSERT INTO TrangThaiLamViec (MaTrangThai, TenTrangThai, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', '{3}', '{4}')";
        const string StrUpdate = "UPDATE TrangThaiLamViec SET TenTrangThai = N'{0}', GhiChu = N'{1}' WHERE MaTrangThai = '{2}'";
        const string StrDelete = "UPDATE TrangThaiLamViec SET TinhTrang = 1 WHERE MaTrangThai = '{0}'";
        const string StrMaxMaTrangThai = "SELECT MAX(MaTrangThai) MaTrangThai FROM TrangThaiLamViec";
        const string SelectMaTenTrangThai = "SELECT MaTrangThai,TenTrangThai FROM TrangThaiLamViec TTLV WHERE TTLV.TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        private TrangThaiLamViecDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            //TrangThaiLamViecDTO ttDTO = Utils.MappingSQLDRToDTO<TrangThaiLamViecDTO>(sdr);
            //vdtoan upd
            TrangThaiLamViecDTO ttDTO = Utils.MappingSQLDRToDTO<TrangThaiLamViecDTO>(sdr);

            //if (ttDTO.GhiChu != null && ttDTO.GhiChu.CompareTo("NULL") == 0)
            //    ttDTO.GhiChu = string.Empty;
 
            if (ttDTO.GhiChu.CompareTo("NULL") == 0)
                ttDTO.GhiChu = string.Empty;

            return ttDTO;
        }

        public List<TrangThaiLamViecDTO> GetAll()
        {
            var result = new List<TrangThaiLamViecDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(SelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
                
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public TrangThaiLamViecDTO GetByMaTrangThai(string maTrangThai)
        {
            var result = new TrangThaiLamViecDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaTrangThai, maTrangThai);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        //public int GetMaxMaTrangThai()
        //{
        //    int result = -1;

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrMaxMaTrangThai);

        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    if (sdr.Read())
        //    {
        //        //result = sdr.GetInt32(0);
        //        result = int.Parse(sdr["MaTrangThai"].ToString());
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        public string GetMaxMaTrangThai()
        {
            var result = string.Empty; ;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaTrangThai);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                //result = sdr.GetInt32(0);
                result = sdr["MaTrangThai"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(TrangThaiLamViecDTO ttNew)
        {
            var query = string.Format(StrInsert, ttNew.MaTrangThai, ttNew.TenTrangThai, ttNew.GhiChu, ttNew.NgayHieuLuc.ToString("yyyy-MM-dd"), ttNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(TrangThaiLamViecDTO ttUpdate)
        {
            var query = string.Format(StrUpdate, ttUpdate.TenTrangThai, ttUpdate.GhiChu, ttUpdate.MaTrangThai);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(TrangThaiLamViecDTO ttDel)
        {
            var query = string.Format(StrDelete, ttDel.MaTrangThai);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public List<TrangThaiLamViec2DTO> GetMaTenTrangThai()
        {
            var result = new List<TrangThaiLamViec2DTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(SelectMaTenTrangThai, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO2(sdr));

            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        private TrangThaiLamViec2DTO MappingSQLDRToDTO2(SqlDataReader sdr)
        {
    
            TrangThaiLamViec2DTO ttDTO = Utils.MappingSQLDRToDTO<TrangThaiLamViec2DTO>(sdr);


            return ttDTO;
        }
    }
}
