﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuanHuyenDAC
    {
        const string StrSelectAll = "SELECT QH.*, TT.TENTINHTHANH FROM QuanHuyen QH, TINHTHANH TT WHERE QH.TinhTrang = 0 AND QH.MATINHTHANH = TT.MATINHTHANH";
        const string StrSelectTheoTinhThanh = "SELECT * FROM QuanHuyen QH WHERE QH.TinhTrang = 0 and QH.MaTinhThanh = '{0}'";
        const string StrSelectByMaQuanHuyen = "SELECT * FROM QuanHuyen QH WHERE QH.TinhTrang = 0 and QH.MaQuanHuyen = '{0}'";
        const string StrInsert = "INSERT INTO QuanHuyen (MaQuanHuyen, TenQuanHuyen, TenDayDu, TenVietTat, Loai, GhiChu, NgayHieuLuc, TinhTrang, MaTinhThanh) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', N'{5}', '{6}', '{7}', '{8}')";
        const string StrUpdate = "UPDATE QuanHuyen SET TenQuanHuyen = N'{0}', TenDayDu = N'{1}', TenVietTat = N'{2}', Loai = '{3}', GhiChu = '{4}', MaTinhThanh = '{5}' WHERE MaQuanHuyen = '{6}'";
        const string StrDelete = "UPDATE QuanHuyen SET TinhTrang = 1 WHERE MaQuanHuyen = '{0}'";
        const string StrMaxMaQuanHuyen = "SELECT MAX(MAQUANHUYEN) MAQUANHUYEN FROM QuanHuyen";
        SqlConnection _conn = new SqlConnection();

        private QuanHuyenDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            QuanHuyenDTO qhDTO = Utils.MappingSQLDRToDTO<QuanHuyenDTO>(sdr);

            switch (qhDTO.Loai)
            {
                case 0:
                    qhDTO.TenLoaiQuanHuyen = "Quận";
                    break;
                case 1:
                    qhDTO.TenLoaiQuanHuyen = "Huyện";
                    break;
                case 2:
                    qhDTO.TenLoaiQuanHuyen = "Thị xã/Thành phố";
                    break;
            }

            if (qhDTO.GhiChu != null && qhDTO.GhiChu.CompareTo("NULL") == 0)
                qhDTO.GhiChu = string.Empty;

            return qhDTO;
        }

        public List<QuanHuyenDTO> GetAll()
        {
            var result = new List<QuanHuyenDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();

            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            TinhThanhDAC ttDAC = new TinhThanhDAC();
            List<TinhThanhDTO> listTinhThanh = ttDAC.GetAll();

            while (sdr.Read())
            {                
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public List<QuanHuyenDTO> GetListByMaTinhThanh(string maTinhThanh)
        {
            var result = new List<QuanHuyenDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectTheoTinhThanh, maTinhThanh);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public QuanHuyenDTO GetByMaQuanHuyen(string maQuanHuyen)
        {
            var result = new QuanHuyenDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectByMaQuanHuyen, maQuanHuyen);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = Utils.MappingSQLDRToDTO<QuanHuyenDTO>(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaQuanHuyen()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaQuanHuyen);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaQuanHuyen"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(QuanHuyenDTO qhNew)
        {
            var query = string.Format(StrInsert, qhNew.MaQuanHuyen, qhNew.TenQuanHuyen, qhNew.TenDayDu, qhNew.TenVietTat, qhNew.Loai, qhNew.GhiChu, qhNew.NgayHieuLuc.ToString("yyyy-MM-dd"), qhNew.TinhTrang, qhNew.MaTinhThanh);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }        
        
        public bool Update(QuanHuyenDTO qhUpdate)
        {
            var query = string.Format(StrUpdate, qhUpdate.TenQuanHuyen, qhUpdate.TenDayDu, qhUpdate.TenVietTat, qhUpdate.Loai, qhUpdate.GhiChu, qhUpdate.MaTinhThanh, qhUpdate.MaQuanHuyen);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(QuanHuyenDTO qhDel)
        {
            var query = string.Format(StrDelete, qhDel.MaQuanHuyen);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
