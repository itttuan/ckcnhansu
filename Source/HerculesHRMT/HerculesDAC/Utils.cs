﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Reflection;

namespace HerculesDAC
{
    public static class Utils
    {
        public static T MappingSQLDRToDTO<T>(SqlDataReader sdr) where T : class
        {
            T obj = default(T);
            obj = Activator.CreateInstance<T>();
            Type classType = typeof(T);
            foreach(PropertyInfo proInfo in classType.GetProperties().ToList())
            {
                if (HasColumnByName(sdr,proInfo.Name))
                {
                    if (!object.Equals(sdr[proInfo.Name], DBNull.Value))
                    {
                        proInfo.SetValue(obj, sdr[proInfo.Name], null);
                    }
                }
            }
            return obj;
        }

        public static bool HasColumnByName(SqlDataReader sdr, string name)
        {

            try
            {
                return (sdr.GetOrdinal(name) >= 0);
            }
            catch
            {
                return false;
            }
        }

        public static bool HasProperty<T>(string name) where T : class
        {
            Type classType = typeof(T);
            return classType.GetProperty(name) != null;
        }
        public static T MappingSQLDRToDTO2<T>(SqlDataReader sdr) where T : class
        {
            T obj = default(T);
            obj = Activator.CreateInstance<T>();
            Type classType = typeof(T);
            for (int i = 0; i < sdr.FieldCount; i++)
            {
                if (HasProperty<T>(sdr.GetName(i)))
                {
                    if (!sdr.IsDBNull(i))
                    {
                        if (sdr[i].GetType() == typeof(DateTime) && sdr.GetName(i) != "NgayHieuLuc")
                        {
                            classType.GetProperty(sdr.GetName(i)).SetValue(obj, ((DateTime)sdr[i]).ToString("dd/MM/yyyy"), null);
                        }
                        else
                        {
                            classType.GetProperty(sdr.GetName(i)).SetValue(obj, sdr[i], null);
                        }
                    }
                }
            }
            return obj;
        }

        public static T MappingSQLDRToDTO3<T>(SqlDataReader sdr) where T : class
        {
            T obj = default(T);
            obj = Activator.CreateInstance<T>();
            Type classType = typeof(T);
            for (int i = 0; i < sdr.FieldCount; i++)
            {
                if (HasProperty<T>(sdr.GetName(i)))
                {
                    if (!sdr.IsDBNull(i))
                    {
                        classType.GetProperty(sdr.GetName(i)).SetValue(obj, sdr[i], null);
                    }
                }
            }
            return obj;
        }

        public static T MappingSQLDRToDTO4<T>(SqlDataReader sdr) where T : class
        {
            T obj = default(T);
            obj = Activator.CreateInstance<T>();
            Type classType = typeof(T);
            for (int i = 0; i < sdr.FieldCount; i++)
            {
                if (HasProperty<T>(sdr.GetName(i)))
                {
                    if (!sdr.IsDBNull(i))
                    {
                        if (sdr[i].GetType() == typeof(DateTime) && sdr.GetName(i) != "NgayHieuLuc")
                        {
                            classType.GetProperty(sdr.GetName(i)).SetValue(obj, ((DateTime)sdr[i]).ToString("dd/MM/yyyy"), null);
                        }
                        else if (sdr[i].GetType() == typeof(int) && sdr.GetName(i) != "Id" && sdr.GetName(i) != "TinhTrang")
                        {
                            if (sdr[i] != null)
                            {
                                classType.GetProperty(sdr.GetName(i)).SetValue(obj, sdr[i].ToString(), null);
                            }
                        }
                        else if (sdr[i].GetType() == typeof(Decimal))
                        {
                            classType.GetProperty(sdr.GetName(i)).SetValue(obj, double.Parse(sdr[i].ToString()), null);
                        }
                        else
                        {
                            classType.GetProperty(sdr.GetName(i)).SetValue(obj, sdr[i], null);
                        }
                    }
                }
            }
            return obj;
        }
    }
}
