﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    
    public class ThiDuaDAC
    {
        const string StrSelectByNhanVienThang = "select * from ThiDua where MaNV = '{0}' and TinhTrang = 0 and LoaiXetTD = '0'";
        const string StrSelectByNhanVienHocKy = "select * from ThiDua where MaNV = '{0}' and TinhTrang = 0 and LoaiXetTD = '1'";
        const string StrSelectByNhanVienNamHoc = "select * from ThiDua where MaNV = '{0}' and TinhTrang = 0 and LoaiXetTD = '2'";
        SqlConnection _conn = new SqlConnection();

        public List<ThiDuaDTO> LayDanhSachThiDuaThangTheoNV(string strmaNV)
        {
            List<ThiDuaDTO> listResult = new List<ThiDuaDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVienThang,strmaNV),_conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<ThiDuaDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<ThiDuaDTO> LayDanhSachThiDuaHocKyTheoNV(string strmaNV)
        {
            List<ThiDuaDTO> listResult = new List<ThiDuaDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVienHocKy, strmaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<ThiDuaDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<ThiDuaDTO> LayDanhSachThiDuaNamHocTheoNV(string strmaNV)
        {
            List<ThiDuaDTO> listResult = new List<ThiDuaDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVienNamHoc, strmaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<ThiDuaDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }
    }
}
