﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuanHeGiaDinhDAC
    {
        const string StrSelectByNhanVien = "select * from ThanNhan where MaNV = '{0}' and TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<QuanHeGiaDinhDto> LayDanhSachQuanHeGiaDinhTheoNhanVien(string maNV)
        {
            List<QuanHeGiaDinhDto> listResult = new List<QuanHeGiaDinhDto>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, maNV), _conn);
            QuanHeGiaDinhDto qhgdDTO;
            while (sdr.Read())
            {
                qhgdDTO = new QuanHeGiaDinhDto();
                TinhThanhDTO ttDTO = new TinhThanhDTO();
                QuanHuyenDTO qhDTO = new QuanHuyenDTO();
                QuanHeThanNhanDTO qhtnDTO = new QuanHeThanNhanDTO();
                ttDTO.MaTinhThanh =  sdr["MaTinhThanh"] as String;
                qhDTO.MaQuanHuyen = sdr["MaQuanHuyen"] as String;
                qhtnDTO.MaQuanHe = sdr["MaQuanHe"] as String;
                qhgdDTO.TinhThanh = ttDTO;
                qhgdDTO.QuanHuyen = qhDTO;
                qhgdDTO.LoaiQuanHe = qhtnDTO;
                qhgdDTO.MaNv = sdr["MaNV"] as String;
                if (sdr["NamSinh"] !=null)
                qhgdDTO.NgaySinh = sdr["NamSinh"].ToString();
                qhgdDTO.NgheNghiep = sdr["NgheNghiep"] as String;
                qhgdDTO.DacDiemLichSu = sdr["DacDiemLichSu"] as String;
                qhgdDTO.GhiChu = sdr["GhiChu"] as String;
                qhgdDTO.HoTen = sdr["HoTen"] as String;
                qhgdDTO.DiaChi = sdr["DiaChi"] as String;
                qhgdDTO.ThanNhanVoChong = Boolean.Parse(sdr["ThanNhanVoChong"].ToString());
                //qhgdDTO.GioiTinh = Convert.ToInt32(sdr["GioiTinh"]);
                qhgdDTO.Id = Convert.ToInt32(sdr["Id"]);
                listResult.Add(qhgdDTO);
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public bool Save(QuanHeGiaDinhDto qhgdDTO)
        {
            string query = "INSERT INTO ThanNhan([MaNV],[HoTen],[NamSinh],[MaQuanHe],[DacDiemLichSu],[NgheNghiep],[GhiChu]"
                + ",[MaQuanHuyen],[NgayHieuLuc],[TinhTrang],[MaTinhThanh], DiaChi, ThanNhanVoChong) VALUES ('" + qhgdDTO.MaNv + "', N'" + qhgdDTO.HoTen + "',"
                + (String.IsNullOrEmpty(qhgdDTO.NgaySinh)? "null,": "'" +qhgdDTO.NgaySinh + "',")
                + (String.IsNullOrEmpty(qhgdDTO.LoaiQuanHe.MaQuanHe) ? "null," : ("'" + qhgdDTO.LoaiQuanHe.MaQuanHe + "',"))
                + (String.IsNullOrEmpty(qhgdDTO.DacDiemLichSu) ? "null," : ("N'" + qhgdDTO.DacDiemLichSu + "',"))
                + (String.IsNullOrEmpty(qhgdDTO.NgheNghiep) ? "null," : ("N'" + qhgdDTO.NgheNghiep + "',"))
                + (String.IsNullOrEmpty(qhgdDTO.GhiChu) ? "null," : ("N'" + qhgdDTO.GhiChu + "',"))
                + (String.IsNullOrEmpty(qhgdDTO.QuanHuyen.MaQuanHuyen) ? "null," : ("'" + qhgdDTO.QuanHuyen.MaQuanHuyen + "',"))
                + "getdate(),0,"
                + (String.IsNullOrEmpty(qhgdDTO.TinhThanh.MaTinhThanh) ? "null," : ("'" + qhgdDTO.TinhThanh.MaTinhThanh + "',"))
                + (String.IsNullOrEmpty(qhgdDTO.DiaChi) ? "null," : ("N'" + qhgdDTO.DiaChi + "',"))
                + (qhgdDTO.ThanNhanVoChong? "1": "0")
                + ")";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Update(QuanHeGiaDinhDto qhgdDTO)
        {
            string query = "UPDATE ThanNhan SET HoTen = N'" + qhgdDTO.HoTen + "', NamSinh = "
                + (String.IsNullOrEmpty(qhgdDTO.NgaySinh) ? "null," : "'" + qhgdDTO.NgaySinh + "',")
                // + "convert(date, '" + qhgdDTO.NgaySinh.ToString("dd/MM/yyyy") + "',103),"
                + "MaQuanHe = " + (String.IsNullOrEmpty(qhgdDTO.LoaiQuanHe.MaQuanHe) ? "null," : ("'" + qhgdDTO.LoaiQuanHe.MaQuanHe + "',"))
                + "DacDiemLichSu = " + (String.IsNullOrEmpty(qhgdDTO.DacDiemLichSu) ? "null," : ("N'" + qhgdDTO.DacDiemLichSu + "',"))
                + "NgheNghiep = " + (String.IsNullOrEmpty(qhgdDTO.NgheNghiep) ? "null," : ("N'" + qhgdDTO.NgheNghiep + "',"))
                + "GhiChu = " + (String.IsNullOrEmpty(qhgdDTO.GhiChu) ? "null," : ("N'" + qhgdDTO.GhiChu + "',"))
                + "MaQuanHuyen = " + (String.IsNullOrEmpty(qhgdDTO.QuanHuyen.MaQuanHuyen) ? "null," : ("'" + qhgdDTO.QuanHuyen.MaQuanHuyen + "',"))
                + "MaTinhThanh = " + (String.IsNullOrEmpty(qhgdDTO.TinhThanh.MaTinhThanh) ? "null," : ("'" + qhgdDTO.TinhThanh.MaTinhThanh + "',"))
                + "DiaChi = " + (String.IsNullOrEmpty(qhgdDTO.DiaChi) ? "null, " : ("N'" + qhgdDTO.DiaChi + "', "))
                + "ThanNhanVoChong = " +(qhgdDTO.ThanNhanVoChong ? "1 " : "0 ")
                //+ "GioiTinh = " + qhgdDTO.GioiTinh
                + " WHERE Id = " + qhgdDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(string id)
        {
            string query = "UPDATE ThanNhan SET TinhTrang = 1 WHERE Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
