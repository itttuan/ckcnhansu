﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class DataProvider
    {
    //    static string strTenServerDevelopment = ".";//server local khi code
        static string strTenServerDevelopment = ".\\SQLEXPRESS";//server local khi code
        static string strTenServerProduction = "KTCT_3";//server khi release
        static string strTenServer = strTenServerProduction;
        static string strTenDb = "hrmtdb1";//db chính
        static string strTenImgDb = "hrmtdb_img";//db phụ chứa hình ảnh
        static string strConnectionStringFormat = @"Data Source={0};Initial Catalog={1};Integrated Security=true;";

        /// <summary>
        /// Đổi connection string, kết nối local server
        /// </summary>
        public static void ChangeToDevelopmentServer()
        {
            strTenServer = strTenServerDevelopment;
        }

        /// <summary>
        /// Đổi connection string, kết nối server thật
        /// </summary>
        public static void ChangeToProductionServer()
        {
            strTenServer = strTenServerProduction;
        }

        //có cần hàm này không? đã có hàm TaoKetNoiCSDL rồi?
        public static string SqlConnectionString()
        {
            return string.Format(strConnectionStringFormat,strTenServer,strTenDb);
        }

        /// <summary>
        /// Tạo kết nối
        /// </summary>
        /// <param name="imgDb">true: db hình ảnh, false (default): db chính</param>
        /// <returns></returns>
        public static SqlConnection TaoKetNoiCSDL(bool imgDb=false)
        {
            try
            {
                string strChuoiKetNoi = string.Format(strConnectionStringFormat, strTenServer, imgDb?strTenImgDb:strTenDb);
                SqlConnection conn = new SqlConnection(strChuoiKetNoi);
                conn.Open();
                return conn;
            }
            catch
            {
                throw new Exception("Lỗi kết nối Cơ Sở Dữ Liệu...");
            }
        }

        public static SqlDataReader TruyVanDuLieu(string strCauTruyVan, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                SqlDataReader dr = com.ExecuteReader();
                return dr;
            }
            catch
            {
                throw new Exception("Lỗi Truy Vấn Dữ Liệu...");
            }
        }

        public static SqlDataReader TruyVanDuLieu(string strCauTruyVan, SqlParameter[] arrParam, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(strCauTruyVan, conn);
                com.Parameters.AddRange(arrParam);
                SqlDataReader dr = com.ExecuteReader();
                return dr;
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi Truy Vấn Dữ Liệu..." + ex.Message);
            }
        }

        public static int ThucThiSQL(string strCauTruyVan, SqlParameter[] arrParam, SqlConnection conn)
        {
            SqlCommand com = null;
            try
            {
                com = new SqlCommand(strCauTruyVan, conn);
                com.Parameters.AddRange(arrParam);
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi thực thi SQL..." + ex.Message);
            }
            finally
            {
                com.Dispose();
            }
        }

        public static int ThucThiSQL(string strCauTruyVan, SqlConnection conn)
        {
            SqlCommand com = null;
            try
            {
                com = new SqlCommand(strCauTruyVan, conn);
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi thực thi SQL..." + ex.Message);
            }
            finally
            {
                com.Dispose();
            }
        }

        public static bool KiemTraKetNoiSERVER()
        {
            bool isKQ = false;
            try
            {
                SqlConnection conn = TaoKetNoiCSDL();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return isKQ;
        }
    }
}
