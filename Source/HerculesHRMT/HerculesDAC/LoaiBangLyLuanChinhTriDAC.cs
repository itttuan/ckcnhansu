﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiBangLyLuanChinhTriDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<LoaiBangLyLuanChinhTriDTO> LayDanhSachLoaiBangLLChinhTri()
        {
            List<LoaiBangLyLuanChinhTriDTO> listRes = new List<LoaiBangLyLuanChinhTriDTO>();
            string query = "select * from LoaiBangLyLuanChinhTri";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<LoaiBangLyLuanChinhTriDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public LoaiBangLyLuanChinhTriDTO TimLoaiBangLLChinhTri(string id)
        {
            LoaiBangLyLuanChinhTriDTO listRes = new LoaiBangLyLuanChinhTriDTO();
            string query = "select * from LoaiBangLyLuanChinhTri where Id = " + id;
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<LoaiBangLyLuanChinhTriDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
