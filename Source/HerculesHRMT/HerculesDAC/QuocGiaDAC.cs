﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class QuocGiaDAC
    {
        SqlConnection _conn = new SqlConnection();
        public List<QuocGiaDTO> LayDanhSachQuocGia()
        {
            List<QuocGiaDTO> listRes = new List<QuocGiaDTO>();
            string query = "select * from QuocGia";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO2<QuocGiaDTO>(sdr));
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }

        public QuocGiaDTO TimQuocGiaTheoMa(string strMaQG)
        {
            QuocGiaDTO listRes = new QuocGiaDTO();
            string query = "select * from QuocGia where MaQuocGia = N'" + strMaQG + "'";
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                return Utils.MappingSQLDRToDTO2<QuocGiaDTO>(sdr);
            }
            sdr.Close();
            _conn.Close();
            return listRes;
        }
    }
}
