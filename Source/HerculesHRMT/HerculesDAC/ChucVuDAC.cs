﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class ChucVuDAC
    {
        //const string StrSelect = "SELECT * FROM ChucVu WHERE TinhTrang = 0";
        //vdtoan update 04102016
        const string StrSelect = "SELECT * FROM ChucVu WHERE TinhTrang = 0 order by Id asc";

        const string StrInsert = "INSERT INTO ChucVu (MaChucVu, TenChucVu, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', '{3}', '{4}')";
        const string StrUpdate = "UPDATE ChucVu SET TenChucVu = N'{0}', GhiChu = N'{1}' WHERE MaChucVu = '{2}'";
        const string StrDelete = "UPDATE ChucVu SET TinhTrang = 1 WHERE MaChucVu = '{0}'";
        const string SelectByMaChucVu = "SELECT * FROM ChucVu WHERE TinhTrang = 0 AND MaChucVu = \'{0}\'";

        SqlConnection _conn = new SqlConnection();

        public ChucVuDAC()
        {
            //_conn = DataProvider.TaoKetNoiCSDL();
        }

        public List<ChucVuDTO> GetAll()
        {
            var results = new List<ChucVuDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var reader = DataProvider.TruyVanDuLieu(StrSelect, _conn);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var chucVuDto = new ChucVuDTO()
                    {
                        Id = reader.GetInt32(0),
                        MaChucVu = reader.GetString(1),
                        TenChucVu = reader.GetString(2),
                        GhiChu = reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                        NgayHieuLuc = reader.GetDateTime(4),
                        TinhTrang = reader.GetInt32(5)
                    };

                    results.Add(chucVuDto);
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }

            reader.Close();
            _conn.Close();

            return results;
        }

        public bool Save(ChucVuDTO obj)
        {
            var query = string.Format(StrInsert, obj.MaChucVu, obj.TenChucVu, obj.GhiChu, obj.NgayHieuLuc, obj.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(ChucVuDTO obj)
        {
            var query = string.Format(StrUpdate, obj.TenChucVu, obj.GhiChu, obj.MaChucVu);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(ChucVuDTO obj)
        {
            var query = string.Format(StrDelete, obj.MaChucVu);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public ChucVuDTO GetByMaDanToc(string maChucVu)
        {
            var result = new ChucVuDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaChucVu, maChucVu);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = Utils.MappingSQLDRToDTO<ChucVuDTO>(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }
    }
}
