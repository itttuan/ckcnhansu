﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class UngVienDAC
    {
        //const string StrSelectAll = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 order by left(MaPhongKhoa,1) desc";
        //const string StrSelectByMaPhongKhoa = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 AND MaPhongKhoa = \'{0}\'";
        //const string StrInsert = "INSERT INTO PhongKhoa (MaPhongKhoa, TenPhongKhoa, TenVietTat, ThuTuBaoCao, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', {3}, N'{4}', '{5}', '{6}')";
        //const string StrUpdate = "UPDATE PhongKhoa SET TenPhongKhoa = N'{0}', TenVietTat = N'{1}', ThuTuBaoCao = {2}, GhiChu = N'{3}' WHERE MaPhongKhoa = '{4}'";
        //const string StrDelete = "UPDATE PhongKhoa SET TinhTrang = 1 WHERE MaLoaiTrinhDo = '{0}'";
        //const string StrMaxMaPhongKhoa = "SELECT MAX(MaPhongKhoa) MaPhongKhoa FROM PhongKhoa";

        //const string StrSelectAll = "SELECT * FROM UngVien order by MaUV desc";
        const string StrSelectAll = "SELECT * FROM UngVien WHERE TinhTrang = 0 order by MaUV asc";

        //const string StrInsert = "INSERT INTO UngVien(MaUV, MaDot, Ho, Ten, NgaySinh, GioiTinh, CMND, DienThoai, Email, QueQuan,NganhHoc, MaTruong, TruongKhac,HinhThucDaoTao,XepHang, DiemTN,SauDaiHoc,MaPK,MaBMBP,NganhDuTuyen,ChungChi,GhiChu,YeuCauChuyenMon,KyNangSuPham) VALUES ('{0}', '{1}', N'{2}', N'{3}', '{4}', N'{5}', '{6}', '{7}', '{8}', N'{9}', N'{10}', N'{11}', N'{12}', N'{13}', N'{14}', '{15}', N'{16}', N'{17}', N'{18}', N'{19}', N'{20}', N'{21}', N'{22}', N'{23}')";
        const string StrInsert = "INSERT INTO UngVien(MaUV, MaDot, Ho, Ten, NgaySinh, GioiTinh, CMND, DienThoai, Email, QueQuan,NganhHoc, MaTruong, TruongKhac,HinhThucDaoTao,XepHang, DiemTN,SauDaiHoc,MaPK,MaBMBP,NganhDuTuyen,ChungChi,GhiChu,YeuCauChuyenMon,KyNangSuPham,TinhTrang) VALUES ('{0}', '{1}', N'{2}', N'{3}', '{4}', N'{5}', '{6}', '{7}', '{8}', N'{9}', N'{10}', N'{11}', N'{12}', N'{13}', N'{14}', '{15}', N'{16}', N'{17}', N'{18}', N'{19}', N'{20}', N'{21}', N'{22}', N'{23}', N'{24}')";

        const string StrUpdate = "UPDATE UngVien SET MaDot = '{0}', Ho = N'{1}', Ten = N'{2}', NgaySinh ='{3}', GioiTinh = N'{4}', CMND ='{5}', DienThoai ='{6}', Email ='{7}', QueQuan =N'{8}', NganhHoc =N'{9}', MaTruong =N'{10}', TruongKhac =N'{11}', HinhThucDaoTao =N'{12}', XepHang =N'{13}', DiemTN ='{14}', SauDaiHoc =N'{15}', MaPK ='{16}', MaBMBP ='{17}', NganhDuTuyen =N'{18}' , ChungChi =N'{19}' , GhiChu =N'{20}' , YeuCauChuyenMon =N'{21}' , KyNangSuPham =N'{22}' WHERE MaUV = '{23}'";
        //const string StrDelete = "DELETE from UngVien WHERE MaUV = '{0}'";
        const string StrDelete = "UPDATE UngVien SET TinhTrang = 1 WHERE MaUV = '{0}'";

        const string StrMaxMaUV = "SELECT MAX(MaUV) MaUV FROM UngVien";
        SqlConnection _conn = new SqlConnection();

        private UngVienDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            UngVienDTO uvDTO = Utils.MappingSQLDRToDTO<UngVienDTO>(sdr);
            //if (!sdr.IsDBNull(sdr.GetOrdinal("NgayGio")))
            //{
            //    dtDTO.NgayGio = ((DateTime)sdr["NgayGio"]).ToString("dd/MM/yyyy HH:mm:ss");
            //}

            //if (int.Parse(dtDTO.Thang) < 10 && int.Parse(dtDTO.Thang) > 0)
            //    dtDTO.Thang = "0" + dtDTO.Thang;
            //if (pkDTO.TenVietTat != null && pkDTO.TenVietTat.CompareTo("NULL") == 0)
            //    pkDTO.TenVietTat = string.Empty;

            //if (pkDTO.GhiChu != null && pkDTO.GhiChu.CompareTo("NULL") == 0)
            //    pkDTO.GhiChu = string.Empty;

            return uvDTO;
        }

        public List<UngVienDTO> GetAll()
        {
            var result = new List<UngVienDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    var result = new PhongKhoaDTO();

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrSelectByMaPhongKhoa, maPhongKhoa);
        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    while (sdr.Read())
        //    {
        //        result = MappingSQLDRToDTO(sdr);
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        public string GetMaxMaUV()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaUV);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaUV"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(UngVienDTO uvNew)
        {
           // string ttbc = pkNew.ThuTuBaoCao != null ? pkNew.ThuTuBaoCao.ToString() : "NULL";
            //var query = string.Format(StrInsert, uvNew.MaUV, uvNew.MaDot, uvNew.Ho, uvNew.Ten, uvNew.NgaySinh, uvNew.GioiTinh, uvNew.CMND, uvNew.DienThoai, uvNew.Email, uvNew.QueQuan, uvNew.NganhHoc, uvNew.MaTruong, uvNew.TruongKhac, uvNew.HinhThucDaoTao, uvNew.XepHang, uvNew.DiemTN, uvNew.SauDaiHoc, uvNew.MaPK, uvNew.MaBMBP, uvNew.NganhDuTuyen, uvNew.ChungChi, uvNew.GhiChu, uvNew.YeuCauChuyenMon, uvNew.KyNangSuPham);
            var query = string.Format(StrInsert, uvNew.MaUV, uvNew.MaDot, uvNew.Ho, uvNew.Ten, uvNew.NgaySinh, uvNew.GioiTinh, uvNew.CMND, uvNew.DienThoai, uvNew.Email, uvNew.QueQuan, uvNew.NganhHoc, uvNew.MaTruong, uvNew.TruongKhac, uvNew.HinhThucDaoTao, uvNew.XepHang, uvNew.DiemTN, uvNew.SauDaiHoc, uvNew.MaPK, uvNew.MaBMBP, uvNew.NganhDuTuyen, uvNew.ChungChi, uvNew.GhiChu, uvNew.YeuCauChuyenMon, uvNew.KyNangSuPham,uvNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(UngVienDTO uvUpdate)
        {
          //  string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrUpdate, uvUpdate.MaDot, uvUpdate.Ho, uvUpdate.Ten, uvUpdate.NgaySinh, uvUpdate.GioiTinh, uvUpdate.CMND, uvUpdate.DienThoai, uvUpdate.Email, uvUpdate.QueQuan, uvUpdate.NganhHoc, uvUpdate.MaTruong, uvUpdate.TruongKhac, uvUpdate.HinhThucDaoTao, uvUpdate.XepHang, uvUpdate.DiemTN, uvUpdate.SauDaiHoc, uvUpdate.MaPK, uvUpdate.MaBMBP, uvUpdate.NganhDuTuyen, uvUpdate.ChungChi, uvUpdate.GhiChu, uvUpdate.YeuCauChuyenMon, uvUpdate.KyNangSuPham, uvUpdate.MaUV);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(UngVienDTO uvDel)
        {
            var query = string.Format(StrDelete, uvDel.MaUV);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
