﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class BangCapChungChiDAC
    {
        const string StrSelectByNhanVien = "select * from TrinhDo where MaNV = '{0}' and TinhTrang = 0";
        SqlConnection _conn = new SqlConnection();

        public List<BangCapChungChiDto> LayDanhSachBangCapChungChiNhanVien(string strMaNV)
        {
            List<BangCapChungChiDto> result = new List<BangCapChungChiDto>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(string.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                BangCapChungChiDto dto = new BangCapChungChiDto();
                LoaiBCapCChiDTO lbcdto = new LoaiBCapCChiDTO();
                LoaiTrinhDoDTO ltdto = new LoaiTrinhDoDTO();
                HeDaoTaoDTO hdtdto = new HeDaoTaoDTO();
                NganhDTO ngdto = new NganhDTO();
                ChuyenNganhDto cngdto = new ChuyenNganhDto();
                TruongDTO truongdto = new TruongDTO();
                dto.Id = int.Parse(sdr["Id"].ToString());
                dto.MaBangCapChungChi = sdr["MaBCCC"] as string;
                dto.MaNv = sdr["MaNV"] as string;
                truongdto.MaTruong = sdr["MaCoSoDaoTao"] as string;
                dto.CoSoDaotao = truongdto;
                lbcdto.MaLoaiBCCC = sdr["MaLoaiBCCC"] as string;
                dto.LoaiBangCapChungChi = lbcdto;
                //ngdto = null;
                dto.NganhDaoTao = ngdto;
                dto.ChuyenNganhDaoTao = cngdto;
                ltdto.MaLoaiTrinhDo = sdr["MaLoaiTrinhDo"] as string;
                dto.LoaiTrinhDo = ltdto;
                hdtdto.MaHeDaoTao = sdr["MaHeDaoTao"] as string;
                dto.HeDaoTao = hdtdto;
                dto.ThoiGianDaoTao = sdr["ThoiGianDaoTaoTu"] as string;
                dto.ThoiGianHieuLuc = sdr["ThoiGianHieuLuc"] as string;
                if (!sdr.IsDBNull(sdr.GetOrdinal("NgayCapBang")))
                {
                     dto.NgayCapBang = Convert.ToDateTime(sdr["NgayCapBang"].ToString());
                }
                dto.GhiChu = sdr["MoTaThem"] as string;
                result.Add(dto);
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public bool Save(BangCapChungChiDto bcccDTO)
        {
            string query = "INSERT INTO TrinhDo([MaBCCC],[MaNV],[MaLoaiBCCC],[MaLoaiTrinhDo],[MaHeDaoTao] "
                + ",[ThoiGianHieuLuc],[NgayCap],[MoTaThem],[NgayHieuLuc],[TinhTrang],[IsBangCap],[MaCoSoDaoTao], ThoiGianDaoTaoTu) VALUES ("
                + (String.IsNullOrEmpty(bcccDTO.MaBangCapChungChi) ? "null,'" : ("'" + bcccDTO.MaBangCapChungChi + "','"))
                + bcccDTO.MaNv + "'," + (String.IsNullOrEmpty(bcccDTO.LoaiBangCapChungChi.MaLoaiBCCC) ? "null," : ("'" + bcccDTO.LoaiBangCapChungChi.MaLoaiBCCC + "',"))
                + (String.IsNullOrEmpty(bcccDTO.LoaiTrinhDo.MaLoaiTrinhDo) ? "null," : ("'" + bcccDTO.LoaiTrinhDo.MaLoaiTrinhDo + "',"))
                + (String.IsNullOrEmpty(bcccDTO.HeDaoTao.MaHeDaoTao) ? "null," : ("'" + bcccDTO.HeDaoTao.MaHeDaoTao + "',"))
                + (String.IsNullOrEmpty(bcccDTO.ThoiGianHieuLuc) ? "null," : ("N'" + bcccDTO.ThoiGianHieuLuc + "',"))
                + "convert(date,'" + bcccDTO.NgayCapBang.ToString("dd/MM/yyyy") + "',103),"
                + (String.IsNullOrEmpty(bcccDTO.GhiChu) ? "null," : ("N'" + bcccDTO.GhiChu + "',")) + "getdate(),0,"
                + Convert.ToInt32(bcccDTO.IsBangCap) + "," + (String.IsNullOrEmpty(bcccDTO.CoSoDaotao.MaTruong) ? "null," : ("'" + bcccDTO.CoSoDaotao.MaTruong + "',"))
                + (String.IsNullOrEmpty(bcccDTO.ThoiGianDaoTao) ? "null)" : "N'" + bcccDTO.ThoiGianDaoTao + "')");

            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Update(BangCapChungChiDto bcccDTO)
        {
            string query = "UPDATE TrinhDo SET MaBCCC = " + (String.IsNullOrEmpty(bcccDTO.MaBangCapChungChi) ? "null,'" : ("'" + bcccDTO.MaBangCapChungChi + "',"))
                + "MaLoaiBCCC = " + (String.IsNullOrEmpty(bcccDTO.LoaiBangCapChungChi.MaLoaiBCCC) ? "null," : ("'" + bcccDTO.LoaiBangCapChungChi.MaLoaiBCCC + "',"))
                + "MaLoaiTrinhDo = " + (String.IsNullOrEmpty(bcccDTO.LoaiTrinhDo.MaLoaiTrinhDo) ? "null," : ("'" + bcccDTO.LoaiTrinhDo.MaLoaiTrinhDo + "',"))
                + "MaHeDaoTao = " + (String.IsNullOrEmpty(bcccDTO.HeDaoTao.MaHeDaoTao) ? "null," : ("'" + bcccDTO.HeDaoTao.MaHeDaoTao + "',"))
                + "ThoiGianHieuLuc = " + (String.IsNullOrEmpty(bcccDTO.ThoiGianHieuLuc) ? "null," : ("N'" + bcccDTO.ThoiGianHieuLuc + "',"))
                + "NgayCap = " + "convert(date,'" + bcccDTO.NgayCapBang.ToString("dd/MM/yyyy") + "',103),"
                + "MoTaThem = " + (String.IsNullOrEmpty(bcccDTO.GhiChu) ? "null," : ("N'" + bcccDTO.GhiChu + "',"))
                + "ThoiGianDaoTaoTu = " + (String.IsNullOrEmpty(bcccDTO.ThoiGianDaoTao) ? "null," : ("N'" + bcccDTO.ThoiGianDaoTao + "',"))
                + "IsBangCap = " + Convert.ToInt32(bcccDTO.IsBangCap) + ",MaCoSoDaoTao = " + (String.IsNullOrEmpty(bcccDTO.CoSoDaotao.MaTruong) ? "null," : ("'" + bcccDTO.CoSoDaotao.MaTruong + "'"))
                + " where Id = " + bcccDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(string id)
        {
            string query = "update TrinhDo set TinhTrang = 1 where Id = " + id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
