﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class TrinhDoDAC
    {
        string strTDChuyenMon = "";
        string strTDNgoaiNgu = "";
        string strTDTinHoc = "";
        string strTDNghiepVu = "";
        string strTDLLCT = "";
        string strQuery = "SELECT * FROM TRINHDO WHERE MaNV = '{0}' AND TinhTrang = 0 and ";
        SqlConnection _conn = new SqlConnection();
        public List<TrinhDoDTO> LayDanhSachTrinhDo(string strMaNV, int LoaiDaoTao)
        {
            List<TrinhDoDTO> listRes = new List<TrinhDoDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            string query = "";
            if (LoaiDaoTao == 1 || LoaiDaoTao == 2)
            {
                query = strQuery + "(LoaiDaoTao = 1 OR LoaiDaoTao = 2)";
            }
            else 
            {
                query = strQuery + "LoaiDaoTao = " + LoaiDaoTao;
            }
            query = String.Format(query, strMaNV);
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(query, _conn);
            while (sdr.Read())
            {
                listRes.Add(Utils.MappingSQLDRToDTO4<TrinhDoDTO>(sdr));
            }
            sdr.Close();
            _conn.Close(); 
            return listRes;
        }

        //public bool Save(TrinhDoDTO trinhdoDTO)
        //{
        //    string query = "INSERT INTO TRINHDO(MaNV,LoaiDaoTao, LoaiTrinhDoChuyenMon,MaNgoaiNgu,LoaiTenBangCapNgoaiNgu,"
        //        + "LoaiTrinhDoNgoaiNgu,LoaiTrinhDoTinHoc,LoaiNghiepVu,LoaiNghiepVuSuPham,"
        //        + "LoaiLyLuanChinhTri,HinhThucDaoTao,MaTruong,HocBongNhaNuoc,TenDeAnHocBong,ChuyenNganhDaoTao,"
        //        + "ThoiGianDaoTaoTu,ThoiGianDaoTaoDen,NoiCapBang,NgayCapBang,NgayHieuLuc,TinhTrang) VALUES ("
        //        + "'" + trinhdoDTO.MaNV + "'," + trinhdoDTO.LoaiDaoTao + ","
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoChuyenMon) ? "null," : trinhdoDTO.LoaiTrinhDoChuyenMon + ",")
        //        + (string.IsNullOrEmpty(trinhdoDTO.MaNgoaiNgu) ? "null," : "'" + trinhdoDTO.MaNgoaiNgu + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiTenBangCapNgoaiNgu)? "null,": "'" + trinhdoDTO.LoaiTenBangCapNgoaiNgu + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoNgoaiNgu) ? "null," : trinhdoDTO.LoaiTrinhDoNgoaiNgu + ",")
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoTinHoc) ? "null," : trinhdoDTO.LoaiTrinhDoTinHoc + ",")
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiNghiepVu) ? "null," : "N'"+trinhdoDTO.LoaiNghiepVu + "',")
        //        + (trinhdoDTO.LoaiNghiepVuSuPham ? "1," : "0,")
        //        + (string.IsNullOrEmpty(trinhdoDTO.LoaiLyLuanChinhTri) ? "null," : trinhdoDTO.LoaiLyLuanChinhTri + ",")
        //        + (string.IsNullOrEmpty(trinhdoDTO.HinhThucDaoTao) ? "null," : trinhdoDTO.HinhThucDaoTao + ",")
        //        + (string.IsNullOrEmpty(trinhdoDTO.MaTruong)? "null," : "N'"+trinhdoDTO.MaTruong + "',")
        //        + (trinhdoDTO.HocBongNhaNuoc ? "1," : "0,")
        //        + (string.IsNullOrEmpty(trinhdoDTO.TenDeAnHocBong) ? "null," : "N'" + trinhdoDTO.TenDeAnHocBong + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.ChuyenNganhDaoTao) ? "null," : "N'" + trinhdoDTO.ChuyenNganhDaoTao + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoTu) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoTu + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoDen) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoDen + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.NoiCapBang) ? "null," : "N'" + trinhdoDTO.NoiCapBang + "',")
        //        + (string.IsNullOrEmpty(trinhdoDTO.NgayCapBang) ? "null," : "'" + trinhdoDTO.NgayCapBang + "',")
        //        + "GetDate(),0)";
        //    try
        //    {
        //        _conn = DataProvider.TaoKetNoiCSDL();
        //        int kq = DataProvider.ThucThiSQL(query, _conn);
        //        _conn.Close();
        //        if (kq != 0)
        //            return true;
        //    }
        //    catch
        //    {
        //    }
        //    return false;
        //}

        //vdtoan update 04102016
        public bool Save(TrinhDoDTO trinhdoDTO)
        {
            string query = "INSERT INTO TRINHDO(MaNV,LoaiDaoTao, LoaiTrinhDoChuyenMon,MaNgoaiNgu,LoaiTenBangCapNgoaiNgu,"
                + "LoaiTrinhDoNgoaiNgu,LoaiTrinhDoTinHoc,LoaiNghiepVu,LoaiNghiepVuSuPham,"
                + "LoaiLyLuanChinhTri,HinhThucDaoTao,MaTruong,HocBongNhaNuoc,TenDeAnHocBong,ChuyenNganhDaoTao,"
                + "ThoiGianDaoTaoTu,ThoiGianDaoTaoDen,NoiCapBang,NgayCapBang,NgayHieuLuc,TinhTrang,TrinhDoBanDau) VALUES ("
                + "'" + trinhdoDTO.MaNV + "'," + trinhdoDTO.LoaiDaoTao + ","
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoChuyenMon) ? "null," : trinhdoDTO.LoaiTrinhDoChuyenMon + ",")
                + (string.IsNullOrEmpty(trinhdoDTO.MaNgoaiNgu) ? "null," : "'" + trinhdoDTO.MaNgoaiNgu + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiTenBangCapNgoaiNgu) ? "null," : "'" + trinhdoDTO.LoaiTenBangCapNgoaiNgu + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoNgoaiNgu) ? "null," : trinhdoDTO.LoaiTrinhDoNgoaiNgu + ",")
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoTinHoc) ? "null," : trinhdoDTO.LoaiTrinhDoTinHoc + ",")
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiNghiepVu) ? "null," : "N'" + trinhdoDTO.LoaiNghiepVu + "',")
                + (trinhdoDTO.LoaiNghiepVuSuPham ? "1," : "0,")
                + (string.IsNullOrEmpty(trinhdoDTO.LoaiLyLuanChinhTri) ? "null," : trinhdoDTO.LoaiLyLuanChinhTri + ",")
                + (string.IsNullOrEmpty(trinhdoDTO.HinhThucDaoTao) ? "null," : trinhdoDTO.HinhThucDaoTao + ",")
                + (string.IsNullOrEmpty(trinhdoDTO.MaTruong) ? "null," : "N'" + trinhdoDTO.MaTruong + "',")
                + (trinhdoDTO.HocBongNhaNuoc ? "1," : "0,")
                + (string.IsNullOrEmpty(trinhdoDTO.TenDeAnHocBong) ? "null," : "N'" + trinhdoDTO.TenDeAnHocBong + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.ChuyenNganhDaoTao) ? "null," : "N'" + trinhdoDTO.ChuyenNganhDaoTao + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoTu) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoTu + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoDen) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoDen + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.NoiCapBang) ? "null," : "N'" + trinhdoDTO.NoiCapBang + "',")
                + (string.IsNullOrEmpty(trinhdoDTO.NgayCapBang) ? "null," : "'" + trinhdoDTO.NgayCapBang + "',")
                + "GetDate(),0,"
                + (trinhdoDTO.TrinhDoBanDau ? "1" : "0")
                + ")";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        //public bool Update(TrinhDoDTO trinhdoDTO)
        //{
        //    string query = "UPDATE TRINHDO SET LoaiTrinhDoChuyenMon =" + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoChuyenMon) ? "null," : trinhdoDTO.LoaiTrinhDoChuyenMon + ",")
        //        + " MaNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.MaNgoaiNgu) ? "null," : "'" + trinhdoDTO.MaNgoaiNgu + "',")
        //        + " LoaiDaoTao = '" + trinhdoDTO.LoaiDaoTao + "',"
        //        + " LoaiTenBangCapNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTenBangCapNgoaiNgu) ? "null," : "'" + trinhdoDTO.LoaiTenBangCapNgoaiNgu + "',")
        //        + " LoaiTrinhDoNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoNgoaiNgu) ? "null," : trinhdoDTO.LoaiTrinhDoNgoaiNgu + ",")
        //        + " LoaiTrinhDoTinHoc = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoTinHoc) ? "null," : trinhdoDTO.LoaiTrinhDoTinHoc + ",")
        //        + " LoaiNghiepVu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiNghiepVu) ? "null," :"N'" + trinhdoDTO.LoaiNghiepVu + "',")
        //        + " LoaiNghiepVuSuPham = " + (trinhdoDTO.LoaiNghiepVuSuPham ? "1," : "0,")
        //        + " LoaiLyLuanChinhTri = " +(string.IsNullOrEmpty(trinhdoDTO.LoaiLyLuanChinhTri) ? "null," : trinhdoDTO.LoaiLyLuanChinhTri + ",")
        //        + " HinhThucDaoTao = " + (string.IsNullOrEmpty(trinhdoDTO.HinhThucDaoTao) ? "null," : trinhdoDTO.HinhThucDaoTao + ",")
        //        + " MaTruong = " + (string.IsNullOrEmpty(trinhdoDTO.MaTruong) ? "null," : "N'" + trinhdoDTO.MaTruong + "',")
        //        + " HocBongNhaNuoc = " + (trinhdoDTO.HocBongNhaNuoc ? "1," : "0,")
        //        + " TenDeAnHocBong = " + (string.IsNullOrEmpty(trinhdoDTO.TenDeAnHocBong) ? "null," : "N'" + trinhdoDTO.TenDeAnHocBong + "',")
        //        + " ChuyenNganhDaoTao = " + (string.IsNullOrEmpty(trinhdoDTO.ChuyenNganhDaoTao) ? "null," : "N'" + trinhdoDTO.ChuyenNganhDaoTao + "',")
        //        + " ThoiGianDaoTaoTu = " + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoTu) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoTu + "',")
        //        + " ThoiGianDaoTaoDen = " + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoDen) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoDen + "',")
        //        + " NoiCapBang = " + (string.IsNullOrEmpty(trinhdoDTO.NoiCapBang) ? "null," : "N'" + trinhdoDTO.NoiCapBang + "',")
        //        + " NgayCapBang = " + (string.IsNullOrEmpty(trinhdoDTO.NgayCapBang) ? "null " : "'" + trinhdoDTO.NgayCapBang + "' ")
        //        + " WHERE Id = " + trinhdoDTO.Id;
        //    try
        //    {
        //        _conn = DataProvider.TaoKetNoiCSDL();
        //        int kq = DataProvider.ThucThiSQL(query, _conn);
        //        _conn.Close();
        //        if (kq != 0)
        //            return true;
        //    }
        //    catch
        //    {
        //    }
        //    return false;
        //}

        //vdtoan update 04102016
        public bool Update(TrinhDoDTO trinhdoDTO)
        {
            string query = "UPDATE TRINHDO SET LoaiTrinhDoChuyenMon =" + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoChuyenMon) ? "null," : trinhdoDTO.LoaiTrinhDoChuyenMon + ",")
                + " MaNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.MaNgoaiNgu) ? "null," : "'" + trinhdoDTO.MaNgoaiNgu + "',")
                + " LoaiDaoTao = '" + trinhdoDTO.LoaiDaoTao + "',"
                + " LoaiTenBangCapNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTenBangCapNgoaiNgu) ? "null," : "'" + trinhdoDTO.LoaiTenBangCapNgoaiNgu + "',")
                + " LoaiTrinhDoNgoaiNgu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoNgoaiNgu) ? "null," : trinhdoDTO.LoaiTrinhDoNgoaiNgu + ",")
                + " LoaiTrinhDoTinHoc = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiTrinhDoTinHoc) ? "null," : trinhdoDTO.LoaiTrinhDoTinHoc + ",")
                + " LoaiNghiepVu = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiNghiepVu) ? "null," : "N'" + trinhdoDTO.LoaiNghiepVu + "',")
                + " LoaiNghiepVuSuPham = " + (trinhdoDTO.LoaiNghiepVuSuPham ? "1," : "0,")
                + " LoaiLyLuanChinhTri = " + (string.IsNullOrEmpty(trinhdoDTO.LoaiLyLuanChinhTri) ? "null," : trinhdoDTO.LoaiLyLuanChinhTri + ",")
                + " HinhThucDaoTao = " + (string.IsNullOrEmpty(trinhdoDTO.HinhThucDaoTao) ? "null," : trinhdoDTO.HinhThucDaoTao + ",")
                + " MaTruong = " + (string.IsNullOrEmpty(trinhdoDTO.MaTruong) ? "null," : "N'" + trinhdoDTO.MaTruong + "',")
                + " HocBongNhaNuoc = " + (trinhdoDTO.HocBongNhaNuoc ? "1," : "0,")
                + " TenDeAnHocBong = " + (string.IsNullOrEmpty(trinhdoDTO.TenDeAnHocBong) ? "null," : "N'" + trinhdoDTO.TenDeAnHocBong + "',")
                + " ChuyenNganhDaoTao = " + (string.IsNullOrEmpty(trinhdoDTO.ChuyenNganhDaoTao) ? "null," : "N'" + trinhdoDTO.ChuyenNganhDaoTao + "',")
                + " ThoiGianDaoTaoTu = " + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoTu) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoTu + "',")
                + " ThoiGianDaoTaoDen = " + (string.IsNullOrEmpty(trinhdoDTO.ThoiGianDaoTaoDen) ? "null," : "'" + trinhdoDTO.ThoiGianDaoTaoDen + "',")
                + " NoiCapBang = " + (string.IsNullOrEmpty(trinhdoDTO.NoiCapBang) ? "null," : "N'" + trinhdoDTO.NoiCapBang + "',")
                + " NgayCapBang = " + (string.IsNullOrEmpty(trinhdoDTO.NgayCapBang) ? "null, " : "'" + trinhdoDTO.NgayCapBang + "',")
                + " TrinhDoBanDau = " + (trinhdoDTO.TrinhDoBanDau ? "1" : "0")
                + " WHERE Id = " + trinhdoDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(TrinhDoDTO trinhdoDTO)
        {
            string query = "UPDATE TRINHDO SET TinhTrang = 1 WHERE Id = " + trinhdoDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close(); 
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
    }
}
