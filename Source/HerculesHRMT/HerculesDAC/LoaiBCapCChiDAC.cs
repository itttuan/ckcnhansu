﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiBCapCChiDAC
    {
        const string SelectAll = "SELECT * FROM LoaiBCCC LBCCC WHERE LBCCC.TinhTrang = 0";
        const string SelectByMaBCapCChi = "SELECT * FROM LoaiBCCC LBCCC WHERE LBCCC.TinhTrang = 0 AND MaLoaiBCCC = \'{0}\'";
        const string StrInsert = "INSERT INTO LoaiBCCC (MaLoaiBCCC, TenLoai, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', '{3}', '{4}')";
        const string StrUpdate = "UPDATE LoaiBCCC SET TenLoai = N'{0}', GhiChu = N'{1}' WHERE MaLoaiBCCC = '{2}'";
        const string StrDelete = "UPDATE LoaiBCCC SET TinhTrang = 1 WHERE MaLoaiBCCC = '{0}'";
        const string StrMaxMaLoai = "SELECT MAX(MaLoaiBCCC) MaLoaiBCCC FROM LoaiBCCC LBCCC";

        SqlConnection _conn = new SqlConnection();

        private LoaiBCapCChiDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            LoaiBCapCChiDTO bcccDTO = Utils.MappingSQLDRToDTO<LoaiBCapCChiDTO>(sdr);

            if (bcccDTO.GhiChu != null && bcccDTO.GhiChu.CompareTo("NULL") == 0)
                bcccDTO.GhiChu = string.Empty;

            return bcccDTO;
        }

        public List<LoaiBCapCChiDTO> GetAll()
        {
            var result = new List<LoaiBCapCChiDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(SelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public LoaiBCapCChiDTO GetByMaLoaiBCapCChi(string maLoaiBCapCChi)
        {
            var result = new LoaiBCapCChiDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaBCapCChi, maLoaiBCapCChi);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiBCCC()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoai);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaLoaiBCCC"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(LoaiBCapCChiDTO bcccNew)
        {
            var query = string.Format(StrInsert, bcccNew.MaLoaiBCCC, bcccNew.TenLoai, bcccNew.GhiChu, bcccNew.NgayHieuLuc.ToString("yyyy-MM-dd"), bcccNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(LoaiBCapCChiDTO bcccUpdate)
        {
            var query = string.Format(StrUpdate, bcccUpdate.TenLoai, bcccUpdate.GhiChu, bcccUpdate.MaLoaiBCCC);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(LoaiBCapCChiDTO bcccDel)
        {
            var query = string.Format(StrDelete, bcccDel.MaLoaiBCCC);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
