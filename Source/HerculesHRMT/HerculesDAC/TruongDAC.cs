﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class TruongDAC
    {
        const string StrSelectAll = "SELECT * FROM Truong Trg WHERE Trg.TinhTrang = 0";
        const string SelectByMaTruong = "SELECT * FROM Truong Trg WHERE Trg.TinhTrang = 0 AND MaTruong = \'{0}\'";
        const string StrInsert = "INSERT INTO Truong (MaTruong, TenTruong, TenVietTat, LoaiTruong, GhiChu, NgayHieuLuc, TinhTrang, NuocNgoai) VALUES ('{0}', N'{1}', N'{2}', N'{3}', N'{4}', '{5}', '{6}', '{7}')";
        const string StrUpdate = "UPDATE Truong SET TenTruong = N'{0}', TenVietTat = N'{1}', LoaiTruong = N'{2}', NuocNgoai = '{3}', GhiChu = N'{4}' WHERE MaTruong = '{5}'";
        const string StrDelete = "UPDATE Truong SET TinhTrang = 1 WHERE MaTruong = '{0}'";
        const string StrMaxMaTruong = "SELECT MAX(MaTruong) MaTruong FROM Truong";

        SqlConnection _conn = new SqlConnection();

        private TruongDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            TruongDTO tDTO = Utils.MappingSQLDRToDTO<TruongDTO>(sdr);
            if (tDTO.TenVietTat != null && tDTO.TenVietTat.CompareTo("NULL") == 0)
                tDTO.TenVietTat = string.Empty;

            if (tDTO.GhiChu != null && tDTO.GhiChu.CompareTo("NULL") == 0)
                tDTO.GhiChu = string.Empty;

            switch (int.Parse(tDTO.LoaiTruong))
            {
                case 0:
                    tDTO.TenLoaiTruong = "THPT";
                    break;
                case 1:
                    tDTO.TenLoaiTruong = "Trung học chuyên nghiệp";
                    break;

                case 2:
                    tDTO.TenLoaiTruong = "Cao đẳng";
                    break;

                case 3:
                    tDTO.TenLoaiTruong = "Đại học";
                    break;

                case 4:
                    tDTO.TenLoaiTruong = "Học viện";
                    break;
            }

            return tDTO;
        }

        public List<TruongDTO> GetAll()
        {
            List<TruongDTO> result = new List<TruongDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            
            sdr.Close();
            _conn.Close();

            return result;
        }

        public TruongDTO GetByMaTruong(string maTruong)
        {
            var result = new TruongDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaTruong, maTruong);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaTruong()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaTruong);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaTruong"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(TruongDTO tNew)
        {
            var query = string.Format(StrInsert, tNew.MaTruong, tNew.TenTruong, tNew.TenVietTat, tNew.LoaiTruong, tNew.GhiChu, tNew.NgayHieuLuc.ToString("yyyy-MM-dd"), tNew.TinhTrang, tNew.NuocNgoai);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(TruongDTO tUpdate)
        {
            var query = string.Format(StrUpdate, tUpdate.TenTruong, tUpdate.TenVietTat, tUpdate.LoaiTruong, tUpdate.NuocNgoai, tUpdate.GhiChu, tUpdate.MaTruong);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(TruongDTO tDel)
        {
            var query = string.Format(StrDelete, tDel.MaTruong);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
