﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class HopDongDAC
    {
        const string StrSelectByNhanVien = "select hd.*, lhd.TenLoaiHD from HopDong hd left join LoaiHopDong lhd on hd.MaLoaiHD = lhd.MaLoaiHD where MaNV = '{0}' and hd.TinhTrang = 0";
        const string StrSelectAll = "select hd.*, lhd.TenLoaiHD from HopDong hd left join LoaiHopDong lhd on hd.MaLoaiHD = lhd.MaLoaiHD where hd.TinhTrang = 0";
        const string StrMaxMaHD = "SELECT MAX(MaHD) MaHD FROM HopDong";

        const string StrInsert = "INSERT INTO HopDong (MaHD,TenHD,MaLoaiHD,MaNV,NgayKy,NguoiKy,TuNgay,DenNgay,NoiDung,TinhTrang,LanThu,NgayHieuLuc) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', N'{5}', '{6}', '{7}', N'{8}','{9}', '{10}', '{11}')";
        const string StrUpdate = "UPDATE HopDong SET MaHD ='{0}', TenHD ='{1}', MaLoaiHD ='{2}', MaNV ='{3}', NgayKy ='{4}', NguoiKy =N'{5}', TuNgay = '{6}', DenNgay ='{7}', NoiDung =N'{8}', LanThu ='{10}' WHERE MaHD = '{9}'";
        const string StrDelete = "UPDATE HopDong SET TinhTrang = 1 WHERE MaHD = '{0}'";

        SqlConnection _conn = new SqlConnection();

        public List<HopDongDTO> LayDanhSachHopDongTheoNhanVien(string strMaNV)
        {
            List<HopDongDTO> listResult = new List<HopDongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while(sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<HopDongDTO>(sdr));
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<HopDongDTO> GetAll()
        {
            List<HopDongDTO> listResult = new List<HopDongDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<HopDongDTO>(sdr));
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public string GetMaxMaHD()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaHD);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaHD"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }


        public bool Save(HopDongDTO hdtNew)
        {
            // string ttbc = pkNew.ThuTuBaoCao != null ? pkNew.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrInsert, hdtNew.MaHD, hdtNew.TenHD, hdtNew.MaLoaiHD, hdtNew.MaNV, hdtNew.NgayKy, hdtNew.NguoiKy, hdtNew.TuNgay, hdtNew.DenNgay, hdtNew.NoiDung, hdtNew.TinhTrang, hdtNew.LanThu, hdtNew.NgayHieuLuc.ToString("yyyy-MM-dd"));

            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(HopDongDTO hdUpdate)
        {
            //  string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            //var query = string.Format(StrUpdate, dtUpdate.Nam + dtUpdate.Thang, dtUpdate.Thang, dtUpdate.Nam, dtUpdate.NgayGio, dtUpdate.Phong, dtUpdate.GhiChu, dtUpdate.NgayBatDau, dtUpdate.NgayKetThuc,dtUpdate.MaDot);
            //vdtoan update 230916
            var query = string.Format(StrUpdate, hdUpdate.MaHD, hdUpdate.TenHD, hdUpdate.MaLoaiHD, hdUpdate.MaNV, hdUpdate.NgayKy, hdUpdate.NguoiKy, hdUpdate.TuNgay, hdUpdate.DenNgay, hdUpdate.NoiDung, hdUpdate.MaHD, hdUpdate.LanThu);
           
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(HopDongDTO hdDel)
        {
            var query = string.Format(StrDelete, hdDel.MaHD);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
