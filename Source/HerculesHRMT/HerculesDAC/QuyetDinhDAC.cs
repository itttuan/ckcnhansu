﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuyetDinhDAC
    {
        const string StrSelectByNhanVien = "select qd.*, lqd.TenLoaiQD, nv.Ho + ' ' + nv.Ten as HoTenNguoiKy from QuyetDinh qd left join LoaiQuyetDinh lqd on qd.MaLoaiQD = lqd.MaLoaiQD left join NhanVien nv on qd.NguoiKy = nv.MaNV where qd.MaNV = '{0}' and qd.TinhTrang = 0";
        const string StrSelectAll = "select qd.*, lqd.TenLoaiQD from QuyetDinh qd left join LoaiQuyetDinh lqd on qd.MaLoaiQD = lqd.MaLoaiQD where qd.TinhTrang = 0";
        const string StrMaxMaHD = "SELECT MAX(MaHD) MaHD FROM HopDong";

        const string StrInsert = "INSERT INTO QuyetDinh (SoQD,MaNV,MaLoaiQD,Lan,NoiDungQD,NgayKy,NguoiKy,TuNgay,DenNgay,MaNgach,Bac,HeSo,TyLeVuotKhung,HeSoCV,PhuCapNhaGiao,PhuCapThamNien,HienTaiHuong,NgayHieuLuc,TinhTrang) VALUES ('{0}', '{1}', '{2}', '{3}', N'{4}', '{5}', '{6}', '{7}', '{8}','{9}', {10}, '{11}', {12}, {13}, {14}, {15}, {16}, '{17}', {18})";
        const string StrUpdate = "UPDATE QuyetDinh SET SoQD ='{0}', MaLoaiQD ='{1}', Lan ={2}, NoiDungQD =N'{3}', NguoiKy ='{4}', NgayKy =N'{5}', TuNgay ='{6}', DenNgay = '{7}', MaNgach ='{8}', Bac ={9}, HeSo = {10}, TyLeVuotKhung = {11}, HeSoCV = {12}, PhuCapNhaGiao = {13}, PhuCapThamNien = {14}, HienTaiHuong = '{15}' WHERE Id = '{16}'";
        const string StrDelete = "UPDATE QuyetDinh SET TinhTrang = 1 WHERE Id = '{0}'";
        SqlConnection _conn = new SqlConnection();

        public List<QuyetDinhDTO> LayDanhSachQuyetDinhTheoNhanVien(string strMaNV)
        {
            List<QuyetDinhDTO> listResult = new List<QuyetDinhDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(String.Format(StrSelectByNhanVien, strMaNV), _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO4<QuyetDinhDTO>(sdr));
          
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<QuyetDinhDTO> GetAll()
        {
            List<QuyetDinhDTO> listResult = new List<QuyetDinhDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);
            while (sdr.Read())
            {
                listResult.Add(Utils.MappingSQLDRToDTO2<QuyetDinhDTO>(sdr));
            }

            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public string GetMaxMaHD()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaHD);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaHD"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }


        public bool Save(QuyetDinhDTO qdtNew)
        {

            var query = string.Format(StrInsert, qdtNew.SoQD, qdtNew.MaNV, qdtNew.MaLoaiQD, qdtNew.Lan, qdtNew.NoiDungQD, qdtNew.NgayKy, qdtNew.NguoiKy, qdtNew.TuNgay, qdtNew.DenNgay, qdtNew.MaNgach, qdtNew.Bac, qdtNew.HeSo, qdtNew.TyLeVuotKhung, qdtNew.HeSoCV, qdtNew.PhuCapNhaGiao, qdtNew.PhuCapThamNien, qdtNew.HienTaiHuong ? "1" : "0", qdtNew.NgayHieuLuc.ToString("yyyy-MM-dd"), qdtNew.TinhTrang);

            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(QuyetDinhDTO qdUpdate)
        {

            var query = string.Format(StrUpdate, qdUpdate.SoQD, qdUpdate.MaLoaiQD, qdUpdate.Lan, qdUpdate.NoiDungQD, qdUpdate.NguoiKy, qdUpdate.NgayKy, qdUpdate.TuNgay, qdUpdate.DenNgay, qdUpdate.MaNgach, qdUpdate.Bac, qdUpdate.HeSo, qdUpdate.TyLeVuotKhung, qdUpdate.HeSoCV, qdUpdate.PhuCapNhaGiao, qdUpdate.PhuCapThamNien, qdUpdate.HienTaiHuong ? "1" : "0", qdUpdate.Id);

            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(QuyetDinhDTO qdDel)
        {
            var query = string.Format(StrDelete, qdDel.Id);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
   
    }
}
