﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;
//vdtoan add 230916
using System.Data.SqlTypes;
using System.Globalization;

namespace HerculesDAC
{
    public class DotTuyenDAC
    {
        //const string StrSelectAll = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 order by left(MaPhongKhoa,1) desc";
        //const string StrSelectByMaPhongKhoa = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 AND MaPhongKhoa = \'{0}\'";
        //const string StrInsert = "INSERT INTO PhongKhoa (MaPhongKhoa, TenPhongKhoa, TenVietTat, ThuTuBaoCao, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', {3}, N'{4}', '{5}', '{6}')";
        //const string StrUpdate = "UPDATE PhongKhoa SET TenPhongKhoa = N'{0}', TenVietTat = N'{1}', ThuTuBaoCao = {2}, GhiChu = N'{3}' WHERE MaPhongKhoa = '{4}'";
        //const string StrDelete = "UPDATE PhongKhoa SET TinhTrang = 1 WHERE MaLoaiTrinhDo = '{0}'";
        //const string StrMaxMaPhongKhoa = "SELECT MAX(MaPhongKhoa) MaPhongKhoa FROM PhongKhoa";
        const string StrInsert = "INSERT INTO DotTuyen (MaDot, Thang, Nam, NgayGio, Phong, GhiChu, NgayBatDau, NgayKetThuc, TinhTrang) VALUES ('{0}', '{1}', '{2}', '{3}', N'{4}', N'{5}', '{6}', '{7}', '{8}')";
        ////vdtoan update 220916 sửa kiểu ngày là datetime
        //const string StrInsert = "INSERT INTO DotTuyen (MaDot, Thang, Nam, NgayGio, Phong, GhiChu, NgayBatDau, NgayKetThuc, TinhTrang) VALUES ('{0}', '{1}', '{2}', {3}, N'{4}', N'{5}', {6}, {7}, '{8}')";
        const string StrSelectAll = "SELECT * FROM DotTuyen WHERE TinhTrang = 0 order by MaDot asc";
        const string StrUpdate = "UPDATE DotTuyen SET MaDot = '{0}', Thang ='{1}', Nam ='{2}', NgayGio ='{3}', Phong =N'{4}', GhiChu = N'{5}', NgayBatDau ='{6}', NgayKetThuc ='{7}' WHERE MaDot = '{8}'";
        const string StrDelete = "UPDATE DotTuyen SET TinhTrang = 1 WHERE MaDot = '{0}'";

        //vdtoan add 230916
        const string StrMaxMaDT = "SELECT MAX(MaDot) MaDot FROM DotTuyen";

        SqlConnection _conn = new SqlConnection();

        private DotTuyenDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            DotTuyenDTO dtDTO = Utils.MappingSQLDRToDTO4<DotTuyenDTO>(sdr);
            if (!sdr.IsDBNull(sdr.GetOrdinal("NgayGio")))
            {
                //dtDTO.NgayGio = ((DateTime)sdr["NgayGio"]).ToString("dd/MM/yyyy HH:mm:ss");
                //dtDTO.NgayGio = sdr["NgayGio"].ToString("dd/MM/yyyy HH:mm");
            }

            if (int.Parse(dtDTO.Thang) < 10 && int.Parse(dtDTO.Thang) > 0)
                dtDTO.Thang = "0" + dtDTO.Thang;
            //if (pkDTO.TenVietTat != null && pkDTO.TenVietTat.CompareTo("NULL") == 0)
            //    pkDTO.TenVietTat = string.Empty;

            //if (pkDTO.GhiChu != null && pkDTO.GhiChu.CompareTo("NULL") == 0)
            //    pkDTO.GhiChu = string.Empty;

            ////vdtoan add 161016
            //if (dtDTO.NgayBatDau != null && dtDTO.NgayBatDau != string.Empty && dtDTO.NgayBatDau != "")
            //{
            //    dtDTO.NgayBatDau = DateTime.ParseExact(dtDTO.NgayBatDau, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    dtDTO.NgayBatDau = string.Empty;
            //}

            return dtDTO;
        }

        public List<DotTuyenDTO> GetAll()
        {
            var result = new List<DotTuyenDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    var result = new PhongKhoaDTO();

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrSelectByMaPhongKhoa, maPhongKhoa);
        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    while (sdr.Read())
        //    {
        //        result = MappingSQLDRToDTO(sdr);
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        //public string GetMaxMaPhongKhoa()
        //{
        //    var result = string.Empty;

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrMaxMaPhongKhoa);

        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    if (sdr.Read())
        //    {
        //        result = sdr["MaPhongKhoa"].ToString();
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        public bool Save(DotTuyenDTO dtNew)
        {
           // string ttbc = pkNew.ThuTuBaoCao != null ? pkNew.ThuTuBaoCao.ToString() : "NULL";
           var query = string.Format(StrInsert, dtNew.MaDot,dtNew.Thang,dtNew.Nam,dtNew.NgayGio,dtNew.Phong,dtNew.GhiChu, dtNew.NgayBatDau, dtNew.NgayKetThuc, dtNew.TinhTrang);
           
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(DotTuyenDTO dtUpdate)
        {
          //  string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            //var query = string.Format(StrUpdate, dtUpdate.Nam + dtUpdate.Thang, dtUpdate.Thang, dtUpdate.Nam, dtUpdate.NgayGio, dtUpdate.Phong, dtUpdate.GhiChu, dtUpdate.NgayBatDau, dtUpdate.NgayKetThuc,dtUpdate.MaDot);
            //vdtoan update 230916
            var query = string.Format(StrUpdate, dtUpdate.MaDot, dtUpdate.Thang, dtUpdate.Nam, dtUpdate.NgayGio, dtUpdate.Phong, dtUpdate.GhiChu, dtUpdate.NgayBatDau, dtUpdate.NgayKetThuc, dtUpdate.MaDot);

            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(DotTuyenDTO dtDel)
        {
            var query = string.Format(StrDelete, dtDel.MaDot);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        //vdtoan add 230916
        public string GetMaxMaDT()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaDT);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaDot"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }
    }
}
