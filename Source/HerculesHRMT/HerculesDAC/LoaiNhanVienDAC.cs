﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiNhanVienDAC
    {
        const string StrSelectAll = "SELECT * FROM LoaiNhanVien WHERE TinhTrang = 0";
        const string SelectByMaLoaiNV = "SELECT * FROM LoaiNhanVien WHERE TinhTrang = 0 AND MaLoaiNV = \'{0}\'";
        const string StrInsert = "INSERT INTO LoaiNhanVien (MaLoaiNV, TenLoaiNV, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}')";
        const string StrUpdate = "UPDATE LoaiNhanVien SET TenLoaiNV = N'{0}', GhiChu = N'{1}' WHERE MaLoaiNV = '{2}'";
        const string StrDelete = "UPDATE LoaiNhanVien SET TinhTrang = 1 WHERE MaLoaiNV = '{0}'";
        const string StrMaxMaLoaiNV = "SELECT MAX(MaLoaiNV) MaLoaiNV FROM LoaiNhanVien";
        SqlConnection _conn = new SqlConnection();

        private LoaiNhanVienDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            LoaiNhanVienDTO nvDTO = Utils.MappingSQLDRToDTO<LoaiNhanVienDTO>(sdr);

            if (nvDTO.GhiChu != null && nvDTO.GhiChu.CompareTo("NULL") == 0)
                nvDTO.GhiChu = string.Empty;

            return nvDTO;
        }

        public List<LoaiNhanVienDTO> GetAll()
        {
            List<LoaiNhanVienDTO> result = new List<LoaiNhanVienDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();

            return result;
        }

        public LoaiNhanVienDTO GetByMaLoaiQuyetDinh(string maNV)
        {
            var result = new LoaiNhanVienDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaLoaiNV, maNV);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiNhanVien()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoaiNV);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaLoaiNV"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(LoaiNhanVienDTO nvNew)
        {
            var query = string.Format(StrInsert, nvNew.MaLoaiNV, nvNew.TenLoaiNV, nvNew.GhiChu, nvNew.NgayHieuLuc.ToString("yyyy-MM-dd"), nvNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(LoaiNhanVienDTO nvUpdate)
        {
            var query = string.Format(StrUpdate, nvUpdate.TenLoaiNV, nvUpdate.GhiChu, nvUpdate.MaLoaiNV);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(LoaiNhanVienDTO nvDel)
        {
            var query = string.Format(StrDelete, nvDel.MaLoaiNV);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
