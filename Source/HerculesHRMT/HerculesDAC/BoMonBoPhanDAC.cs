﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class BoMonBoPhanDAC
    {
        const string StrSelectAll = "SELECT BM.*, PK.TenPhongKhoa FROM BoMonBoPhan BM, PhongKhoa PK WHERE BM.TinhTrang = 0 and BM.MaPK = PK.MaPhongKhoa";
        //const string StrSelectAll = "SELECT * FROM BoMonBoPhan BM WHERE BM.TinhTrang = \'false\'";
        const string StrSelectByMaPk = "SELECT * FROM BoMonBoPhan BM, PhongKhoa PK WHERE BM.TinhTrang = 0 and BM.MaPK = PK.MaPhongKhoa and BM.MaPK = '{0}'";
        const string StrSelectByMaBmBp = "SELECT * FROM BoMonBoPhan BM WHERE BM.TinhTrang = 0 AND MaBMBP = \'{0}\'";
        const string StrInsert = "INSERT INTO BoMonBoPhan (MaBMBP, MaPK, TenBMBP, TenVietTat, ThuTuBaoCao, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', '{1}', N'{2}', N'{3}', {4}, N'{5}', '{6}', '{7}')";
        const string StrUpdate = "UPDATE BoMonBoPhan SET MaPK = '{0}', TenBMBP = N'{1}', TenVietTat = N'{2}', ThuTuBaoCao = {3}, GhiChu = N'{4}' WHERE MaBMBP = '{5}'";
        const string StrDelete = "UPDATE BoMonBoPhan SET TinhTrang = 1 WHERE MaBMBP = '{0}'";
        const string StrMaxMaBMBP = "SELECT MAX(MaBMBP) MaBMBP FROM BoMonBoPhan";

        SqlConnection _conn = new SqlConnection();

        private BoMonBoPhanDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            BoMonBoPhanDTO bmbpDTO = Utils.MappingSQLDRToDTO<BoMonBoPhanDTO>(sdr);
            if (bmbpDTO.TenVietTat != null && bmbpDTO.TenVietTat.CompareTo("NULL") == 0)
                bmbpDTO.TenVietTat = string.Empty;

            if (bmbpDTO.GhiChu != null && bmbpDTO.GhiChu.CompareTo("NULL") == 0)
                bmbpDTO.GhiChu = string.Empty;

            return bmbpDTO;
        }

        public List<BoMonBoPhanDTO> GetAll()
        {
            var result = new List<BoMonBoPhanDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public List<BoMonBoPhanDTO> GetListByMaPhongKhoa(string maPhongKhoa)
        {
            var result = new List<BoMonBoPhanDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectByMaPk, maPhongKhoa);
            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public BoMonBoPhanDTO GetByMaBmBp(string maBmBp)
        {
            var result = new BoMonBoPhanDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrSelectByMaBmBp, maBmBp);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaBMBP()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaBMBP);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaBMBP"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(BoMonBoPhanDTO bmbpNew)
        {
            string ttbc = bmbpNew.ThuTuBaoCao != null ? bmbpNew.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrInsert, bmbpNew.MaBMBP, bmbpNew.MaPK, bmbpNew.TenBMBP, bmbpNew.TenVietTat, ttbc, bmbpNew.GhiChu, bmbpNew.NgayHieuLuc.ToString("yyyy-MM-dd"), bmbpNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(BoMonBoPhanDTO bmbpUpdate)
        {
            string ttbc = bmbpUpdate.ThuTuBaoCao != null ? bmbpUpdate.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrUpdate, bmbpUpdate.MaPK,bmbpUpdate.TenBMBP, bmbpUpdate.TenVietTat, ttbc, bmbpUpdate.GhiChu, bmbpUpdate.MaBMBP);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(BoMonBoPhanDTO bmbpDel)
        {
            var query = string.Format(StrDelete, bmbpDel.MaBMBP);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
