﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class KhenThuongKyLuatDAC
    {
        const string StrSelectKhenThuongByNhanVien = "select * from ThiDuaKhenThuong where MaNV = '{0}' and TinhTrang = 0";
        const string StrSelectKyLuatByNhanVien = "select * from KyLuat where MaNV = '{0}' and TinhTrang = 0";
        const string strInsertKhenThuongNhanVien = "INSERT INTO ThiDuaKhenThuong([MaNV],[Ngay],[NoiDung],[CapQuyetDinh],[NgayHieuLuc],[TinhTrang],[SoQuyetDinh])VALUES('";
        const string strInsertKyLuatNhanVien = "INSERT INTO KyLuat([MaNV],[NgayKL],[LyDoKL],[CapQuyetDinh],[NgayHieuLuc],[TinhTrang],[SoQuyetDinh]) VALUES ('";
        
        SqlConnection _conn = new SqlConnection();
        public List<KhenThuongKyLuatDto> LayDanhSachKhenThuongNhanVien(string maNV)
        {
            List<KhenThuongKyLuatDto> listResult = new List<KhenThuongKyLuatDto>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(string.Format(StrSelectKhenThuongByNhanVien, maNV), _conn);
            while (sdr.Read())
            {
                KhenThuongKyLuatDto kt = Utils.MappingSQLDRToDTO3<KhenThuongKyLuatDto>(sdr);
                kt.IsKhenThuong = true;
                if (sdr["Ngay"] != null)
                {
                    kt.ThoiGianHieuLuc = sdr["Ngay"].ToString();
                }
                listResult.Add(kt);
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public List<KhenThuongKyLuatDto> LayDanhSachKyLuatNhanVien(string maNV)
        {
            List<KhenThuongKyLuatDto> listResult = new List<KhenThuongKyLuatDto>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(string.Format(StrSelectKyLuatByNhanVien, maNV), _conn);
            while (sdr.Read())
            {
                KhenThuongKyLuatDto kl = Utils.MappingSQLDRToDTO3<KhenThuongKyLuatDto>(sdr);
                kl.IsKhenThuong = false;
                if (sdr["NgayKL"] != null)
                {
                    kl.ThoiGianHieuLuc = sdr["NgayKL"].ToString();
                }
                listResult.Add(kl);
            }
            sdr.Close();
            _conn.Close();
            return listResult;
        }

        public bool Save(KhenThuongKyLuatDto ktklDTO)
        {
            string query;
            if (ktklDTO.IsKhenThuong)
            {
                query = strInsertKhenThuongNhanVien;
            }
            else
            {
                query = strInsertKyLuatNhanVien;
            }
            query = query + ktklDTO.MaNv + "','" + ktklDTO.ThoiGianHieuLuc + "',"
                + (String.IsNullOrEmpty(ktklDTO.NoiDung) ? "null," : ("N'" + ktklDTO.NoiDung + "',"))
                + (String.IsNullOrEmpty(ktklDTO.CapQuyetDinh) ? "null," : ("N'" + ktklDTO.CapQuyetDinh + "',"))
                + "getdate(), 0,N'" + ktklDTO.SoQuyetDinh + "')";
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Update(KhenThuongKyLuatDto ktklDTO)
        {
            string query = "";
            if (ktklDTO.IsKhenThuong)
            {
                query = "UPDATE ThiDuaKhenThuong SET SoQuyetDinh = N'" + ktklDTO.SoQuyetDinh + "', Ngay = "
                    + "'" + ktklDTO.ThoiGianHieuLuc+ "', CapQuyetDinh = "
                    + (String.IsNullOrEmpty(ktklDTO.CapQuyetDinh) ? "null," : ("N'" + ktklDTO.CapQuyetDinh + "',"))
                    + "NoiDung = " + (String.IsNullOrEmpty(ktklDTO.NoiDung) ? "null " : ("N'" + ktklDTO.NoiDung + "' "));
            }
            else
            {
                query = "UPDATE KyLuat SET SoQuyetDinh = N'" + ktklDTO.SoQuyetDinh + "', NgayKL = "
                    + "'" + ktklDTO.ThoiGianHieuLuc + "', CapQuyetDinh = "
                    + (String.IsNullOrEmpty(ktklDTO.CapQuyetDinh) ? "null," : ("N'" + ktklDTO.CapQuyetDinh + "',"))
                    + "LyDoKL = " + (String.IsNullOrEmpty(ktklDTO.NoiDung) ? "null " : ("N'" + ktklDTO.NoiDung + "' "));
            }
            query += "where Id = " + ktklDTO.Id;
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }

        public bool Delete(KhenThuongKyLuatDto ktklDTO)
        {
            string query = "";
            if (ktklDTO.IsKhenThuong)
            {
                query = "Update ThiDuaKhenThuong SET TinhTrang = 1 where Id = " + ktklDTO.Id;
            }
            else
            {
                query = "Update KyLuat SET TinhTrang = 1 where Id = " + ktklDTO.Id;
            }
            try
            {
                _conn = DataProvider.TaoKetNoiCSDL();
                int kq = DataProvider.ThucThiSQL(query, _conn);
                _conn.Close();
                if (kq != 0)
                    return true;
            }
            catch
            {

            }
            return false;
        }
    }
}
