﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesDAC
{
    public static class SqlQueries
    {
        #region SQL QUERIES

        public const string SqlInsertNhanVien =
            @"INSERT INTO NhanVien (MaNV, Ho, Ten, TenThuongDung, TenGoiKhac, NgaySinh, NoiSinh, GioiTinh, MaDanToc, MaTonGiao, QuocTich, DienThoai, MaChucVu, CMND, NgayCapCMND, NoiCapCMND, DiaChiHKTT, MaQuanHuyenHKTT, MaTinhThanhHKTT, DiaChiHT, MaQuanHuyenHT, MaTinhThanhHT, ThanhPhanGiaDinhXuatThan, NgheNghiepTuyenDung, HangNhanVien, BacNhanVien, MaNgach, Email, TinhTrangHonNhan, NgayVaoLam, NgayTuyenDung, NgayHieuLuc, TinhTrang, TinhTrangLamViec, TrinhDoVanHoa, QueQuan, NgayVaoDCSVN, NgayChinhThucVaoDCSVN, NgayNhapNgu, NgayXuatNgu, QuanHamCaoNhat, TinhTrangSucKhoe, ChieuCao, CanNang, NhomMau, ThuongBinhHang, GiaDinhChinhSach, TuNhanXet)
                VALUES (@maNV, @ho, @ten, @tenThuongDung, @tenGoiKhac, Convert(Date, @ngaySinh, 101), @noiSinh, @gioiTinh, @maDanToc, @maTonGiao, @quocTich, @dienThoai, @maChucVu, @CMND, Convert(Date, @ngayCapCMND, 101), @noiCapCMND, @diaChiHKTT, @maQuanHuyenHKTT, @maTinhThanhHKTT, @diaChiHT, @maQuanHuyenHT, @maTinhThanhHT, @thanhPhanGiaDinhXuatThan, @ngheNghiepTuyenDung, @hangNhanVien, @bacNhanVien, @maNgach, @email, @tinhTrangHonNhan, Convert(Date, @ngayVaoLam, 101), Convert(Date, @ngayTuyenDung, 101), Convert(Date, @ngayHieuLuc, 101), @tinhTrang, @tinhTrangLamViec, @trinhDoVanHoa, @queQuan, Convert(Date, @ngayVaoDCSVN, 101), Convert(Date, @ngayChinhThucVaoDCSVN, 101), Convert(Date, @ngayNhapNgu, 101), Convert(Date, @ngayXuatNgu, 101), @quanHamCaoNhat, @tinhTrangSucKhoe, @chieuCao, @canNang, @nhomMau, @thuongBinhHang, @giaDinhChinhSach, @tuNhanXet);
              SELECT CAST(scope_identity() AS int)";

        public const string SqlInsertQuaTrinhCongTac =
            @"INSERT INTO QuaTrinhCongTac (MaNV, NoiCongTac, NgheNghiep, ThanhTich, TuNgay, DenNgay, GhiChu, NgayHieuLuc, TinhTrang, GiaiDoan) 
                VALUES (@maNV, @noiCongTac, @ngheNghiep, @thanhTich, Convert(Date, @tuNgay, 101), Convert(Date, @denNgay, 101), @ghiChu, Convert(Date, @ngayHieuLuc, 101), @tinhTrang, @giaiDoan)";

        public const string SqlInsertTrinhDo =
            @"INSERT INTO TrinhDo (MaBCCC, MaNV, MaTruong, MaLoaiBCCC, /*MaNganh, MaChuyenNganh,*/ MaLoaiTrinhDo, MaHeDaoTao, ThoiGianDaoTaoTu, ThoiGianDaoTaoDen, NgayTotNghiep, LoaiTotNghiep, NgayCap, MoTaThem, IsBangCap, MaCoSoDaoTao, NgayHieuLuc, TinhTrang) 
                VALUES (@maBCCC, @maNV, @maTruong, @maLoaiBCCC, /*@maNganh, @maChuyenNganh,*/ @maLoaiTrinhDo, @maHeDaoTao, @thoiGianDaoTaoTu, @thoiGianDaoTaoDen, Convert(Date, @ngayTotNghiep, 101), @loaiTotNghiep, Convert(Date, @ngayCap, 101), @moTaThem, @isBangCap, @maCoSoDaoTao, Convert(Date, @ngayHieuLuc, 101), @tinhTrang)";

        public const string SqlInsertKhenThuong = 
            @"INSERT INTO ThiDuaKhenThuong (MaNV, Ngay, Loai, NoiDung, HinhThuc, CapQuyetDinh, SoQuyetDinh, NgayHieuLuc, TinhTrang) 
                VALUES (@maNV, Convert(Date, @ngay, 101), @loai, @noiDung, @hinhThuc, @capQuyetDinh, @soQuyetDinh, Convert(Date, @ngayHieuLuc, 101), @tinhTrang)";

        public const string SqlInsertKyLuat = 
            @"INSERT INTO KyLuat (MaNV, NgayKL, LoaiKL, LyDoKL, HinhThucKL, CapQuyetDinh, SoQuyetDinh, NgayHieuLuc, TinhTrang ) 
                VALUES (@maNV, Convert(Date, @ngayKL, 101), @loaiKL, @lyDoKL, @hinhThucKL, @capQuyetDinh, @soQuyetDinh, Convert(Date, @ngayHieuLuc, 101), @tinhTrang)";

        public const string SqlInsertThanNhan =
            @"INSERT INTO ThanNhan (MaNV, HoTen, NgaySinh, NoiSinh, MaQuanHe, DacDiemLichSu, NgheNghiep, GhiChu, QuanHuyen, TinhThanh, NgayHieuLuc, TinhTrang) 
                VALUES (@maNV, @hoTen, Convert(Date, @ngaySinh, 101), @noiSinh, @maQuanHe, @dacDiemLichSu, @ngheNghiep, @ghiChu, @quanHuyen, @tinhThanh, Convert(Date, @ngayHieuLuc, 101), @tinhTrang)";

        public const string SqlInsertHinhAnh =
            @"INSERT INTO HinhAnh (MaNV, HinhAnh, GhiChu, NgayHieuLuc, TinhTrang)
                VALUES (@maNV, @hinhAnh, @ghiChu, getDate(), @tinhTrang)";

        #endregion
    }
}
