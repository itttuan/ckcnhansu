﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiTrinhDoDAC
    {
        const string StrSelectAll = "SELECT * FROM LoaiTrinhDo LTD WHERE LTD.TinhTrang = 0";
        const string SelectByMaLoaiTrinhDo = "SELECT * FROM LoaiTrinhDo LTD WHERE LTD.TinhTrang = 0 AND MaLoaiTrinhDo = {0}";
        const string StrInsert = "INSERT INTO LoaiTrinhDo (MaLoaiTrinhDo, TenLoaiTrinhDo, TenVietTat, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE LoaiTrinhDo SET TenLoaiTrinhDo = N'{0}', TenVietTat = N'{1}', GhiChu = N'{2}' WHERE MaLoaiTrinhDo = '{3}'";
        const string StrDelete = "UPDATE LoaiTrinhDo SET TinhTrang = 1 WHERE MaLoaiTrinhDo = '{0}'";
        const string StrMaxMaLoaiTrinhDo = "SELECT MAX(MaLoaiTrinhDo) MaLoaiTrinhDo FROM LoaiTrinhDo";

        SqlConnection _conn = new SqlConnection();

        private LoaiTrinhDoDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            LoaiTrinhDoDTO tdDTO = Utils.MappingSQLDRToDTO<LoaiTrinhDoDTO>(sdr);
            if (tdDTO.TenVietTat != null && tdDTO.TenVietTat.CompareTo("NULL") == 0)
                tdDTO.TenVietTat = string.Empty;

            if (tdDTO.GhiChu != null && tdDTO.GhiChu.CompareTo("NULL") == 0)
                tdDTO.GhiChu = string.Empty;

            return tdDTO;
        }

        public List<LoaiTrinhDoDTO> GetAll()
        {
            var result = new List<LoaiTrinhDoDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public LoaiTrinhDoDTO GetByMaLoaiTrinhDo(string maLoaiTrinhDo)
        {
            var result = new LoaiTrinhDoDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaLoaiTrinhDo, maLoaiTrinhDo);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiTrinhDo()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoaiTrinhDo);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaLoaiTrinhDo"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(LoaiTrinhDoDTO tdNew)
        {
            var query = string.Format(StrInsert, tdNew.MaLoaiTrinhDo, tdNew.TenLoaiTrinhDo, tdNew.TenVietTat, tdNew.GhiChu, tdNew.NgayHieuLuc.ToString("yyyy-MM-dd"), tdNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(LoaiTrinhDoDTO tdUpdate)
        {
            var query = string.Format(StrUpdate, tdUpdate.TenLoaiTrinhDo, tdUpdate.TenVietTat, tdUpdate.GhiChu, tdUpdate.MaLoaiTrinhDo);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(LoaiTrinhDoDTO tdDel)
        {
            var query = string.Format(StrDelete, tdDel.MaLoaiTrinhDo);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
