﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using System.Data.SqlClient;

namespace HerculesDAC
{
    public class NhacNhoDAC
    {
        //const string StrSelectAll = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 order by left(MaPhongKhoa,1) desc";
        //const string StrSelectByMaPhongKhoa = "SELECT * FROM PhongKhoa WHERE TinhTrang = 0 AND MaPhongKhoa = \'{0}\'";
        //const string StrInsert = "INSERT INTO PhongKhoa (MaPhongKhoa, TenPhongKhoa, TenVietTat, ThuTuBaoCao, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', {3}, N'{4}', '{5}', '{6}')";
        //const string StrUpdate = "UPDATE PhongKhoa SET TenPhongKhoa = N'{0}', TenVietTat = N'{1}', ThuTuBaoCao = {2}, GhiChu = N'{3}' WHERE MaPhongKhoa = '{4}'";
        //const string StrDelete = "UPDATE PhongKhoa SET TinhTrang = 1 WHERE MaLoaiTrinhDo = '{0}'";
        //const string StrMaxMaPhongKhoa = "SELECT MAX(MaPhongKhoa) MaPhongKhoa FROM PhongKhoa";
        const string StrInsert = "INSERT INTO NhacNho (MaLoaiNhacNho, SoQuyetDinh, ThoiGianNhac, NgayHetHan, NoiDung, GhiChu, NgayHieuLuc,TinhTrang) VALUES ('{0}', '{1}', '{2}', '{3}', N'{4}', N'{5}', '{6}', '{7}')";
        const string StrSelectAll = "SELECT * FROM NhacNho WHERE TinhTrang = 0 order by Id desc";
        const string StrUpdate = "UPDATE NhacNho SET MaLoaiNhacNho = '{0}', SoQuyetDinh ='{1}', ThoiGianNhac ='{2}', NgayHetHan ='{3}', NoiDung =N'{4}', GhiChu = N'{5}', NgayHieuLuc ='{6}' WHERE Id = '{7}'";
        const string StrDelete = "UPDATE NhacNho SET TinhTrang = 1 WHERE Id = '{0}'";
        const string StrMaxId = "  SELECT MAX(ID) ID FROM NhacNho";
        const string StrSelectAllNhacNho = "SELECT * FROM NhacNho order by TinhTrang asc , id desc";

        const string StrChuyenTrangThaiNhacNho = "UPDATE NhacNho SET TinhTrang = '{0}' WHERE Id = '{1}'";
        const string StrSelectNgayHetHan = "SELECT * FROM NhacNho WHERE NgayHetHan = '{0}' order by Id desc";

        SqlConnection _conn = new SqlConnection();

        private NhacNhoDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            NhacNhoDTO nnDTO = Utils.MappingSQLDRToDTO4<NhacNhoDTO>(sdr);
            //if (!sdr.IsDBNull(sdr.GetOrdinal("NgayGio")))
            //{
            //    dtDTO.NgayGio = ((DateTime)sdr["NgayGio"]).ToString("dd/MM/yyyy HH:mm:ss");
            //}

            //if (int.Parse(dtDTO.Thang) < 10 && int.Parse(dtDTO.Thang) > 0)
            //    dtDTO.Thang = "0" + dtDTO.Thang;
            //if (pkDTO.TenVietTat != null && pkDTO.TenVietTat.CompareTo("NULL") == 0)
            //    pkDTO.TenVietTat = string.Empty;

            //if (pkDTO.GhiChu != null && pkDTO.GhiChu.CompareTo("NULL") == 0)
            //    pkDTO.GhiChu = string.Empty;

            return nnDTO;
        }

        public List<NhacNhoDTO> GetAll()
        {
            var result = new List<NhacNhoDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public List<NhacNhoDTO> GetAllNhacNho()
        {
            var result = new List<NhacNhoDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAllNhacNho, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }
        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    var result = new PhongKhoaDTO();

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrSelectByMaPhongKhoa, maPhongKhoa);
        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    while (sdr.Read())
        //    {
        //        result = MappingSQLDRToDTO(sdr);
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        //public string GetMaxMaPhongKhoa()
        //{
        //    var result = string.Empty;

        //    _conn = DataProvider.TaoKetNoiCSDL();
        //    var query = string.Format(StrMaxMaPhongKhoa);

        //    var sdr = DataProvider.TruyVanDuLieu(query, _conn);

        //    if (sdr.Read())
        //    {
        //        result = sdr["MaPhongKhoa"].ToString();
        //    }

        //    sdr.Close();
        //    _conn.Close();

        //    return result;
        //}

        public bool Save(NhacNhoDTO nnNew)
        {
            // string ttbc = pkNew.ThuTuBaoCao != null ? pkNew.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrInsert, nnNew.MaLoaiNhacNho, nnNew.SoQuyetDinh, nnNew.ThoiGianNhac, nnNew.NgayHetHan, nnNew.NoiDung, nnNew.GhiChu, nnNew.NgayHieuLuc, nnNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(NhacNhoDTO nnUpdate)
        {
            //  string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrUpdate, nnUpdate.MaLoaiNhacNho, nnUpdate.SoQuyetDinh, nnUpdate.ThoiGianNhac, nnUpdate.NgayHetHan, nnUpdate.NoiDung, nnUpdate.GhiChu, nnUpdate.NgayHieuLuc, nnUpdate.Id);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(NhacNhoDTO nnDel)
        {
            var query = string.Format(StrDelete, nnDel.Id);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public string GetMaxId()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxId);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["ID"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool ChuyenTrangThaiNhacNho(NhacNhoDTO nnUpdate)
        {
            //  string ttbc = pkUpdate.ThuTuBaoCao != null ? pkUpdate.ThuTuBaoCao.ToString() : "NULL";
            var query = string.Format(StrChuyenTrangThaiNhacNho, (nnUpdate.TinhTrang == 0)? 1: 0, nnUpdate.Id);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public List<NhacNhoDTO> TimNhacNhoTheoNgayHetHan(string sNgayHetHan)
        {
            var result = new List<NhacNhoDTO>();
            var query = string.Format(StrSelectNgayHetHan, sNgayHetHan);
            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }
    }
}
