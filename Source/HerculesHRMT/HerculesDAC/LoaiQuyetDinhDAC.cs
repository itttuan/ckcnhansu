﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class LoaiQuyetDinhDAC
    {
        const string StrSelectAll = "SELECT * FROM LoaiQuyetDinh WHERE TinhTrang = 0";
        const string SelectByMaLoaiQD = "SELECT * FROM LoaiQuyetDinh WHERE TinhTrang = 0 AND MaLoaiQD = \'{0}\'";
        const string StrInsert = "INSERT INTO LoaiQuyetDinh (MaLoaiQD, TenLoaiQD, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}')";
        const string StrUpdate = "UPDATE LoaiQuyetDinh SET TenLoaiQD = N'{0}', GhiChu = N'{1}' WHERE MaLoaiQD = '{2}'";
        const string StrDelete = "UPDATE LoaiQuyetDinh SET TinhTrang = 1 WHERE MaLoaiQD = '{0}'";
        const string StrMaxMaLoaiQD = "SELECT MAX(MaLoaiQD) MaLoaiQD FROM LoaiQuyetDinh";
        SqlConnection _conn = new SqlConnection();

        private LoaiQuyetDinhDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            LoaiQuyetDinhDTO qdDTO = Utils.MappingSQLDRToDTO<LoaiQuyetDinhDTO>(sdr);

            if (qdDTO.GhiChu != null && qdDTO.GhiChu.CompareTo("NULL") == 0)
                qdDTO.GhiChu = string.Empty;

            return qdDTO;
        }

        public List<LoaiQuyetDinhDTO> GetAll()
        {
            List<LoaiQuyetDinhDTO> result = new List<LoaiQuyetDinhDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();

            return result;
        }

        public LoaiQuyetDinhDTO GetByMaLoaiQuyetDinh(string maQD)
        {
            var result = new LoaiQuyetDinhDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaLoaiQD, maQD);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiQuyetDinh()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoaiQD);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaLoaiQD"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(LoaiQuyetDinhDTO qdNew)
        {
            var query = string.Format(StrInsert, qdNew.MaLoaiQD, qdNew.TenLoaiQD, qdNew.GhiChu, qdNew.NgayHieuLuc.ToString("yyyy-MM-dd"), qdNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(LoaiQuyetDinhDTO qdUpdate)
        {
            var query = string.Format(StrUpdate, qdUpdate.TenLoaiQD, qdUpdate.GhiChu, qdUpdate.MaLoaiQD);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(LoaiQuyetDinhDTO qdDel)
        {
            var query = string.Format(StrDelete, qdDel.MaLoaiQD);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
