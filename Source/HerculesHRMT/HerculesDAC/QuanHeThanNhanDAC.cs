﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HerculesDTO;

namespace HerculesDAC
{
    public class QuanHeThanNhanDAC
    {
        const string StrSelectAll = "SELECT * FROM QuanHeThanNhan QHTN WHERE QHTN.TinhTrang = 0";
        const string SelectByMaQuanHe = "SELECT * FROM QuanHeThanNhan QHTN WHERE QHTN.TinhTrang = 0 AND QHTN.MaQuanHe = \'{0}\'";
        const string StrInsert = "INSERT INTO QuanHeThanNhan (MaQuanHe, TenQuanHe, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}')";
        const string StrUpdate = "UPDATE QuanHeThanNhan SET TenQuanHe = N'{0}', GhiChu = N'{1}' WHERE MaQuanHe = '{2}'";
        const string StrDelete = "UPDATE QuanHeThanNhan SET TinhTrang = 1 WHERE MaQuanHe = '{0}'";
        const string StrMaxMaLoaiQuanHe = "SELECT MAX(MaQuanHe) MaQuanHe FROM QuanHeThanNhan";

        SqlConnection _conn = new SqlConnection();

        private QuanHeThanNhanDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            QuanHeThanNhanDTO tnDTO = Utils.MappingSQLDRToDTO<QuanHeThanNhanDTO>(sdr);

            if (tnDTO.GhiChu != null && tnDTO.GhiChu.CompareTo("NULL") == 0)
                tnDTO.GhiChu = string.Empty;

            return tnDTO;
        }

        public List<QuanHeThanNhanDTO> GetAll()
        {
            List<QuanHeThanNhanDTO> result = new List<QuanHeThanNhanDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            SqlDataReader sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }
            sdr.Close();
            _conn.Close();
            return result;
        }

        public QuanHeThanNhanDTO GetByMaQuanHe(string maQuanHe)
        {
            var result = new QuanHeThanNhanDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaQuanHe, maQuanHe);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaLoaiQuanHe()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaLoaiQuanHe);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaQuanHe"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(QuanHeThanNhanDTO qhNew)
        {
            var query = string.Format(StrInsert, qhNew.MaQuanHe, qhNew.TenQuanHe, qhNew.GhiChu, qhNew.NgayHieuLuc.ToString("yyyy-MM-dd"), qhNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(QuanHeThanNhanDTO qhUpdate)
        {
            var query = string.Format(StrUpdate, qhUpdate.TenQuanHe, qhUpdate.GhiChu, qhUpdate.MaQuanHe);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(QuanHeThanNhanDTO qhDel)
        {
            var query = string.Format(StrDelete, qhDel.MaQuanHe);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
