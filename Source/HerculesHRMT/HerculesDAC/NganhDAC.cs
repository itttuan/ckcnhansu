﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;

namespace HerculesDAC
{
    public class NganhDAC
    {
        const string StrSelectAll = "SELECT * FROM Nganh ng WHERE ng.TinhTrang = 0";
        const string SelectByMaNganh = "SELECT * FROM Nganh ng WHERE Trg.TinhTrang = 0 AND ng.MaNganh = \'{0}\'";
        const string StrInsert = "INSERT INTO Nganh (MaNganh, TenNganh, TenVietTat, GhiChu, NgayHieuLuc, TinhTrang) VALUES ('{0}', N'{1}', N'{2}', N'{3}', '{4}', '{5}')";
        const string StrUpdate = "UPDATE Nganh SET TenNganh = N'{0}', TenVietTat = N'{1}', GhiChu = N'{2}' WHERE MaNganh = '{3}'";
        const string StrDelete = "UPDATE Nganh SET TinhTrang = 1 WHERE MaNganh = '{0}'";
        const string StrMaxMaNganh = "SELECT MAX(MaNganh) MaNganh FROM Nganh";

        SqlConnection _conn = new SqlConnection();

        private NganhDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            NganhDTO ngDTO = Utils.MappingSQLDRToDTO<NganhDTO>(sdr);
            if (ngDTO.TenVietTat != null && ngDTO.TenVietTat.CompareTo("NULL") == 0)
                ngDTO.TenVietTat = string.Empty;

            if (ngDTO.GhiChu != null && ngDTO.GhiChu.CompareTo("NULL") == 0)
                ngDTO.GhiChu = string.Empty;
            
            return ngDTO;
        }

        public List<NganhDTO> GetAll()
        {
            List<NganhDTO> result = new List<NganhDTO>();

            _conn = DataProvider.TaoKetNoiCSDL();
            var sdr = DataProvider.TruyVanDuLieu(StrSelectAll, _conn);

            while (sdr.Read())
            {
                result.Add(MappingSQLDRToDTO(sdr));
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public NganhDTO GetByMaNganh(string maNganh)
        {
            var result = new NganhDTO();

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(SelectByMaNganh, maNganh);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            while (sdr.Read())
            {
                result = MappingSQLDRToDTO(sdr);
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public string GetMaxMaNganh()
        {
            var result = string.Empty;

            _conn = DataProvider.TaoKetNoiCSDL();
            var query = string.Format(StrMaxMaNganh);

            var sdr = DataProvider.TruyVanDuLieu(query, _conn);

            if (sdr.Read())
            {
                result = sdr["MaNganh"].ToString();
            }

            sdr.Close();
            _conn.Close();

            return result;
        }

        public bool Save(NganhDTO ngNew)
        {
            var query = string.Format(StrInsert, ngNew.MaNganh, ngNew.TenNganh, ngNew.TenVietTat, ngNew.GhiChu, ngNew.NgayHieuLuc.ToString("yyyy-MM-dd"), ngNew.TinhTrang);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }

        public bool Update(NganhDTO ngUpdate)
        {
            var query = string.Format(StrUpdate, ngUpdate.TenNganh, ngUpdate.TenVietTat, ngUpdate.GhiChu, ngUpdate.MaNganh);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();

            return results > 0;
        }

        public bool Delete(NganhDTO ngDel)
        {
            var query = string.Format(StrDelete, ngDel.MaNganh);
            _conn = DataProvider.TaoKetNoiCSDL();
            var results = DataProvider.ThucThiSQL(query, _conn);
            _conn.Close();
            return results > 0;
        }
    }
}
