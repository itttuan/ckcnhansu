﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HerculesDTO;
using System.Globalization;

namespace HerculesDAC
{
    public class ThongTinNhanSuDAC
    {
        const string StrSelect = "Select NV.Ho, NV.Ten, NV.GioiTinh, NV.ChucDanh, NV.NgaySinh, TDBD.ChuyenNganhDaoTao ChuyenMonBanDau, TDBD.MaTruong TruongBanDau,"
                                + "PK.TenVietTat DonVi, BM.TenVietTat BoMon, NV.NgayVaoLam TDungTu, nv.NgayTuyenDung TamTuyenTu, "
                                +"CASE WHEN TD.LoaiTrinhDoChuyenMon = 3 THEN '        TC' "
                                +"WHEN TD.LoaiTrinhDoChuyenMon = 4 THEN '       CD' "
                                + "WHEN TD.LoaiTrinhDoChuyenMon = 5 OR TD.LoaiTrinhDoChuyenMon = 6 OR TD.LoaiTrinhDoChuyenMon = 7 THEN '    DH' "
                                + "WHEN TD.LoaiTrinhDoChuyenMon = 8 OR TD.LoaiTrinhDoChuyenMon = 9 THEN '  ThS' "
                                +"WHEN TD.LoaiTrinhDoChuyenMon = 10 THEN 'TS' "
                                +"END AS TrinhDo, nv.KiemNhiem KiemNhiem "
                                +"from NhanVien NV "
                                +"left join BoMonBoPhan BM on NV.MaBMBP = BM.MaBMBP "
                                +"left join PhongKhoa PK on NV.MaPK = PK.MaPhongKhoa "
                                +"left join TrinhDo TD on NV.MaNV = TD.MaNV "
                                +"left join TrinhDo TDBD on NV.MaNV = TDBD.MaNV "
                                + "where NV.TinhTrang = 0 and (NV.TinhTrangLamViec = 0 or NV.TinhTrangLamViec = 1 or NV.TinhTrangLamViec = 2 or NV.TinhTrangLamViec = 3 or NV.TinhTrangLamViec is NULL) and (TD.LoaiDaoTao = 1 or TD.LoaiDaoTao is NULL) and (TD.TinhTrang = 0 or TD.TinhTrang is NULL) and TDBD.TrinhDoBanDau = 1 and (TDBD.TinhTrang = 0 or TDBD.TinhTrang is NULL) "
                                + "order by PK.ThuTuBaoCao, PK.TenPhongKhoa, BM.TenBMBP, NV.NgayVaoLam, NV.Ten";

        SqlConnection _conn = new SqlConnection();
        private DateTime timeGetDate;
        List<ThongTinNhanSuDTO> results;

        private ThongTinNhanSuDTO MappingSQLDRToDTO(SqlDataReader sdr)
        {
            ThongTinNhanSuDTO nsDTO = Utils.MappingSQLDRToDTO<ThongTinNhanSuDTO>(sdr);

            if (nsDTO.TDungTu != null)
            {
                if (DateTime.ParseExact(nsDTO.TDungTu, "yyyy-MM-dd", CultureInfo.InvariantCulture) > timeGetDate)
                {
                    nsDTO.Loai = "TT";

                    //vdtoan update 011116
                    if (nsDTO.TamTuyenTu != null)
                    {
                        nsDTO.TamTuyenTu = DateTime.ParseExact(nsDTO.TamTuyenTu, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd/MM/yy");
                    }
                }
                else
                    nsDTO.TamTuyenTu = string.Empty;

                nsDTO.TDungTu = DateTime.ParseExact(nsDTO.TDungTu, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd/MM/yy");
            }

            if (nsDTO.NgaySinh != null)
            {
                
                    nsDTO.NgaySinh = DateTime.ParseExact(nsDTO.NgaySinh, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd/MM/yy");
               
                                
            }
                else
                {
                    nsDTO.NgaySinh = string.Empty;
                }
                            

            if (nsDTO.GioiTinh)
                nsDTO.GioiTinhNu = 'x';

            if (results.Count == 0 || nsDTO.DonVi != results[results.Count - 1].DonVi)
            {
                nsDTO.STT_DonVi = 1;
                nsDTO.DonViIn = nsDTO.DonVi;
            }
            else
                if (nsDTO.DonVi == results[results.Count - 1].DonVi)
                {
                    nsDTO.STT_DonVi = results[results.Count - 1].STT_DonVi + 1;
                    nsDTO.DonViIn = "-nt-";
                }

            if (results.Count == 0 || nsDTO.BoMon != results[results.Count - 1].BoMon)
                nsDTO.STT_BoMon = 1;
            else
                if (nsDTO.BoMon == results[results.Count - 1].BoMon)
                    nsDTO.STT_BoMon = results[results.Count - 1].STT_BoMon + 1;

            //Tổng hợp trình độ chuyên môn
            
            if (nsDTO.DonVi.Contains("BGH") || nsDTO.DonVi[0].CompareTo('P') == 0)
            {
                //Gián tiếp
                if (nsDTO.KiemNhiem == null)
                {

                    if (nsDTO.TrinhDo != null)
                    {
                        if (nsDTO.TrinhDo.Contains("TS"))
                            HerculesDTO.Utils.GianTiep.TS++;
                        else
                            if (nsDTO.TrinhDo.Contains("ThS"))
                                HerculesDTO.Utils.GianTiep.ThS++;
                            else
                                if (nsDTO.TrinhDo.Contains("DH"))
                                    HerculesDTO.Utils.GianTiep.DH++;
                                else
                                    if (nsDTO.TrinhDo.Contains("CD"))
                                        HerculesDTO.Utils.GianTiep.CD++;
                                    else
                                        HerculesDTO.Utils.GianTiep.Khac++;
                    }
                    if (nsDTO.GioiTinh)
                        HerculesDTO.Utils.GianTiep.Nu++;
                }
                else//Kiêm nhiệm
                {
                    if (nsDTO.TrinhDo != null)
                    {
                        if (nsDTO.TrinhDo.Contains("TS"))
                            HerculesDTO.Utils.GV_KiemNhiem.TS++;
                        else
                            if (nsDTO.TrinhDo.Contains("ThS"))
                                HerculesDTO.Utils.GV_KiemNhiem.ThS++;
                            else
                                if (nsDTO.TrinhDo.Contains("DH"))
                                    HerculesDTO.Utils.GV_KiemNhiem.DH++;
                                else
                                    if (nsDTO.TrinhDo.Contains("CD"))
                                        HerculesDTO.Utils.GV_KiemNhiem.CD++;
                                    else
                                        HerculesDTO.Utils.GV_KiemNhiem.Khac++;
                    }
                    if (nsDTO.GioiTinh)
                        HerculesDTO.Utils.GV_KiemNhiem.Nu++;
                }
            }
            else //Giảng viên
            {
                //Trực tiếp
                //if (nsDTO.KiemNhiem == null)
                if (nsDTO.TrinhDo != null)
                {
                    if (nsDTO.TrinhDo.Contains("TS"))
                        HerculesDTO.Utils.GV_TrucTiep.TS++;
                    else
                        if (nsDTO.TrinhDo.Contains("ThS"))
                            HerculesDTO.Utils.GV_TrucTiep.ThS++;
                        else
                            if (nsDTO.TrinhDo.Contains("DH"))
                                HerculesDTO.Utils.GV_TrucTiep.DH++;
                            else
                                if (nsDTO.TrinhDo.Contains("CD"))
                                    HerculesDTO.Utils.GV_TrucTiep.CD++;
                                else
                                    HerculesDTO.Utils.GV_TrucTiep.Khac++;
                }
                    if (nsDTO.GioiTinh)
                        HerculesDTO.Utils.GV_TrucTiep.Nu++;
                
                
            }

            return nsDTO;
        }

        public List<ThongTinNhanSuDTO> GetAll(DateTime time)
        {
            timeGetDate = time;
            results = new List<ThongTinNhanSuDTO>();
            _conn = DataProvider.TaoKetNoiCSDL();
            var reader = DataProvider.TruyVanDuLieu(StrSelect, _conn);

            while (reader.Read())
            {
                results.Add(MappingSQLDRToDTO(reader));
            }

            reader.Close();
            _conn.Close();

            return results;
        }
    }
}
