﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class KhenThuongKyLuatDto : BaseDTO
    {
        public string MaNv { get; set; }
        public string SoQuyetDinh { get; set; }
        public bool IsKhenThuong { get; set; }
        public string ThoiGianHieuLuc { get; set; }
        public string CapQuyetDinh { get; set; }
        public string NoiDung { get; set; }
        //vdtoan add 220916
        public string LyDoKL { get; set; }

        public KhenThuongKyLuatDto()
        {
            IsKhenThuong = true;
            Id = -1;
        }

        public string ChuoiThoiGianHieuLuc
        {
            get
            {

                if (!String.IsNullOrEmpty(ThoiGianHieuLuc))
                {
                    return Utils.getDate(ThoiGianHieuLuc);
                }
                return "";
            }
        }
    }
}
