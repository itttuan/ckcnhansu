﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class BaoHiemDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public string SoSo { get; set; }
        public string NgayCap { get; set; }
        public string NoiDKKCB { get; set; }
        public bool LaBHXH { get; set; }
        public BaoHiemDTO()
        {
            this.Id = -1;
        }
    }
}
