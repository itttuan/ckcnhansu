﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class ThongTinNhanSuDTO:BaseDTO
    {
        public int STT_DonVi { get; set; }
        public int STT_BoMon { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string NgaySinh { get; set; }
        public string TDungTu { get; set; }
        public string DonVi { get; set; }
        public string DonViIn { get; set; }
        public string BoMon { get; set; }
        public string Loai { get; set; }
        public string TamTuyenTu { get; set; }
        public string ChucDanh { get; set; }
        public string TrinhDo { get; set; }//Trình độ chuyên môn cao nhất / hiện tại
        public string TruongBanDau { get; set; }
        public string ChuyenMonBanDau { get; set; }
        public bool GioiTinh { get; set; }
        public char GioiTinhNu { get; set; }
        public string KiemNhiem { get; set; }
    }
}
