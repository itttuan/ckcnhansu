﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class StaffDto : BaseDTO
    {

        #region PAGE 1
        public string MaNv { get; set; }
        public byte[] HinhAnh { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string TenKhac { get; set; }
        public int GioiTinh { get; set; }
        public DanTocDTO DanToc { get; set; }
        public TonGiaoDTO TonGiao { get; set; }
        public string QuocTich { get; set; }
        public DateTime NgaySinh { get; set; }
        public TinhThanhDTO NoiSinh { get; set; }
        public TinhThanhDTO QueQuan { get; set; }
        public string TrinhDoVanHoa { get; set; }
        public string Cmnd { get; set; }
        public TinhThanhDTO NoiCapCmnd { get; set; }
        public DateTime NgayCapCmnd { get; set; }
        public string TinhTrangHonNhan { get; set; }
        public PhongKhoaDTO PhongKhoa { get; set; }
        public BoMonBoPhanDTO BoMonBoPhan { get; set; }
        public DateTime NgayVaoLam { get; set; }
        public ChucVuDTO ChucVu { get; set; }
        public TrangThaiLamViecDTO TrangThaiLamViec { get; set; }
        public NganhDTO ChuyenMon { get; set; }
        public string MaNgach { get; set; }
        public string Hang { get; set; }
        public string Bac { get; set; }
        public string DiaChiTt { get; set; }
        public QuanHuyenDTO QuanHuyenTt { get; set; }
        public TinhThanhDTO TinhThanhTt { get; set; }
        public string DiaChiHt { get; set; }
        public QuanHuyenDTO QuanHuyenHt { get; set; }
        public TinhThanhDTO TinhThanhHt { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public DateTime NgayVaoDcsVn { get; set; }
        public DateTime NgayChinhThucVaoDcsVn { get; set; }
        public DateTime NgayNhapNgu { get; set; }
        public DateTime NgayXuatNgu { get; set; }
        public string QuanHamCaoNhat { get; set; }
        public string TinhTrangSucKhoe { get; set; }
        public string ChieuCao { get; set; }
        public string CanNang { get; set; }
        public string NhomMau { get; set; }
        public string ThuongBinhHang { get; set; }
        public string GiaDinhChinhSach { get; set; }
        #endregion

        #region PAGE 2
        public List<QuaTrinhCongTacDTO> DsQuaTrinhCongTac { get; set; } 
        #endregion

        #region PAGE 3
        public List<BangCapChungChiDto> DsBangCapChungChi { get; set; }
        #endregion

        #region PAGE 4
        public List<KhenThuongKyLuatDto> DsKhenThuongKyLuat { get; set; } 
        #endregion

        #region PAGE 5
        public List<QuanHeGiaDinhDto> DsQuanHeGiaDinh { get; set; } 
        #endregion

        #region PAGE 6
        public string TuNhanXet { get; set; }
        #endregion

        public StaffDto()
        {
            DsBangCapChungChi = new List<BangCapChungChiDto>();
            DsKhenThuongKyLuat = new List<KhenThuongKyLuatDto>();
            DsQuaTrinhCongTac = new List<QuaTrinhCongTacDTO>();
            DsQuanHeGiaDinh = new List<QuanHeGiaDinhDto>();
        }

    }
}
