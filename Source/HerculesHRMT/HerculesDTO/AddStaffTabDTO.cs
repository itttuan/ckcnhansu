﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class AddStaffTabDTO
    {
        // user start edit tab (type anything in any tab)
        public bool IsStart { get; set; }
        // user type value not correct format
        // user start edit tab but not fill full require fields
        public bool IsError { get; set; }
        // mark tab fill full require fields and correct format
        public bool IsComplete { get; set; }

        public AddStaffTabDTO()
        {
            IsStart = false;
            IsComplete = false;
            IsError = false;
        }
    }
}
