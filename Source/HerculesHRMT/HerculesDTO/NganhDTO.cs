﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class NganhDTO : BaseDTO
    {
        public string MaNganh { get; set; }
        public string TenNganh { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }        
    }
}
