﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class DangVienDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public string BiDanh { get; set; }
        public string SoThe { get; set; }
        public DateTime NgayChinhThuc { get; set; }
        public DateTime NgayVaoDang { get; set; }
        public DateTime NgayCapThe { get; set; }
        public string ChiBo { get; set; }
        public string DangBo { get; set; }
        public string ChucVuDang { get; set; }
        public DangVienDTO()
        {
            this.Id = -1;
        }
    }
}
