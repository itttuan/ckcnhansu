﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TonGiaoDTO:BaseDTO
    {
        public string MaTonGiao { get; set; }
        public string TenTonGiao { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }
    }
}
