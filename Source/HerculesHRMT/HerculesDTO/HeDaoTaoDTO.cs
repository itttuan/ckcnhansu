﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class HeDaoTaoDTO : BaseDTO
    {
        public string MaHeDaoTao { get; set; }
        public string TenHeDaoTao { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }
    }
}
