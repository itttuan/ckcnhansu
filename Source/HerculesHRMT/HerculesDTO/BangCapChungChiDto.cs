﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class BangCapChungChiDto : BaseDTO
    {
        public string MaNv { get; set; }
        public bool IsBangCap { get; set; }
        public string MaBangCapChungChi { get; set; }
        public LoaiBCapCChiDTO LoaiBangCapChungChi { get; set; }
        public LoaiTrinhDoDTO LoaiTrinhDo { get; set; }
        public HeDaoTaoDTO HeDaoTao { get; set; }
        public NganhDTO NganhDaoTao { get; set; }
        public ChuyenNganhDto ChuyenNganhDaoTao { get; set; }
        public TruongDTO CoSoDaotao { get; set; }
        public string ThoiGianHieuLuc { get; set; }
        public string ThoiGianDaoTao { get; set; }
        public DateTime NgayCapBang { get; set; }
        public string GhiChu { get; set; }
        public BangCapChungChiDto()
        {
            Id = -1;
        }
    }
}
