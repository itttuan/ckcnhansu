﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class DotTuyenDTO : BaseDTO
    {
        public string MaDot { get; set; }
        public string Thang { get; set; }
        public string Nam { get; set; }
        public string NgayGio { get; set; }
        public string Phong { get; set; }
        public string GhiChu { get; set; }
        public string NgayBatDau { get; set; }
        public string NgayKetThuc { get; set; }


      
    }
}
