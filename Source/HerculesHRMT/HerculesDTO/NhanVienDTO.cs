﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class NhanVienDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public string MaPK { get; set; }
        public string MaBMBP { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string TenThuongDung { get; set; }
        public string TenGoiKhac { get; set; }
        public string NgaySinh { get; set; }
        public string NoiSinh { get; set; }
        public Boolean GioiTinh { get; set; }
        public string Email { get; set; }
        public string DienThoai { get; set; }
        public string DienThoaiCoDinh { get; set; }
        public string QueQuan { get; set; }
        public string HoKhauThuongTruDiaChi { get; set; }
        public string HoKhauThuongTruMaQuanHuyen { get; set; }
        public string HoKhauThuongTruMaTinhThanh { get; set; }
        public string DiaChiHienTaiDiaChi { get; set; }
        public string DiaChiHienTaiMaQuanHuyen { get; set; }
        public string DiaChiHienTaiMaTinhThanh { get; set; }
        public string MaChucVu { get; set; }
        public string ChucDanh { get; set; }
        public string SoQuyetDinhChucVu { get; set; }
        public string KiemNhiem { get; set; }
        public string SoQuyetDinhKiemNhiem  { get; set; }
        public string CMND { get; set; }
        public string NgayCapCMND { get; set; }
        public string MaNoiCapCMND { get; set; }
        public string MaDanToc { get; set; }
        public string MaTonGiao { get; set; }
        public string QuocTich { get; set; }
        public string ThanhPhanGiaDinhXuatThan { get; set; }
        public string NgheNghiepTuyenDung { get; set; }
        public string HangNhanVien { get; set; }
        public string BacNhanVien { get; set; }
        public string MaNgach { get; set; }
        public string NgayVaoLam { get; set; }
        public string NgayTuyenDung { get; set; }
        public string TrinhDoVanHoa { get; set; }
        public string QuanLyNhaNuoc { get; set; }
        public Boolean DangVien { get; set; }
        public string NgayVaoDCSVN { get; set; }
        public string NgayChinhThucVaoDCSVN { get; set; }
        public Boolean QuanNgu { get; set; }
        public string NgayNhapNgu { get; set; }
        public string NgayXuatNgu { get; set; }
        public string QuanHamCaoNhat { get; set; }
        public Boolean GiaDinhChinhSach { get; set; }
        public Boolean ThuongBinh { get; set; }
        public string ThuongBinhHang { get; set; }
        public string TinhTrangLamViec { get; set; }
        public string TinhTrangHonNhan { get; set; }
        public string TinhTrangSucKhoe { get; set; }
        public string ChieuCao { get; set; }
        public string CanNang { get; set; }
        public string NhomMau { get; set; }
        public string TuNhanXet { get; set; }
        public string TenPhongKhoa { get; set; }
        public string TenBMBP { get; set; }
        public string TenTrangThai { get; set; }
        public string TenGioiTinh { get; set; }
        public string TenChucVu { get; set; }
        public string MaSoThue { get; set; }
        public string SoTaiKhoan { get; set; }
        public string SoBHXH { get; set; }
        public string SoATM { get; set; }
        public string NgheNghiepTruocTuyenDung { get; set; }
        public string TenDanToc { get; set; }
        public string TenTonGiao { get; set; }
        public string TenNgach { get; set; }
        public decimal HeSoLuong { get; set; }
        public string NgayHuong { get; set; }
        public string NguoiSuaCuoiCung { get; set; }
       //vdtoan
        public string MaUV { get; set; }
        public string HoTen
        {
            get
            {
                return Ho + " " + Ten;
            }
            set
            { 
                HoTen = Ho + " " + Ten;
            }
        }
        public NhanVienDTO()
        {
            MaNV = string.Empty;
            MaPK = string.Empty;
            MaBMBP = string.Empty;
            Ho = string.Empty;
            Ten = string.Empty;
            TenThuongDung = string.Empty;
            TenGoiKhac = string.Empty;
            NgaySinh = string.Empty;
            NoiSinh = string.Empty;
            Email = string.Empty;
            DienThoai = string.Empty;
            DienThoaiCoDinh = string.Empty;
            QueQuan = string.Empty;
            HoKhauThuongTruDiaChi = string.Empty;
            HoKhauThuongTruMaQuanHuyen = string.Empty;
            HoKhauThuongTruMaTinhThanh = string.Empty;
            DiaChiHienTaiDiaChi = string.Empty;
            DiaChiHienTaiMaQuanHuyen = string.Empty;
            DiaChiHienTaiMaTinhThanh = string.Empty;
            MaChucVu = string.Empty;
            ChucDanh = string.Empty;
            SoQuyetDinhChucVu = string.Empty;
            KiemNhiem = string.Empty;
            SoQuyetDinhKiemNhiem  = string.Empty;
            CMND = string.Empty;
            NgayCapCMND = string.Empty;
            MaNoiCapCMND = string.Empty;
            MaDanToc = string.Empty;
            MaTonGiao = string.Empty;
            QuocTich = string.Empty;
            ThanhPhanGiaDinhXuatThan = string.Empty;
            NgheNghiepTuyenDung = string.Empty;
            HangNhanVien = string.Empty;
            BacNhanVien = string.Empty;
            MaNgach = string.Empty;
            NgayVaoLam = string.Empty;
            NgayTuyenDung = string.Empty;
            TrinhDoVanHoa = string.Empty;
            QuanLyNhaNuoc = string.Empty;
            NgayVaoDCSVN = string.Empty;
            NgayChinhThucVaoDCSVN = string.Empty;
            NgayNhapNgu = string.Empty;
            NgayXuatNgu = string.Empty;
            QuanHamCaoNhat = string.Empty;
            ThuongBinhHang = string.Empty;
            TinhTrangLamViec = string.Empty;
            TinhTrangHonNhan = string.Empty;
            TinhTrangSucKhoe = string.Empty;
            ChieuCao = string.Empty;
            CanNang = string.Empty;
            NhomMau = string.Empty;
            TuNhanXet = string.Empty;
            TenPhongKhoa = string.Empty;
            TenBMBP = string.Empty;
            TenTrangThai = string.Empty;
            TenGioiTinh = string.Empty;
            TenChucVu = string.Empty;
            MaSoThue = string.Empty;
            SoTaiKhoan = string.Empty;
            SoBHXH = string.Empty;
            SoATM = string.Empty;
            NgheNghiepTruocTuyenDung = string.Empty;
            TenDanToc = string.Empty;
            TenTonGiao = string.Empty;
            TenNgach = string.Empty;
            HeSoLuong = 0;
            NgayHuong = string.Empty;
            NguoiSuaCuoiCung = string.Empty;
            //vdtoan
            MaUV = string.Empty;
        }
    }
}
