﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class QuaTrinhCongTacDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public string NoiCongTac { get; set; }
        public string CongViec { get; set; }
        public string ThanhTich { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public string GhiChu { get; set; }
        public int GiaiDoan { get; set; } // 0: truoc tuyen dung | 1: sau tuyen dung | 2: tham gian CT-XH
        public string LoaiQuaTrinh { get; set; } // đổi giai đoạn sang text
        public bool DenNay { get; set; }
        
        public string ChuoiNgayCongTac
        {
            get {
                string str = "";
                if (!String.IsNullOrEmpty(TuNgay))
                {
                    //DateTime tuNgay = DateTime.Parse(TuNgay);
                    //str += "Từ ngày " + tuNgay.ToString("dd/MM/yyyy") + " đến ";
                    str += "Từ " + Utils.getDate(TuNgay);

                    if (DenNay)
                        str += " đến nay";
                    else
                    {
                        if (!String.IsNullOrEmpty(DenNgay))
                        {
                            //DateTime denNgay = DateTime.Parse(DenNgay);
                            str += " đến "+ Utils.getDate(DenNgay);
                        }
                    }
                }
                                
                return str; }            
        }
        

        public string ChuoiCongViec
        {
            get
            {
                string str = "";
                if (!String.IsNullOrEmpty(CongViec))
                {
                    str += CongViec;
                    if (!String.IsNullOrEmpty(NoiCongTac))
                        str += " tại " + NoiCongTac;

                    if(!String.IsNullOrEmpty(ThanhTich))
                        str += Environment.NewLine + "Thành tích: " + Environment.NewLine + ThanhTich;

                    if (!String.IsNullOrEmpty(GhiChu))
                        str += Environment.NewLine + "Ghi chú: " + Environment.NewLine + GhiChu;

                }
                return str.Replace("\\r\\n", Environment.NewLine);
            }


        }

        public QuaTrinhCongTacDTO()
        {
            Id = -1;
        }
    }
}
