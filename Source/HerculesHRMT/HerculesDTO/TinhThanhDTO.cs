﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TinhThanhDTO: BaseDTO
    {
        public string MaTinhThanh { get; set; }
        public string TenTinhThanh { get; set; }
        public string TenDayDu { get; set; }
        public string TenVietTat { get; set; }
        public int Loai { get; set; }
        public string TenLoaiTinhThanh { get; set; }
        public string DauSoCMND { get; set; }
    }
}
