﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class ChucVuDTO : BaseDTO
    {
        public string MaChucVu { get; set; }
        public string TenChucVu { get; set; }
        public string GhiChu { get; set; }

        public ChucVuDTO()
        {
            MaChucVu = string.Empty;
            TenChucVu = string.Empty;
            GhiChu = string.Empty;
            NgayHieuLuc = DateTime.Now;
            TinhTrang = 0;
        }
    }
}
