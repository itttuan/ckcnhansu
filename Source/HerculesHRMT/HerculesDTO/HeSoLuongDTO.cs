﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class HeSoLuongDTO
    {
        public string MaNgach { get; set; }
        public string Bac { get; set; }
        public double HeSo { get; set; }
        public string GhiChu { get; set; }
    }
}
