﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiBangNgoaiNguDTO:BaseDTO
    {
        public string TenLoaiBangNgoaiNgu { get; set; }
        public string MaNgoaiNgu { get; set; }
    }
}
