﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class LoaiTrinhDoDTO : BaseDTO
    {
        public string MaLoaiTrinhDo { get; set; }
        public string TenLoaiTrinhDo { get; set; }
        public string TenVietTat { get; set; }
        public string GhiChu { get; set; }
    }
}
