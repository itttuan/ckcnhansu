﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class NhacNhoDTO : BaseDTO
    {
        public string MaLoaiNhacNho { get; set; }
        public string NoiDung { get; set; }
        public string ThoiGianNhac { get; set; }
        public string GhiChu { get; set; }
        public string NgayHetHan { get; set; }
        public string SoQuyetDinh { get; set; }



      
    }
}
