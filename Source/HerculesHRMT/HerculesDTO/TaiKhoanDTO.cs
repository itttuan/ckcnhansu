﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class TaiKhoanDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string MaNV { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public DateTime NgayHieuLuc { get; set; }
        public int TinhTrang { get; set; }
        public DateTime LastLogonTime { get; set; }
    }
}
