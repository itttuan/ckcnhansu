﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class NgachDTO: BaseDTO
    {
        public string MaNgach { get; set; }
        public string TenNgach { get; set; }
        public string GhiChu { get; set; }
    }
}
