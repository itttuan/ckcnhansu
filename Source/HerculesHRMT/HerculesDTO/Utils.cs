﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class Utils
    {

        public static List<int> DSChonTrangThaiLamViec = new List<int>();

        public class GianTiep
        {
            public static int TS { get; set; }
            public static int ThS { get; set; }
            public static int DH { get; set; }
            public static int CD { get; set; }
            public static int Khac { get; set; }
            public static int Nu { get; set; }

            public static void Init()
            {
                TS = ThS = DH = CD = Khac = Nu = 0;
            }
        }

        public class GV_KiemNhiem
        {
            public static int TS { get; set; }
            public static int ThS { get; set; }
            public static int DH { get; set; }
            public static int CD { get; set; }
            public static int Khac { get; set; }
            public static int Nu { get; set; }

            public static void Init()
            {
                TS = ThS = DH = CD = Khac = Nu = 0;
            }
        }

        public class GV_TrucTiep
        {
            public static int TS { get; set; }
            public static int ThS { get; set; }
            public static int DH { get; set; }
            public static int CD { get; set; }
            public static int Khac { get; set; }
            public static int Nu { get; set; }

            public static void Init()
            {
                TS = ThS = DH = CD = Khac = Nu = 0;
            }
        }

        public static string getDate(string date)
        {
            if (String.IsNullOrEmpty(date))
                return null;
            if (!date.Contains("-"))
                return date;
            string[] str = date.Split(new char[] { '-' });
            int y = 0;
            int m = 0;
            int d = 0;

            if (!String.IsNullOrEmpty(str[0]))
                y = int.Parse(str[0]);
            if (!String.IsNullOrEmpty(str[1]))
                m = int.Parse(str[1]);
            if (!String.IsNullOrEmpty(str[2]))
                d = int.Parse(str[2]);

            if (d == 0)
            {
                if (m == 0)
                    return y.ToString();
                else
                    return m.ToString().PadLeft(2, '0') + "/" + y.ToString();
            }
            return d.ToString().PadLeft(2, '0') + "/" + m.ToString().PadLeft(2, '0') + "/" + y.ToString();
        }


    }
}
