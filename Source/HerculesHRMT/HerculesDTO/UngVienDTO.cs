﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class UngVienDTO : BaseDTO
    {
        public string MaUV { get; set; }
        public string MaDot { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string NgaySinh { get; set; }
        public Boolean GioiTinh { get; set; }
        public string CMND { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public string QueQuan { get; set; }
        public string NganhHoc { get; set; }
        public string MaTruong { get; set; }
        public string TruongKhac { get; set; }
        public int HinhThucDaoTao { get; set; }
        public string XepHang { get; set; }
        public string DiemTN { get; set; }
        public string SauDaiHoc { get; set; }
        public string MaPK { get; set; }
        public string MaBMBP { get; set; }
        public string NganhDuTuyen { get; set; }
        public string ChungChi { get; set; }
        public string GhiChu { get; set; }
        public string YeuCauChuyenMon { get; set; }
        public string KyNangSuPham { get; set; }


      
    }
}
