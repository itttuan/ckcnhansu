﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace HerculesDTO
{
    public class PhanCongDTO:BaseDTO
    {
        public string MaNV { get; set; }
        public string MaBMBP { get; set; }
        public string CongViec { get; set; }
        public string TuNgay { get; set; }
        public string TrangThai { get; set; }
        public string STT { get; set; }
        public string TenBMBP { get; set; }
        public string MaPK { get; set; }
        public string TenPhongKhoa { get; set; }
        public PhanCongDTO()
        {
            Id = -1;
        }
    }
}
