﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class QuanHeGiaDinhCTL
    {
        QuanHeGiaDinhDAC quanhegiadinhDAC = new QuanHeGiaDinhDAC();

        public List<QuanHeGiaDinhDto> LayDanhSachThanNhanCuaNhanVien(string strMaNV)
        {
            List<QuanHeGiaDinhDto> listResult = new List<QuanHeGiaDinhDto>();
            List<QuanHeGiaDinhDto> listQuery = quanhegiadinhDAC.LayDanhSachQuanHeGiaDinhTheoNhanVien(strMaNV);
            foreach (QuanHeGiaDinhDto qhgdDTO in listQuery)
            {
                if (qhgdDTO.TinhThanh.MaTinhThanh != null)
                {
                    qhgdDTO.TinhThanh = (new TinhThanhCTL()).GetByMaTinhThanh(qhgdDTO.TinhThanh.MaTinhThanh);
                }
                if (qhgdDTO.QuanHuyen.MaQuanHuyen != null)
                {
                    qhgdDTO.QuanHuyen = (new QuanHuyenCTL()).GetByMaQuanHuyen(qhgdDTO.QuanHuyen.MaQuanHuyen);
                }
                if (qhgdDTO.LoaiQuanHe.MaQuanHe != null)
                {
                    qhgdDTO.LoaiQuanHe = (new QuanHeThanNhanCTL()).GetByMaQuanHe(qhgdDTO.LoaiQuanHe.MaQuanHe);
                }
                if (qhgdDTO.DiaChi != null)
                {
                    qhgdDTO.DiaChiTongHop += qhgdDTO.DiaChi + ", ";
                }
                if (qhgdDTO.QuanHuyen.TenQuanHuyen != null)
                {
                    qhgdDTO.DiaChiTongHop += qhgdDTO.QuanHuyen.TenQuanHuyen + ", ";
                }
                if (qhgdDTO.TinhThanh.TenTinhThanh != null)
                {
                    qhgdDTO.DiaChiTongHop += qhgdDTO.TinhThanh.TenTinhThanh;
                }
                listResult.Add(qhgdDTO);
            }
            return listResult;
        }

        public bool LuuMoiQuanHeGiaDinh(QuanHeGiaDinhDto qhgd)
        {
            return quanhegiadinhDAC.Save(qhgd);
        }

        public bool CapNhatQuanHeGiaDinh(QuanHeGiaDinhDto qhgd)
        {
            return quanhegiadinhDAC.Update(qhgd);
        }

        public bool XoaQuanHeGiaDinh(string id)
        {
            return quanhegiadinhDAC.Delete(id);
        }
    }
}
