﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class BoMonBoPhanCTL
    {
        private readonly BoMonBoPhanDAC _boMonBoPhanDac = new BoMonBoPhanDAC();

        public List<BoMonBoPhanDTO> GetAll()
        {
            return _boMonBoPhanDac.GetAll();
        }

        public List<BoMonBoPhanDTO> GetListByMaPhongKhoa(string maPhongKhoa)
        {
            return _boMonBoPhanDac.GetListByMaPhongKhoa(maPhongKhoa);
        }

        public BoMonBoPhanDTO GetByMaBmBp(string maBmBp)
        {
            return !string.IsNullOrEmpty(maBmBp) ? _boMonBoPhanDac.GetByMaBmBp(maBmBp) : new BoMonBoPhanDTO();
        }

        public string GetMaxMaBMBP()
        {
            return _boMonBoPhanDac.GetMaxMaBMBP();
        }

        public bool Save(BoMonBoPhanDTO bmbpNew)
        {
            return _boMonBoPhanDac.Save(bmbpNew);
        }

        public bool Update(BoMonBoPhanDTO bmbpUpdate)
        {
            return _boMonBoPhanDac.Update(bmbpUpdate);
        }

        public bool Delete(BoMonBoPhanDTO bmbpDel)
        {
            return _boMonBoPhanDac.Delete(bmbpDel);
        }
    }
}
