﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class ThongTinNhanSuCTL
    {
        private readonly ThongTinNhanSuDAC _nhansuDac = new ThongTinNhanSuDAC();

        public List<ThongTinNhanSuDTO> GetAll(DateTime time)
        {
            return _nhansuDac.GetAll(time);
        }
    }
}
