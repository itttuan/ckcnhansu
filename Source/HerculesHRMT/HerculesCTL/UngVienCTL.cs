﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class UngVienCTL
    {
        private readonly UngVienDAC _ungvienDac = new UngVienDAC();


        public List<UngVienDTO> GetAll()
        {
            return _ungvienDac.GetAll();
        }

        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    return !string.IsNullOrEmpty(maPhongKhoa) ? _phongKhoaDac.GetByMaPhongKhoa(maPhongKhoa) : new PhongKhoaDTO();
        //}

        public string GetMaxMaUV()
        {
            return _ungvienDac.GetMaxMaUV();
        }

        public bool Save(UngVienDTO uvNew)
        {
            return _ungvienDac.Save(uvNew);
        }

        public bool Update(UngVienDTO uvUpdate)
        {
            return _ungvienDac.Update(uvUpdate);
        }

        public bool Delete(UngVienDTO uvDel)
        {
            return _ungvienDac.Delete(uvDel);
        }
    }
}
