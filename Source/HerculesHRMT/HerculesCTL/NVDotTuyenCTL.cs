﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

// vdtoan
namespace HerculesCTL
{
    public class NVDotTuyenCTL
    {
        private readonly NVDotTuyenDAC _nhanvienDAC = new NVDotTuyenDAC();

        public bool LuuNhanVien(NVDotTuyenDTO nhanvien)
        {
            //return _nhanvienDAC.Save(nhanvien);
            var id = _nhanvienDAC.LuuNhanVien(nhanvien);

            return id > 0;
        }

        public bool SaveStaff(StaffDto staff)
        {
            return _nhanvienDAC.SaveStaff(staff);
        }

        public List<NVDotTuyenDTO> LayDanhSachTatCaNhanVien()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVien();
        }

        public List<NVDotTuyenDTO> LayDanhSachNhanVienTheoPhongKhoa(string strMaPK)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoPhongKhoa(strMaPK);
        }
        public List<NVDotTuyenDTO> LayDanhSachNhanVienTheoBoMon(string strMaBM)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoBoMon(strMaBM);
        }
        public List<NVDotTuyenDTO> TimNhanVienTheoCMND(string strCMND)
        {
            return _nhanvienDAC.TimNhanVienTheoCMND(strCMND);
        }
        public List<NVDotTuyenDTO> TimNhanVienTheoMaNV(string strMaNV)
        {
            return _nhanvienDAC.TimNhanVienTheoMaNV(strMaNV);
        }

        public NVDotTuyenDTO TimNhanVienTheoId(string id)
        {
            return _nhanvienDAC.TimNhanVienTheoId(id);
        }

        public int LuuPhienBanMoi(NVDotTuyenDTO obj)
        {
            return _nhanvienDAC.LuuNhanVien(obj);
        }

        public void UpdatePhienBanCu(int id)
        {
            _nhanvienDAC.CapNhatTrangThaiPhienBanCu(id);
        }

        public List<NVDotTuyenDTO> LayCacPhienBanNhanVien(string maNV)
        {
            return _nhanvienDAC.LayTatCaCacPhienBanCapNhat(maNV);
        }

        public bool CapNhatTuNhanXet(string tunhanxet, string id)
        {
            return _nhanvienDAC.UpdateTuNhanXet(tunhanxet,id);
        }

        public bool CapNhatBHXHTKNganhang(NVDotTuyenDTO nhanvienDTO)
        {
            return _nhanvienDAC.UpdateBaoHiemTaiKhoanNganHang(nhanvienDTO);
        }

        public string GetMaNvMax(string maBoMon, string maPK)
        {
            return _nhanvienDAC.LayMaNVMax(maBoMon, maPK);
        }

        /// <summary>
        /// vdtoan
        /// </summary>
        /// <param name="strMaDotTuyenDung"></param>
        /// <returns></returns>
        public List<NVDotTuyenDTO> TimNhanVienTheoMaDotTuyenDung(string strMaDotTuyenDung)
        {
            return _nhanvienDAC.TimNhanVienTheoMaDotTuyenDung(strMaDotTuyenDung);
        }

        public List<NVDotTuyenDTO> TimNhanVienTheoNhieuTieuChi(string strMaKhoa, string strMaBMBP, string strMaDot)
        {
            return _nhanvienDAC.TimNhanVienTheoNhieuTieuChi(strMaKhoa, strMaBMBP, strMaDot);
        }
        public List<NVDotTuyenDTO> TimNhanVienTheoMaDot(string strMaDot)
        {
            return _nhanvienDAC.TimNhanVienTheoMaDot(strMaDot);
        }

        //vdtoan add 220916
        public List<NVDotTuyenDTO> TimNhanVienTheoMaUV(string strMaUV)
        {
            return _nhanvienDAC.TimNhanVienTheoMaUV(strMaUV);
        }

    }
}
