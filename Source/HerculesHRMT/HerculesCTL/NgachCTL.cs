﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class NgachCTL
    {
        NgachDAC ngachDAC = new NgachDAC();

        public List<NgachDTO> LayDanhSachNgach()
        {
            return ngachDAC.LayDanhSachNgach();
        }
    }
}
