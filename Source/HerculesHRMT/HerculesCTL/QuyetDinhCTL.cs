﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class QuyetDinhCTL
    {
        QuyetDinhDAC quyetDinhDAC = new QuyetDinhDAC();

        public List<QuyetDinhDTO> DanhSachQuyetDinhNhanVien(string maNV)
        {
            return quyetDinhDAC.LayDanhSachQuyetDinhTheoNhanVien(maNV);
        }

        public List<QuyetDinhDTO> GetAll()
        {
            return quyetDinhDAC.GetAll();
        }

        public string GetMaxMaHD()
        {
            return quyetDinhDAC.GetMaxMaHD();
        }

        public bool Save(QuyetDinhDTO qdNew)
        {
            return quyetDinhDAC.Save(qdNew);
        }

        public bool Update(QuyetDinhDTO qdUpdate)
        {
            return quyetDinhDAC.Update(qdUpdate);
        }

        public bool Delete(QuyetDinhDTO qdDel)
        {
            return quyetDinhDAC.Delete(qdDel);
        }

    }
}
