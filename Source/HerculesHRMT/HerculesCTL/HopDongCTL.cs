﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class HopDongCTL
    {
        HopDongDAC hodongDAC = new HopDongDAC();

        public List<HopDongDTO> DanhSachHopDongNhanVien(string maNV)
        {
            return hodongDAC.LayDanhSachHopDongTheoNhanVien(maNV);
        }

        public List<HopDongDTO> GetAll()
        {
            return hodongDAC.GetAll();
        }

        public string GetMaxMaHD()
        {
            return hodongDAC.GetMaxMaHD();
        }

        public bool Save(HopDongDTO hdNew)
        {
            return hodongDAC.Save(hdNew);
        }

        public bool Update(HopDongDTO hdUpdate)
        {
            return hodongDAC.Update(hdUpdate);
        }

        public bool Delete(HopDongDTO hdDel)
        {
            return hodongDAC.Delete(hdDel);
        }
    }
}
