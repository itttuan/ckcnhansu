﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class BangCapChungChiCTL
    {
        BangCapChungChiDAC bcccDAC = new BangCapChungChiDAC();
        public List<BangCapChungChiDto> DanhSachBangCapChungChiNhanVien(string strMaNV)
        {
            List<BangCapChungChiDto> listResult = new List<BangCapChungChiDto>();
            List<BangCapChungChiDto> list = bcccDAC.LayDanhSachBangCapChungChiNhanVien(strMaNV);
            TruongCTL truongctl = new TruongCTL();
            LoaiBCapCChiCTL loaiBCctl = new LoaiBCapCChiCTL();
            LoaiTrinhDoCTL loaiTDctl = new LoaiTrinhDoCTL();
            HeDaoTaoCTL heDTctl = new HeDaoTaoCTL();
            foreach (BangCapChungChiDto bcdto in list)
            {
                bcdto.CoSoDaotao = truongctl.GetByMaTruong(bcdto.CoSoDaotao.MaTruong);
                bcdto.LoaiBangCapChungChi = loaiBCctl.GetByLoaiBCapCChi(bcdto.LoaiBangCapChungChi.MaLoaiBCCC);
                bcdto.LoaiTrinhDo = loaiTDctl.GetByMaLoaiTrinhDo(bcdto.LoaiTrinhDo.MaLoaiTrinhDo);
                bcdto.HeDaoTao = heDTctl.GetByMaLoaiHeDaoTao(bcdto.HeDaoTao.MaHeDaoTao);
                listResult.Add(bcdto);
            }
            return listResult;
        }

        public bool LuuMoiTrinhDo(BangCapChungChiDto bccc)
        {
            return bcccDAC.Save(bccc);
        }

        public bool CapNhatTrinhDo(BangCapChungChiDto bccc)
        {
            return bcccDAC.Update(bccc);
        }
        public bool XoaTrinhDo(string id)
        {
            return bcccDAC.Delete(id);
        }
    }
}
