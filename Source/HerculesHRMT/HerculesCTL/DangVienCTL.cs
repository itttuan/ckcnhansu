﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class DangVienCTL
    {
        private DangVienDAC _dangvienDAC = new DangVienDAC();

        public bool LuuDangVien(DangVienDTO obj)
        {
            return _dangvienDAC.Save(obj);
        }
    }
}
