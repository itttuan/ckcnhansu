﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiBangLyLuanChinhTriCTL
    {
        LoaiBangLyLuanChinhTriDAC llctDAC = new LoaiBangLyLuanChinhTriDAC();

        public List<LoaiBangLyLuanChinhTriDTO> LayDanhSachLoaiBangLyLuanChinhTri()
        {
            return llctDAC.LayDanhSachLoaiBangLLChinhTri();
        }

        public LoaiBangLyLuanChinhTriDTO TimLoaiBangLLChinhTri(string id)
        {
            return llctDAC.TimLoaiBangLLChinhTri(id);
        }
    }
}
