﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiNhanVienCTL
    {
        LoaiNhanVienDAC _loainhanvienDAC = new LoaiNhanVienDAC();

        public List<LoaiNhanVienDTO> GetAll()
        {
            return _loainhanvienDAC.GetAll();
        }


        public string GetMaxMaLoaiNhanVien()
        {
            return _loainhanvienDAC.GetMaxMaLoaiNhanVien();
        }

        public bool Save(LoaiNhanVienDTO nvNew)
        {
            return _loainhanvienDAC.Save(nvNew);
        }

        public bool Update(LoaiNhanVienDTO nvUpdate)
        {
            return _loainhanvienDAC.Update(nvUpdate);
        }

        public bool Delete(LoaiNhanVienDTO nvDel)
        {
            return _loainhanvienDAC.Delete(nvDel);
        }
    }
}
