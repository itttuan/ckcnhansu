﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class PhanCongCTL
    {
        PhanCongDAC phancongDAC = new PhanCongDAC();

        public List<PhanCongDTO> DanhSachPhanCongNhanVien(string maNV)
        {
            return phancongDAC.LayDanhSachPhanCongTheoNhanVien(maNV);
        }
    }
}
