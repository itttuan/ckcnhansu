﻿using HerculesDAC;
using HerculesDTO;
using HerculesDTO.ReportDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HerculesCTL
{
    public class ReportCTL
    {
        readonly PhongKhoaDAC _phongKhoaDac = new PhongKhoaDAC();
        readonly BoMonBoPhanDAC _boMonBoPhanDac = new BoMonBoPhanDAC();
        readonly ReportDAC _reportDac = new ReportDAC();

        public TongHopNhanSuThangDTO LayDuLieu()
        {
            var result = new TongHopNhanSuThangDTO();

            var dsPhongKhoa = _phongKhoaDac.GetAll();
            foreach(var phongKhoa in dsPhongKhoa)
            {
                var nstPhongKhoa = new NSTPhongKhoaDTO();

                var dsBoMon = _boMonBoPhanDac.GetListByMaPhongKhoa(phongKhoa.MaPhongKhoa);

                foreach (var boMon in dsBoMon)
                {
                    var nstBoMon = new NSTBoMonDTO();

                    nstBoMon.TongSoChinhThuc = _reportDac.LaySoLuongNhanVien(string.Empty, boMon.MaBMBP);
                    nstBoMon.BoMon = boMon;

                    nstPhongKhoa.DsBoMon.Add(nstBoMon);
                }

                nstPhongKhoa.TongSoChinhThuc = _reportDac.LaySoLuongNhanVien(phongKhoa.MaPhongKhoa, string.Empty);
                nstPhongKhoa.PhongKhoa = phongKhoa;

                result.TongSo += nstPhongKhoa.TongSoChinhThuc;
                result.dsPhongKhoa.Add(nstPhongKhoa);
            }

            return result;
        }
    }
}
