﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class QuanHeThanNhanCTL
    {
        readonly QuanHeThanNhanDAC _quanHeThanNhanDac = new QuanHeThanNhanDAC();

        public List<QuanHeThanNhanDTO> GetAll()
        {
            return _quanHeThanNhanDac.GetAll();
        }

        public QuanHeThanNhanDTO GetByMaQuanHe(string maQuanHe)
        {
            return _quanHeThanNhanDac.GetByMaQuanHe(maQuanHe);
        }

        public string GetMaxMaLoaiQuanHe()
        {
            return _quanHeThanNhanDac.GetMaxMaLoaiQuanHe();
        }

        public bool Save(QuanHeThanNhanDTO tnNew)
        {
            return _quanHeThanNhanDac.Save(tnNew);
        }

        public bool Update(QuanHeThanNhanDTO tnUpdate)
        {
            return _quanHeThanNhanDac.Update(tnUpdate);
        }

        public bool Delete(QuanHeThanNhanDTO tnDel)
        {
            return _quanHeThanNhanDac.Delete(tnDel);
        }
    }
}
