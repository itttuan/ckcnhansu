﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class DotTuyenCTL
    {
        private readonly DotTuyenDAC _dottuyenDac = new DotTuyenDAC();


        public List<DotTuyenDTO> GetAll()
        {
            return _dottuyenDac.GetAll();
        }

        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    return !string.IsNullOrEmpty(maPhongKhoa) ? _phongKhoaDac.GetByMaPhongKhoa(maPhongKhoa) : new PhongKhoaDTO();
        //}

        //public string GetMaxMaPhongKhoa()
        //{
        //    return _phongKhoaDac.GetMaxMaPhongKhoa();
        //}

        public bool Save(DotTuyenDTO pkNew)
        {
            return _dottuyenDac.Save(pkNew);
        }

        public bool Update(DotTuyenDTO dtUpdate)
        {
            return _dottuyenDac.Update(dtUpdate);
        }

        public bool Delete(DotTuyenDTO dtDel)
        {
            return _dottuyenDac.Delete(dtDel);
        }

        //vdtoan addd 230916
        public string GetMaxMaDT()
        {
            return _dottuyenDac.GetMaxMaDT();
        }
    }
}
