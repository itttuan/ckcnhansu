﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TonGiaoCTL
    {
        TonGiaoDAC _tongiaoDAC = new TonGiaoDAC();

        public List<TonGiaoDTO> GetAll()
        {
            return _tongiaoDAC.GetAll();
        }


        public string GetMaxMaTonGiao()
        {
            return _tongiaoDAC.GetMaxMaTonGiao();
        }

        public bool Save(TonGiaoDTO tgNew)
        {
            return _tongiaoDAC.Save(tgNew);
        }

        public bool Update(TonGiaoDTO tgUpdate)
        {
            return _tongiaoDAC.Update(tgUpdate);
        }

        public bool Delete(TonGiaoDTO tgDel)
        {
            return _tongiaoDAC.Delete(tgDel);
        }

        public TonGiaoDTO GetByMaTonGiao(string maTonGiao)
        {
            return _tongiaoDAC.GetByMaTonGiao(maTonGiao);
        }
    }
}
