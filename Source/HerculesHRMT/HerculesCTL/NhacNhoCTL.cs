﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class NhacNhoCTL
    {
        private readonly NhacNhoDAC _nhacnhoDac = new NhacNhoDAC();


        public List<NhacNhoDTO> GetAll()
        {
            return _nhacnhoDac.GetAll();
        }

        public List<NhacNhoDTO> GetAllNhacNho()
        {
            return _nhacnhoDac.GetAllNhacNho();
        }

        //public PhongKhoaDTO GetByMaPhongKhoa(string maPhongKhoa)
        //{
        //    return !string.IsNullOrEmpty(maPhongKhoa) ? _phongKhoaDac.GetByMaPhongKhoa(maPhongKhoa) : new PhongKhoaDTO();
        //}

        //public string GetMaxMaPhongKhoa()
        //{
        //    return _phongKhoaDac.GetMaxMaPhongKhoa();
        //}

        public bool Save(NhacNhoDTO nnNew)
        {
            return _nhacnhoDac.Save(nnNew);
        }

        public bool Update(NhacNhoDTO nnUpdate)
        {
            return _nhacnhoDac.Update(nnUpdate);
        }

        public bool Delete(NhacNhoDTO nnDel)
        {
            return _nhacnhoDac.Delete(nnDel);
        }

        public string GetMaxId()
        {
            return _nhacnhoDac.GetMaxId();
        }

        public bool ChuyenTrangThaiNhacNho(NhacNhoDTO nnUpdate)
        {
            return _nhacnhoDac.ChuyenTrangThaiNhacNho(nnUpdate);
        }

        public List<NhacNhoDTO> TimNhacNhoTheoNgayHetHan(String sNgayHetHan)
        {
            return _nhacnhoDac.TimNhacNhoTheoNgayHetHan(sNgayHetHan);
        }
    }
}
