﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDTO;
using HerculesDAC;

namespace HerculesCTL
{
    public class LoaiBangChuyenMonCTL
    {
        LoaiBangChuyenMonDAC chuyenmonDAC = new LoaiBangChuyenMonDAC();

        public List<LoaiBangChuyenMonDTO> LayDanhSachLoaiBangChuyenMon()
        {
            return chuyenmonDAC.LayDanhSachLoaiBangChuyenMon();
        }

        public LoaiBangChuyenMonDTO TimLoaiBangChuyenMon(string id)
        {
            return chuyenmonDAC.TimLoaiBangChuyenMon(id);
        }
    }
}
