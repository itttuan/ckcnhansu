﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class HeDaoTaoCTL
    {
        readonly HeDaoTaoDAC _heDaoTaoDac = new HeDaoTaoDAC();

        public List<HeDaoTaoDTO> GetAll()
        {
            return _heDaoTaoDac.GetAll();
        }

        public HeDaoTaoDTO GetByMaLoaiHeDaoTao(string maLoaiHeDaoTao)
        {
            return _heDaoTaoDac.GetByMaLoaiHeDaoTao(maLoaiHeDaoTao);
        }

        public string GetMaxMaHeDaoTao()
        {
            return _heDaoTaoDac.GetMaxMaHeDaoTao();
        }

        public bool Save(HeDaoTaoDTO hNew)
        {
            return _heDaoTaoDac.Save(hNew);
        }

        public bool Update(HeDaoTaoDTO hUpdate)
        {
            return _heDaoTaoDac.Update(hUpdate);
        }

        public bool Delete(HeDaoTaoDTO hDel)
        {
            return _heDaoTaoDac.Delete(hDel);
        }
    }
}
