﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class TaiKhoanCTL
    {
        TaiKhoanDAC taikhoanDAC = new TaiKhoanDAC();

        public TaiKhoanDTO LayTaiKhoanUserDangNhap(string strUsename, string strPassword)
        {
            return taikhoanDAC.LayTaiKhoanUserDangNhap(strUsename, strPassword);
        }
        public bool TaoTaiKhoanMoi(TaiKhoanDTO tkDTO)
        {
            return taikhoanDAC.Save(tkDTO);
        }
        public void LuuThoiGianDangNhapHienTai(string strUsername)
        {
            taikhoanDAC.CapNhatThoiGianDangNhap(strUsername);
        }
    }
}
