﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class NhanVienCTL
    {
        private readonly NhanVienDAC _nhanvienDAC = new NhanVienDAC();

        public bool LuuNhanVien(NhanVienDTO nhanvien)
        {
            //return _nhanvienDAC.Save(nhanvien);
            var id = _nhanvienDAC.LuuNhanVien(nhanvien);

            return id > 0;
        }

        public bool SaveStaff(StaffDto staff)
        {
            return _nhanvienDAC.SaveStaff(staff);
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVien()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVien();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVien_LocTrangThaiLamViec()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVien_LocTrangThaiLamViec();
        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoPhongKhoa(string strMaPK)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoPhongKhoa(strMaPK);
        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoPhongKhoa_LocTrangThaiLamViec(string strMaPK)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoPhongKhoa_LocTrangThaiLamViec(strMaPK);
        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoBoMon(string strMaBM)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoBoMon(strMaBM);
        }

        public List<NhanVienDTO> LayDanhSachNhanVienTheoBoMon_LocTrangThaiLamViec(string strMaBM)
        {
            return _nhanvienDAC.LayDanhSachNhanVienTheoBoMon_LocTrangThaiLamViec(strMaBM);
        }


        public List<NhanVienDTO> TimNhanVienTheoCMND(string strCMND)
        {
            return _nhanvienDAC.TimNhanVienTheoCMND(strCMND);
        }
        public List<NhanVienDTO> TimNhanVienTheoMaNV(string strMaNV)
        {
            return _nhanvienDAC.TimNhanVienTheoMaNV(strMaNV);
        }

        public NhanVienDTO TimNhanVienTheoId(string id)
        {
            return _nhanvienDAC.TimNhanVienTheoId(id);
        }

        public int LuuPhienBanMoi(NhanVienDTO obj)
        {
            return _nhanvienDAC.LuuNhanVien(obj);
        }

        public void UpdatePhienBanCu(int id)
        {
            _nhanvienDAC.CapNhatTrangThaiPhienBanCu(id);
        }

        public List<NhanVienDTO> LayCacPhienBanNhanVien(string maNV)
        {
            return _nhanvienDAC.LayTatCaCacPhienBanCapNhat(maNV);
        }

        public bool CapNhatTuNhanXet(string tunhanxet, string id)
        {
            return _nhanvienDAC.UpdateTuNhanXet(tunhanxet,id);
        }

        public bool CapNhatBHXHTKNganhang(NhanVienDTO nhanvienDTO)
        {
            return _nhanvienDAC.UpdateBaoHiemTaiKhoanNganHang(nhanvienDTO);
        }

        public string GetMaNvMax(string maBoMon, string maPK)
        {
            return _nhanvienDAC.LayMaNVMax(maBoMon, maPK);
        }

        public bool CapNhatNhanVien(NhanVienDTO nhanvienDTO)
        {
            return _nhanvienDAC.UpdateNhanVien(nhanvienDTO);
        }

        //vdtoan add
        public bool ChuyenUngVienVaoNhanVien(NhanVienDTO nhanvien)
        {
            //return _nhanvienDAC.Save(nhanvien);
            var id = _nhanvienDAC.ChuyenUngVienVaoNhanVien(nhanvien);

            return id > 0;
        }

        //vdtoan add 201016
        public string LayHoTenNVTheoMaNV(string manv)
        {
            return _nhanvienDAC.LayHoTenNVTheoMaNV(manv);
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNam()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNam();
        }
    
        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNu()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNu();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNgachGiangVien()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNgachGiangVien();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienPhongBan()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienPhongBan();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienDuHoc()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienDuHoc();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNghiViec()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNghiViec();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNghiHuu()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNghiHuu();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNghiThaiSan()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNghiThaiSan();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienNghiPhep()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienNghiPhep();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienDangLamViec()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienDangLamViec();
        }

        public List<NhanVienDTO> LayDanhSachTatCaNhanVienTatCaTrangThaiLamViec()
        {
            return _nhanvienDAC.LayDanhSachTatCaNhanVienTatCaTrangThaiLamViec();
        }
    }
}
