﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class ChucVuCTL
    {
        private readonly ChucVuDAC _chucVuDac = new ChucVuDAC();

        public List<ChucVuDTO> GetAll()
        {
            return _chucVuDac.GetAll();
        }

        public bool Save(ChucVuDTO chucVuDto)
        {
            return _chucVuDac.Save(chucVuDto);
        }

        public bool Update(ChucVuDTO chucVuDto)
        {
            return _chucVuDac.Update(chucVuDto);
        }

        public bool Delete(ChucVuDTO chucVuDto)
        {
            return _chucVuDac.Delete(chucVuDto);
        }

        public ChucVuDTO GetByMaChucVu(string maChucVu)
        {
            return !string.IsNullOrEmpty(maChucVu) ? _chucVuDac.GetByMaDanToc(maChucVu) : new ChucVuDTO();
        }

    }
}
