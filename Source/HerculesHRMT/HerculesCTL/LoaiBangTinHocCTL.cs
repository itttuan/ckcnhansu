﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerculesDAC;
using HerculesDTO;

namespace HerculesCTL
{
    public class LoaiBangTinHocCTL
    {
        LoaiBangTinHocDAC tinhocDAC = new LoaiBangTinHocDAC();

        public List<LoaiBangTinHocDTO> LayDanhSachLoaiBangTinHoc()
        {
            return tinhocDAC.LayDanhSachLoaiBangTinHoc();
        }
        public LoaiBangTinHocDTO TimLoaiBangTinHoc(string id)
        {
            return tinhocDAC.TimLoaiBangTinHoc(id);
        }
    }
}
