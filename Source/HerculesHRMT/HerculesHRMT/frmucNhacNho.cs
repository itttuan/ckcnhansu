﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Linq;

namespace HerculesHRMT
{
    public partial class frmucNhacNho : DevExpress.XtraEditors.XtraForm
    {
        NhacNhoCTL nnCTL = new NhacNhoCTL();
        NhacNhoDTO nnSelect = null;
        List<NhacNhoDTO> listnn = new List<NhacNhoDTO>();

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public frmucNhacNho()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listnn = nnCTL.GetAll();
            gcNhacNho.DataSource = listnn;
            gridViewNhacNho.BestFitColumns();

            btnLuu.Enabled = false;

        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:

                    txtID.Properties.ReadOnly = false;
                    cbbeLoaiNhacNho.Properties.ReadOnly = false;
                    deThoiGianNhac.Properties.ReadOnly = false;
                    deNgayHetHan.Properties.ReadOnly = false;
                    txtSoQuyetDinh.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    meNoiDung.Properties.ReadOnly = false;

                    txtID.Text = string.Empty;
                    cbbeLoaiNhacNho.Text = string.Empty;
                    txtSoQuyetDinh.Text = string.Empty;
                    meGhiChu.Text = string.Empty;
                    meNoiDung.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;

                    // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case EDIT:
                    txtID.Properties.ReadOnly = false;
                    cbbeLoaiNhacNho.Properties.ReadOnly = false;
                    deThoiGianNhac.Properties.ReadOnly = false;
                    deNgayHetHan.Properties.ReadOnly = false;
                    txtSoQuyetDinh.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    meNoiDung.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(nnSelect);
                    _selectedStatus = EDIT;

                    // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case VIEW:

                    txtID.Properties.ReadOnly = true;
                    cbbeLoaiNhacNho.Properties.ReadOnly = true;
                    deThoiGianNhac.Properties.ReadOnly = true;
                    deNgayHetHan.Properties.ReadOnly = true;
                    txtSoQuyetDinh.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;
                    meNoiDung.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(nnSelect);
                    _selectedStatus = VIEW;

                    // add new
                    btnThem.Enabled = true;
                    btnCapNhat.Enabled = true;
                    break;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

            SetStatus(NEW);
            //deNgay.EditValue = Constants.EmptyDateTimeType2;
            //teGio.EditValue = "00:00";
            //deNgayBatDau.EditValue = Constants.EmptyDateTimeType2;
            //deNgayKetThuc.EditValue = Constants.EmptyDateTimeType2;
            //txtID.Text = String.Empty;
            
            int mann = 1;
            string sLastIndexIDNhacNho = nnCTL.GetMaxId();
            if (sLastIndexIDNhacNho.CompareTo(string.Empty) != 0)
            {

                mann = int.Parse(sLastIndexIDNhacNho) + 1;

            }
            //else
            //    mann = 1;
            txtID.Text = mann.ToString();


            cbbeLoaiNhacNho.Text = String.Empty;
            txtSoQuyetDinh.Text = String.Empty;
            deThoiGianNhac.EditValue = Constants.EmptyDateTimeType2;
            deNgayHetHan.EditValue = Constants.EmptyDateTimeType2;
            meNoiDung.Text = String.Empty;
            meGhiChu.Text = String.Empty;
           
           
        }


        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            //if (!CheckInputData())
            //{
            //    //   XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm \n Ngày", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            NhacNhoDTO nnNew = GetData(new NhacNhoDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = nnCTL.Save(nnNew);
            else
                isSucess = nnCTL.Update(nnNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listnn.IndexOf(listnn.FirstOrDefault(p => p.Id == nnNew.Id));
                gridViewNhacNho.FocusedRowHandle = _selectedIndex;
                
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (nnSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa nhắc nhở " + nnSelect.Id + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = nnCTL.Delete(nnSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listnn.Count != 0)
                            gridViewNhacNho.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmThongBao_Load(object sender, EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private NhacNhoDTO GetData(NhacNhoDTO nnDTO)
        {
            //    string madt = "";
            if (_selectedStatus == NEW)
            {
                //string sLastIndexNganh = pkCTL.GetMaxMaPhongKhoa();
                //if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                //{
                //    int nLastIndexTG = int.Parse(sLastIndexNganh);
                //    maPK += (nLastIndexTG + 1).ToString().PadLeft(2, '0');
                //}
                //else
                //    maPK += "01";

               
                nnDTO.Id = Int32.Parse(txtID.Text);

                nnDTO.MaLoaiNhacNho = cbbeLoaiNhacNho.Text;
                nnDTO.SoQuyetDinh = txtSoQuyetDinh.Text;
                DateTime ThoiGianNhac = DateTime.Parse(deThoiGianNhac.EditValue.ToString());
                nnDTO.ThoiGianNhac = ThoiGianNhac.ToString("yyyy-MM-dd");
                DateTime NgayHetHan = DateTime.Parse(deNgayHetHan.EditValue.ToString());
                nnDTO.NgayHetHan = NgayHetHan.ToString("yyyy-MM-dd");
                nnDTO.NoiDung = meNoiDung.Text;
                nnDTO.GhiChu = meGhiChu.Text;
                nnDTO.NgayHieuLuc = DateTime.Now;
                nnDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                nnDTO.Id = nnSelect.Id;

                nnDTO.MaLoaiNhacNho = cbbeLoaiNhacNho.Text;
                nnDTO.NoiDung = meNoiDung.Text;
                nnDTO.GhiChu = meGhiChu.Text;
                nnDTO.SoQuyetDinh = txtSoQuyetDinh.Text;
                DateTime ThoiGianNhac = DateTime.Parse(deThoiGianNhac.EditValue.ToString());
                nnDTO.ThoiGianNhac = ThoiGianNhac.ToString("yyyy-MM-dd");
                DateTime NgayHetHan = DateTime.Parse(deNgayHetHan.EditValue.ToString());
                nnDTO.NgayHetHan = NgayHetHan.ToString("yyyy-MM-dd");
            }



            //int result = 0;
            //if (int.TryParse(txtPhong.Text, out result))
            //    pkDTO.ThuTuBaoCao = result;
            //else
            //    pkDTO.ThuTuBaoCao = null;

            //pkDTO.TenVietTat = txtTenVT.Text;
            //pkDTO.GhiChu = meGhiChu.Text;

            return nnDTO;
        }
        //private bool CheckInputData()
        //{
        //    //if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty || deNgay.Text === string.Empty)
        //    if (cbbeLoaiNhacNho.Text == string.Empty || cbbeNam.Text == string.Empty)
        //        return false;

        //    return true;
        //}

        private void BindingData(NhacNhoDTO nnDTO)
        {
            if (nnDTO != null)
            {

                txtID.Text = nnDTO.Id.ToString();
                cbbeLoaiNhacNho.Text = nnDTO.MaLoaiNhacNho;
                deThoiGianNhac.Text = nnDTO.ThoiGianNhac;
                deNgayHetHan.Text = nnDTO.NgayHetHan;
                txtSoQuyetDinh.Text = nnDTO.SoQuyetDinh;
                meGhiChu.Text = nnDTO.GhiChu;
                meNoiDung.Text = nnDTO.NoiDung;


            }
        }

        //private void gridViewNhacNho_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        //{
        //    int _selectedIndex = ((GridView)sender).FocusedRowHandle;
        //    if (_selectedIndex < listnn.Count)
        //    {
        //        nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
        //    }
        //    else
        //        if (listnn.Count != 0)
        //            nnSelect = listnn[0];
        //        else
        //            nnSelect = null;

        //    SetStatus(VIEW);
        //}

        private void gcNhacNho_Click(object sender, EventArgs e)
        {

        }

        private void gridViewNhacNho_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listnn.Count)
            {
                nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
            }
            else
                if (listnn.Count != 0)
                    nnSelect = listnn[0];
                else
                    nnSelect = null;

            SetStatus(VIEW);
        }

        //private void gridViewNhacNho_FocusedRowChanged_1(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        //{
        //    int _selectedIndex = ((GridView)sender).FocusedRowHandle;
        //    if (_selectedIndex < listnn.Count)
        //    {
        //        nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
        //    }
        //    else
        //        if (listnn.Count != 0)
        //            nnSelect = listnn[0];
        //        else
        //            nnSelect = null;

        //    SetStatus(VIEW);
        //}
    }
}