﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.UserSkins;
using HerculesDTO;
using HerculesDAC;
using HerculesCTL;
using System.IO;

namespace HerculesHRMT
{
    static class Program
    {
        public static TaiKhoanDTO herculesUser;
        public static bool isKetNoiServer = false;
        public static List<TinhThanhDTO> listTinhThanh = new List<TinhThanhDTO>();
        public static List<QuanHuyenDTO> listQuanHuyen = new List<QuanHuyenDTO>();
        public static List<ChucVuDTO> listChucVu = new List<ChucVuDTO>();
        public static List<PhongKhoaDTO> listPhongKhoa_NV = new List<PhongKhoaDTO>();
        public static List<BoMonBoPhanDTO> listBMBP_NV = new List<BoMonBoPhanDTO>();

        public static List<DanTocDTO> listDanToc = new List<DanTocDTO>();
        public static List<TruongDTO> listTruong = new List<TruongDTO>();

        //vdtoan add
        public static List<TrangThaiLamViecDTO> listTrangThaiLamViec = new List<TrangThaiLamViecDTO>();

        //vdtoan add 05102016
        public static List<PhongKhoaDTO> listPhongKhoa_UV = new List<PhongKhoaDTO>();
        public static List<BoMonBoPhanDTO> listBMBP_UV = new List<BoMonBoPhanDTO>();

        //vdtoan add 20102016
        public static List<PhongKhoaDTO> listPhongKhoa_HD = new List<PhongKhoaDTO>();
        public static List<BoMonBoPhanDTO> listBMBP_HD = new List<BoMonBoPhanDTO>();

        public static bool isDevelopmentEnvr;//cho biết đang chạy trên máy phòng ban hay máy devs team
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SkinManager.EnableFormSkins();
            BonusSkins.Register();
            UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            #region Load tham so chung
            //kiểm tra production/development
            string sPath = Path.GetDirectoryName(Application.ExecutablePath);
            string sAUFilePath = sPath + "\\AutoUpdate.exe";


            FileInfo newFi = new FileInfo(sAUFilePath);
            if (newFi.Exists)
            {
                isDevelopmentEnvr = false;
                DataProvider.ChangeToProductionServer();
                //nếu chạy được auto update -> production environment
                //thay đổi tên DB server
                //TODO: Khải Khải cho đường dẫn server
            }
            else
            {
                isDevelopmentEnvr = true;
                DataProvider.ChangeToDevelopmentServer();
                //else -> development environment -> local DB
            }
            //vdtoan add để test trên chạy dữ liệu server
            isDevelopmentEnvr = false;
            DataProvider.ChangeToProductionServer();

            isKetNoiServer = DataProvider.KiemTraKetNoiSERVER();
            if (isKetNoiServer)
            {
                TinhThanhCTL tinhthanhCTL = new TinhThanhCTL();
                listTinhThanh = tinhthanhCTL.GetAll();

                QuanHuyenCTL quanhuyenCTL = new QuanHuyenCTL();
                listQuanHuyen = quanhuyenCTL.GetAll();

                ChucVuCTL chucvuCTL = new ChucVuCTL();
                listChucVu = chucvuCTL.GetAll();

                PhongKhoaCTL phongkhoaCTL = new PhongKhoaCTL();
                listPhongKhoa_NV = phongkhoaCTL.GetAll();
                //vdtoan add 05102016
                listPhongKhoa_UV = phongkhoaCTL.GetAll();
                //vdtoan add 20102016
                listPhongKhoa_HD = phongkhoaCTL.GetAll();

                BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
                listBMBP_NV = bmbpCTL.GetAll();
                //vdtoan add 05102016
                listBMBP_UV = bmbpCTL.GetAll();
                //vdtoan add 20102016
                listBMBP_HD = bmbpCTL.GetAll();

                DanTocCTL danTocCtl = new DanTocCTL();
                listDanToc = danTocCtl.GetAll();

                TruongCTL truongCtl = new TruongCTL();
                listTruong = truongCtl.GetAll();

                //vdtoan add
                TrangThaiLamViecCTL trangthailamviecCTL = new TrangThaiLamViecCTL();
                listTrangThaiLamViec = trangthailamviecCTL.GetAll();
            }
            #endregion

            #region Change skin of datagridview
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
            UserLookAndFeel.Default.SetSkinStyle("Blue");
            #endregion

            Application.Run(new frmMain());
        }
    }
}