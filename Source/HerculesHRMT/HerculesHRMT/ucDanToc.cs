﻿using DevExpress.XtraEditors;
using HerculesCTL;
using System.Collections.Generic;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System;
using System.Linq;

namespace HerculesHRMT
{
    public partial class ucDanToc : XtraUserControl
    {
        DanTocCTL dtCTL = new DanTocCTL();
        List<DanTocDTO> listDanToc = new List<DanTocDTO>();
        DanTocDTO dtSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucDanToc()
        {
            InitializeComponent();
            txtTenVT.Properties.MaxLength = 50;
        }

        private void ucDanToc_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void LoadData()
        {
            listDanToc = dtCTL.GetAll();

            gcDanToc.DataSource = listDanToc;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(DanTocDTO dtDTO)
        {
            if (dtDTO != null)
            {
                txtDantoc.Text = dtDTO.TenDanToc;
                txtTenVT.Text = dtDTO.TenVietTat;
                meGhiChu.Text = dtDTO.GhiChu;
            }
        }

        private void gridViewDantoc_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listDanToc.Count)
            {
                dtSelect = ((GridView)sender).GetRow(_selectedIndex) as DanTocDTO;
            }
            else
                if (listDanToc.Count > 0)
                    dtSelect = listDanToc[0];
                else
                    dtSelect = null;

            SetStatus(VIEW);
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:                    
                    txtDantoc.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;
                    
                    txtDantoc.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtDantoc.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(dtSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtDantoc.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(dtSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtDantoc.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên dân tộc\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DanTocDTO dtNew = GetData(new DanTocDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = dtCTL.Save(dtNew);
            else
                isSucess = dtCTL.Update(dtNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listDanToc.IndexOf(listDanToc.FirstOrDefault(p => p.MaDanToc == dtNew.MaDanToc));
                gridViewDantoc.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(VIEW);
        }

        private DanTocDTO GetData(DanTocDTO danTocDTO)
        {
            string maDT = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexDT = dtCTL.GetMaxMaDanToc();
                if (sLastIndexDT.CompareTo(string.Empty) != 0)
                {                    
                    int nLastIndexTT = int.Parse(sLastIndexDT);
                    maDT += (nLastIndexTT + 1).ToString().PadLeft(2, '0');
                }
                else
                    maDT += "01";

                danTocDTO.NgayHieuLuc = DateTime.Now;
                danTocDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maDT = dtSelect.MaDanToc;
                danTocDTO.NgayHieuLuc = dtSelect.NgayHieuLuc;
                danTocDTO.TinhTrang = dtSelect.TinhTrang;
            }
            danTocDTO.MaDanToc = maDT;
            danTocDTO.TenDanToc = txtDantoc.Text;            
            danTocDTO.TenVietTat = txtTenVT.Text;
            danTocDTO.GhiChu = meGhiChu.Text;

            return danTocDTO;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dtSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa dân tộc " + dtSelect.TenDanToc + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = dtCTL.Delete(dtSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if(listDanToc.Count != 0)
                            gridViewDantoc.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
