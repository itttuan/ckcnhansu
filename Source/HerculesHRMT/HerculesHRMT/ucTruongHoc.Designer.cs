﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucTruongHoc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoaiTruong = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.txtTenTruongHocvien = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtMaTruong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gcTruongHocvien = new DevExpress.XtraGrid.GridControl();
            this.gridViewTruongHocvien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiTruong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.checkboxIsNuocNgoai = new System.Windows.Forms.CheckBox();
            this.gridColNuocNgoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTruong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTruongHocvien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTruong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTruongHocvien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTruongHocvien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.checkboxIsNuocNgoai);
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.lueLoaiTruong);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.txtTenTruongHocvien);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtMaTruong);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 199);
            this.panelControl1.TabIndex = 5;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(482, 105);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(237, 58);
            this.meGhiChu.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(383, 107);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(39, 13);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "Ghi chú:";
            // 
            // lueLoaiTruong
            // 
            this.lueLoaiTruong.Location = new System.Drawing.Point(482, 69);
            this.lueLoaiTruong.Name = "lueLoaiTruong";
            this.lueLoaiTruong.Properties.AutoHeight = false;
            this.lueLoaiTruong.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoaiTruong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiTruong.Properties.DisplayMember = "Key";
            this.lueLoaiTruong.Properties.NullText = "";
            this.lueLoaiTruong.Properties.ShowFooter = false;
            this.lueLoaiTruong.Properties.ShowHeader = false;
            this.lueLoaiTruong.Properties.ValueMember = "Value";
            this.lueLoaiTruong.Size = new System.Drawing.Size(110, 20);
            this.lueLoaiTruong.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(383, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(61, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Loại Trường:";
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(136, 143);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(214, 20);
            this.txtTenVT.TabIndex = 2;
            // 
            // txtTenTruongHocvien
            // 
            this.txtTenTruongHocvien.Location = new System.Drawing.Point(136, 104);
            this.txtTenTruongHocvien.Name = "txtTenTruongHocvien";
            this.txtTenTruongHocvien.Size = new System.Drawing.Size(214, 20);
            this.txtTenTruongHocvien.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 72);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(56, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Mã Trường:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(666, 169);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(573, 169);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(480, 169);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 7;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(387, 169);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtMaTruong
            // 
            this.txtMaTruong.Location = new System.Drawing.Point(136, 69);
            this.txtMaTruong.Name = "txtMaTruong";
            this.txtMaTruong.Size = new System.Drawing.Size(214, 20);
            this.txtMaTruong.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(30, 146);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 13);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Tên viết tắt:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 107);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Trường:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(267, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(246, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Trường - Học Viện";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcTruongHocvien);
            this.panelControl3.Location = new System.Drawing.Point(0, 200);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 323);
            this.panelControl3.TabIndex = 4;
            // 
            // gcTruongHocvien
            // 
            this.gcTruongHocvien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTruongHocvien.Location = new System.Drawing.Point(2, 2);
            this.gcTruongHocvien.MainView = this.gridViewTruongHocvien;
            this.gcTruongHocvien.Name = "gcTruongHocvien";
            this.gcTruongHocvien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcTruongHocvien.Size = new System.Drawing.Size(781, 319);
            this.gcTruongHocvien.TabIndex = 3;
            this.gcTruongHocvien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTruongHocvien});
            // 
            // gridViewTruongHocvien
            // 
            this.gridViewTruongHocvien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaTruong,
            this.gridColTenTruong,
            this.gridColLoaiTruong,
            this.gridColTenVT,
            this.gridColNuocNgoai});
            this.gridViewTruongHocvien.CustomizationFormBounds = new System.Drawing.Rectangle(742, 488, 216, 183);
            this.gridViewTruongHocvien.GridControl = this.gcTruongHocvien;
            this.gridViewTruongHocvien.GroupPanelText = " ";
            this.gridViewTruongHocvien.Name = "gridViewTruongHocvien";
            this.gridViewTruongHocvien.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewTruongHocvien_FocusedRowChanged);
            // 
            // gridColMaTruong
            // 
            this.gridColMaTruong.Caption = "Mã Trường";
            this.gridColMaTruong.FieldName = "MaTruong";
            this.gridColMaTruong.Name = "gridColMaTruong";
            this.gridColMaTruong.Visible = true;
            this.gridColMaTruong.VisibleIndex = 0;
            // 
            // gridColTenTruong
            // 
            this.gridColTenTruong.Caption = "Tên Trường";
            this.gridColTenTruong.FieldName = "TenTruong";
            this.gridColTenTruong.Name = "gridColTenTruong";
            this.gridColTenTruong.OptionsColumn.AllowEdit = false;
            this.gridColTenTruong.OptionsColumn.ReadOnly = true;
            this.gridColTenTruong.Visible = true;
            this.gridColTenTruong.VisibleIndex = 1;
            // 
            // gridColLoaiTruong
            // 
            this.gridColLoaiTruong.Caption = "Loại Trường";
            this.gridColLoaiTruong.FieldName = "TenLoaiTruong";
            this.gridColLoaiTruong.Name = "gridColLoaiTruong";
            this.gridColLoaiTruong.OptionsColumn.AllowEdit = false;
            this.gridColLoaiTruong.OptionsColumn.ReadOnly = true;
            this.gridColLoaiTruong.Visible = true;
            this.gridColLoaiTruong.VisibleIndex = 2;
            // 
            // gridColTenVT
            // 
            this.gridColTenVT.Caption = "Tên Viết Tắt";
            this.gridColTenVT.FieldName = "TenVietTat";
            this.gridColTenVT.Name = "gridColTenVT";
            this.gridColTenVT.OptionsColumn.AllowEdit = false;
            this.gridColTenVT.OptionsColumn.ReadOnly = true;
            this.gridColTenVT.Visible = true;
            this.gridColTenVT.VisibleIndex = 3;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(599, 72);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(0, 13);
            this.labelControl6.TabIndex = 10;
            // 
            // checkboxIsNuocNgoai
            // 
            this.checkboxIsNuocNgoai.AutoSize = true;
            this.checkboxIsNuocNgoai.Location = new System.Drawing.Point(605, 72);
            this.checkboxIsNuocNgoai.Name = "checkboxIsNuocNgoai";
            this.checkboxIsNuocNgoai.Size = new System.Drawing.Size(115, 17);
            this.checkboxIsNuocNgoai.TabIndex = 14;
            this.checkboxIsNuocNgoai.Text = "trường nước ngoài";
            this.checkboxIsNuocNgoai.UseVisualStyleBackColor = true;
            // 
            // gridColNuocNgoai
            // 
            this.gridColNuocNgoai.Caption = "Trường nước ngoài";
            this.gridColNuocNgoai.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColNuocNgoai.FieldName = "NuocNgoai";
            this.gridColNuocNgoai.Name = "gridColNuocNgoai";
            this.gridColNuocNgoai.Visible = true;
            this.gridColNuocNgoai.VisibleIndex = 4;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // ucTruongHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucTruongHoc";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucTruongHoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiTruong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTruongHocvien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTruong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTruongHocvien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTruongHocvien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelControl panelControl2;
        private PanelControl panelControl3;
        private GridControl gcTruongHocvien;
        private GridView gridViewTruongHocvien;
        private GridColumn gridColTenDayDu;
        private GridColumn gridColTenVietTat;
        private GridColumn gridColMaTruong;
        private GridColumn gridColTenTruong;
        private GridColumn gridColLoaiTruong;
        private GridColumn gridColTenVT;
        private PanelControl panelControl1;
        private MemoEdit meGhiChu;
        private LabelControl labelControl7;
        private LookUpEdit lueLoaiTruong;
        private LabelControl labelControl4;
        private TextEdit txtTenVT;
        private TextEdit txtTenTruongHocvien;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtMaTruong;
        private LabelControl labelControl5;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private System.Windows.Forms.CheckBox checkboxIsNuocNgoai;
        private LabelControl labelControl6;
        private GridColumn gridColNuocNgoai;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    }
}
