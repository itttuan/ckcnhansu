﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;
using System.Data;
using HerculesDTO;
using System.Windows.Forms;
using System.Collections.Generic;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    public partial class ucNVQuyetDinh : XtraUserControl
    {
        string strMaNV = "";
        QuyetDinhCTL quyetdinhCTL = new QuyetDinhCTL();
        NgachCTL ngachCTL = new NgachCTL();
        HeSoLuongCTL hesoluongCTL = new HeSoLuongCTL();
        QuyetDinhDTO quyetdinhDTO = new QuyetDinhDTO();
        List<QuyetDinhDTO> listquyetdinh = new List<QuyetDinhDTO>();
        Common.STATUS _status;
        static Boolean chaylandau4 = true;
        static Boolean chaylandau5 = true;
        List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
        NhanVienCTL nhanvienCTL = new NhanVienCTL();
        List<QuyetDinhDTO> listquyetdinh_kiemtra = new List<QuyetDinhDTO>();

        public ucNVQuyetDinh()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _status = status;
            quyetdinhDTO = new QuyetDinhDTO();
            BindingData(quyetdinhDTO);
            SetStatusComponents();
            LoadDanhSach();
        }


        private void BindingData(QuyetDinhDTO quyetdinhDTO)
        {
            txtSoQD.EditValue = quyetdinhDTO.SoQD;
        
            txtTenQD.EditValue = quyetdinhDTO.TenQD;
            lueLoaiQD.EditValue = quyetdinhDTO.MaLoaiQD;
            txtLanThu.EditValue = quyetdinhDTO.Lan;
            meNoiDung.EditValue = quyetdinhDTO.NoiDungQD;
            lueNguoiKy.EditValue = quyetdinhDTO.NguoiKy;
            deNgayKy.EditValue = quyetdinhDTO.NgayKy;
            deHieuLucTu.EditValue = quyetdinhDTO.TuNgay;
            deHieuLucDen.EditValue = quyetdinhDTO.DenNgay;

            cbbPg1MaNgach.EditValue = quyetdinhDTO.MaNgach;
            cbbPg1Bac.EditValue = quyetdinhDTO.Bac;
            txtHeSo.EditValue = quyetdinhDTO.HeSo;

            txtTileVuot.EditValue = quyetdinhDTO.TyLeVuotKhung;
            txtHeSoCV.EditValue = quyetdinhDTO.HeSoCV;
            txtPhuCap.EditValue = quyetdinhDTO.PhuCapNhaGiao;
            txtPCThamnien.EditValue = quyetdinhDTO.PhuCapThamNien;
            chbHienTaiHuong.EditValue = quyetdinhDTO.HienTaiHuong;
        }

        private void LoadDanhSach()
        {
            //load gridview quyetdinh
            listquyetdinh = quyetdinhCTL.DanhSachQuyetDinhNhanVien(strMaNV);
            gridQuyetDinh.DataSource = listquyetdinh;

            //load lueNguoiKy
            listNhanVien = nhanvienCTL.LayDanhSachTatCaNhanVien();
            if (chaylandau4 == true)
                listNhanVien.Insert(0, new NhanVienDTO());
            lueNguoiKy.Properties.DataSource = listNhanVien;
            chaylandau4 = false;

            //load lueLoaiQD
            List<LoaiQuyetDinhDTO> listloaiqd = new List<LoaiQuyetDinhDTO>();
            LoaiQuyetDinhCTL loaiqdCTL = new LoaiQuyetDinhCTL();
            listloaiqd = loaiqdCTL.GetAll();
            if (chaylandau5 == true)
                listloaiqd.Insert(0, new LoaiQuyetDinhDTO());
            chaylandau5 = false;

            lueLoaiQD.Properties.DataSource = listloaiqd;


            //load MaNgach
            cbbPg1MaNgach.Properties.DataSource = ngachCTL.LayDanhSachNgach();
        }

        private void ucNVLuong_Load(object sender, EventArgs e)
        {
            //cbbPg1MaNgach.Properties.DataSource = ngachCTL.LayDanhSachNgach();
        }

        private void cbbPg1MaNgach_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1MaNgach.EditValue != null)
            {
                cbbPg1Bac.Properties.DataSource = hesoluongCTL.LayDanhSachHeSOLuongTheoNgach(cbbPg1MaNgach.EditValue.ToString());
            }
            else
            {
                cbbPg1Bac.Properties.DataSource = null;
            }
        }

        private void cbbPg1Bac_EditValueChanged(object sender, EventArgs e)
        {
            if (cbbPg1Bac.EditValue != null)
            {
                HeSoLuongDTO o = ((HeSoLuongDTO)((LookUpEdit)sender).Properties.GetDataSourceRowByKeyValue(cbbPg1Bac.EditValue));
                if (o != null)
                {
                    txtHeSo.EditValue = o.HeSo;
                }
            }
            else
            {
                txtHeSo.EditValue = 0;
            }
        }
        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    BindingData(new QuyetDinhDTO());
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    SetReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetReadOnly(true);
                    if (quyetdinhDTO.Id > 0)
                    {
                        btnPg3Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg3Sua.Enabled = false;
                    }
                    btnPg3Them.Enabled = false;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetReadOnly(false);
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void SetReadOnly(bool read)
        {
      
            txtSoQD.Properties.ReadOnly = read;

            txtTenQD.Properties.ReadOnly = read;
            lueLoaiQD.Properties.ReadOnly = read;
            txtLanThu.Properties.ReadOnly = read;
            lueNguoiKy.Properties.ReadOnly = read;
            deNgayKy.Properties.ReadOnly = read;
            deHieuLucTu.Properties.ReadOnly = read;
            deHieuLucDen.Properties.ReadOnly = read;

            cbbPg1MaNgach.Properties.ReadOnly = read;
            cbbPg1Bac.Properties.ReadOnly = read;
            txtHeSo.Properties.ReadOnly = read;

            txtTileVuot.Properties.ReadOnly = read;
            txtHeSoCV.Properties.ReadOnly = read;

            txtPhuCap.Properties.ReadOnly = read;
            txtPCThamnien.Properties.ReadOnly = read;
            meNoiDung.Properties.ReadOnly = read;
            chbHienTaiHuong.Properties.ReadOnly = read;
        }

        private void btnPg3TaoMoi_Click(object sender, EventArgs e)
        {
            quyetdinhDTO = new QuyetDinhDTO();
            _status = Common.STATUS.NEW;
            SetStatusComponents();
        }

        private void btnPg3Sua_Click(object sender, EventArgs e)
        {
            if (quyetdinhDTO.Id > 0)
            {
                _status = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }

        private bool GetDataOncomponentsAndCheck()
        {
            //if (cbbPg1MaNgach.EditValue == null || cbbPg1Bac.EditValue == null)
            //    return false;
            quyetdinhDTO.MaNV = strMaNV;
            //quyetdinhDTO.SoQD = txtSoQD.EditValue as String;
            //quyetdinhDTO.TenQD = txtTenQD.EditValue as String;
            //quyetdinhDTO.MaLoaiQD = lueLoaiQD.EditValue as String;
            //quyetdinhDTO.Lan = txtLanThu.EditValue.ToString();
            //quyetdinhDTO.NoiDungQD = meNoiDung.EditValue as String;
            //quyetdinhDTO.NguoiKy = lueNguoiKy.EditValue as String;
            quyetdinhDTO.SoQD = txtSoQD.Text;
            quyetdinhDTO.TenQD = txtTenQD.Text;

            if (!String.IsNullOrEmpty(lueLoaiQD.Text))
            {
                quyetdinhDTO.MaLoaiQD = lueLoaiQD.EditValue.ToString();
            }
            else
                quyetdinhDTO.MaLoaiQD = null;

            quyetdinhDTO.Lan = txtLanThu.Text;
            quyetdinhDTO.NoiDungQD = meNoiDung.Text;

            if (!String.IsNullOrEmpty(lueNguoiKy.Text))
            {
                quyetdinhDTO.NguoiKy = lueNguoiKy.EditValue.ToString();
            }
            else
                quyetdinhDTO.NgayKy = null;

            if (!String.IsNullOrEmpty(deNgayKy.Text))
            {
                DateTime ngayky = DateTime.Parse(deNgayKy.EditValue.ToString());
                quyetdinhDTO.NgayKy = ngayky.ToString("yyyy-MM-dd");
            }
            else
                quyetdinhDTO.NgayKy = null;

            if (!String.IsNullOrEmpty(deNgayKy.Text))
            {
                DateTime ngayky = DateTime.Parse(deNgayKy.EditValue.ToString());
                quyetdinhDTO.NgayKy = ngayky.ToString("yyyy-MM-dd");
            }
            else
                quyetdinhDTO.NgayKy = null;

            if (!String.IsNullOrEmpty(deHieuLucTu.Text))
            {
                DateTime hieuluctu = DateTime.Parse(deHieuLucTu.EditValue.ToString());
                quyetdinhDTO.TuNgay = hieuluctu.ToString("yyyy-MM-dd");
            }
            else
                quyetdinhDTO.TuNgay = null;

            if (!String.IsNullOrEmpty(deHieuLucDen.Text))
            {
                DateTime hieulucden = DateTime.Parse(deHieuLucDen.EditValue.ToString());
                quyetdinhDTO.DenNgay = hieulucden.ToString("yyyy-MM-dd");
            }
            else
                quyetdinhDTO.DenNgay = null;

            if (!String.IsNullOrEmpty(cbbPg1MaNgach.Text))
            {
                quyetdinhDTO.MaNgach = cbbPg1MaNgach.EditValue as String;
            }
            else
                quyetdinhDTO.MaNgach = null;

            if (!String.IsNullOrEmpty(cbbPg1Bac.Text))
            {
                quyetdinhDTO.Bac = cbbPg1Bac.EditValue as String;
            }
            else
                quyetdinhDTO.Bac = "0";

            //quyetdinhDTO.Bac = cbbPg1Bac.Text;

            if (txtHeSo.EditValue != null)
                quyetdinhDTO.HeSo = Double.Parse(txtHeSo.EditValue.ToString());
            else
                quyetdinhDTO.HeSo = 0;

            if (!String.IsNullOrEmpty(txtTileVuot.Text))
                quyetdinhDTO.TyLeVuotKhung = Double.Parse(txtTileVuot.Text);
            else
                quyetdinhDTO.TyLeVuotKhung = 0;

            if (txtHeSoCV.EditValue != null)
                quyetdinhDTO.HeSoCV = Double.Parse(txtHeSoCV.EditValue.ToString());

            if (!String.IsNullOrEmpty(txtPhuCap.Text))
                quyetdinhDTO.PhuCapNhaGiao = Double.Parse(txtPhuCap.Text);
            else
                quyetdinhDTO.PhuCapNhaGiao = 0;

            if (!String.IsNullOrEmpty(txtPCThamnien.Text))
                quyetdinhDTO.PhuCapThamNien = Double.Parse(txtPCThamnien.Text);
            else
                quyetdinhDTO.PhuCapThamNien = 0;

     
            quyetdinhDTO.HienTaiHuong = chbHienTaiHuong.Checked;
   
            quyetdinhDTO.NgayHieuLuc = DateTime.Now;
            quyetdinhDTO.TinhTrang = 0;

            return true;
        }

        private void btnPg3Them_Click(object sender, EventArgs e)
        {
            if (GetDataOncomponentsAndCheck())
            {
                bool kq = false;
                if (_status == Common.STATUS.NEW)
                {
                    kq = quyetdinhCTL.Save(quyetdinhDTO);
                }
                if (_status == Common.STATUS.EDIT)
                {
                    kq = quyetdinhCTL.Update(quyetdinhDTO);
                }

                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _status = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusComponents();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg3Xoa_Click(object sender, EventArgs e)
        {
            if (quyetdinhDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (quyetdinhCTL.Delete(quyetdinhDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void gridViewQuyetDinh_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                quyetdinhDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as QuyetDinhDTO;
                if (quyetdinhDTO == null) quyetdinhDTO = new QuyetDinhDTO();
                BindingData(quyetdinhDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void gridViewQuyetDinh_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            quyetdinhDTO = ((GridView)sender).GetRow(e.RowHandle) as QuyetDinhDTO;
            if (quyetdinhDTO == null) quyetdinhDTO = new QuyetDinhDTO();
            BindingData(quyetdinhDTO);
            _status = Common.STATUS.VIEW;
            SetStatusComponents();
        }

        private void ucNVQuyetDinh_Load(object sender, EventArgs e)
        {

        }

        private void lueLoaiHD_EditValueChanged(object sender, EventArgs e)
        {
              
        }

        private void cbbPg1Bac_EditValueChanged_1(object sender, EventArgs e)
        {
            if (cbbPg1Bac.EditValue != null)
            {
                HeSoLuongDTO o = ((HeSoLuongDTO)((LookUpEdit)sender).Properties.GetDataSourceRowByKeyValue(cbbPg1Bac.EditValue));
                if (o != null)
                {
                    txtHeSo.EditValue = o.HeSo;
                }
            }
            else
            {
                txtHeSo.EditValue = 0;
            }
        }

        private void txtTenQD_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void lueLoaiQD_EditValueChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(lueLoaiQD.Text))
            {
                int LanThu = 1;

                foreach (QuyetDinhDTO quyetdinh in listquyetdinh)
                {

                    if (quyetdinh.MaLoaiQD == lueLoaiQD.EditValue.ToString())
                    {
                        if (int.Parse(quyetdinh.Lan) == 1)
                            LanThu = LanThu + 1;
                    }


                }

                txtLanThu.Text = LanThu.ToString();
            }
        }

    }
}
