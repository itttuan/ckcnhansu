﻿namespace HerculesHRMT
{
    partial class ucNVDotTuyen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcDSNhanVien = new DevExpress.XtraGrid.GridControl();
            this.gridViewDSNhanVien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColNVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditNgaySinh = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColChucvu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTinhTrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCoDotTuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaUngVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDEDSNgayS = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.grcTimKiem = new DevExpress.XtraEditors.GroupControl();
            this.btnTimKiemMaUV = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTKMaUV = new DevExpress.XtraEditors.TextEdit();
            this.btnTimKiemTheoDotTuyenDung = new DevExpress.XtraEditors.SimpleButton();
            this.lueTKMaDotTuyenDung = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnTimKiemMaNV = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTKMaNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKBPBM = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lueTKPhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).BeginInit();
            this.grcTimKiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaUV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKMaDotTuyenDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcDSNhanVien
            // 
            this.gcDSNhanVien.Location = new System.Drawing.Point(5, 148);
            this.gcDSNhanVien.MainView = this.gridViewDSNhanVien;
            this.gcDSNhanVien.Name = "gcDSNhanVien";
            this.gcDSNhanVien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDEDSNgayS,
            this.repositoryItemDateEditNgaySinh});
            this.gcDSNhanVien.Size = new System.Drawing.Size(1009, 444);
            this.gcDSNhanVien.TabIndex = 4;
            this.gcDSNhanVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDSNhanVien});
            this.gcDSNhanVien.Click += new System.EventHandler(this.gcDSNhanVien_Click);
            // 
            // gridViewDSNhanVien
            // 
            this.gridViewDSNhanVien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColNVID,
            this.gridColMaNV,
            this.gridColHo,
            this.gridColTen,
            this.gridColNgaySinh,
            this.gridColChucvu,
            this.gridColPhongKhoa,
            this.gridColBoMon,
            this.gridColTinhTrang,
            this.gridColGioiTinh,
            this.gridColEmail,
            this.gridCoDotTuyen,
            this.gridColMaUngVien,
            this.gridColThang,
            this.gridColNam});
            this.gridViewDSNhanVien.GridControl = this.gcDSNhanVien;
            this.gridViewDSNhanVien.GroupPanelText = " ";
            this.gridViewDSNhanVien.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewDSNhanVien.Name = "gridViewDSNhanVien";
            this.gridViewDSNhanVien.OptionsView.ColumnAutoWidth = false;
            this.gridViewDSNhanVien.OptionsView.ShowAutoFilterRow = true;
            // 
            // gridColNVID
            // 
            this.gridColNVID.Caption = "ID";
            this.gridColNVID.FieldName = "Id";
            this.gridColNVID.Name = "gridColNVID";
            this.gridColNVID.OptionsColumn.AllowEdit = false;
            this.gridColNVID.OptionsColumn.FixedWidth = true;
            this.gridColNVID.OptionsColumn.ReadOnly = true;
            // 
            // gridColMaNV
            // 
            this.gridColMaNV.Caption = "Mã nhân viên";
            this.gridColMaNV.FieldName = "MaNV";
            this.gridColMaNV.Name = "gridColMaNV";
            this.gridColMaNV.OptionsColumn.AllowEdit = false;
            this.gridColMaNV.OptionsColumn.FixedWidth = true;
            this.gridColMaNV.OptionsColumn.ReadOnly = true;
            this.gridColMaNV.Visible = true;
            this.gridColMaNV.VisibleIndex = 0;
            // 
            // gridColHo
            // 
            this.gridColHo.Caption = "Họ";
            this.gridColHo.FieldName = "Ho";
            this.gridColHo.Name = "gridColHo";
            this.gridColHo.OptionsColumn.AllowEdit = false;
            this.gridColHo.OptionsColumn.FixedWidth = true;
            this.gridColHo.OptionsColumn.ReadOnly = true;
            this.gridColHo.Visible = true;
            this.gridColHo.VisibleIndex = 1;
            this.gridColHo.Width = 100;
            // 
            // gridColTen
            // 
            this.gridColTen.Caption = "Tên";
            this.gridColTen.FieldName = "Ten";
            this.gridColTen.Name = "gridColTen";
            this.gridColTen.OptionsColumn.AllowEdit = false;
            this.gridColTen.OptionsColumn.FixedWidth = true;
            this.gridColTen.OptionsColumn.ReadOnly = true;
            this.gridColTen.Visible = true;
            this.gridColTen.VisibleIndex = 2;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày sinh";
            this.gridColNgaySinh.ColumnEdit = this.repositoryItemDateEditNgaySinh;
            this.gridColNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.FixedWidth = true;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 3;
            // 
            // repositoryItemDateEditNgaySinh
            // 
            this.repositoryItemDateEditNgaySinh.AutoHeight = false;
            this.repositoryItemDateEditNgaySinh.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditNgaySinh.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditNgaySinh.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditNgaySinh.Name = "repositoryItemDateEditNgaySinh";
            this.repositoryItemDateEditNgaySinh.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColChucvu
            // 
            this.gridColChucvu.Caption = "Chức vụ";
            this.gridColChucvu.FieldName = "TenChucVu";
            this.gridColChucvu.Name = "gridColChucvu";
            this.gridColChucvu.OptionsColumn.AllowEdit = false;
            this.gridColChucvu.OptionsColumn.FixedWidth = true;
            this.gridColChucvu.OptionsColumn.ReadOnly = true;
            this.gridColChucvu.Visible = true;
            this.gridColChucvu.VisibleIndex = 4;
            this.gridColChucvu.Width = 180;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng/Khoa";
            this.gridColPhongKhoa.FieldName = "TenPhongKhoa";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.OptionsColumn.AllowEdit = false;
            this.gridColPhongKhoa.OptionsColumn.FixedWidth = true;
            this.gridColPhongKhoa.OptionsColumn.ReadOnly = true;
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 5;
            this.gridColPhongKhoa.Width = 200;
            // 
            // gridColBoMon
            // 
            this.gridColBoMon.Caption = "Bộ phận/Bộ môn";
            this.gridColBoMon.FieldName = "TenBMBP";
            this.gridColBoMon.Name = "gridColBoMon";
            this.gridColBoMon.OptionsColumn.AllowEdit = false;
            this.gridColBoMon.OptionsColumn.FixedWidth = true;
            this.gridColBoMon.OptionsColumn.ReadOnly = true;
            this.gridColBoMon.Visible = true;
            this.gridColBoMon.VisibleIndex = 6;
            this.gridColBoMon.Width = 200;
            // 
            // gridColTinhTrang
            // 
            this.gridColTinhTrang.Caption = "Tình trạng làm việc";
            this.gridColTinhTrang.FieldName = "TenTrangThai";
            this.gridColTinhTrang.Name = "gridColTinhTrang";
            this.gridColTinhTrang.OptionsColumn.AllowEdit = false;
            this.gridColTinhTrang.OptionsColumn.FixedWidth = true;
            this.gridColTinhTrang.OptionsColumn.ReadOnly = true;
            this.gridColTinhTrang.Visible = true;
            this.gridColTinhTrang.VisibleIndex = 7;
            this.gridColTinhTrang.Width = 120;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Nữ";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.OptionsColumn.AllowEdit = false;
            this.gridColGioiTinh.OptionsColumn.FixedWidth = true;
            this.gridColGioiTinh.OptionsColumn.ReadOnly = true;
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 8;
            this.gridColGioiTinh.Width = 36;
            // 
            // gridColEmail
            // 
            this.gridColEmail.Caption = "Email";
            this.gridColEmail.FieldName = "Email";
            this.gridColEmail.Name = "gridColEmail";
            this.gridColEmail.OptionsColumn.AllowEdit = false;
            this.gridColEmail.OptionsColumn.FixedWidth = true;
            this.gridColEmail.OptionsColumn.ReadOnly = true;
            this.gridColEmail.Visible = true;
            this.gridColEmail.VisibleIndex = 9;
            this.gridColEmail.Width = 170;
            // 
            // gridCoDotTuyen
            // 
            this.gridCoDotTuyen.Caption = "Đợt tuyển dụng";
            this.gridCoDotTuyen.FieldName = "MaDot";
            this.gridCoDotTuyen.Name = "gridCoDotTuyen";
            this.gridCoDotTuyen.Visible = true;
            this.gridCoDotTuyen.VisibleIndex = 10;
            // 
            // gridColMaUngVien
            // 
            this.gridColMaUngVien.Caption = "Mã ứng viên";
            this.gridColMaUngVien.FieldName = "MaUngVien";
            this.gridColMaUngVien.Name = "gridColMaUngVien";
            this.gridColMaUngVien.OptionsColumn.AllowEdit = false;
            this.gridColMaUngVien.OptionsColumn.FixedWidth = true;
            this.gridColMaUngVien.OptionsColumn.ReadOnly = true;
            this.gridColMaUngVien.ToolTip = "Mã ứng viên";
            this.gridColMaUngVien.Visible = true;
            this.gridColMaUngVien.VisibleIndex = 11;
            // 
            // gridColThang
            // 
            this.gridColThang.Caption = "Tháng";
            this.gridColThang.FieldName = "Thang";
            this.gridColThang.Name = "gridColThang";
            this.gridColThang.OptionsColumn.AllowEdit = false;
            this.gridColThang.OptionsColumn.FixedWidth = true;
            this.gridColThang.OptionsColumn.ReadOnly = true;
            this.gridColThang.ToolTip = "Tháng tuyển dụng";
            this.gridColThang.Visible = true;
            this.gridColThang.VisibleIndex = 12;
            this.gridColThang.Width = 50;
            // 
            // gridColNam
            // 
            this.gridColNam.Caption = "Năm";
            this.gridColNam.FieldName = "Nam";
            this.gridColNam.Name = "gridColNam";
            this.gridColNam.OptionsColumn.AllowEdit = false;
            this.gridColNam.OptionsColumn.FixedWidth = true;
            this.gridColNam.OptionsColumn.ReadOnly = true;
            this.gridColNam.ToolTip = "Năm tuyển dụng";
            this.gridColNam.Visible = true;
            this.gridColNam.VisibleIndex = 13;
            this.gridColNam.Width = 50;
            // 
            // repositoryItemDEDSNgayS
            // 
            this.repositoryItemDEDSNgayS.AutoHeight = false;
            this.repositoryItemDEDSNgayS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDEDSNgayS.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDEDSNgayS.Name = "repositoryItemDEDSNgayS";
            this.repositoryItemDEDSNgayS.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // grcTimKiem
            // 
            this.grcTimKiem.Controls.Add(this.btnTimKiemMaUV);
            this.grcTimKiem.Controls.Add(this.labelControl3);
            this.grcTimKiem.Controls.Add(this.txtTKMaUV);
            this.grcTimKiem.Controls.Add(this.btnTimKiemTheoDotTuyenDung);
            this.grcTimKiem.Controls.Add(this.lueTKMaDotTuyenDung);
            this.grcTimKiem.Controls.Add(this.labelControl5);
            this.grcTimKiem.Controls.Add(this.btnTimKiemMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl4);
            this.grcTimKiem.Controls.Add(this.txtTKMaNV);
            this.grcTimKiem.Controls.Add(this.labelControl2);
            this.grcTimKiem.Controls.Add(this.lueTKBPBM);
            this.grcTimKiem.Controls.Add(this.labelControl1);
            this.grcTimKiem.Controls.Add(this.lueTKPhongKhoa);
            this.grcTimKiem.Location = new System.Drawing.Point(5, 5);
            this.grcTimKiem.Name = "grcTimKiem";
            this.grcTimKiem.Size = new System.Drawing.Size(1009, 139);
            this.grcTimKiem.TabIndex = 3;
            this.grcTimKiem.Text = "Tìm kiếm";
            this.grcTimKiem.Paint += new System.Windows.Forms.PaintEventHandler(this.grcTimKiem_Paint);
            // 
            // btnTimKiemMaUV
            // 
            this.btnTimKiemMaUV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemMaUV.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemMaUV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemMaUV.Location = new System.Drawing.Point(616, 61);
            this.btnTimKiemMaUV.Name = "btnTimKiemMaUV";
            this.btnTimKiemMaUV.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemMaUV.TabIndex = 14;
            this.btnTimKiemMaUV.ToolTip = "Tìm theo Mã Nhân viên";
            this.btnTimKiemMaUV.Click += new System.EventHandler(this.btnTimKiemMaUV_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(413, 66);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 15);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "Mã Ứng viên";
            // 
            // txtTKMaUV
            // 
            this.txtTKMaUV.Location = new System.Drawing.Point(486, 59);
            this.txtTKMaUV.Name = "txtTKMaUV";
            this.txtTKMaUV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKMaUV.Properties.Appearance.Options.UseFont = true;
            this.txtTKMaUV.Size = new System.Drawing.Size(124, 22);
            this.txtTKMaUV.TabIndex = 12;
            // 
            // btnTimKiemTheoDotTuyenDung
            // 
            this.btnTimKiemTheoDotTuyenDung.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemTheoDotTuyenDung.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemTheoDotTuyenDung.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemTheoDotTuyenDung.Location = new System.Drawing.Point(616, 29);
            this.btnTimKiemTheoDotTuyenDung.Name = "btnTimKiemTheoDotTuyenDung";
            this.btnTimKiemTheoDotTuyenDung.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemTheoDotTuyenDung.TabIndex = 11;
            this.btnTimKiemTheoDotTuyenDung.ToolTip = "Tìm theo Mã Nhân viên";
            this.btnTimKiemTheoDotTuyenDung.Click += new System.EventHandler(this.btnTimKiemTheoDotTuyenDung_Click);
            // 
            // lueTKMaDotTuyenDung
            // 
            this.lueTKMaDotTuyenDung.Location = new System.Drawing.Point(486, 29);
            this.lueTKMaDotTuyenDung.Name = "lueTKMaDotTuyenDung";
            this.lueTKMaDotTuyenDung.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKMaDotTuyenDung.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaDot", "Mã đợt tuyển dụng")});
            this.lueTKMaDotTuyenDung.Properties.DisplayMember = "MaDot";
            this.lueTKMaDotTuyenDung.Properties.NullText = "";
            this.lueTKMaDotTuyenDung.Properties.ValueMember = "MaDot";
            this.lueTKMaDotTuyenDung.Size = new System.Drawing.Size(124, 20);
            this.lueTKMaDotTuyenDung.TabIndex = 3;
            this.lueTKMaDotTuyenDung.EditValueChanged += new System.EventHandler(this.lueTKMaDotTuyenDung_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(396, 34);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(87, 15);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Đợt tuyển dụng";
            this.labelControl5.Click += new System.EventHandler(this.labelControl5_Click);
            // 
            // btnTimKiemMaNV
            // 
            this.btnTimKiemMaNV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemMaNV.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemMaNV.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemMaNV.Location = new System.Drawing.Point(616, 93);
            this.btnTimKiemMaNV.Name = "btnTimKiemMaNV";
            this.btnTimKiemMaNV.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemMaNV.TabIndex = 8;
            this.btnTimKiemMaNV.ToolTip = "Tìm theo Mã Nhân viên";
            this.btnTimKiemMaNV.Click += new System.EventHandler(this.btnTimKiemMaNV_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(408, 98);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(75, 15);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Mã Nhân viên";
            // 
            // txtTKMaNV
            // 
            this.txtTKMaNV.Location = new System.Drawing.Point(486, 91);
            this.txtTKMaNV.Name = "txtTKMaNV";
            this.txtTKMaNV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKMaNV.Properties.Appearance.Options.UseFont = true;
            this.txtTKMaNV.Size = new System.Drawing.Size(124, 22);
            this.txtTKMaNV.TabIndex = 4;
            this.txtTKMaNV.EditValueChanged += new System.EventHandler(this.txtTKMaNV_EditValueChanged);
            this.txtTKMaNV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTKMaNV_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(49, 66);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(92, 15);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Bộ phận/Bộ môn";
            // 
            // lueTKBPBM
            // 
            this.lueTKBPBM.Location = new System.Drawing.Point(145, 59);
            this.lueTKBPBM.Name = "lueTKBPBM";
            this.lueTKBPBM.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKBPBM.Properties.Appearance.Options.UseFont = true;
            this.lueTKBPBM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKBPBM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Bộ môn/Bộ phận")});
            this.lueTKBPBM.Properties.DisplayMember = "TenBMBP";
            this.lueTKBPBM.Properties.NullText = "";
            this.lueTKBPBM.Properties.ValueMember = "MaBMBP";
            this.lueTKBPBM.Size = new System.Drawing.Size(230, 22);
            this.lueTKBPBM.TabIndex = 2;
            this.lueTKBPBM.EditValueChanged += new System.EventHandler(this.lueTKBPBM_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(73, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(68, 15);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Phòng/Khoa";
            // 
            // lueTKPhongKhoa
            // 
            this.lueTKPhongKhoa.Location = new System.Drawing.Point(145, 27);
            this.lueTKPhongKhoa.Name = "lueTKPhongKhoa";
            this.lueTKPhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTKPhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.lueTKPhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTKPhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã PK", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Phòng/Khoa")});
            this.lueTKPhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.lueTKPhongKhoa.Properties.NullText = "";
            this.lueTKPhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.lueTKPhongKhoa.Size = new System.Drawing.Size(230, 22);
            this.lueTKPhongKhoa.TabIndex = 1;
            this.lueTKPhongKhoa.EditValueChanged += new System.EventHandler(this.lueTKPhongKhoa_EditValueChanged);
            // 
            // ucNVDotTuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcDSNhanVien);
            this.Controls.Add(this.grcTimKiem);
            this.Name = "ucNVDotTuyen";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.ucNVDotTuyenDung_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSNhanVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDEDSNgayS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcTimKiem)).EndInit();
            this.grcTimKiem.ResumeLayout(false);
            this.grcTimKiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaUV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKMaDotTuyenDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKBPBM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTKPhongKhoa.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grcTimKiem;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtTKMaNV;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueTKBPBM;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit lueTKPhongKhoa;
        private DevExpress.XtraGrid.GridControl gcDSNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDSNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNVID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaNV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChucvu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhongKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBoMon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTinhTrang;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDEDSNgayS;
        private DevExpress.XtraEditors.LookUpEdit lueTKMaDotTuyenDung;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaUngVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNam;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemTheoDotTuyenDung;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemMaUV;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtTKMaUV;
        private DevExpress.XtraGrid.Columns.GridColumn gridCoDotTuyen;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditNgaySinh;

    }
}
