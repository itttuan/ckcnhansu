﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucBoPhanBoMon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtDonVi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtThuTu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.luePhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gridViewBoPhanBoMon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenBoMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDonVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColViettat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColthutubaocao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcBoPhanBoMon = new DevExpress.XtraGrid.GridControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonVi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThuTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBoPhanBoMon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBoPhanBoMon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(489, 96);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(235, 59);
            this.meGhiChu.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(399, 99);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(637, 192);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(544, 192);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 7;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(451, 192);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 6;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(358, 192);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtDonVi
            // 
            this.txtDonVi.Location = new System.Drawing.Point(120, 96);
            this.txtDonVi.Name = "txtDonVi";
            this.txtDonVi.Size = new System.Drawing.Size(249, 20);
            this.txtDonVi.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 99);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(82, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Bộ phận/Bộ môn:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(269, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(242, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Bộ Phận - Bộ Môn";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtThuTu);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.luePhongKhoa);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtDonVi);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 222);
            this.panelControl1.TabIndex = 5;
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(120, 135);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(249, 20);
            this.txtTenVT.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(30, 138);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Tên viết tắt:";
            // 
            // txtThuTu
            // 
            this.txtThuTu.Location = new System.Drawing.Point(489, 57);
            this.txtThuTu.Name = "txtThuTu";
            this.txtThuTu.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtThuTu.Properties.Mask.EditMask = "d";
            this.txtThuTu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThuTu.Size = new System.Drawing.Size(235, 20);
            this.txtThuTu.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(399, 60);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(78, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Thứ tự báo cáo:";
            // 
            // luePhongKhoa
            // 
            this.luePhongKhoa.Location = new System.Drawing.Point(120, 57);
            this.luePhongKhoa.Name = "luePhongKhoa";
            this.luePhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã Phòng Khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên Phòng Khoa")});
            this.luePhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoa.Size = new System.Drawing.Size(249, 20);
            this.luePhongKhoa.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(30, 60);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Phòng/Khoa:";
            // 
            // gridViewBoPhanBoMon
            // 
            this.gridViewBoPhanBoMon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaBoMon,
            this.gridColTenBoMon,
            this.gridColTenDonVi,
            this.gridColViettat,
            this.gridColGhiChu,
            this.gridColthutubaocao});
            this.gridViewBoPhanBoMon.GridControl = this.gcBoPhanBoMon;
            this.gridViewBoPhanBoMon.GroupPanelText = " ";
            this.gridViewBoPhanBoMon.Name = "gridViewBoPhanBoMon";
            this.gridViewBoPhanBoMon.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBoPhanBoMon_FocusedRowChanged);
            // 
            // gridColMaBoMon
            // 
            this.gridColMaBoMon.Caption = "Mã Bộ Môn";
            this.gridColMaBoMon.FieldName = "MaBMBP";
            this.gridColMaBoMon.Name = "gridColMaBoMon";
            // 
            // gridColTenBoMon
            // 
            this.gridColTenBoMon.Caption = "Bộ Phận/Bộ Môn";
            this.gridColTenBoMon.FieldName = "TenBMBP";
            this.gridColTenBoMon.Name = "gridColTenBoMon";
            this.gridColTenBoMon.OptionsColumn.AllowEdit = false;
            this.gridColTenBoMon.OptionsColumn.ReadOnly = true;
            this.gridColTenBoMon.Visible = true;
            this.gridColTenBoMon.VisibleIndex = 1;
            this.gridColTenBoMon.Width = 151;
            // 
            // gridColTenDonVi
            // 
            this.gridColTenDonVi.Caption = "Đơn vị Phòng/Khoa";
            this.gridColTenDonVi.FieldName = "TenPhongKhoa";
            this.gridColTenDonVi.Name = "gridColTenDonVi";
            this.gridColTenDonVi.OptionsColumn.AllowEdit = false;
            this.gridColTenDonVi.OptionsColumn.ReadOnly = true;
            this.gridColTenDonVi.Visible = true;
            this.gridColTenDonVi.VisibleIndex = 0;
            this.gridColTenDonVi.Width = 151;
            // 
            // gridColViettat
            // 
            this.gridColViettat.Caption = "Tên Viết Tắt";
            this.gridColViettat.FieldName = "TenVietTat";
            this.gridColViettat.Name = "gridColViettat";
            this.gridColViettat.OptionsColumn.AllowEdit = false;
            this.gridColViettat.OptionsColumn.ReadOnly = true;
            this.gridColViettat.Visible = true;
            this.gridColViettat.VisibleIndex = 2;
            this.gridColViettat.Width = 151;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 4;
            this.gridColGhiChu.Width = 206;
            // 
            // gridColthutubaocao
            // 
            this.gridColthutubaocao.Caption = "Thứ Tự Báo Cáo";
            this.gridColthutubaocao.FieldName = "ThuTuBaoCao";
            this.gridColthutubaocao.Name = "gridColthutubaocao";
            this.gridColthutubaocao.OptionsColumn.AllowEdit = false;
            this.gridColthutubaocao.OptionsColumn.ReadOnly = true;
            this.gridColthutubaocao.Visible = true;
            this.gridColthutubaocao.VisibleIndex = 3;
            this.gridColthutubaocao.Width = 100;
            // 
            // gcBoPhanBoMon
            // 
            this.gcBoPhanBoMon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBoPhanBoMon.Location = new System.Drawing.Point(2, 2);
            this.gcBoPhanBoMon.MainView = this.gridViewBoPhanBoMon;
            this.gcBoPhanBoMon.Name = "gcBoPhanBoMon";
            this.gcBoPhanBoMon.Size = new System.Drawing.Size(777, 288);
            this.gcBoPhanBoMon.TabIndex = 3;
            this.gcBoPhanBoMon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBoPhanBoMon});
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 7;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcBoPhanBoMon);
            this.panelControl3.Location = new System.Drawing.Point(2, 224);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(781, 292);
            this.panelControl3.TabIndex = 4;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // ucBoPhanBoMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucBoPhanBoMon";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucBoPhanBoMon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonVi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThuTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBoPhanBoMon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBoPhanBoMon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtDonVi;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private PanelControl panelControl1;
        private LookUpEdit luePhongKhoa;
        private LabelControl labelControl4;
        private GridView gridViewBoPhanBoMon;
        private GridColumn gridColMaBoMon;
        private GridColumn gridColTenDonVi;
        private GridColumn gridColGhiChu;
        private GridControl gcBoPhanBoMon;
        private PanelControl panelControl2;
        private PanelControl panelControl3;
        private GridColumn gridColTenVietTat;
        private GridColumn gridColTenDayDu;
        private GridColumn gridColTenBoMon;
        private TextEdit txtThuTu;
        private LabelControl labelControl5;
        private TextEdit txtTenVT;
        private LabelControl labelControl6;
        private GridColumn gridColViettat;
        private GridColumn gridColthutubaocao;
    }
}
