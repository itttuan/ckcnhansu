﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    public partial class ucNVKhenThuongKyLuat : DevExpress.XtraEditors.XtraUserControl
    {
        string strMaNV = "";
        KhenThuongKyLuatCTL ktklCTL = new KhenThuongKyLuatCTL();
        KhenThuongKyLuatDto ktklDTO = new KhenThuongKyLuatDto();
        Common.STATUS _status;

        #region CAU HINH THEM NHAN VIEN
        // false: opened by add new staff
        // true : opened by edit staff
        //private Boolean _openedByEditStaff = false;

        //
        //private string LUU_TAM = "Lưu Tạm";

        //public delegate void SendKhenThuongKyLuat(KhenThuongKyLuatDto khenThuongKyLuatDto);
        //public SendKhenThuongKyLuat KhenThuongKyLuatSender;

        #endregion

        public ucNVKhenThuongKyLuat()
        {
            InitializeComponent();
        }
        public ucNVKhenThuongKyLuat(Boolean openedByEditStaff = true)
        {
            InitializeComponent();

            //if (openedByEditStaff) return;

            //_openedByEditStaff = openedByEditStaff;
            //btnPg4Them.Text = LUU_TAM;
            //btnPg4Sua.Visible = false;
            //btnPg4Xoa.Visible = false;
        }
        public void setMaNV (string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _status = status;
            ktklDTO = new KhenThuongKyLuatDto();
            LoadDanhSachKyLuat();
            LoadDanhSachKhenThuong();
            SetStatusComponents();
        }

        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    ktklDTO = new KhenThuongKyLuatDto();
                    BindingData(ktklDTO);
                    btnPg4Sua.Enabled = false;
                    btnPg4Them.Enabled = true;
                    setReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    setReadOnly(true);
                    if (ktklDTO.Id > 0)
                    {
                        btnPg4Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg4Sua.Enabled = false;
                    }
                    btnPg4Them.Enabled = false;
                    btnPg4TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    setReadOnly(false);
                    btnPg4Sua.Enabled = false;
                    btnPg4Them.Enabled = true;
                    btnPg4TaoMoi.Enabled = true;
                    rdbPg4HinhThuc.Properties.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void setReadOnly(bool read)
        {
            rdbPg4HinhThuc.Properties.ReadOnly = read;
            txtPg4CapQuyetDinh.Properties.ReadOnly = read;
            txtPg4NoiDung.Properties.ReadOnly = read;
            txtPg4SoQuyetDinh.Properties.ReadOnly = read;
            dtPg4TGianHieuLuc.Properties.ReadOnly = read;
        }

        private void LoadDanhSachKhenThuong()
        {
            gvPg4KhenThuong.DataSource = ktklCTL.LayDanhSachKhenThuongNhanVien(strMaNV);
        }
        private void LoadDanhSachKyLuat()
        {
            gvPg4KyLuat.DataSource = ktklCTL.LayDanhSachKyLuatNhanVien(strMaNV);
        }
        private void BindingData(KhenThuongKyLuatDto ktklDTO)
        {
            rdbPg4HinhThuc.EditValue = ktklDTO.IsKhenThuong;
            txtPg4SoQuyetDinh.EditValue = ktklDTO.SoQuyetDinh;
            dtPg4TGianHieuLuc.EditValue = ktklDTO.ThoiGianHieuLuc;
            txtPg4CapQuyetDinh.EditValue = ktklDTO.CapQuyetDinh;
            //txtPg4NoiDung.EditValue = ktklDTO.NoiDung;
            //vdtoan update 22092016
            if (ktklDTO.IsKhenThuong) // la khen thuong
                txtPg4NoiDung.EditValue = ktklDTO.NoiDung;
            else //la ky luat
                txtPg4NoiDung.EditValue = ktklDTO.LyDoKL;

        }

        private void btnPg4TaoMoi_Click(object sender, EventArgs e)
        {
            ktklDTO = new KhenThuongKyLuatDto();
            _status = Common.STATUS.NEW;
            SetStatusComponents();
            dtPg4TGianHieuLuc.EditValue = Constants.EmptyDateTimeType2;
        }

        private void gridviewKhenThuong_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                ktklDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as KhenThuongKyLuatDto;
                if (ktklDTO == null) ktklDTO = new KhenThuongKyLuatDto();
                BindingData(ktklDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void gridviewKyLuat_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ktklDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as KhenThuongKyLuatDto;
            if (ktklDTO == null) ktklDTO = new KhenThuongKyLuatDto();
            BindingData(ktklDTO);
            _status = Common.STATUS.VIEW;
            SetStatusComponents();
        }

        private void btnPg4Sua_Click(object sender, EventArgs e)
        {
            if (ktklDTO.Id > 0)
            {
                _status = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }

        public bool GetDataOnComponentsAndCheck()
        {
            int error = 0;
            ktklDTO.MaNv = strMaNV;
            if (txtPg4SoQuyetDinh.Text.Trim() == "") error = 0;
            ktklDTO.IsKhenThuong = Convert.ToBoolean(rdbPg4HinhThuc.EditValue);
            ktklDTO.SoQuyetDinh = txtPg4SoQuyetDinh.EditValue as String;
            if (dtPg4TGianHieuLuc.Text == "") error = 0;
            else
            {
                DateTime TGHL = DateTime.Parse(dtPg4TGianHieuLuc.EditValue.ToString());
                ktklDTO.ThoiGianHieuLuc = TGHL.ToString("yyyy-MM-dd");
            }
            ktklDTO.CapQuyetDinh = txtPg4CapQuyetDinh.EditValue as String;
            ktklDTO.NoiDung = txtPg4NoiDung.EditValue as String;
            return error == 0;
        }
        private void btnPg4Them_Click(object sender, EventArgs e)
        {
            
            if (GetDataOnComponentsAndCheck())
            {
                // for add new staff
                //if (_openedByEditStaff)
                //{
                //    KhenThuongKyLuatSender(ktklDTO);
                //    return;
                //}

                bool kq = false;
                if (_status == Common.STATUS.NEW)
                {
                    kq = ktklCTL.LuuMoiKhenThuongKyLuat(ktklDTO);
                }
                if (_status == Common.STATUS.EDIT)
                {
                    kq = ktklCTL.CapNhapKhenThuongKyLuat(ktklDTO);
                }
                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _status = Common.STATUS.VIEW;
                    if (ktklDTO.IsKhenThuong)
                    {
                        LoadDanhSachKhenThuong();
                    }
                    else
                    {
                        LoadDanhSachKyLuat();
                    }
                    SetStatusComponents();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg4Xoa_Click(object sender, EventArgs e)
        {
            if (ktklDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (ktklCTL.XoaKhenThuongKyLuat(ktklDTO))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (ktklDTO.IsKhenThuong)
                        {
                            LoadDanhSachKhenThuong();
                        }
                        else
                        {
                            LoadDanhSachKyLuat();
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void gridviewKhenThuong_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                ktklDTO = ((GridView)sender).GetRow(e.RowHandle) as KhenThuongKyLuatDto;
                BindingData(ktklDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void gridviewKyLuat_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                ktklDTO = ((GridView)sender).GetRow(e.RowHandle) as KhenThuongKyLuatDto;
                BindingData(ktklDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        public KhenThuongKyLuatDto GetData()
        {
            return ktklDTO;
        }
    }
}
