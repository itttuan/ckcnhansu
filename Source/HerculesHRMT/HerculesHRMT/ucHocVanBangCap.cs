﻿using DevExpress.XtraEditors;
using HerculesCTL;
using System.Collections.Generic;
using System.Linq;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace HerculesHRMT
{
    public partial class ucHocVanBangCap : XtraUserControl
    {
        LoaiBCapCChiCTL bcccCTL = new LoaiBCapCChiCTL();
        List<LoaiBCapCChiDTO> listBCCC = new List<LoaiBCapCChiDTO>();
        LoaiBCapCChiDTO bcccSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucHocVanBangCap()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listBCCC = bcccCTL.GetAll();

            gcBCCC.DataSource = listBCCC;
            btnLuu.Enabled = false;

        }

        private void ucHocVanBangCap_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void BindingData(LoaiBCapCChiDTO bcccDTO)
        {
            if (bcccDTO != null)
            {
                txtVBCC.Text = bcccDTO.TenLoai;                
                meGhiChu.Text = bcccDTO.GhiChu;
            }
        }

        private void gridViewBCCC_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listBCCC.Count)
            {
                bcccSelect = ((GridView)sender).GetRow(_selectedIndex) as LoaiBCapCChiDTO;
            }
            else
                if (listBCCC.Count != 0)
                    bcccSelect = listBCCC[0];
                else
                    bcccSelect = null;

            SetStatus(VIEW);
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtVBCC.Properties.ReadOnly = false;                    
                    meGhiChu.Properties.ReadOnly = false;

                    txtVBCC.Text = string.Empty;                    
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtVBCC.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(bcccSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtVBCC.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(bcccSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtVBCC.Text == string.Empty)
                return false;

            return true;
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên Văn bằng/Chứng chỉ", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoaiBCapCChiDTO bcccNew = GetData(new LoaiBCapCChiDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = bcccCTL.Save(bcccNew);
            else
                isSucess = bcccCTL.Update(bcccNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listBCCC.IndexOf(listBCCC.FirstOrDefault(p => p.MaLoaiBCCC == bcccNew.MaLoaiBCCC));
                gridViewBCCC.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (bcccSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa văn bằng/chứng chỉ " + bcccSelect.TenLoai + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = bcccCTL.Delete(bcccSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listBCCC.Count != 0)
                            gridViewBCCC.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private LoaiBCapCChiDTO GetData(LoaiBCapCChiDTO bcccDTO)
        {
            string maLoaiBCCC = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexBCCC = bcccCTL.GetMaxMaLoaiBCCC();
                if (sLastIndexBCCC.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexBCCC);
                    maLoaiBCCC += (nLastIndexTG + 1).ToString();
                }
                else
                    maLoaiBCCC += "1";

                bcccDTO.NgayHieuLuc = DateTime.Now;
                bcccDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maLoaiBCCC = bcccSelect.MaLoaiBCCC;
                bcccDTO.NgayHieuLuc = bcccSelect.NgayHieuLuc;
                bcccDTO.TinhTrang = bcccSelect.TinhTrang;
            }
            bcccDTO.MaLoaiBCCC = maLoaiBCCC;
            bcccDTO.TenLoai = txtVBCC.Text;            
            bcccDTO.GhiChu = meGhiChu.Text;

            return bcccDTO;
        }
    }
}
