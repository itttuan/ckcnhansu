﻿namespace HerculesHRMT
{
    partial class ucHinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucHinh));
            this.imgHinhAnh = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // imgHinhAnh
            // 
            this.imgHinhAnh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imgHinhAnh.EditValue = ((object)(resources.GetObject("imgHinhAnh.EditValue")));
            this.imgHinhAnh.Location = new System.Drawing.Point(40, 29);
            this.imgHinhAnh.Name = "imgHinhAnh";
            this.imgHinhAnh.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Properties.Appearance.Image")));
            this.imgHinhAnh.Properties.Appearance.Options.UseImage = true;
            this.imgHinhAnh.Properties.InitialImage = null;
            this.imgHinhAnh.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.imgHinhAnh.Size = new System.Drawing.Size(127, 171);
            this.imgHinhAnh.TabIndex = 97;
            this.imgHinhAnh.ToolTip = "Click để thay đổi ảnh đại diện";
            // 
            // ucHinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.imgHinhAnh);
            this.Name = "ucHinh";
            this.Size = new System.Drawing.Size(208, 235);
            ((System.ComponentModel.ISupportInitialize)(this.imgHinhAnh.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit imgHinhAnh;
    }
}
