﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    public partial class ucDanhSachUngVien : DevExpress.XtraEditors.XtraUserControl
    {
        public ucDanhSachUngVien()
        {
            InitializeComponent();
            ucUngVien1.LoadDuLieuTatCaLenUC();
            ucDotTuyen1.LoadDuLieuTatCaLenUC();
            ucNVDotTuyen1.LoadDuLieuTatCaLenUC();
        }

        private void ucDotTuyen1_Load(object sender, EventArgs e)
        {

        }

        private void ucUngVien1_Load(object sender, EventArgs e)
        {

        }

        private void ucNVDotTuyen1_Load(object sender, EventArgs e)
        {

        }
    }
}
