﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucBoPhanBoMon : XtraUserControl
    {
        BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
        PhongKhoaCTL pkCTL = new PhongKhoaCTL();
        List<BoMonBoPhanDTO> listBMBP = new List<BoMonBoPhanDTO>();
        List<PhongKhoaDTO> listPK = new List<PhongKhoaDTO>();
        BoMonBoPhanDTO bmbpSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucBoPhanBoMon()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listBMBP = bmbpCTL.GetAll();

            gcBoPhanBoMon.DataSource = listBMBP;
            btnLuu.Enabled = false;

        }

        private void BindingData(BoMonBoPhanDTO bmbpDTO)
        {
            if (bmbpDTO != null)
            {
                luePhongKhoa.EditValue = bmbpDTO.MaPK;
                txtDonVi.Text = bmbpDTO.TenBMBP;
                txtTenVT.Text = bmbpDTO.TenVietTat;
                txtThuTu.Text = bmbpDTO.ThuTuBaoCao.ToString();
                meGhiChu.Text = bmbpDTO.GhiChu;
            }
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    luePhongKhoa.Properties.ReadOnly = false;
                    txtDonVi.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtThuTu.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;


                    txtDonVi.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    txtThuTu.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    luePhongKhoa.Properties.ReadOnly = false;
                    txtDonVi.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtThuTu.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(bmbpSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    luePhongKhoa.Properties.ReadOnly = true;
                    txtDonVi.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    txtThuTu.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(bmbpSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtDonVi.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private BoMonBoPhanDTO GetData(BoMonBoPhanDTO bmbpDTO)
        {
            string maBMBP = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndex = bmbpCTL.GetMaxMaBMBP();
                if (sLastIndex.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndex);
                    maBMBP += (nLastIndexTG + 1).ToString().PadLeft(2, '0');
                }
                else
                    maBMBP += "01";

                bmbpDTO.NgayHieuLuc = DateTime.Now;
                bmbpDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maBMBP = bmbpSelect.MaBMBP;
                bmbpDTO.NgayHieuLuc = bmbpSelect.NgayHieuLuc;
                bmbpDTO.TinhTrang = bmbpSelect.TinhTrang;
            }
            bmbpDTO.MaBMBP = maBMBP;
            bmbpDTO.MaPK = luePhongKhoa.EditValue.ToString();
            bmbpDTO.TenBMBP = txtDonVi.Text;

            int result = 0;
            if (int.TryParse(txtThuTu.Text, out result))
                bmbpDTO.ThuTuBaoCao = result;
            else
                bmbpDTO.ThuTuBaoCao = null;

            bmbpDTO.TenVietTat = txtTenVT.Text;
            bmbpDTO.GhiChu = meGhiChu.Text;

            return bmbpDTO;
        }

        private void ucBoPhanBoMon_Load(object sender, System.EventArgs e)
        {
            listPK = pkCTL.GetAll();
            luePhongKhoa.Properties.DataSource = listPK;

            LoadData();
            txtTenVT.Properties.MaxLength = 10;
        }

        private void gridViewBoPhanBoMon_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listBMBP.Count)
            {
                bmbpSelect = ((GridView)sender).GetRow(_selectedIndex) as BoMonBoPhanDTO;
            }
            else
                if (listBMBP.Count != 0)
                    bmbpSelect = listBMBP[0];
                else
                    bmbpSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên Bộ phận/Bộ môn\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            BoMonBoPhanDTO bmbpNew = GetData(new BoMonBoPhanDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = bmbpCTL.Save(bmbpNew);
            else
                isSucess = bmbpCTL.Update(bmbpNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listBMBP.IndexOf(listBMBP.FirstOrDefault(p => p.MaBMBP == bmbpNew.MaBMBP));
                gridViewBoPhanBoMon.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (bmbpSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa bộ phận/bộ môn " + bmbpSelect.TenBMBP + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = bmbpCTL.Delete(bmbpSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listBMBP.Count != 0)
                            gridViewBoPhanBoMon.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }


    }
}
