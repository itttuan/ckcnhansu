﻿namespace HerculesHRMT
{
    partial class rptDSNhanSuThang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TÊN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.labelNgayLap = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.lblNguoiLap = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_TS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_ThS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_DH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_CD = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_Khac = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGT_Nu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_ThS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_DH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_CD = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_Khac = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_Nu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.iem = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_TS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_ThS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_DH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_CD = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_Khac = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_KN_Nu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_TS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_ThS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_DH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_CD = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_Khac = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellGV_TT_Nu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_TS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_ThS = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_DH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_CD = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_Khac = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTC_Nu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrCrossBandBox1 = new DevExpress.XtraReports.UI.XRCrossBandBox();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrCrossBandLine1 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine2 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine3 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine4 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine5 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine6 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine7 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine8 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine9 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine10 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine11 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine12 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine13 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine14 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine15 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.xrCrossBandLine16 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.xrCrossBandLine17 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 26.07756F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1117F, 25F);
            this.xrTable2.StylePriority.UseBorderDashStyle = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell13,
            this.xrTableCell15,
            this.TÊN,
            this.xrTableCell16,
            this.xrTableCell60,
            this.xrTableCell35,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell51,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell50});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ho")});
            this.xrTableCell12.Name = "xrTableCell12";
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell12.Summary = xrSummary1;
            this.xrTableCell12.Text = "xrTableCell12";
            this.xrTableCell12.Weight = 0.2323576056055478D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "STT_DonVi")});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.25885602345335812D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "STT_BoMon")});
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "xrTableCell13";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.2384962580933811D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ho")});
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "xrTableCell15";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 1.1452224156982254D;
            // 
            // TÊN
            // 
            this.TÊN.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TÊN.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ten")});
            this.TÊN.Name = "TÊN";
            this.TÊN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.TÊN.StylePriority.UseBorders = false;
            this.TÊN.StylePriority.UsePadding = false;
            this.TÊN.StylePriority.UseTextAlignment = false;
            this.TÊN.Text = "TÊN";
            this.TÊN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TÊN.Weight = 0.45421620057437545D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NgaySinh")});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 0.44124062909425538D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DonViIn")});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell60.StylePriority.UsePadding = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "xrTableCell60";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell60.Weight = 0.43759774173754751D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BoMon")});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UsePadding = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "xrTableCell35";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.45414935937908596D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Loai")});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.Weight = 0.33873107808301955D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TamTuyenTu")});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.Text = "xrTableCell18";
            this.xrTableCell18.Weight = 0.46675866426589374D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell19.Weight = 1.0682462313878478D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ChuyenMonBanDau")});
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "xrTableCell20";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell20.Weight = 0.80156703005255958D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TruongBanDau")});
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell51.StylePriority.UsePadding = false;
            this.xrTableCell51.Text = "xrTableCell51";
            this.xrTableCell51.Weight = 0.51885774727995071D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TrinhDo")});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "xrTableCell21";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.4594611226459992D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "GioiTinhNu")});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.Text = "xrTableCell22";
            this.xrTableCell22.Weight = 0.20256248246813663D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 0.45204199744701634D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 34F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 36F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTitle,
            this.labelNgayLap,
            this.xrLabel1});
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblTitle.LocationFloat = new DevExpress.Utils.PointFloat(1.48184F, 55.8542F);
            this.lblTitle.Multiline = true;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTitle.SizeF = new System.Drawing.SizeF(1113.435F, 28.20837F);
            this.lblTitle.StylePriority.UseFont = false;
            this.lblTitle.StylePriority.UseTextAlignment = false;
            this.lblTitle.Text = "DANH SÁCH NHÂN SỰ TRƯỜNG CĐ KT CAO THẮNG - THÁNG 09 NĂM 2015";
            this.lblTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labelNgayLap
            // 
            this.labelNgayLap.LocationFloat = new DevExpress.Utils.PointFloat(873.2916F, 10F);
            this.labelNgayLap.Multiline = true;
            this.labelNgayLap.Name = "labelNgayLap";
            this.labelNgayLap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelNgayLap.SizeF = new System.Drawing.SizeF(241.6251F, 23F);
            this.labelNgayLap.StylePriority.UseTextAlignment = false;
            this.labelNgayLap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0.481842F, 0F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(251.0417F, 49.04167F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "BỘ CÔNG THƯƠNG\r\nTRƯỜNG CĐ KỸ THUẬT CAO THẮNG\r\n------oOo------";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNguoiLap,
            this.xrLabel27,
            this.xrTable3,
            this.xrLabel2});
            this.ReportFooter.HeightF = 225.2442F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lblNguoiLap
            // 
            this.lblNguoiLap.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblNguoiLap.LocationFloat = new DevExpress.Utils.PointFloat(873.2917F, 126.25F);
            this.lblNguoiLap.Multiline = true;
            this.lblNguoiLap.Name = "lblNguoiLap";
            this.lblNguoiLap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiLap.SizeF = new System.Drawing.SizeF(134.9471F, 18.86188F);
            this.lblNguoiLap.StylePriority.UseFont = false;
            this.lblNguoiLap.StylePriority.UseTextAlignment = false;
            this.lblNguoiLap.Text = "Người lập";
            this.lblNguoiLap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(873.2916F, 34.62497F);
            this.xrLabel27.Multiline = true;
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(137.0307F, 18.86187F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Người lập";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1.481811F, 66.90344F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
            this.xrTable3.SizeF = new System.Drawing.SizeF(571.7708F, 150F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBackColor = false;
            this.xrTableCell23.Text = "LOẠI CÁN BỘ/VIÊN CHỨC";
            this.xrTableCell23.Weight = 1.98D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.Text = "TS";
            this.xrTableCell25.Weight = 0.53041702270507818D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.Text = "ThS";
            this.xrTableCell26.Weight = 0.50455612182617193D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBackColor = false;
            this.xrTableCell27.Text = "ĐH";
            this.xrTableCell27.Weight = 0.48502716064453144D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.Text = "CĐ";
            this.xrTableCell28.Weight = 0.49427001953124994D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBackColor = false;
            this.xrTableCell29.Text = "Khác";
            this.xrTableCell29.Weight = 0.48671844482421867D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBackColor = false;
            this.xrTableCell30.Text = "Cộng";
            this.xrTableCell30.Weight = 0.85794250488281232D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBackColor = false;
            this.xrTableCell31.Text = "Nữ";
            this.xrTableCell31.Weight = 0.37877624511718733D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.cellGT_TS,
            this.cellGT_ThS,
            this.cellGT_DH,
            this.cellGT_CD,
            this.cellGT_Khac,
            this.cellGT_Cong,
            this.cellGT_Nu});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "I. GIÁN TIẾP (NGHIỆP VỤ)";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell32.Weight = 1.98D;
            // 
            // cellGT_TS
            // 
            this.cellGT_TS.Name = "cellGT_TS";
            this.cellGT_TS.Weight = 0.53041702270507818D;
            // 
            // cellGT_ThS
            // 
            this.cellGT_ThS.Name = "cellGT_ThS";
            this.cellGT_ThS.Weight = 0.50455612182617193D;
            // 
            // cellGT_DH
            // 
            this.cellGT_DH.Name = "cellGT_DH";
            this.cellGT_DH.Weight = 0.48502716064453144D;
            // 
            // cellGT_CD
            // 
            this.cellGT_CD.Name = "cellGT_CD";
            this.cellGT_CD.Weight = 0.49427001953124994D;
            // 
            // cellGT_Khac
            // 
            this.cellGT_Khac.Name = "cellGT_Khac";
            this.cellGT_Khac.Weight = 0.48671844482421867D;
            // 
            // cellGT_Cong
            // 
            this.cellGT_Cong.Name = "cellGT_Cong";
            this.cellGT_Cong.Weight = 0.85794250488281232D;
            // 
            // cellGT_Nu
            // 
            this.cellGT_Nu.Name = "cellGT_Nu";
            this.cellGT_Nu.Weight = 0.37877624511718733D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.cellGV_TS,
            this.cellGV_ThS,
            this.cellGV_DH,
            this.cellGV_CD,
            this.cellGV_Khac,
            this.cellGV_Cong,
            this.cellGV_Nu});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.StylePriority.UsePadding = false;
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = "II. GIẢNG VIÊN";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 1.98D;
            // 
            // cellGV_TS
            // 
            this.cellGV_TS.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_TS.Name = "cellGV_TS";
            this.cellGV_TS.StylePriority.UseBorders = false;
            this.cellGV_TS.Weight = 0.53041702270507818D;
            // 
            // cellGV_ThS
            // 
            this.cellGV_ThS.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_ThS.Name = "cellGV_ThS";
            this.cellGV_ThS.StylePriority.UseBorders = false;
            this.cellGV_ThS.Weight = 0.50455612182617193D;
            // 
            // cellGV_DH
            // 
            this.cellGV_DH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_DH.Name = "cellGV_DH";
            this.cellGV_DH.StylePriority.UseBorders = false;
            this.cellGV_DH.Weight = 0.48502716064453144D;
            // 
            // cellGV_CD
            // 
            this.cellGV_CD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_CD.Name = "cellGV_CD";
            this.cellGV_CD.StylePriority.UseBorders = false;
            this.cellGV_CD.Weight = 0.49427001953124994D;
            // 
            // cellGV_Khac
            // 
            this.cellGV_Khac.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_Khac.Name = "cellGV_Khac";
            this.cellGV_Khac.StylePriority.UseBorders = false;
            this.cellGV_Khac.Weight = 0.48671844482421867D;
            // 
            // cellGV_Cong
            // 
            this.cellGV_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_Cong.Name = "cellGV_Cong";
            this.cellGV_Cong.StylePriority.UseBorders = false;
            this.cellGV_Cong.Weight = 0.85794250488281232D;
            // 
            // cellGV_Nu
            // 
            this.cellGV_Nu.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_Nu.Name = "cellGV_Nu";
            this.cellGV_Nu.StylePriority.UseBorders = false;
            this.cellGV_Nu.Weight = 0.37877624511718733D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.iem,
            this.cellGV_KN_TS,
            this.cellGV_KN_ThS,
            this.cellGV_KN_DH,
            this.cellGV_KN_CD,
            this.cellGV_KN_Khac,
            this.cellGV_KN_Cong,
            this.cellGV_KN_Nu});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // iem
            // 
            this.iem.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.iem.Name = "iem";
            this.iem.Padding = new DevExpress.XtraPrinting.PaddingInfo(25, 0, 0, 0, 100F);
            this.iem.StylePriority.UseBorders = false;
            this.iem.StylePriority.UsePadding = false;
            this.iem.StylePriority.UseTextAlignment = false;
            this.iem.Text = "1. Kiêm nhiệm";
            this.iem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.iem.Weight = 1.98D;
            // 
            // cellGV_KN_TS
            // 
            this.cellGV_KN_TS.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_TS.Name = "cellGV_KN_TS";
            this.cellGV_KN_TS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_TS.StylePriority.UseBorders = false;
            this.cellGV_KN_TS.StylePriority.UsePadding = false;
            this.cellGV_KN_TS.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_TS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_TS.Weight = 0.53041702270507818D;
            // 
            // cellGV_KN_ThS
            // 
            this.cellGV_KN_ThS.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_ThS.Name = "cellGV_KN_ThS";
            this.cellGV_KN_ThS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_ThS.StylePriority.UseBorders = false;
            this.cellGV_KN_ThS.StylePriority.UsePadding = false;
            this.cellGV_KN_ThS.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_ThS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_ThS.Weight = 0.50455612182617193D;
            // 
            // cellGV_KN_DH
            // 
            this.cellGV_KN_DH.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_DH.Name = "cellGV_KN_DH";
            this.cellGV_KN_DH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_DH.StylePriority.UseBorders = false;
            this.cellGV_KN_DH.StylePriority.UsePadding = false;
            this.cellGV_KN_DH.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_DH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_DH.Weight = 0.48502716064453144D;
            // 
            // cellGV_KN_CD
            // 
            this.cellGV_KN_CD.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_CD.Name = "cellGV_KN_CD";
            this.cellGV_KN_CD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_CD.StylePriority.UseBorders = false;
            this.cellGV_KN_CD.StylePriority.UsePadding = false;
            this.cellGV_KN_CD.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_CD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_CD.Weight = 0.49427001953124994D;
            // 
            // cellGV_KN_Khac
            // 
            this.cellGV_KN_Khac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_Khac.Name = "cellGV_KN_Khac";
            this.cellGV_KN_Khac.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_Khac.StylePriority.UseBorders = false;
            this.cellGV_KN_Khac.StylePriority.UsePadding = false;
            this.cellGV_KN_Khac.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_Khac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_Khac.Weight = 0.48671844482421867D;
            // 
            // cellGV_KN_Cong
            // 
            this.cellGV_KN_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_Cong.Name = "cellGV_KN_Cong";
            this.cellGV_KN_Cong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_Cong.StylePriority.UseBorders = false;
            this.cellGV_KN_Cong.StylePriority.UsePadding = false;
            this.cellGV_KN_Cong.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_Cong.Weight = 0.85794250488281232D;
            // 
            // cellGV_KN_Nu
            // 
            this.cellGV_KN_Nu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.cellGV_KN_Nu.Name = "cellGV_KN_Nu";
            this.cellGV_KN_Nu.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_KN_Nu.StylePriority.UseBorders = false;
            this.cellGV_KN_Nu.StylePriority.UsePadding = false;
            this.cellGV_KN_Nu.StylePriority.UseTextAlignment = false;
            this.cellGV_KN_Nu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_KN_Nu.Weight = 0.37877624511718733D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.cellGV_TT_TS,
            this.cellGV_TT_ThS,
            this.cellGV_TT_DH,
            this.cellGV_TT_CD,
            this.cellGV_TT_Khac,
            this.cellGV_TT_Cong,
            this.cellGV_TT_Nu});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(25, 0, 0, 0, 100F);
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UsePadding = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "2. Trực tiếp";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell59.Weight = 1.98D;
            // 
            // cellGV_TT_TS
            // 
            this.cellGV_TT_TS.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_TS.Name = "cellGV_TT_TS";
            this.cellGV_TT_TS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_TS.StylePriority.UseBorders = false;
            this.cellGV_TT_TS.StylePriority.UsePadding = false;
            this.cellGV_TT_TS.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_TS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_TS.Weight = 0.53041702270507818D;
            // 
            // cellGV_TT_ThS
            // 
            this.cellGV_TT_ThS.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_ThS.Name = "cellGV_TT_ThS";
            this.cellGV_TT_ThS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_ThS.StylePriority.UseBorders = false;
            this.cellGV_TT_ThS.StylePriority.UsePadding = false;
            this.cellGV_TT_ThS.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_ThS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_ThS.Weight = 0.50455612182617193D;
            // 
            // cellGV_TT_DH
            // 
            this.cellGV_TT_DH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_DH.Name = "cellGV_TT_DH";
            this.cellGV_TT_DH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_DH.StylePriority.UseBorders = false;
            this.cellGV_TT_DH.StylePriority.UsePadding = false;
            this.cellGV_TT_DH.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_DH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_DH.Weight = 0.48502716064453144D;
            // 
            // cellGV_TT_CD
            // 
            this.cellGV_TT_CD.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_CD.Name = "cellGV_TT_CD";
            this.cellGV_TT_CD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_CD.StylePriority.UseBorders = false;
            this.cellGV_TT_CD.StylePriority.UsePadding = false;
            this.cellGV_TT_CD.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_CD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_CD.Weight = 0.49427001953124994D;
            // 
            // cellGV_TT_Khac
            // 
            this.cellGV_TT_Khac.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_Khac.Name = "cellGV_TT_Khac";
            this.cellGV_TT_Khac.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_Khac.StylePriority.UseBorders = false;
            this.cellGV_TT_Khac.StylePriority.UsePadding = false;
            this.cellGV_TT_Khac.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_Khac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_Khac.Weight = 0.48671844482421867D;
            // 
            // cellGV_TT_Cong
            // 
            this.cellGV_TT_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_Cong.Name = "cellGV_TT_Cong";
            this.cellGV_TT_Cong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_Cong.StylePriority.UseBorders = false;
            this.cellGV_TT_Cong.StylePriority.UsePadding = false;
            this.cellGV_TT_Cong.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_Cong.Weight = 0.85794250488281232D;
            // 
            // cellGV_TT_Nu
            // 
            this.cellGV_TT_Nu.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.cellGV_TT_Nu.Name = "cellGV_TT_Nu";
            this.cellGV_TT_Nu.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.cellGV_TT_Nu.StylePriority.UseBorders = false;
            this.cellGV_TT_Nu.StylePriority.UsePadding = false;
            this.cellGV_TT_Nu.StylePriority.UseTextAlignment = false;
            this.cellGV_TT_Nu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellGV_TT_Nu.Weight = 0.37877624511718733D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.cellTC_TS,
            this.cellTC_ThS,
            this.cellTC_DH,
            this.cellTC_CD,
            this.cellTC_Khac,
            this.cellTC_Cong,
            this.cellTC_Nu});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "Tổng cộng:";
            this.xrTableCell68.Weight = 1.98D;
            // 
            // cellTC_TS
            // 
            this.cellTC_TS.Name = "cellTC_TS";
            this.cellTC_TS.Weight = 0.53041702270507818D;
            // 
            // cellTC_ThS
            // 
            this.cellTC_ThS.Name = "cellTC_ThS";
            this.cellTC_ThS.Weight = 0.50455612182617193D;
            // 
            // cellTC_DH
            // 
            this.cellTC_DH.Name = "cellTC_DH";
            this.cellTC_DH.Weight = 0.48502716064453144D;
            // 
            // cellTC_CD
            // 
            this.cellTC_CD.Name = "cellTC_CD";
            this.cellTC_CD.Weight = 0.49427001953124994D;
            // 
            // cellTC_Khac
            // 
            this.cellTC_Khac.Name = "cellTC_Khac";
            this.cellTC_Khac.Weight = 0.48671844482421867D;
            // 
            // cellTC_Cong
            // 
            this.cellTC_Cong.Name = "cellTC_Cong";
            this.cellTC_Cong.Weight = 0.85794250488281232D;
            // 
            // cellTC_Nu
            // 
            this.cellTC_Nu.Name = "cellTC_Nu";
            this.cellTC_Nu.Weight = 0.37877624511718733D;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1.481811F, 25.2785F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(571.7708F, 28.20834F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "TỔNG HỢP TRÌNH ĐỘ CHUYÊN MÔN KỸ THUẬT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 38.61311F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1.48181F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(1115.518F, 23F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1117F, 25F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell33,
            this.xrTableCell24,
            this.xrTableCell42,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell34});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "STT";
            this.xrTableCell1.Weight = 0.23321888448324948D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "TĐV";
            this.xrTableCell2.Weight = 0.25981554611372959D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "TBM";
            this.xrTableCell4.Weight = 0.23938029822706636D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "HỌ VÀ TÊN";
            this.xrTableCell3.Weight = 1.605367081603003D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Ngày sinh";
            this.xrTableCell5.Weight = 0.44287662622432111D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "Phòng / Khoa";
            this.xrTableCell33.Weight = 0.43921960130891635D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "B.Môn / B.Phận";
            this.xrTableCell24.Weight = 0.455832767874824D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "Loại";
            this.xrTableCell42.Weight = 0.33998666213576845D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Tạm tuyển từ";
            this.xrTableCell6.Weight = 0.46848879744282412D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Kiêm nhiệm";
            this.xrTableCell7.Weight = 1.072205493233902D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "Chuyên ngành CD-DH ban đầu\r\n";
            this.xrTableCell8.Weight = 0.80453911957543223D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "ĐH/CĐ\r\nban đầu";
            this.xrTableCell10.Weight = 0.520780495874969D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "TrĐộ ch.Môn \r\nTTế";
            this.xrTableCell9.Weight = 0.4611620745135569D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Nữ";
            this.xrTableCell11.Weight = 0.20331323823336422D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Multiline = true;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "Ghi chú\r\n";
            this.xrTableCell34.Weight = 0.45371988510054145D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.HeightF = 14.16218F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrCrossBandBox1
            // 
            this.xrCrossBandBox1.EndBand = this.GroupFooter1;
            this.xrCrossBandBox1.EndPointFloat = new DevExpress.Utils.PointFloat(0F, 1.99999F);
            this.xrCrossBandBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.916122F);
            this.xrCrossBandBox1.Name = "xrCrossBandBox1";
            this.xrCrossBandBox1.StartBand = this.GroupHeader1;
            this.xrCrossBandBox1.StartPointFloat = new DevExpress.Utils.PointFloat(0F, 0.916122F);
            this.xrCrossBandBox1.WidthF = 1117F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupHeader1.HeightF = 27.36367F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrCrossBandLine1
            // 
            this.xrCrossBandLine1.EndBand = this.GroupFooter1;
            this.xrCrossBandLine1.EndPointFloat = new DevExpress.Utils.PointFloat(890.5F, 1.999993F);
            this.xrCrossBandLine1.LocationFloat = new DevExpress.Utils.PointFloat(890.5F, 2.894132F);
            this.xrCrossBandLine1.Name = "xrCrossBandLine1";
            this.xrCrossBandLine1.StartBand = this.GroupHeader1;
            this.xrCrossBandLine1.StartPointFloat = new DevExpress.Utils.PointFloat(890.5F, 2.894132F);
            this.xrCrossBandLine1.WidthF = 1.000061F;
            // 
            // xrCrossBandLine2
            // 
            this.xrCrossBandLine2.EndBand = this.GroupFooter1;
            this.xrCrossBandLine2.EndPointFloat = new DevExpress.Utils.PointFloat(775F, 1.999993F);
            this.xrCrossBandLine2.LocationFloat = new DevExpress.Utils.PointFloat(775F, 2.747845F);
            this.xrCrossBandLine2.Name = "xrCrossBandLine2";
            this.xrCrossBandLine2.StartBand = this.GroupHeader1;
            this.xrCrossBandLine2.StartPointFloat = new DevExpress.Utils.PointFloat(775F, 2.747845F);
            this.xrCrossBandLine2.WidthF = 1F;
            // 
            // xrCrossBandLine3
            // 
            this.xrCrossBandLine3.EndBand = this.GroupFooter1;
            this.xrCrossBandLine3.EndPointFloat = new DevExpress.Utils.PointFloat(626F, 1.999993F);
            this.xrCrossBandLine3.LocationFloat = new DevExpress.Utils.PointFloat(626F, 2.556807F);
            this.xrCrossBandLine3.Name = "xrCrossBandLine3";
            this.xrCrossBandLine3.StartBand = this.GroupHeader1;
            this.xrCrossBandLine3.StartPointFloat = new DevExpress.Utils.PointFloat(626F, 2.556807F);
            this.xrCrossBandLine3.WidthF = 1F;
            // 
            // xrCrossBandLine4
            // 
            this.xrCrossBandLine4.EndBand = this.GroupFooter1;
            this.xrCrossBandLine4.EndPointFloat = new DevExpress.Utils.PointFloat(559.5F, 1.999993F);
            this.xrCrossBandLine4.LocationFloat = new DevExpress.Utils.PointFloat(559.5F, 3.372021F);
            this.xrCrossBandLine4.Name = "xrCrossBandLine4";
            this.xrCrossBandLine4.StartBand = this.GroupHeader1;
            this.xrCrossBandLine4.StartPointFloat = new DevExpress.Utils.PointFloat(559.5F, 3.372021F);
            this.xrCrossBandLine4.WidthF = 1.000061F;
            // 
            // xrCrossBandLine5
            // 
            this.xrCrossBandLine5.EndBand = this.GroupFooter1;
            this.xrCrossBandLine5.EndPointFloat = new DevExpress.Utils.PointFloat(450F, 1.999993F);
            this.xrCrossBandLine5.LocationFloat = new DevExpress.Utils.PointFloat(450F, 3.210383F);
            this.xrCrossBandLine5.Name = "xrCrossBandLine5";
            this.xrCrossBandLine5.StartBand = this.GroupHeader1;
            this.xrCrossBandLine5.StartPointFloat = new DevExpress.Utils.PointFloat(450F, 3.210383F);
            this.xrCrossBandLine5.WidthF = 1F;
            // 
            // xrCrossBandLine6
            // 
            this.xrCrossBandLine6.EndBand = this.GroupFooter1;
            this.xrCrossBandLine6.EndPointFloat = new DevExpress.Utils.PointFloat(387.5F, 1.999993F);
            this.xrCrossBandLine6.LocationFloat = new DevExpress.Utils.PointFloat(387.5F, 2.802776F);
            this.xrCrossBandLine6.Name = "xrCrossBandLine6";
            this.xrCrossBandLine6.StartBand = this.GroupHeader1;
            this.xrCrossBandLine6.StartPointFloat = new DevExpress.Utils.PointFloat(387.5F, 2.802776F);
            this.xrCrossBandLine6.WidthF = 1F;
            // 
            // xrCrossBandLine7
            // 
            this.xrCrossBandLine7.EndBand = this.GroupFooter1;
            this.xrCrossBandLine7.EndPointFloat = new DevExpress.Utils.PointFloat(325F, 1.999993F);
            this.xrCrossBandLine7.LocationFloat = new DevExpress.Utils.PointFloat(325F, 3.210383F);
            this.xrCrossBandLine7.Name = "xrCrossBandLine7";
            this.xrCrossBandLine7.StartBand = this.GroupHeader1;
            this.xrCrossBandLine7.StartPointFloat = new DevExpress.Utils.PointFloat(325F, 3.210383F);
            this.xrCrossBandLine7.WidthF = 1F;
            // 
            // xrCrossBandLine8
            // 
            this.xrCrossBandLine8.EndBand = this.GroupFooter1;
            this.xrCrossBandLine8.EndPointFloat = new DevExpress.Utils.PointFloat(31.56355F, 2.000013F);
            this.xrCrossBandLine8.LocationFloat = new DevExpress.Utils.PointFloat(31.56355F, 2.53382F);
            this.xrCrossBandLine8.Name = "xrCrossBandLine8";
            this.xrCrossBandLine8.StartBand = this.GroupHeader1;
            this.xrCrossBandLine8.StartPointFloat = new DevExpress.Utils.PointFloat(31.56355F, 2.53382F);
            this.xrCrossBandLine8.WidthF = 1.033056F;
            // 
            // xrCrossBandLine9
            // 
            this.xrCrossBandLine9.EndBand = this.GroupFooter1;
            this.xrCrossBandLine9.EndPointFloat = new DevExpress.Utils.PointFloat(67.84072F, 2.000013F);
            this.xrCrossBandLine9.LocationFloat = new DevExpress.Utils.PointFloat(67.84072F, 2.941447F);
            this.xrCrossBandLine9.Name = "xrCrossBandLine9";
            this.xrCrossBandLine9.StartBand = this.GroupHeader1;
            this.xrCrossBandLine9.StartPointFloat = new DevExpress.Utils.PointFloat(67.84072F, 2.941447F);
            this.xrCrossBandLine9.WidthF = 1.033058F;
            // 
            // xrCrossBandLine10
            // 
            this.xrCrossBandLine10.EndBand = this.GroupFooter1;
            this.xrCrossBandLine10.EndPointFloat = new DevExpress.Utils.PointFloat(101.2646F, 2.000013F);
            this.xrCrossBandLine10.LocationFloat = new DevExpress.Utils.PointFloat(101.2646F, 2.941447F);
            this.xrCrossBandLine10.Name = "xrCrossBandLine10";
            this.xrCrossBandLine10.StartBand = this.GroupHeader1;
            this.xrCrossBandLine10.StartPointFloat = new DevExpress.Utils.PointFloat(101.2646F, 2.941447F);
            this.xrCrossBandLine10.WidthF = 1.033058F;
            // 
            // xrCrossBandLine11
            // 
            this.xrCrossBandLine11.EndBand = this.GroupFooter1;
            this.xrCrossBandLine11.EndPointFloat = new DevExpress.Utils.PointFloat(322F, 1.999993F);
            this.xrCrossBandLine11.LocationFloat = new DevExpress.Utils.PointFloat(322F, 2.802776F);
            this.xrCrossBandLine11.Name = "xrCrossBandLine11";
            this.xrCrossBandLine11.StartBand = this.GroupHeader1;
            this.xrCrossBandLine11.StartPointFloat = new DevExpress.Utils.PointFloat(322F, 2.802776F);
            this.xrCrossBandLine11.WidthF = 1.000031F;
            // 
            // xrCrossBandLine12
            // 
            this.xrCrossBandLine12.EndBand = this.GroupFooter1;
            this.xrCrossBandLine12.EndPointFloat = new DevExpress.Utils.PointFloat(772.5001F, 1.343682F);
            this.xrCrossBandLine12.LocationFloat = new DevExpress.Utils.PointFloat(772.5001F, 2.600255F);
            this.xrCrossBandLine12.Name = "xrCrossBandLine12";
            this.xrCrossBandLine12.StartBand = this.GroupHeader1;
            this.xrCrossBandLine12.StartPointFloat = new DevExpress.Utils.PointFloat(772.5001F, 2.600255F);
            this.xrCrossBandLine12.WidthF = 1F;
            // 
            // xrCrossBandLine13
            // 
            this.xrCrossBandLine13.EndBand = this.GroupFooter1;
            this.xrCrossBandLine13.EndPointFloat = new DevExpress.Utils.PointFloat(961.4999F, 1.960649F);
            this.xrCrossBandLine13.LocationFloat = new DevExpress.Utils.PointFloat(961.4999F, 2.969821F);
            this.xrCrossBandLine13.Name = "xrCrossBandLine13";
            this.xrCrossBandLine13.StartBand = this.GroupHeader1;
            this.xrCrossBandLine13.StartPointFloat = new DevExpress.Utils.PointFloat(961.4999F, 2.969821F);
            this.xrCrossBandLine13.WidthF = 1.000061F;
            // 
            // xrCrossBandLine14
            // 
            this.xrCrossBandLine14.EndBand = this.GroupFooter1;
            this.xrCrossBandLine14.EndPointFloat = new DevExpress.Utils.PointFloat(887.5F, 1.999993F);
            this.xrCrossBandLine14.LocationFloat = new DevExpress.Utils.PointFloat(887.5F, 2.894132F);
            this.xrCrossBandLine14.Name = "xrCrossBandLine14";
            this.xrCrossBandLine14.StartBand = this.GroupHeader1;
            this.xrCrossBandLine14.StartPointFloat = new DevExpress.Utils.PointFloat(887.5F, 2.894132F);
            this.xrCrossBandLine14.WidthF = 1F;
            // 
            // xrCrossBandLine15
            // 
            this.xrCrossBandLine15.EndBand = this.GroupFooter1;
            this.xrCrossBandLine15.EndPointFloat = new DevExpress.Utils.PointFloat(1024.255F, 1.615985F);
            this.xrCrossBandLine15.LocationFloat = new DevExpress.Utils.PointFloat(1024.255F, 2.625156F);
            this.xrCrossBandLine15.Name = "xrCrossBandLine15";
            this.xrCrossBandLine15.StartBand = this.GroupHeader1;
            this.xrCrossBandLine15.StartPointFloat = new DevExpress.Utils.PointFloat(1024.255F, 2.625156F);
            this.xrCrossBandLine15.WidthF = 1F;
            // 
            // xrCrossBandLine16
            // 
            this.xrCrossBandLine16.EndBand = this.GroupFooter1;
            this.xrCrossBandLine16.EndPointFloat = new DevExpress.Utils.PointFloat(1053.207F, 2.305317F);
            this.xrCrossBandLine16.LocationFloat = new DevExpress.Utils.PointFloat(1053.207F, 2.663438F);
            this.xrCrossBandLine16.Name = "xrCrossBandLine16";
            this.xrCrossBandLine16.StartBand = this.GroupHeader1;
            this.xrCrossBandLine16.StartPointFloat = new DevExpress.Utils.PointFloat(1053.207F, 2.663438F);
            this.xrCrossBandLine16.WidthF = 1.041626F;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(HerculesDTO.ThongTinNhanSuDTO);
            // 
            // xrCrossBandLine17
            // 
            this.xrCrossBandLine17.EndBand = this.GroupFooter1;
            this.xrCrossBandLine17.EndPointFloat = new DevExpress.Utils.PointFloat(512.5F, 0.9999965F);
            this.xrCrossBandLine17.LocationFloat = new DevExpress.Utils.PointFloat(512.5F, 2.424569F);
            this.xrCrossBandLine17.Name = "xrCrossBandLine17";
            this.xrCrossBandLine17.StartBand = this.GroupHeader1;
            this.xrCrossBandLine17.StartPointFloat = new DevExpress.Utils.PointFloat(512.5F, 2.424569F);
            this.xrCrossBandLine17.WidthF = 1.000061F;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // rptDSNhanSuThang
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.PageFooter,
            this.GroupFooter1,
            this.GroupHeader1});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandLine17,
            this.xrCrossBandLine16,
            this.xrCrossBandLine15,
            this.xrCrossBandLine14,
            this.xrCrossBandLine13,
            this.xrCrossBandLine12,
            this.xrCrossBandLine11,
            this.xrCrossBandLine10,
            this.xrCrossBandLine9,
            this.xrCrossBandLine8,
            this.xrCrossBandLine7,
            this.xrCrossBandLine6,
            this.xrCrossBandLine5,
            this.xrCrossBandLine4,
            this.xrCrossBandLine3,
            this.xrCrossBandLine2,
            this.xrCrossBandLine1,
            this.xrCrossBandBox1});
            this.DataSource = this.bindingSource1;
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(23, 28, 34, 36);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblTitle;
        private DevExpress.XtraReports.UI.XRLabel labelNgayLap;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiLap;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_TS;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_ThS;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_DH;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_CD;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_Khac;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_Cong;
        private DevExpress.XtraReports.UI.XRTableCell cellGT_Nu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_ThS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_DH;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_CD;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_Khac;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_Cong;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_Nu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell iem;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_TS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_ThS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_DH;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_CD;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_Khac;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_Cong;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_KN_Nu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_TS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_ThS;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_DH;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_CD;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_Khac;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_Cong;
        private DevExpress.XtraReports.UI.XRTableCell cellGV_TT_Nu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_TS;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_ThS;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_DH;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_CD;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_Khac;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_Cong;
        private DevExpress.XtraReports.UI.XRTableCell cellTC_Nu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRCrossBandBox xrCrossBandBox1;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine1;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine2;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine3;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine4;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine5;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine6;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine7;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine8;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine9;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine10;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine11;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine12;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell TÊN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine13;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine14;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine15;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine17;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
    }
}
