﻿namespace HerculesHRMT
{
    partial class frmucNhacNho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.grcDotTuyen = new DevExpress.XtraEditors.GroupControl();
            this.deNgayHetHan = new DevExpress.XtraEditors.DateEdit();
            this.deThoiGianNhac = new DevExpress.XtraEditors.DateEdit();
            this.meNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoQuyetDinh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cbbeLoaiNhacNho = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridColThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayGio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaDot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewNhacNho = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThoiGianNhac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayKetThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcNhacNho = new DevExpress.XtraGrid.GridControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::HerculesHRMT.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).BeginInit();
            this.grcDotTuyen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQuyetDinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeLoaiNhacNho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNhacNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNhacNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(89, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(11, 15);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "ID";
            // 
            // grcDotTuyen
            // 
            this.grcDotTuyen.Controls.Add(this.deNgayHetHan);
            this.grcDotTuyen.Controls.Add(this.deThoiGianNhac);
            this.grcDotTuyen.Controls.Add(this.meNoiDung);
            this.grcDotTuyen.Controls.Add(this.labelControl8);
            this.grcDotTuyen.Controls.Add(this.txtSoQuyetDinh);
            this.grcDotTuyen.Controls.Add(this.labelControl5);
            this.grcDotTuyen.Controls.Add(this.labelControl4);
            this.grcDotTuyen.Controls.Add(this.labelControl2);
            this.grcDotTuyen.Controls.Add(this.labelControl3);
            this.grcDotTuyen.Controls.Add(this.meGhiChu);
            this.grcDotTuyen.Controls.Add(this.labelControl6);
            this.grcDotTuyen.Controls.Add(this.txtID);
            this.grcDotTuyen.Controls.Add(this.labelControl7);
            this.grcDotTuyen.Controls.Add(this.cbbeLoaiNhacNho);
            this.grcDotTuyen.Location = new System.Drawing.Point(64, 5);
            this.grcDotTuyen.Name = "grcDotTuyen";
            this.grcDotTuyen.Size = new System.Drawing.Size(1014, 196);
            this.grcDotTuyen.TabIndex = 18;
            this.grcDotTuyen.Text = "Thông tin cơ bản";
            // 
            // deNgayHetHan
            // 
            this.deNgayHetHan.EditValue = null;
            this.deNgayHetHan.Location = new System.Drawing.Point(110, 140);
            this.deNgayHetHan.Name = "deNgayHetHan";
            this.deNgayHetHan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayHetHan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayHetHan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayHetHan.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayHetHan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayHetHan.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayHetHan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayHetHan.Size = new System.Drawing.Size(100, 20);
            this.deNgayHetHan.TabIndex = 5;
            // 
            // deThoiGianNhac
            // 
            this.deThoiGianNhac.EditValue = null;
            this.deThoiGianNhac.Location = new System.Drawing.Point(110, 113);
            this.deThoiGianNhac.Name = "deThoiGianNhac";
            this.deThoiGianNhac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deThoiGianNhac.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deThoiGianNhac.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deThoiGianNhac.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deThoiGianNhac.Size = new System.Drawing.Size(100, 20);
            this.deThoiGianNhac.TabIndex = 4;
            // 
            // meNoiDung
            // 
            this.meNoiDung.Location = new System.Drawing.Point(243, 55);
            this.meNoiDung.Name = "meNoiDung";
            this.meNoiDung.Size = new System.Drawing.Size(260, 105);
            this.meNoiDung.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(24, 88);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 15);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Số quyết định";
            // 
            // txtSoQuyetDinh
            // 
            this.txtSoQuyetDinh.EditValue = "000000";
            this.txtSoQuyetDinh.Location = new System.Drawing.Point(110, 83);
            this.txtSoQuyetDinh.Name = "txtSoQuyetDinh";
            this.txtSoQuyetDinh.Size = new System.Drawing.Size(100, 20);
            this.txtSoQuyetDinh.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(27, 145);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(73, 15);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Ngày hết hạn";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(19, 61);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(81, 15);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Loại nhắc nhở";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(534, 28);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(42, 15);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú";
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(534, 55);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(260, 105);
            this.meGhiChu.TabIndex = 7;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(41, 118);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(59, 15);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Ngày nhắc";
            // 
            // txtID
            // 
            this.txtID.EditValue = "000000";
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(110, 28);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(243, 28);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(49, 15);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Nội dung";
            // 
            // cbbeLoaiNhacNho
            // 
            this.cbbeLoaiNhacNho.Location = new System.Drawing.Point(110, 56);
            this.cbbeLoaiNhacNho.Name = "cbbeLoaiNhacNho";
            this.cbbeLoaiNhacNho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeLoaiNhacNho.Size = new System.Drawing.Size(100, 20);
            this.cbbeLoaiNhacNho.TabIndex = 2;
            // 
            // gridColThang
            // 
            this.gridColThang.Caption = "Loại nhắc nhở";
            this.gridColThang.FieldName = "LoaiNhacNho";
            this.gridColThang.Name = "gridColThang";
            this.gridColThang.OptionsColumn.AllowEdit = false;
            this.gridColThang.OptionsColumn.FixedWidth = true;
            this.gridColThang.OptionsColumn.ReadOnly = true;
            this.gridColThang.Visible = true;
            this.gridColThang.VisibleIndex = 1;
            this.gridColThang.Width = 70;
            // 
            // gridColNam
            // 
            this.gridColNam.Caption = "Nội dung";
            this.gridColNam.FieldName = "NoiDung";
            this.gridColNam.Name = "gridColNam";
            this.gridColNam.OptionsColumn.AllowEdit = false;
            this.gridColNam.OptionsColumn.FixedWidth = true;
            this.gridColNam.OptionsColumn.ReadOnly = true;
            this.gridColNam.Visible = true;
            this.gridColNam.VisibleIndex = 2;
            this.gridColNam.Width = 100;
            // 
            // gridColNgayGio
            // 
            this.gridColNgayGio.Caption = "Ghi chú";
            this.gridColNgayGio.FieldName = "GhiChu";
            this.gridColNgayGio.Name = "gridColNgayGio";
            this.gridColNgayGio.OptionsColumn.AllowEdit = false;
            this.gridColNgayGio.OptionsColumn.FixedWidth = true;
            this.gridColNgayGio.OptionsColumn.ReadOnly = true;
            this.gridColNgayGio.Visible = true;
            this.gridColNgayGio.VisibleIndex = 3;
            this.gridColNgayGio.Width = 50;
            // 
            // gridColMaDot
            // 
            this.gridColMaDot.Caption = "ID";
            this.gridColMaDot.FieldName = "Id";
            this.gridColMaDot.Name = "gridColMaDot";
            this.gridColMaDot.OptionsColumn.AllowEdit = false;
            this.gridColMaDot.OptionsColumn.FixedWidth = true;
            this.gridColMaDot.OptionsColumn.ReadOnly = true;
            this.gridColMaDot.Visible = true;
            this.gridColMaDot.VisibleIndex = 0;
            this.gridColMaDot.Width = 50;
            // 
            // gridViewNhacNho
            // 
            this.gridViewNhacNho.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaDot,
            this.gridColThang,
            this.gridColNam,
            this.gridColNgayGio,
            this.gridColPhong,
            this.gridColThoiGianNhac,
            this.gridColNgayKetThuc});
            this.gridViewNhacNho.GridControl = this.gcNhacNho;
            this.gridViewNhacNho.GroupPanelText = " ";
            this.gridViewNhacNho.Name = "gridViewNhacNho";
            this.gridViewNhacNho.OptionsView.ShowAutoFilterRow = true;
            this.gridViewNhacNho.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewNhacNho_FocusedRowChanged);
            // 
            // gridColPhong
            // 
            this.gridColPhong.Caption = "Số quyết định";
            this.gridColPhong.FieldName = "SoQuyetDinh";
            this.gridColPhong.Name = "gridColPhong";
            this.gridColPhong.OptionsColumn.AllowEdit = false;
            this.gridColPhong.OptionsColumn.FixedWidth = true;
            this.gridColPhong.OptionsColumn.ReadOnly = true;
            this.gridColPhong.Visible = true;
            this.gridColPhong.VisibleIndex = 4;
            // 
            // gridColThoiGianNhac
            // 
            this.gridColThoiGianNhac.Caption = "Thời gian nhắc";
            this.gridColThoiGianNhac.FieldName = "ThoiGianNhac";
            this.gridColThoiGianNhac.Name = "gridColThoiGianNhac";
            this.gridColThoiGianNhac.OptionsColumn.FixedWidth = true;
            this.gridColThoiGianNhac.OptionsColumn.ReadOnly = true;
            this.gridColThoiGianNhac.Visible = true;
            this.gridColThoiGianNhac.VisibleIndex = 5;
            this.gridColThoiGianNhac.Width = 50;
            // 
            // gridColNgayKetThuc
            // 
            this.gridColNgayKetThuc.Caption = "Ngày hết hạn";
            this.gridColNgayKetThuc.FieldName = "NgayHetHan";
            this.gridColNgayKetThuc.Name = "gridColNgayKetThuc";
            this.gridColNgayKetThuc.OptionsColumn.AllowEdit = false;
            this.gridColNgayKetThuc.OptionsColumn.FixedWidth = true;
            this.gridColNgayKetThuc.OptionsColumn.ReadOnly = true;
            this.gridColNgayKetThuc.Visible = true;
            this.gridColNgayKetThuc.VisibleIndex = 6;
            this.gridColNgayKetThuc.Width = 50;
            // 
            // gcNhacNho
            // 
            this.gcNhacNho.Location = new System.Drawing.Point(4, 251);
            this.gcNhacNho.MainView = this.gridViewNhacNho;
            this.gcNhacNho.Name = "gcNhacNho";
            this.gcNhacNho.Size = new System.Drawing.Size(1014, 280);
            this.gcNhacNho.TabIndex = 19;
            this.gcNhacNho.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNhacNho});
            this.gcNhacNho.Click += new System.EventHandler(this.gcNhacNho_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.grcDotTuyen);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(-58, -6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1085, 256);
            this.panelControl1.TabIndex = 18;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(831, 226);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 13;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(738, 226);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 12;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCapNhat.Location = new System.Drawing.Point(645, 226);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 11;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(552, 226);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 10;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(395, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(161, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Đợt tuyển dụng";
            // 
            // frmucNhacNho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 528);
            this.Controls.Add(this.gcNhacNho);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmucNhacNho";
            this.Text = "Nhắc nhở";
            this.Load += new System.EventHandler(this.frmThongBao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).EndInit();
            this.grcDotTuyen.ResumeLayout(false);
            this.grcDotTuyen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQuyetDinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeLoaiNhacNho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNhacNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNhacNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl grcDotTuyen;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.MemoEdit meGhiChu;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtID;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeLoaiNhacNho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayGio;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaDot;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewNhacNho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayKetThuc;
        private DevExpress.XtraGrid.GridControl gcNhacNho;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit meNoiDung;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtSoQuyetDinh;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThoiGianNhac;
        private DevExpress.XtraEditors.DateEdit deThoiGianNhac;
        private DevExpress.XtraEditors.DateEdit deNgayHetHan;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}