﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    public partial class frmHinhAnh : DevExpress.XtraEditors.XtraForm
    {
        int current = 0;
        int totalPage = 0;
        List<byte[]> listImage;
        ucHinh uc = new ucHinh();
        public frmHinhAnh()
        {
            InitializeComponent();
        }

        public frmHinhAnh(string strMaNV, List<byte[]> listImg)
        {
            InitializeComponent();
            totalPage = listImg.Count;
            this.Text += " " + strMaNV;
            listImage = listImg;
            if (totalPage > 0)
            {
                current = totalPage - 1;
                LoadPage();
            }
            else
            {
                btnBack.Visible = false;
                btnNext.Visible = false;
                lbPageNum.Text = "Thầy/cô này chưa có hình ảnh";
                uc = new ucHinh(null);
                uc.Dock = System.Windows.Forms.DockStyle.Fill;
                pnUC.Controls.Add(uc);
            }
            

        }

        private void LoadPage()
        {
            if (current == 0)
            {
                btnBack.Visible = false;
            }
            else
            {
                btnBack.Visible = true;
            }

            if (current == (totalPage - 1))
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            lbPageNum.Text = (current + 1) + "/" + totalPage;
            uc.setImg(listImage[current]);
            uc.Dock = System.Windows.Forms.DockStyle.Fill;
            pnUC.Controls.Add(uc);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            current--;
            LoadPage();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            current++;
            LoadPage();
        }
    }
}