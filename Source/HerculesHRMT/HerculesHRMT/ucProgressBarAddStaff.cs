﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HerculesHRMT
{
    public partial class ucProgressBarAddStaff : UserControl
    {
        public ucProgressBarAddStaff()
        {
            InitializeComponent();

            SetFormat(0, Constants.CurrentStatus);
        }

        public void SetToolTip(int index, string messages)
        {
            switch (index)
            {
                case 0:
                    ckbStep1.ToolTip = messages;
                    break;
                case 1:
                    ckbStep2.ToolTip = messages;
                    break;
                case 2:
                    ckbStep3.ToolTip = messages;
                    break;
                case 3:
                    ckbStep4.ToolTip = messages;
                    break;
                case 4:
                    ckbStep5.ToolTip = messages;
                    break;
                case 5:
                    ckbStep6.ToolTip = messages;
                    break;
            }
        }

        public void SetFormat(int index, int status)
        {
            var currentStep = new CheckEdit();
            var currentLable = new LabelControl();

            switch (index)
            {
                case 0:
                    currentStep = ckbStep1;
                    currentLable = lblStep1;
                    break;
                case 1:
                    currentStep = ckbStep2;
                    currentLable = lblStep2;
                    break;
                case 2:
                    currentStep = ckbStep3;
                    currentLable = lblStep3;
                    break;
                case 3:
                    currentStep = ckbStep4;
                    currentLable = lblStep4;
                    break;
                case 4:
                    currentStep = ckbStep5;
                    currentLable = lblStep5;
                    break;
                case 5:
                    currentStep = ckbStep6;
                    currentLable = lblStep6;
                    break;
            }

            switch (status)
            {
                case Constants.CheckedStatus:
                    SetChecked(currentStep, currentLable);
                    break;
                case Constants.ErrorStatus:
                    SetError(currentStep, currentLable);
                    break;
                case Constants.CurrentStatus:
                    SetCurrent(currentStep, currentLable);
                    break;
                case Constants.NormalStatus:
                    SetNormal(currentStep, currentLable);
                    break;
            }
        }

        private void SetChecked(CheckEdit checkBox, LabelControl label)
        {
            checkBox.EditValue = 1;
            label.Font = new Font(label.Font, FontStyle.Regular);
            label.ForeColor = Color.Gray;
        }

        private void SetError(CheckEdit checkBox, LabelControl label)
        {
            checkBox.EditValue = -1;
            label.Font = new Font(label.Font, FontStyle.Bold);
            label.ForeColor = Color.DarkRed;
        }

        private void SetCurrent(CheckEdit checkBox, LabelControl label)
        {
            checkBox.EditValue = 0;
            label.Font = new Font(label.Font, FontStyle.Bold);
            label.ForeColor = Color.Green;
        }

        private void SetNormal(CheckEdit checkBox, LabelControl label)
        {
            checkBox.EditValue = 0;
            label.Font = new Font(label.Font, FontStyle.Regular);
            label.ForeColor = Color.Black;
        }
    }
}
