﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Popup;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Calendar;
using System.IO;
using Utilities;
using HerculesDTO;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucTongHopTinhHinhNhanSu : XtraUserControl
    {
        public string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
                     + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
  

        public ucTongHopTinhHinhNhanSu()
        {
            InitializeComponent();
            MonthYearEdit monthEdit = new MonthYearEdit();
            monthEdit.SetBounds(350, 65, 100, 20);
            monthEdit.Enabled = true;
            monthEdit.Name = "DateEditor";
            panelControl2.Controls.Add(monthEdit);
        }

        private void dateEdit1_Popup(object sender, EventArgs e)
        {
            DateEdit edit = sender as DateEdit;
            PopupDateEditForm form = (edit as IPopupControl).PopupWindow as PopupDateEditForm;
            form.Calendar.View = DevExpress.XtraEditors.Controls.DateEditCalendarViewType.YearInfo;

        }

        public class MonthYearEdit : DateEdit
        {
            public MonthYearEdit()
            {
                Properties.Mask.EditMask = "MM/yyyy";
                Properties.DisplayFormat.FormatString = "MM/yyyy";
            }

            protected override PopupBaseForm CreatePopupForm()
            {
                return new YearMonthVistaPopupDateEditForm(this);
            }

            protected override void CreateRepositoryItem()
            {
                fProperties = new YearMonthRepositoryItemDateEdit(this);
            }

            class YearMonthRepositoryItemDateEdit : RepositoryItemDateEdit
            {
                public YearMonthRepositoryItemDateEdit(DateEdit dateEdit)
                {
                    SetOwnerEdit(dateEdit);
                }

                [Browsable(false)]
                public override bool ShowToday
                {
                    get
                    {
                        return false;
                    }
                }
            }

            class YearMonthVistaPopupDateEditForm : VistaPopupDateEditForm
            {
                public YearMonthVistaPopupDateEditForm(DateEdit ownerEdit)
                    : base(ownerEdit) { }

                protected override DateEditCalendar CreateCalendar()
                {
                    return new YearMonthVistaDateEditCalendar(OwnerEdit.Properties,
                        OwnerEdit.EditValue);
                }
            }

            class YearMonthVistaDateEditCalendar : VistaDateEditCalendar
            {
                public YearMonthVistaDateEditCalendar(RepositoryItemDateEdit item,
                    object editDate)
                    : base(item, editDate) { }

                protected override void Init()
                {
                    base.Init();
                    View = DateEditCalendarViewType.YearInfo;
                }

                protected override void OnItemClick(CalendarHitInfo hitInfo)
                {
                    DayNumberCellInfo cell = (DayNumberCellInfo)hitInfo.HitObject;

                    if (View == DateEditCalendarViewType.YearInfo)
                    {
                        DateTime date = new DateTime(DateTime.Year, cell.Date.Month, 1);
                        OnDateTimeCommit(date, false);
                    }
                    else
                    {
                        base.OnItemClick(hitInfo);
                    }
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            rptTongHopNhanSuThang_temp rpt = new rptTongHopNhanSuThang_temp();
            rpt.ShowPreview();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //rptDanhSachNhanSuThang rpt = new rptDanhSachNhanSuThang(DateTime.Parse(panelControl2.Controls["DateEditor"].Text));
            rptDSNhanSuThang rpt = new rptDSNhanSuThang(DateTime.Parse(panelControl2.Controls["DateEditor"].Text));            
            rpt.ShowPreview();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            rptDSNhanSuChuyenGiaiDoan rpt = new rptDSNhanSuChuyenGiaiDoan(DateTime.Parse(panelControl2.Controls["DateEditor"].Text));
            rpt.ShowPreview();
        }

        //private void btn_DSNhanSu_Click(object sender, EventArgs e)
        //{
        //    string strDuongDanTapTinLuu;
        //    string strDuongDanTapTinTemplate;

        //    SaveFileDialog saveFileDlg = new SaveFileDialog();
        //    saveFileDlg.Title = "Lưu tập tin";
        //    saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
        //    saveFileDlg.OverwritePrompt = true;

            

        //    string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
        //                + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00");
        //    saveFileDlg.FileName = "DSNhanSu_" + strTime + ".xls";

        //    if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        strDuongDanTapTinLuu = saveFileDlg.FileName;

        //        //strDuongDanTapTinTemplate = Directory.GetCurrentDirectory();
        //        strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

        //        strDuongDanTapTinTemplate += @"\template_DSNhanSu.xls";

        //        //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
        //        List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
        //        listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVien();

        //        bool kq = IOExcel.XuatDanhSachNhanSu(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
        //        //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

        //        if (kq == true)
        //            MessageBox.Show("Đã xuất ds thành công. Đường dẫn: " + strDuongDanTapTinLuu);
        //        else
        //            MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất ds!");

        //    }
        //}

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_Excel_ceDSNhanVienNam_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //         +"_"   + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNam_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNam.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNam();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất ds thành công. Đường dẫn: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất ds!");

            }
        }

        private void btn_Excel_ceDSNhanVienNu_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //           + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNu_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNu.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNu();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSNhanVien_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVien_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVien.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVien();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {

        }

        private void btn_Excel_ceDSNangBacLuongThang_Click(object sender, EventArgs e)
        {
          

            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienXetChuyenGiaiDoanCongTac_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienXetChuyenGiaiDoanCongTac.xls";

                ////string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                //List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                //listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVien();

                ThongTinNhanSuChuyenGiaiDoanCongTacCTL nsCTL = new ThongTinNhanSuChuyenGiaiDoanCongTacCTL();
                int iSoLuongNVXet = 0;

                List<ThongTinNhanSuChuyenGiaiDoanCongTacDTO> listNhanVien = nsCTL.GetAll(DateTime.Parse(panelControl2.Controls["DateEditor"].Text));

                iSoLuongNVXet = listNhanVien.Count;


                bool kq = IOExcel.XuatDanhSachNhanVienChuyenGiaiDoanTheoThang(listNhanVien, iSoLuongNVXet ,strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSGiangVien_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNgachGiangVien_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNgachGiangVien.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNgachGiangVien();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSNhanVienPhongBan_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienPhongBan_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienPhongBan.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienPhongBan();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSGiangVienDuHoc_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //           + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienDuHoc" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienDuHoc.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienDuHoc();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSNhanVienNghiViecThang_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //          + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNghiViec" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNghiViec.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNghiViec();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSNhanVienNghiHuuThang_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNghiHuu" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNghiHuu.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNghiHuu();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSGiangVienNghiThaiSan_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //           + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNghiThaiSan" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNghiThaiSan.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNghiThaiSan();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSGiangVienNghiPhep_Click(object sender, EventArgs e)
        {
                 string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienNghiPhep" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienNghiPhep.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienNghiPhep();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSGiangVienDangLamViec_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienDangLamViec" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienDangLamViec.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienDangLamViec();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceDSNhanVienTatCaTTLV_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "DSNhanVienTCTTLV" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_DSNhanVienTCTTLV.xls";

                //string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVienTatCaTrangThaiLamViec();

                bool kq = IOExcel.XuatDanhSachNhanVien(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }

        private void btn_Excel_ceNhanSuThang_Click(object sender, EventArgs e)
        {
            string strDuongDanTapTinLuu;
            string strDuongDanTapTinTemplate;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Title = "Lưu tập tin";
            saveFileDlg.Filter = "Tập tin Excel (*.xls)|*.xls";
            saveFileDlg.OverwritePrompt = true;



            //string strTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")
            //            + "_" + DateTime.Now.Hour.ToString("00") + "h" + DateTime.Now.Minute.ToString("00");
            saveFileDlg.FileName = "BaoCaoNhanSuThang_" + strTime + ".xls";

            if (saveFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strDuongDanTapTinLuu = saveFileDlg.FileName;

                strDuongDanTapTinTemplate = Path.GetDirectoryName(Application.ExecutablePath);

                strDuongDanTapTinTemplate += @"\template_BaoCaoNhanSuThang.xls";

                ////string sThoiGianXuat = LayNgayGioHienTaiHeThong();
                //List<NhanVienDTO> listNhanVien = new List<NhanVienDTO>();
                //listNhanVien = (new NhanVienCTL()).LayDanhSachTatCaNhanVien();

                ThongTinNhanSuCTL nsCTL = new ThongTinNhanSuCTL();
                int iSoLuongNVXet = 0;
                
                List<ThongTinNhanSuDTO> listNhanVien = nsCTL.GetAll(DateTime.Parse(panelControl2.Controls["DateEditor"].Text));

                iSoLuongNVXet = listNhanVien.Count;


                bool kq = IOExcel.XuatBaoCaoNhanSu(listNhanVien, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);
                //bool kq = IOExcel.(lstKQRLThangLopObj, strBacKhoa, strThangNam, txt_GVCTCT_QLKhoi.Text, sThoiGianXuat, strDuongDanTapTinLuu, strDuongDanTapTinTemplate);

                if (kq == true)
                {
                    MessageBox.Show("Đã xuất danh sách thành công. Đường dẫn lưu file excel là: " + strDuongDanTapTinLuu);

                    System.Diagnostics.Process.Start(strDuongDanTapTinLuu);
                }
                else
                    MessageBox.Show("Đã xảy ra lỗi trong quá trình xuất danh sách!");

            }
        }
        }
}
