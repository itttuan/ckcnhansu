﻿namespace HerculesHRMT
{
    partial class ucTongHopTinhHinhNhanSu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btn_Excel_ceDSNhanVienTatCaTTLV = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienTatCaTTLV = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSNhanVienTatCaTTLV = new DevExpress.XtraEditors.CheckEdit();
            this.btn_Excel_ceDSGiangVienDangLamViec = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSGiangVienDangLamViec = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSGiangVienDangLamViec = new DevExpress.XtraEditors.CheckEdit();
            this.btn_Excel_ceDSGiangVienNghiPhep = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSGiangVienNghiPhep = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSGiangVienNghiPhep = new DevExpress.XtraEditors.CheckEdit();
            this.btn_Excel_ceDSGiangVienNghiThaiSan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSGiangVienNghiThaiSan = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSGiangVienNghiThaiSan = new DevExpress.XtraEditors.CheckEdit();
            this.btn_Excel_ceDSNhanVien = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVien = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSNhanVien = new DevExpress.XtraEditors.CheckEdit();
            this.btn_Excel_ceDSGiangVienDuHoc = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSGiangVienDuHoc = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienPhongBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienPhongBan = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSGiangVien = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSGiangVien = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNghiHuuNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNghiHuuNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNghiHuuThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNghiHuuThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNghiViecNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNghiViecNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNghiViecThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNghiViecThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNu = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNhanVienNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNhanVienNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNangBacLuongNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceDSNangBacLuongNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceNhanSuNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceNhanSuNam = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceDSNangBacLuongThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Excel_ceNhanSuThang = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.ceDSGiangVienDuHoc = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienPhongBan = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSGiangVien = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNghiHuuNam = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNghiHuuThang = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNghiViecNam = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNghiViecThang = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNu = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNhanVienNam = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNangBacLuongNam = new DevExpress.XtraEditors.CheckEdit();
            this.ceDSNangBacLuongThang = new DevExpress.XtraEditors.CheckEdit();
            this.ceNhanSuNam = new DevExpress.XtraEditors.CheckEdit();
            this.ceNhanSuThang = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btn_Report_ceDSNangBacLuongThang = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Report_ceNhanSuThang = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienTatCaTTLV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienDangLamViec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienNghiPhep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienNghiThaiSan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienDuHoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienPhongBan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiHuuNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiHuuThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiViecNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiViecThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNangBacLuongNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNangBacLuongThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNhanSuNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNhanSuThang.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(214, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(415, 33);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "BÁO CÁO TỔNG HỢP NHÂN SỰ";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienTatCaTTLV);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienTatCaTTLV);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Controls.Add(this.ceDSNhanVienTatCaTTLV);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSGiangVienDangLamViec);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSGiangVienDangLamViec);
            this.panelControl2.Controls.Add(this.labelControl19);
            this.panelControl2.Controls.Add(this.ceDSGiangVienDangLamViec);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSGiangVienNghiPhep);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSGiangVienNghiPhep);
            this.panelControl2.Controls.Add(this.labelControl18);
            this.panelControl2.Controls.Add(this.ceDSGiangVienNghiPhep);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSGiangVienNghiThaiSan);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSGiangVienNghiThaiSan);
            this.panelControl2.Controls.Add(this.labelControl17);
            this.panelControl2.Controls.Add(this.ceDSGiangVienNghiThaiSan);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVien);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVien);
            this.panelControl2.Controls.Add(this.labelControl16);
            this.panelControl2.Controls.Add(this.ceDSNhanVien);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSGiangVienDuHoc);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSGiangVienDuHoc);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienPhongBan);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienPhongBan);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSGiangVien);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSGiangVien);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNghiHuuNam);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNghiHuuNam);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNghiHuuThang);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNghiHuuThang);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNghiViecNam);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNghiViecNam);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNghiViecThang);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNghiViecThang);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNu);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNu);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNhanVienNam);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNhanVienNam);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNangBacLuongNam);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNangBacLuongNam);
            this.panelControl2.Controls.Add(this.btn_Excel_ceNhanSuNam);
            this.panelControl2.Controls.Add(this.btn_Report_ceNhanSuNam);
            this.panelControl2.Controls.Add(this.btn_Excel_ceDSNangBacLuongThang);
            this.panelControl2.Controls.Add(this.btn_Excel_ceNhanSuThang);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.ceDSGiangVienDuHoc);
            this.panelControl2.Controls.Add(this.ceDSNhanVienPhongBan);
            this.panelControl2.Controls.Add(this.ceDSGiangVien);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNghiHuuNam);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNghiHuuThang);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNghiViecNam);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNghiViecThang);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNu);
            this.panelControl2.Controls.Add(this.ceDSNhanVienNam);
            this.panelControl2.Controls.Add(this.ceDSNangBacLuongNam);
            this.panelControl2.Controls.Add(this.ceDSNangBacLuongThang);
            this.panelControl2.Controls.Add(this.ceNhanSuNam);
            this.panelControl2.Controls.Add(this.ceNhanSuThang);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.btn_Report_ceDSNangBacLuongThang);
            this.panelControl2.Controls.Add(this.btn_Report_ceNhanSuThang);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(842, 768);
            this.panelControl2.TabIndex = 3;
            // 
            // btn_Excel_ceDSNhanVienTatCaTTLV
            // 
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Location = new System.Drawing.Point(456, 704);
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Name = "btn_Excel_ceDSNhanVienTatCaTTLV";
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienTatCaTTLV.TabIndex = 57;
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Text = "Excel";
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Visible = false;
            this.btn_Excel_ceDSNhanVienTatCaTTLV.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienTatCaTTLV_Click);
            // 
            // btn_Report_ceDSNhanVienTatCaTTLV
            // 
            this.btn_Report_ceDSNhanVienTatCaTTLV.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienTatCaTTLV.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienTatCaTTLV.Location = new System.Drawing.Point(379, 704);
            this.btn_Report_ceDSNhanVienTatCaTTLV.Name = "btn_Report_ceDSNhanVienTatCaTTLV";
            this.btn_Report_ceDSNhanVienTatCaTTLV.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienTatCaTTLV.TabIndex = 56;
            this.btn_Report_ceDSNhanVienTatCaTTLV.Text = "Report";
            this.btn_Report_ceDSNhanVienTatCaTTLV.Visible = false;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(32, 704);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(338, 19);
            this.labelControl20.TabIndex = 55;
            this.labelControl20.Text = "Danh sách tất cả nhân viên (bao gồm nghỉ việc)";
            this.labelControl20.Visible = false;
            // 
            // ceDSNhanVienTatCaTTLV
            // 
            this.ceDSNhanVienTatCaTTLV.Location = new System.Drawing.Point(7, 704);
            this.ceDSNhanVienTatCaTTLV.Name = "ceDSNhanVienTatCaTTLV";
            this.ceDSNhanVienTatCaTTLV.Properties.Caption = "";
            this.ceDSNhanVienTatCaTTLV.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienTatCaTTLV.TabIndex = 54;
            this.ceDSNhanVienTatCaTTLV.Visible = false;
            // 
            // btn_Excel_ceDSGiangVienDangLamViec
            // 
            this.btn_Excel_ceDSGiangVienDangLamViec.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSGiangVienDangLamViec.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSGiangVienDangLamViec.Location = new System.Drawing.Point(467, 312);
            this.btn_Excel_ceDSGiangVienDangLamViec.Name = "btn_Excel_ceDSGiangVienDangLamViec";
            this.btn_Excel_ceDSGiangVienDangLamViec.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSGiangVienDangLamViec.TabIndex = 53;
            this.btn_Excel_ceDSGiangVienDangLamViec.Text = "Excel";
            this.btn_Excel_ceDSGiangVienDangLamViec.Click += new System.EventHandler(this.btn_Excel_ceDSGiangVienDangLamViec_Click);
            // 
            // btn_Report_ceDSGiangVienDangLamViec
            // 
            this.btn_Report_ceDSGiangVienDangLamViec.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSGiangVienDangLamViec.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSGiangVienDangLamViec.Location = new System.Drawing.Point(474, 312);
            this.btn_Report_ceDSGiangVienDangLamViec.Name = "btn_Report_ceDSGiangVienDangLamViec";
            this.btn_Report_ceDSGiangVienDangLamViec.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSGiangVienDangLamViec.TabIndex = 52;
            this.btn_Report_ceDSGiangVienDangLamViec.Text = "Report";
            this.btn_Report_ceDSGiangVienDangLamViec.Visible = false;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(174, 312);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(254, 19);
            this.labelControl19.TabIndex = 51;
            this.labelControl19.Text = "Danh sách giảng viên đang làm việc";
            // 
            // ceDSGiangVienDangLamViec
            // 
            this.ceDSGiangVienDangLamViec.Location = new System.Drawing.Point(147, 312);
            this.ceDSGiangVienDangLamViec.Name = "ceDSGiangVienDangLamViec";
            this.ceDSGiangVienDangLamViec.Properties.Caption = "";
            this.ceDSGiangVienDangLamViec.Size = new System.Drawing.Size(23, 19);
            this.ceDSGiangVienDangLamViec.TabIndex = 50;
            // 
            // btn_Excel_ceDSGiangVienNghiPhep
            // 
            this.btn_Excel_ceDSGiangVienNghiPhep.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSGiangVienNghiPhep.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSGiangVienNghiPhep.Location = new System.Drawing.Point(467, 393);
            this.btn_Excel_ceDSGiangVienNghiPhep.Name = "btn_Excel_ceDSGiangVienNghiPhep";
            this.btn_Excel_ceDSGiangVienNghiPhep.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSGiangVienNghiPhep.TabIndex = 49;
            this.btn_Excel_ceDSGiangVienNghiPhep.Text = "Excel";
            this.btn_Excel_ceDSGiangVienNghiPhep.Click += new System.EventHandler(this.btn_Excel_ceDSGiangVienNghiPhep_Click);
            // 
            // btn_Report_ceDSGiangVienNghiPhep
            // 
            this.btn_Report_ceDSGiangVienNghiPhep.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSGiangVienNghiPhep.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSGiangVienNghiPhep.Location = new System.Drawing.Point(474, 393);
            this.btn_Report_ceDSGiangVienNghiPhep.Name = "btn_Report_ceDSGiangVienNghiPhep";
            this.btn_Report_ceDSGiangVienNghiPhep.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSGiangVienNghiPhep.TabIndex = 48;
            this.btn_Report_ceDSGiangVienNghiPhep.Text = "Report";
            this.btn_Report_ceDSGiangVienNghiPhep.Visible = false;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(174, 393);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(267, 19);
            this.labelControl18.TabIndex = 47;
            this.labelControl18.Text = "Danh sách giảng viên đang nghỉ phép";
            // 
            // ceDSGiangVienNghiPhep
            // 
            this.ceDSGiangVienNghiPhep.Location = new System.Drawing.Point(147, 393);
            this.ceDSGiangVienNghiPhep.Name = "ceDSGiangVienNghiPhep";
            this.ceDSGiangVienNghiPhep.Properties.Caption = "";
            this.ceDSGiangVienNghiPhep.Size = new System.Drawing.Size(23, 19);
            this.ceDSGiangVienNghiPhep.TabIndex = 46;
            // 
            // btn_Excel_ceDSGiangVienNghiThaiSan
            // 
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Location = new System.Drawing.Point(467, 366);
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Name = "btn_Excel_ceDSGiangVienNghiThaiSan";
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSGiangVienNghiThaiSan.TabIndex = 45;
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Text = "Excel";
            this.btn_Excel_ceDSGiangVienNghiThaiSan.Click += new System.EventHandler(this.btn_Excel_ceDSGiangVienNghiThaiSan_Click);
            // 
            // btn_Report_ceDSGiangVienNghiThaiSan
            // 
            this.btn_Report_ceDSGiangVienNghiThaiSan.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSGiangVienNghiThaiSan.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSGiangVienNghiThaiSan.Location = new System.Drawing.Point(474, 366);
            this.btn_Report_ceDSGiangVienNghiThaiSan.Name = "btn_Report_ceDSGiangVienNghiThaiSan";
            this.btn_Report_ceDSGiangVienNghiThaiSan.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSGiangVienNghiThaiSan.TabIndex = 44;
            this.btn_Report_ceDSGiangVienNghiThaiSan.Text = "Report";
            this.btn_Report_ceDSGiangVienNghiThaiSan.Visible = false;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(174, 366);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(287, 19);
            this.labelControl17.TabIndex = 43;
            this.labelControl17.Text = "Danh sách giảng viên đang nghỉ thai sản";
            // 
            // ceDSGiangVienNghiThaiSan
            // 
            this.ceDSGiangVienNghiThaiSan.Location = new System.Drawing.Point(147, 366);
            this.ceDSGiangVienNghiThaiSan.Name = "ceDSGiangVienNghiThaiSan";
            this.ceDSGiangVienNghiThaiSan.Properties.Caption = "";
            this.ceDSGiangVienNghiThaiSan.Size = new System.Drawing.Size(23, 19);
            this.ceDSGiangVienNghiThaiSan.TabIndex = 42;
            // 
            // btn_Excel_ceDSNhanVien
            // 
            this.btn_Excel_ceDSNhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVien.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVien.Location = new System.Drawing.Point(467, 231);
            this.btn_Excel_ceDSNhanVien.Name = "btn_Excel_ceDSNhanVien";
            this.btn_Excel_ceDSNhanVien.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVien.TabIndex = 41;
            this.btn_Excel_ceDSNhanVien.Text = "Excel";
            this.btn_Excel_ceDSNhanVien.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVien_Click);
            // 
            // btn_Report_ceDSNhanVien
            // 
            this.btn_Report_ceDSNhanVien.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVien.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVien.Location = new System.Drawing.Point(474, 231);
            this.btn_Report_ceDSNhanVien.Name = "btn_Report_ceDSNhanVien";
            this.btn_Report_ceDSNhanVien.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVien.TabIndex = 40;
            this.btn_Report_ceDSNhanVien.Text = "Report";
            this.btn_Report_ceDSNhanVien.Visible = false;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(174, 231);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(190, 19);
            this.labelControl16.TabIndex = 39;
            this.labelControl16.Text = "Danh sách tất cả nhân viên";
            // 
            // ceDSNhanVien
            // 
            this.ceDSNhanVien.Location = new System.Drawing.Point(149, 231);
            this.ceDSNhanVien.Name = "ceDSNhanVien";
            this.ceDSNhanVien.Properties.Caption = "";
            this.ceDSNhanVien.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVien.TabIndex = 38;
            // 
            // btn_Excel_ceDSGiangVienDuHoc
            // 
            this.btn_Excel_ceDSGiangVienDuHoc.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSGiangVienDuHoc.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSGiangVienDuHoc.Location = new System.Drawing.Point(467, 339);
            this.btn_Excel_ceDSGiangVienDuHoc.Name = "btn_Excel_ceDSGiangVienDuHoc";
            this.btn_Excel_ceDSGiangVienDuHoc.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSGiangVienDuHoc.TabIndex = 37;
            this.btn_Excel_ceDSGiangVienDuHoc.Text = "Excel";
            this.btn_Excel_ceDSGiangVienDuHoc.Click += new System.EventHandler(this.btn_Excel_ceDSGiangVienDuHoc_Click);
            // 
            // btn_Report_ceDSGiangVienDuHoc
            // 
            this.btn_Report_ceDSGiangVienDuHoc.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSGiangVienDuHoc.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSGiangVienDuHoc.Location = new System.Drawing.Point(474, 339);
            this.btn_Report_ceDSGiangVienDuHoc.Name = "btn_Report_ceDSGiangVienDuHoc";
            this.btn_Report_ceDSGiangVienDuHoc.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSGiangVienDuHoc.TabIndex = 36;
            this.btn_Report_ceDSGiangVienDuHoc.Text = "Report";
            this.btn_Report_ceDSGiangVienDuHoc.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienPhongBan
            // 
            this.btn_Excel_ceDSNhanVienPhongBan.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienPhongBan.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienPhongBan.Location = new System.Drawing.Point(467, 498);
            this.btn_Excel_ceDSNhanVienPhongBan.Name = "btn_Excel_ceDSNhanVienPhongBan";
            this.btn_Excel_ceDSNhanVienPhongBan.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienPhongBan.TabIndex = 35;
            this.btn_Excel_ceDSNhanVienPhongBan.Text = "Excel";
            this.btn_Excel_ceDSNhanVienPhongBan.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienPhongBan_Click);
            // 
            // btn_Report_ceDSNhanVienPhongBan
            // 
            this.btn_Report_ceDSNhanVienPhongBan.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienPhongBan.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienPhongBan.Location = new System.Drawing.Point(474, 498);
            this.btn_Report_ceDSNhanVienPhongBan.Name = "btn_Report_ceDSNhanVienPhongBan";
            this.btn_Report_ceDSNhanVienPhongBan.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienPhongBan.TabIndex = 34;
            this.btn_Report_ceDSNhanVienPhongBan.Text = "Report";
            this.btn_Report_ceDSNhanVienPhongBan.Visible = false;
            // 
            // btn_Excel_ceDSGiangVien
            // 
            this.btn_Excel_ceDSGiangVien.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSGiangVien.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSGiangVien.Location = new System.Drawing.Point(467, 473);
            this.btn_Excel_ceDSGiangVien.Name = "btn_Excel_ceDSGiangVien";
            this.btn_Excel_ceDSGiangVien.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSGiangVien.TabIndex = 33;
            this.btn_Excel_ceDSGiangVien.Text = "Excel";
            this.btn_Excel_ceDSGiangVien.Click += new System.EventHandler(this.btn_Excel_ceDSGiangVien_Click);
            // 
            // btn_Report_ceDSGiangVien
            // 
            this.btn_Report_ceDSGiangVien.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSGiangVien.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSGiangVien.Location = new System.Drawing.Point(474, 473);
            this.btn_Report_ceDSGiangVien.Name = "btn_Report_ceDSGiangVien";
            this.btn_Report_ceDSGiangVien.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSGiangVien.TabIndex = 32;
            this.btn_Report_ceDSGiangVien.Text = "Report";
            this.btn_Report_ceDSGiangVien.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNghiHuuNam
            // 
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Location = new System.Drawing.Point(417, 745);
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Name = "btn_Excel_ceDSNhanVienNghiHuuNam";
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNghiHuuNam.TabIndex = 31;
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNghiHuuNam.Visible = false;
            // 
            // btn_Report_ceDSNhanVienNghiHuuNam
            // 
            this.btn_Report_ceDSNhanVienNghiHuuNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNghiHuuNam.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNghiHuuNam.Location = new System.Drawing.Point(340, 745);
            this.btn_Report_ceDSNhanVienNghiHuuNam.Name = "btn_Report_ceDSNhanVienNghiHuuNam";
            this.btn_Report_ceDSNhanVienNghiHuuNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNghiHuuNam.TabIndex = 30;
            this.btn_Report_ceDSNhanVienNghiHuuNam.Text = "Report";
            this.btn_Report_ceDSNhanVienNghiHuuNam.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNghiHuuThang
            // 
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Location = new System.Drawing.Point(467, 447);
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Name = "btn_Excel_ceDSNhanVienNghiHuuThang";
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNghiHuuThang.TabIndex = 29;
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNghiHuuThang.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienNghiHuuThang_Click);
            // 
            // btn_Report_ceDSNhanVienNghiHuuThang
            // 
            this.btn_Report_ceDSNhanVienNghiHuuThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNghiHuuThang.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNghiHuuThang.Location = new System.Drawing.Point(474, 447);
            this.btn_Report_ceDSNhanVienNghiHuuThang.Name = "btn_Report_ceDSNhanVienNghiHuuThang";
            this.btn_Report_ceDSNhanVienNghiHuuThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNghiHuuThang.TabIndex = 28;
            this.btn_Report_ceDSNhanVienNghiHuuThang.Text = "Report";
            this.btn_Report_ceDSNhanVienNghiHuuThang.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNghiViecNam
            // 
            this.btn_Excel_ceDSNhanVienNghiViecNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNghiViecNam.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNghiViecNam.Location = new System.Drawing.Point(417, 679);
            this.btn_Excel_ceDSNhanVienNghiViecNam.Name = "btn_Excel_ceDSNhanVienNghiViecNam";
            this.btn_Excel_ceDSNhanVienNghiViecNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNghiViecNam.TabIndex = 27;
            this.btn_Excel_ceDSNhanVienNghiViecNam.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNghiViecNam.Visible = false;
            // 
            // btn_Report_ceDSNhanVienNghiViecNam
            // 
            this.btn_Report_ceDSNhanVienNghiViecNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNghiViecNam.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNghiViecNam.Location = new System.Drawing.Point(340, 679);
            this.btn_Report_ceDSNhanVienNghiViecNam.Name = "btn_Report_ceDSNhanVienNghiViecNam";
            this.btn_Report_ceDSNhanVienNghiViecNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNghiViecNam.TabIndex = 26;
            this.btn_Report_ceDSNhanVienNghiViecNam.Text = "Report";
            this.btn_Report_ceDSNhanVienNghiViecNam.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNghiViecThang
            // 
            this.btn_Excel_ceDSNhanVienNghiViecThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNghiViecThang.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNghiViecThang.Location = new System.Drawing.Point(467, 420);
            this.btn_Excel_ceDSNhanVienNghiViecThang.Name = "btn_Excel_ceDSNhanVienNghiViecThang";
            this.btn_Excel_ceDSNhanVienNghiViecThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNghiViecThang.TabIndex = 25;
            this.btn_Excel_ceDSNhanVienNghiViecThang.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNghiViecThang.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienNghiViecThang_Click);
            // 
            // btn_Report_ceDSNhanVienNghiViecThang
            // 
            this.btn_Report_ceDSNhanVienNghiViecThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNghiViecThang.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNghiViecThang.Location = new System.Drawing.Point(474, 420);
            this.btn_Report_ceDSNhanVienNghiViecThang.Name = "btn_Report_ceDSNhanVienNghiViecThang";
            this.btn_Report_ceDSNhanVienNghiViecThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNghiViecThang.TabIndex = 24;
            this.btn_Report_ceDSNhanVienNghiViecThang.Text = "Report";
            this.btn_Report_ceDSNhanVienNghiViecThang.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNu
            // 
            this.btn_Excel_ceDSNhanVienNu.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNu.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNu.Location = new System.Drawing.Point(467, 285);
            this.btn_Excel_ceDSNhanVienNu.Name = "btn_Excel_ceDSNhanVienNu";
            this.btn_Excel_ceDSNhanVienNu.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNu.TabIndex = 23;
            this.btn_Excel_ceDSNhanVienNu.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNu.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienNu_Click);
            // 
            // btn_Report_ceDSNhanVienNu
            // 
            this.btn_Report_ceDSNhanVienNu.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNu.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNu.Location = new System.Drawing.Point(474, 285);
            this.btn_Report_ceDSNhanVienNu.Name = "btn_Report_ceDSNhanVienNu";
            this.btn_Report_ceDSNhanVienNu.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNu.TabIndex = 22;
            this.btn_Report_ceDSNhanVienNu.Text = "Report";
            this.btn_Report_ceDSNhanVienNu.Visible = false;
            // 
            // btn_Excel_ceDSNhanVienNam
            // 
            this.btn_Excel_ceDSNhanVienNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNhanVienNam.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNhanVienNam.Location = new System.Drawing.Point(467, 258);
            this.btn_Excel_ceDSNhanVienNam.Name = "btn_Excel_ceDSNhanVienNam";
            this.btn_Excel_ceDSNhanVienNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNhanVienNam.TabIndex = 21;
            this.btn_Excel_ceDSNhanVienNam.Text = "Excel";
            this.btn_Excel_ceDSNhanVienNam.Click += new System.EventHandler(this.btn_Excel_ceDSNhanVienNam_Click);
            // 
            // btn_Report_ceDSNhanVienNam
            // 
            this.btn_Report_ceDSNhanVienNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNhanVienNam.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNhanVienNam.Location = new System.Drawing.Point(474, 258);
            this.btn_Report_ceDSNhanVienNam.Name = "btn_Report_ceDSNhanVienNam";
            this.btn_Report_ceDSNhanVienNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNhanVienNam.TabIndex = 20;
            this.btn_Report_ceDSNhanVienNam.Text = "Report";
            this.btn_Report_ceDSNhanVienNam.Visible = false;
            this.btn_Report_ceDSNhanVienNam.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // btn_Excel_ceDSNangBacLuongNam
            // 
            this.btn_Excel_ceDSNangBacLuongNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNangBacLuongNam.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNangBacLuongNam.Location = new System.Drawing.Point(611, 204);
            this.btn_Excel_ceDSNangBacLuongNam.Name = "btn_Excel_ceDSNangBacLuongNam";
            this.btn_Excel_ceDSNangBacLuongNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNangBacLuongNam.TabIndex = 19;
            this.btn_Excel_ceDSNangBacLuongNam.Text = "Excel";
            this.btn_Excel_ceDSNangBacLuongNam.Visible = false;
            // 
            // btn_Report_ceDSNangBacLuongNam
            // 
            this.btn_Report_ceDSNangBacLuongNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNangBacLuongNam.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNangBacLuongNam.Location = new System.Drawing.Point(534, 204);
            this.btn_Report_ceDSNangBacLuongNam.Name = "btn_Report_ceDSNangBacLuongNam";
            this.btn_Report_ceDSNangBacLuongNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNangBacLuongNam.TabIndex = 18;
            this.btn_Report_ceDSNangBacLuongNam.Text = "Report";
            this.btn_Report_ceDSNangBacLuongNam.Visible = false;
            // 
            // btn_Excel_ceNhanSuNam
            // 
            this.btn_Excel_ceNhanSuNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceNhanSuNam.Appearance.Options.UseFont = true;
            this.btn_Excel_ceNhanSuNam.Location = new System.Drawing.Point(458, 150);
            this.btn_Excel_ceNhanSuNam.Name = "btn_Excel_ceNhanSuNam";
            this.btn_Excel_ceNhanSuNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceNhanSuNam.TabIndex = 17;
            this.btn_Excel_ceNhanSuNam.Text = "Excel";
            this.btn_Excel_ceNhanSuNam.Visible = false;
            this.btn_Excel_ceNhanSuNam.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // btn_Report_ceNhanSuNam
            // 
            this.btn_Report_ceNhanSuNam.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceNhanSuNam.Appearance.Options.UseFont = true;
            this.btn_Report_ceNhanSuNam.Location = new System.Drawing.Point(381, 150);
            this.btn_Report_ceNhanSuNam.Name = "btn_Report_ceNhanSuNam";
            this.btn_Report_ceNhanSuNam.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceNhanSuNam.TabIndex = 16;
            this.btn_Report_ceNhanSuNam.Text = "Report";
            this.btn_Report_ceNhanSuNam.Visible = false;
            // 
            // btn_Excel_ceDSNangBacLuongThang
            // 
            this.btn_Excel_ceDSNangBacLuongThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceDSNangBacLuongThang.Appearance.Options.UseFont = true;
            this.btn_Excel_ceDSNangBacLuongThang.Location = new System.Drawing.Point(611, 177);
            this.btn_Excel_ceDSNangBacLuongThang.Name = "btn_Excel_ceDSNangBacLuongThang";
            this.btn_Excel_ceDSNangBacLuongThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceDSNangBacLuongThang.TabIndex = 15;
            this.btn_Excel_ceDSNangBacLuongThang.Text = "Excel";
            this.btn_Excel_ceDSNangBacLuongThang.Click += new System.EventHandler(this.btn_Excel_ceDSNangBacLuongThang_Click);
            // 
            // btn_Excel_ceNhanSuThang
            // 
            this.btn_Excel_ceNhanSuThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel_ceNhanSuThang.Appearance.Options.UseFont = true;
            this.btn_Excel_ceNhanSuThang.Location = new System.Drawing.Point(458, 123);
            this.btn_Excel_ceNhanSuThang.Name = "btn_Excel_ceNhanSuThang";
            this.btn_Excel_ceNhanSuThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Excel_ceNhanSuThang.TabIndex = 14;
            this.btn_Excel_ceNhanSuThang.Text = "Excel";
            this.btn_Excel_ceNhanSuThang.Click += new System.EventHandler(this.btn_Excel_ceNhanSuThang_Click);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(174, 339);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(244, 19);
            this.labelControl15.TabIndex = 13;
            this.labelControl15.Text = "Danh sách giảng viên đang du học";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(174, 498);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(228, 19);
            this.labelControl14.TabIndex = 11;
            this.labelControl14.Text = "Danh sách nhân viên phòng ban";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(174, 473);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(288, 19);
            this.labelControl13.TabIndex = 11;
            this.labelControl13.Text = "Danh sách giảng viên (ngạch giảng viên)";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(32, 743);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(288, 19);
            this.labelControl12.TabIndex = 11;
            this.labelControl12.Text = "Danh sách nhân viên nghỉ hưu theo năm";
            this.labelControl12.Visible = false;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(174, 447);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(216, 19);
            this.labelControl11.TabIndex = 11;
            this.labelControl11.Text = "Danh sách nhân viên nghỉ hưu";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(32, 677);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(287, 19);
            this.labelControl10.TabIndex = 11;
            this.labelControl10.Text = "Danh sách nhân viên nghỉ việc theo năm";
            this.labelControl10.Visible = false;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(174, 420);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(215, 19);
            this.labelControl9.TabIndex = 11;
            this.labelControl9.Text = "Danh sách nhân viên nghỉ việc";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(174, 285);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(226, 19);
            this.labelControl8.TabIndex = 11;
            this.labelControl8.Text = "Danh sách tất cả nhân viên (nữ)";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(174, 258);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(238, 19);
            this.labelControl7.TabIndex = 11;
            this.labelControl7.Text = "Danh sách tất cả nhân viên (nam)";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(174, 204);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(344, 19);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Danh sách dự kiến xét nâng bậc lương theo năm";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(174, 177);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(353, 19);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Danh sách dự kiến xét nâng bậc lương theo tháng";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(174, 150);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(189, 19);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Báo cáo nhân sự theo năm";
            // 
            // ceDSGiangVienDuHoc
            // 
            this.ceDSGiangVienDuHoc.Location = new System.Drawing.Point(147, 339);
            this.ceDSGiangVienDuHoc.Name = "ceDSGiangVienDuHoc";
            this.ceDSGiangVienDuHoc.Properties.Caption = "";
            this.ceDSGiangVienDuHoc.Size = new System.Drawing.Size(23, 19);
            this.ceDSGiangVienDuHoc.TabIndex = 10;
            // 
            // ceDSNhanVienPhongBan
            // 
            this.ceDSNhanVienPhongBan.Location = new System.Drawing.Point(148, 498);
            this.ceDSNhanVienPhongBan.Name = "ceDSNhanVienPhongBan";
            this.ceDSNhanVienPhongBan.Properties.Caption = "";
            this.ceDSNhanVienPhongBan.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienPhongBan.TabIndex = 10;
            // 
            // ceDSGiangVien
            // 
            this.ceDSGiangVien.Location = new System.Drawing.Point(148, 473);
            this.ceDSGiangVien.Name = "ceDSGiangVien";
            this.ceDSGiangVien.Properties.Caption = "";
            this.ceDSGiangVien.Size = new System.Drawing.Size(23, 19);
            this.ceDSGiangVien.TabIndex = 10;
            // 
            // ceDSNhanVienNghiHuuNam
            // 
            this.ceDSNhanVienNghiHuuNam.Location = new System.Drawing.Point(7, 743);
            this.ceDSNhanVienNghiHuuNam.Name = "ceDSNhanVienNghiHuuNam";
            this.ceDSNhanVienNghiHuuNam.Properties.Caption = "";
            this.ceDSNhanVienNghiHuuNam.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNghiHuuNam.TabIndex = 10;
            this.ceDSNhanVienNghiHuuNam.Visible = false;
            // 
            // ceDSNhanVienNghiHuuThang
            // 
            this.ceDSNhanVienNghiHuuThang.Location = new System.Drawing.Point(148, 447);
            this.ceDSNhanVienNghiHuuThang.Name = "ceDSNhanVienNghiHuuThang";
            this.ceDSNhanVienNghiHuuThang.Properties.Caption = "";
            this.ceDSNhanVienNghiHuuThang.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNghiHuuThang.TabIndex = 10;
            // 
            // ceDSNhanVienNghiViecNam
            // 
            this.ceDSNhanVienNghiViecNam.Location = new System.Drawing.Point(7, 677);
            this.ceDSNhanVienNghiViecNam.Name = "ceDSNhanVienNghiViecNam";
            this.ceDSNhanVienNghiViecNam.Properties.Caption = "";
            this.ceDSNhanVienNghiViecNam.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNghiViecNam.TabIndex = 10;
            this.ceDSNhanVienNghiViecNam.Visible = false;
            // 
            // ceDSNhanVienNghiViecThang
            // 
            this.ceDSNhanVienNghiViecThang.Location = new System.Drawing.Point(149, 420);
            this.ceDSNhanVienNghiViecThang.Name = "ceDSNhanVienNghiViecThang";
            this.ceDSNhanVienNghiViecThang.Properties.Caption = "";
            this.ceDSNhanVienNghiViecThang.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNghiViecThang.TabIndex = 10;
            // 
            // ceDSNhanVienNu
            // 
            this.ceDSNhanVienNu.Location = new System.Drawing.Point(149, 285);
            this.ceDSNhanVienNu.Name = "ceDSNhanVienNu";
            this.ceDSNhanVienNu.Properties.Caption = "";
            this.ceDSNhanVienNu.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNu.TabIndex = 10;
            // 
            // ceDSNhanVienNam
            // 
            this.ceDSNhanVienNam.Location = new System.Drawing.Point(149, 258);
            this.ceDSNhanVienNam.Name = "ceDSNhanVienNam";
            this.ceDSNhanVienNam.Properties.Caption = "";
            this.ceDSNhanVienNam.Size = new System.Drawing.Size(23, 19);
            this.ceDSNhanVienNam.TabIndex = 10;
            // 
            // ceDSNangBacLuongNam
            // 
            this.ceDSNangBacLuongNam.Location = new System.Drawing.Point(149, 204);
            this.ceDSNangBacLuongNam.Name = "ceDSNangBacLuongNam";
            this.ceDSNangBacLuongNam.Properties.Caption = "";
            this.ceDSNangBacLuongNam.Size = new System.Drawing.Size(23, 19);
            this.ceDSNangBacLuongNam.TabIndex = 10;
            // 
            // ceDSNangBacLuongThang
            // 
            this.ceDSNangBacLuongThang.Location = new System.Drawing.Point(149, 177);
            this.ceDSNangBacLuongThang.Name = "ceDSNangBacLuongThang";
            this.ceDSNangBacLuongThang.Properties.Caption = "";
            this.ceDSNangBacLuongThang.Size = new System.Drawing.Size(23, 19);
            this.ceDSNangBacLuongThang.TabIndex = 10;
            // 
            // ceNhanSuNam
            // 
            this.ceNhanSuNam.Location = new System.Drawing.Point(149, 150);
            this.ceNhanSuNam.Name = "ceNhanSuNam";
            this.ceNhanSuNam.Properties.Caption = "";
            this.ceNhanSuNam.Size = new System.Drawing.Size(23, 19);
            this.ceNhanSuNam.TabIndex = 10;
            // 
            // ceNhanSuThang
            // 
            this.ceNhanSuThang.Location = new System.Drawing.Point(149, 123);
            this.ceNhanSuThang.Name = "ceNhanSuThang";
            this.ceNhanSuThang.Properties.Caption = "";
            this.ceNhanSuThang.Size = new System.Drawing.Size(23, 19);
            this.ceNhanSuThang.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(174, 123);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(198, 19);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Báo cáo nhân sự theo tháng";
            // 
            // btn_Report_ceDSNangBacLuongThang
            // 
            this.btn_Report_ceDSNangBacLuongThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceDSNangBacLuongThang.Appearance.Options.UseFont = true;
            this.btn_Report_ceDSNangBacLuongThang.Location = new System.Drawing.Point(534, 177);
            this.btn_Report_ceDSNangBacLuongThang.Name = "btn_Report_ceDSNangBacLuongThang";
            this.btn_Report_ceDSNangBacLuongThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceDSNangBacLuongThang.TabIndex = 6;
            this.btn_Report_ceDSNangBacLuongThang.Text = "Report";
            this.btn_Report_ceDSNangBacLuongThang.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // btn_Report_ceNhanSuThang
            // 
            this.btn_Report_ceNhanSuThang.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report_ceNhanSuThang.Appearance.Options.UseFont = true;
            this.btn_Report_ceNhanSuThang.Location = new System.Drawing.Point(381, 123);
            this.btn_Report_ceNhanSuThang.Name = "btn_Report_ceNhanSuThang";
            this.btn_Report_ceNhanSuThang.Size = new System.Drawing.Size(60, 19);
            this.btn_Report_ceNhanSuThang.TabIndex = 5;
            this.btn_Report_ceNhanSuThang.Text = "Report";
            this.btn_Report_ceNhanSuThang.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(257, 63);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(88, 19);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Tháng/năm:";
            // 
            // ucTongHopTinhHinhNhanSu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucTongHopTinhHinhNhanSu";
            this.Size = new System.Drawing.Size(871, 768);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienTatCaTTLV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienDangLamViec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienNghiPhep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienNghiThaiSan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVienDuHoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienPhongBan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSGiangVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiHuuNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiHuuThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiViecNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNghiViecThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNhanVienNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNangBacLuongNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDSNangBacLuongThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNhanSuNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNhanSuThang.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceNhanSuThang;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNangBacLuongThang;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSGiangVienDuHoc;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSGiangVienDuHoc;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienPhongBan;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienPhongBan;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSGiangVien;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSGiangVien;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNghiHuuNam;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNghiHuuNam;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNghiHuuThang;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNghiHuuThang;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNghiViecNam;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNghiViecNam;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNghiViecThang;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNghiViecThang;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNu;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNu;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienNam;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienNam;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNangBacLuongNam;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNangBacLuongNam;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceNhanSuNam;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceNhanSuNam;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNangBacLuongThang;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceNhanSuThang;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit ceDSGiangVienDuHoc;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienPhongBan;
        private DevExpress.XtraEditors.CheckEdit ceDSGiangVien;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNghiHuuNam;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNghiHuuThang;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNghiViecNam;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNghiViecThang;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNu;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienNam;
        private DevExpress.XtraEditors.CheckEdit ceDSNangBacLuongNam;
        private DevExpress.XtraEditors.CheckEdit ceDSNangBacLuongThang;
        private DevExpress.XtraEditors.CheckEdit ceNhanSuNam;
        private DevExpress.XtraEditors.CheckEdit ceNhanSuThang;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVien;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVien;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVien;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSGiangVienNghiPhep;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSGiangVienNghiPhep;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.CheckEdit ceDSGiangVienNghiPhep;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSGiangVienNghiThaiSan;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSGiangVienNghiThaiSan;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.CheckEdit ceDSGiangVienNghiThaiSan;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSGiangVienDangLamViec;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSGiangVienDangLamViec;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.CheckEdit ceDSGiangVienDangLamViec;
        public DevExpress.XtraEditors.SimpleButton btn_Excel_ceDSNhanVienTatCaTTLV;
        public DevExpress.XtraEditors.SimpleButton btn_Report_ceDSNhanVienTatCaTTLV;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.CheckEdit ceDSNhanVienTatCaTTLV;
    }
}
