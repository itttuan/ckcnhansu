﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucHeDaoTao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColMaHeDaoTao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtHeDaoTao = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenHeDaoTao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewHeDaoTao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColViettat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcHeDaoTao = new DevExpress.XtraGrid.GridControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeDaoTao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeDaoTao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcHeDaoTao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridColMaHeDaoTao
            // 
            this.gridColMaHeDaoTao.Caption = "Mã Hệ Đào Tạo";
            this.gridColMaHeDaoTao.FieldName = "MaHeDaoTao";
            this.gridColMaHeDaoTao.Name = "gridColMaHeDaoTao";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtHeDaoTao);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 217);
            this.panelControl1.TabIndex = 5;
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(540, 69);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(180, 20);
            this.txtTenVT.TabIndex = 1;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(450, 72);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Tên viết tắt:";
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(120, 112);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(600, 49);
            this.meGhiChu.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 115);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(632, 187);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 6;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(539, 187);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(446, 187);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 4;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(353, 187);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 3;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtHeDaoTao
            // 
            this.txtHeDaoTao.Location = new System.Drawing.Point(120, 69);
            this.txtHeDaoTao.Name = "txtHeDaoTao";
            this.txtHeDaoTao.Size = new System.Drawing.Size(269, 20);
            this.txtHeDaoTao.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Hệ đào tạo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(312, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(157, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Hệ Đào Tạo";
            // 
            // gridColTenHeDaoTao
            // 
            this.gridColTenHeDaoTao.Caption = "Hệ Đào Tạo";
            this.gridColTenHeDaoTao.FieldName = "TenHeDaoTao";
            this.gridColTenHeDaoTao.Name = "gridColTenHeDaoTao";
            this.gridColTenHeDaoTao.OptionsColumn.AllowEdit = false;
            this.gridColTenHeDaoTao.OptionsColumn.ReadOnly = true;
            this.gridColTenHeDaoTao.Visible = true;
            this.gridColTenHeDaoTao.VisibleIndex = 0;
            // 
            // gridViewHeDaoTao
            // 
            this.gridViewHeDaoTao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaHeDaoTao,
            this.gridColTenHeDaoTao,
            this.gridColViettat,
            this.gridColGhiChu});
            this.gridViewHeDaoTao.GridControl = this.gcHeDaoTao;
            this.gridViewHeDaoTao.GroupPanelText = " ";
            this.gridViewHeDaoTao.Name = "gridViewHeDaoTao";
            this.gridViewHeDaoTao.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewHeDaoTao_FocusedRowChanged);
            // 
            // gridColViettat
            // 
            this.gridColViettat.Caption = "Tên Viết Tắt";
            this.gridColViettat.FieldName = "TenVietTat";
            this.gridColViettat.Name = "gridColViettat";
            this.gridColViettat.OptionsColumn.AllowEdit = false;
            this.gridColViettat.OptionsColumn.ReadOnly = true;
            this.gridColViettat.Visible = true;
            this.gridColViettat.VisibleIndex = 1;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 2;
            // 
            // gcHeDaoTao
            // 
            this.gcHeDaoTao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcHeDaoTao.Location = new System.Drawing.Point(2, 2);
            this.gcHeDaoTao.MainView = this.gridViewHeDaoTao;
            this.gcHeDaoTao.Name = "gcHeDaoTao";
            this.gcHeDaoTao.Size = new System.Drawing.Size(781, 294);
            this.gcHeDaoTao.TabIndex = 3;
            this.gcHeDaoTao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHeDaoTao});
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcHeDaoTao);
            this.panelControl3.Location = new System.Drawing.Point(0, 218);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 298);
            this.panelControl3.TabIndex = 4;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 8;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // ucHeDaoTao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucHeDaoTao";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucHeDaoTao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeDaoTao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeDaoTao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcHeDaoTao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GridColumn gridColMaHeDaoTao;
        private PanelControl panelControl1;
        private TextEdit txtTenVT;
        private LabelControl labelControl6;
        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtHeDaoTao;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenHeDaoTao;
        private GridView gridViewHeDaoTao;
        private GridColumn gridColViettat;
        private GridColumn gridColGhiChu;
        private GridControl gcHeDaoTao;
        private PanelControl panelControl3;
        private GridColumn gridColTenVietTat;
        private PanelControl panelControl2;
        private GridColumn gridColTenDayDu;
    }
}
