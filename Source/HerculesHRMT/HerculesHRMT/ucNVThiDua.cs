﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucNVThiDua : DevExpress.XtraEditors.XtraUserControl
    {
        string strMaNV = "";
        ThiDuaCTL thiduaCTL = new ThiDuaCTL();
        public ucNVThiDua()
        {
            InitializeComponent();
        }
        public void SetMaNV(string maNV)
        {
            strMaNV = maNV;
            LoadDanhSach();
            //BindingData();
        }

        private void LoadDanhSach()
        {
            gridThang.DataSource = thiduaCTL.DanhSachThiDuaThangTheoNhanVien(strMaNV);
            gridTDHK.DataSource = thiduaCTL.DanhSachThiDuahocKyTheoNhanVien(strMaNV);
            gridTDNam.DataSource = thiduaCTL.DanhSachThiDuaNamHocTheoNhanVien(strMaNV);
        }

        private void BindingData()
        {
            
        }
    }
}
