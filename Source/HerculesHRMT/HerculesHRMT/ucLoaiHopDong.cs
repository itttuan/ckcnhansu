﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucLoaiHopDong : XtraUserControl
    {
        LoaiHopDongCTL hdCTL = new LoaiHopDongCTL();
        List<LoaiHopDongDTO> listLoaiHD = new List<LoaiHopDongDTO>();
        LoaiHopDongDTO hdSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucLoaiHopDong()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            listLoaiHD = hdCTL.GetAll();

            gcLoaiHopDong.DataSource = listLoaiHD;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(LoaiHopDongDTO hdDTO)
        {
            if (hdDTO != null)
            {
                txtLoaiHopDong.Text = hdDTO.TenLoaiHD;
                cheTamTuyen.Checked = hdDTO.LaTamTuyen;
                meGhiChu.Text = hdDTO.GhiChu;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtLoaiHopDong.Properties.ReadOnly = false;
                    cheTamTuyen.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtLoaiHopDong.Text = string.Empty;
                    cheTamTuyen.Checked = false;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtLoaiHopDong.Properties.ReadOnly = false;
                    cheTamTuyen.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(hdSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtLoaiHopDong.Properties.ReadOnly = true;
                    cheTamTuyen.Properties.ReadOnly = true;                    
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(hdSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtLoaiHopDong.Text == string.Empty)
                return false;

            return true;
        }

        private LoaiHopDongDTO GetData(LoaiHopDongDTO hdDTO)
        {
            string maHD = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexHD = hdCTL.GetMaxMaLoaiHopDong();
                if (sLastIndexHD.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexHD);
                    maHD += (nLastIndexTG + 1).ToString();
                }
                else
                    maHD += "1";

                hdDTO.NgayHieuLuc = DateTime.Now;
                hdDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maHD = hdSelect.MaLoaiHD;
                hdDTO.NgayHieuLuc = hdSelect.NgayHieuLuc;
                hdDTO.TinhTrang = hdSelect.TinhTrang;
            }
            hdDTO.MaLoaiHD = maHD;
            hdDTO.TenLoaiHD = txtLoaiHopDong.Text;
            hdDTO.LaTamTuyen = cheTamTuyen.Checked;
            hdDTO.GhiChu = meGhiChu.Text;

            return hdDTO;
        }

        private void gridViewLoaiHopDong_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listLoaiHD.Count)
            {
                hdSelect = ((GridView)sender).GetRow(_selectedIndex) as LoaiHopDongDTO;
            }
            else
                if (listLoaiHD.Count != 0)
                    hdSelect = listLoaiHD[0];
                else
                    hdSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên loại hợp đồng", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoaiHopDongDTO hdNew = GetData(new LoaiHopDongDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = hdCTL.Save(hdNew);
            else
                isSucess = hdCTL.Update(hdNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listLoaiHD.IndexOf(listLoaiHD.FirstOrDefault(p => p.MaLoaiHD == hdNew.MaLoaiHD));
                gridViewLoaiHopDong.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (hdSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa loại hợp đồng " + hdSelect.TenLoaiHD + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = hdCTL.Delete(hdSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listLoaiHD.Count != 0)
                            gridViewLoaiHopDong.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void ucLoaiHopDong_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }


    }
}
