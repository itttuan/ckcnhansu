﻿using DevExpress.XtraEditors;
using System.Runtime.InteropServices;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucNVBangCapChungChi : XtraUserControl
    {
        Common.STATUS _status;
        string strMaNV = "";
        BangCapChungChiCTL bcccCTtl = new BangCapChungChiCTL();
        LoaiBCapCChiCTL loaiBCapCChiCtl = new LoaiBCapCChiCTL();
        HeDaoTaoCTL heDaoTaoCtl = new HeDaoTaoCTL();
        LoaiTrinhDoCTL loaiTrinhDoCtl = new LoaiTrinhDoCTL();
        BangCapChungChiDto bcccDTO = new BangCapChungChiDto();
        List<BangCapChungChiDto> listBCCC = new List<BangCapChungChiDto>();
        public ucNVBangCapChungChi()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV, Common.STATUS status)
        {
            strMaNV = maNV;
            _status = status;
            SetStatusComponents();
            LoadDanhSach();
        }

        private void SetStatusComponents()
        {
            switch (_status)
            {
                case Common.STATUS.NEW:
                    bcccDTO = new BangCapChungChiDto();
                    bcccDTO.LoaiBangCapChungChi = new LoaiBCapCChiDTO();
                    bcccDTO.LoaiTrinhDo = new LoaiTrinhDoDTO();
                    bcccDTO.CoSoDaotao = new TruongDTO();
                    bcccDTO.HeDaoTao = new HeDaoTaoDTO();
                    BindingData(bcccDTO);
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    setReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    setReadOnly(true);
                    if (bcccDTO.Id > 0)
                    {
                        btnPg3Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg3Sua.Enabled = false;
                    }
                    btnPg3Them.Enabled = false;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    setReadOnly(false);
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void setReadOnly(bool read)
        {
            rdbPg3PhanLoaiBCCC.Properties.ReadOnly = read;
            txtPg3MaBCCC.Properties.ReadOnly = read;
            txtPg3ThoiGianDT.Properties.ReadOnly = read;
            txtPg3ThoiGianHieuLuc.Properties.ReadOnly = read;
            txtPg3GhiChu.Properties.ReadOnly = read;
            cbbPg3ChuyenNganhDT.Properties.ReadOnly = read;
            cbbPg3CoSoDT.Properties.ReadOnly = read;
            cbbPg3HeDT.Properties.ReadOnly = read;
            cbbPg3LoaiBCCC.Properties.ReadOnly = read;
            cbbPg3NganhDT.Properties.ReadOnly = read;
            cbbPg3TrinhDoDT.Properties.ReadOnly = read;
            dtPg3NgayCapBCCC.Properties.ReadOnly = read;
        }

        private void LoadDanhSach()
        {
            listBCCC = bcccCTtl.DanhSachBangCapChungChiNhanVien(strMaNV);
            gvPg3BCCC.DataSource = listBCCC;
        }

        private void BindingData(BangCapChungChiDto bcccDTO)
        {
            rdbPg3PhanLoaiBCCC.EditValue = Convert.ToInt32(bcccDTO.IsBangCap);
            txtPg3MaBCCC.EditValue = bcccDTO.MaBangCapChungChi;
            cbbPg3LoaiBCCC.EditValue = bcccDTO.LoaiBangCapChungChi.MaLoaiBCCC;
            cbbPg3TrinhDoDT.EditValue = bcccDTO.LoaiTrinhDo.MaLoaiTrinhDo;
            cbbPg3HeDT.EditValue = bcccDTO.HeDaoTao.MaHeDaoTao;
            //cbbPg3NganhDT.EditValue = bcccDTO.NganhDaoTao.MaNganh;
            //cbbPg3ChuyenNganhDT.EditValue = bcccDTO.ChuyenNganhDaoTao.
            cbbPg3CoSoDT.EditValue = bcccDTO.CoSoDaotao.MaTruong;
            txtPg3ThoiGianDT.EditValue = bcccDTO.ThoiGianDaoTao;
            txtPg3ThoiGianHieuLuc.EditValue = bcccDTO.ThoiGianHieuLuc;
            dtPg3NgayCapBCCC.EditValue = bcccDTO.NgayCapBang;
            txtPg3GhiChu.EditValue = bcccDTO.GhiChu;

        }

        private void ucNVBangCapChungChi_Load(object sender, System.EventArgs e)
        {
            cbbPg3CoSoDT.Properties.DataSource = Program.listTruong;
            cbbPg3HeDT.Properties.DataSource = heDaoTaoCtl.GetAll();
            cbbPg3TrinhDoDT.Properties.DataSource = loaiTrinhDoCtl.GetAll();
            cbbPg3LoaiBCCC.Properties.DataSource = loaiBCapCChiCtl.GetAll();
        }

        private void btnPg3TaoMoi_Click(object sender, EventArgs e)
        {
            bcccDTO = new BangCapChungChiDto();
            _status = Common.STATUS.NEW;
            SetStatusComponents();
            dtPg3NgayCapBCCC.EditValue = "01/01/1980";
        }

        private void gridviewBCCC_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                int id = Convert.ToInt32(((GridView)sender).GetRowCellValue(e.FocusedRowHandle, "Id").ToString());
                bcccDTO = listBCCC.Find(v => v.Id == id);
                BindingData(bcccDTO);
                _status = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }
        private void GetDataOnComponents()
        {
            bcccDTO.MaNv = strMaNV;
            bcccDTO.IsBangCap = Convert.ToInt32(rdbPg3PhanLoaiBCCC.EditValue) == 0;
            bcccDTO.MaBangCapChungChi = txtPg3MaBCCC.Text;
            LoaiBCapCChiDTO loaibccc = new LoaiBCapCChiDTO();
            loaibccc.MaLoaiBCCC = cbbPg3LoaiBCCC.EditValue as String;
            bcccDTO.LoaiBangCapChungChi = loaibccc;
            LoaiTrinhDoDTO loaiTD = new LoaiTrinhDoDTO();
            loaiTD.MaLoaiTrinhDo = cbbPg3TrinhDoDT.EditValue as String;
            bcccDTO.LoaiTrinhDo = loaiTD;
            HeDaoTaoDTO hdt = new HeDaoTaoDTO();
            hdt.MaHeDaoTao = cbbPg3HeDT.EditValue as String;
            bcccDTO.HeDaoTao = hdt;
            //NganhDaoTao = 
            //ChuyenNganhDaoTao = 
            TruongDTO truong = new TruongDTO();
            truong.MaTruong = cbbPg3CoSoDT.EditValue as String;
            bcccDTO.CoSoDaotao = truong;
            bcccDTO.ThoiGianHieuLuc = txtPg3ThoiGianHieuLuc.EditValue as String;
            bcccDTO.ThoiGianDaoTao = txtPg3ThoiGianDT.EditValue as String;
            bcccDTO.NgayCapBang = Convert.ToDateTime(dtPg3NgayCapBCCC.EditValue);
            bcccDTO.GhiChu = txtPg3GhiChu.EditValue as String;
        }
        private void btnPg3Them_Click(object sender, EventArgs e)
        {
            GetDataOnComponents();
            bool kq = false;
            if (_status == Common.STATUS.NEW)
            {
                kq = bcccCTtl.LuuMoiTrinhDo(bcccDTO);
            }
            if (_status == Common.STATUS.EDIT)
            {
                kq = bcccCTtl.CapNhatTrinhDo(bcccDTO);
            }
            if (kq)
            {
                XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _status = Common.STATUS.VIEW;
                LoadDanhSach();
                SetStatusComponents();
            }
            else
            {
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg3Xoa_Click(object sender, EventArgs e)
        {
            if (bcccDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (bcccCTtl.XoaTrinhDo(bcccDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnPg3Sua_Click(object sender, EventArgs e)
        {
            if (bcccDTO.Id > 0)
            {
                _status = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }
        
    }
}
