﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;

namespace HerculesHRMT
{
    public partial class ucNVPhanCong : XtraUserControl
    {
        string strMaNV = "";
        PhanCongCTL phancongCTL = new PhanCongCTL();
        BoMonBoPhanCTL bmCTL = new BoMonBoPhanCTL();
        public ucNVPhanCong()
        {
            InitializeComponent();
        }

        public void SetMaNV(string maNV)
        {
            strMaNV = maNV;
            //LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gridPhancong.DataSource = phancongCTL.DanhSachPhanCongNhanVien(strMaNV);
        }

        private void ucNVPhanCong_Load(object sender, EventArgs e)
        {
            luePhongKhoa.Properties.DataSource = Program.listPhongKhoa_NV;
            lueBPBM.Properties.DataSource = Program.listBMBP_NV;
            lueChucVu.Properties.DataSource = Program.listChucVu;
        }

        private void luePhongKhoa_EditValueChanged(object sender, EventArgs e)
        {
            if (luePhongKhoa.EditValue != null)
            {
                lueBPBM.Properties.DataSource = bmCTL.GetListByMaPhongKhoa(luePhongKhoa.EditValue.ToString());
            }
        }

        private void lueChucVu_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}
