﻿namespace HerculesHRMT
{
    partial class ucUngVien
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnTamTuyen = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtChungChi = new DevExpress.XtraEditors.TextEdit();
            this.txtKyNangSuPham = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtChuyenMon = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtNganhDuTuyen = new DevExpress.XtraEditors.TextEdit();
            this.luePhongKhoa = new DevExpress.XtraEditors.LookUpEdit();
            this.lueBMBP = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.txtNganhDaoTao = new DevExpress.XtraEditors.TextEdit();
            this.txtTrinhDoSauDaiHoc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiemTotNghiep = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtXepHang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtTruongKhac = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lueHinhThucDaoTao = new DevExpress.XtraEditors.LookUpEdit();
            this.lueTruongDaoTao = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lueMaDot = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaUV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtQueQuan = new DevExpress.XtraEditors.TextEdit();
            this.txtCMND = new DevExpress.XtraEditors.TextEdit();
            this.txtDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.deNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.rgGioiTinh = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtHoLot = new DevExpress.XtraEditors.TextEdit();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.gcUngVien = new DevExpress.XtraGrid.GridControl();
            this.gvUngVien = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaUV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaDot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColCMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCoQueQuan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDienThoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNganhHoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTruongDaoTao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTruongKhac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHinhThucDaoTao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColXepHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDiemTotNghiep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTrinhDoSauDaiHoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChungChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColChuyenMon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColKyNangSuPham = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhongKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBMBP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNganhDuTuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyNangSuPham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChuyenMon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganhDuTuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBMBP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganhDaoTao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrinhDoSauDaiHoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTotNghiep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXepHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTruongKhac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHinhThucDaoTao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTruongDaoTao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueMaDot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaUV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQueQuan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUngVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUngVien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.btnTamTuyen);
            this.panelControl1.Controls.Add(this.groupControl4);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(9106, 413);
            this.panelControl1.TabIndex = 7;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // btnTamTuyen
            // 
            this.btnTamTuyen.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnTamTuyen.Appearance.Options.UseFont = true;
            this.btnTamTuyen.Location = new System.Drawing.Point(316, 383);
            this.btnTamTuyen.Name = "btnTamTuyen";
            this.btnTamTuyen.Size = new System.Drawing.Size(87, 23);
            this.btnTamTuyen.TabIndex = 45;
            this.btnTamTuyen.Text = "Tạm tuyển";
            this.btnTamTuyen.Click += new System.EventHandler(this.btnTamTuyen_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.meGhiChu);
            this.groupControl4.Controls.Add(this.labelControl1);
            this.groupControl4.Controls.Add(this.labelControl4);
            this.groupControl4.Controls.Add(this.txtChungChi);
            this.groupControl4.Controls.Add(this.txtKyNangSuPham);
            this.groupControl4.Controls.Add(this.labelControl2);
            this.groupControl4.Controls.Add(this.labelControl3);
            this.groupControl4.Controls.Add(this.txtChuyenMon);
            this.groupControl4.Location = new System.Drawing.Point(5, 189);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1012, 109);
            this.groupControl4.TabIndex = 3;
            this.groupControl4.Text = "Thông tin khác";
            this.groupControl4.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl4_Paint);
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(493, 26);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(227, 79);
            this.meGhiChu.TabIndex = 24;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(50, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 15);
            this.labelControl1.TabIndex = 32;
            this.labelControl1.Text = "Chứng chỉ";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(445, 26);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 15);
            this.labelControl4.TabIndex = 38;
            this.labelControl4.Text = "Ghi chú";
            // 
            // txtChungChi
            // 
            this.txtChungChi.Location = new System.Drawing.Point(114, 26);
            this.txtChungChi.Name = "txtChungChi";
            this.txtChungChi.Properties.AllowFocused = false;
            this.txtChungChi.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungChi.Properties.Appearance.Options.UseFont = true;
            this.txtChungChi.Size = new System.Drawing.Size(250, 22);
            this.txtChungChi.TabIndex = 21;
            // 
            // txtKyNangSuPham
            // 
            this.txtKyNangSuPham.Location = new System.Drawing.Point(114, 79);
            this.txtKyNangSuPham.Name = "txtKyNangSuPham";
            this.txtKyNangSuPham.Properties.AllowFocused = false;
            this.txtKyNangSuPham.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyNangSuPham.Properties.Appearance.Options.UseFont = true;
            this.txtKyNangSuPham.Size = new System.Drawing.Size(250, 22);
            this.txtKyNangSuPham.TabIndex = 23;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(38, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 15);
            this.labelControl2.TabIndex = 34;
            this.labelControl2.Text = "Chuyên môn";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(9, 86);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(99, 15);
            this.labelControl3.TabIndex = 36;
            this.labelControl3.Text = "Kỹ năng sư phạm";
            // 
            // txtChuyenMon
            // 
            this.txtChuyenMon.Location = new System.Drawing.Point(114, 52);
            this.txtChuyenMon.Name = "txtChuyenMon";
            this.txtChuyenMon.Properties.AllowFocused = false;
            this.txtChuyenMon.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuyenMon.Properties.Appearance.Options.UseFont = true;
            this.txtChuyenMon.Size = new System.Drawing.Size(250, 22);
            this.txtChuyenMon.TabIndex = 22;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtNganhDuTuyen);
            this.groupControl1.Controls.Add(this.luePhongKhoa);
            this.groupControl1.Controls.Add(this.lueBMBP);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl31);
            this.groupControl1.Controls.Add(this.labelControl32);
            this.groupControl1.Location = new System.Drawing.Point(5, 301);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1012, 77);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Vị trí ứng tuyển";
            // 
            // txtNganhDuTuyen
            // 
            this.txtNganhDuTuyen.Location = new System.Drawing.Point(422, 25);
            this.txtNganhDuTuyen.Name = "txtNganhDuTuyen";
            this.txtNganhDuTuyen.Properties.AllowFocused = false;
            this.txtNganhDuTuyen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNganhDuTuyen.Properties.Appearance.Options.UseFont = true;
            this.txtNganhDuTuyen.Size = new System.Drawing.Size(165, 22);
            this.txtNganhDuTuyen.TabIndex = 32;
            // 
            // luePhongKhoa
            // 
            this.luePhongKhoa.EditValue = "";
            this.luePhongKhoa.Location = new System.Drawing.Point(143, 25);
            this.luePhongKhoa.Name = "luePhongKhoa";
            this.luePhongKhoa.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luePhongKhoa.Properties.Appearance.Options.UseFont = true;
            this.luePhongKhoa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhongKhoa.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaPhongKhoa", "Mã phòng khoa", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenPhongKhoa", "Tên phòng khoa")});
            this.luePhongKhoa.Properties.DisplayMember = "TenPhongKhoa";
            this.luePhongKhoa.Properties.NullText = "";
            this.luePhongKhoa.Properties.ValueMember = "MaPhongKhoa";
            this.luePhongKhoa.Size = new System.Drawing.Size(165, 22);
            this.luePhongKhoa.TabIndex = 30;
            this.luePhongKhoa.EditValueChanged += new System.EventHandler(this.luePhongKhoa_EditValueChanged);
            // 
            // lueBMBP
            // 
            this.lueBMBP.EditValue = "";
            this.lueBMBP.Location = new System.Drawing.Point(143, 52);
            this.lueBMBP.Name = "lueBMBP";
            this.lueBMBP.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueBMBP.Properties.Appearance.Options.UseFont = true;
            this.lueBMBP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBMBP.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaBMBP", "Mã B.Môn B.Phận", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenBMBP", "Tên bộ môn/bộ phận")});
            this.lueBMBP.Properties.DisplayMember = "TenBMBP";
            this.lueBMBP.Properties.NullText = "";
            this.lueBMBP.Properties.ValueMember = "MaBMBP";
            this.lueBMBP.Size = new System.Drawing.Size(165, 22);
            this.lueBMBP.TabIndex = 31;
            this.lueBMBP.EditValueChanged += new System.EventHandler(this.lueBMBP_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(327, 32);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(89, 15);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Ngành dự tuyển";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Location = new System.Drawing.Point(58, 59);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(83, 15);
            this.labelControl31.TabIndex = 0;
            this.labelControl31.Text = "B.Môn/B.Phận*";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Location = new System.Drawing.Point(68, 32);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(73, 15);
            this.labelControl32.TabIndex = 0;
            this.labelControl32.Text = "Phòng/Khoa*";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.txtNganhDaoTao);
            this.groupControl3.Controls.Add(this.txtTrinhDoSauDaiHoc);
            this.groupControl3.Controls.Add(this.labelControl22);
            this.groupControl3.Controls.Add(this.txtDiemTotNghiep);
            this.groupControl3.Controls.Add(this.labelControl21);
            this.groupControl3.Controls.Add(this.txtXepHang);
            this.groupControl3.Controls.Add(this.labelControl20);
            this.groupControl3.Controls.Add(this.txtTruongKhac);
            this.groupControl3.Controls.Add(this.labelControl19);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.labelControl18);
            this.groupControl3.Controls.Add(this.lueHinhThucDaoTao);
            this.groupControl3.Controls.Add(this.lueTruongDaoTao);
            this.groupControl3.Controls.Add(this.labelControl57);
            this.groupControl3.Location = new System.Drawing.Point(5, 83);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1012, 103);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = "Trình độ chuyên môn";
            this.groupControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl3_Paint);
            // 
            // txtNganhDaoTao
            // 
            this.txtNganhDaoTao.Location = new System.Drawing.Point(114, 24);
            this.txtNganhDaoTao.Name = "txtNganhDaoTao";
            this.txtNganhDaoTao.Properties.AllowFocused = false;
            this.txtNganhDaoTao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNganhDaoTao.Properties.Appearance.Options.UseFont = true;
            this.txtNganhDaoTao.Size = new System.Drawing.Size(250, 22);
            this.txtNganhDaoTao.TabIndex = 12;
            // 
            // txtTrinhDoSauDaiHoc
            // 
            this.txtTrinhDoSauDaiHoc.Location = new System.Drawing.Point(733, 24);
            this.txtTrinhDoSauDaiHoc.Name = "txtTrinhDoSauDaiHoc";
            this.txtTrinhDoSauDaiHoc.Properties.AllowFocused = false;
            this.txtTrinhDoSauDaiHoc.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrinhDoSauDaiHoc.Properties.Appearance.Options.UseFont = true;
            this.txtTrinhDoSauDaiHoc.Size = new System.Drawing.Size(141, 22);
            this.txtTrinhDoSauDaiHoc.TabIndex = 18;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Location = new System.Drawing.Point(616, 31);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(113, 15);
            this.labelControl22.TabIndex = 30;
            this.labelControl22.Text = "Trình độ sau đại học";
            // 
            // txtDiemTotNghiep
            // 
            this.txtDiemTotNghiep.Location = new System.Drawing.Point(493, 77);
            this.txtDiemTotNghiep.Name = "txtDiemTotNghiep";
            this.txtDiemTotNghiep.Properties.AllowFocused = false;
            this.txtDiemTotNghiep.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiemTotNghiep.Properties.Appearance.Options.UseFont = true;
            this.txtDiemTotNghiep.Size = new System.Drawing.Size(115, 22);
            this.txtDiemTotNghiep.TabIndex = 17;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Location = new System.Drawing.Point(398, 84);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(89, 15);
            this.labelControl21.TabIndex = 28;
            this.labelControl21.Text = "Điểm tốt nghiệp";
            // 
            // txtXepHang
            // 
            this.txtXepHang.Location = new System.Drawing.Point(493, 51);
            this.txtXepHang.Name = "txtXepHang";
            this.txtXepHang.Properties.AllowFocused = false;
            this.txtXepHang.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXepHang.Properties.Appearance.Options.UseFont = true;
            this.txtXepHang.Size = new System.Drawing.Size(115, 22);
            this.txtXepHang.TabIndex = 16;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Location = new System.Drawing.Point(434, 58);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(53, 15);
            this.labelControl20.TabIndex = 26;
            this.labelControl20.Text = "Xếp hạng";
            // 
            // txtTruongKhac
            // 
            this.txtTruongKhac.Location = new System.Drawing.Point(114, 77);
            this.txtTruongKhac.Name = "txtTruongKhac";
            this.txtTruongKhac.Properties.AllowFocused = false;
            this.txtTruongKhac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTruongKhac.Properties.Appearance.Options.UseFont = true;
            this.txtTruongKhac.Size = new System.Drawing.Size(250, 22);
            this.txtTruongKhac.TabIndex = 14;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Location = new System.Drawing.Point(33, 84);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(74, 15);
            this.labelControl19.TabIndex = 24;
            this.labelControl19.Text = "Trường khác";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Location = new System.Drawing.Point(387, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(105, 15);
            this.labelControl12.TabIndex = 21;
            this.labelControl12.Text = "Hình thức đào tạo*";
            this.labelControl12.Click += new System.EventHandler(this.labelControl12_Click);
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Location = new System.Drawing.Point(19, 58);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(93, 15);
            this.labelControl18.TabIndex = 20;
            this.labelControl18.Text = "Trường đào tạo*";
            // 
            // lueHinhThucDaoTao
            // 
            this.lueHinhThucDaoTao.Location = new System.Drawing.Point(493, 24);
            this.lueHinhThucDaoTao.Name = "lueHinhThucDaoTao";
            this.lueHinhThucDaoTao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueHinhThucDaoTao.Properties.Appearance.Options.UseFont = true;
            this.lueHinhThucDaoTao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHinhThucDaoTao.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Mã HTDT", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiHinhThucDaoTao", "Hình thức đào tạo")});
            this.lueHinhThucDaoTao.Properties.DisplayMember = "TenLoaiHinhThucDaoTao";
            this.lueHinhThucDaoTao.Properties.NullText = "";
            this.lueHinhThucDaoTao.Properties.PopupSizeable = false;
            this.lueHinhThucDaoTao.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueHinhThucDaoTao.Properties.ValueMember = "Id";
            this.lueHinhThucDaoTao.Size = new System.Drawing.Size(115, 22);
            this.lueHinhThucDaoTao.TabIndex = 15;
            // 
            // lueTruongDaoTao
            // 
            this.lueTruongDaoTao.Location = new System.Drawing.Point(114, 51);
            this.lueTruongDaoTao.Name = "lueTruongDaoTao";
            this.lueTruongDaoTao.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueTruongDaoTao.Properties.Appearance.Options.UseFont = true;
            this.lueTruongDaoTao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTruongDaoTao.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTruong", "Tên trường")});
            this.lueTruongDaoTao.Properties.DisplayMember = "TenTruong";
            this.lueTruongDaoTao.Properties.NullText = "";
            this.lueTruongDaoTao.Properties.PopupSizeable = false;
            this.lueTruongDaoTao.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueTruongDaoTao.Properties.ValueMember = "TenTruong";
            this.lueTruongDaoTao.Size = new System.Drawing.Size(250, 22);
            this.lueTruongDaoTao.TabIndex = 13;
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Location = new System.Drawing.Point(27, 31);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(81, 15);
            this.labelControl57.TabIndex = 9;
            this.labelControl57.Text = "Ngành đào tạo";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.lueMaDot);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtMaUV);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.txtTen);
            this.groupControl2.Controls.Add(this.labelControl27);
            this.groupControl2.Controls.Add(this.labelControl16);
            this.groupControl2.Controls.Add(this.labelControl24);
            this.groupControl2.Controls.Add(this.txtEmail);
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.txtQueQuan);
            this.groupControl2.Controls.Add(this.txtCMND);
            this.groupControl2.Controls.Add(this.txtDienThoai);
            this.groupControl2.Controls.Add(this.deNgaySinh);
            this.groupControl2.Controls.Add(this.rgGioiTinh);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.labelControl15);
            this.groupControl2.Controls.Add(this.txtHoLot);
            this.groupControl2.Location = new System.Drawing.Point(5, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1012, 76);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Thông tin cơ bản";
            this.groupControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl2_Paint);
            // 
            // lueMaDot
            // 
            this.lueMaDot.EditValue = "";
            this.lueMaDot.Location = new System.Drawing.Point(228, 24);
            this.lueMaDot.Name = "lueMaDot";
            this.lueMaDot.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueMaDot.Properties.Appearance.Options.UseFont = true;
            this.lueMaDot.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMaDot.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaDot", 10, "Mã đợt tuyển dụng")});
            this.lueMaDot.Properties.DisplayMember = "MaDot";
            this.lueMaDot.Properties.NullText = "";
            this.lueMaDot.Properties.PopupSizeable = false;
            this.lueMaDot.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueMaDot.Properties.ValueMember = "MaDot";
            this.lueMaDot.Size = new System.Drawing.Size(75, 22);
            this.lueMaDot.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(177, 31);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(48, 15);
            this.labelControl7.TabIndex = 47;
            this.labelControl7.Text = "Mã đợt *";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(6, 31);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(69, 15);
            this.labelControl6.TabIndex = 45;
            this.labelControl6.Text = "Mã ứng viên";
            // 
            // txtMaUV
            // 
            this.txtMaUV.Enabled = false;
            this.txtMaUV.Location = new System.Drawing.Point(78, 24);
            this.txtMaUV.Name = "txtMaUV";
            this.txtMaUV.Properties.AllowFocused = false;
            this.txtMaUV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaUV.Properties.Appearance.Options.UseFont = true;
            this.txtMaUV.Size = new System.Drawing.Size(63, 22);
            this.txtMaUV.TabIndex = 1111;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Location = new System.Drawing.Point(209, 58);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(29, 15);
            this.labelControl14.TabIndex = 42;
            this.labelControl14.Text = "Tên *";
            // 
            // txtTen
            // 
            this.txtTen.EditValue = "";
            this.txtTen.Location = new System.Drawing.Point(240, 51);
            this.txtTen.Name = "txtTen";
            this.txtTen.Properties.AllowFocused = false;
            this.txtTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Properties.Appearance.Options.UseFont = true;
            this.txtTen.Size = new System.Drawing.Size(63, 22);
            this.txtTen.TabIndex = 4;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Location = new System.Drawing.Point(671, 58);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(31, 15);
            this.labelControl27.TabIndex = 34;
            this.labelControl27.Text = "Email";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Location = new System.Drawing.Point(503, 31);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(34, 15);
            this.labelControl16.TabIndex = 32;
            this.labelControl16.Text = "CMND";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Location = new System.Drawing.Point(645, 31);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(57, 15);
            this.labelControl24.TabIndex = 35;
            this.labelControl24.Text = "Điện thoại";
            // 
            // txtEmail
            // 
            this.txtEmail.EditValue = "vuductoan12345@gmail.com";
            this.txtEmail.Location = new System.Drawing.Point(705, 51);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.AllowFocused = false;
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(169, 22);
            this.txtEmail.TabIndex = 10;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Location = new System.Drawing.Point(483, 58);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(54, 15);
            this.labelControl17.TabIndex = 33;
            this.labelControl17.Text = "Quê quán";
            // 
            // txtQueQuan
            // 
            this.txtQueQuan.Location = new System.Drawing.Point(540, 51);
            this.txtQueQuan.Name = "txtQueQuan";
            this.txtQueQuan.Properties.AllowFocused = false;
            this.txtQueQuan.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQueQuan.Properties.Appearance.Options.UseFont = true;
            this.txtQueQuan.Size = new System.Drawing.Size(100, 22);
            this.txtQueQuan.TabIndex = 8;
            // 
            // txtCMND
            // 
            this.txtCMND.EditValue = "";
            this.txtCMND.Location = new System.Drawing.Point(540, 24);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Properties.AllowFocused = false;
            this.txtCMND.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMND.Properties.Appearance.Options.UseFont = true;
            this.txtCMND.Size = new System.Drawing.Size(100, 22);
            this.txtCMND.TabIndex = 7;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.EditValue = "";
            this.txtDienThoai.Location = new System.Drawing.Point(705, 24);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Properties.AllowFocused = false;
            this.txtDienThoai.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.Properties.Appearance.Options.UseFont = true;
            this.txtDienThoai.Size = new System.Drawing.Size(169, 22);
            this.txtDienThoai.TabIndex = 9;
            // 
            // deNgaySinh
            // 
            this.deNgaySinh.EditValue = null;
            this.deNgaySinh.Location = new System.Drawing.Point(363, 24);
            this.deNgaySinh.Name = "deNgaySinh";
            this.deNgaySinh.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.deNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgaySinh.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgaySinh.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgaySinh.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgaySinh.Properties.NullDate = "1-1-0001";
            this.deNgaySinh.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgaySinh.Size = new System.Drawing.Size(115, 22);
            this.deNgaySinh.TabIndex = 5;
            // 
            // rgGioiTinh
            // 
            this.rgGioiTinh.EditValue = 0;
            this.rgGioiTinh.Location = new System.Drawing.Point(363, 51);
            this.rgGioiTinh.Name = "rgGioiTinh";
            this.rgGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rgGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.rgGioiTinh.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Nam"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Nữ")});
            this.rgGioiTinh.Size = new System.Drawing.Size(115, 22);
            this.rgGioiTinh.TabIndex = 6;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(312, 58);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 15);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Giới tính";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Location = new System.Drawing.Point(304, 31);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(55, 15);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "Sinh ngày";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(35, 58);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(40, 15);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Họ lót *";
            // 
            // txtHoLot
            // 
            this.txtHoLot.Location = new System.Drawing.Point(78, 51);
            this.txtHoLot.Name = "txtHoLot";
            this.txtHoLot.Properties.AllowFocused = false;
            this.txtHoLot.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoLot.Properties.Appearance.Options.UseFont = true;
            this.txtHoLot.Size = new System.Drawing.Size(126, 22);
            this.txtHoLot.TabIndex = 3;
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(764, 383);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 43;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(485, 383);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 40;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCapNhat.Location = new System.Drawing.Point(578, 383);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 41;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(671, 383);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 42;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // gcUngVien
            // 
            this.gcUngVien.Location = new System.Drawing.Point(5, 419);
            this.gcUngVien.MainView = this.gvUngVien;
            this.gcUngVien.Name = "gcUngVien";
            this.gcUngVien.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gcUngVien.Size = new System.Drawing.Size(1012, 186);
            this.gcUngVien.TabIndex = 17;
            this.gcUngVien.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUngVien});
            this.gcUngVien.Click += new System.EventHandler(this.gcUngVien_Click_1);
            // 
            // gvUngVien
            // 
            this.gvUngVien.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaUV,
            this.gridColMaDot,
            this.gridColHo,
            this.gridColTen,
            this.gridColNgaySinh,
            this.gridColGioiTinh,
            this.gridColCMND,
            this.gridCoQueQuan,
            this.gridColDienThoai,
            this.gridColEmail,
            this.gridColNganhHoc,
            this.gridColTruongDaoTao,
            this.gridColTruongKhac,
            this.gridColHinhThucDaoTao,
            this.gridColXepHang,
            this.gridColDiemTotNghiep,
            this.gridColTrinhDoSauDaiHoc,
            this.gridColChungChi,
            this.gridColChuyenMon,
            this.gridColKyNangSuPham,
            this.gridColGhiChu,
            this.gridColPhongKhoa,
            this.gridColBMBP,
            this.gridColNganhDuTuyen});
            this.gvUngVien.GridControl = this.gcUngVien;
            this.gvUngVien.GroupPanelText = " ";
            this.gvUngVien.Name = "gvUngVien";
            this.gvUngVien.OptionsView.ColumnAutoWidth = false;
            this.gvUngVien.OptionsView.ShowAutoFilterRow = true;
            this.gvUngVien.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gcUngVien_FocusedRowChanged);
            // 
            // gridColMaUV
            // 
            this.gridColMaUV.Caption = "Mã UV";
            this.gridColMaUV.FieldName = "MaUV";
            this.gridColMaUV.Name = "gridColMaUV";
            this.gridColMaUV.OptionsColumn.AllowEdit = false;
            this.gridColMaUV.OptionsColumn.FixedWidth = true;
            this.gridColMaUV.OptionsColumn.ReadOnly = true;
            this.gridColMaUV.Visible = true;
            this.gridColMaUV.VisibleIndex = 0;
            this.gridColMaUV.Width = 67;
            // 
            // gridColMaDot
            // 
            this.gridColMaDot.Caption = "Mã đợt";
            this.gridColMaDot.FieldName = "MaDot";
            this.gridColMaDot.Name = "gridColMaDot";
            this.gridColMaDot.OptionsColumn.AllowEdit = false;
            this.gridColMaDot.OptionsColumn.FixedWidth = true;
            this.gridColMaDot.OptionsColumn.ReadOnly = true;
            this.gridColMaDot.Visible = true;
            this.gridColMaDot.VisibleIndex = 1;
            // 
            // gridColHo
            // 
            this.gridColHo.Caption = "Họ lót";
            this.gridColHo.FieldName = "Ho";
            this.gridColHo.Name = "gridColHo";
            this.gridColHo.OptionsColumn.AllowEdit = false;
            this.gridColHo.OptionsColumn.FixedWidth = true;
            this.gridColHo.OptionsColumn.ReadOnly = true;
            this.gridColHo.Visible = true;
            this.gridColHo.VisibleIndex = 2;
            this.gridColHo.Width = 100;
            // 
            // gridColTen
            // 
            this.gridColTen.Caption = "Tên";
            this.gridColTen.FieldName = "Ten";
            this.gridColTen.Name = "gridColTen";
            this.gridColTen.OptionsColumn.AllowEdit = false;
            this.gridColTen.OptionsColumn.FixedWidth = true;
            this.gridColTen.OptionsColumn.ReadOnly = true;
            this.gridColTen.Visible = true;
            this.gridColTen.VisibleIndex = 3;
            this.gridColTen.Width = 70;
            // 
            // gridColNgaySinh
            // 
            this.gridColNgaySinh.Caption = "Ngày sinh";
            this.gridColNgaySinh.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColNgaySinh.FieldName = "NgaySinh";
            this.gridColNgaySinh.Name = "gridColNgaySinh";
            this.gridColNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColNgaySinh.OptionsColumn.FixedWidth = true;
            this.gridColNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColNgaySinh.Visible = true;
            this.gridColNgaySinh.VisibleIndex = 4;
            // 
            // gridColGioiTinh
            // 
            this.gridColGioiTinh.Caption = "Nữ";
            this.gridColGioiTinh.FieldName = "GioiTinh";
            this.gridColGioiTinh.Name = "gridColGioiTinh";
            this.gridColGioiTinh.OptionsColumn.AllowEdit = false;
            this.gridColGioiTinh.OptionsColumn.FixedWidth = true;
            this.gridColGioiTinh.OptionsColumn.ReadOnly = true;
            this.gridColGioiTinh.Visible = true;
            this.gridColGioiTinh.VisibleIndex = 5;
            this.gridColGioiTinh.Width = 30;
            // 
            // gridColCMND
            // 
            this.gridColCMND.Caption = "CMND";
            this.gridColCMND.FieldName = "CMND";
            this.gridColCMND.Name = "gridColCMND";
            this.gridColCMND.OptionsColumn.AllowEdit = false;
            this.gridColCMND.OptionsColumn.FixedWidth = true;
            this.gridColCMND.OptionsColumn.ReadOnly = true;
            this.gridColCMND.Visible = true;
            this.gridColCMND.VisibleIndex = 6;
            this.gridColCMND.Width = 90;
            // 
            // gridCoQueQuan
            // 
            this.gridCoQueQuan.Caption = "Quê quán";
            this.gridCoQueQuan.FieldName = "QueQuan";
            this.gridCoQueQuan.Name = "gridCoQueQuan";
            this.gridCoQueQuan.OptionsColumn.AllowEdit = false;
            this.gridCoQueQuan.OptionsColumn.FixedWidth = true;
            this.gridCoQueQuan.OptionsColumn.ReadOnly = true;
            this.gridCoQueQuan.Visible = true;
            this.gridCoQueQuan.VisibleIndex = 7;
            this.gridCoQueQuan.Width = 120;
            // 
            // gridColDienThoai
            // 
            this.gridColDienThoai.Caption = "Điện thoại";
            this.gridColDienThoai.FieldName = "DienThoai";
            this.gridColDienThoai.Name = "gridColDienThoai";
            this.gridColDienThoai.OptionsColumn.AllowEdit = false;
            this.gridColDienThoai.OptionsColumn.FixedWidth = true;
            this.gridColDienThoai.OptionsColumn.ReadOnly = true;
            this.gridColDienThoai.Visible = true;
            this.gridColDienThoai.VisibleIndex = 8;
            this.gridColDienThoai.Width = 100;
            // 
            // gridColEmail
            // 
            this.gridColEmail.Caption = "Email";
            this.gridColEmail.FieldName = "Email";
            this.gridColEmail.Name = "gridColEmail";
            this.gridColEmail.OptionsColumn.AllowEdit = false;
            this.gridColEmail.OptionsColumn.FixedWidth = true;
            this.gridColEmail.OptionsColumn.ReadOnly = true;
            this.gridColEmail.Visible = true;
            this.gridColEmail.VisibleIndex = 9;
            this.gridColEmail.Width = 150;
            // 
            // gridColNganhHoc
            // 
            this.gridColNganhHoc.Caption = "Ngành đào tạo";
            this.gridColNganhHoc.FieldName = "NganhHoc";
            this.gridColNganhHoc.Name = "gridColNganhHoc";
            this.gridColNganhHoc.OptionsColumn.AllowEdit = false;
            this.gridColNganhHoc.OptionsColumn.FixedWidth = true;
            this.gridColNganhHoc.OptionsColumn.ReadOnly = true;
            this.gridColNganhHoc.Visible = true;
            this.gridColNganhHoc.VisibleIndex = 10;
            this.gridColNganhHoc.Width = 100;
            // 
            // gridColTruongDaoTao
            // 
            this.gridColTruongDaoTao.Caption = "Trường đào tạo";
            this.gridColTruongDaoTao.FieldName = "MaTruong";
            this.gridColTruongDaoTao.Name = "gridColTruongDaoTao";
            this.gridColTruongDaoTao.OptionsColumn.AllowEdit = false;
            this.gridColTruongDaoTao.OptionsColumn.FixedWidth = true;
            this.gridColTruongDaoTao.OptionsColumn.ReadOnly = true;
            this.gridColTruongDaoTao.Visible = true;
            this.gridColTruongDaoTao.VisibleIndex = 12;
            this.gridColTruongDaoTao.Width = 150;
            // 
            // gridColTruongKhac
            // 
            this.gridColTruongKhac.Caption = "Trường khác";
            this.gridColTruongKhac.FieldName = "TruongKhac";
            this.gridColTruongKhac.Name = "gridColTruongKhac";
            this.gridColTruongKhac.OptionsColumn.AllowEdit = false;
            this.gridColTruongKhac.OptionsColumn.FixedWidth = true;
            this.gridColTruongKhac.OptionsColumn.ReadOnly = true;
            this.gridColTruongKhac.Visible = true;
            this.gridColTruongKhac.VisibleIndex = 14;
            this.gridColTruongKhac.Width = 100;
            // 
            // gridColHinhThucDaoTao
            // 
            this.gridColHinhThucDaoTao.Caption = "Hình thức đào tạo";
            this.gridColHinhThucDaoTao.FieldName = "HinhThucDaoTao";
            this.gridColHinhThucDaoTao.Name = "gridColHinhThucDaoTao";
            this.gridColHinhThucDaoTao.OptionsColumn.AllowEdit = false;
            this.gridColHinhThucDaoTao.OptionsColumn.FixedWidth = true;
            this.gridColHinhThucDaoTao.OptionsColumn.ReadOnly = true;
            this.gridColHinhThucDaoTao.Visible = true;
            this.gridColHinhThucDaoTao.VisibleIndex = 11;
            this.gridColHinhThucDaoTao.Width = 100;
            // 
            // gridColXepHang
            // 
            this.gridColXepHang.Caption = "Xếp loại";
            this.gridColXepHang.FieldName = "XepHang";
            this.gridColXepHang.Name = "gridColXepHang";
            this.gridColXepHang.OptionsColumn.AllowEdit = false;
            this.gridColXepHang.OptionsColumn.FixedWidth = true;
            this.gridColXepHang.OptionsColumn.ReadOnly = true;
            this.gridColXepHang.Visible = true;
            this.gridColXepHang.VisibleIndex = 13;
            this.gridColXepHang.Width = 70;
            // 
            // gridColDiemTotNghiep
            // 
            this.gridColDiemTotNghiep.Caption = "Điểm tốt nghiệp";
            this.gridColDiemTotNghiep.FieldName = "DiemTN";
            this.gridColDiemTotNghiep.Name = "gridColDiemTotNghiep";
            this.gridColDiemTotNghiep.OptionsColumn.AllowEdit = false;
            this.gridColDiemTotNghiep.OptionsColumn.FixedWidth = true;
            this.gridColDiemTotNghiep.OptionsColumn.ReadOnly = true;
            this.gridColDiemTotNghiep.Visible = true;
            this.gridColDiemTotNghiep.VisibleIndex = 15;
            this.gridColDiemTotNghiep.Width = 100;
            // 
            // gridColTrinhDoSauDaiHoc
            // 
            this.gridColTrinhDoSauDaiHoc.Caption = "Trình độ SĐH";
            this.gridColTrinhDoSauDaiHoc.FieldName = "SauDaiHoc";
            this.gridColTrinhDoSauDaiHoc.Name = "gridColTrinhDoSauDaiHoc";
            this.gridColTrinhDoSauDaiHoc.OptionsColumn.AllowEdit = false;
            this.gridColTrinhDoSauDaiHoc.OptionsColumn.FixedWidth = true;
            this.gridColTrinhDoSauDaiHoc.OptionsColumn.ReadOnly = true;
            this.gridColTrinhDoSauDaiHoc.Visible = true;
            this.gridColTrinhDoSauDaiHoc.VisibleIndex = 16;
            this.gridColTrinhDoSauDaiHoc.Width = 100;
            // 
            // gridColChungChi
            // 
            this.gridColChungChi.Caption = "Chứng chỉ";
            this.gridColChungChi.FieldName = "ChungChi";
            this.gridColChungChi.Name = "gridColChungChi";
            this.gridColChungChi.OptionsColumn.AllowEdit = false;
            this.gridColChungChi.OptionsColumn.FixedWidth = true;
            this.gridColChungChi.OptionsColumn.ReadOnly = true;
            this.gridColChungChi.Visible = true;
            this.gridColChungChi.VisibleIndex = 17;
            this.gridColChungChi.Width = 100;
            // 
            // gridColChuyenMon
            // 
            this.gridColChuyenMon.Caption = "Chuyên môn";
            this.gridColChuyenMon.FieldName = "YeuCauChuyenMon";
            this.gridColChuyenMon.Name = "gridColChuyenMon";
            this.gridColChuyenMon.OptionsColumn.AllowEdit = false;
            this.gridColChuyenMon.OptionsColumn.FixedWidth = true;
            this.gridColChuyenMon.OptionsColumn.ReadOnly = true;
            this.gridColChuyenMon.Visible = true;
            this.gridColChuyenMon.VisibleIndex = 18;
            this.gridColChuyenMon.Width = 100;
            // 
            // gridColKyNangSuPham
            // 
            this.gridColKyNangSuPham.Caption = "Kỹ năng sư phạm";
            this.gridColKyNangSuPham.FieldName = "KyNangSuPham";
            this.gridColKyNangSuPham.Name = "gridColKyNangSuPham";
            this.gridColKyNangSuPham.OptionsColumn.AllowEdit = false;
            this.gridColKyNangSuPham.OptionsColumn.FixedWidth = true;
            this.gridColKyNangSuPham.OptionsColumn.ReadOnly = true;
            this.gridColKyNangSuPham.Visible = true;
            this.gridColKyNangSuPham.VisibleIndex = 19;
            this.gridColKyNangSuPham.Width = 150;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.FixedWidth = true;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 20;
            this.gridColGhiChu.Width = 200;
            // 
            // gridColPhongKhoa
            // 
            this.gridColPhongKhoa.Caption = "Phòng khoa";
            this.gridColPhongKhoa.FieldName = "MaPK";
            this.gridColPhongKhoa.Name = "gridColPhongKhoa";
            this.gridColPhongKhoa.OptionsColumn.AllowEdit = false;
            this.gridColPhongKhoa.OptionsColumn.FixedWidth = true;
            this.gridColPhongKhoa.OptionsColumn.ReadOnly = true;
            this.gridColPhongKhoa.Visible = true;
            this.gridColPhongKhoa.VisibleIndex = 21;
            this.gridColPhongKhoa.Width = 100;
            // 
            // gridColBMBP
            // 
            this.gridColBMBP.Caption = "Bộ môn/Bộ phận";
            this.gridColBMBP.FieldName = "MaBMBP";
            this.gridColBMBP.Name = "gridColBMBP";
            this.gridColBMBP.OptionsColumn.AllowEdit = false;
            this.gridColBMBP.OptionsColumn.FixedWidth = true;
            this.gridColBMBP.OptionsColumn.ReadOnly = true;
            this.gridColBMBP.Visible = true;
            this.gridColBMBP.VisibleIndex = 22;
            this.gridColBMBP.Width = 100;
            // 
            // gridColNganhDuTuyen
            // 
            this.gridColNganhDuTuyen.Caption = "Ngành dự tuyển";
            this.gridColNganhDuTuyen.FieldName = "NganhDuTuyen";
            this.gridColNganhDuTuyen.Name = "gridColNganhDuTuyen";
            this.gridColNganhDuTuyen.OptionsColumn.AllowEdit = false;
            this.gridColNganhDuTuyen.OptionsColumn.FixedWidth = true;
            this.gridColNganhDuTuyen.OptionsColumn.ReadOnly = true;
            this.gridColNganhDuTuyen.Visible = true;
            this.gridColNganhDuTuyen.VisibleIndex = 23;
            this.gridColNganhDuTuyen.Width = 150;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // ucUngVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gcUngVien);
            this.Name = "ucUngVien";
            this.Size = new System.Drawing.Size(1024, 672);
            this.Load += new System.EventHandler(this.ucUngVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChungChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyNangSuPham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChuyenMon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganhDuTuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhongKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBMBP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganhDaoTao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrinhDoSauDaiHoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiemTotNghiep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXepHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTruongKhac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHinhThucDaoTao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTruongDaoTao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueMaDot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaUV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQueQuan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUngVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUngVien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.GridControl gcUngVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUngVien;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaUV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColCMND;
        private DevExpress.XtraGrid.Columns.GridColumn gridCoQueQuan;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LookUpEdit luePhongKhoa;
        private DevExpress.XtraEditors.LookUpEdit lueBMBP;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtDiemTotNghiep;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtXepHang;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtTruongKhac;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LookUpEdit lueHinhThucDaoTao;
        private DevExpress.XtraEditors.LookUpEdit lueTruongDaoTao;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtQueQuan;
        private DevExpress.XtraEditors.TextEdit txtCMND;
        private DevExpress.XtraEditors.TextEdit txtDienThoai;
        private DevExpress.XtraEditors.DateEdit deNgaySinh;
        private DevExpress.XtraEditors.RadioGroup rgGioiTinh;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtHoLot;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtChungChi;
        private DevExpress.XtraEditors.TextEdit txtKyNangSuPham;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtChuyenMon;
        private DevExpress.XtraEditors.TextEdit txtTrinhDoSauDaiHoc;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtMaUV;
        private DevExpress.XtraEditors.MemoEdit meGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn gridColEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNganhHoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColHinhThucDaoTao;
        private DevExpress.XtraGrid.Columns.GridColumn gridColXepHang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColDiemTotNghiep;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTrinhDoSauDaiHoc;
        private DevExpress.XtraEditors.LookUpEdit lueMaDot;
        private DevExpress.XtraEditors.TextEdit txtNganhDuTuyen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaDot;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTruongDaoTao;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTruongKhac;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChungChi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColChuyenMon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColKyNangSuPham;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhongKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColBMBP;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNganhDuTuyen;
        private DevExpress.XtraEditors.TextEdit txtNganhDaoTao;
        private DevExpress.XtraEditors.SimpleButton btnTamTuyen;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;

    }
}
