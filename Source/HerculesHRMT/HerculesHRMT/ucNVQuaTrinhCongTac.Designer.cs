﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVQuaTrinhCongTac
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.txtPg2GDGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.gridColID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNoiCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNghenghiep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTuNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditNgaySinh = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateCongtac = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColDenNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThanhTich = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvPg2QuaTrinhCongTac = new DevExpress.XtraGrid.GridControl();
            this.gridviewQuaTrinhCongTac = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.rdbPg2GiaiDoan = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.dtPg2GDDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.dtPg2GDTu = new DevExpress.XtraEditors.DateEdit();
            this.btnPg2XoaGD = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg2ThemGD = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg2TaoMoiGD = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg2GDCongTac = new DevExpress.XtraEditors.TextEdit();
            this.txtPg2GDCongViec = new DevExpress.XtraEditors.TextEdit();
            this.txtPg2GDThanhTich = new DevExpress.XtraEditors.MemoEdit();
            this.gvPg2QuaTrinhThamGia = new DevExpress.XtraGrid.GridControl();
            this.gridviewQuaTrinhThamGia = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl16 = new DevExpress.XtraEditors.GroupControl();
            this.btnEditTC = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl18 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.dtPg2TGDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.dtPg2TGTu = new DevExpress.XtraEditors.DateEdit();
            this.btnPg2XoaTG = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg2ThemTG = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg2TaoMoiTG = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg2TGChinhTriXaHoi = new DevExpress.XtraEditors.TextEdit();
            this.txtPg2TGCongViec = new DevExpress.XtraEditors.TextEdit();
            this.txtPg2TGGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.txtPg2TGThanhTich = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.btnEditQT = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateCongtac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateCongtac.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg2QuaTrinhCongTac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuaTrinhCongTac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg2GiaiDoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDCongTac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDCongViec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDThanhTich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg2QuaTrinhThamGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuaTrinhThamGia)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).BeginInit();
            this.groupControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).BeginInit();
            this.groupControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGChinhTriXaHoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGCongViec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGThanhTich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPg2GDGhiChu
            // 
            this.txtPg2GDGhiChu.Location = new System.Drawing.Point(624, 45);
            this.txtPg2GDGhiChu.Name = "txtPg2GDGhiChu";
            this.txtPg2GDGhiChu.Properties.AllowFocused = false;
            this.txtPg2GDGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2GDGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtPg2GDGhiChu.Size = new System.Drawing.Size(98, 71);
            this.txtPg2GDGhiChu.TabIndex = 0;
            // 
            // gridColID
            // 
            this.gridColID.AppearanceCell.Options.UseTextOptions = true;
            this.gridColID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColID.Caption = "Mã ID";
            this.gridColID.FieldName = "Id";
            this.gridColID.Name = "gridColID";
            this.gridColID.OptionsColumn.AllowEdit = false;
            this.gridColID.OptionsColumn.FixedWidth = true;
            this.gridColID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn1.Caption = "Loại";
            this.gridColumn1.FieldName = "GiaiDoan";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 150;
            // 
            // gridColNoiCT
            // 
            this.gridColNoiCT.Caption = "Nơi Học tập/Công tác";
            this.gridColNoiCT.FieldName = "NoiCongTac";
            this.gridColNoiCT.Name = "gridColNoiCT";
            this.gridColNoiCT.OptionsColumn.AllowEdit = false;
            this.gridColNoiCT.OptionsColumn.FixedWidth = true;
            this.gridColNoiCT.OptionsColumn.ReadOnly = true;
            this.gridColNoiCT.Visible = true;
            this.gridColNoiCT.VisibleIndex = 1;
            this.gridColNoiCT.Width = 150;
            // 
            // gridColNghenghiep
            // 
            this.gridColNghenghiep.Caption = "Công việc";
            this.gridColNghenghiep.FieldName = "CongViec";
            this.gridColNghenghiep.Name = "gridColNghenghiep";
            this.gridColNghenghiep.OptionsColumn.AllowEdit = false;
            this.gridColNghenghiep.OptionsColumn.FixedWidth = true;
            this.gridColNghenghiep.OptionsColumn.ReadOnly = true;
            this.gridColNghenghiep.Visible = true;
            this.gridColNghenghiep.VisibleIndex = 2;
            this.gridColNghenghiep.Width = 100;
            // 
            // gridColTuNgay
            // 
            this.gridColTuNgay.Caption = "Từ ngày";
            this.gridColTuNgay.ColumnEdit = this.repositoryItemDateEditNgaySinh;
            this.gridColTuNgay.FieldName = "TuNgay";
            this.gridColTuNgay.Name = "gridColTuNgay";
            this.gridColTuNgay.OptionsColumn.AllowEdit = false;
            this.gridColTuNgay.OptionsColumn.FixedWidth = true;
            this.gridColTuNgay.OptionsColumn.ReadOnly = true;
            this.gridColTuNgay.Visible = true;
            this.gridColTuNgay.VisibleIndex = 3;
            // 
            // repositoryItemDateEditNgaySinh
            // 
            this.repositoryItemDateEditNgaySinh.AutoHeight = false;
            this.repositoryItemDateEditNgaySinh.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditNgaySinh.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditNgaySinh.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditNgaySinh.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEditNgaySinh.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditNgaySinh.Name = "repositoryItemDateEditNgaySinh";
            this.repositoryItemDateEditNgaySinh.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemDateCongtac
            // 
            this.repositoryItemDateCongtac.AutoHeight = false;
            this.repositoryItemDateCongtac.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateCongtac.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateCongtac.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateCongtac.Name = "repositoryItemDateCongtac";
            this.repositoryItemDateCongtac.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColDenNgay
            // 
            this.gridColDenNgay.Caption = "Đến ngày";
            this.gridColDenNgay.ColumnEdit = this.repositoryItemDateEditNgaySinh;
            this.gridColDenNgay.FieldName = "DenNgay";
            this.gridColDenNgay.Name = "gridColDenNgay";
            this.gridColDenNgay.OptionsColumn.AllowEdit = false;
            this.gridColDenNgay.OptionsColumn.FixedWidth = true;
            this.gridColDenNgay.OptionsColumn.ReadOnly = true;
            this.gridColDenNgay.Visible = true;
            this.gridColDenNgay.VisibleIndex = 4;
            // 
            // gridColThanhTich
            // 
            this.gridColThanhTich.Caption = "Thành tích";
            this.gridColThanhTich.FieldName = "ThanhTich";
            this.gridColThanhTich.Name = "gridColThanhTich";
            this.gridColThanhTich.OptionsColumn.AllowEdit = false;
            this.gridColThanhTich.OptionsColumn.FixedWidth = true;
            this.gridColThanhTich.OptionsColumn.ReadOnly = true;
            this.gridColThanhTich.Visible = true;
            this.gridColThanhTich.VisibleIndex = 5;
            this.gridColThanhTich.Width = 200;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.FixedWidth = true;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 6;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gvPg2QuaTrinhCongTac;
            this.gridView1.Name = "gridView1";
            // 
            // gvPg2QuaTrinhCongTac
            // 
            this.gvPg2QuaTrinhCongTac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "Level1";
            this.gvPg2QuaTrinhCongTac.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gvPg2QuaTrinhCongTac.Location = new System.Drawing.Point(0, 122);
            this.gvPg2QuaTrinhCongTac.MainView = this.gridviewQuaTrinhCongTac;
            this.gvPg2QuaTrinhCongTac.Name = "gvPg2QuaTrinhCongTac";
            this.gvPg2QuaTrinhCongTac.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateCongtac,
            this.repositoryItemDateEditNgaySinh});
            this.gvPg2QuaTrinhCongTac.Size = new System.Drawing.Size(1280, 132);
            this.gvPg2QuaTrinhCongTac.TabIndex = 29;
            this.gvPg2QuaTrinhCongTac.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewQuaTrinhCongTac,
            this.gridView1});
            // 
            // gridviewQuaTrinhCongTac
            // 
            this.gridviewQuaTrinhCongTac.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridviewQuaTrinhCongTac.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColID,
            this.gridColumn1,
            this.gridColNoiCT,
            this.gridColNghenghiep,
            this.gridColTuNgay,
            this.gridColDenNgay,
            this.gridColThanhTich,
            this.gridColGhiChu});
            this.gridviewQuaTrinhCongTac.GridControl = this.gvPg2QuaTrinhCongTac;
            this.gridviewQuaTrinhCongTac.GroupPanelText = "Quá trình công tác";
            this.gridviewQuaTrinhCongTac.Name = "gridviewQuaTrinhCongTac";
            this.gridviewQuaTrinhCongTac.OptionsView.ColumnAutoWidth = false;
            this.gridviewQuaTrinhCongTac.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewQuaTrinhCongTac_RowClick);
            this.gridviewQuaTrinhCongTac.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewQuaTrinhCongTac_FocusedRowChanged);
            this.gridviewQuaTrinhCongTac.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridviewQuaTrinhCongTac_CustomColumnDisplayText);
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl8.Appearance.Options.UseFont = true;
            this.groupControl8.Controls.Add(this.rdbPg2GiaiDoan);
            this.groupControl8.Location = new System.Drawing.Point(6, 24);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(136, 92);
            this.groupControl8.TabIndex = 5;
            this.groupControl8.Text = "Giai đoạn";
            // 
            // rdbPg2GiaiDoan
            // 
            this.rdbPg2GiaiDoan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbPg2GiaiDoan.EditValue = 0;
            this.rdbPg2GiaiDoan.Location = new System.Drawing.Point(2, 21);
            this.rdbPg2GiaiDoan.Name = "rdbPg2GiaiDoan";
            this.rdbPg2GiaiDoan.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Trước khi tuyển dụng"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Khi được tuyển dụng")});
            this.rdbPg2GiaiDoan.Size = new System.Drawing.Size(132, 69);
            this.rdbPg2GiaiDoan.TabIndex = 4;
            // 
            // groupControl7
            // 
            this.groupControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl7.Appearance.Options.UseFont = true;
            this.groupControl7.Controls.Add(this.labelControl37);
            this.groupControl7.Controls.Add(this.dtPg2GDDen);
            this.groupControl7.Controls.Add(this.labelControl55);
            this.groupControl7.Controls.Add(this.dtPg2GDTu);
            this.groupControl7.Location = new System.Drawing.Point(144, 24);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(138, 92);
            this.groupControl7.TabIndex = 5;
            this.groupControl7.Text = "Thời gian";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Location = new System.Drawing.Point(5, 55);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(23, 15);
            this.labelControl37.TabIndex = 0;
            this.labelControl37.Text = "Đến";
            // 
            // dtPg2GDDen
            // 
            this.dtPg2GDDen.EditValue = null;
            this.dtPg2GDDen.Location = new System.Drawing.Point(34, 52);
            this.dtPg2GDDen.Name = "dtPg2GDDen";
            this.dtPg2GDDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg2GDDen.Properties.Appearance.Options.UseFont = true;
            this.dtPg2GDDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg2GDDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2GDDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2GDDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2GDDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2GDDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg2GDDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg2GDDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg2GDDen.Size = new System.Drawing.Size(98, 22);
            this.dtPg2GDDen.TabIndex = 2;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Location = new System.Drawing.Point(5, 27);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(16, 15);
            this.labelControl55.TabIndex = 0;
            this.labelControl55.Text = "Từ";
            // 
            // dtPg2GDTu
            // 
            this.dtPg2GDTu.EditValue = null;
            this.dtPg2GDTu.Location = new System.Drawing.Point(34, 24);
            this.dtPg2GDTu.Name = "dtPg2GDTu";
            this.dtPg2GDTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg2GDTu.Properties.Appearance.Options.UseFont = true;
            this.dtPg2GDTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg2GDTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2GDTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2GDTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2GDTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2GDTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg2GDTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg2GDTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg2GDTu.Size = new System.Drawing.Size(98, 22);
            this.dtPg2GDTu.TabIndex = 2;
            // 
            // btnPg2XoaGD
            // 
            this.btnPg2XoaGD.Location = new System.Drawing.Point(808, 91);
            this.btnPg2XoaGD.Name = "btnPg2XoaGD";
            this.btnPg2XoaGD.Size = new System.Drawing.Size(75, 23);
            this.btnPg2XoaGD.TabIndex = 0;
            this.btnPg2XoaGD.Text = "Xóa";
            this.btnPg2XoaGD.Click += new System.EventHandler(this.btnPg2XoaGD_Click);
            // 
            // btnPg2ThemGD
            // 
            this.btnPg2ThemGD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPg2ThemGD.Location = new System.Drawing.Point(726, 91);
            this.btnPg2ThemGD.Name = "btnPg2ThemGD";
            this.btnPg2ThemGD.Size = new System.Drawing.Size(75, 23);
            this.btnPg2ThemGD.TabIndex = 0;
            this.btnPg2ThemGD.Text = "Lưu";
            this.btnPg2ThemGD.Click += new System.EventHandler(this.btnPg2ThemGD_Click);
            // 
            // btnPg2TaoMoiGD
            // 
            this.btnPg2TaoMoiGD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPg2TaoMoiGD.Location = new System.Drawing.Point(726, 51);
            this.btnPg2TaoMoiGD.Name = "btnPg2TaoMoiGD";
            this.btnPg2TaoMoiGD.Size = new System.Drawing.Size(75, 23);
            this.btnPg2TaoMoiGD.TabIndex = 0;
            this.btnPg2TaoMoiGD.Text = "Tạo mới";
            this.btnPg2TaoMoiGD.Click += new System.EventHandler(this.btnPg2TaoMoiGD_Click);
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Location = new System.Drawing.Point(286, 24);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(122, 15);
            this.labelControl41.TabIndex = 0;
            this.labelControl41.Text = "Nơi học tập/công tác*";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Location = new System.Drawing.Point(627, 24);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(42, 15);
            this.labelControl49.TabIndex = 0;
            this.labelControl49.Text = "Ghi chú";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Location = new System.Drawing.Point(286, 73);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(60, 15);
            this.labelControl52.TabIndex = 0;
            this.labelControl52.Text = "Công việc*";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Location = new System.Drawing.Point(474, 24);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(59, 15);
            this.labelControl56.TabIndex = 0;
            this.labelControl56.Text = "Thành tích";
            // 
            // txtPg2GDCongTac
            // 
            this.txtPg2GDCongTac.Location = new System.Drawing.Point(286, 45);
            this.txtPg2GDCongTac.Name = "txtPg2GDCongTac";
            this.txtPg2GDCongTac.Properties.AllowFocused = false;
            this.txtPg2GDCongTac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2GDCongTac.Properties.Appearance.Options.UseFont = true;
            this.txtPg2GDCongTac.Size = new System.Drawing.Size(181, 22);
            this.txtPg2GDCongTac.TabIndex = 0;
            // 
            // txtPg2GDCongViec
            // 
            this.txtPg2GDCongViec.Location = new System.Drawing.Point(286, 94);
            this.txtPg2GDCongViec.Name = "txtPg2GDCongViec";
            this.txtPg2GDCongViec.Properties.AllowFocused = false;
            this.txtPg2GDCongViec.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2GDCongViec.Properties.Appearance.Options.UseFont = true;
            this.txtPg2GDCongViec.Size = new System.Drawing.Size(181, 22);
            this.txtPg2GDCongViec.TabIndex = 0;
            // 
            // txtPg2GDThanhTich
            // 
            this.txtPg2GDThanhTich.Location = new System.Drawing.Point(471, 45);
            this.txtPg2GDThanhTich.Name = "txtPg2GDThanhTich";
            this.txtPg2GDThanhTich.Properties.AllowFocused = false;
            this.txtPg2GDThanhTich.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2GDThanhTich.Properties.Appearance.Options.UseFont = true;
            this.txtPg2GDThanhTich.Size = new System.Drawing.Size(147, 71);
            this.txtPg2GDThanhTich.TabIndex = 0;
            // 
            // gvPg2QuaTrinhThamGia
            // 
            this.gvPg2QuaTrinhThamGia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gvPg2QuaTrinhThamGia.Location = new System.Drawing.Point(0, 371);
            this.gvPg2QuaTrinhThamGia.MainView = this.gridviewQuaTrinhThamGia;
            this.gvPg2QuaTrinhThamGia.Name = "gvPg2QuaTrinhThamGia";
            this.gvPg2QuaTrinhThamGia.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gvPg2QuaTrinhThamGia.Size = new System.Drawing.Size(1277, 139);
            this.gvPg2QuaTrinhThamGia.TabIndex = 28;
            this.gvPg2QuaTrinhThamGia.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewQuaTrinhThamGia});
            // 
            // gridviewQuaTrinhThamGia
            // 
            this.gridviewQuaTrinhThamGia.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridviewQuaTrinhThamGia.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridviewQuaTrinhThamGia.GridControl = this.gvPg2QuaTrinhThamGia;
            this.gridviewQuaTrinhThamGia.GroupPanelText = "Quá trình tham gia tổ chức chính trị - xã hội";
            this.gridviewQuaTrinhThamGia.Name = "gridviewQuaTrinhThamGia";
            this.gridviewQuaTrinhThamGia.OptionsView.ColumnAutoWidth = false;
            this.gridviewQuaTrinhThamGia.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewQuaTrinhThamGia_RowClick);
            this.gridviewQuaTrinhThamGia.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewQuaTrinhThamGia_FocusedRowChanged);
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Mã ID";
            this.gridColumn9.FieldName = "ID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tổ chức CT - XH";
            this.gridColumn10.FieldName = "NoiCongTac";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 300;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Công việc";
            this.gridColumn11.FieldName = "CongViec";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 100;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Từ ngày";
            this.gridColumn12.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn12.FieldName = "TuNgay";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.FixedWidth = true;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Đến ngày";
            this.gridColumn13.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn13.FieldName = "DenNgay";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.FixedWidth = true;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Thành tích";
            this.gridColumn14.FieldName = "ThanhTich";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.FixedWidth = true;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            this.gridColumn14.Width = 200;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Ghi chú";
            this.gridColumn15.FieldName = "GhiChu";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.FixedWidth = true;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gvPg2QuaTrinhThamGia);
            this.xtraScrollableControl1.Controls.Add(this.groupControl16);
            this.xtraScrollableControl1.Controls.Add(this.gvPg2QuaTrinhCongTac);
            this.xtraScrollableControl1.Controls.Add(this.groupControl6);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1280, 510);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // groupControl16
            // 
            this.groupControl16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl16.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl16.Appearance.Options.UseFont = true;
            this.groupControl16.Controls.Add(this.btnEditTC);
            this.groupControl16.Controls.Add(this.groupControl18);
            this.groupControl16.Controls.Add(this.btnPg2XoaTG);
            this.groupControl16.Controls.Add(this.btnPg2ThemTG);
            this.groupControl16.Controls.Add(this.btnPg2TaoMoiTG);
            this.groupControl16.Controls.Add(this.labelControl79);
            this.groupControl16.Controls.Add(this.labelControl80);
            this.groupControl16.Controls.Add(this.labelControl81);
            this.groupControl16.Controls.Add(this.labelControl82);
            this.groupControl16.Controls.Add(this.txtPg2TGChinhTriXaHoi);
            this.groupControl16.Controls.Add(this.txtPg2TGCongViec);
            this.groupControl16.Controls.Add(this.txtPg2TGGhiChu);
            this.groupControl16.Controls.Add(this.txtPg2TGThanhTich);
            this.groupControl16.Location = new System.Drawing.Point(-1, 249);
            this.groupControl16.Name = "groupControl16";
            this.groupControl16.Size = new System.Drawing.Size(1278, 131);
            this.groupControl16.TabIndex = 30;
            this.groupControl16.Text = "Thông tin tham gia tổ chức chính trị - xã hội";
            // 
            // btnEditTC
            // 
            this.btnEditTC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditTC.Location = new System.Drawing.Point(808, 55);
            this.btnEditTC.Name = "btnEditTC";
            this.btnEditTC.Size = new System.Drawing.Size(75, 23);
            this.btnEditTC.TabIndex = 7;
            this.btnEditTC.Text = "Hiệu chỉnh";
            this.btnEditTC.Click += new System.EventHandler(this.btnEditTC_Click);
            // 
            // groupControl18
            // 
            this.groupControl18.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl18.Appearance.Options.UseFont = true;
            this.groupControl18.Controls.Add(this.labelControl76);
            this.groupControl18.Controls.Add(this.dtPg2TGDen);
            this.groupControl18.Controls.Add(this.labelControl77);
            this.groupControl18.Controls.Add(this.dtPg2TGTu);
            this.groupControl18.Location = new System.Drawing.Point(5, 24);
            this.groupControl18.Name = "groupControl18";
            this.groupControl18.Size = new System.Drawing.Size(138, 92);
            this.groupControl18.TabIndex = 5;
            this.groupControl18.Text = "Thời gian";
            // 
            // labelControl76
            // 
            this.labelControl76.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl76.Location = new System.Drawing.Point(5, 55);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(23, 15);
            this.labelControl76.TabIndex = 0;
            this.labelControl76.Text = "Đến";
            // 
            // dtPg2TGDen
            // 
            this.dtPg2TGDen.EditValue = null;
            this.dtPg2TGDen.Location = new System.Drawing.Point(34, 52);
            this.dtPg2TGDen.Name = "dtPg2TGDen";
            this.dtPg2TGDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg2TGDen.Properties.Appearance.Options.UseFont = true;
            this.dtPg2TGDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg2TGDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2TGDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2TGDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2TGDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2TGDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg2TGDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg2TGDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg2TGDen.Size = new System.Drawing.Size(98, 22);
            this.dtPg2TGDen.TabIndex = 2;
            // 
            // labelControl77
            // 
            this.labelControl77.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl77.Location = new System.Drawing.Point(5, 27);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(16, 15);
            this.labelControl77.TabIndex = 0;
            this.labelControl77.Text = "Từ";
            // 
            // dtPg2TGTu
            // 
            this.dtPg2TGTu.EditValue = null;
            this.dtPg2TGTu.Location = new System.Drawing.Point(34, 24);
            this.dtPg2TGTu.Name = "dtPg2TGTu";
            this.dtPg2TGTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPg2TGTu.Properties.Appearance.Options.UseFont = true;
            this.dtPg2TGTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtPg2TGTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2TGTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2TGTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dtPg2TGTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtPg2TGTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtPg2TGTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtPg2TGTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPg2TGTu.Size = new System.Drawing.Size(98, 22);
            this.dtPg2TGTu.TabIndex = 2;
            // 
            // btnPg2XoaTG
            // 
            this.btnPg2XoaTG.Location = new System.Drawing.Point(808, 87);
            this.btnPg2XoaTG.Name = "btnPg2XoaTG";
            this.btnPg2XoaTG.Size = new System.Drawing.Size(75, 23);
            this.btnPg2XoaTG.TabIndex = 0;
            this.btnPg2XoaTG.Text = "Xóa";
            this.btnPg2XoaTG.Click += new System.EventHandler(this.btnPg2XoaTG_Click);
            // 
            // btnPg2ThemTG
            // 
            this.btnPg2ThemTG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPg2ThemTG.Location = new System.Drawing.Point(726, 87);
            this.btnPg2ThemTG.Name = "btnPg2ThemTG";
            this.btnPg2ThemTG.Size = new System.Drawing.Size(75, 23);
            this.btnPg2ThemTG.TabIndex = 0;
            this.btnPg2ThemTG.Text = "Lưu";
            this.btnPg2ThemTG.Click += new System.EventHandler(this.btnPg2ThemTG_Click);
            // 
            // btnPg2TaoMoiTG
            // 
            this.btnPg2TaoMoiTG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPg2TaoMoiTG.Location = new System.Drawing.Point(726, 55);
            this.btnPg2TaoMoiTG.Name = "btnPg2TaoMoiTG";
            this.btnPg2TaoMoiTG.Size = new System.Drawing.Size(75, 23);
            this.btnPg2TaoMoiTG.TabIndex = 0;
            this.btnPg2TaoMoiTG.Text = "Tạo mới";
            this.btnPg2TaoMoiTG.Click += new System.EventHandler(this.btnPg2TaoMoiTG_Click);
            // 
            // labelControl79
            // 
            this.labelControl79.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl79.Location = new System.Drawing.Point(149, 24);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(138, 15);
            this.labelControl79.TabIndex = 0;
            this.labelControl79.Text = "Tổ chức chính trị/xã hội*";
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl80.Location = new System.Drawing.Point(627, 27);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(42, 15);
            this.labelControl80.TabIndex = 0;
            this.labelControl80.Text = "Ghi chú";
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl81.Location = new System.Drawing.Point(149, 73);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(60, 15);
            this.labelControl81.TabIndex = 0;
            this.labelControl81.Text = "Công việc*";
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl82.Location = new System.Drawing.Point(461, 24);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(59, 15);
            this.labelControl82.TabIndex = 0;
            this.labelControl82.Text = "Thành tích";
            // 
            // txtPg2TGChinhTriXaHoi
            // 
            this.txtPg2TGChinhTriXaHoi.Location = new System.Drawing.Point(149, 45);
            this.txtPg2TGChinhTriXaHoi.Name = "txtPg2TGChinhTriXaHoi";
            this.txtPg2TGChinhTriXaHoi.Properties.AllowFocused = false;
            this.txtPg2TGChinhTriXaHoi.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2TGChinhTriXaHoi.Properties.Appearance.Options.UseFont = true;
            this.txtPg2TGChinhTriXaHoi.Size = new System.Drawing.Size(303, 22);
            this.txtPg2TGChinhTriXaHoi.TabIndex = 0;
            // 
            // txtPg2TGCongViec
            // 
            this.txtPg2TGCongViec.Location = new System.Drawing.Point(149, 94);
            this.txtPg2TGCongViec.Name = "txtPg2TGCongViec";
            this.txtPg2TGCongViec.Properties.AllowFocused = false;
            this.txtPg2TGCongViec.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2TGCongViec.Properties.Appearance.Options.UseFont = true;
            this.txtPg2TGCongViec.Size = new System.Drawing.Size(303, 22);
            this.txtPg2TGCongViec.TabIndex = 0;
            // 
            // txtPg2TGGhiChu
            // 
            this.txtPg2TGGhiChu.Location = new System.Drawing.Point(624, 48);
            this.txtPg2TGGhiChu.Name = "txtPg2TGGhiChu";
            this.txtPg2TGGhiChu.Properties.AllowFocused = false;
            this.txtPg2TGGhiChu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2TGGhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtPg2TGGhiChu.Size = new System.Drawing.Size(98, 71);
            this.txtPg2TGGhiChu.TabIndex = 0;
            // 
            // txtPg2TGThanhTich
            // 
            this.txtPg2TGThanhTich.Location = new System.Drawing.Point(458, 45);
            this.txtPg2TGThanhTich.Name = "txtPg2TGThanhTich";
            this.txtPg2TGThanhTich.Properties.AllowFocused = false;
            this.txtPg2TGThanhTich.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg2TGThanhTich.Properties.Appearance.Options.UseFont = true;
            this.txtPg2TGThanhTich.Size = new System.Drawing.Size(147, 71);
            this.txtPg2TGThanhTich.TabIndex = 0;
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.btnEditQT);
            this.groupControl6.Controls.Add(this.groupControl8);
            this.groupControl6.Controls.Add(this.groupControl7);
            this.groupControl6.Controls.Add(this.btnPg2XoaGD);
            this.groupControl6.Controls.Add(this.btnPg2ThemGD);
            this.groupControl6.Controls.Add(this.btnPg2TaoMoiGD);
            this.groupControl6.Controls.Add(this.labelControl41);
            this.groupControl6.Controls.Add(this.labelControl49);
            this.groupControl6.Controls.Add(this.labelControl52);
            this.groupControl6.Controls.Add(this.labelControl56);
            this.groupControl6.Controls.Add(this.txtPg2GDCongTac);
            this.groupControl6.Controls.Add(this.txtPg2GDCongViec);
            this.groupControl6.Controls.Add(this.txtPg2GDGhiChu);
            this.groupControl6.Controls.Add(this.txtPg2GDThanhTich);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl6.Location = new System.Drawing.Point(0, 0);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(1280, 122);
            this.groupControl6.TabIndex = 27;
            this.groupControl6.Text = "Thông tin";
            // 
            // btnEditQT
            // 
            this.btnEditQT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditQT.Location = new System.Drawing.Point(808, 51);
            this.btnEditQT.Name = "btnEditQT";
            this.btnEditQT.Size = new System.Drawing.Size(75, 23);
            this.btnEditQT.TabIndex = 6;
            this.btnEditQT.Text = "Hiệu chỉnh";
            this.btnEditQT.Click += new System.EventHandler(this.btnEditQT_Click);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // ucNVQuaTrinhCongTac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ucNVQuaTrinhCongTac";
            this.Size = new System.Drawing.Size(1280, 510);
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditNgaySinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateCongtac.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateCongtac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg2QuaTrinhCongTac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuaTrinhCongTac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdbPg2GiaiDoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2GDTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDCongTac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDCongViec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2GDThanhTich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg2QuaTrinhThamGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuaTrinhThamGia)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).EndInit();
            this.groupControl16.ResumeLayout(false);
            this.groupControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).EndInit();
            this.groupControl18.ResumeLayout(false);
            this.groupControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg2TGTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGChinhTriXaHoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGCongViec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg2TGThanhTich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MemoEdit txtPg2GDGhiChu;
        private GridColumn gridColID;
        private GridColumn gridColumn1;
        private GridColumn gridColNoiCT;
        private GridColumn gridColNghenghiep;
        private GridColumn gridColTuNgay;
        private GridColumn gridColDenNgay;
        private GridColumn gridColThanhTich;
        private GridColumn gridColGhiChu;
        private GridView gridView1;
        private GridControl gvPg2QuaTrinhCongTac;
        private GridView gridviewQuaTrinhCongTac;
        private GroupControl groupControl8;
        private RadioGroup rdbPg2GiaiDoan;
        private GroupControl groupControl7;
        private LabelControl labelControl37;
        private DateEdit dtPg2GDDen;
        private LabelControl labelControl55;
        private DateEdit dtPg2GDTu;
        private SimpleButton btnPg2XoaGD;
        private SimpleButton btnPg2ThemGD;
        private SimpleButton btnPg2TaoMoiGD;
        private LabelControl labelControl41;
        private LabelControl labelControl49;
        private LabelControl labelControl52;
        private LabelControl labelControl56;
        private TextEdit txtPg2GDCongTac;
        private TextEdit txtPg2GDCongViec;
        private MemoEdit txtPg2GDThanhTich;
        private GridControl gvPg2QuaTrinhThamGia;
        private GridView gridviewQuaTrinhThamGia;
        private GridColumn gridColumn9;
        private GridColumn gridColumn10;
        private GridColumn gridColumn11;
        private GridColumn gridColumn12;
        private GridColumn gridColumn13;
        private GridColumn gridColumn14;
        private GridColumn gridColumn15;
        private XtraScrollableControl xtraScrollableControl1;
        private GroupControl groupControl16;
        private GroupControl groupControl18;
        private LabelControl labelControl76;
        private DateEdit dtPg2TGDen;
        private LabelControl labelControl77;
        private DateEdit dtPg2TGTu;
        private SimpleButton btnPg2XoaTG;
        private SimpleButton btnPg2ThemTG;
        private SimpleButton btnPg2TaoMoiTG;
        private LabelControl labelControl79;
        private LabelControl labelControl80;
        private LabelControl labelControl81;
        private LabelControl labelControl82;
        private TextEdit txtPg2TGChinhTriXaHoi;
        private TextEdit txtPg2TGCongViec;
        private MemoEdit txtPg2TGGhiChu;
        private MemoEdit txtPg2TGThanhTich;
        private GroupControl groupControl6;
        private RepositoryItemDateEdit repositoryItemDateCongtac;
        private SimpleButton btnEditQT;
        private SimpleButton btnEditTC;
        private RepositoryItemDateEdit repositoryItemDateEditNgaySinh;
        private RepositoryItemDateEdit repositoryItemDateEdit1;


    }
}
