﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;

namespace HerculesHRMT
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this.humanGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.itemUngTuyen = new DevExpress.XtraNavBar.NavBarItem();
            this.itemNhanVien = new DevExpress.XtraNavBar.NavBarItem();
            this.itemNhacNho = new DevExpress.XtraNavBar.NavBarItem();
            this.itemQuyetDinh = new DevExpress.XtraNavBar.NavBarItem();
            this.thongKeBaoCaoGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.itemTongHopNhanSu = new DevExpress.XtraNavBar.NavBarItem();
            this.comonGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.itemTinhThanh = new DevExpress.XtraNavBar.NavBarItem();
            this.itemQuanHuyen = new DevExpress.XtraNavBar.NavBarItem();
            this.itemDantoc = new DevExpress.XtraNavBar.NavBarItem();
            this.itemTongiao = new DevExpress.XtraNavBar.NavBarItem();
            this.itemTruong = new DevExpress.XtraNavBar.NavBarItem();
            this.itemNganh = new DevExpress.XtraNavBar.NavBarItem();
            this.itemHocVan = new DevExpress.XtraNavBar.NavBarItem();
            this.itemHedaotao = new DevExpress.XtraNavBar.NavBarItem();
            this.itemLoaitrinhdo = new DevExpress.XtraNavBar.NavBarItem();
            this.itemPhongKhoa = new DevExpress.XtraNavBar.NavBarItem();
            this.itemBoMon = new DevExpress.XtraNavBar.NavBarItem();
            this.itemChuVu = new DevExpress.XtraNavBar.NavBarItem();
            this.itemLoaiHopDong = new DevExpress.XtraNavBar.NavBarItem();
            this.itemLoaiNV = new DevExpress.XtraNavBar.NavBarItem();
            this.itemLoaiQD = new DevExpress.XtraNavBar.NavBarItem();
            this.itemTrangThaiLV = new DevExpress.XtraNavBar.NavBarItem();
            this.itemQuanHe = new DevExpress.XtraNavBar.NavBarItem();
            this.navbarImageListLarge = new System.Windows.Forms.ImageList(this.components);
            this.navbarImageList = new System.Windows.Forms.ImageList(this.components);
            this.appMenu = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.popupControlContainer2 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.buttonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.iHelp = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.siStatus = new DevExpress.XtraBars.BarStaticItem();
            this.siInfo = new DevExpress.XtraBars.BarStaticItem();
            this.alignButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.iBoldFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iItalicFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iUnderlinedFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.fontStyleButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.iLeftTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iCenterTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iRightTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.iLogout = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.homeRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.someLabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.someLabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.fileRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.iFind = new DevExpress.XtraBars.BarButtonItem();
            this.iClose = new DevExpress.XtraBars.BarButtonItem();
            this.iNew = new DevExpress.XtraBars.BarButtonItem();
            this.helpRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.helpRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.formatRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.skinsRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.exitRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.iOpen = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.iSave = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonMiniToolbar1 = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            this.ribbonMiniToolbar2 = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer2)).BeginInit();
            this.popupControlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            this.popupControlContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 131);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Padding = new System.Windows.Forms.Padding(6);
            this.splitContainerControl.Panel1.Controls.Add(this.navBarControl);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(680, 239);
            this.splitContainerControl.SplitterPosition = 157;
            this.splitContainerControl.TabIndex = 0;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // navBarControl
            // 
            this.navBarControl.ActiveGroup = this.humanGroup;
            this.navBarControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.humanGroup,
            this.thongKeBaoCaoGroup,
            this.comonGroup});
            this.navBarControl.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.itemTinhThanh,
            this.itemQuanHuyen,
            this.itemTruong,
            this.itemNganh,
            this.itemHocVan,
            this.itemChuVu,
            this.itemPhongKhoa,
            this.itemBoMon,
            this.itemTrangThaiLV,
            this.itemQuanHe,
            this.itemLoaiHopDong,
            this.itemUngTuyen,
            this.itemNhanVien,
            this.itemDantoc,
            this.itemTongiao,
            this.itemHedaotao,
            this.itemLoaiNV,
            this.itemLoaiQD,
            this.itemLoaitrinhdo,
            this.itemTongHopNhanSu,
            this.itemNhacNho,
            this.itemQuyetDinh});
            this.navBarControl.LargeImages = this.navbarImageListLarge;
            this.navBarControl.Location = new System.Drawing.Point(0, 0);
            this.navBarControl.Name = "navBarControl";
            this.navBarControl.OptionsNavPane.ExpandedWidth = 157;
            this.navBarControl.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.navBarControl.Size = new System.Drawing.Size(157, 227);
            this.navBarControl.SmallImages = this.navbarImageList;
            this.navBarControl.StoreDefaultPaintStyleName = true;
            this.navBarControl.TabIndex = 0;
            this.navBarControl.Text = "navBarControl1";
            // 
            // humanGroup
            // 
            this.humanGroup.Caption = "Thông tin nhân viên";
            this.humanGroup.Expanded = true;
            this.humanGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemUngTuyen),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemNhanVien),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemNhacNho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemQuyetDinh)});
            this.humanGroup.Name = "humanGroup";
            // 
            // itemUngTuyen
            // 
            this.itemUngTuyen.Caption = "Quản lý Ứng viên";
            this.itemUngTuyen.Name = "itemUngTuyen";
            this.itemUngTuyen.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemUngTuyen_LinkClicked);
            // 
            // itemNhanVien
            // 
            this.itemNhanVien.Caption = "Quản lý Nhân viên";
            this.itemNhanVien.Name = "itemNhanVien";
            this.itemNhanVien.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemNhanVien_LinkClicked);
            // 
            // itemNhacNho
            // 
            this.itemNhacNho.Caption = "Quản lý Nhắc nhở";
            this.itemNhacNho.Name = "itemNhacNho";
            this.itemNhacNho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemNhacNho_LinkClicked);
            // 
            // itemQuyetDinh
            // 
            this.itemQuyetDinh.Caption = "Quản lý Quyết định";
            this.itemQuyetDinh.Name = "itemQuyetDinh";
            this.itemQuyetDinh.Visible = false;
            this.itemQuyetDinh.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemHopDong_LinkClicked);
            // 
            // thongKeBaoCaoGroup
            // 
            this.thongKeBaoCaoGroup.Caption = "Thống kê - Báo cáo";
            this.thongKeBaoCaoGroup.Expanded = true;
            this.thongKeBaoCaoGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemTongHopNhanSu)});
            this.thongKeBaoCaoGroup.Name = "thongKeBaoCaoGroup";
            // 
            // itemTongHopNhanSu
            // 
            this.itemTongHopNhanSu.Caption = "Báo cáo nhân sự";
            this.itemTongHopNhanSu.Name = "itemTongHopNhanSu";
            this.itemTongHopNhanSu.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemTongHopNhanSu_LinkClicked);
            // 
            // comonGroup
            // 
            this.comonGroup.Caption = "Danh mục chung";
            this.comonGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemTinhThanh),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemQuanHuyen),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemDantoc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemTongiao),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemTruong),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemNganh),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemHocVan),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemHedaotao),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemLoaitrinhdo),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemPhongKhoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemBoMon),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemChuVu),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemLoaiHopDong),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemLoaiNV),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemLoaiQD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemTrangThaiLV),
            new DevExpress.XtraNavBar.NavBarItemLink(this.itemQuanHe)});
            this.comonGroup.Name = "comonGroup";
            // 
            // itemTinhThanh
            // 
            this.itemTinhThanh.Caption = "Tỉnh/Thành";
            this.itemTinhThanh.Name = "itemTinhThanh";
            this.itemTinhThanh.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemTinhThanh_LinkClicked);
            // 
            // itemQuanHuyen
            // 
            this.itemQuanHuyen.Caption = "Quận/Huyện";
            this.itemQuanHuyen.Name = "itemQuanHuyen";
            this.itemQuanHuyen.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemQuanHuyen_LinkClicked);
            // 
            // itemDantoc
            // 
            this.itemDantoc.Caption = "Dân tộc";
            this.itemDantoc.Name = "itemDantoc";
            this.itemDantoc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemDantoc_LinkClicked);
            // 
            // itemTongiao
            // 
            this.itemTongiao.Caption = "Tôn giáo";
            this.itemTongiao.Name = "itemTongiao";
            this.itemTongiao.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemTongiao_LinkClicked);
            // 
            // itemTruong
            // 
            this.itemTruong.Caption = "Trường/Học viện";
            this.itemTruong.Name = "itemTruong";
            this.itemTruong.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemTruong_LinkClicked);
            // 
            // itemNganh
            // 
            this.itemNganh.Caption = "Ngành - Chuyên Ngành";
            this.itemNganh.Name = "itemNganh";
            this.itemNganh.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemNganh_LinkClicked);
            // 
            // itemHocVan
            // 
            this.itemHocVan.Caption = "Văn bằng - Chứng chỉ";
            this.itemHocVan.Name = "itemHocVan";
            this.itemHocVan.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemHocVan_LinkClicked);
            // 
            // itemHedaotao
            // 
            this.itemHedaotao.Caption = "Hệ đào tạo";
            this.itemHedaotao.Name = "itemHedaotao";
            this.itemHedaotao.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemHedaotao_LinkClicked);
            // 
            // itemLoaitrinhdo
            // 
            this.itemLoaitrinhdo.Caption = "Loại trình độ";
            this.itemLoaitrinhdo.Name = "itemLoaitrinhdo";
            this.itemLoaitrinhdo.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemLoaitrinhdo_LinkClicked);
            // 
            // itemPhongKhoa
            // 
            this.itemPhongKhoa.Caption = "Phòng - Khoa";
            this.itemPhongKhoa.Name = "itemPhongKhoa";
            this.itemPhongKhoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemPhongKhoa_LinkClicked);
            // 
            // itemBoMon
            // 
            this.itemBoMon.Caption = "Bộ phận - Bộ môn";
            this.itemBoMon.Name = "itemBoMon";
            this.itemBoMon.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemBoMon_LinkClicked);
            // 
            // itemChuVu
            // 
            this.itemChuVu.Caption = "Chức vụ";
            this.itemChuVu.Name = "itemChuVu";
            this.itemChuVu.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemChuVu_LinkClicked);
            // 
            // itemLoaiHopDong
            // 
            this.itemLoaiHopDong.Caption = "Loại hợp đồng";
            this.itemLoaiHopDong.Name = "itemLoaiHopDong";
            this.itemLoaiHopDong.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemLoaiHopDong_LinkClicked);
            // 
            // itemLoaiNV
            // 
            this.itemLoaiNV.Caption = "Nhóm nhân viên";
            this.itemLoaiNV.Name = "itemLoaiNV";
            this.itemLoaiNV.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemLoaiNV_LinkClicked);
            // 
            // itemLoaiQD
            // 
            this.itemLoaiQD.Caption = "Loại quyết định";
            this.itemLoaiQD.Name = "itemLoaiQD";
            this.itemLoaiQD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemLoaiQD_LinkClicked);
            // 
            // itemTrangThaiLV
            // 
            this.itemTrangThaiLV.Caption = "Trạng thái làm việc";
            this.itemTrangThaiLV.Name = "itemTrangThaiLV";
            this.itemTrangThaiLV.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemTrangThaiLV_LinkClicked);
            // 
            // itemQuanHe
            // 
            this.itemQuanHe.Caption = "Quan hệ thân nhân";
            this.itemQuanHe.Name = "itemQuanHe";
            this.itemQuanHe.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.itemQuanHe_LinkClicked);
            // 
            // navbarImageListLarge
            // 
            this.navbarImageListLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("navbarImageListLarge.ImageStream")));
            this.navbarImageListLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.navbarImageListLarge.Images.SetKeyName(0, "Mail_16x16.png");
            this.navbarImageListLarge.Images.SetKeyName(1, "Organizer_16x16.png");
            // 
            // navbarImageList
            // 
            this.navbarImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("navbarImageList.ImageStream")));
            this.navbarImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.navbarImageList.Images.SetKeyName(0, "Inbox_16x16.png");
            this.navbarImageList.Images.SetKeyName(1, "Outbox_16x16.png");
            this.navbarImageList.Images.SetKeyName(2, "Drafts_16x16.png");
            this.navbarImageList.Images.SetKeyName(3, "Trash_16x16.png");
            this.navbarImageList.Images.SetKeyName(4, "Calendar_16x16.png");
            this.navbarImageList.Images.SetKeyName(5, "Tasks_16x16.png");
            // 
            // appMenu
            // 
            this.appMenu.BottomPaneControlContainer = this.popupControlContainer2;
            this.appMenu.Name = "appMenu";
            this.appMenu.Ribbon = this.ribbonControl;
            this.appMenu.RightPaneControlContainer = this.popupControlContainer1;
            this.appMenu.ShowRightPane = true;
            // 
            // popupControlContainer2
            // 
            this.popupControlContainer2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.popupControlContainer2.Appearance.Options.UseBackColor = true;
            this.popupControlContainer2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer2.Controls.Add(this.buttonEdit);
            this.popupControlContainer2.Location = new System.Drawing.Point(316, 258);
            this.popupControlContainer2.Name = "popupControlContainer2";
            this.popupControlContainer2.Ribbon = this.ribbonControl;
            this.popupControlContainer2.Size = new System.Drawing.Size(118, 28);
            this.popupControlContainer2.TabIndex = 5;
            this.popupControlContainer2.Visible = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.EditValue = "Some Text";
            this.buttonEdit.Location = new System.Drawing.Point(3, 3);
            this.buttonEdit.MenuManager = this.ribbonControl;
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.buttonEdit.Size = new System.Drawing.Size(100, 20);
            this.buttonEdit.TabIndex = 0;
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.appMenu;
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.ExpandCollapseItem.Name = "";
            this.ribbonControl.Images = this.ribbonImageCollection;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.iHelp,
            this.iAbout,
            this.siStatus,
            this.siInfo,
            this.alignButtonGroup,
            this.iBoldFontStyle,
            this.iItalicFontStyle,
            this.iUnderlinedFontStyle,
            this.fontStyleButtonGroup,
            this.iLeftTextAlign,
            this.iCenterTextAlign,
            this.iRightTextAlign,
            this.rgbiSkins,
            this.iLogout});
            this.ribbonControl.LargeImages = this.ribbonImageCollectionLarge;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 63;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbonControl.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbonControl.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.homeRibbonPage});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl.Size = new System.Drawing.Size(680, 131);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.Toolbar.ItemLinks.Add(this.iHelp);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.iHelp);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.iHelp);
            this.ribbonControl.Click += new System.EventHandler(this.ribbonControl_Click);
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            // 
            // iHelp
            // 
            this.iHelp.Caption = "Help";
            this.iHelp.Description = "Start the program help system.";
            this.iHelp.Hint = "Start the program help system";
            this.iHelp.Id = 22;
            this.iHelp.ImageIndex = 7;
            this.iHelp.LargeImageIndex = 7;
            this.iHelp.Name = "iHelp";
            // 
            // iAbout
            // 
            this.iAbout.Caption = "About";
            this.iAbout.Description = "Displays general program information.";
            this.iAbout.Hint = "Displays general program information";
            this.iAbout.Id = 24;
            this.iAbout.ImageIndex = 8;
            this.iAbout.LargeImageIndex = 8;
            this.iAbout.Name = "iAbout";
            // 
            // siStatus
            // 
            this.siStatus.Caption = "Some Status Info";
            this.siStatus.Id = 31;
            this.siStatus.Name = "siStatus";
            this.siStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // siInfo
            // 
            this.siInfo.Caption = "Some Info";
            this.siInfo.Id = 32;
            this.siInfo.Name = "siInfo";
            this.siInfo.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // alignButtonGroup
            // 
            this.alignButtonGroup.Caption = "Align Commands";
            this.alignButtonGroup.Id = 52;
            this.alignButtonGroup.ItemLinks.Add(this.iBoldFontStyle);
            this.alignButtonGroup.ItemLinks.Add(this.iItalicFontStyle);
            this.alignButtonGroup.ItemLinks.Add(this.iUnderlinedFontStyle);
            this.alignButtonGroup.Name = "alignButtonGroup";
            // 
            // iBoldFontStyle
            // 
            this.iBoldFontStyle.Caption = "Bold";
            this.iBoldFontStyle.Id = 53;
            this.iBoldFontStyle.ImageIndex = 9;
            this.iBoldFontStyle.Name = "iBoldFontStyle";
            // 
            // iItalicFontStyle
            // 
            this.iItalicFontStyle.Caption = "Italic";
            this.iItalicFontStyle.Id = 54;
            this.iItalicFontStyle.ImageIndex = 10;
            this.iItalicFontStyle.Name = "iItalicFontStyle";
            // 
            // iUnderlinedFontStyle
            // 
            this.iUnderlinedFontStyle.Caption = "Underlined";
            this.iUnderlinedFontStyle.Id = 55;
            this.iUnderlinedFontStyle.ImageIndex = 11;
            this.iUnderlinedFontStyle.Name = "iUnderlinedFontStyle";
            // 
            // fontStyleButtonGroup
            // 
            this.fontStyleButtonGroup.Caption = "Font Style";
            this.fontStyleButtonGroup.Id = 56;
            this.fontStyleButtonGroup.ItemLinks.Add(this.iLeftTextAlign);
            this.fontStyleButtonGroup.ItemLinks.Add(this.iCenterTextAlign);
            this.fontStyleButtonGroup.ItemLinks.Add(this.iRightTextAlign);
            this.fontStyleButtonGroup.Name = "fontStyleButtonGroup";
            // 
            // iLeftTextAlign
            // 
            this.iLeftTextAlign.Caption = "Left";
            this.iLeftTextAlign.Id = 57;
            this.iLeftTextAlign.ImageIndex = 12;
            this.iLeftTextAlign.Name = "iLeftTextAlign";
            // 
            // iCenterTextAlign
            // 
            this.iCenterTextAlign.Caption = "Center";
            this.iCenterTextAlign.Id = 58;
            this.iCenterTextAlign.ImageIndex = 13;
            this.iCenterTextAlign.Name = "iCenterTextAlign";
            // 
            // iRightTextAlign
            // 
            this.iRightTextAlign.Caption = "Right";
            this.iRightTextAlign.Id = 59;
            this.iRightTextAlign.ImageIndex = 14;
            this.iRightTextAlign.Name = "iRightTextAlign";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Skins";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Gallery.AllowHoverImages = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.ColumnCount = 4;
            this.rgbiSkins.Gallery.FixedHoverImageSize = false;
            this.rgbiSkins.Gallery.ImageSize = new System.Drawing.Size(32, 17);
            this.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.rgbiSkins.Gallery.RowCount = 4;
            this.rgbiSkins.Id = 60;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // iLogout
            // 
            this.iLogout.Caption = "Đăng Xuất";
            this.iLogout.Hint = "Thoát tài khoản đang đăng nhập";
            this.iLogout.Id = 62;
            this.iLogout.ImageIndex = 6;
            this.iLogout.Name = "iLogout";
            this.iLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iLogout_ItemClick_1);
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            // 
            // homeRibbonPage
            // 
            this.homeRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.homeRibbonPage.Name = "homeRibbonPage";
            this.homeRibbonPage.Text = "Home";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.iLogout);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Thông tin đăng nhập";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 370);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(680, 31);
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.popupControlContainer1.Appearance.Options.UseBackColor = true;
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer1.Controls.Add(this.someLabelControl2);
            this.popupControlContainer1.Controls.Add(this.someLabelControl1);
            this.popupControlContainer1.Location = new System.Drawing.Point(208, 168);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Ribbon = this.ribbonControl;
            this.popupControlContainer1.Size = new System.Drawing.Size(76, 70);
            this.popupControlContainer1.TabIndex = 4;
            this.popupControlContainer1.Visible = false;
            // 
            // someLabelControl2
            // 
            this.someLabelControl2.Location = new System.Drawing.Point(3, 57);
            this.someLabelControl2.Name = "someLabelControl2";
            this.someLabelControl2.Size = new System.Drawing.Size(49, 13);
            this.someLabelControl2.TabIndex = 0;
            this.someLabelControl2.Text = "Some Info";
            // 
            // someLabelControl1
            // 
            this.someLabelControl1.Location = new System.Drawing.Point(3, 3);
            this.someLabelControl1.Name = "someLabelControl1";
            this.someLabelControl1.Size = new System.Drawing.Size(49, 13);
            this.someLabelControl1.TabIndex = 0;
            this.someLabelControl1.Text = "Some Info";
            // 
            // fileRibbonPageGroup
            // 
            this.fileRibbonPageGroup.Name = "fileRibbonPageGroup";
            this.fileRibbonPageGroup.Text = "File";
            // 
            // iFind
            // 
            this.iFind.Caption = "Find";
            this.iFind.Description = "Searches for the specified info.";
            this.iFind.Hint = "Searches for the specified info";
            this.iFind.Id = 15;
            this.iFind.ImageIndex = 3;
            this.iFind.LargeImageIndex = 3;
            this.iFind.Name = "iFind";
            this.iFind.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iClose
            // 
            this.iClose.Caption = "&Close";
            this.iClose.Description = "Closes the active document.";
            this.iClose.Hint = "Closes the active document";
            this.iClose.Id = 3;
            this.iClose.ImageIndex = 2;
            this.iClose.LargeImageIndex = 2;
            this.iClose.Name = "iClose";
            this.iClose.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iNew
            // 
            this.iNew.Caption = "New";
            this.iNew.Description = "Creates a new, blank file.";
            this.iNew.Hint = "Creates a new, blank file";
            this.iNew.Id = 1;
            this.iNew.ImageIndex = 0;
            this.iNew.LargeImageIndex = 0;
            this.iNew.Name = "iNew";
            // 
            // helpRibbonPageGroup
            // 
            this.helpRibbonPageGroup.Name = "helpRibbonPageGroup";
            this.helpRibbonPageGroup.Text = "Help";
            // 
            // helpRibbonPage
            // 
            this.helpRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.helpRibbonPageGroup});
            this.helpRibbonPage.Name = "helpRibbonPage";
            this.helpRibbonPage.Text = "Help";
            // 
            // formatRibbonPageGroup
            // 
            this.formatRibbonPageGroup.Name = "formatRibbonPageGroup";
            this.formatRibbonPageGroup.Text = "Format";
            // 
            // skinsRibbonPageGroup
            // 
            this.skinsRibbonPageGroup.Name = "skinsRibbonPageGroup";
            this.skinsRibbonPageGroup.ShowCaptionButton = false;
            this.skinsRibbonPageGroup.Text = "Skins";
            // 
            // exitRibbonPageGroup
            // 
            this.exitRibbonPageGroup.Name = "exitRibbonPageGroup";
            this.exitRibbonPageGroup.Text = "Exit";
            // 
            // iExit
            // 
            this.iExit.Caption = "Exit";
            this.iExit.Description = "Closes this program after prompting you to save unsaved data.";
            this.iExit.Hint = "Closes this program after prompting you to save unsaved data";
            this.iExit.Id = 20;
            this.iExit.ImageIndex = 6;
            this.iExit.LargeImageIndex = 6;
            this.iExit.Name = "iExit";
            // 
            // iOpen
            // 
            this.iOpen.Caption = "&Open";
            this.iOpen.Description = "Opens a file.";
            this.iOpen.Hint = "Opens a file";
            this.iOpen.Id = 2;
            this.iOpen.ImageIndex = 1;
            this.iOpen.LargeImageIndex = 1;
            this.iOpen.Name = "iOpen";
            this.iOpen.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iSaveAs
            // 
            this.iSaveAs.Caption = "Save As";
            this.iSaveAs.Description = "Saves the active document in a different location.";
            this.iSaveAs.Hint = "Saves the active document in a different location";
            this.iSaveAs.Id = 17;
            this.iSaveAs.ImageIndex = 5;
            this.iSaveAs.LargeImageIndex = 5;
            this.iSaveAs.Name = "iSaveAs";
            // 
            // iSave
            // 
            this.iSave.Caption = "&Save";
            this.iSave.Description = "Saves the active document.";
            this.iSave.Hint = "Saves the active document";
            this.iSave.Id = 16;
            this.iSave.ImageIndex = 4;
            this.iSave.LargeImageIndex = 4;
            this.iSave.Name = "iSave";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 401);
            this.Controls.Add(this.splitContainerControl);
            this.Controls.Add(this.ribbonControl);
            this.Controls.Add(this.popupControlContainer1);
            this.Controls.Add(this.popupControlContainer2);
            this.Controls.Add(this.ribbonStatusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Chương trình Quản Lý Nhân Sự HerculesHRMT- Trường Cao Đẳng Kỹ Thuật Cao Thắng (ve" +
                "r1.1.8 - 01/11/16) ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer2)).EndInit();
            this.popupControlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            this.popupControlContainer1.ResumeLayout(false);
            this.popupControlContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private SplitContainerControl splitContainerControl;
        private ApplicationMenu appMenu;
        private PopupControlContainer popupControlContainer1;
        private LabelControl someLabelControl2;
        private LabelControl someLabelControl1;
        private PopupControlContainer popupControlContainer2;
        private ButtonEdit buttonEdit;
        private RibbonStatusBar ribbonStatusBar;
        private ImageCollection ribbonImageCollection;
        private ImageCollection ribbonImageCollectionLarge;
        private NavBarControl navBarControl;
        private NavBarGroup comonGroup;
        private NavBarGroup humanGroup;
        private NavBarItem itemTinhThanh;
        private NavBarItem itemQuanHuyen;
        private NavBarItem itemTruong;
        private NavBarItem itemHocVan;
        private ImageList navbarImageList;
        private ImageList navbarImageListLarge;
        private RibbonPageGroup fileRibbonPageGroup;
        private BarButtonItem iFind;
        private BarButtonItem iClose;
        private BarButtonItem iNew;
        private RibbonPageGroup helpRibbonPageGroup;
        private RibbonPage helpRibbonPage;
        private RibbonPageGroup formatRibbonPageGroup;
        private RibbonPageGroup skinsRibbonPageGroup;
        private RibbonPageGroup exitRibbonPageGroup;
        private BarButtonItem iExit;
        private BarButtonItem iOpen;
        private BarButtonItem iSaveAs;
        private BarButtonItem iSave;
        private NavBarItem itemChuVu;
        private NavBarItem itemPhongKhoa;
        private NavBarItem itemBoMon;
        private NavBarItem itemTrangThaiLV;
        private NavBarGroup thongKeBaoCaoGroup;
        private NavBarItem itemQuanHe;
        private NavBarItem itemLoaiHopDong;
        private NavBarItem itemUngTuyen;
        private NavBarItem itemNhanVien;
        private RibbonControl ribbonControl;
        private BarButtonItem iHelp;
        private BarButtonItem iAbout;
        private BarStaticItem siStatus;
        private BarStaticItem siInfo;
        private BarButtonGroup alignButtonGroup;
        private BarButtonItem iBoldFontStyle;
        private BarButtonItem iItalicFontStyle;
        private BarButtonItem iUnderlinedFontStyle;
        private BarButtonGroup fontStyleButtonGroup;
        private BarButtonItem iLeftTextAlign;
        private BarButtonItem iCenterTextAlign;
        private BarButtonItem iRightTextAlign;
        private RibbonGalleryBarItem rgbiSkins;
        private BarButtonItem iLogout;
        private RibbonPage homeRibbonPage;
        private RibbonPageGroup ribbonPageGroup1;
        private NavBarItem itemNganh;
        private NavBarItem itemDantoc;
        private NavBarItem itemTongiao;
        private NavBarItem itemHedaotao;
        private NavBarItem itemLoaiNV;
        private NavBarItem itemLoaiQD;
        private NavBarItem itemLoaitrinhdo;
        private RibbonMiniToolbar ribbonMiniToolbar1;
        private RibbonMiniToolbar ribbonMiniToolbar2;
        private NavBarItem itemTongHopNhanSu;
        private NavBarItem itemNhacNho;
        private NavBarItem itemQuyetDinh;

    }
}
