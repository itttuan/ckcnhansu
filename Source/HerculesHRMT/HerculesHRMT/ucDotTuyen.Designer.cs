﻿namespace HerculesHRMT
{
    partial class ucDotTuyen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grcDotTuyen = new DevExpress.XtraEditors.GroupControl();
            this.deNgayBatDau = new DevExpress.XtraEditors.DateEdit();
            this.deNgayKetThuc = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaDot = new DevExpress.XtraEditors.TextEdit();
            this.deNgay = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.teGio = new DevExpress.XtraEditors.TimeEdit();
            this.cbbeThang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbeNam = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcDotTuyen = new DevExpress.XtraGrid.GridControl();
            this.gridViewDotTuyen = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaDot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayGio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayBatDau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayKetThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpideNgay = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).BeginInit();
            this.grcDotTuyen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBatDau.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBatDau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKetThuc.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKetThuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teGio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDotTuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDotTuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpideNgay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpideNgay.VistaTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.grcDotTuyen);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1016, 237);
            this.panelControl1.TabIndex = 7;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // grcDotTuyen
            // 
            this.grcDotTuyen.Controls.Add(this.deNgayBatDau);
            this.grcDotTuyen.Controls.Add(this.deNgayKetThuc);
            this.grcDotTuyen.Controls.Add(this.labelControl4);
            this.grcDotTuyen.Controls.Add(this.txtPhong);
            this.grcDotTuyen.Controls.Add(this.labelControl2);
            this.grcDotTuyen.Controls.Add(this.labelControl10);
            this.grcDotTuyen.Controls.Add(this.labelControl3);
            this.grcDotTuyen.Controls.Add(this.meGhiChu);
            this.grcDotTuyen.Controls.Add(this.labelControl5);
            this.grcDotTuyen.Controls.Add(this.labelControl9);
            this.grcDotTuyen.Controls.Add(this.labelControl6);
            this.grcDotTuyen.Controls.Add(this.labelControl8);
            this.grcDotTuyen.Controls.Add(this.txtMaDot);
            this.grcDotTuyen.Controls.Add(this.deNgay);
            this.grcDotTuyen.Controls.Add(this.labelControl7);
            this.grcDotTuyen.Controls.Add(this.teGio);
            this.grcDotTuyen.Controls.Add(this.cbbeThang);
            this.grcDotTuyen.Controls.Add(this.cbbeNam);
            this.grcDotTuyen.Location = new System.Drawing.Point(4, 5);
            this.grcDotTuyen.Name = "grcDotTuyen";
            this.grcDotTuyen.Size = new System.Drawing.Size(1005, 196);
            this.grcDotTuyen.TabIndex = 18;
            this.grcDotTuyen.Text = "Thông tin cơ bản";
            this.grcDotTuyen.Paint += new System.Windows.Forms.PaintEventHandler(this.grcDotTuyen_Paint);
            // 
            // deNgayBatDau
            // 
            this.deNgayBatDau.EditValue = null;
            this.deNgayBatDau.Location = new System.Drawing.Point(363, 26);
            this.deNgayBatDau.Name = "deNgayBatDau";
            this.deNgayBatDau.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.deNgayBatDau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayBatDau.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayBatDau.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayBatDau.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayBatDau.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayBatDau.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayBatDau.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayBatDau.Properties.NullDate = "1-1-0001";
            this.deNgayBatDau.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayBatDau.Size = new System.Drawing.Size(100, 20);
            this.deNgayBatDau.TabIndex = 28;
            // 
            // deNgayKetThuc
            // 
            this.deNgayKetThuc.EditValue = null;
            this.deNgayKetThuc.Location = new System.Drawing.Point(363, 56);
            this.deNgayKetThuc.Name = "deNgayKetThuc";
            this.deNgayKetThuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayKetThuc.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKetThuc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKetThuc.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKetThuc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKetThuc.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayKetThuc.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayKetThuc.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayKetThuc.Size = new System.Drawing.Size(100, 20);
            this.deNgayKetThuc.TabIndex = 27;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(26, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(74, 15);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Mã đợt tuyển";
            // 
            // txtPhong
            // 
            this.txtPhong.Location = new System.Drawing.Point(110, 169);
            this.txtPhong.Name = "txtPhong";
            this.txtPhong.Size = new System.Drawing.Size(100, 20);
            this.txtPhong.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(65, 61);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 15);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tháng";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(80, 145);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(20, 15);
            this.labelControl10.TabIndex = 25;
            this.labelControl10.Text = "Giờ";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(315, 84);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(42, 15);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Ghi chú";
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(363, 84);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(260, 105);
            this.meGhiChu.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(64, 174);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 15);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Phòng";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(280, 61);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(77, 15);
            this.labelControl9.TabIndex = 22;
            this.labelControl9.Text = "Ngày kết thúc";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(72, 118);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(28, 15);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Ngày";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(284, 33);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(73, 15);
            this.labelControl8.TabIndex = 21;
            this.labelControl8.Text = "Ngày bắt đầu";
            // 
            // txtMaDot
            // 
            this.txtMaDot.EditValue = "000000";
            this.txtMaDot.Enabled = false;
            this.txtMaDot.Location = new System.Drawing.Point(110, 28);
            this.txtMaDot.Name = "txtMaDot";
            this.txtMaDot.Size = new System.Drawing.Size(100, 20);
            this.txtMaDot.TabIndex = 1;
            // 
            // deNgay
            // 
            this.deNgay.EditValue = null;
            this.deNgay.Location = new System.Drawing.Point(110, 113);
            this.deNgay.Name = "deNgay";
            this.deNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgay.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgay.Properties.NullDate = "";
            this.deNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgay.Size = new System.Drawing.Size(100, 20);
            this.deNgay.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(74, 89);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(26, 15);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Năm";
            // 
            // teGio
            // 
            this.teGio.EditValue = new System.DateTime(2016, 4, 18, 0, 0, 0, 0);
            this.teGio.Location = new System.Drawing.Point(110, 140);
            this.teGio.Name = "teGio";
            this.teGio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.teGio.Properties.Mask.EditMask = "\\d?\\d:\\d\\d";
            this.teGio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.teGio.Size = new System.Drawing.Size(100, 20);
            this.teGio.TabIndex = 5;
            // 
            // cbbeThang
            // 
            this.cbbeThang.Location = new System.Drawing.Point(110, 56);
            this.cbbeThang.Name = "cbbeThang";
            this.cbbeThang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeThang.Size = new System.Drawing.Size(100, 20);
            this.cbbeThang.TabIndex = 2;
            this.cbbeThang.SelectedIndexChanged += new System.EventHandler(this.cbbeThang_SelectedIndexChanged);
            // 
            // cbbeNam
            // 
            this.cbbeNam.Location = new System.Drawing.Point(110, 84);
            this.cbbeNam.Name = "cbbeNam";
            this.cbbeNam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeNam.Size = new System.Drawing.Size(100, 20);
            this.cbbeNam.TabIndex = 3;
            this.cbbeNam.SelectedIndexChanged += new System.EventHandler(this.cbbeNam_SelectedIndexChanged);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(749, 207);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 13;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(654, 207);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 12;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCapNhat.Location = new System.Drawing.Point(560, 207);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 11;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(467, 207);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 10;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(395, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(161, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Đợt tuyển dụng";
            // 
            // gcDotTuyen
            // 
            this.gcDotTuyen.Location = new System.Drawing.Point(4, 244);
            this.gcDotTuyen.MainView = this.gridViewDotTuyen;
            this.gcDotTuyen.Name = "gcDotTuyen";
            this.gcDotTuyen.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpideNgay,
            this.repositoryItemDateEdit1});
            this.gcDotTuyen.Size = new System.Drawing.Size(1014, 280);
            this.gcDotTuyen.TabIndex = 17;
            this.gcDotTuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDotTuyen});
            this.gcDotTuyen.Click += new System.EventHandler(this.gcDotTuyen_Click);
            // 
            // gridViewDotTuyen
            // 
            this.gridViewDotTuyen.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaDot,
            this.gridColThang,
            this.gridColNam,
            this.gridColNgayGio,
            this.gridColPhong,
            this.gridColNgayBatDau,
            this.gridColNgayKetThuc,
            this.gridColGhiChu});
            this.gridViewDotTuyen.GridControl = this.gcDotTuyen;
            this.gridViewDotTuyen.GroupPanelText = " ";
            this.gridViewDotTuyen.Name = "gridViewDotTuyen";
            this.gridViewDotTuyen.OptionsView.ColumnAutoWidth = false;
            this.gridViewDotTuyen.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDotTuyen.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDotTuyen_FocusedRowChanged);
            // 
            // gridColMaDot
            // 
            this.gridColMaDot.Caption = "Mã Đợt";
            this.gridColMaDot.FieldName = "MaDot";
            this.gridColMaDot.Name = "gridColMaDot";
            this.gridColMaDot.OptionsColumn.AllowEdit = false;
            this.gridColMaDot.OptionsColumn.FixedWidth = true;
            this.gridColMaDot.OptionsColumn.ReadOnly = true;
            this.gridColMaDot.Visible = true;
            this.gridColMaDot.VisibleIndex = 0;
            this.gridColMaDot.Width = 100;
            // 
            // gridColThang
            // 
            this.gridColThang.Caption = "Tháng";
            this.gridColThang.FieldName = "Thang";
            this.gridColThang.Name = "gridColThang";
            this.gridColThang.OptionsColumn.AllowEdit = false;
            this.gridColThang.OptionsColumn.FixedWidth = true;
            this.gridColThang.OptionsColumn.ReadOnly = true;
            this.gridColThang.Visible = true;
            this.gridColThang.VisibleIndex = 1;
            this.gridColThang.Width = 50;
            // 
            // gridColNam
            // 
            this.gridColNam.Caption = "Năm";
            this.gridColNam.FieldName = "Nam";
            this.gridColNam.Name = "gridColNam";
            this.gridColNam.OptionsColumn.AllowEdit = false;
            this.gridColNam.OptionsColumn.FixedWidth = true;
            this.gridColNam.OptionsColumn.ReadOnly = true;
            this.gridColNam.Visible = true;
            this.gridColNam.VisibleIndex = 2;
            this.gridColNam.Width = 50;
            // 
            // gridColNgayGio
            // 
            this.gridColNgayGio.Caption = "Ngày giờ";
            this.gridColNgayGio.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColNgayGio.FieldName = "NgayGio";
            this.gridColNgayGio.Name = "gridColNgayGio";
            this.gridColNgayGio.OptionsColumn.AllowEdit = false;
            this.gridColNgayGio.OptionsColumn.FixedWidth = true;
            this.gridColNgayGio.OptionsColumn.ReadOnly = true;
            this.gridColNgayGio.Visible = true;
            this.gridColNgayGio.VisibleIndex = 3;
            this.gridColNgayGio.Width = 120;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm tt";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy hh:mm tt";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColPhong
            // 
            this.gridColPhong.Caption = "Phòng";
            this.gridColPhong.FieldName = "Phong";
            this.gridColPhong.Name = "gridColPhong";
            this.gridColPhong.OptionsColumn.AllowEdit = false;
            this.gridColPhong.OptionsColumn.FixedWidth = true;
            this.gridColPhong.OptionsColumn.ReadOnly = true;
            this.gridColPhong.Visible = true;
            this.gridColPhong.VisibleIndex = 4;
            // 
            // gridColNgayBatDau
            // 
            this.gridColNgayBatDau.Caption = "Ngày bắt đầu";
            this.gridColNgayBatDau.ColumnEdit = this.rpideNgay;
            this.gridColNgayBatDau.FieldName = "NgayBatDau";
            this.gridColNgayBatDau.Name = "gridColNgayBatDau";
            this.gridColNgayBatDau.OptionsColumn.AllowEdit = false;
            this.gridColNgayBatDau.OptionsColumn.FixedWidth = true;
            this.gridColNgayBatDau.OptionsColumn.ReadOnly = true;
            this.gridColNgayBatDau.Visible = true;
            this.gridColNgayBatDau.VisibleIndex = 5;
            this.gridColNgayBatDau.Width = 100;
            // 
            // gridColNgayKetThuc
            // 
            this.gridColNgayKetThuc.Caption = "Ngày Kết Thúc";
            this.gridColNgayKetThuc.ColumnEdit = this.rpideNgay;
            this.gridColNgayKetThuc.FieldName = "NgayKetThuc";
            this.gridColNgayKetThuc.Name = "gridColNgayKetThuc";
            this.gridColNgayKetThuc.OptionsColumn.AllowEdit = false;
            this.gridColNgayKetThuc.OptionsColumn.FixedWidth = true;
            this.gridColNgayKetThuc.OptionsColumn.ReadOnly = true;
            this.gridColNgayKetThuc.Visible = true;
            this.gridColNgayKetThuc.VisibleIndex = 6;
            this.gridColNgayKetThuc.Width = 100;
            // 
            // rpideNgay
            // 
            this.rpideNgay.AutoHeight = false;
            this.rpideNgay.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpideNgay.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.rpideNgay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rpideNgay.EditFormat.FormatString = "dd/MM/yyyy";
            this.rpideNgay.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rpideNgay.Mask.EditMask = "dd/MM/yyyy";
            this.rpideNgay.Mask.UseMaskAsDisplayFormat = true;
            this.rpideNgay.Name = "rpideNgay";
            this.rpideNgay.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.FixedWidth = true;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 7;
            this.gridColGhiChu.Width = 200;
            // 
            // ucDotTuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcDotTuyen);
            this.Controls.Add(this.panelControl1);
            this.Name = "ucDotTuyen";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.ucDotTuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).EndInit();
            this.grcDotTuyen.ResumeLayout(false);
            this.grcDotTuyen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBatDau.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBatDau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKetThuc.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKetThuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teGio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDotTuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDotTuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpideNgay.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpideNgay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MemoEdit meGhiChu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtMaDot;
        private DevExpress.XtraGrid.GridControl gcDotTuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDotTuyen;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaDot;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayGio;
        private DevExpress.XtraGrid.Columns.GridColumn gridColGhiChu;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeNam;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeThang;
        private DevExpress.XtraEditors.TimeEdit teGio;
        private DevExpress.XtraEditors.DateEdit deNgay;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtPhong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayBatDau;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayKetThuc;
        private DevExpress.XtraEditors.GroupControl grcDotTuyen;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rpideNgay;
        private DevExpress.XtraEditors.DateEdit deNgayKetThuc;
        private DevExpress.XtraEditors.DateEdit deNgayBatDau;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;

    }
}
