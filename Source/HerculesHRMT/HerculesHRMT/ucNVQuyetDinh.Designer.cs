﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVQuyetDinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcQuyetDinh = new DevExpress.XtraEditors.GroupControl();
            this.txtHeSoCV = new DevExpress.XtraEditors.TextEdit();
            this.chbHienTaiHuong = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lueNguoiKy = new DevExpress.XtraEditors.LookUpEdit();
            this.cbbPg1Bac = new DevExpress.XtraEditors.LookUpEdit();
            this.txtHeSo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.txtLanThu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenQD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.meNoiDung = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucDen = new DevExpress.XtraEditors.DateEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.deHieuLucTu = new DevExpress.XtraEditors.DateEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.deNgayKy = new DevExpress.XtraEditors.DateEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoaiQD = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.txtSoQD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhuCap = new DevExpress.XtraEditors.TextEdit();
            this.btnPg3Sua = new DevExpress.XtraEditors.SimpleButton();
            this.cbbPg1MaNgach = new DevExpress.XtraEditors.LookUpEdit();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.txtTileVuot = new DevExpress.XtraEditors.TextEdit();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCThamnien = new DevExpress.XtraEditors.TextEdit();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.gridQuyetDinh = new DevExpress.XtraGrid.GridControl();
            this.gridViewQuyetDinh = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColLuongID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongSQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiQD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenLoaiQĐ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLanThu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNguoiKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenNguoiKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColTuNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColDenNgay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongNgach = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongBac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongHeSo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongVuot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColPhuCap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCCV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLuongPCTN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNoiDungQĐ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColHienTaiHuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuyetDinh)).BeginInit();
            this.gcQuyetDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSoCV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHienTaiHuong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNguoiKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLanThu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTileVuot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCThamnien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuyetDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcQuyetDinh
            // 
            this.gcQuyetDinh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gcQuyetDinh.Controls.Add(this.txtHeSoCV);
            this.gcQuyetDinh.Controls.Add(this.chbHienTaiHuong);
            this.gcQuyetDinh.Controls.Add(this.labelControl5);
            this.gcQuyetDinh.Controls.Add(this.lueNguoiKy);
            this.gcQuyetDinh.Controls.Add(this.cbbPg1Bac);
            this.gcQuyetDinh.Controls.Add(this.txtHeSo);
            this.gcQuyetDinh.Controls.Add(this.labelControl86);
            this.gcQuyetDinh.Controls.Add(this.labelControl85);
            this.gcQuyetDinh.Controls.Add(this.txtLanThu);
            this.gcQuyetDinh.Controls.Add(this.labelControl4);
            this.gcQuyetDinh.Controls.Add(this.txtTenQD);
            this.gcQuyetDinh.Controls.Add(this.labelControl3);
            this.gcQuyetDinh.Controls.Add(this.meNoiDung);
            this.gcQuyetDinh.Controls.Add(this.labelControl117);
            this.gcQuyetDinh.Controls.Add(this.deHieuLucDen);
            this.gcQuyetDinh.Controls.Add(this.labelControl116);
            this.gcQuyetDinh.Controls.Add(this.deHieuLucTu);
            this.gcQuyetDinh.Controls.Add(this.labelControl115);
            this.gcQuyetDinh.Controls.Add(this.labelControl114);
            this.gcQuyetDinh.Controls.Add(this.deNgayKy);
            this.gcQuyetDinh.Controls.Add(this.labelControl113);
            this.gcQuyetDinh.Controls.Add(this.lueLoaiQD);
            this.gcQuyetDinh.Controls.Add(this.labelControl112);
            this.gcQuyetDinh.Controls.Add(this.txtSoQD);
            this.gcQuyetDinh.Controls.Add(this.labelControl111);
            this.gcQuyetDinh.Controls.Add(this.labelControl2);
            this.gcQuyetDinh.Controls.Add(this.labelControl1);
            this.gcQuyetDinh.Controls.Add(this.txtPhuCap);
            this.gcQuyetDinh.Controls.Add(this.btnPg3Sua);
            this.gcQuyetDinh.Controls.Add(this.cbbPg1MaNgach);
            this.gcQuyetDinh.Controls.Add(this.btnPg3Xoa);
            this.gcQuyetDinh.Controls.Add(this.btnPg3Them);
            this.gcQuyetDinh.Controls.Add(this.btnPg3TaoMoi);
            this.gcQuyetDinh.Controls.Add(this.txtTileVuot);
            this.gcQuyetDinh.Controls.Add(this.labelControl91);
            this.gcQuyetDinh.Controls.Add(this.txtPCThamnien);
            this.gcQuyetDinh.Controls.Add(this.labelControl90);
            this.gcQuyetDinh.Controls.Add(this.labelControl84);
            this.gcQuyetDinh.Location = new System.Drawing.Point(3, 3);
            this.gcQuyetDinh.Name = "gcQuyetDinh";
            this.gcQuyetDinh.Size = new System.Drawing.Size(1196, 283);
            this.gcQuyetDinh.TabIndex = 0;
            this.gcQuyetDinh.Text = "Lương";
            // 
            // txtHeSoCV
            // 
            this.txtHeSoCV.Location = new System.Drawing.Point(430, 123);
            this.txtHeSoCV.Name = "txtHeSoCV";
            this.txtHeSoCV.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeSoCV.Properties.Appearance.Options.UseFont = true;
            this.txtHeSoCV.Properties.DisplayFormat.FormatString = "##0.#0";
            this.txtHeSoCV.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHeSoCV.Properties.EditFormat.FormatString = "##0.#0";
            this.txtHeSoCV.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHeSoCV.Properties.Mask.EditMask = "##0.#0";
            this.txtHeSoCV.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtHeSoCV.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtHeSoCV.Size = new System.Drawing.Size(49, 22);
            this.txtHeSoCV.TabIndex = 135;
            // 
            // chbHienTaiHuong
            // 
            this.chbHienTaiHuong.EditValue = true;
            this.chbHienTaiHuong.Location = new System.Drawing.Point(11, 229);
            this.chbHienTaiHuong.Name = "chbHienTaiHuong";
            this.chbHienTaiHuong.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbHienTaiHuong.Properties.Appearance.Options.UseFont = true;
            this.chbHienTaiHuong.Properties.Caption = "Đây là quyết định mới nhất hiện tại";
            this.chbHienTaiHuong.Size = new System.Drawing.Size(254, 20);
            this.chbHienTaiHuong.TabIndex = 134;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(345, 130);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(81, 15);
            this.labelControl5.TabIndex = 133;
            this.labelControl5.Text = "Hệ số chức vụ";
            // 
            // lueNguoiKy
            // 
            this.lueNguoiKy.Location = new System.Drawing.Point(67, 90);
            this.lueNguoiKy.Name = "lueNguoiKy";
            this.lueNguoiKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueNguoiKy.Properties.Appearance.Options.UseFont = true;
            this.lueNguoiKy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNguoiKy.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaNV", 15, "Mã NV"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HoTen", 30, "Họ Tên")});
            this.lueNguoiKy.Properties.DisplayMember = "HoTen";
            this.lueNguoiKy.Properties.NullText = "";
            this.lueNguoiKy.Properties.ValueMember = "MaNV";
            this.lueNguoiKy.Size = new System.Drawing.Size(159, 22);
            this.lueNguoiKy.TabIndex = 131;
            // 
            // cbbPg1Bac
            // 
            this.cbbPg1Bac.Location = new System.Drawing.Point(430, 57);
            this.cbbPg1Bac.Name = "cbbPg1Bac";
            this.cbbPg1Bac.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1Bac.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1Bac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1Bac.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Bac", "Bậc lương"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("HeSo", "Hệ số", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cbbPg1Bac.Properties.DisplayMember = "Bac";
            this.cbbPg1Bac.Properties.NullText = "";
            this.cbbPg1Bac.Properties.ValueMember = "Bac";
            this.cbbPg1Bac.Size = new System.Drawing.Size(49, 22);
            this.cbbPg1Bac.TabIndex = 10;
            this.cbbPg1Bac.EditValueChanged += new System.EventHandler(this.cbbPg1Bac_EditValueChanged_1);
            // 
            // txtHeSo
            // 
            this.txtHeSo.Location = new System.Drawing.Point(513, 57);
            this.txtHeSo.Name = "txtHeSo";
            this.txtHeSo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeSo.Properties.Appearance.Options.UseFont = true;
            this.txtHeSo.Properties.DisplayFormat.FormatString = "##0.#0";
            this.txtHeSo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtHeSo.Properties.ReadOnly = true;
            this.txtHeSo.Size = new System.Drawing.Size(33, 22);
            this.txtHeSo.TabIndex = 11;
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl86.Location = new System.Drawing.Point(480, 64);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(32, 15);
            this.labelControl86.TabIndex = 130;
            this.labelControl86.Text = "Hệ số";
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl85.Location = new System.Drawing.Point(404, 64);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(22, 15);
            this.labelControl85.TabIndex = 129;
            this.labelControl85.Text = "Bậc";
            // 
            // txtLanThu
            // 
            this.txtLanThu.Location = new System.Drawing.Point(256, 57);
            this.txtLanThu.Name = "txtLanThu";
            this.txtLanThu.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(247)))));
            this.txtLanThu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanThu.Properties.Appearance.Options.UseBackColor = true;
            this.txtLanThu.Properties.Appearance.Options.UseFont = true;
            this.txtLanThu.Size = new System.Drawing.Size(30, 22);
            this.txtLanThu.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(232, 64);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(21, 15);
            this.labelControl4.TabIndex = 128;
            this.labelControl4.Text = "Lần";
            // 
            // txtTenQD
            // 
            this.txtTenQD.Location = new System.Drawing.Point(682, 252);
            this.txtTenQD.Name = "txtTenQD";
            this.txtTenQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenQD.Properties.Appearance.Options.UseFont = true;
            this.txtTenQD.Size = new System.Drawing.Size(159, 22);
            this.txtTenQD.TabIndex = 2;
            this.txtTenQD.Visible = false;
            this.txtTenQD.EditValueChanged += new System.EventHandler(this.txtTenQD_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(638, 259);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(42, 15);
            this.labelControl3.TabIndex = 127;
            this.labelControl3.Text = "Tên QĐ";
            this.labelControl3.Visible = false;
            // 
            // meNoiDung
            // 
            this.meNoiDung.Location = new System.Drawing.Point(650, 31);
            this.meNoiDung.Name = "meNoiDung";
            this.meNoiDung.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meNoiDung.Properties.Appearance.Options.UseFont = true;
            this.meNoiDung.Size = new System.Drawing.Size(340, 191);
            this.meNoiDung.TabIndex = 15;
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl117.Location = new System.Drawing.Point(597, 31);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(49, 15);
            this.labelControl117.TabIndex = 126;
            this.labelControl117.Text = "Nội dung";
            // 
            // deHieuLucDen
            // 
            this.deHieuLucDen.EditValue = null;
            this.deHieuLucDen.Location = new System.Drawing.Point(67, 189);
            this.deHieuLucDen.Name = "deHieuLucDen";
            this.deHieuLucDen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucDen.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucDen.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucDen.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucDen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucDen.Size = new System.Drawing.Size(84, 22);
            this.deHieuLucDen.TabIndex = 8;
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl116.Location = new System.Drawing.Point(10, 196);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(53, 15);
            this.labelControl116.TabIndex = 125;
            this.labelControl116.Text = "Đến ngày";
            // 
            // deHieuLucTu
            // 
            this.deHieuLucTu.EditValue = null;
            this.deHieuLucTu.Location = new System.Drawing.Point(67, 156);
            this.deHieuLucTu.Name = "deHieuLucTu";
            this.deHieuLucTu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deHieuLucTu.Properties.Appearance.Options.UseFont = true;
            this.deHieuLucTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHieuLucTu.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHieuLucTu.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deHieuLucTu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deHieuLucTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deHieuLucTu.Size = new System.Drawing.Size(84, 22);
            this.deHieuLucTu.TabIndex = 7;
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl115.Location = new System.Drawing.Point(17, 163);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(46, 15);
            this.labelControl115.TabIndex = 124;
            this.labelControl115.Text = "Từ ngày";
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl114.Location = new System.Drawing.Point(11, 97);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(52, 15);
            this.labelControl114.TabIndex = 123;
            this.labelControl114.Text = "Người ký";
            // 
            // deNgayKy
            // 
            this.deNgayKy.EditValue = null;
            this.deNgayKy.Location = new System.Drawing.Point(67, 123);
            this.deNgayKy.Name = "deNgayKy";
            this.deNgayKy.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgayKy.Properties.Appearance.Options.UseFont = true;
            this.deNgayKy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayKy.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayKy.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayKy.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayKy.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayKy.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayKy.Size = new System.Drawing.Size(84, 22);
            this.deNgayKy.TabIndex = 6;
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl113.Location = new System.Drawing.Point(19, 130);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(44, 15);
            this.labelControl113.TabIndex = 122;
            this.labelControl113.Text = "Ngày ký";
            // 
            // lueLoaiQD
            // 
            this.lueLoaiQD.Location = new System.Drawing.Point(67, 57);
            this.lueLoaiQD.Name = "lueLoaiQD";
            this.lueLoaiQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lueLoaiQD.Properties.Appearance.Options.UseFont = true;
            this.lueLoaiQD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoaiQD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaLoaiQD", "Mã Loại", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenLoaiQD", "Loại quyết định")});
            this.lueLoaiQD.Properties.DisplayMember = "TenLoaiQD";
            this.lueLoaiQD.Properties.NullText = "";
            this.lueLoaiQD.Properties.ValueMember = "MaLoaiQD";
            this.lueLoaiQD.Size = new System.Drawing.Size(159, 22);
            this.lueLoaiQD.TabIndex = 3;
            this.lueLoaiQD.EditValueChanged += new System.EventHandler(this.lueLoaiQD_EditValueChanged);
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl112.Location = new System.Drawing.Point(18, 64);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(45, 15);
            this.labelControl112.TabIndex = 121;
            this.labelControl112.Text = "Loại QĐ";
            // 
            // txtSoQD
            // 
            this.txtSoQD.Location = new System.Drawing.Point(67, 24);
            this.txtSoQD.Name = "txtSoQD";
            this.txtSoQD.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQD.Properties.Appearance.Options.UseFont = true;
            this.txtSoQD.Size = new System.Drawing.Size(159, 22);
            this.txtSoQD.TabIndex = 1;
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl111.Location = new System.Drawing.Point(27, 31);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(36, 15);
            this.labelControl111.TabIndex = 120;
            this.labelControl111.Text = "Số QĐ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(481, 97);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(9, 15);
            this.labelControl2.TabIndex = 110;
            this.labelControl2.Text = "%";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(329, 163);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(97, 15);
            this.labelControl1.TabIndex = 109;
            this.labelControl1.Text = "Phụ cấp nhà giáo";
            // 
            // txtPhuCap
            // 
            this.txtPhuCap.Location = new System.Drawing.Point(430, 156);
            this.txtPhuCap.Name = "txtPhuCap";
            this.txtPhuCap.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuCap.Properties.Appearance.Options.UseFont = true;
            this.txtPhuCap.Properties.DisplayFormat.FormatString = "##0.#0";
            this.txtPhuCap.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPhuCap.Properties.EditFormat.FormatString = "##0.#0";
            this.txtPhuCap.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPhuCap.Properties.Mask.EditMask = "##0.#0";
            this.txtPhuCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhuCap.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPhuCap.Size = new System.Drawing.Size(105, 22);
            this.txtPhuCap.TabIndex = 13;
            // 
            // btnPg3Sua
            // 
            this.btnPg3Sua.Location = new System.Drawing.Point(352, 255);
            this.btnPg3Sua.Name = "btnPg3Sua";
            this.btnPg3Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Sua.TabIndex = 18;
            this.btnPg3Sua.Text = "Hiệu chỉnh";
            this.btnPg3Sua.Click += new System.EventHandler(this.btnPg3Sua_Click);
            // 
            // cbbPg1MaNgach
            // 
            this.cbbPg1MaNgach.Location = new System.Drawing.Point(430, 24);
            this.cbbPg1MaNgach.Name = "cbbPg1MaNgach";
            this.cbbPg1MaNgach.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg1MaNgach.Properties.Appearance.Options.UseFont = true;
            this.cbbPg1MaNgach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg1MaNgach.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaNgach", "Mã ngạch")});
            this.cbbPg1MaNgach.Properties.DisplayMember = "MaNgach";
            this.cbbPg1MaNgach.Properties.NullText = "[Chọn mã ngạch]";
            this.cbbPg1MaNgach.Properties.ValueMember = "MaNgach";
            this.cbbPg1MaNgach.Size = new System.Drawing.Size(116, 22);
            this.cbbPg1MaNgach.TabIndex = 9;
            this.cbbPg1MaNgach.EditValueChanged += new System.EventHandler(this.cbbPg1MaNgach_EditValueChanged);
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(448, 255);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 19;
            this.btnPg3Xoa.Text = "Xóa";
            this.btnPg3Xoa.Click += new System.EventHandler(this.btnPg3Xoa_Click);
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(256, 255);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 17;
            this.btnPg3Them.Text = "Lưu";
            this.btnPg3Them.Click += new System.EventHandler(this.btnPg3Them_Click);
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(160, 255);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 16;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            this.btnPg3TaoMoi.Click += new System.EventHandler(this.btnPg3TaoMoi_Click);
            // 
            // txtTileVuot
            // 
            this.txtTileVuot.Location = new System.Drawing.Point(430, 90);
            this.txtTileVuot.Name = "txtTileVuot";
            this.txtTileVuot.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTileVuot.Properties.Appearance.Options.UseFont = true;
            this.txtTileVuot.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTileVuot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTileVuot.Properties.DisplayFormat.FormatString = "##0";
            this.txtTileVuot.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTileVuot.Properties.EditFormat.FormatString = "##0";
            this.txtTileVuot.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTileVuot.Properties.Mask.EditMask = "##0";
            this.txtTileVuot.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTileVuot.Properties.NullText = "0";
            this.txtTileVuot.Size = new System.Drawing.Size(49, 22);
            this.txtTileVuot.TabIndex = 12;
            // 
            // labelControl91
            // 
            this.labelControl91.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl91.Location = new System.Drawing.Point(334, 97);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(92, 15);
            this.labelControl91.TabIndex = 94;
            this.labelControl91.Text = "Tỉ lệ vượt khung";
            // 
            // txtPCThamnien
            // 
            this.txtPCThamnien.Location = new System.Drawing.Point(430, 189);
            this.txtPCThamnien.Name = "txtPCThamnien";
            this.txtPCThamnien.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCThamnien.Properties.Appearance.Options.UseFont = true;
            this.txtPCThamnien.Size = new System.Drawing.Size(105, 22);
            this.txtPCThamnien.TabIndex = 14;
            // 
            // labelControl90
            // 
            this.labelControl90.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl90.Location = new System.Drawing.Point(321, 196);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(105, 15);
            this.labelControl90.TabIndex = 22;
            this.labelControl90.Text = "Phụ cấp thâm niên";
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl84.Location = new System.Drawing.Point(390, 31);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(36, 15);
            this.labelControl84.TabIndex = 10;
            this.labelControl84.Text = "Ngạch";
            // 
            // gridQuyetDinh
            // 
            this.gridQuyetDinh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridQuyetDinh.Location = new System.Drawing.Point(0, 292);
            this.gridQuyetDinh.MainView = this.gridViewQuyetDinh;
            this.gridQuyetDinh.Name = "gridQuyetDinh";
            this.gridQuyetDinh.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa,
            this.repositoryItemCheckEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemCheckEdit2});
            this.gridQuyetDinh.Size = new System.Drawing.Size(1199, 408);
            this.gridQuyetDinh.TabIndex = 101;
            this.gridQuyetDinh.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuyetDinh});
            // 
            // gridViewQuyetDinh
            // 
            this.gridViewQuyetDinh.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColLuongID,
            this.gridColMaNV,
            this.gridColLuongSQD,
            this.gridColTenQD,
            this.gridColLoaiQD,
            this.gridColTenLoaiQĐ,
            this.gridColLanThu,
            this.gridColNguoiKy,
            this.gridColTenNguoiKy,
            this.gridColNgayKy,
            this.gridColTuNgay,
            this.gridColDenNgay,
            this.gridColLuongNgach,
            this.gridColLuongBac,
            this.gridColLuongHeSo,
            this.gridColLuongVuot,
            this.gridColPhuCap,
            this.gridColLuongPCCV,
            this.gridColLuongPCTN,
            this.gridColNoiDungQĐ,
            this.gridColHienTaiHuong});
            this.gridViewQuyetDinh.GridControl = this.gridQuyetDinh;
            this.gridViewQuyetDinh.GroupPanelText = "Lịch sử quyết định";
            this.gridViewQuyetDinh.Name = "gridViewQuyetDinh";
            this.gridViewQuyetDinh.OptionsView.ColumnAutoWidth = false;
            this.gridViewQuyetDinh.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewQuyetDinh_RowClick);
            this.gridViewQuyetDinh.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewQuyetDinh_FocusedRowChanged);
            // 
            // gridColLuongID
            // 
            this.gridColLuongID.Caption = "Mã ID";
            this.gridColLuongID.FieldName = "Id";
            this.gridColLuongID.Name = "gridColLuongID";
            this.gridColLuongID.OptionsColumn.AllowEdit = false;
            this.gridColLuongID.OptionsColumn.FixedWidth = true;
            this.gridColLuongID.OptionsColumn.ReadOnly = true;
            // 
            // gridColMaNV
            // 
            this.gridColMaNV.Caption = "Mã NV";
            this.gridColMaNV.FieldName = "MaNV";
            this.gridColMaNV.Name = "gridColMaNV";
            this.gridColMaNV.OptionsColumn.AllowEdit = false;
            this.gridColMaNV.OptionsColumn.FixedWidth = true;
            this.gridColMaNV.OptionsColumn.ReadOnly = true;
            // 
            // gridColLuongSQD
            // 
            this.gridColLuongSQD.Caption = "Số QĐ";
            this.gridColLuongSQD.FieldName = "SoQD";
            this.gridColLuongSQD.Name = "gridColLuongSQD";
            this.gridColLuongSQD.OptionsColumn.AllowEdit = false;
            this.gridColLuongSQD.OptionsColumn.FixedWidth = true;
            this.gridColLuongSQD.OptionsColumn.ReadOnly = true;
            this.gridColLuongSQD.Visible = true;
            this.gridColLuongSQD.VisibleIndex = 0;
            // 
            // gridColTenQD
            // 
            this.gridColTenQD.Caption = "Tên QĐ";
            this.gridColTenQD.Name = "gridColTenQD";
            this.gridColTenQD.OptionsColumn.AllowEdit = false;
            this.gridColTenQD.OptionsColumn.FixedWidth = true;
            this.gridColTenQD.OptionsColumn.ReadOnly = true;
            // 
            // gridColLoaiQD
            // 
            this.gridColLoaiQD.Caption = "Mã loại quyết định";
            this.gridColLoaiQD.FieldName = "MaLoaiQD";
            this.gridColLoaiQD.Name = "gridColLoaiQD";
            this.gridColLoaiQD.OptionsColumn.AllowEdit = false;
            this.gridColLoaiQD.OptionsColumn.FixedWidth = true;
            this.gridColLoaiQD.OptionsColumn.ReadOnly = true;
            this.gridColLoaiQD.Width = 100;
            // 
            // gridColTenLoaiQĐ
            // 
            this.gridColTenLoaiQĐ.Caption = "Tên loại quyết định";
            this.gridColTenLoaiQĐ.FieldName = "TenLoaiQD";
            this.gridColTenLoaiQĐ.Name = "gridColTenLoaiQĐ";
            this.gridColTenLoaiQĐ.OptionsColumn.AllowEdit = false;
            this.gridColTenLoaiQĐ.OptionsColumn.FixedWidth = true;
            this.gridColTenLoaiQĐ.OptionsColumn.ReadOnly = true;
            this.gridColTenLoaiQĐ.Visible = true;
            this.gridColTenLoaiQĐ.VisibleIndex = 1;
            this.gridColTenLoaiQĐ.Width = 120;
            // 
            // gridColLanThu
            // 
            this.gridColLanThu.Caption = "Lần thứ";
            this.gridColLanThu.FieldName = "Lan";
            this.gridColLanThu.Name = "gridColLanThu";
            this.gridColLanThu.OptionsColumn.AllowEdit = false;
            this.gridColLanThu.OptionsColumn.FixedWidth = true;
            this.gridColLanThu.OptionsColumn.ReadOnly = true;
            this.gridColLanThu.Visible = true;
            this.gridColLanThu.VisibleIndex = 2;
            this.gridColLanThu.Width = 50;
            // 
            // gridColNguoiKy
            // 
            this.gridColNguoiKy.Caption = "Người Ký";
            this.gridColNguoiKy.FieldName = "NguoiKy";
            this.gridColNguoiKy.Name = "gridColNguoiKy";
            this.gridColNguoiKy.OptionsColumn.AllowEdit = false;
            this.gridColNguoiKy.OptionsColumn.FixedWidth = true;
            this.gridColNguoiKy.OptionsColumn.ReadOnly = true;
            // 
            // gridColTenNguoiKy
            // 
            this.gridColTenNguoiKy.Caption = "Tên người ký";
            this.gridColTenNguoiKy.FieldName = "HoTenNguoiKy";
            this.gridColTenNguoiKy.Name = "gridColTenNguoiKy";
            this.gridColTenNguoiKy.OptionsColumn.AllowEdit = false;
            this.gridColTenNguoiKy.OptionsColumn.FixedWidth = true;
            this.gridColTenNguoiKy.OptionsColumn.ReadOnly = true;
            this.gridColTenNguoiKy.Visible = true;
            this.gridColTenNguoiKy.VisibleIndex = 3;
            this.gridColTenNguoiKy.Width = 120;
            // 
            // gridColNgayKy
            // 
            this.gridColNgayKy.Caption = "Ngày ký";
            this.gridColNgayKy.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColNgayKy.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgayKy.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgayKy.FieldName = "NgayKy";
            this.gridColNgayKy.Name = "gridColNgayKy";
            this.gridColNgayKy.OptionsColumn.AllowEdit = false;
            this.gridColNgayKy.OptionsColumn.FixedWidth = true;
            this.gridColNgayKy.OptionsColumn.ReadOnly = true;
            this.gridColNgayKy.Visible = true;
            this.gridColNgayKy.VisibleIndex = 4;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColTuNgay
            // 
            this.gridColTuNgay.Caption = "Hiệu lực từ";
            this.gridColTuNgay.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColTuNgay.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColTuNgay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColTuNgay.FieldName = "TuNgay";
            this.gridColTuNgay.Name = "gridColTuNgay";
            this.gridColTuNgay.OptionsColumn.AllowEdit = false;
            this.gridColTuNgay.OptionsColumn.FixedWidth = true;
            this.gridColTuNgay.OptionsColumn.ReadOnly = true;
            this.gridColTuNgay.Visible = true;
            this.gridColTuNgay.VisibleIndex = 5;
            this.gridColTuNgay.Width = 100;
            // 
            // gridColDenNgay
            // 
            this.gridColDenNgay.Caption = "Hiệu lực đến";
            this.gridColDenNgay.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColDenNgay.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColDenNgay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColDenNgay.FieldName = "DenNgay";
            this.gridColDenNgay.Name = "gridColDenNgay";
            this.gridColDenNgay.OptionsColumn.AllowEdit = false;
            this.gridColDenNgay.OptionsColumn.FixedWidth = true;
            this.gridColDenNgay.OptionsColumn.ReadOnly = true;
            this.gridColDenNgay.Visible = true;
            this.gridColDenNgay.VisibleIndex = 6;
            this.gridColDenNgay.Width = 100;
            // 
            // gridColLuongNgach
            // 
            this.gridColLuongNgach.Caption = "Ngạch";
            this.gridColLuongNgach.FieldName = "MaNgach";
            this.gridColLuongNgach.Name = "gridColLuongNgach";
            this.gridColLuongNgach.OptionsColumn.AllowEdit = false;
            this.gridColLuongNgach.OptionsColumn.FixedWidth = true;
            this.gridColLuongNgach.OptionsColumn.ReadOnly = true;
            this.gridColLuongNgach.Visible = true;
            this.gridColLuongNgach.VisibleIndex = 7;
            this.gridColLuongNgach.Width = 60;
            // 
            // gridColLuongBac
            // 
            this.gridColLuongBac.Caption = "Bậc";
            this.gridColLuongBac.FieldName = "Bac";
            this.gridColLuongBac.Name = "gridColLuongBac";
            this.gridColLuongBac.OptionsColumn.AllowEdit = false;
            this.gridColLuongBac.OptionsColumn.FixedWidth = true;
            this.gridColLuongBac.OptionsColumn.ReadOnly = true;
            this.gridColLuongBac.Visible = true;
            this.gridColLuongBac.VisibleIndex = 8;
            this.gridColLuongBac.Width = 30;
            // 
            // gridColLuongHeSo
            // 
            this.gridColLuongHeSo.Caption = "Hệ số";
            this.gridColLuongHeSo.DisplayFormat.FormatString = "##0.#0";
            this.gridColLuongHeSo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColLuongHeSo.FieldName = "HeSo";
            this.gridColLuongHeSo.Name = "gridColLuongHeSo";
            this.gridColLuongHeSo.OptionsColumn.AllowEdit = false;
            this.gridColLuongHeSo.OptionsColumn.FixedWidth = true;
            this.gridColLuongHeSo.OptionsColumn.ReadOnly = true;
            this.gridColLuongHeSo.Visible = true;
            this.gridColLuongHeSo.VisibleIndex = 9;
            this.gridColLuongHeSo.Width = 50;
            // 
            // gridColLuongVuot
            // 
            this.gridColLuongVuot.Caption = "Tỉ lệ vượt khung";
            this.gridColLuongVuot.FieldName = "TyLeVuotKhung";
            this.gridColLuongVuot.Name = "gridColLuongVuot";
            this.gridColLuongVuot.OptionsColumn.AllowEdit = false;
            this.gridColLuongVuot.OptionsColumn.FixedWidth = true;
            this.gridColLuongVuot.OptionsColumn.ReadOnly = true;
            this.gridColLuongVuot.Visible = true;
            this.gridColLuongVuot.VisibleIndex = 10;
            this.gridColLuongVuot.Width = 100;
            // 
            // gridColPhuCap
            // 
            this.gridColPhuCap.Caption = "Hệ số CV";
            this.gridColPhuCap.DisplayFormat.FormatString = "##0.#0";
            this.gridColPhuCap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColPhuCap.FieldName = "HeSoCV";
            this.gridColPhuCap.Name = "gridColPhuCap";
            this.gridColPhuCap.OptionsColumn.AllowEdit = false;
            this.gridColPhuCap.OptionsColumn.FixedWidth = true;
            this.gridColPhuCap.OptionsColumn.ReadOnly = true;
            this.gridColPhuCap.Visible = true;
            this.gridColPhuCap.VisibleIndex = 11;
            // 
            // gridColLuongPCCV
            // 
            this.gridColLuongPCCV.Caption = "Phụ cấp nhà giáo";
            this.gridColLuongPCCV.FieldName = "PhuCapNhaGiao";
            this.gridColLuongPCCV.Name = "gridColLuongPCCV";
            this.gridColLuongPCCV.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCCV.OptionsColumn.FixedWidth = true;
            this.gridColLuongPCCV.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCCV.Visible = true;
            this.gridColLuongPCCV.VisibleIndex = 12;
            this.gridColLuongPCCV.Width = 100;
            // 
            // gridColLuongPCTN
            // 
            this.gridColLuongPCTN.Caption = "Phụ cấp thâm niên";
            this.gridColLuongPCTN.FieldName = "PhuCapThamNien";
            this.gridColLuongPCTN.Name = "gridColLuongPCTN";
            this.gridColLuongPCTN.OptionsColumn.AllowEdit = false;
            this.gridColLuongPCTN.OptionsColumn.FixedWidth = true;
            this.gridColLuongPCTN.OptionsColumn.ReadOnly = true;
            this.gridColLuongPCTN.Visible = true;
            this.gridColLuongPCTN.VisibleIndex = 13;
            this.gridColLuongPCTN.Width = 100;
            // 
            // gridColNoiDungQĐ
            // 
            this.gridColNoiDungQĐ.Caption = "Nội dung QĐ";
            this.gridColNoiDungQĐ.FieldName = "NoiDungQD";
            this.gridColNoiDungQĐ.Name = "gridColNoiDungQĐ";
            this.gridColNoiDungQĐ.OptionsColumn.AllowEdit = false;
            this.gridColNoiDungQĐ.OptionsColumn.FixedWidth = true;
            this.gridColNoiDungQĐ.OptionsColumn.ReadOnly = true;
            this.gridColNoiDungQĐ.Visible = true;
            this.gridColNoiDungQĐ.VisibleIndex = 14;
            this.gridColNoiDungQĐ.Width = 150;
            // 
            // gridColHienTaiHuong
            // 
            this.gridColHienTaiHuong.Caption = "Đang hưởng";
            this.gridColHienTaiHuong.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColHienTaiHuong.FieldName = "HienTaiHuong";
            this.gridColHienTaiHuong.Name = "gridColHienTaiHuong";
            this.gridColHienTaiHuong.OptionsColumn.AllowEdit = false;
            this.gridColHienTaiHuong.OptionsColumn.FixedWidth = true;
            this.gridColHienTaiHuong.OptionsColumn.ReadOnly = true;
            this.gridColHienTaiHuong.Visible = true;
            this.gridColHienTaiHuong.VisibleIndex = 15;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = "1";
            this.repositoryItemCheckEdit1.ValueUnchecked = "0";
            // 
            // ucNVQuyetDinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.gridQuyetDinh);
            this.Controls.Add(this.gcQuyetDinh);
            this.Name = "ucNVQuyetDinh";
            this.Size = new System.Drawing.Size(1200, 725);
            this.Load += new System.EventHandler(this.ucNVQuyetDinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcQuyetDinh)).EndInit();
            this.gcQuyetDinh.ResumeLayout(false);
            this.gcQuyetDinh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSoCV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbHienTaiHuong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNguoiKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1Bac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeSo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLanThu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNoiDung.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHieuLucTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayKy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoaiQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhuCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg1MaNgach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTileVuot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCThamnien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuyetDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuyetDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcQuyetDinh;
        private LabelControl labelControl84;
        private TextEdit txtPCThamnien;
        private LabelControl labelControl90;
        private TextEdit txtTileVuot;
        private LabelControl labelControl91;
        private GridControl gridQuyetDinh;
        private GridView gridViewQuyetDinh;
        private GridColumn gridColLuongID;
        private GridColumn gridColLuongSQD;
        private GridColumn gridColLuongNgach;
        private GridColumn gridColLuongBac;
        private GridColumn gridColLuongHeSo;
        private GridColumn gridColLuongPCCV;
        private GridColumn gridColLuongPCTN;
        private GridColumn gridColLuongVuot;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
        private LookUpEdit cbbPg1MaNgach;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private GridColumn gridColPhuCap;
        private SimpleButton btnPg3Sua;
        private TextEdit txtPhuCap;
        private LabelControl labelControl1;
        private LabelControl labelControl2;
        private RepositoryItemDateEdit repositoryItemDateEdit1;
        private TextEdit txtLanThu;
        private LabelControl labelControl4;
        private TextEdit txtTenQD;
        private LabelControl labelControl3;
        private MemoEdit meNoiDung;
        private LabelControl labelControl117;
        private DateEdit deHieuLucDen;
        private LabelControl labelControl116;
        private DateEdit deHieuLucTu;
        private LabelControl labelControl115;
        private LabelControl labelControl114;
        private DateEdit deNgayKy;
        private LabelControl labelControl113;
        private LookUpEdit lueLoaiQD;
        private LabelControl labelControl112;
        private TextEdit txtSoQD;
        private LabelControl labelControl111;
        private LookUpEdit cbbPg1Bac;
        private TextEdit txtHeSo;
        private LabelControl labelControl86;
        private LabelControl labelControl85;
        private GridColumn gridColMaNV;
        private GridColumn gridColTenQD;
        private GridColumn gridColLoaiQD;
        private GridColumn gridColTenLoaiQĐ;
        private GridColumn gridColLanThu;
        private GridColumn gridColNguoiKy;
        private GridColumn gridColNgayKy;
        private GridColumn gridColTuNgay;
        private GridColumn gridColDenNgay;
        private GridColumn gridColNoiDungQĐ;
        private LookUpEdit lueNguoiKy;
        private LabelControl labelControl5;
        private CheckEdit chbHienTaiHuong;
        private GridColumn gridColTenNguoiKy;
        private GridColumn gridColHienTaiHuong;
        private TextEdit txtHeSoCV;
        private RepositoryItemCheckEdit repositoryItemCheckEdit2;
    }
}
