﻿namespace HerculesHRMT
{
    partial class ucDanhSachUngVien
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.ucUngVien1 = new HerculesHRMT.ucUngVien();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.ucDotTuyen1 = new HerculesHRMT.ucDotTuyen();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.ucNVDotTuyen1 = new HerculesHRMT.ucNVDotTuyen();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1270, 760);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.ucUngVien1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1264, 732);
            this.xtraTabPage1.Text = "Danh sách ứng viên";
            // 
            // ucUngVien1
            // 
            this.ucUngVien1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucUngVien1.Location = new System.Drawing.Point(0, 0);
            this.ucUngVien1.Name = "ucUngVien1";
            this.ucUngVien1.Size = new System.Drawing.Size(1264, 732);
            this.ucUngVien1.TabIndex = 0;
            this.ucUngVien1.Load += new System.EventHandler(this.ucUngVien1_Load);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.ucDotTuyen1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1264, 732);
            this.xtraTabPage2.Text = "Thông tin đợt tuyển dụng";
            // 
            // ucDotTuyen1
            // 
            this.ucDotTuyen1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucDotTuyen1.Location = new System.Drawing.Point(0, 0);
            this.ucDotTuyen1.Name = "ucDotTuyen1";
            this.ucDotTuyen1.Size = new System.Drawing.Size(1264, 732);
            this.ucDotTuyen1.TabIndex = 0;
            this.ucDotTuyen1.Load += new System.EventHandler(this.ucDotTuyen1_Load);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.ucNVDotTuyen1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1264, 732);
            this.xtraTabPage3.Text = "Tra cứu nhân viên dựa theo đợt tuyển dụng";
            // 
            // ucNVDotTuyen1
            // 
            this.ucNVDotTuyen1.Location = new System.Drawing.Point(-3, -3);
            this.ucNVDotTuyen1.Name = "ucNVDotTuyen1";
            this.ucNVDotTuyen1.Size = new System.Drawing.Size(1024, 768);
            this.ucNVDotTuyen1.TabIndex = 0;
            this.ucNVDotTuyen1.Load += new System.EventHandler(this.ucNVDotTuyen1_Load);
            // 
            // ucDanhSachUngVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "ucDanhSachUngVien";
            this.Size = new System.Drawing.Size(1270, 760);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private ucUngVien ucUngVien1;
        private ucDotTuyen ucDotTuyen1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private ucNVDotTuyen ucNVDotTuyen1;


    }
}
