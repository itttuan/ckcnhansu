﻿namespace HerculesHRMT
{
    public static class Constants
    {
        #region PARAMETERS
        public const int CheckedStatus = 1;
        public const int ErrorStatus = 2;
        public const int CurrentStatus = 3;
        public const int NormalStatus = 4;


        public const string EmptyDateTime = "01/01/0001";
        //public const string EmptyDateTimeType1 = "01-01-0001";
        //vdtoan
        public const string EmptyDateTimeType1 = "";
        //public const string EmptyDateTimeType2 = "01/01/0001";
        //vdtoan
        public const string EmptyDateTimeType2 = "";
        public const string DefaultDateTime = "1/1/1980";
        public const string LabelMaBangCap = "Mã bằng cấp";
        public const string LabelMaChungChi = "Mã chứng chỉ";
        public const string LabelLoaiBangCap = "Loại bằng cấp";
        public const string LabelLoaiChungChi = "Loại chứng chỉ";

        public const string AddNewStaffFinish = "Kết thúc";
        public const string AddNewStaffNext = "Kế tiếp";

        public const int LoaiTrinhDoCaoNhat = 0;
        public const int LoaiTrinhDoBinhThuong = 1;
        public const int LoaiTrinhDoNghiepVu = 2;
        public const int LoaiTrinhDoLyLuanChinhTri = 3;
        public const int LoaiTrinhDoNgoaiNgu = 4;
        public const int LoaiTrinhDoTinHoc = 5;

        public const int GiaDoanTruocTuyenDung = 0;
        public const int GiaDoanSauTuyenDung = 1;
        public const int GiaDoanCongTacXaHoi = 2;

        #endregion      

        #region WINDOWS NAME
        public const string FrmThemNvFormat = "{0} - {1}";
        public const string FrmThemNv = "Thêm Nhân Viên";
        #endregion

        #region TAB NAME
        public const string SoLuocLyLich = "Sơ Lược Lý Lịch";
        public const string LichSuBanThan = "Lý Sử Bản Thân";
        public const string TrinhDoChuyenMon = "Trình Độ Đào Tạo, Chuyên Môn Nghiệp Vụ";
        public const string KhenThuongKyLuat = "Khen Thưởng, Kỷ Luật";
        public const string QuanHeGiaDinh = "Quan Hệ Gia Đình, Thân Tộc";
        public const string TuNhanXet = "Tự Nhận Xét";
        #endregion
        
        #region DAN TOC
        public const string DisplayTenDanToc = "TenDanToc";
        public const string ValueTenDanToc = "MaDanToc";
        public const string DefaultTenDanToc = "01";
        #endregion

        #region TON GIAO
        public const string DisplayTonGiao = "TenTonGiao";
        public const string ValueTonGiao = "MaTonGiao";
        public const int DefaultTonGiao = 6;
        #endregion

        #region TINH THANH
        public const string DisplayTinhThanh = "TenTinhThanh";
        public const string ValueTinhThanh = "MaTinhThanh";
        #endregion

        #region QUAN HUYEN
        public const string DisplayQuanHuyen = "TenQuanHuyen";
        public const string ValueQuanHuyen = "MaQuanHuyen";
        #endregion

        #region PHONG KHOA
        public const string DisplayPhongKhoa = "TenPhongKhoa";
        public const string ValuePhongKhoa = "MaPhongKhoa";
        #endregion

        #region BO MON BO PHAN
        public const string DisplayBMonBPhan = "TenBMBP";
        public const string ValueBMonBPhan = "MaBMBP";
        #endregion

        #region NGANH
        public const string DisplayNganh = "TenNganh";
        public const string ValueNganh = "MaNganh";
        #endregion

        #region QUAN HE THAN NHAN
        public const string DisplayQuanHe = "TenQuanHe";
        public const string ValueQuanHe = "MaQuanHe";
        #endregion

        #region CHUC VU
        public const string DisplayChucVu = "TenChucVu";
        public const string ValueChucVu = "MaChucVu";
        public const string DefaultChucVu = "GV"; // default value: GV - Giảng viên
        #endregion

        #region TRANG THAI LAM VIEC
        public const string DisplayTrangThaiLamViec = "TenTrangThai";
        public const string ValueTrangThaiLamViec = "MaTrangThai";
        public const string DefaultTrangThaiLamViec = "1"; // default value: 1 - Đang làm việc

        #endregion

        #region FORMAT
        public const string EmailFormat = "{0}@{1}";
        public const string HangThuongBinhFormat = "{0}/{1}";
        public const string HeaderFormat = "Thông báo bước {0}";
        public const string ContinueStepFormat = "\nTiếp tục sang bước {0}?";
        public const string ImageFilter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
        #endregion

        #region MESSAGES
        public const string ErrTenNhanVienEmpty = "Tên nhân viên không được để trống";
        public const string ErrCmndEmpty = "Số CMND không được để trống";
        public const string ErrNoiCapCmndEmpty = "Nơi cấp CMND không được để trống";
        public const string ErrPhongKhoaEmpty = "Thông tin phòng/khoa không được để trống";
        public const string ErrBMonBPhanEmpty = "Thông tin bộ môn/bộ phận không được để trống";
        public const string ErrDiaChiThuongTruEmpty = "Thông tin địa chỉ thường trú không được để trống";
        public const string ErrDiaChiHienTaiEmpty = "Thông tin chỗ ở hiện tại không được để trống";
        public const string ErrNoiCongTacEmpty = "Nơi công tác không được để trống";
        public const string ErrCongViecEmpty = "Thông tin công việc không được để trống";
        public const string ErrDateTimeInvalid = "Khoảng thời gian không hợp lệ";
        public const string ErrToChucEmpty = "Tổ chức CT-XH không được để trống";
        public const string ErrData = "Lỗi Dữ Liệu";

        public const string ConfirmRemoveData = "Xác nhận xóa dữ liệu";
        public const string Confirm = "Xác nhận";

        public const string Notify = "Xác nhận";

        public const string MsgLuuThanhCong = "Lưu thành công";
        public const string MsgLuuThatBai = "Lưu không thành công";

        public const string ErrLuuSoLuocLyLich = "Hoàn tất Sơ Lược Lý Lịch trước khi chuyển nội dung";

        public const string WarnPreviewReport = "Lưu ý chỉ xem được thông tin trên báo cáo khi đã lưu thông tin";

        public const string ErrNgayTuyenDungEmpty = "Ngày tuyển dụng không được để trống";

        #endregion


    }
}
