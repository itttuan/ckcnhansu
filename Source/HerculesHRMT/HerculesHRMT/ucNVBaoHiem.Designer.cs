﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVBaoHiem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcBaoHiem = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcBHXH = new DevExpress.XtraEditors.GroupControl();
            this.txtTKNH = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtATM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtMSThue = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnPg3BHXHSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3BHXHThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtSoBHXH = new DevExpress.XtraEditors.TextEdit();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.gcBHYT = new DevExpress.XtraEditors.GroupControl();
            this.btnPg3Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg3TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.gridBHYT = new DevExpress.XtraGrid.GridControl();
            this.gridViewBHYT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColBHYTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTSothe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTNoiDK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColBHYTNgaycap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditBHYTXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.txtNoiKham = new DevExpress.XtraEditors.TextEdit();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.deNgayBHYT = new DevExpress.XtraEditors.DateEdit();
            this.txtSoBHYT = new DevExpress.XtraEditors.TextEdit();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcBaoHiem)).BeginInit();
            this.gcBaoHiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcBHXH)).BeginInit();
            this.gcBHXH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKNH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtATM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSThue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBHXH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBHYT)).BeginInit();
            this.gcBHYT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHYT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHYT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditBHYTXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiKham.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBHYT.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBHYT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBHYT.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcBaoHiem
            // 
            this.gcBaoHiem.Controls.Add(this.splitContainerControl1);
            this.gcBaoHiem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBaoHiem.Location = new System.Drawing.Point(0, 0);
            this.gcBaoHiem.Name = "gcBaoHiem";
            this.gcBaoHiem.Size = new System.Drawing.Size(1270, 510);
            this.gcBaoHiem.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(2, 21);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1266, 487);
            this.splitContainerControl1.SplitterPosition = 0;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gcBHXH);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gcBHYT);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1266, 482);
            this.splitContainerControl2.SplitterPosition = 634;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gcBHXH
            // 
            this.gcBHXH.Controls.Add(this.txtTKNH);
            this.gcBHXH.Controls.Add(this.labelControl2);
            this.gcBHXH.Controls.Add(this.txtATM);
            this.gcBHXH.Controls.Add(this.labelControl3);
            this.gcBHXH.Controls.Add(this.txtMSThue);
            this.gcBHXH.Controls.Add(this.labelControl1);
            this.gcBHXH.Controls.Add(this.btnPg3BHXHSua);
            this.gcBHXH.Controls.Add(this.btnPg3BHXHThem);
            this.gcBHXH.Controls.Add(this.txtSoBHXH);
            this.gcBHXH.Controls.Add(this.labelControl129);
            this.gcBHXH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBHXH.Location = new System.Drawing.Point(0, 0);
            this.gcBHXH.Name = "gcBHXH";
            this.gcBHXH.Size = new System.Drawing.Size(634, 482);
            this.gcBHXH.TabIndex = 0;
            this.gcBHXH.Text = "Mã số thuế - Tài khoản ngân hàng - BHXH";
            // 
            // txtTKNH
            // 
            this.txtTKNH.Location = new System.Drawing.Point(181, 136);
            this.txtTKNH.Name = "txtTKNH";
            this.txtTKNH.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKNH.Properties.Appearance.Options.UseFont = true;
            this.txtTKNH.Size = new System.Drawing.Size(189, 22);
            this.txtTKNH.TabIndex = 149;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(81, 139);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(96, 15);
            this.labelControl2.TabIndex = 148;
            this.labelControl2.Text = "Số TK Vietinbank";
            // 
            // txtATM
            // 
            this.txtATM.Location = new System.Drawing.Point(181, 181);
            this.txtATM.Name = "txtATM";
            this.txtATM.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtATM.Properties.Appearance.Options.UseFont = true;
            this.txtATM.Size = new System.Drawing.Size(189, 22);
            this.txtATM.TabIndex = 147;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(51, 184);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(126, 15);
            this.labelControl3.TabIndex = 146;
            this.labelControl3.Text = "Số thẻ ATM Vietinbank";
            // 
            // txtMSThue
            // 
            this.txtMSThue.Location = new System.Drawing.Point(181, 46);
            this.txtMSThue.Name = "txtMSThue";
            this.txtMSThue.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMSThue.Properties.Appearance.Options.UseFont = true;
            this.txtMSThue.Size = new System.Drawing.Size(189, 22);
            this.txtMSThue.TabIndex = 145;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(115, 49);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 15);
            this.labelControl1.TabIndex = 144;
            this.labelControl1.Text = "Mã số thuế";
            // 
            // btnPg3BHXHSua
            // 
            this.btnPg3BHXHSua.Location = new System.Drawing.Point(147, 295);
            this.btnPg3BHXHSua.Name = "btnPg3BHXHSua";
            this.btnPg3BHXHSua.Size = new System.Drawing.Size(75, 23);
            this.btnPg3BHXHSua.TabIndex = 142;
            this.btnPg3BHXHSua.Text = "Hiệu chỉnh";
            this.btnPg3BHXHSua.Click += new System.EventHandler(this.btnPg3BHXHSua_Click);
            // 
            // btnPg3BHXHThem
            // 
            this.btnPg3BHXHThem.Location = new System.Drawing.Point(37, 295);
            this.btnPg3BHXHThem.Name = "btnPg3BHXHThem";
            this.btnPg3BHXHThem.Size = new System.Drawing.Size(75, 23);
            this.btnPg3BHXHThem.TabIndex = 143;
            this.btnPg3BHXHThem.Text = "Lưu";
            this.btnPg3BHXHThem.Click += new System.EventHandler(this.btnPg3BHXHThem_Click);
            // 
            // txtSoBHXH
            // 
            this.txtSoBHXH.Location = new System.Drawing.Point(181, 91);
            this.txtSoBHXH.Name = "txtSoBHXH";
            this.txtSoBHXH.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBHXH.Properties.Appearance.Options.UseFont = true;
            this.txtSoBHXH.Size = new System.Drawing.Size(189, 22);
            this.txtSoBHXH.TabIndex = 95;
            // 
            // labelControl129
            // 
            this.labelControl129.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl129.Location = new System.Drawing.Point(110, 94);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(67, 15);
            this.labelControl129.TabIndex = 94;
            this.labelControl129.Text = "Số sổ BHXH";
            // 
            // gcBHYT
            // 
            this.gcBHYT.Controls.Add(this.btnPg3Sua);
            this.gcBHYT.Controls.Add(this.btnPg3Xoa);
            this.gcBHYT.Controls.Add(this.btnPg3Them);
            this.gcBHYT.Controls.Add(this.btnPg3TaoMoi);
            this.gcBHYT.Controls.Add(this.gridBHYT);
            this.gcBHYT.Controls.Add(this.txtNoiKham);
            this.gcBHYT.Controls.Add(this.labelControl133);
            this.gcBHYT.Controls.Add(this.labelControl131);
            this.gcBHYT.Controls.Add(this.deNgayBHYT);
            this.gcBHYT.Controls.Add(this.txtSoBHYT);
            this.gcBHYT.Controls.Add(this.labelControl132);
            this.gcBHYT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBHYT.Location = new System.Drawing.Point(0, 0);
            this.gcBHYT.Name = "gcBHYT";
            this.gcBHYT.Size = new System.Drawing.Size(627, 482);
            this.gcBHYT.TabIndex = 0;
            this.gcBHYT.Text = "Bảo hiểm Y tế";
            // 
            // btnPg3Sua
            // 
            this.btnPg3Sua.Location = new System.Drawing.Point(229, 151);
            this.btnPg3Sua.Name = "btnPg3Sua";
            this.btnPg3Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Sua.TabIndex = 146;
            this.btnPg3Sua.Text = "Hiệu chỉnh";
            this.btnPg3Sua.Click += new System.EventHandler(this.btnPg3Sua_Click);
            // 
            // btnPg3Xoa
            // 
            this.btnPg3Xoa.Location = new System.Drawing.Point(325, 151);
            this.btnPg3Xoa.Name = "btnPg3Xoa";
            this.btnPg3Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Xoa.TabIndex = 143;
            this.btnPg3Xoa.Text = "Xóa";
            this.btnPg3Xoa.Click += new System.EventHandler(this.btnPg3Xoa_Click);
            // 
            // btnPg3Them
            // 
            this.btnPg3Them.Location = new System.Drawing.Point(133, 151);
            this.btnPg3Them.Name = "btnPg3Them";
            this.btnPg3Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg3Them.TabIndex = 144;
            this.btnPg3Them.Text = "Lưu";
            this.btnPg3Them.Click += new System.EventHandler(this.btnPg3Them_Click);
            // 
            // btnPg3TaoMoi
            // 
            this.btnPg3TaoMoi.Location = new System.Drawing.Point(37, 151);
            this.btnPg3TaoMoi.Name = "btnPg3TaoMoi";
            this.btnPg3TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg3TaoMoi.TabIndex = 145;
            this.btnPg3TaoMoi.Text = "Tạo mới";
            this.btnPg3TaoMoi.Click += new System.EventHandler(this.btnPg3TaoMoi_Click);
            // 
            // gridBHYT
            // 
            this.gridBHYT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBHYT.Location = new System.Drawing.Point(24, 180);
            this.gridBHYT.MainView = this.gridViewBHYT;
            this.gridBHYT.Name = "gridBHYT";
            this.gridBHYT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditBHYTXoa,
            this.repositoryItemDateEdit1});
            this.gridBHYT.Size = new System.Drawing.Size(579, 297);
            this.gridBHYT.TabIndex = 142;
            this.gridBHYT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBHYT});
            // 
            // gridViewBHYT
            // 
            this.gridViewBHYT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColBHYTID,
            this.gridColBHYTSothe,
            this.gridColBHYTNoiDK,
            this.gridColBHYTNgaycap});
            this.gridViewBHYT.GridControl = this.gridBHYT;
            this.gridViewBHYT.GroupPanelText = " ";
            this.gridViewBHYT.Name = "gridViewBHYT";
            this.gridViewBHYT.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewBHYT_RowClick);
            this.gridViewBHYT.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBHYT_FocusedRowChanged);
            // 
            // gridColBHYTID
            // 
            this.gridColBHYTID.Caption = "Mã";
            this.gridColBHYTID.FieldName = "ID";
            this.gridColBHYTID.Name = "gridColBHYTID";
            // 
            // gridColBHYTSothe
            // 
            this.gridColBHYTSothe.Caption = "Số thẻ BHYT";
            this.gridColBHYTSothe.FieldName = "SoSo";
            this.gridColBHYTSothe.Name = "gridColBHYTSothe";
            this.gridColBHYTSothe.OptionsColumn.AllowEdit = false;
            this.gridColBHYTSothe.OptionsColumn.ReadOnly = true;
            this.gridColBHYTSothe.Visible = true;
            this.gridColBHYTSothe.VisibleIndex = 0;
            // 
            // gridColBHYTNoiDK
            // 
            this.gridColBHYTNoiDK.Caption = "Nơi ĐK KCB";
            this.gridColBHYTNoiDK.FieldName = "NoiDKKCB";
            this.gridColBHYTNoiDK.Name = "gridColBHYTNoiDK";
            this.gridColBHYTNoiDK.OptionsColumn.AllowEdit = false;
            this.gridColBHYTNoiDK.OptionsColumn.ReadOnly = true;
            this.gridColBHYTNoiDK.Visible = true;
            this.gridColBHYTNoiDK.VisibleIndex = 1;
            // 
            // gridColBHYTNgaycap
            // 
            this.gridColBHYTNgaycap.Caption = "Ngày cấp";
            this.gridColBHYTNgaycap.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColBHYTNgaycap.FieldName = "NgayCap";
            this.gridColBHYTNgaycap.Name = "gridColBHYTNgaycap";
            this.gridColBHYTNgaycap.OptionsColumn.AllowEdit = false;
            this.gridColBHYTNgaycap.OptionsColumn.ReadOnly = true;
            this.gridColBHYTNgaycap.Visible = true;
            this.gridColBHYTNgaycap.VisibleIndex = 2;
            // 
            // repositoryItemHyperLinkEditBHYTXoa
            // 
            this.repositoryItemHyperLinkEditBHYTXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditBHYTXoa.Name = "repositoryItemHyperLinkEditBHYTXoa";
            this.repositoryItemHyperLinkEditBHYTXoa.NullText = "Xóa";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // txtNoiKham
            // 
            this.txtNoiKham.Location = new System.Drawing.Point(105, 107);
            this.txtNoiKham.Name = "txtNoiKham";
            this.txtNoiKham.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiKham.Properties.Appearance.Options.UseFont = true;
            this.txtNoiKham.Size = new System.Drawing.Size(200, 22);
            this.txtNoiKham.TabIndex = 105;
            // 
            // labelControl133
            // 
            this.labelControl133.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl133.Location = new System.Drawing.Point(37, 110);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(64, 15);
            this.labelControl133.TabIndex = 104;
            this.labelControl133.Text = "Nơi ĐKKCB";
            // 
            // labelControl131
            // 
            this.labelControl131.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl131.Location = new System.Drawing.Point(28, 75);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(73, 15);
            this.labelControl131.TabIndex = 103;
            this.labelControl131.Text = "Ngày cấp thẻ";
            // 
            // deNgayBHYT
            // 
            this.deNgayBHYT.EditValue = null;
            this.deNgayBHYT.Location = new System.Drawing.Point(105, 72);
            this.deNgayBHYT.Name = "deNgayBHYT";
            this.deNgayBHYT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deNgayBHYT.Properties.Appearance.Options.UseFont = true;
            this.deNgayBHYT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayBHYT.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayBHYT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayBHYT.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayBHYT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayBHYT.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deNgayBHYT.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayBHYT.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayBHYT.Size = new System.Drawing.Size(200, 20);
            this.deNgayBHYT.TabIndex = 102;
            // 
            // txtSoBHYT
            // 
            this.txtSoBHYT.Location = new System.Drawing.Point(105, 37);
            this.txtSoBHYT.Name = "txtSoBHYT";
            this.txtSoBHYT.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBHYT.Properties.Appearance.Options.UseFont = true;
            this.txtSoBHYT.Size = new System.Drawing.Size(200, 22);
            this.txtSoBHYT.TabIndex = 101;
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl132.Location = new System.Drawing.Point(24, 40);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(77, 15);
            this.labelControl132.TabIndex = 100;
            this.labelControl132.Text = "Số thẻ BHYT *";
            // 
            // ucNVBaoHiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcBaoHiem);
            this.Name = "ucNVBaoHiem";
            this.Size = new System.Drawing.Size(1270, 510);
            ((System.ComponentModel.ISupportInitialize)(this.gcBaoHiem)).EndInit();
            this.gcBaoHiem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcBHXH)).EndInit();
            this.gcBHXH.ResumeLayout(false);
            this.gcBHXH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTKNH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtATM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSThue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBHXH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBHYT)).EndInit();
            this.gcBHYT.ResumeLayout(false);
            this.gcBHYT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBHYT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBHYT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditBHYTXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiKham.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBHYT.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayBHYT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoBHYT.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupControl gcBaoHiem;
        private SplitContainerControl splitContainerControl1;
        private SplitContainerControl splitContainerControl2;
        private GroupControl gcBHXH;
        private GroupControl gcBHYT;
        private TextEdit txtSoBHXH;
        private LabelControl labelControl129;
        private TextEdit txtNoiKham;
        private LabelControl labelControl133;
        private LabelControl labelControl131;
        private DateEdit deNgayBHYT;
        private TextEdit txtSoBHYT;
        private LabelControl labelControl132;
        private GridControl gridBHYT;
        private GridView gridViewBHYT;
        private GridColumn gridColBHYTID;
        private GridColumn gridColBHYTSothe;
        private GridColumn gridColBHYTNoiDK;
        private GridColumn gridColBHYTNgaycap;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditBHYTXoa;
        private SimpleButton btnPg3BHXHSua;
        private SimpleButton btnPg3BHXHThem;
        private TextEdit txtTKNH;
        private LabelControl labelControl2;
        private TextEdit txtATM;
        private LabelControl labelControl3;
        private TextEdit txtMSThue;
        private LabelControl labelControl1;
        private SimpleButton btnPg3Sua;
        private SimpleButton btnPg3Xoa;
        private SimpleButton btnPg3Them;
        private SimpleButton btnPg3TaoMoi;
        private RepositoryItemDateEdit repositoryItemDateEdit1;
    }
}
