﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Linq;
using System.Xml;

//vdtoan
namespace HerculesHRMT
{
    public partial class ucUngVien : DevExpress.XtraEditors.XtraUserControl
    {
        UngVienCTL uvCTL = new UngVienCTL();
        UngVienDTO uvSelect = null;
        List<UngVienDTO> listuv = new List<UngVienDTO>();

        NhanVienCTL nvCTL = new NhanVienCTL();
        NhanVienDTO nvnew = new NhanVienDTO();

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;
     //   static int iBienTangTuDong = 0;
        public ucUngVien()
        {
            InitializeComponent();
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        public void LoadDuLieuTatCaLenUC()
        {
            LoadData();
            SetStatus(VIEW);

            //teGio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            //teGio.Properties.Mask.EditMask = "HH:mm";
            //teGio.MaskBox.Mask.UseMaskAsDisplayFormat = true;

            //cbbeThang.Properties.Items.Add("01");
            //cbbeThang.Properties.Items.Add("02");
            //cbbeThang.Properties.Items.Add("03");
            //cbbeThang.Properties.Items.Add("04");
            //cbbeThang.Properties.Items.Add("05");
            //cbbeThang.Properties.Items.Add("06");
            //cbbeThang.Properties.Items.Add("07");
            //cbbeThang.Properties.Items.Add("08");
            //cbbeThang.Properties.Items.Add("09");
            //cbbeThang.Properties.Items.Add("10");
            //cbbeThang.Properties.Items.Add("11");
            //cbbeThang.Properties.Items.Add("12");
            //int iNamhientai = DateTime.Now.Year;
            //for (int i = 0; i < 10; i++)
            //{
            //    cbbeNam.Properties.Items.Add(iNamhientai - 5 + i);
            //}

            //load lueMaDot
            List<DotTuyenDTO> listdt = new List<DotTuyenDTO>();
            //  listdt.Insert(0, new DotTuyenDTO());
            DotTuyenCTL dtCTL = new DotTuyenCTL();
            listdt = dtCTL.GetAll();
            lueMaDot.Properties.DataSource = listdt;

            ////load lueMaNganh
            //List<NganhDTO> listnganh = new List<NganhDTO>();
            //NganhCTL nganhCTL = new NganhCTL();
            //listnganh = nganhCTL.GetAll();
            //lueNganh.Properties.DataSource = listnganh;

            //load lueTruongDaoTao
            List<TruongDTO> listtruong = new List<TruongDTO>();
            TruongCTL truongCTL = new TruongCTL();
            listtruong = truongCTL.GetAll();
            lueTruongDaoTao.Properties.DataSource = listtruong;

            //load lueHinhThucDaoTao
            List<HinhThucDaoTaoDTO> listhtdt = new List<HinhThucDaoTaoDTO>();
            HinhThucDaoTaoCTL htdtCTL = new HinhThucDaoTaoCTL();
            listhtdt = htdtCTL.LayDanhSachHinhThucDaoTao();
            lueHinhThucDaoTao.Properties.DataSource = listhtdt;

            //load luePhongKhoa
            List<PhongKhoaDTO> listpk = new List<PhongKhoaDTO>();
            PhongKhoaCTL pkCTL = new PhongKhoaCTL();
            listpk = pkCTL.GetAll();
            luePhongKhoa.Properties.DataSource = listpk;

            //load lueBMBP
            List<BoMonBoPhanDTO> listbmbp = new List<BoMonBoPhanDTO>();
            BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
            listbmbp = bmbpCTL.GetAll();
            lueBMBP.Properties.DataSource = listbmbp;

            //load lueNganhDuTuyen
            //List<ChuyenNganhDto> listcn = new List<ChuyenNganhDto>();
            //ChuyenNganhCTL cnCTL = new ChuyenNganhCTL();
            //listcn = cnCTL.GetAll();
            //lueNganhDuTuyen.Properties.DataSource = listcn;
        }
        public void LoadData()
         {
            listuv = uvCTL.GetAll();
            gcUngVien.DataSource = listuv;
            gvUngVien.BestFitColumns();

            btnLuu.Enabled = false;

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetStatus(NEW);
            deNgaySinh.EditValue = Constants.EmptyDateTimeType2;
            //lueMaDot.Focus();

            //iBienTangTuDong = iBienTangTuDong + 1;

            //if (iBienTangTuDong < 10)
            //    txtMaUV.Text = "UV" + "000" + iBienTangTuDong.ToString();
            //else if (iBienTangTuDong >= 10 && iBienTangTuDong < 100)
            //    txtMaUV.Text = "UV" + "00" + iBienTangTuDong.ToString();
            //else if (iBienTangTuDong >= 100 && iBienTangTuDong < 1000)
            //    txtMaUV.Text = "UV" + "0" + iBienTangTuDong.ToString();
            //else
            //    txtMaUV.Text = "UV" + iBienTangTuDong.ToString();
           
            string mauv = "";
            string sLastIndexNganh = uvCTL.GetMaxMaUV();
            if (sLastIndexNganh.CompareTo(string.Empty) != 0)
            {
                //int nLastIndexTG = int.Parse(sLastIndexNganh);
                //mauv += (nLastIndexTG + 1).ToString().PadLeft(4, '0');
                int nLastIndexTG = int.Parse(sLastIndexNganh.Substring(2,4));
                mauv += (nLastIndexTG + 1).ToString().PadLeft(4, '0');
            }
            else
                mauv = "0001";
            txtMaUV.Text = "UV"+ mauv;
            //teGio.EditValue = "00:00";
            //deNgayBatDau.EditValue = Constants.EmptyDateTimeType2;
            //deNgayKetThuc.EditValue = Constants.EmptyDateTimeType2;
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    //txtMaDot.Properties.ReadOnly = false;
                    lueMaDot.Properties.ReadOnly = false;
                    txtHoLot.Properties.ReadOnly = false;
                    txtTen.Properties.ReadOnly = false;
                    deNgaySinh.Properties.ReadOnly = false;
                    rgGioiTinh.Properties.ReadOnly = false;
                    txtCMND.Properties.ReadOnly = false;
                    txtQueQuan.Properties.ReadOnly = false;
                    txtDienThoai.Properties.ReadOnly = false;
                    txtEmail.Properties.ReadOnly = false;
                    //cbbeEmailTail.Properties.ReadOnly = false;
                    //lueNganh.Properties.ReadOnly = false;
                    txtNganhDaoTao.Properties.ReadOnly = false;
                    lueTruongDaoTao.Properties.ReadOnly = false;
                    txtTruongKhac.Properties.ReadOnly = false;
                    lueHinhThucDaoTao.Properties.ReadOnly = false;
                    txtXepHang.Properties.ReadOnly = false;
                    txtDiemTotNghiep.Properties.ReadOnly = false;
                    txtTrinhDoSauDaiHoc.Properties.ReadOnly = false;
                    txtChungChi.Properties.ReadOnly = false;
                    txtChuyenMon.Properties.ReadOnly = false;
                    txtKyNangSuPham.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    luePhongKhoa.Properties.ReadOnly = false;
                    lueBMBP.Properties.ReadOnly = false;
                    //lueNganhDuTuyen.Properties.ReadOnly = false;
                    txtNganhDuTuyen.Properties.ReadOnly = false;
                    //lueMaDot.Text = string.Empty;
                    lueMaDot.EditValue = null;
                    txtHoLot.Text = string.Empty;
                    txtTen.Text = string.Empty;
                    rgGioiTinh.Text = string.Empty;
                    txtCMND.Text = string.Empty;
                    txtQueQuan.Text = string.Empty;
                    txtDienThoai.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    //cbbeEmailTail.Text = string.Empty;
                    //lueNganh.Text = string.Empty;
                    //lueTruongDaoTao.Text = string.Empty;
                    //lueNganh.EditValue = null;
                    txtNganhDaoTao.Text = string.Empty;
                    lueTruongDaoTao.EditValue = null;
                    txtTruongKhac.Text = string.Empty;
                    //lueHinhThucDaoTao.Text = string.Empty;
                    lueHinhThucDaoTao.EditValue = null;
                    txtXepHang.Text = string.Empty;
                    txtDiemTotNghiep.Text = string.Empty;
                    txtTrinhDoSauDaiHoc.Text = string.Empty;
                    txtChungChi.Text = string.Empty;
                    txtChuyenMon.Text = string.Empty;
                    txtKyNangSuPham.Text = string.Empty;
                    meGhiChu.Text = string.Empty;
                    //luePhongKhoa.Text = string.Empty;
                    //lueBMBP.Text = string.Empty;
                    //lueNganhDuTuyen.Text = string.Empty;
                    luePhongKhoa.EditValue = null;
                    lueBMBP.EditValue = null;
                    //lueNganhDuTuyen.EditValue = null;
                    txtNganhDuTuyen.Text = string.Empty;
                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;

                    // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case EDIT:
                    //txtMaDot.Properties.ReadOnly = false;
                    lueMaDot.Properties.ReadOnly = false;
                    txtHoLot.Properties.ReadOnly = false;
                    txtTen.Properties.ReadOnly = false;
                    deNgaySinh.Properties.ReadOnly = false;
                    rgGioiTinh.Properties.ReadOnly = false;
                    txtCMND.Properties.ReadOnly = false;
                    txtQueQuan.Properties.ReadOnly = false;
                    txtDienThoai.Properties.ReadOnly = false;
                    txtEmail.Properties.ReadOnly = false;
                    //cbbeEmailTail.Properties.ReadOnly = false;
                    //lueNganh.Properties.ReadOnly = false;
                    txtNganhDaoTao.Properties.ReadOnly = false;
                    lueTruongDaoTao.Properties.ReadOnly = false;
                    txtTruongKhac.Properties.ReadOnly = false;
                    lueHinhThucDaoTao.Properties.ReadOnly = false;
                    txtXepHang.Properties.ReadOnly = false;
                    txtDiemTotNghiep.Properties.ReadOnly = false;
                    txtTrinhDoSauDaiHoc.Properties.ReadOnly = false;
                    txtChungChi.Properties.ReadOnly = false;
                    txtChuyenMon.Properties.ReadOnly = false;
                    txtKyNangSuPham.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    luePhongKhoa.Properties.ReadOnly = false;
                    lueBMBP.Properties.ReadOnly = false;
                    //lueNganhDuTuyen.Properties.ReadOnly = false;
                    txtNganhDuTuyen.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(uvSelect);
                    _selectedStatus = EDIT;
                    
                // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case VIEW:

                    //txtMaDot.Properties.ReadOnly = true;
                    lueMaDot.Properties.ReadOnly = true;
                    txtHoLot.Properties.ReadOnly = true;
                    txtTen.Properties.ReadOnly = true;
                    deNgaySinh.Properties.ReadOnly = true;
                    rgGioiTinh.Properties.ReadOnly = true;
                    txtCMND.Properties.ReadOnly = true;
                    txtQueQuan.Properties.ReadOnly = true;
                    txtDienThoai.Properties.ReadOnly = true;
                    txtEmail.Properties.ReadOnly = true;
                    //cbbeEmailTail.Properties.ReadOnly = true;
                    //lueNganh.Properties.ReadOnly = true;
                    txtNganhDaoTao.Properties.ReadOnly = true;
                    lueTruongDaoTao.Properties.ReadOnly = true;
                    txtTruongKhac.Properties.ReadOnly = true;
                    lueHinhThucDaoTao.Properties.ReadOnly = true;
                    txtXepHang.Properties.ReadOnly = true;
                    txtDiemTotNghiep.Properties.ReadOnly = true;
                    txtTrinhDoSauDaiHoc.Properties.ReadOnly = true;
                    txtChungChi.Properties.ReadOnly = true;
                    txtChuyenMon.Properties.ReadOnly = true;
                    txtKyNangSuPham.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;
                    luePhongKhoa.Properties.ReadOnly = true;
                    lueBMBP.Properties.ReadOnly = true;
                    //lueNganhDuTuyen.Properties.ReadOnly = true;
                    txtNganhDuTuyen.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(uvSelect);
                    _selectedStatus = VIEW;
                  
                // add new
                    btnThem.Enabled = true;
                    btnCapNhat.Enabled = true;
                    break;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInputData())
            {
             //   XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm \n Ngày", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Mã đợt \n Họ lót\n Tên", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin các mục có dấu * \n Mã đợt \n Họ lót \n Tên \n Trường đào tạo \n Hình thức đào tạo \n Phòng/Khoa \n B.Môn/B.Phận", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            UngVienDTO uvNew = GetData(new UngVienDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = uvCTL.Save(uvNew);
            else
                isSucess = uvCTL.Update(uvNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listuv.IndexOf(listuv.FirstOrDefault(p => p.MaUV == uvNew.MaUV));
                gvUngVien.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

  

        private void ucUngVien_Load(object sender, EventArgs e)
        {
            
        }


        private UngVienDTO GetData(UngVienDTO uvDTO)
        {
           
            if (_selectedStatus == NEW)
            {
                

                uvDTO.MaUV = txtMaUV.Text;
                uvDTO.MaDot = lueMaDot.EditValue.ToString();
                uvDTO.Ho= txtHoLot.Text;
                uvDTO.Ten= txtTen.Text;
                //uvDTO.NgaySinh= deNgaySinh.Text;


                if (deNgaySinh.EditValue != null && deNgaySinh.Text != string.Empty)
                {
                    DateTime NgaySinh = DateTime.Parse(deNgaySinh.EditValue.ToString());
                    uvDTO.NgaySinh = NgaySinh.ToString("yyyy-MM-dd");
                }
                else
                    uvDTO.NgaySinh = string.Empty;

                //uvDTO.GioiTinh= rgGioiTinh.Text;
                //uvDTO.GioiTinh = Convert.ToBoolean(rgGioiTinh.EditValue);
                uvDTO.GioiTinh = Convert.ToBoolean(rgGioiTinh.EditValue);
                uvDTO.CMND= txtCMND.Text;
                uvDTO.DienThoai= txtDienThoai.Text;
                //uvDTO.Email= txtEmail.Text +'@' + cbbeEmailTail.Text;
                uvDTO.Email = txtEmail.Text;
                uvDTO.QueQuan= txtQueQuan.Text;
                //uvDTO.NganhHoc = lueNganh.EditValue.ToString();
                uvDTO.NganhHoc = txtNganhDaoTao.Text;
                uvDTO.MaTruong= lueTruongDaoTao.EditValue.ToString();
                uvDTO.TruongKhac= txtTruongKhac.Text;
                
                uvDTO.HinhThucDaoTao= int.Parse(lueHinhThucDaoTao.EditValue.ToString());
                uvDTO.XepHang= txtXepHang.Text;
                uvDTO.DiemTN= txtDiemTotNghiep.Text;
                uvDTO.SauDaiHoc= txtTrinhDoSauDaiHoc.Text;
                uvDTO.MaPK= luePhongKhoa.EditValue.ToString();
                uvDTO.MaBMBP = lueBMBP.EditValue.ToString();
                //uvDTO.NganhDuTuyen = lueNganhDuTuyen.EditValue.ToString();
                uvDTO.NganhDuTuyen = txtNganhDuTuyen.Text;
                uvDTO.ChungChi= txtChungChi.Text;
                uvDTO.GhiChu= meGhiChu.Text;
                uvDTO.YeuCauChuyenMon= txtChuyenMon.Text;
                uvDTO.KyNangSuPham= txtKyNangSuPham.Text;


                //DateTime Ngay = DateTime.Parse(deNgay.EditValue.ToString());
                //dtDTO.NgayGio = Ngay.ToString("yyyy-MM-dd") + " " + teGio.Text;
              
                //dtDTO.Phong = txtPhong.Text;
                //dtDTO.GhiChu = meGhiChu.Text;

                //DateTime NgayBatDau = DateTime.Parse(deNgayBatDau.EditValue.ToString());
                //dtDTO.NgayBatDau = NgayBatDau.ToString("yyyy-MM-dd");
                //DateTime NgayKetThuc = DateTime.Parse(deNgayKetThuc.EditValue.ToString());
                //dtDTO.NgayKetThuc = NgayKetThuc.ToString("yyyy-MM-dd");

                //dtDTO.NgayBatDau = deNgayBatDau;
                //dtDTO.NgayKetThuc = deNgayKetThuc;


                //dtDTO.NgayHieuLuc = DateTime.Now;
                //dtDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                //dtDTO.MaDot = dtSelect.MaDot;
                //dtDTO.NgayHieuLuc = dtSelect.NgayHieuLuc;
                //dtDTO.TinhTrang = dtSelect.TinhTrang;

                //dtDTO.Thang = cbbeThang.Text;
                //dtDTO.Nam = cbbeNam.Text;

                //DateTime Ngay = DateTime.Parse(deNgay.EditValue.ToString());
                //dtDTO.NgayGio = Ngay.ToString("yyyy-MM-dd") + " " + teGio.Text;

                //dtDTO.Phong = txtPhong.Text;
                //dtDTO.GhiChu = meGhiChu.Text;
                ////dtDTO.NgayBatDau = deNgayBatDau;
                ////dtDTO.NgayKetThuc = deNgayKetThuc;
                //DateTime NgayBatDau = DateTime.Parse(deNgayBatDau.EditValue.ToString());
                //dtDTO.NgayBatDau = NgayBatDau.ToString("yyyy-MM-dd");
                //DateTime NgayKetThuc = DateTime.Parse(deNgayKetThuc.EditValue.ToString());
                //dtDTO.NgayKetThuc = NgayKetThuc.ToString("yyyy-MM-dd");

                //uvDTO.MaUV = txtMaUngVien.Text;
                uvDTO.MaUV = uvSelect.MaUV;
                uvDTO.MaDot = lueMaDot.EditValue.ToString();
                uvDTO.Ho = txtHoLot.Text;
                uvDTO.Ten = txtTen.Text;
                //uvDTO.NgaySinh= deNgaySinh.Text;
                DateTime NgaySinh = DateTime.Parse(deNgaySinh.EditValue.ToString());
                uvDTO.NgaySinh = NgaySinh.ToString("yyyy-MM-dd");
                uvDTO.GioiTinh = Convert.ToBoolean(rgGioiTinh.EditValue);
                uvDTO.CMND = txtCMND.Text;
                uvDTO.DienThoai = txtDienThoai.Text;
                //uvDTO.Email = txtEmail.Text + '@' + cbbeEmailTail.Text;
                uvDTO.Email = txtEmail.Text;
                uvDTO.QueQuan = txtQueQuan.Text;
                //uvDTO.NganhHoc = lueNganh.EditValue.ToString();
                uvDTO.NganhHoc = txtNganhDaoTao.Text;
                uvDTO.MaTruong = lueTruongDaoTao.EditValue.ToString();
                uvDTO.TruongKhac = txtTruongKhac.Text;
                uvDTO.HinhThucDaoTao = int.Parse(lueHinhThucDaoTao.EditValue.ToString());
                uvDTO.XepHang = txtXepHang.Text;
                uvDTO.DiemTN = txtDiemTotNghiep.Text;
                uvDTO.SauDaiHoc = txtTrinhDoSauDaiHoc.Text;
                uvDTO.MaPK = luePhongKhoa.EditValue.ToString();
                uvDTO.MaBMBP = lueBMBP.EditValue.ToString();
                //uvDTO.NganhDuTuyen = lueNganhDuTuyen.EditValue.ToString();
                uvDTO.NganhDuTuyen = txtNganhDuTuyen.Text;
                uvDTO.ChungChi = txtChungChi.Text;
                uvDTO.GhiChu = meGhiChu.Text;
                uvDTO.YeuCauChuyenMon = txtChuyenMon.Text;
                uvDTO.KyNangSuPham = txtKyNangSuPham.Text;
            }

           

            //int result = 0;
            //if (int.TryParse(txtPhong.Text, out result))
            //    pkDTO.ThuTuBaoCao = result;
            //else
            //    pkDTO.ThuTuBaoCao = null;

            //pkDTO.TenVietTat = txtTenVT.Text;
            //pkDTO.GhiChu = meGhiChu.Text;

            return uvDTO;
        }

        //private void cbbeThang_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    txtMaDot.Text = cbbeNam.Text.Trim() + cbbeThang.Text.Trim();
        //}

        //private void cbbeNam_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    txtMaDot.Text = cbbeNam.Text.Trim() + cbbeThang.Text.Trim();
        //}

        private bool CheckInputData()
        {
           //if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty || deNgay.Text === string.Empty)
            //if (lueMaDot.Text == string.Empty || txtHoLot.Text == string.Empty || txtTen.Text == string.Empty)
            if (lueMaDot.Text == string.Empty || txtHoLot.Text == string.Empty || txtTen.Text == string.Empty || lueTruongDaoTao.Text == string.Empty || lueHinhThucDaoTao.Text == string.Empty || luePhongKhoa.Text == string.Empty || lueBMBP.Text == string.Empty)
                return false;

            return true;
        }

        private void gcUngVien_Click(object sender, EventArgs e)
        {

        }

        private void BindingData(UngVienDTO uvDTO)
        {
            if (uvDTO != null)
            {


                //txtMaDot.Text = dtDTO.MaDot;
                //cbbeThang.Text = dtDTO.Thang;
                //cbbeNam.Text = dtDTO.Nam;
                //deNgay.Text = dtDTO.NgayGio.Substring(0,10);
                //teGio.EditValue = dtDTO.NgayGio.Substring(11, 5);
                //txtPhong.Text = dtDTO.Phong;
                //meGhiChu.Text = dtDTO.GhiChu;
                //deNgayBatDau.Text = dtDTO.NgayBatDau;
                //deNgayKetThuc.Text = dtDTO.NgayKetThuc;

                txtMaUV.Text = uvDTO.MaUV;
                lueMaDot.EditValue = uvDTO.MaDot;
                txtHoLot.Text = uvDTO.Ho;
                txtTen.Text = uvDTO.Ten;
                //uvDTO.NgaySinh= deNgaySinh.Text;
                //DateTime NgaySinh = DateTime.Parse(deNgaySinh.EditValue.ToString());
                //uvDTO.NgaySinh = NgaySinh.ToString("yyyy-MM-dd");
                deNgaySinh.Text = uvDTO.NgaySinh;
                //rgGioiTinh.Text = uvDTO.GioiTinh;
                rgGioiTinh.EditValue = Convert.ToInt32(uvDTO.GioiTinh);
                txtCMND.Text = uvDTO.CMND;
                txtDienThoai.Text = uvDTO.DienThoai;
                //uvDTO.Email = txtEmail.Text + '@' + cbbeEmailTail.Text;
                txtEmail.Text = uvDTO.Email;
                txtQueQuan.Text = uvDTO.QueQuan;
                //lueNganh.EditValue = uvDTO.NganhHoc;
                txtNganhDaoTao.Text = uvDTO.NganhHoc;
                lueTruongDaoTao.EditValue = uvDTO.MaTruong;
                txtTruongKhac.Text = uvDTO.TruongKhac;
                lueHinhThucDaoTao.EditValue = uvDTO.HinhThucDaoTao;
                txtXepHang.Text = uvDTO.XepHang;
                txtDiemTotNghiep.Text = uvDTO.DiemTN;
                 txtTrinhDoSauDaiHoc.Text = uvDTO.SauDaiHoc;
                 luePhongKhoa.EditValue = uvDTO.MaPK;
                 lueBMBP.EditValue = uvDTO.MaBMBP;
                //lueNganhDuTuyen.Text = uvDTO.NganhDuTuyen;
                txtNganhDuTuyen.Text = uvDTO.NganhDuTuyen;
                txtChungChi.Text = uvDTO.ChungChi;
                meGhiChu.Text = uvDTO.GhiChu;
                txtChuyenMon.Text = uvDTO.YeuCauChuyenMon;
                txtKyNangSuPham.Text = uvDTO.KyNangSuPham;

            }
        }

        private void gcUngVien_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listuv.Count)
            {
                uvSelect = ((GridView)sender).GetRow(_selectedIndex) as UngVienDTO;
            }
            else
                if (listuv.Count != 0)
                    uvSelect = listuv[0];
                else
                    uvSelect = null;

            SetStatus(VIEW);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (uvSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa ứng viên " + uvSelect.MaUV + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = uvCTL.Delete(uvSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listuv.Count != 0)
                            gvUngVien.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void labelControl12_Click(object sender, EventArgs e)
        {

        }

        private void groupControl3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gcUngVien_Click_1(object sender, EventArgs e)
        {

        }

        private void groupControl4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void luePhongKhoa_EditValueChanged(object sender, EventArgs e)
        {
            if (luePhongKhoa.EditValue != null)
            {
                BoMonBoPhanCTL bomonbophanCTL = new BoMonBoPhanCTL();
                List<BoMonBoPhanDTO> listBPBM = bomonbophanCTL.GetListByMaPhongKhoa(luePhongKhoa.EditValue.ToString().Trim());
                //listBPBM.Insert(0, new BoMonBoPhanDTO());
                lueBMBP.Properties.DataSource = listBPBM;
                //if (listBPBM.Count > 0)
                //{
                    //lueBMBP.EditValue = listBPBM[0].MaBMBP;
                //}
            }
        }

        private void lueBMBP_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnTamTuyen_Click(object sender, EventArgs e)
        {
            //if (!CheckInputData())
            //{
            //    //   XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm \n Ngày", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Mã đợt \n Họ lót\n Tên", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            //SetStatus(EDIT);
            //UngVienDTO uvEdit = GetData(new UngVienDTO());
            UngVienDTO uvEdit = new UngVienDTO();
            uvEdit.MaUV = uvSelect.MaUV;
            uvEdit.MaDot = lueMaDot.Text;
            uvEdit.Ho = txtHoLot.Text;
            uvEdit.Ten = txtTen.Text;
            uvEdit.NgaySinh = deNgaySinh.Text;
            //DateTime NgaySinh = DateTime.Parse(deNgaySinh.EditValue.ToString());
            //uvEdit.NgaySinh = NgaySinh.ToString("yyyy-MM-dd");
            uvEdit.GioiTinh = Convert.ToBoolean(rgGioiTinh.EditValue);
            uvEdit.CMND = txtCMND.Text;
            uvEdit.DienThoai = txtDienThoai.Text;
            uvEdit.Email = txtEmail.Text;
            uvEdit.QueQuan = txtQueQuan.Text;
            uvEdit.NganhHoc = txtNganhDaoTao.Text;
            uvEdit.MaTruong = lueTruongDaoTao.Text;
            uvEdit.TruongKhac = txtTruongKhac.Text;
            uvEdit.HinhThucDaoTao = int.Parse(lueHinhThucDaoTao.EditValue.ToString());
            uvEdit.XepHang = txtXepHang.Text;
            uvEdit.DiemTN = txtDiemTotNghiep.Text;
            uvEdit.SauDaiHoc = txtTrinhDoSauDaiHoc.Text;
            uvEdit.MaPK = luePhongKhoa.EditValue.ToString();
            uvEdit.MaBMBP = lueBMBP.EditValue.ToString();
            uvEdit.NganhDuTuyen = txtNganhDuTuyen.Text;
            uvEdit.ChungChi = txtChungChi.Text;
            uvEdit.GhiChu = meGhiChu.Text;
            uvEdit.YeuCauChuyenMon = txtChuyenMon.Text;
            uvEdit.KyNangSuPham = txtKyNangSuPham.Text;

            nvnew.MaNV = Common.GenerateStaffCode(uvEdit.MaPK, uvEdit.MaBMBP);
            nvnew.MaUV = uvEdit.MaUV;
            nvnew.Ho = uvEdit.Ho;
            nvnew.Ten = uvEdit.Ten;
            nvnew.NgaySinh = uvEdit.NgaySinh;
            nvnew.GioiTinh = uvEdit.GioiTinh;
            nvnew.CMND = uvEdit.CMND;
            nvnew.DienThoai = uvEdit.DienThoai;
            nvnew.Email = uvEdit.Email;
            nvnew.QueQuan = uvEdit.QueQuan;
            nvnew.MaPK = uvEdit.MaPK;
            nvnew.MaBMBP = uvEdit.MaBMBP;
            nvnew.MaUV = uvEdit.MaUV;




            var isSucess = false;
            //if (_selectedStatus == NEW)
            //    isSucess = uvCTL.Save(uvNew);
            //else
            //    isSucess = uvCTL.Update(uvNew);
            string warrning = "Quý thầy/cô có chắc muốn tạm tuyển ứng viên " + uvSelect.Ho + " " + uvSelect.Ten + " " + "hay không?";

            if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //isSucess = nvCTL.LuuNhanVien(nvnew);
                isSucess = nvCTL.ChuyenUngVienVaoNhanVien(nvnew);
            }
            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                isSucess = uvCTL.Delete(uvSelect);
                LoadData();
                int _selectedIndex = listuv.IndexOf(listuv.FirstOrDefault(p => p.MaUV == uvEdit.MaUV));
                gvUngVien.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }
    }
}
