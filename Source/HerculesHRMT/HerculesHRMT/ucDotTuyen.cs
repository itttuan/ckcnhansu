﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Linq;

//vdtoan
namespace HerculesHRMT
{
    public partial class ucDotTuyen : DevExpress.XtraEditors.XtraUserControl
    {
        DotTuyenCTL dtCTL = new DotTuyenCTL();
        DotTuyenDTO dtSelect = null;
        List<DotTuyenDTO> listdt = new List<DotTuyenDTO>();
 
        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucDotTuyen()
        {
            InitializeComponent();
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }


        public void LoadData()
         {
            listdt = dtCTL.GetAll();
            gcDotTuyen.DataSource = listdt;
            gridViewDotTuyen.BestFitColumns();

            btnLuu.Enabled = false;

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetStatus(NEW);
            teGio.EditValue = "00:00";
            //deNgay.EditValue = Constants.EmptyDateTimeType2;
            //    deNgayBatDau.EditValue = Constants.EmptyDateTimeType2;
            //    deNgayKetThuc.EditValue = Constants.EmptyDateTimeType2;
            //vdtoan update 220916
            deNgay.EditValue = null;
            deNgayBatDau.EditValue = Constants.EmptyDateTimeType2;
            deNgayKetThuc.EditValue = null;
         
            //vdtoan add 230916
            string madt = "";
            string sLastIndexNganh = dtCTL.GetMaxMaDT();
            if (sLastIndexNganh.CompareTo(string.Empty) != 0)
            {
                int nLastIndexTG = int.Parse(sLastIndexNganh.Substring(2, 4));
                madt += (nLastIndexTG + 1).ToString().PadLeft(4, '0');
            }
            else
                madt = "0001";
            txtMaDot.Text = "DT" + madt;
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    //txtMaDot.Properties.ReadOnly = false;
                    cbbeThang.Properties.ReadOnly = false;
                    cbbeNam.Properties.ReadOnly = false;
                    deNgay.Properties.ReadOnly = false;
                    teGio.Properties.ReadOnly = false;
                    txtPhong.Properties.ReadOnly = false;
                    deNgayBatDau.Properties.ReadOnly = false;
                    deNgayKetThuc.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    
                    cbbeThang.Text = string.Empty;
                    cbbeNam.Text = string.Empty;
                    txtPhong.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;

                    // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case EDIT:
                    cbbeThang.Properties.ReadOnly = false;
                    cbbeNam.Properties.ReadOnly = false;
                    deNgay.Properties.ReadOnly = false;
                    teGio.Properties.ReadOnly = false;
                    txtPhong.Properties.ReadOnly = false;
                    deNgayBatDau.Properties.ReadOnly = false;
                    deNgayKetThuc.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(dtSelect);
                    _selectedStatus = EDIT;
                    
                // add new
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                    break;
                case VIEW:
  
                    cbbeThang.Properties.ReadOnly = true;
                    cbbeNam.Properties.ReadOnly = true;
                    deNgay.Properties.ReadOnly = true;
                    teGio.Properties.ReadOnly = true;
                    txtPhong.Properties.ReadOnly = true;
                    deNgayBatDau.Properties.ReadOnly = true;
                    deNgayKetThuc.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;


                    btnLuu.Enabled = false;

                    BindingData(dtSelect);
                    _selectedStatus = VIEW;
                  
                // add new
                    btnThem.Enabled = true;
                    btnCapNhat.Enabled = true;
                    break;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInputData())
            {
             //   XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm \n Ngày", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DotTuyenDTO dtNew = GetData(new DotTuyenDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = dtCTL.Save(dtNew);
            else
                isSucess = dtCTL.Update(dtNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listdt.IndexOf(listdt.FirstOrDefault(p => p.MaDot == dtNew.MaDot));
                gridViewDotTuyen.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        public void LoadDuLieuTatCaLenUC()
        {
            LoadData();
            SetStatus(VIEW);

            teGio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            teGio.Properties.Mask.EditMask = "HH:mm";
            teGio.MaskBox.Mask.UseMaskAsDisplayFormat = true;

            cbbeThang.Properties.Items.Add("01");
            cbbeThang.Properties.Items.Add("02");
            cbbeThang.Properties.Items.Add("03");
            cbbeThang.Properties.Items.Add("04");
            cbbeThang.Properties.Items.Add("05");
            cbbeThang.Properties.Items.Add("06");
            cbbeThang.Properties.Items.Add("07");
            cbbeThang.Properties.Items.Add("08");
            cbbeThang.Properties.Items.Add("09");
            cbbeThang.Properties.Items.Add("10");
            cbbeThang.Properties.Items.Add("11");
            cbbeThang.Properties.Items.Add("12");
            int iNamhientai = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbbeNam.Properties.Items.Add(iNamhientai - 5 + i);
            }
        }

        private void ucDotTuyen_Load(object sender, EventArgs e)
        {
            
      
        }


        private DotTuyenDTO GetData(DotTuyenDTO dtDTO)
        {
            string madt = "";
            if (_selectedStatus == NEW)
            {
                //string sLastIndexNganh = pkCTL.GetMaxMaPhongKhoa();
                //if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                //{
                //    int nLastIndexTG = int.Parse(sLastIndexNganh);
                //    maPK += (nLastIndexTG + 1).ToString().PadLeft(2, '0');
                //}
                //else
                //    maPK += "01";

                dtDTO.MaDot = txtMaDot.Text;
                dtDTO.Thang = cbbeThang.Text;
                dtDTO.Nam = cbbeNam.Text;
                //DateTime Ngay = DateTime.Parse(deNgay.EditValue.ToString());
                //dtDTO.NgayGio = Ngay.ToString("yyyy-MM-dd") + " " + teGio.Text;
                //vdtoan update 210916
                if (deNgay.EditValue != null && deNgay.Text != string.Empty)
                {
                    DateTime Ngay = DateTime.Parse(deNgay.EditValue.ToString());
                    dtDTO.NgayGio = Ngay.ToString("yyyy-MM-dd") + " " + teGio.Text;
                }
                else
                    dtDTO.NgayGio = string.Empty + " " + teGio.Text;
              
                dtDTO.Phong = txtPhong.Text;
                dtDTO.GhiChu = meGhiChu.Text;

                //DateTime NgayBatDau = DateTime.Parse(deNgayBatDau.EditValue.ToString());
                //dtDTO.NgayBatDau = NgayBatDau.ToString("yyyy-MM-dd");
                //DateTime NgayKetThuc = DateTime.Parse(deNgayKetThuc.EditValue.ToString());
                //dtDTO.NgayKetThuc = NgayKetThuc.ToString("yyyy-MM-dd");
                //vdtoan update 210916
                if (deNgayBatDau.EditValue != null && deNgayBatDau.Text != string.Empty)
                {
                    DateTime NgayBatDau = DateTime.Parse(deNgayBatDau.EditValue.ToString());
                    dtDTO.NgayBatDau = NgayBatDau.ToString("yyyy-MM-dd");
                }
                else 
                    //dtDTO.NgayBatDau = string.Empty;
                    //vdtoan update 220916
                    dtDTO.NgayBatDau = string.Empty;
                
                if (deNgayKetThuc.EditValue != null && deNgayKetThuc.Text != string.Empty)
                {

                DateTime NgayKetThuc = DateTime.Parse(deNgayKetThuc.EditValue.ToString());
                dtDTO.NgayKetThuc = NgayKetThuc.ToString("yyyy-MM-dd");
                }
                else 
                    dtDTO.NgayKetThuc = string.Empty;

                //dtDTO.NgayBatDau = deNgayBatDau;
                //dtDTO.NgayKetThuc = deNgayKetThuc;


                dtDTO.NgayHieuLuc = DateTime.Now;
                dtDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                dtDTO.MaDot = dtSelect.MaDot;
                dtDTO.NgayHieuLuc = dtSelect.NgayHieuLuc;
                dtDTO.TinhTrang = dtSelect.TinhTrang;

                dtDTO.Thang = cbbeThang.Text;
                dtDTO.Nam = cbbeNam.Text;
                
                if (deNgay.EditValue != null && deNgay.Text != string.Empty)
                {
                DateTime Ngay = DateTime.Parse(deNgay.EditValue.ToString());
                dtDTO.NgayGio = Ngay.ToString("yyyy-MM-dd") + " " + teGio.Text;
                }
                else
                    dtDTO.NgayGio = string.Empty + " " + teGio.Text;
              
                dtDTO.Phong = txtPhong.Text;
                dtDTO.GhiChu = meGhiChu.Text;
                //dtDTO.NgayBatDau = deNgayBatDau;
                //dtDTO.NgayKetThuc = deNgayKetThuc;
                if (deNgayBatDau.EditValue != null && deNgayBatDau.Text != string.Empty)
                {
                    DateTime NgayBatDau = DateTime.Parse(deNgayBatDau.EditValue.ToString());
                    dtDTO.NgayBatDau = NgayBatDau.ToString("yyyy-MM-dd");
                }
                else
                    dtDTO.NgayBatDau = string.Empty;
                if (deNgayKetThuc.EditValue != null && deNgayKetThuc.Text != string.Empty)
                {
                    DateTime NgayKetThuc = DateTime.Parse(deNgayKetThuc.EditValue.ToString());
                    dtDTO.NgayKetThuc = NgayKetThuc.ToString("yyyy-MM-dd");
                }
                else
                    dtDTO.NgayKetThuc = string.Empty;
            }

           

            //int result = 0;
            //if (int.TryParse(txtPhong.Text, out result))
            //    pkDTO.ThuTuBaoCao = result;
            //else
            //    pkDTO.ThuTuBaoCao = null;

            //pkDTO.TenVietTat = txtTenVT.Text;
            //pkDTO.GhiChu = meGhiChu.Text;

            return dtDTO;
        }

        private void cbbeThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            //vdtoan delete 230916
            //txtMaDot.Text = cbbeNam.Text.Trim() + cbbeThang.Text.Trim();
        }

        private void cbbeNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            //vdtoan delete 230916
            //txtMaDot.Text = cbbeNam.Text.Trim() + cbbeThang.Text.Trim();
        }

        private bool CheckInputData()
        {
           //if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty || deNgay.Text === string.Empty)
            if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty)
                return false;

            return true;
        }

        private void gcDotTuyen_Click(object sender, EventArgs e)
        {

        }

        private void BindingData(DotTuyenDTO dtDTO)
        {
            if (dtDTO != null)
            {


                txtMaDot.Text = dtDTO.MaDot;
                cbbeThang.Text = dtDTO.Thang;
                cbbeNam.Text = dtDTO.Nam;
                if (dtDTO.NgayGio.Length != 6)
                {
                    deNgay.Text = dtDTO.NgayGio.Substring(0, 10);
                    teGio.EditValue = dtDTO.NgayGio.Substring(11, 5);
                }
                else
                {
                    deNgay.Text = "";
                    teGio.EditValue = dtDTO.NgayGio.Substring(1, 5);
                }
                txtPhong.Text = dtDTO.Phong;
                meGhiChu.Text = dtDTO.GhiChu;
                deNgayBatDau.Text = dtDTO.NgayBatDau;
                deNgayKetThuc.Text = dtDTO.NgayKetThuc;

            }
        }

        private void gridViewDotTuyen_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listdt.Count)
            {
                dtSelect = ((GridView)sender).GetRow(_selectedIndex) as DotTuyenDTO;
            }
            else
                if (listdt.Count != 0)
                    dtSelect = listdt[0];
                else
                    dtSelect = null;

            SetStatus(VIEW);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dtSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa đợt tuyển " + dtSelect.MaDot + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = dtCTL.Delete(dtSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listdt.Count != 0)
                            gridViewDotTuyen.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private void grcDotTuyen_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
