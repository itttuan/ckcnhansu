﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using HerculesDTO;
using HerculesCTL;
using System.IO;

namespace HerculesHRMT
{
    public partial class rptLyLich : DevExpress.XtraReports.UI.XtraReport
    {
        List<byte[]> listImgNV = new List<byte[]>();
        HinhAnhCTL hinhanhCTL = new HinhAnhCTL();
        QuanHuyenCTL quanhuyenCTL = new QuanHuyenCTL();
        TinhThanhCTL tinhthanhCTL = new TinhThanhCTL();
                
        public rptLyLich()
        {
            InitializeComponent();

        }

        public rptLyLich(ThongTinNhanVienDTO aa)
        {
            InitializeComponent();
            printReport(aa);
            List<ThongTinNhanVienDTO> nv = new List<ThongTinNhanVienDTO>();
            nv.Add(aa);
            this.DataSource = nv;
          
        }


        private void LoadImageNV(string strMaNV)
        {
            try
            {
                listImgNV = hinhanhCTL.LayTatCaHinhTheoNhanVien(strMaNV);
                if (listImgNV.Count == 0)
                {
                    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDanhSachNhanVien));
                    this.imgHinhAnh.Image = ((System.Drawing.Image)(resources.GetObject("imgHinhAnh.Properties.Appearance.Image")));

                }
                else
                {
                    MemoryStream stream = new MemoryStream(listImgNV[listImgNV.Count - 1]);
                    this.imgHinhAnh.Image = Image.FromStream(stream);
                }
            }
            catch
            {
            }
        }

        public void printReport(ThongTinNhanVienDTO nv)
        {
            //Xử lý hộ khẩu thường trú
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.HoKhauThuongTruDiaChi))
            {
                lbHKTT.Text = nv.lylichsoluoc.HoKhauThuongTruDiaChi + ", " + quanhuyenCTL.GetByMaQuanHuyen(nv.lylichsoluoc.HoKhauThuongTruMaQuanHuyen).TenDayDu
                + ", " + tinhthanhCTL.GetByMaTinhThanh(nv.lylichsoluoc.HoKhauThuongTruMaTinhThanh).TenDayDu;
            }

            //Xử lý nơi ở hiện tại
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.DiaChiHienTaiDiaChi))
            {
                lbNOHN.Text = nv.lylichsoluoc.DiaChiHienTaiDiaChi + ", " + quanhuyenCTL.GetByMaQuanHuyen(nv.lylichsoluoc.DiaChiHienTaiMaQuanHuyen).TenDayDu
                + ", " + tinhthanhCTL.GetByMaTinhThanh(nv.lylichsoluoc.DiaChiHienTaiMaTinhThanh).TenDayDu;
            }

            //Xử lý hình ảnh
            LoadImageNV(nv.lylichsoluoc.MaNV);
                        
            //Xử lý ngày sinh
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgaySinh))
            {
                DateTime ngaySinh = DateTime.Parse(nv.lylichsoluoc.NgaySinh);
                lbNgaySinh.Text = ngaySinh.ToString("dd/MM/yyyy");
            }

            //Xử lý ngày tuyển dụng
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayTuyenDung ))
            {
                DateTime ngayTD = DateTime.Parse(nv.lylichsoluoc.NgayTuyenDung);
                lbNgayTD.Text = ngayTD.ToString("dd/MM/yyyy");
            }

            //Xử lý ngày hưởng lương
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayHuong))
            {
                //DateTime ngayTD = DateTime.Parse(nv.lylichsoluoc.NgayTuyenDung);
                lbNgayHuongLuong.Text = Utils.getDate(nv.lylichsoluoc.NgayHuong);
            }

            //Xử lý trình độ chuyên môn cao nhất, không đang học
            if (nv.trinhdochuyenmon != null && nv.trinhdochuyenmon.Count!=0)
            {
                TrinhDoDTO td = nv.trinhdochuyenmon.Find(delegate(TrinhDoDTO _td) { return (_td.LoaiDaoTao.CompareTo("0") == 0); });
                if(td != null)
                    lbTrinhDoChuyenMonCaoNhat.Text = td.LoaiBangChuyenMon.TenLoaiBangChuyenMon + " " + td.ChuyenNganhDaoTao;
            }

            //Xử lý ngày vào Đảng
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayVaoDCSVN ))
            {
                DateTime ngayVaoDang = DateTime.Parse(nv.lylichsoluoc.NgayVaoDCSVN);
                lbNgayVaoDCSVN.Text = ngayVaoDang.ToString("dd/MM/yyyy");
            }

            //Xử lý ngày chính thức vào Đảng
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayChinhThucVaoDCSVN ))
            {
                DateTime ngayChinhThucVaoDCSVN = DateTime.Parse(nv.lylichsoluoc.NgayChinhThucVaoDCSVN);
                lbNgayChinhThucVaoDCSVN.Text = ngayChinhThucVaoDCSVN.ToString("dd/MM/yyyy");
            }

            //Xử lý ngày nhập ngũ
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayNhapNgu ))
            {
                DateTime ngayNhapNgu = DateTime.Parse(nv.lylichsoluoc.NgayNhapNgu);
                lbNgayNhapNgu.Text = ngayNhapNgu.ToString("dd/MM/yyyy");
            }

            //Xử lý ngày xuất ngũ
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayXuatNgu ))
            {
                DateTime ngayXuatNgu = DateTime.Parse(nv.lylichsoluoc.NgayXuatNgu);
                lbNgayXuatNgu.Text = ngayXuatNgu.ToString("dd/MM/yyyy");
            }

            //Xử lý chiều cao
            string chieuCao = nv.lylichsoluoc.ChieuCao;
            if (chieuCao.CompareTo("") != 0)
            {
                if (chieuCao.Contains(",") || chieuCao.Contains("."))
                {
                    if (!chieuCao.Contains("m"))
                        lbChieuCao.Text = chieuCao + " m";
                    else
                        lbChieuCao.Text = chieuCao;
                }
                else
                    lbChieuCao.Text = chieuCao + " cm";
            }
            

            //Xử lý cân nặng
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.CanNang))
            {
                lbCanNang.Text = nv.lylichsoluoc.CanNang;
                if(!lbCanNang.Text.Contains("kg"))
                    lbCanNang.Text += " kg";
            }

            //Xử lý nhóm máu
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NhomMau))
                lbNhomMau.Text = nv.lylichsoluoc.NhomMau.ToUpper();

            //Xử lý ngày cấp CMND
            if (!String.IsNullOrEmpty(nv.lylichsoluoc.NgayCapCMND))
            {
                DateTime ngayCapCMND = DateTime.Parse(nv.lylichsoluoc.NgayCapCMND);
                lbNgayCapCMND.Text = ngayCapCMND.ToString("dd/MM/yyyy");
            }

            //Xử lý tự nhận xét
            if (!String.IsNullOrEmpty(nv.tunhanxet))
            {
                lbTuNhanXet.Text = nv.tunhanxet.Replace("\\r\\n", Environment.NewLine);
            }
        }
    }
}
