﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using HerculesCTL;
using System.IO;
using System.Diagnostics;

namespace HerculesHRMT
{
    public partial class frmMain : RibbonForm
    {
        bool isDangnhap = false;

        ////vdtoan add 260916
        //private int tempHeight = 0, tempWidth = 0;
        //private int FixHeight = 1024, FixWidth = 768;

        public frmMain()
        {
            InitializeComponent();
            InitSkinGallery();
            InitGrid();

            ////vdtoan add 260916
            //Screen Srn = Screen.PrimaryScreen;
            //tempHeight = Srn.Bounds.Width;
            //tempWidth = Srn.Bounds.Height;
            ////MessageBox.Show("Resolution is going to change to " + FixHeight.ToString() + " X " + FixWidth.ToString());
            //Resolution.CResolution ChangeRes = new Resolution.CResolution(FixHeight, FixWidth);


        }
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        void InitGrid()
        {
            
        }

        private void iLogout_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            RunAutoUpdate();

            this.Ribbon.Minimized = true;
            if (splitContainerControl.Panel2.Controls.Count > 1)
            {
                splitContainerControl.Panel2.Controls.RemoveAt(0);
                
            }
            LoadDangNhap();
        }

        private void LoadDangNhap()
        {
            this.iLogout.Links[0].Visible = false;
            isDangnhap = false;
            RemoveAllUserControl();
            ucDangNhap ucDN = new ucDangNhap();
            Point p = new Point(splitContainerControl.Panel2.Width / 2 - ucDN.Width / 2, splitContainerControl.Panel2.Height / 4 - ucDN.Height / 4);
            ucDN.Anchor = AnchorStyles.None;
            ucDN.Location = p;
            splitContainerControl.Panel2.Controls.Add(ucDN);
            AcceptButton = ucDN.btnDangNhap;
            ucDN.dnSender = getSender;
        }
        private void itemTinhThanh_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTinhThanh());
        }

        private void RemoveAllUserControl()
        {
            foreach (Control ctl in splitContainerControl.Panel2.Controls)
            {
                splitContainerControl.Panel2.Controls.Remove(ctl);
            }
        }

        private void itemQuanHuyen_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucQuanHuyen());
        }

        private void itemTruong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTruongHoc());
        }

        private void itemHocVan_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucHocVanBangCap());
        }

        private void itemChuVu_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucChucVu());
        }

        private void itemPhongKhoa_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucPhongKhoa());
        }

        private void itemNganh_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucNganh());
        }

        private void itemTrangThaiLV_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTrangThaiLamViec());
        }

        private void itemQuanHe_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucQuanHeThanNhan());
        }

        private void itemLoaiHopDong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiHopDong());
        }

        private void itemNhanVien_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDanhSachNhanVien());
        }

        private void itemBoMon_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucBoPhanBoMon());
        }

        public void getSender(bool isDaDangNhap)
        {
            isDangnhap = isDaDangNhap;
            this.iLogout.Links[0].Visible = true;
            RemoveAllUserControl();
            ucDanhSachNhanVien ucDSNV = new ucDanhSachNhanVien();
            ucDSNV.Dock = DockStyle.Fill;
            splitContainerControl.Panel2.Controls.Add(ucDSNV);
        }

        private void itemDantoc_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDanToc());
        }

        private void itemTongiao_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTonGiao());
        }

        private void itemHedaotao_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucHeDaoTao());
        }

        private void GanUserControl(XtraUserControl uc)
        {
            if (isDangnhap)
            {
                RemoveAllUserControl();
                uc.Dock = DockStyle.Fill;
                splitContainerControl.Panel2.Controls.Add(uc);
            }
            else
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng đăng nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void itemLoaitrinhdo_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiTrinhDo());
        }

        private void itemLoaiNV_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiNhanVien());
        }

        private void itemLoaiQD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucLoaiQuyetDinh());
        }

        private void iLogout_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            ReportCTL abc = new ReportCTL();
            abc.LayDuLieu();

            //if (XtraMessageBox.Show("Quý thầy cô xác thực đăng xuất.", "Đăng xuất", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    LoadDangNhap();
            //}
        }


        private void itemTongHopNhanSu_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucTongHopTinhHinhNhanSu());
        }
                

        private void RunAutoUpdate()
        {
            try
            {
                string sPath = Path.GetDirectoryName(Application.ExecutablePath);
                string sAUFilePath = sPath + "\\AutoUpdate.exe";
                string sNewAUFilePath = sPath + "\\AutoUpdate.ex_";


                FileInfo newFi = new FileInfo(sNewAUFilePath);
                if (newFi.Exists)
                {
                    File.Delete(sAUFilePath);
                    File.Move(sNewAUFilePath, sAUFilePath);

                    string sPDBFilePath = sPath + "\\AutoUpdate.pdb";
                    string sNewPDBFilePath = sPath + "\\AutoUpdate.pd_";
                    FileInfo newPDBFi = new FileInfo(sNewPDBFilePath);
                    if (newPDBFi.Exists)
                    {
                        File.Delete(sPDBFilePath);
                        File.Move(sNewPDBFilePath, sPDBFilePath);
                    }
                }

                //NBPhúc 30/06/2013

                FileInfo fi = new FileInfo(sAUFilePath);
                if (fi.Exists)
                {
                    Process.Start(sAUFilePath);
                }
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show(ex.Message);
            }
        }

        private void itemUngTuyen_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDanhSachUngVien());
            //frmThongBao ucTB = new frmThongBao();
            //ucTB.Show();
        }

        private void itemDotTuyenDung_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucDotTuyen());
        }

        private void itemNhacNho_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
           // GanUserControl(new frmNhacNho());
            frmNhacNho frm = new frmNhacNho();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void ribbonControl_Click(object sender, EventArgs e)
        {

        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            ////MessageBox.Show("Resolution is going to change to " + tempHeight.ToString() + " X " + tempWidth.ToString());
            //Resolution.CResolution ChangeRes = new Resolution.CResolution(tempHeight, tempWidth);
        }

        private void itemHetHanHopDong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            //GanUserControl(new ucBaoCaoHetHanHopDong());
        }

        private void itemHopDong_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            GanUserControl(new ucHopDong());
        }

    }
}