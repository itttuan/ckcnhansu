﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucQuanHuyen : XtraUserControl
    {
        QuanHuyenCTL qhCTL = new QuanHuyenCTL();
        TinhThanhCTL ttCTL = new TinhThanhCTL();
        List<QuanHuyenDTO> listQuanHuyen = new List<QuanHuyenDTO>();
        List<TinhThanhDTO> listTinhThanh = new List<TinhThanhDTO>();
        QuanHuyenDTO qhSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucQuanHuyen()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listQuanHuyen = qhCTL.GetAll();            
                        
            gcQuanHuyen.DataSource = listQuanHuyen;
            btnLuu.Enabled = false;
        }

        private void ucQuanHuyen_Load(object sender, System.EventArgs e)
        {
            listTinhThanh = ttCTL.GetAll();
            lueTinhThanh.Properties.DataSource = listTinhThanh;

            LoadData();
            SetStatus(VIEW);
        }

        // Display content of row to textboxes
        private void BindingData(QuanHuyenDTO quanhuyenDTO)
        {
            if (quanhuyenDTO != null)
            {
                lueTinhThanh.EditValue = quanhuyenDTO.MaTinhThanh;
                txtTenDD.Text = quanhuyenDTO.TenDayDu;
                txtQuanHuyen.Text = quanhuyenDTO.TenQuanHuyen;
                txtTenVT.Text = quanhuyenDTO.TenVietTat;
                meGhiChu.Text = quanhuyenDTO.GhiChu;
                radLoaiQH.EditValue = quanhuyenDTO.Loai;
            }
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    lueTinhThanh.Properties.ReadOnly = false;
                    txtTenDD.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtQuanHuyen.Properties.ReadOnly = false;
                    radLoaiQH.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    
                    txtTenDD.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    txtQuanHuyen.Text = string.Empty;
                    radLoaiQH.EditValue = 0;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    lueTinhThanh.Properties.ReadOnly = false;
                    txtTenDD.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    txtQuanHuyen.Properties.ReadOnly = false;
                    radLoaiQH.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(qhSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    lueTinhThanh.Properties.ReadOnly = true;
                    txtTenDD.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    txtQuanHuyen.Properties.ReadOnly = true;
                    radLoaiQH.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(qhSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private QuanHuyenDTO GetData(QuanHuyenDTO _qhDTO)
        {
            string maQH = "QH";
            if (_selectedStatus == NEW)
            {
                string sLastIndexQH = qhCTL.GetMaxMaQuanHuyen();
                if (sLastIndexQH.CompareTo(string.Empty) != 0)
                {                   
                    int nLastIndexTT = int.Parse(sLastIndexQH.Split(new string[] { "QH" }, System.StringSplitOptions.RemoveEmptyEntries)[0]);
                    maQH += (nLastIndexTT + 1).ToString().PadLeft(3, '0');
                }
                else
                    maQH += "001";

                _qhDTO.NgayHieuLuc = DateTime.Now;
                _qhDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maQH = qhSelect.MaQuanHuyen;
                _qhDTO.NgayHieuLuc = qhSelect.NgayHieuLuc;
                _qhDTO.TinhTrang = qhSelect.TinhTrang;
            }
            _qhDTO.MaQuanHuyen = maQH;
            _qhDTO.TenQuanHuyen = txtQuanHuyen.Text;
            _qhDTO.TenDayDu = txtTenDD.Text;
            _qhDTO.TenVietTat = txtTenVT.Text;
            _qhDTO.Loai = (int)radLoaiQH.EditValue;
            _qhDTO.GhiChu = meGhiChu.Text;
            _qhDTO.MaTinhThanh = (string)lueTinhThanh.EditValue;

            return _qhDTO;
        }

        private void gridViewDSQuanHuyen_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listQuanHuyen.Count)
            {
                qhSelect = ((GridView)sender).GetRow(_selectedIndex) as QuanHuyenDTO;
            }
            else
                if (listQuanHuyen.Count != 0)
                    qhSelect = listQuanHuyen[0];
                else
                    qhSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            SetStatus(EDIT);
        }

        private bool CheckInputData()
        {
            if (txtTenDD.Text == string.Empty || txtTenVT.Text == string.Empty || txtQuanHuyen.Text == string.Empty)
                return false;

            return true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tỉnh/Thành\n Tên đầy đủ\n Tên Quận/Huyện\n Tên viết tắt\n Loại", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            QuanHuyenDTO qhNew = GetData(new QuanHuyenDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = qhCTL.Save(qhNew);
            else
                isSucess = qhCTL.Update(qhNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listQuanHuyen.IndexOf(listQuanHuyen.FirstOrDefault(p => p.MaQuanHuyen == qhNew.MaQuanHuyen));
                gridViewDSQuanHuyen.FocusedRowHandle = _selectedIndex;                
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //txtChucVu.Enter += textbox_Enter;
            //meMoTa.Enter += textbox_Enter;

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (qhSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa quận/huyện " + qhSelect.TenQuanHuyen + " hay không?";
                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = qhCTL.Delete(qhSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if(listQuanHuyen.Count != 0)
                            gridViewDSQuanHuyen.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
