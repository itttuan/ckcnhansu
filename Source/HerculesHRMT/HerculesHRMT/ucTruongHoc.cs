﻿using System.Collections.Generic;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System;
using System.Linq;

namespace HerculesHRMT
{
    public partial class ucTruongHoc : XtraUserControl
    {
        TruongCTL tCTL = new TruongCTL();
        List<TruongDTO> listTruongHocvien = new List<TruongDTO>();
        TruongDTO tSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;


        //H.Nguyen
        public delegate void SendInfor(bool isSuccess);
        public SendInfor truonghocSender;
        public ucTruongHoc()
        {
            InitializeComponent();
            TaodanhsachLoaiTruong();
            
        }

        public void TaodanhsachLoaiTruong()
        {
            List<KeyValuePair<string, string>> dsLoaiTruong = new List<KeyValuePair<string,string>>();
            dsLoaiTruong.Add(new KeyValuePair<string, string>("THPT", "0"));
            dsLoaiTruong.Add(new KeyValuePair<string, string>("Trung học chuyên nghiệp", "1"));
            dsLoaiTruong.Add(new KeyValuePair<string, string>("Cao đẳng", "2"));
            dsLoaiTruong.Add(new KeyValuePair<string, string>("Đại học", "3"));
            dsLoaiTruong.Add(new KeyValuePair<string, string>("Học viện", "4"));
            lueLoaiTruong.Properties.DataSource = dsLoaiTruong;
        }

        public void LoadData()
        {
            listTruongHocvien = tCTL.GetAll();

            gcTruongHocvien.DataSource = listTruongHocvien;
            btnLuu.Enabled = false;
        }

        // Display content of row to textboxes
        private void BindingData(TruongDTO tDTO)
        {
            if (tDTO != null)
            {
                txtMaTruong.Text = tDTO.MaTruong;
                txtTenTruongHocvien.Text = tDTO.TenTruong;
                txtTenVT.Text = tDTO.TenVietTat;
                lueLoaiTruong.EditValue = tDTO.LoaiTruong;
                meGhiChu.Text = tDTO.GhiChu;
                checkboxIsNuocNgoai.Checked = tDTO.NuocNgoai;
            }
        }

        private void gridViewTruongHocvien_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listTruongHocvien.Count)
            {
                tSelect = ((GridView)sender).GetRow(_selectedIndex) as TruongDTO;
            }
            else
                if (listTruongHocvien.Count != 0)
                    tSelect = listTruongHocvien[0];
                else
                    tSelect = null;

            SetStatus(VIEW);
        }

        // Set status (enable) for buttons follow user's actions
        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtMaTruong.Properties.ReadOnly = false;
                    txtTenTruongHocvien.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    lueLoaiTruong.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    checkboxIsNuocNgoai.Enabled = true;
                    
                    txtMaTruong.Text = string.Empty;
                    txtTenTruongHocvien.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    lueLoaiTruong.EditValue = "3";//Trường Đại Học
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtMaTruong.Properties.ReadOnly = true;
                    txtTenTruongHocvien.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    lueLoaiTruong.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;
                    checkboxIsNuocNgoai.Enabled = true;

                    btnLuu.Enabled = true;

                    BindingData(tSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtMaTruong.Properties.ReadOnly = true;
                    txtTenTruongHocvien.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    lueLoaiTruong.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;
                    checkboxIsNuocNgoai.Enabled = false;

                    btnLuu.Enabled = false;

                    BindingData(tSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtMaTruong.Text == string.Empty || txtTenTruongHocvien.Text == string.Empty)
                return false;

            return true;
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Mã trường\n Tên trường", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TruongDTO tNew = GetData(new TruongDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = tCTL.Save(tNew);
            else
                isSucess = tCTL.Update(tNew);

            if (isSucess)
            {
                truonghocSender(isSucess);
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listTruongHocvien.IndexOf(listTruongHocvien.FirstOrDefault(p => p.MaTruong == tNew.MaTruong));
                gridViewTruongHocvien.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (tSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa trường " + tSelect.TenTruong + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = tCTL.Delete(tSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if(listTruongHocvien.Count != null)
                            gridViewTruongHocvien.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }

        private TruongDTO GetData(TruongDTO tDTO)
        {
            if (_selectedStatus == NEW)
            {
                tDTO.NgayHieuLuc = DateTime.Now;
                tDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                tDTO.NgayHieuLuc = tSelect.NgayHieuLuc;
                tDTO.TinhTrang = tSelect.TinhTrang;
            }
            tDTO.MaTruong = txtMaTruong.Text;
            tDTO.TenTruong = txtTenTruongHocvien.Text;
            tDTO.TenVietTat = txtTenVT.Text;
            tDTO.LoaiTruong = lueLoaiTruong.EditValue.ToString();
            tDTO.GhiChu = meGhiChu.Text;
            tDTO.NuocNgoai = checkboxIsNuocNgoai.Checked;

            return tDTO;
        }

        private void ucTruongHoc_Load(object sender, EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }
    }
}
