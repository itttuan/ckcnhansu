﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using DevExpress.XtraGrid.Views.Grid;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.XtraGrid.Columns;

namespace HerculesHRMT
{
    public partial class frmNhacNho : DevExpress.XtraEditors.XtraForm
    {
        NhacNhoCTL nnCTL = new NhacNhoCTL();
        NhacNhoDTO nnSelect = null;
        List<NhacNhoDTO> listnn = new List<NhacNhoDTO>();

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public frmNhacNho()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            listnn = nnCTL.GetAllNhacNho();
            gcNhacNho.DataSource = listnn;
            gridViewNhacNho.BestFitColumns();

            //btnLuu.Enabled = false;
            //this.gridViewNhacNho.Columns["NgayHetHan"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.Value;

        }

        //private void SetStatus(int status)
        //{
        //    _selectedStatus = status;

        //    switch (status)
        //    {
        //        case NEW:

        //            txtID.Properties.ReadOnly = false;
        //            cbbeLoaiNhacNho.Properties.ReadOnly = false;
        //            deThoiGianNhac.Properties.ReadOnly = false;
        //            deNgayHetHan.Properties.ReadOnly = false;
        //            txtSoQuyetDinh.Properties.ReadOnly = false;
        //            meGhiChu.Properties.ReadOnly = false;
        //            meNoiDung.Properties.ReadOnly = false;

        //            txtID.Text = string.Empty;
        //            cbbeLoaiNhacNho.Text = string.Empty;
        //            txtSoQuyetDinh.Text = string.Empty;
        //            meGhiChu.Text = string.Empty;
        //            meNoiDung.Text = string.Empty;

        //            btnLuu.Enabled = true;

        //            _selectedStatus = NEW;

        //            // add new
        //            btnThem.Enabled = false;
        //            btnCapNhat.Enabled = false;
        //            break;
        //        case EDIT:
        //            txtID.Properties.ReadOnly = false;
        //            cbbeLoaiNhacNho.Properties.ReadOnly = false;
        //            deThoiGianNhac.Properties.ReadOnly = false;
        //            deNgayHetHan.Properties.ReadOnly = false;
        //            txtSoQuyetDinh.Properties.ReadOnly = false;
        //            meGhiChu.Properties.ReadOnly = false;
        //            meNoiDung.Properties.ReadOnly = false;

        //            btnLuu.Enabled = true;

        //            BindingData(nnSelect);
        //            _selectedStatus = EDIT;

        //            // add new
        //            btnThem.Enabled = false;
        //            btnCapNhat.Enabled = false;
        //            break;
        //        case VIEW:

        //            txtID.Properties.ReadOnly = true;
        //            cbbeLoaiNhacNho.Properties.ReadOnly = true;
        //            deThoiGianNhac.Properties.ReadOnly = true;
        //            deNgayHetHan.Properties.ReadOnly = true;
        //            txtSoQuyetDinh.Properties.ReadOnly = true;
        //            meGhiChu.Properties.ReadOnly = true;
        //            meNoiDung.Properties.ReadOnly = true;

        //            btnLuu.Enabled = false;

        //            BindingData(nnSelect);
        //            _selectedStatus = VIEW;

        //            // add new
        //            btnThem.Enabled = true;
        //            btnCapNhat.Enabled = true;
        //            break;
        //    }
        //}

        //private void btnThem_Click(object sender, EventArgs e)
        //{
        //    SetStatus(NEW);
        //    //deNgay.EditValue = Constants.EmptyDateTimeType2;
        //    //teGio.EditValue = "00:00";
        //    //deNgayBatDau.EditValue = Constants.EmptyDateTimeType2;
        //    //deNgayKetThuc.EditValue = Constants.EmptyDateTimeType2;
        //    //txtID.Text = String.Empty;
            
        //    int mann = 1;
        //    string sLastIndexIDNhacNho = nnCTL.GetMaxId();
        //    if (sLastIndexIDNhacNho.CompareTo(string.Empty) != 0)
        //    {

        //        mann = int.Parse(sLastIndexIDNhacNho) + 1;

        //    }
        //    //else
        //    //    mann = 1;
        //    txtID.Text = mann.ToString();


        //    cbbeLoaiNhacNho.Text = String.Empty;
        //    txtSoQuyetDinh.Text = String.Empty;
        //    deThoiGianNhac.EditValue = Constants.EmptyDateTimeType2;
        //    deNgayHetHan.EditValue = Constants.EmptyDateTimeType2;
        //    meNoiDung.Text = String.Empty;
        //    meGhiChu.Text = String.Empty;

        //}


        //private void btnCapNhat_Click(object sender, EventArgs e)
        //{
        //    SetStatus(EDIT);
        //}

        //private void btnLuu_Click(object sender, EventArgs e)
        //{
        //    //if (!CheckInputData())
        //    //{
        //    //    //   XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm \n Ngày", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    //    XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tháng\n Năm", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    //    return;
        //    //}

        //    NhacNhoDTO nnNew = GetData(new NhacNhoDTO());
        //    var isSucess = false;
        //    if (_selectedStatus == NEW)
        //        isSucess = nnCTL.Save(nnNew);
        //    else
        //        isSucess = nnCTL.Update(nnNew);

        //    if (isSucess)
        //    {
        //        XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //        LoadData();
        //        int _selectedIndex = listnn.IndexOf(listnn.FirstOrDefault(p => p.Id == nnNew.Id));
        //        gridViewNhacNho.FocusedRowHandle = _selectedIndex;
                
        //    }
        //    else
        //    {
        //        XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }

        //    SetStatus(VIEW);
        //}

        //private void btnXoa_Click(object sender, EventArgs e)
        //{
        //    if (nnSelect != null)
        //    {
        //        string warrning = "Quý thầy/cô có chắc muốn xóa nhắc nhở " + nnSelect.Id + " hay không?";

        //        if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
        //        {
        //            bool isSucess = nnCTL.Delete(nnSelect);

        //            if (isSucess)
        //            {
        //                XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //                LoadData();
        //                if (listnn.Count != 0)
        //                    gridViewNhacNho.FocusedRowHandle = 0;
        //            }
        //            else
        //            {
        //                XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //                SetStatus(VIEW);
        //            }
        //        }
        //    }
        //}

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmNhacNho2_Load(object sender, EventArgs e)
        {
            LoadData();
            //SetStatus(VIEW);

            cbbeThang.Properties.Items.Add("01");
            cbbeThang.Properties.Items.Add("02");
            cbbeThang.Properties.Items.Add("03");
            cbbeThang.Properties.Items.Add("04");
            cbbeThang.Properties.Items.Add("05");
            cbbeThang.Properties.Items.Add("06");
            cbbeThang.Properties.Items.Add("07");
            cbbeThang.Properties.Items.Add("08");
            cbbeThang.Properties.Items.Add("09");
            cbbeThang.Properties.Items.Add("10");
            cbbeThang.Properties.Items.Add("11");
            cbbeThang.Properties.Items.Add("12");
            int iNamhientai = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                cbbeNam.Properties.Items.Add(iNamhientai - 5 + i);
            }

    
            cbbeTCTK_TrangThai.Properties.Items.Add("Tất cả");
            cbbeTCTK_TrangThai.Properties.Items.Add("Đã xử lý");
            cbbeTCTK_TrangThai.Properties.Items.Add("Chưa xử lý");

        }

        //private NhacNhoDTO GetData(NhacNhoDTO nnDTO)
        //{
        //    //    string madt = "";
        //    if (_selectedStatus == NEW)
        //    {
        //        //string sLastIndexNganh = pkCTL.GetMaxMaPhongKhoa();
        //        //if (sLastIndexNganh.CompareTo(string.Empty) != 0)
        //        //{
        //        //    int nLastIndexTG = int.Parse(sLastIndexNganh);
        //        //    maPK += (nLastIndexTG + 1).ToString().PadLeft(2, '0');
        //        //}
        //        //else
        //        //    maPK += "01";

               
        //        nnDTO.Id = Int32.Parse(txtID.Text);

        //        nnDTO.MaLoaiNhacNho = cbbeLoaiNhacNho.Text;
        //        nnDTO.SoQuyetDinh = txtSoQuyetDinh.Text;
        //        DateTime ThoiGianNhac = DateTime.Parse(deThoiGianNhac.EditValue.ToString());
        //        nnDTO.ThoiGianNhac = ThoiGianNhac.ToString("yyyy-MM-dd");
        //        DateTime NgayHetHan = DateTime.Parse(deNgayHetHan.EditValue.ToString());
        //        nnDTO.NgayHetHan = NgayHetHan.ToString("yyyy-MM-dd");
        //        nnDTO.NoiDung = meNoiDung.Text;
        //        nnDTO.GhiChu = meGhiChu.Text;
        //        nnDTO.NgayHieuLuc = DateTime.Now;
        //        nnDTO.TinhTrang = 0;
        //    }
        //    else if (_selectedStatus == EDIT)
        //    {
        //        nnDTO.Id = nnSelect.Id;

        //        nnDTO.MaLoaiNhacNho = cbbeLoaiNhacNho.Text;
        //        nnDTO.NoiDung = meNoiDung.Text;
        //        nnDTO.GhiChu = meGhiChu.Text;
        //        nnDTO.SoQuyetDinh = txtSoQuyetDinh.Text;
        //        DateTime ThoiGianNhac = DateTime.Parse(deThoiGianNhac.EditValue.ToString());
        //        nnDTO.ThoiGianNhac = ThoiGianNhac.ToString("yyyy-MM-dd");
        //        DateTime NgayHetHan = DateTime.Parse(deNgayHetHan.EditValue.ToString());
        //        nnDTO.NgayHetHan = NgayHetHan.ToString("yyyy-MM-dd");
        //    }



        //    //int result = 0;
        //    //if (int.TryParse(txtPhong.Text, out result))
        //    //    pkDTO.ThuTuBaoCao = result;
        //    //else
        //    //    pkDTO.ThuTuBaoCao = null;

        //    //pkDTO.TenVietTat = txtTenVT.Text;
        //    //pkDTO.GhiChu = meGhiChu.Text;

        //    return nnDTO;
        //}
        //private bool CheckInputData()
        //{
        //    //if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty || deNgay.Text === string.Empty)
        //    if (cbbeLoaiNhacNho.Text == string.Empty || cbbeNam.Text == string.Empty)
        //        return false;

        //    return true;
        //}

        //private void BindingData(NhacNhoDTO nnDTO)
        //{
        //    if (nnDTO != null)
        //    {

        //        txtID.Text = nnDTO.Id.ToString();
        //        cbbeLoaiNhacNho.Text = nnDTO.MaLoaiNhacNho;
        //        deThoiGianNhac.Text = nnDTO.ThoiGianNhac;
        //        deNgayHetHan.Text = nnDTO.NgayHetHan;
        //        txtSoQuyetDinh.Text = nnDTO.SoQuyetDinh;
        //        meGhiChu.Text = nnDTO.GhiChu;
        //        meNoiDung.Text = nnDTO.NoiDung;


        //    }
        //}

        //private void gridViewNhacNho_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        //{
        //    int _selectedIndex = ((GridView)sender).FocusedRowHandle;
        //    if (_selectedIndex < listnn.Count)
        //    {
        //        nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
        //    }
        //    else
        //        if (listnn.Count != 0)
        //            nnSelect = listnn[0];
        //        else
        //            nnSelect = null;

        //    SetStatus(VIEW);
        //}

        private void gcNhacNho_Click(object sender, EventArgs e)
        {

        }

        private void reButtonEdit_XuLy_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (nnSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn chuyển trạng thái nhắc nhở cho ID " + nnSelect.Id + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = nnCTL.ChuyenTrangThaiNhacNho(nnSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Chuyển trạng thái thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listnn.Count != 0)
                            gridViewNhacNho.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Chuyển trạng thái thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                   //     SetStatus(VIEW);
                    }
                }
            }
        }

        private void gridViewNhacNho_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listnn.Count)
            {
                nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
            }
            else
                if (listnn.Count != 0)
                    nnSelect = listnn[0];
                else
                    nnSelect = null;

            //SetStatus(VIEW);
        }

        private void cbbeTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            
               
            

        }

        private void btnTimKiemMaNV_Click(object sender, EventArgs e)
        {
            //DateTime FromDate, ToDate;
            //FromDate = new DateTime(int.Parse(cbbeNam.Text), int.Parse(cbbeThang.Text), 01);
            //ToDate = new DateTime(int.Parse(cbbeNam.Text), int.Parse(cbbeThang.Text), 10);
           // //CriteriaOperator filter = new GroupOperator(GroupOperatorType.And,
           // //new BinaryOperator("NgayHetHan", FromDate, BinaryOperatorType.GreaterOrEqual),
           // //new BinaryOperator("NgayHetHan", ToDate, BinaryOperatorType.LessOrEqual));

           // CriteriaOperator binaryFilter = (new OperandProperty("NgayHetHan") >= new OperandValue(FromDate)) & (new OperandProperty("NgayHetHan") < new OperandValue(ToDate));
           //// string filterDisplayText = String.Format("NgayHetHan is between {0:d} and {1:d}", FromDate, ToDate);
           // ColumnFilterInfo dateFilter = new ColumnFilterInfo(binaryFilter.ToString());


           // gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = dateFilter;

            //gridViewNhacNho.ActiveFilterString = (new OperandProperty("NgayHetHan") like 

            //gridViewNhacNho.ActiveFilterString = "[NgayHetHan] like %/08/2016";

            if (cbbeThang.Text == string.Empty || cbbeNam.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập Tháng - Năm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
            }
            else
            {
                string StringTimKiemThangNam = "[NgayHetHan] LIKE '%/" + cbbeThang.Text + "/" + cbbeNam.Text + "'";
                gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo(StringTimKiemThangNam);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo();

            //gridViewNhacNho.ActiveFilterString = "[TinhTrang] = 1";
            if (cbbeTCTK_TrangThai.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập Trạng thái nhắc nhở!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
            }
            else
            {

                //if (cbbeTCTK_TrangThai.Text == "Tất cả")
                //{
                //    gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo();
                //}
                //else if (cbbeTCTK_TrangThai.Text == "Đã xử lý")
                //{
                //    gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo("[TinhTrang] = 1");
                //}
                //else if (cbbeTCTK_TrangThai.Text == "Chưa xử lý")
                //{
                //    gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo("[TinhTrang] = 0");
                //};

                if (cbbeTCTK_TrangThai.Text == "Tất cả")
                {
                    //gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo();
                    gcNhacNho.DataSource = listnn;
                }
                else if (cbbeTCTK_TrangThai.Text == "Đã xử lý")
                {
                    //gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo("[TinhTrang] = 1");
                    gcNhacNho.DataSource = listnn.FindAll(onn => onn.TinhTrang == 1);
                }
                else if (cbbeTCTK_TrangThai.Text == "Chưa xử lý")
                {
                    //gridViewNhacNho.Columns["TinhTrang"].FilterInfo = new ColumnFilterInfo("[TinhTrang] = 0");
                    gcNhacNho.DataSource = listnn.FindAll(onn => onn.TinhTrang == 0);
                };
            }
        }

        private void btnTimKiem3_Click(object sender, EventArgs e)
        {
            DateTime FromDate, ToDate;
            //FromDate = new DateTime(deTCTK_TuNgay.ToString());
            //ToDate = deTCTK_DenNgay.DateTime();
            // //CriteriaOperator filter = new GroupOperator(GroupOperatorType.And,
            // //new BinaryOperator("NgayHetHan", FromDate, BinaryOperatorType.GreaterOrEqual),
            // //new BinaryOperator("NgayHetHan", ToDate, BinaryOperatorType.LessOrEqual));

            // CriteriaOperator binaryFilter = (new OperandProperty("NgayHetHan") >= new OperandValue(FromDate)) & (new OperandProperty("NgayHetHan") < new OperandValue(ToDate));
            //// string filterDisplayText = String.Format("NgayHetHan is between {0:d} and {1:d}", FromDate, ToDate);
            // ColumnFilterInfo dateFilter = new ColumnFilterInfo(binaryFilter.ToString());


            // gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = dateFilter;

            //gridViewNhacNho.ActiveFilterString = (new OperandProperty("NgayHetHan") like 

            //gridViewNhacNho.ActiveFilterString = "[NgayHetHan] like %/08/2016";

            //string StringTimKiemThangNam = "[NgayHetHan] > " + "#" + deTCTK_TuNgay.Text + "#" + " and [NgayHetHan] < " + "#" + deTCTK_DenNgay.Text + "#";
            //string StringTimKiemThangNam = "[NgayHetHan] > " + "05/09/2016" + " and [NgayHetHan] < " + "12/09/2016";
            //string StringTimKiemThangNam = "[NgayHetHan] > #05/09/2016# and [NgayHetHan] < #12/09/2016#";




            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo();
            //gridViewNhacNho.Columns["NgayHetHan"].ClearFilter();

            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo();
            //string StringTimKiemTheoNgay = "[NgayHetHan] >= " + "#" + DateTime.Parse(deTCTK_TuNgay.EditValue.ToString()).ToString("d") + "#" + " and [NgayHetHan] <= " + "#" + DateTime.Parse(deTCTK_DenNgay.Text.ToString()).ToString("d") + "#";
            ////string StringTimKiemThangNam = "[NgayHetHan] >= " + "#" + DateTime.Parse(deTCTK_TuNgay.Text).Date + "#" + " and [NgayHetHan] <= " + "#" + DateTime.Parse(deTCTK_DenNgay.Text).Date + "#";
            ////string StringTimKiemThangNam =  "[NgayHetHan] >= # 09/13/2016 # and [NgayHetHan] <= # 09/27/2016 #";
            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo(StringTimKiemTheoNgay);

            //gridViewNhacNho.ActiveFilterString = "[NgayHetHan] >= #" + DateTime.Parse(deTCTK_TuNgay.EditValue.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "# AND [NgayHetHan] <= #" + DateTime.Parse(deTCTK_DenNgay.EditValue.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "#";
           
            //gridViewNhacNho.ActiveFilterString = String.Format("[NgayHetHan] Between (#{0}#, #{1}#)", DateTime.Parse(deTCTK_DenNgay.EditValue.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture), DateTime.Parse(deTCTK_DenNgay.EditValue.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
            //DateTime dateFrom = DateTime.MinValue;
            //DateTime dateTo = DateTime.MaxValue;
            
            //if (DateTime.TryParse(deTCTK_TuNgay.EditValue.ToString(), out dateFrom) && DateTime.TryParse(deTCTK_DenNgay.EditValue.ToString(), out dateTo))
            //  {  
                
            //    string StringTimKiemThangNam = "[NgayHetHan] >= #" + dateFrom.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "# AND [NgayHetHan] <= #" + dateTo.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "#";
            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo(StringTimKiemThangNam);
            //   }

            //string tungay = ((DateTime)deTCTK_TuNgay.EditValue).ToString("dd/MM/yyyy");
            //string denngay = ((DateTime)deTCTK_DenNgay.EditValue).ToString("dd/MM/yyyy");
            if (deTCTK_TuNgay.Text == string.Empty || deTCTK_DenNgay.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập Từ ngày - Đến ngày!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
            }
            else
            {
                //string tungay = ((DateTime)deTCTK_TuNgay.EditValue).ToString("dd/MM/yyyy");
                //string denngay = ((DateTime)deTCTK_DenNgay.EditValue).ToString("dd/MM/yyyy");
                gcNhacNho.DataSource = listnn.FindAll(onn => DateTime.Parse(onn.NgayHetHan) >= (DateTime)deTCTK_TuNgay.EditValue && DateTime.Parse(onn.NgayHetHan) <= (DateTime)deTCTK_DenNgay.EditValue);

            }
        }
        private void cbbeThang_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            if (txtID.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập ID!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
               
            }
            else
            {
                //gridViewNhacNho.Columns["Id"].FilterInfo = new ColumnFilterInfo("[Id] = " + txtID.Text);
                gcNhacNho.DataSource = listnn.FindAll(onn => onn.Id == int.Parse(txtID.Text));
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (deThoiGianNhac.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập Thời gian nhắc!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
            }
            else
            {
                string ngay = ((DateTime)deThoiGianNhac.EditValue).ToString("dd/MM/yyyy");
                gcNhacNho.DataSource = listnn.FindAll(onn => onn.ThoiGianNhac == ngay);
                //gridViewNhacNho.Columns["ThoiGianNhac"].FilterInfo = new ColumnFilterInfo("[ThoiGianNhac] = #" + DateTime.Parse(deThoiGianNhac.EditValue.ToString()).ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture) + "#");
            }
             }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            //gridViewNhacNho.Columns["NgayHetHan"].FilterInfo = new ColumnFilterInfo("[NgayHetHan] = #" + DateTime.Parse(deNgayHetHan.EditValue.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "#");
           
            //if (deNgayHetHan.EditValue.ToString() != "")
            //{
            //    string sNgayHetHan = DateTime.Parse(deNgayHetHan.EditValue.ToString()).ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
            //    listnn = nnCTL.TimNhacNhoTheoNgayHetHan(sNgayHetHan);
            //    gcNhacNho.DataSource = listnn;
            //    gridViewNhacNho.BestFitColumns();
            //}
            //else
            //    LoadData();
            if (deNgayHetHan.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập Ngày hết hạn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcNhacNho.DataSource = listnn;
            }
            else
            {
                string ngay = ((DateTime)deNgayHetHan.EditValue).ToString("dd/MM/yyyy");
                gcNhacNho.DataSource = listnn.FindAll(onn => onn.NgayHetHan == ngay);
            }
        }

        private void grcDotTuyen_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCMTaoMoi_Click(object sender, EventArgs e)
        {
            LoadData();
                       
        }

        //private void gridViewNhacNho_FocusedRowChanged_1(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        //{
        //    int _selectedIndex = ((GridView)sender).FocusedRowHandle;
        //    if (_selectedIndex < listnn.Count)
        //    {
        //        nnSelect = ((GridView)sender).GetRow(_selectedIndex) as NhacNhoDTO;
        //    }
        //    else
        //        if (listnn.Count != 0)
        //            nnSelect = listnn[0];
        //        else
        //            nnSelect = null;

        //    SetStatus(VIEW);
        //}
    }
}