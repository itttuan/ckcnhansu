﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using HerculesCTL;
using HerculesDTO;
using System.Collections.Generic;

namespace HerculesHRMT
{
    public partial class rptDSNhanSuThang : DevExpress.XtraReports.UI.XtraReport
    {        

        ThongTinNhanSuCTL nsCTL = new ThongTinNhanSuCTL();

        public rptDSNhanSuThang()
        {
            InitializeComponent();
        }

        public rptDSNhanSuThang(DateTime time)
        {
            InitializeComponent();
            
            //Tổng hợp trình độ chuyên môn
            HerculesDTO.Utils.GianTiep.Init();
            HerculesDTO.Utils.GV_KiemNhiem.Init();
            HerculesDTO.Utils.GV_TrucTiep.Init();

            List<ThongTinNhanSuDTO> dsNS = nsCTL.GetAll(time);
            this.DataSource = dsNS;

            printReport(time);
        }

        public void printReport(DateTime time)
        {
            lblTitle.Text = "DANH SÁCH NHÂN SỰ TRƯỜNG CĐ KT CAO THẮNG - THÁNG "+time.ToString("MM")+" NĂM "+ time.ToString("yyyy");
            labelNgayLap.Text = string.Format("Ngày {0:dd} tháng {0:MM} năm {0:yyyy}", DateTime.Today);
            lblNguoiLap.Text = "LÊ ĐÌNH KHA";            

            //Tổng hợp trình độ
            //Gián tiếp
            cellGT_TS.Text = Utils.GianTiep.TS.ToString();
            cellGT_ThS.Text = Utils.GianTiep.ThS.ToString();
            cellGT_DH.Text = Utils.GianTiep.DH.ToString();
            cellGT_CD.Text = Utils.GianTiep.CD.ToString();
            cellGT_Khac.Text = Utils.GianTiep.Khac.ToString();
            cellGT_Cong.Text = (Utils.GianTiep.TS + Utils.GianTiep.ThS + Utils.GianTiep.DH + Utils.GianTiep.CD +
                Utils.GianTiep.Khac).ToString();
            cellGT_Nu.Text = Utils.GianTiep.Nu.ToString();

            //Giảng viên
            cellGV_TS.Text = (Utils.GV_KiemNhiem.TS + Utils.GV_TrucTiep.TS).ToString();
            cellGV_ThS.Text = (Utils.GV_KiemNhiem.ThS + Utils.GV_TrucTiep.ThS).ToString();
            cellGV_DH.Text = (Utils.GV_KiemNhiem.DH + Utils.GV_TrucTiep.DH).ToString();
            cellGV_CD.Text = (Utils.GV_KiemNhiem.CD + Utils.GV_TrucTiep.CD).ToString();
            cellGV_Khac.Text = (Utils.GV_KiemNhiem.Khac + Utils.GV_TrucTiep.Khac).ToString();
            cellGV_Cong.Text = (Utils.GV_KiemNhiem.TS + Utils.GV_KiemNhiem.ThS + Utils.GV_KiemNhiem.DH +
                Utils.GV_KiemNhiem.CD + Utils.GV_KiemNhiem.Khac + Utils.GV_TrucTiep.TS + Utils.GV_TrucTiep.ThS + 
                Utils.GV_TrucTiep.DH + Utils.GV_TrucTiep.CD + Utils.GV_TrucTiep.Khac).ToString();
            cellGV_Nu.Text = (Utils.GV_KiemNhiem.Nu + Utils.GV_TrucTiep.Nu).ToString();
            
            //Kiêm nhiệm
            cellGV_KN_TS.Text = Utils.GV_KiemNhiem.TS.ToString();
            cellGV_KN_ThS.Text = Utils.GV_KiemNhiem.ThS.ToString();
            cellGV_KN_DH.Text = Utils.GV_KiemNhiem.DH.ToString();
            cellGV_KN_CD.Text = Utils.GV_KiemNhiem.CD.ToString();
            cellGV_KN_Khac.Text = Utils.GV_KiemNhiem.Khac.ToString();
            cellGV_KN_Cong.Text = (Utils.GV_KiemNhiem.TS + Utils.GV_KiemNhiem.ThS + Utils.GV_KiemNhiem.DH + 
                Utils.GV_KiemNhiem.CD + Utils.GV_KiemNhiem.Khac).ToString();
            cellGV_KN_Nu.Text = Utils.GV_KiemNhiem.Nu.ToString();

            //Trực tiếp
            cellGV_TT_TS.Text = Utils.GV_TrucTiep.TS.ToString();
            cellGV_TT_ThS.Text = Utils.GV_TrucTiep.ThS.ToString();
            cellGV_TT_DH.Text = Utils.GV_TrucTiep.DH.ToString();
            cellGV_TT_CD.Text = Utils.GV_TrucTiep.CD.ToString();
            cellGV_TT_Khac.Text = Utils.GV_TrucTiep.Khac.ToString();
            cellGV_TT_Cong.Text = (Utils.GV_TrucTiep.TS + Utils.GV_TrucTiep.ThS + Utils.GV_TrucTiep.DH +
                Utils.GV_TrucTiep.CD + Utils.GV_TrucTiep.Khac).ToString();
            cellGV_TT_Nu.Text = Utils.GV_TrucTiep.Nu.ToString();

            //Tổng cộng
            cellTC_TS.Text = (Utils.GianTiep.TS + Utils.GV_KiemNhiem.TS + Utils.GV_TrucTiep.TS).ToString();
            cellTC_ThS.Text = (Utils.GianTiep.ThS + Utils.GV_KiemNhiem.ThS + Utils.GV_TrucTiep.ThS).ToString();
            cellTC_DH.Text = (Utils.GianTiep.DH + Utils.GV_KiemNhiem.DH + Utils.GV_TrucTiep.DH).ToString();
            cellTC_CD.Text = (Utils.GianTiep.CD + Utils.GV_KiemNhiem.CD + Utils.GV_TrucTiep.CD).ToString();
            cellTC_Khac.Text = (Utils.GianTiep.Khac + Utils.GV_KiemNhiem.Khac + Utils.GV_TrucTiep.Khac).ToString();
            cellTC_Cong.Text = (Utils.GianTiep.TS + Utils.GianTiep.ThS + Utils.GianTiep.DH + Utils.GianTiep.CD +
                Utils.GianTiep.Khac + Utils.GV_KiemNhiem.TS + Utils.GV_KiemNhiem.ThS + Utils.GV_KiemNhiem.DH +
                Utils.GV_KiemNhiem.CD + Utils.GV_KiemNhiem.Khac + Utils.GV_TrucTiep.TS + Utils.GV_TrucTiep.ThS +
                Utils.GV_TrucTiep.DH + Utils.GV_TrucTiep.CD + Utils.GV_TrucTiep.Khac).ToString();
            cellTC_Nu.Text = (Utils.GianTiep.Nu + Utils.GV_KiemNhiem.Nu + Utils.GV_TrucTiep.Nu).ToString();
        }

    }
}
