﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucLoaiHopDong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.gcLoaiHopDong = new DevExpress.XtraGrid.GridControl();
            this.gridViewLoaiHopDong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaLoaiHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiHopDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoaiTamTuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtLoaiHopDong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cheTamTuyen = new DevExpress.XtraEditors.CheckEdit();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcLoaiHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoaiHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoaiHopDong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheTamTuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(503, 172);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(410, 172);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 4;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // gcLoaiHopDong
            // 
            this.gcLoaiHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLoaiHopDong.Location = new System.Drawing.Point(2, 2);
            this.gcLoaiHopDong.MainView = this.gridViewLoaiHopDong;
            this.gcLoaiHopDong.Name = "gcLoaiHopDong";
            this.gcLoaiHopDong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcLoaiHopDong.Size = new System.Drawing.Size(781, 296);
            this.gcLoaiHopDong.TabIndex = 3;
            this.gcLoaiHopDong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLoaiHopDong});
            // 
            // gridViewLoaiHopDong
            // 
            this.gridViewLoaiHopDong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaLoaiHopDong,
            this.gridColLoaiHopDong,
            this.gridColGhiChu,
            this.gridColLoaiTamTuyen});
            this.gridViewLoaiHopDong.GridControl = this.gcLoaiHopDong;
            this.gridViewLoaiHopDong.GroupPanelText = " ";
            this.gridViewLoaiHopDong.Name = "gridViewLoaiHopDong";
            this.gridViewLoaiHopDong.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewLoaiHopDong_FocusedRowChanged);
            // 
            // gridColMaLoaiHopDong
            // 
            this.gridColMaLoaiHopDong.Caption = "Mã Loại Hợp Đồng";
            this.gridColMaLoaiHopDong.FieldName = "MaLoaiHD";
            this.gridColMaLoaiHopDong.Name = "gridColMaLoaiHopDong";
            // 
            // gridColLoaiHopDong
            // 
            this.gridColLoaiHopDong.Caption = "Loại Hợp Đồng";
            this.gridColLoaiHopDong.FieldName = "TenLoaiHD";
            this.gridColLoaiHopDong.Name = "gridColLoaiHopDong";
            this.gridColLoaiHopDong.OptionsColumn.AllowEdit = false;
            this.gridColLoaiHopDong.OptionsColumn.ReadOnly = true;
            this.gridColLoaiHopDong.Visible = true;
            this.gridColLoaiHopDong.VisibleIndex = 0;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 1;
            // 
            // gridColLoaiTamTuyen
            // 
            this.gridColLoaiTamTuyen.Caption = "Loại Tạm Tuyển";
            this.gridColLoaiTamTuyen.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColLoaiTamTuyen.FieldName = "LaTamTuyen";
            this.gridColLoaiTamTuyen.Name = "gridColLoaiTamTuyen";
            this.gridColLoaiTamTuyen.OptionsColumn.AllowEdit = false;
            this.gridColLoaiTamTuyen.OptionsColumn.ReadOnly = true;
            this.gridColLoaiTamTuyen.Visible = true;
            this.gridColLoaiTamTuyen.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(319, 172);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 3;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtLoaiHopDong
            // 
            this.txtLoaiHopDong.Location = new System.Drawing.Point(162, 74);
            this.txtLoaiHopDong.Name = "txtLoaiHopDong";
            this.txtLoaiHopDong.Size = new System.Drawing.Size(269, 20);
            this.txtLoaiHopDong.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(59, 77);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(72, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Loại Hợp đồng:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(291, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(199, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Loại Hợp Đồng";
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cheTamTuyen);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtLoaiHopDong);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 202);
            this.panelControl1.TabIndex = 5;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(162, 100);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(521, 49);
            this.meGhiChu.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(59, 102);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 13);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Ghi chú:";
            // 
            // cheTamTuyen
            // 
            this.cheTamTuyen.Location = new System.Drawing.Point(548, 71);
            this.cheTamTuyen.Name = "cheTamTuyen";
            this.cheTamTuyen.Properties.Caption = "Loại Tạm Tuyển";
            this.cheTamTuyen.Size = new System.Drawing.Size(135, 19);
            this.cheTamTuyen.TabIndex = 1;
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(596, 172);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 6;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcLoaiHopDong);
            this.panelControl3.Location = new System.Drawing.Point(0, 218);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 300);
            this.panelControl3.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 10;
            // 
            // ucLoaiHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucLoaiHopDong";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucLoaiHopDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcLoaiHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoaiHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoaiHopDong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheTamTuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private GridControl gcLoaiHopDong;
        private GridView gridViewLoaiHopDong;
        private GridColumn gridColMaLoaiHopDong;
        private GridColumn gridColLoaiHopDong;
        private SimpleButton btnThem;
        private TextEdit txtLoaiHopDong;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridColumn gridColTenDayDu;
        private PanelControl panelControl1;
        private SimpleButton btnXoa;
        private GridColumn gridColTenVietTat;
        private PanelControl panelControl3;
        private PanelControl panelControl2;
        private CheckEdit cheTamTuyen;
        private GridColumn gridColGhiChu;
        private GridColumn gridColLoaiTamTuyen;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private MemoEdit meGhiChu;
        private LabelControl labelControl3;
    }
}
