﻿using System;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    public partial class ucNVBaoHiem : XtraUserControl
    {
        string strMaNV = "";
        NhanVienDTO nhanvienDTO = new NhanVienDTO();
        BaoHiemDTO baohiemDTO = new BaoHiemDTO();
        BaoHiemCTL baohiemCTL = new BaoHiemCTL();
        NhanVienCTL nvCTL = new NhanVienCTL();
        Common.STATUS _statusNV;
        Common.STATUS _statusBHYT;
        public ucNVBaoHiem()
        {
            InitializeComponent();
        }
        
        public void SetIDNhanVien (string Id, Common.STATUS status)
        {
            nhanvienDTO = (new NhanVienCTL()).TimNhanVienTheoId(Id);
            baohiemDTO = new BaoHiemDTO();
            strMaNV = nhanvienDTO.MaNV;
            _statusBHYT = status;
            _statusNV = status;
            BinddingNVData();
            BinddingBHYTData(baohiemDTO);
            SetBHXHStatusComponents();
            SetStatusComponents();
            LoadDanhSach();

        }

        private void BinddingBHYTData(BaoHiemDTO baohiem)
        {
            txtSoBHYT.EditValue = baohiem.SoSo;
            deNgayBHYT.Text = baohiem.NgayCap;
            txtNoiKham.EditValue = baohiem.NoiDKKCB;
        }

        private void BinddingNVData()
        {
            txtMSThue.EditValue = nhanvienDTO.MaSoThue;
            txtSoBHXH.EditValue = nhanvienDTO.SoBHXH;
            txtTKNH.EditValue = nhanvienDTO.SoTaiKhoan;
            txtATM.EditValue = nhanvienDTO.SoATM;
        }

        private void LoadDanhSach()
        {
            gridBHYT.DataSource = baohiemCTL.DanhSachBHYTNhanVien(strMaNV);
        }

        private void SetNVReadOnly(bool read)
        {
            txtMSThue.Properties.ReadOnly = read;
            txtSoBHXH.Properties.ReadOnly = read;
            txtTKNH.Properties.ReadOnly = read;
            txtATM.Properties.ReadOnly = read;
        }

        private void SetReadOnly(bool read)
        {
            txtSoBHYT.Properties.ReadOnly = read;
            txtNoiKham.Properties.ReadOnly = read;
            deNgayBHYT.Properties.ReadOnly = read;
        }

        private void SetBHXHStatusComponents()
        {
            switch (_statusNV)
            {
                case Common.STATUS.VIEW:
                    SetNVReadOnly(true);
                    if (nhanvienDTO.Id > 0)
                    {
                        btnPg3BHXHSua.Enabled = true;
                    }
                    else
                    {
                        btnPg3BHXHSua.Enabled = false;
                    }
                    btnPg3BHXHThem.Enabled = false;
                    break;
                case Common.STATUS.EDIT:
                    SetNVReadOnly(false);
                    btnPg3BHXHSua.Enabled = false;
                    btnPg3BHXHThem.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetStatusComponents()
        {
            switch (_statusBHYT)
            {
                case Common.STATUS.NEW:
                    BinddingBHYTData(new BaoHiemDTO());
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    SetReadOnly(false);
                    break;
                case Common.STATUS.VIEW:
                    SetReadOnly(true);
                    if (baohiemDTO.Id > 0)
                    {
                        btnPg3Sua.Enabled = true;
                    }
                    else
                    {
                        btnPg3Sua.Enabled = false;
                    }
                    btnPg3Them.Enabled = false;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                case Common.STATUS.EDIT:
                    SetReadOnly(false);
                    btnPg3Sua.Enabled = false;
                    btnPg3Them.Enabled = true;
                    btnPg3TaoMoi.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void btnPg3BHXHSua_Click(object sender, EventArgs e)
        {
            if (nhanvienDTO.Id > 0)
            {
                _statusNV = Common.STATUS.EDIT;
                SetBHXHStatusComponents();
            }
        }

        public void GetBHXHDataOncomponents()
        {
            nhanvienDTO.MaSoThue = txtMSThue.Text;
            nhanvienDTO.SoBHXH = txtSoBHXH.Text;
            nhanvienDTO.SoTaiKhoan = txtTKNH.Text;
            nhanvienDTO.SoATM = txtATM.Text;
        }

        private void btnPg3BHXHThem_Click(object sender, EventArgs e)
        {
            GetBHXHDataOncomponents();
            bool kq = nvCTL.CapNhatBHXHTKNganhang(nhanvienDTO);
            if (kq)
            {
                XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _statusNV = Common.STATUS.VIEW;
                SetBHXHStatusComponents();
            }
            else
            {
                XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool GetDataOncomponentsAndCheck()
        {
            if (String.IsNullOrEmpty(txtSoBHYT.Text.Trim()))
                return false;
            baohiemDTO.MaNV = strMaNV;
            baohiemDTO.SoSo = txtSoBHYT.Text;
            if (deNgayBHYT.EditValue != null)
            {
                DateTime ngaycap = DateTime.Parse(deNgayBHYT.EditValue.ToString());
                baohiemDTO.NgayCap = ngaycap.ToString("yyyy-MM-dd");
            }
            else
            {
                baohiemDTO.NgayCap = null;
            }
            baohiemDTO.NoiDKKCB = txtNoiKham.Text;
            baohiemDTO.LaBHXH = false;
            return true;
        }

        private void btnPg3TaoMoi_Click(object sender, EventArgs e)
        {
            baohiemDTO = new BaoHiemDTO();
            _statusBHYT = Common.STATUS.NEW;
            SetStatusComponents();
        }

        private void btnPg3Them_Click(object sender, EventArgs e)
        {
            if (GetDataOncomponentsAndCheck())
            {
                bool kq = false;
                if (_statusBHYT == Common.STATUS.NEW)
                {
                    kq = baohiemCTL.LuuBaoHiem(baohiemDTO);
                }
                if (_statusBHYT == Common.STATUS.EDIT)
                {
                    kq = baohiemCTL.CapNhatBaoHiem(baohiemDTO);
                }

                if (kq)
                {
                    XtraMessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _statusBHYT = Common.STATUS.VIEW;
                    LoadDanhSach();
                    SetStatusComponents();
                }
                else
                {
                    XtraMessageBox.Show("Lưu không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                XtraMessageBox.Show("Vui lòng nhập các thông tin bắt buộc (*)", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPg3Sua_Click(object sender, EventArgs e)
        {
            if (baohiemDTO.Id > 0)
            {
                _statusBHYT = Common.STATUS.EDIT;
                SetStatusComponents();
            }
        }

        private void btnPg3Xoa_Click(object sender, EventArgs e)
        {
            if (baohiemDTO.Id > 0)
            {
                DialogResult result = XtraMessageBox.Show("Quý thầy/cô vui lòng xác nhận xóa", "Thông tin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (baohiemCTL.XoaBaoHiem(baohiemDTO.Id.ToString()))
                    {
                        XtraMessageBox.Show("xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSach();
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void gridViewBHYT_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                baohiemDTO = ((GridView)sender).GetRow(e.FocusedRowHandle) as BaoHiemDTO;
                if (baohiemDTO == null) baohiemDTO = new BaoHiemDTO();
                BinddingBHYTData(baohiemDTO);
                _statusBHYT = Common.STATUS.VIEW;
                SetStatusComponents();
            }
        }

        private void gridViewBHYT_RowClick(object sender, RowClickEventArgs e)
        {
            baohiemDTO = ((GridView)sender).GetRow(e.RowHandle) as BaoHiemDTO;
            if (baohiemDTO == null) baohiemDTO = new BaoHiemDTO();
            BinddingBHYTData(baohiemDTO);
            _statusBHYT = Common.STATUS.VIEW;
            SetStatusComponents();
        }
    }
}
