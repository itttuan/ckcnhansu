﻿namespace HerculesHRMT
{
    partial class frmNhacNho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.grcDotTuyen = new DevExpress.XtraEditors.GroupControl();
            this.btnCMTaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnTimKiemNgayHetHan = new DevExpress.XtraEditors.SimpleButton();
            this.btnTimKiemThoiGianNhac = new DevExpress.XtraEditors.SimpleButton();
            this.btn_TimKiemID = new DevExpress.XtraEditors.SimpleButton();
            this.deTCTK_DenNgay = new DevExpress.XtraEditors.DateEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.deTCTK_TuNgay = new DevExpress.XtraEditors.DateEdit();
            this.btnTimKiem3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnTimKiem2 = new DevExpress.XtraEditors.SimpleButton();
            this.cbbeTCTK_TrangThai = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnTimKiem1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cbbeThang = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbbeNam = new DevExpress.XtraEditors.ComboBoxEdit();
            this.deNgayHetHan = new DevExpress.XtraEditors.DateEdit();
            this.deThoiGianNhac = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtID = new DevExpress.XtraEditors.TextEdit();
            this.gridColThang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayGio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColMaDot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewNhacNho = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColThoiGianNhac = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColNgayKetThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTinhTrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCheckComboEdit_XuLy = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.gridColXuLy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reButtonEdit_XuLy = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gcNhacNho = new DevExpress.XtraGrid.GridControl();
            this.repCheckEdit_XuLy = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repRadioGroup_XuLy = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.reComboboxEdit_XuLy = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::HerculesHRMT.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).BeginInit();
            this.grcDotTuyen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_DenNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_DenNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_TuNgay.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_TuNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeTCTK_TrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeThang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeNam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNhacNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckComboEdit_XuLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reButtonEdit_XuLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNhacNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckEdit_XuLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRadioGroup_XuLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reComboboxEdit_XuLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(89, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(11, 15);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "ID";
            this.labelControl4.Visible = false;
            // 
            // grcDotTuyen
            // 
            this.grcDotTuyen.Controls.Add(this.btnCMTaoMoi);
            this.grcDotTuyen.Controls.Add(this.btnTimKiemNgayHetHan);
            this.grcDotTuyen.Controls.Add(this.btnTimKiemThoiGianNhac);
            this.grcDotTuyen.Controls.Add(this.btn_TimKiemID);
            this.grcDotTuyen.Controls.Add(this.deTCTK_DenNgay);
            this.grcDotTuyen.Controls.Add(this.labelControl9);
            this.grcDotTuyen.Controls.Add(this.labelControl8);
            this.grcDotTuyen.Controls.Add(this.deTCTK_TuNgay);
            this.grcDotTuyen.Controls.Add(this.btnTimKiem3);
            this.grcDotTuyen.Controls.Add(this.btnTimKiem2);
            this.grcDotTuyen.Controls.Add(this.cbbeTCTK_TrangThai);
            this.grcDotTuyen.Controls.Add(this.labelControl3);
            this.grcDotTuyen.Controls.Add(this.btnTimKiem1);
            this.grcDotTuyen.Controls.Add(this.labelControl2);
            this.grcDotTuyen.Controls.Add(this.labelControl7);
            this.grcDotTuyen.Controls.Add(this.cbbeThang);
            this.grcDotTuyen.Controls.Add(this.cbbeNam);
            this.grcDotTuyen.Controls.Add(this.deNgayHetHan);
            this.grcDotTuyen.Controls.Add(this.deThoiGianNhac);
            this.grcDotTuyen.Controls.Add(this.labelControl5);
            this.grcDotTuyen.Controls.Add(this.labelControl4);
            this.grcDotTuyen.Controls.Add(this.labelControl6);
            this.grcDotTuyen.Controls.Add(this.txtID);
            this.grcDotTuyen.Location = new System.Drawing.Point(62, 7);
            this.grcDotTuyen.Name = "grcDotTuyen";
            this.grcDotTuyen.Size = new System.Drawing.Size(1025, 115);
            this.grcDotTuyen.TabIndex = 18;
            this.grcDotTuyen.Text = "Tìm kiếm";
            this.grcDotTuyen.Paint += new System.Windows.Forms.PaintEventHandler(this.grcDotTuyen_Paint);
            // 
            // btnCMTaoMoi
            // 
            this.btnCMTaoMoi.Location = new System.Drawing.Point(124, 27);
            this.btnCMTaoMoi.Name = "btnCMTaoMoi";
            this.btnCMTaoMoi.Size = new System.Drawing.Size(121, 20);
            this.btnCMTaoMoi.TabIndex = 34;
            this.btnCMTaoMoi.Text = "Xem tất cả nhắc nhở";
            this.btnCMTaoMoi.Click += new System.EventHandler(this.btnCMTaoMoi_Click);
            // 
            // btnTimKiemNgayHetHan
            // 
            this.btnTimKiemNgayHetHan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemNgayHetHan.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemNgayHetHan.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemNgayHetHan.Location = new System.Drawing.Point(252, 88);
            this.btnTimKiemNgayHetHan.Name = "btnTimKiemNgayHetHan";
            this.btnTimKiemNgayHetHan.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemNgayHetHan.TabIndex = 33;
            this.btnTimKiemNgayHetHan.ToolTip = "Tìm theo Ngày hết hạn";
            this.btnTimKiemNgayHetHan.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // btnTimKiemThoiGianNhac
            // 
            this.btnTimKiemThoiGianNhac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiemThoiGianNhac.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiemThoiGianNhac.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiemThoiGianNhac.Location = new System.Drawing.Point(252, 57);
            this.btnTimKiemThoiGianNhac.Name = "btnTimKiemThoiGianNhac";
            this.btnTimKiemThoiGianNhac.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiemThoiGianNhac.TabIndex = 32;
            this.btnTimKiemThoiGianNhac.ToolTip = "Tìm theo Ngày nhắc";
            this.btnTimKiemThoiGianNhac.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btn_TimKiemID
            // 
            this.btn_TimKiemID.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_TimKiemID.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btn_TimKiemID.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_TimKiemID.Location = new System.Drawing.Point(252, 28);
            this.btn_TimKiemID.Name = "btn_TimKiemID";
            this.btn_TimKiemID.Size = new System.Drawing.Size(32, 20);
            this.btn_TimKiemID.TabIndex = 31;
            this.btn_TimKiemID.ToolTip = "Tìm theo ID";
            this.btn_TimKiemID.Visible = false;
            this.btn_TimKiemID.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // deTCTK_DenNgay
            // 
            this.deTCTK_DenNgay.EditValue = null;
            this.deTCTK_DenNgay.Location = new System.Drawing.Point(523, 88);
            this.deTCTK_DenNgay.Name = "deTCTK_DenNgay";
            this.deTCTK_DenNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTCTK_DenNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deTCTK_DenNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTCTK_DenNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deTCTK_DenNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTCTK_DenNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deTCTK_DenNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deTCTK_DenNgay.Size = new System.Drawing.Size(100, 20);
            this.deTCTK_DenNgay.TabIndex = 30;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Location = new System.Drawing.Point(465, 93);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(53, 15);
            this.labelControl9.TabIndex = 29;
            this.labelControl9.Text = "Đến ngày";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(309, 93);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(46, 15);
            this.labelControl8.TabIndex = 28;
            this.labelControl8.Text = "Từ ngày";
            // 
            // deTCTK_TuNgay
            // 
            this.deTCTK_TuNgay.EditValue = null;
            this.deTCTK_TuNgay.Location = new System.Drawing.Point(361, 88);
            this.deTCTK_TuNgay.Name = "deTCTK_TuNgay";
            this.deTCTK_TuNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTCTK_TuNgay.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deTCTK_TuNgay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTCTK_TuNgay.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deTCTK_TuNgay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deTCTK_TuNgay.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deTCTK_TuNgay.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deTCTK_TuNgay.Size = new System.Drawing.Size(100, 20);
            this.deTCTK_TuNgay.TabIndex = 27;
            // 
            // btnTimKiem3
            // 
            this.btnTimKiem3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiem3.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiem3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiem3.Location = new System.Drawing.Point(630, 88);
            this.btnTimKiem3.Name = "btnTimKiem3";
            this.btnTimKiem3.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiem3.TabIndex = 26;
            this.btnTimKiem3.ToolTip = "Tìm theo Từ ngày - Đến ngày";
            this.btnTimKiem3.Click += new System.EventHandler(this.btnTimKiem3_Click);
            // 
            // btnTimKiem2
            // 
            this.btnTimKiem2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiem2.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiem2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiem2.Location = new System.Drawing.Point(630, 27);
            this.btnTimKiem2.Name = "btnTimKiem2";
            this.btnTimKiem2.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiem2.TabIndex = 25;
            this.btnTimKiem2.ToolTip = "Tìm theo Trạng thái nhắc nhở";
            this.btnTimKiem2.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cbbeTCTK_TrangThai
            // 
            this.cbbeTCTK_TrangThai.Location = new System.Drawing.Point(523, 27);
            this.cbbeTCTK_TrangThai.Name = "cbbeTCTK_TrangThai";
            this.cbbeTCTK_TrangThai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeTCTK_TrangThai.Size = new System.Drawing.Size(100, 20);
            this.cbbeTCTK_TrangThai.TabIndex = 24;
            this.cbbeTCTK_TrangThai.SelectedIndexChanged += new System.EventHandler(this.cbbeTrangThai_SelectedIndexChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(405, 32);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(113, 15);
            this.labelControl3.TabIndex = 23;
            this.labelControl3.Text = "Trạng thái nhắc nhở";
            // 
            // btnTimKiem1
            // 
            this.btnTimKiem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTimKiem1.Image = global::HerculesHRMT.Properties.Resources.search_icon__1_;
            this.btnTimKiem1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnTimKiem1.Location = new System.Drawing.Point(630, 57);
            this.btnTimKiem1.Name = "btnTimKiem1";
            this.btnTimKiem1.Size = new System.Drawing.Size(32, 20);
            this.btnTimKiem1.TabIndex = 21;
            this.btnTimKiem1.ToolTip = "Tìm theo Tháng - Năm";
            this.btnTimKiem1.Click += new System.EventHandler(this.btnTimKiemMaNV_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(320, 62);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 15);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Tháng";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(492, 62);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(26, 15);
            this.labelControl7.TabIndex = 20;
            this.labelControl7.Text = "Năm";
            // 
            // cbbeThang
            // 
            this.cbbeThang.Location = new System.Drawing.Point(361, 57);
            this.cbbeThang.Name = "cbbeThang";
            this.cbbeThang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeThang.Size = new System.Drawing.Size(100, 20);
            this.cbbeThang.TabIndex = 18;
            this.cbbeThang.SelectedIndexChanged += new System.EventHandler(this.cbbeThang_SelectedIndexChanged);
            // 
            // cbbeNam
            // 
            this.cbbeNam.Location = new System.Drawing.Point(523, 57);
            this.cbbeNam.Name = "cbbeNam";
            this.cbbeNam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbeNam.Size = new System.Drawing.Size(100, 20);
            this.cbbeNam.TabIndex = 19;
            // 
            // deNgayHetHan
            // 
            this.deNgayHetHan.EditValue = null;
            this.deNgayHetHan.Location = new System.Drawing.Point(145, 88);
            this.deNgayHetHan.Name = "deNgayHetHan";
            this.deNgayHetHan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgayHetHan.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deNgayHetHan.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayHetHan.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deNgayHetHan.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deNgayHetHan.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deNgayHetHan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deNgayHetHan.Size = new System.Drawing.Size(100, 20);
            this.deNgayHetHan.TabIndex = 5;
            // 
            // deThoiGianNhac
            // 
            this.deThoiGianNhac.EditValue = null;
            this.deThoiGianNhac.Location = new System.Drawing.Point(145, 57);
            this.deThoiGianNhac.Name = "deThoiGianNhac";
            this.deThoiGianNhac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deThoiGianNhac.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deThoiGianNhac.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deThoiGianNhac.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deThoiGianNhac.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deThoiGianNhac.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deThoiGianNhac.Size = new System.Drawing.Size(100, 20);
            this.deThoiGianNhac.TabIndex = 4;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(67, 93);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(73, 15);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Ngày hết hạn";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(81, 62);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(59, 15);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Ngày nhắc";
            // 
            // txtID
            // 
            this.txtID.EditValue = "";
            this.txtID.Location = new System.Drawing.Point(110, 28);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 1;
            this.txtID.Visible = false;
            // 
            // gridColThang
            // 
            this.gridColThang.Caption = "Loại nhắc nhở";
            this.gridColThang.FieldName = "LoaiNhacNho";
            this.gridColThang.Name = "gridColThang";
            this.gridColThang.OptionsColumn.AllowEdit = false;
            this.gridColThang.OptionsColumn.FixedWidth = true;
            this.gridColThang.OptionsColumn.ReadOnly = true;
            this.gridColThang.Visible = true;
            this.gridColThang.VisibleIndex = 1;
            this.gridColThang.Width = 25;
            // 
            // gridColNam
            // 
            this.gridColNam.Caption = "Nội dung";
            this.gridColNam.FieldName = "NoiDung";
            this.gridColNam.Name = "gridColNam";
            this.gridColNam.OptionsColumn.AllowEdit = false;
            this.gridColNam.OptionsColumn.FixedWidth = true;
            this.gridColNam.OptionsColumn.ReadOnly = true;
            this.gridColNam.Visible = true;
            this.gridColNam.VisibleIndex = 2;
            this.gridColNam.Width = 70;
            // 
            // gridColNgayGio
            // 
            this.gridColNgayGio.Caption = "Ghi chú";
            this.gridColNgayGio.FieldName = "GhiChu";
            this.gridColNgayGio.Name = "gridColNgayGio";
            this.gridColNgayGio.OptionsColumn.AllowEdit = false;
            this.gridColNgayGio.OptionsColumn.FixedWidth = true;
            this.gridColNgayGio.OptionsColumn.ReadOnly = true;
            this.gridColNgayGio.Visible = true;
            this.gridColNgayGio.VisibleIndex = 3;
            this.gridColNgayGio.Width = 40;
            // 
            // gridColMaDot
            // 
            this.gridColMaDot.Caption = "ID";
            this.gridColMaDot.FieldName = "Id";
            this.gridColMaDot.MinWidth = 10;
            this.gridColMaDot.Name = "gridColMaDot";
            this.gridColMaDot.OptionsColumn.AllowEdit = false;
            this.gridColMaDot.OptionsColumn.FixedWidth = true;
            this.gridColMaDot.OptionsColumn.ReadOnly = true;
            this.gridColMaDot.Visible = true;
            this.gridColMaDot.VisibleIndex = 0;
            this.gridColMaDot.Width = 10;
            // 
            // gridViewNhacNho
            // 
            this.gridViewNhacNho.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaDot,
            this.gridColThang,
            this.gridColNam,
            this.gridColNgayGio,
            this.gridColPhong,
            this.gridColThoiGianNhac,
            this.gridColNgayKetThuc,
            this.gridColTinhTrang,
            this.gridColXuLy});
            this.gridViewNhacNho.GridControl = this.gcNhacNho;
            this.gridViewNhacNho.GroupPanelText = " ";
            this.gridViewNhacNho.Name = "gridViewNhacNho";
            this.gridViewNhacNho.OptionsView.ShowAutoFilterRow = true;
            this.gridViewNhacNho.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewNhacNho_FocusedRowChanged);
            // 
            // gridColPhong
            // 
            this.gridColPhong.Caption = "Số quyết định";
            this.gridColPhong.FieldName = "SoQuyetDinh";
            this.gridColPhong.Name = "gridColPhong";
            this.gridColPhong.OptionsColumn.AllowEdit = false;
            this.gridColPhong.OptionsColumn.FixedWidth = true;
            this.gridColPhong.OptionsColumn.ReadOnly = true;
            this.gridColPhong.Visible = true;
            this.gridColPhong.VisibleIndex = 4;
            this.gridColPhong.Width = 25;
            // 
            // gridColThoiGianNhac
            // 
            this.gridColThoiGianNhac.Caption = "Thời gian nhắc";
            this.gridColThoiGianNhac.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColThoiGianNhac.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColThoiGianNhac.FieldName = "ThoiGianNhac";
            this.gridColThoiGianNhac.Name = "gridColThoiGianNhac";
            this.gridColThoiGianNhac.OptionsColumn.FixedWidth = true;
            this.gridColThoiGianNhac.OptionsColumn.ReadOnly = true;
            this.gridColThoiGianNhac.Visible = true;
            this.gridColThoiGianNhac.VisibleIndex = 5;
            this.gridColThoiGianNhac.Width = 22;
            // 
            // gridColNgayKetThuc
            // 
            this.gridColNgayKetThuc.Caption = "Ngày hết hạn";
            this.gridColNgayKetThuc.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColNgayKetThuc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColNgayKetThuc.FieldName = "NgayHetHan";
            this.gridColNgayKetThuc.Name = "gridColNgayKetThuc";
            this.gridColNgayKetThuc.OptionsColumn.AllowEdit = false;
            this.gridColNgayKetThuc.OptionsColumn.FixedWidth = true;
            this.gridColNgayKetThuc.OptionsColumn.ReadOnly = true;
            this.gridColNgayKetThuc.Visible = true;
            this.gridColNgayKetThuc.VisibleIndex = 6;
            this.gridColNgayKetThuc.Width = 22;
            // 
            // gridColTinhTrang
            // 
            this.gridColTinhTrang.Caption = "Trạng thái nhắc nhở";
            this.gridColTinhTrang.ColumnEdit = this.repCheckComboEdit_XuLy;
            this.gridColTinhTrang.FieldName = "TinhTrang";
            this.gridColTinhTrang.Name = "gridColTinhTrang";
            this.gridColTinhTrang.OptionsColumn.AllowEdit = false;
            this.gridColTinhTrang.OptionsColumn.FixedWidth = true;
            this.gridColTinhTrang.OptionsColumn.ReadOnly = true;
            this.gridColTinhTrang.Visible = true;
            this.gridColTinhTrang.VisibleIndex = 7;
            this.gridColTinhTrang.Width = 30;
            // 
            // repCheckComboEdit_XuLy
            // 
            this.repCheckComboEdit_XuLy.AutoHeight = false;
            this.repCheckComboEdit_XuLy.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCheckComboEdit_XuLy.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("0", "Chưa xử lý"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1", "Đã xử lý")});
            this.repCheckComboEdit_XuLy.Name = "repCheckComboEdit_XuLy";
            // 
            // gridColXuLy
            // 
            this.gridColXuLy.Caption = "Thao tác";
            this.gridColXuLy.ColumnEdit = this.reButtonEdit_XuLy;
            this.gridColXuLy.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.gridColXuLy.MinWidth = 10;
            this.gridColXuLy.Name = "gridColXuLy";
            this.gridColXuLy.OptionsColumn.FixedWidth = true;
            this.gridColXuLy.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColXuLy.Visible = true;
            this.gridColXuLy.VisibleIndex = 8;
            this.gridColXuLy.Width = 15;
            // 
            // reButtonEdit_XuLy
            // 
            this.reButtonEdit_XuLy.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Xử lý", 10, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::HerculesHRMT.Properties.Resources.hrmt_notepad, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Chuyển trạng thái nhắc nhở", "Chuyển trạng thái", null, false)});
            this.reButtonEdit_XuLy.Name = "reButtonEdit_XuLy";
            this.reButtonEdit_XuLy.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.reButtonEdit_XuLy_ButtonClick);
            // 
            // gcNhacNho
            // 
            this.gcNhacNho.Location = new System.Drawing.Point(4, 121);
            this.gcNhacNho.MainView = this.gridViewNhacNho;
            this.gcNhacNho.Name = "gcNhacNho";
            this.gcNhacNho.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repCheckEdit_XuLy,
            this.repRadioGroup_XuLy,
            this.repCheckComboEdit_XuLy,
            this.reComboboxEdit_XuLy,
            this.reButtonEdit_XuLy});
            this.gcNhacNho.Size = new System.Drawing.Size(1023, 389);
            this.gcNhacNho.TabIndex = 19;
            this.gcNhacNho.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNhacNho});
            this.gcNhacNho.Click += new System.EventHandler(this.gcNhacNho_Click);
            // 
            // repCheckEdit_XuLy
            // 
            this.repCheckEdit_XuLy.AutoHeight = false;
            this.repCheckEdit_XuLy.AutoWidth = true;
            this.repCheckEdit_XuLy.Name = "repCheckEdit_XuLy";
            this.repCheckEdit_XuLy.ValueChecked = "1";
            this.repCheckEdit_XuLy.ValueUnchecked = "0";
            // 
            // repRadioGroup_XuLy
            // 
            this.repRadioGroup_XuLy.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "da xu ly"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "chua xu ly")});
            this.repRadioGroup_XuLy.Name = "repRadioGroup_XuLy";
            // 
            // reComboboxEdit_XuLy
            // 
            this.reComboboxEdit_XuLy.AutoHeight = false;
            this.reComboboxEdit_XuLy.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reComboboxEdit_XuLy.Items.AddRange(new object[] {
            "đã xử lý ",
            "chưa xử lý"});
            this.reComboboxEdit_XuLy.Name = "reComboboxEdit_XuLy";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.grcDotTuyen);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(-60, -6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1094, 256);
            this.panelControl1.TabIndex = 18;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(395, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(161, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Đợt tuyển dụng";
            // 
            // frmNhacNho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 511);
            this.Controls.Add(this.gcNhacNho);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmNhacNho";
            this.Text = "Nhắc nhở";
            this.Load += new System.EventHandler(this.frmNhacNho2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcDotTuyen)).EndInit();
            this.grcDotTuyen.ResumeLayout(false);
            this.grcDotTuyen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_DenNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_DenNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_TuNgay.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTCTK_TuNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeTCTK_TrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeThang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbeNam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgayHetHan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deThoiGianNhac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNhacNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckComboEdit_XuLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reButtonEdit_XuLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNhacNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckEdit_XuLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repRadioGroup_XuLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reComboboxEdit_XuLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl grcDotTuyen;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThang;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayGio;
        private DevExpress.XtraGrid.Columns.GridColumn gridColMaDot;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewNhacNho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColPhong;
        private DevExpress.XtraGrid.Columns.GridColumn gridColNgayKetThuc;
        private DevExpress.XtraGrid.GridControl gcNhacNho;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColThoiGianNhac;
        private DevExpress.XtraEditors.DateEdit deThoiGianNhac;
        private DevExpress.XtraEditors.DateEdit deNgayHetHan;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeThang;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeNam;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTinhTrang;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckEdit_XuLy;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repRadioGroup_XuLy;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repCheckComboEdit_XuLy;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox reComboboxEdit_XuLy;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reButtonEdit_XuLy;
        private DevExpress.XtraGrid.Columns.GridColumn gridColXuLy;
        private DevExpress.XtraEditors.ComboBoxEdit cbbeTCTK_TrangThai;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem2;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem3;
        private DevExpress.XtraEditors.DateEdit deTCTK_DenNgay;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.DateEdit deTCTK_TuNgay;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemNgayHetHan;
        private DevExpress.XtraEditors.SimpleButton btnTimKiemThoiGianNhac;
        private DevExpress.XtraEditors.SimpleButton btn_TimKiemID;
        private DevExpress.XtraEditors.SimpleButton btnCMTaoMoi;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}