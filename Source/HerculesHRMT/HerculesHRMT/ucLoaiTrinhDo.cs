﻿using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
using System;
using DevExpress.XtraGrid.Views.Grid;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace HerculesHRMT
{
    public partial class ucLoaiTrinhDo : XtraUserControl
    {
        LoaiTrinhDoCTL tdCTL = new LoaiTrinhDoCTL();
        List<LoaiTrinhDoDTO> listLoaiTD = new List<LoaiTrinhDoDTO>();
        LoaiTrinhDoDTO tdSelect = null;

        private int _selectedStatus = -1;
        private const int NEW = 0;
        private const int EDIT = 1;
        private const int VIEW = 2;

        public ucLoaiTrinhDo()
        {
            InitializeComponent();
            txtTenVT.Properties.MaxLength = 50;
        }

        public void LoadData()
        {
            listLoaiTD = tdCTL.GetAll();

            gcLoaiTrinhDo.DataSource = listLoaiTD;
            btnLuu.Enabled = false;

        }

        private void BindingData(LoaiTrinhDoDTO tdDTO)
        {
            if (tdDTO != null)
            {
                txtLoaiTrinhDo.Text = tdDTO.TenLoaiTrinhDo;
                txtTenVT.Text = tdDTO.TenVietTat;
                meGhiChu.Text = tdDTO.GhiChu;
            }
        }

        private void SetStatus(int status)
        {
            _selectedStatus = status;

            switch (status)
            {
                case NEW:
                    txtLoaiTrinhDo.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    txtLoaiTrinhDo.Text = string.Empty;
                    txtTenVT.Text = string.Empty;
                    meGhiChu.Text = string.Empty;

                    btnLuu.Enabled = true;

                    _selectedStatus = NEW;
                    break;
                case EDIT:
                    txtLoaiTrinhDo.Properties.ReadOnly = false;
                    txtTenVT.Properties.ReadOnly = false;
                    meGhiChu.Properties.ReadOnly = false;

                    btnLuu.Enabled = true;

                    BindingData(tdSelect);
                    _selectedStatus = EDIT;
                    break;
                case VIEW:
                    txtLoaiTrinhDo.Properties.ReadOnly = true;
                    txtTenVT.Properties.ReadOnly = true;
                    meGhiChu.Properties.ReadOnly = true;

                    btnLuu.Enabled = false;

                    BindingData(tdSelect);
                    _selectedStatus = VIEW;
                    break;
            }
        }

        private bool CheckInputData()
        {
            if (txtLoaiTrinhDo.Text == string.Empty || txtTenVT.Text == string.Empty)
                return false;

            return true;
        }

        private LoaiTrinhDoDTO GetData(LoaiTrinhDoDTO tdDTO)
        {
            string maLoaiTD = "";
            if (_selectedStatus == NEW)
            {
                string sLastIndexNganh = tdCTL.GetMaxMaLoaiTrinhDo();
                if (sLastIndexNganh.CompareTo(string.Empty) != 0)
                {
                    int nLastIndexTG = int.Parse(sLastIndexNganh);
                    maLoaiTD += (nLastIndexTG + 1).ToString();
                }
                else
                    maLoaiTD += "1";

                tdDTO.NgayHieuLuc = DateTime.Now;
                tdDTO.TinhTrang = 0;
            }
            else if (_selectedStatus == EDIT)
            {
                maLoaiTD = tdSelect.MaLoaiTrinhDo;
                tdDTO.NgayHieuLuc = tdSelect.NgayHieuLuc;
                tdDTO.TinhTrang = tdSelect.TinhTrang;
            }
            tdDTO.MaLoaiTrinhDo = maLoaiTD;
            tdDTO.TenLoaiTrinhDo = txtLoaiTrinhDo.Text;
            tdDTO.TenVietTat = txtTenVT.Text;
            tdDTO.GhiChu = meGhiChu.Text;

            return tdDTO;
        }

        private void ucLoaiTrinhDo_Load(object sender, System.EventArgs e)
        {
            LoadData();
            SetStatus(VIEW);
        }

        private void gridViewLoaiTrinhDo_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int _selectedIndex = ((GridView)sender).FocusedRowHandle;
            if (_selectedIndex < listLoaiTD.Count)
            {
                tdSelect = ((GridView)sender).GetRow(_selectedIndex) as LoaiTrinhDoDTO;
            }
            else
                if (listLoaiTD.Count != 0)
                    tdSelect = listLoaiTD[0];
                else
                    tdSelect = null;

            SetStatus(VIEW);
        }

        private void btnThem_Click(object sender, System.EventArgs e)
        {
            SetStatus(NEW);
        }

        private void btnCapNhat_Click(object sender, System.EventArgs e)
        {
            SetStatus(EDIT);
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            if (!CheckInputData())
            {
                XtraMessageBox.Show("Quý thầy/cô vui lòng điền đầy đủ thông tin: \n Tên loại trình độ\n Tên viết tắt", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoaiTrinhDoDTO tdNew = GetData(new LoaiTrinhDoDTO());
            var isSucess = false;
            if (_selectedStatus == NEW)
                isSucess = tdCTL.Save(tdNew);
            else
                isSucess = tdCTL.Update(tdNew);

            if (isSucess)
            {
                XtraMessageBox.Show("Tác vụ thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadData();
                int _selectedIndex = listLoaiTD.IndexOf(listLoaiTD.FirstOrDefault(p => p.MaLoaiTrinhDo == tdNew.MaLoaiTrinhDo));
                gridViewLoaiTrinhDo.FocusedRowHandle = _selectedIndex;
            }
            else
            {
                XtraMessageBox.Show("Tác vụ thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            SetStatus(VIEW);
        }

        private void btnXoa_Click(object sender, System.EventArgs e)
        {
            if (tdSelect != null)
            {
                string warrning = "Quý thầy/cô có chắc muốn xóa loại trình độ " + tdSelect.TenLoaiTrinhDo + " hay không?";

                if (XtraMessageBox.Show(warrning, "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    bool isSucess = tdCTL.Delete(tdSelect);

                    if (isSucess)
                    {
                        XtraMessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        LoadData();
                        if (listLoaiTD.Count != 0)
                            gridViewLoaiTrinhDo.FocusedRowHandle = 0;
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        SetStatus(VIEW);
                    }
                }
            }
        }
    }
}
