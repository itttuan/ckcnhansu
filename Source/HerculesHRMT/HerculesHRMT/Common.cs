﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using HerculesCTL;

namespace HerculesHRMT
{
    public static class Common
    {
        private static NhanVienCTL _nhanVienCtl = new NhanVienCTL();
        
        public enum STATUS { NEW = 0, EDIT, VIEW};

        public static string TaoChuoiMD5(string strInput)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(strInput));
            StringBuilder strMD5 = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                strMD5.Append(data[i].ToString("x2"));
            }
            return strMD5.ToString();
        }

        public static string GenerateStaffCode(string maPhongKhoa, string maBoMon)
        {
            var result = string.Empty;
            var strMaxNvId = _nhanVienCtl.GetMaNvMax(maBoMon, maPhongKhoa);
            var maNvId = strMaxNvId == string.Empty ? 1 : Convert.ToInt32(strMaxNvId.Substring(strMaxNvId.Length - 4)) + 1;

            if (maNvId < 10) result = string.Format("{0}{1}000{2}", maPhongKhoa, maBoMon, maNvId);
            else if (maNvId < 100) result = string.Format("{0}{1}00{2}", maPhongKhoa, maBoMon, maNvId);
            else if (maNvId < 1000) result = string.Format("{0}{1}0{2}", maPhongKhoa, maBoMon, maNvId);
            else if (maNvId < 10000) result = string.Format("{0}{1}{2}", maPhongKhoa, maBoMon, maNvId);

            return result;
        }
    }
}
