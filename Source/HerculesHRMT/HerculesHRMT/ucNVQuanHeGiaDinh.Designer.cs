﻿using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucNVQuanHeGiaDinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.gvPg5QuanHeGiaDinh = new DevExpress.XtraGrid.GridControl();
            this.gridviewQuanHeGiaDinh = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColQHGDID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDHoTen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDLoaiQuanHe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTNVoChong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColQHGDCongViec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDNoiO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDDacDiem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColQHGDGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditXoa = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.chbTNBenVoChong = new DevExpress.XtraEditors.CheckEdit();
            this.dtPg5NgaySinh = new DevExpress.XtraEditors.TextEdit();
            this.btnPg5Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg5Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.cbbPg5LoaiQH = new DevExpress.XtraEditors.LookUpEdit();
            this.btnPg5Them = new DevExpress.XtraEditors.SimpleButton();
            this.btnPg5TaoMoi = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg5DiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtPg5GhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.txtPg5DacDiemLS = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.txtPg5NgheNghiep = new DevExpress.XtraEditors.TextEdit();
            this.txtPg5HoTen = new DevExpress.XtraEditors.TextEdit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPg5QuanHeGiaDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuanHeGiaDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbTNBenVoChong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg5NgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg5LoaiQH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5DiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5GhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5DacDiemLS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5NgheNghiep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5HoTen.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gvPg5QuanHeGiaDinh);
            this.xtraScrollableControl1.Controls.Add(this.groupControl13);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1282, 510);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // gvPg5QuanHeGiaDinh
            // 
            this.gvPg5QuanHeGiaDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPg5QuanHeGiaDinh.Location = new System.Drawing.Point(0, 173);
            this.gvPg5QuanHeGiaDinh.MainView = this.gridviewQuanHeGiaDinh;
            this.gvPg5QuanHeGiaDinh.Name = "gvPg5QuanHeGiaDinh";
            this.gvPg5QuanHeGiaDinh.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditXoa,
            this.repositoryItemCheckEdit1});
            this.gvPg5QuanHeGiaDinh.Size = new System.Drawing.Size(1282, 337);
            this.gvPg5QuanHeGiaDinh.TabIndex = 126;
            this.gvPg5QuanHeGiaDinh.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewQuanHeGiaDinh});
            // 
            // gridviewQuanHeGiaDinh
            // 
            this.gridviewQuanHeGiaDinh.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColQHGDID,
            this.gridColQHGDHoTen,
            this.gridColQHGDLoaiQuanHe,
            this.gridColTNVoChong,
            this.gridColQHGDCongViec,
            this.gridColQHGDNgaySinh,
            this.gridColQHGDNoiO,
            this.gridColQHGDDacDiem,
            this.gridColQHGDGhiChu});
            this.gridviewQuanHeGiaDinh.GridControl = this.gvPg5QuanHeGiaDinh;
            this.gridviewQuanHeGiaDinh.GroupPanelText = "Quan hệ gia đình";
            this.gridviewQuanHeGiaDinh.Name = "gridviewQuanHeGiaDinh";
            this.gridviewQuanHeGiaDinh.OptionsView.ColumnAutoWidth = false;
            this.gridviewQuanHeGiaDinh.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridviewQuanHeGiaDinh_RowClick);
            this.gridviewQuanHeGiaDinh.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridviewQuanHeGiaDinh_FocusedRowChanged);
            // 
            // gridColQHGDID
            // 
            this.gridColQHGDID.Caption = "Mã ID";
            this.gridColQHGDID.FieldName = "Id";
            this.gridColQHGDID.Name = "gridColQHGDID";
            this.gridColQHGDID.OptionsColumn.AllowEdit = false;
            this.gridColQHGDID.OptionsColumn.ReadOnly = true;
            // 
            // gridColQHGDHoTen
            // 
            this.gridColQHGDHoTen.Caption = "Họ tên";
            this.gridColQHGDHoTen.FieldName = "HoTen";
            this.gridColQHGDHoTen.Name = "gridColQHGDHoTen";
            this.gridColQHGDHoTen.OptionsColumn.AllowEdit = false;
            this.gridColQHGDHoTen.OptionsColumn.ReadOnly = true;
            this.gridColQHGDHoTen.Visible = true;
            this.gridColQHGDHoTen.VisibleIndex = 0;
            this.gridColQHGDHoTen.Width = 120;
            // 
            // gridColQHGDLoaiQuanHe
            // 
            this.gridColQHGDLoaiQuanHe.Caption = "Loại quan hệ";
            this.gridColQHGDLoaiQuanHe.FieldName = "LoaiQuanHe.TenQuanHe";
            this.gridColQHGDLoaiQuanHe.Name = "gridColQHGDLoaiQuanHe";
            this.gridColQHGDLoaiQuanHe.OptionsColumn.AllowEdit = false;
            this.gridColQHGDLoaiQuanHe.OptionsColumn.ReadOnly = true;
            this.gridColQHGDLoaiQuanHe.Visible = true;
            this.gridColQHGDLoaiQuanHe.VisibleIndex = 2;
            this.gridColQHGDLoaiQuanHe.Width = 100;
            // 
            // gridColTNVoChong
            // 
            this.gridColTNVoChong.Caption = "Thân nhân bên vợ/chồng";
            this.gridColTNVoChong.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColTNVoChong.FieldName = "ThanNhanVoChong";
            this.gridColTNVoChong.Name = "gridColTNVoChong";
            this.gridColTNVoChong.OptionsColumn.AllowEdit = false;
            this.gridColTNVoChong.OptionsColumn.ReadOnly = true;
            this.gridColTNVoChong.Visible = true;
            this.gridColTNVoChong.VisibleIndex = 3;
            this.gridColTNVoChong.Width = 130;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColQHGDCongViec
            // 
            this.gridColQHGDCongViec.Caption = "Nghề nghiệp";
            this.gridColQHGDCongViec.FieldName = "NgheNghiep";
            this.gridColQHGDCongViec.Name = "gridColQHGDCongViec";
            this.gridColQHGDCongViec.Visible = true;
            this.gridColQHGDCongViec.VisibleIndex = 4;
            this.gridColQHGDCongViec.Width = 100;
            // 
            // gridColQHGDNgaySinh
            // 
            this.gridColQHGDNgaySinh.Caption = "Năm sinh";
            this.gridColQHGDNgaySinh.FieldName = "NgaySinh";
            this.gridColQHGDNgaySinh.Name = "gridColQHGDNgaySinh";
            this.gridColQHGDNgaySinh.OptionsColumn.AllowEdit = false;
            this.gridColQHGDNgaySinh.OptionsColumn.ReadOnly = true;
            this.gridColQHGDNgaySinh.Visible = true;
            this.gridColQHGDNgaySinh.VisibleIndex = 1;
            this.gridColQHGDNgaySinh.Width = 100;
            // 
            // gridColQHGDNoiO
            // 
            this.gridColQHGDNoiO.Caption = "Địa chỉ";
            this.gridColQHGDNoiO.FieldName = "DiaChi";
            this.gridColQHGDNoiO.Name = "gridColQHGDNoiO";
            this.gridColQHGDNoiO.OptionsColumn.AllowEdit = false;
            this.gridColQHGDNoiO.OptionsColumn.ReadOnly = true;
            this.gridColQHGDNoiO.Visible = true;
            this.gridColQHGDNoiO.VisibleIndex = 5;
            this.gridColQHGDNoiO.Width = 120;
            // 
            // gridColQHGDDacDiem
            // 
            this.gridColQHGDDacDiem.Caption = "Đặc điểm lịch sử";
            this.gridColQHGDDacDiem.FieldName = "DacDiemLichSu";
            this.gridColQHGDDacDiem.Name = "gridColQHGDDacDiem";
            this.gridColQHGDDacDiem.OptionsColumn.AllowEdit = false;
            this.gridColQHGDDacDiem.OptionsColumn.ReadOnly = true;
            this.gridColQHGDDacDiem.Visible = true;
            this.gridColQHGDDacDiem.VisibleIndex = 6;
            this.gridColQHGDDacDiem.Width = 200;
            // 
            // gridColQHGDGhiChu
            // 
            this.gridColQHGDGhiChu.Caption = "Ghi chú";
            this.gridColQHGDGhiChu.FieldName = "GhiChu";
            this.gridColQHGDGhiChu.Name = "gridColQHGDGhiChu";
            this.gridColQHGDGhiChu.OptionsColumn.AllowEdit = false;
            this.gridColQHGDGhiChu.OptionsColumn.ReadOnly = true;
            this.gridColQHGDGhiChu.Visible = true;
            this.gridColQHGDGhiChu.VisibleIndex = 7;
            this.gridColQHGDGhiChu.Width = 150;
            // 
            // repositoryItemHyperLinkEditXoa
            // 
            this.repositoryItemHyperLinkEditXoa.AutoHeight = false;
            this.repositoryItemHyperLinkEditXoa.Name = "repositoryItemHyperLinkEditXoa";
            this.repositoryItemHyperLinkEditXoa.NullText = "Xóa";
            // 
            // groupControl13
            // 
            this.groupControl13.Appearance.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl13.Appearance.Options.UseFont = true;
            this.groupControl13.Controls.Add(this.chbTNBenVoChong);
            this.groupControl13.Controls.Add(this.dtPg5NgaySinh);
            this.groupControl13.Controls.Add(this.btnPg5Sua);
            this.groupControl13.Controls.Add(this.btnPg5Xoa);
            this.groupControl13.Controls.Add(this.cbbPg5LoaiQH);
            this.groupControl13.Controls.Add(this.btnPg5Them);
            this.groupControl13.Controls.Add(this.btnPg5TaoMoi);
            this.groupControl13.Controls.Add(this.groupControl14);
            this.groupControl13.Controls.Add(this.txtPg5GhiChu);
            this.groupControl13.Controls.Add(this.txtPg5DacDiemLS);
            this.groupControl13.Controls.Add(this.labelControl69);
            this.groupControl13.Controls.Add(this.labelControl68);
            this.groupControl13.Controls.Add(this.labelControl65);
            this.groupControl13.Controls.Add(this.labelControl67);
            this.groupControl13.Controls.Add(this.labelControl72);
            this.groupControl13.Controls.Add(this.labelControl78);
            this.groupControl13.Controls.Add(this.txtPg5NgheNghiep);
            this.groupControl13.Controls.Add(this.txtPg5HoTen);
            this.groupControl13.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl13.Location = new System.Drawing.Point(0, 0);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(1282, 173);
            this.groupControl13.TabIndex = 125;
            this.groupControl13.Text = "Thông tin thân nhân";
            // 
            // chbTNBenVoChong
            // 
            this.chbTNBenVoChong.Location = new System.Drawing.Point(392, 80);
            this.chbTNBenVoChong.Name = "chbTNBenVoChong";
            this.chbTNBenVoChong.Properties.Caption = "Thân nhân bên vợ/chồng";
            this.chbTNBenVoChong.Size = new System.Drawing.Size(143, 19);
            this.chbTNBenVoChong.TabIndex = 22;
            // 
            // dtPg5NgaySinh
            // 
            this.dtPg5NgaySinh.Location = new System.Drawing.Point(372, 54);
            this.dtPg5NgaySinh.Name = "dtPg5NgaySinh";
            this.dtPg5NgaySinh.Size = new System.Drawing.Size(100, 20);
            this.dtPg5NgaySinh.TabIndex = 21;
            // 
            // btnPg5Sua
            // 
            this.btnPg5Sua.Location = new System.Drawing.Point(196, 143);
            this.btnPg5Sua.Name = "btnPg5Sua";
            this.btnPg5Sua.Size = new System.Drawing.Size(75, 23);
            this.btnPg5Sua.TabIndex = 20;
            this.btnPg5Sua.Text = "Hiệu chỉnh";
            this.btnPg5Sua.Click += new System.EventHandler(this.btnPg5Sua_Click);
            // 
            // btnPg5Xoa
            // 
            this.btnPg5Xoa.Location = new System.Drawing.Point(291, 143);
            this.btnPg5Xoa.Name = "btnPg5Xoa";
            this.btnPg5Xoa.Size = new System.Drawing.Size(75, 23);
            this.btnPg5Xoa.TabIndex = 18;
            this.btnPg5Xoa.Text = "Xóa";
            this.btnPg5Xoa.Click += new System.EventHandler(this.btnPg5Xoa_Click);
            // 
            // cbbPg5LoaiQH
            // 
            this.cbbPg5LoaiQH.EditValue = "01";
            this.cbbPg5LoaiQH.Location = new System.Drawing.Point(372, 24);
            this.cbbPg5LoaiQH.Name = "cbbPg5LoaiQH";
            this.cbbPg5LoaiQH.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPg5LoaiQH.Properties.Appearance.Options.UseFont = true;
            this.cbbPg5LoaiQH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbPg5LoaiQH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaQuanHe", "Mã quan hệ", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenQuanHe", "Tên quan hệ")});
            this.cbbPg5LoaiQH.Properties.DisplayMember = "TenQuanHe";
            this.cbbPg5LoaiQH.Properties.NullText = "";
            this.cbbPg5LoaiQH.Properties.ValueMember = "MaQuanHe";
            this.cbbPg5LoaiQH.Size = new System.Drawing.Size(100, 22);
            this.cbbPg5LoaiQH.TabIndex = 19;
            // 
            // btnPg5Them
            // 
            this.btnPg5Them.Location = new System.Drawing.Point(101, 143);
            this.btnPg5Them.Name = "btnPg5Them";
            this.btnPg5Them.Size = new System.Drawing.Size(75, 23);
            this.btnPg5Them.TabIndex = 17;
            this.btnPg5Them.Text = "Lưu";
            this.btnPg5Them.Click += new System.EventHandler(this.btnPg5Them_Click);
            // 
            // btnPg5TaoMoi
            // 
            this.btnPg5TaoMoi.Location = new System.Drawing.Point(6, 143);
            this.btnPg5TaoMoi.Name = "btnPg5TaoMoi";
            this.btnPg5TaoMoi.Size = new System.Drawing.Size(75, 23);
            this.btnPg5TaoMoi.TabIndex = 16;
            this.btnPg5TaoMoi.Text = "Tạo mới";
            this.btnPg5TaoMoi.Click += new System.EventHandler(this.btnPg5TaoMoi_Click);
            // 
            // groupControl14
            // 
            this.groupControl14.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl14.Appearance.Options.UseFont = true;
            this.groupControl14.Controls.Add(this.labelControl73);
            this.groupControl14.Controls.Add(this.txtPg5DiaChi);
            this.groupControl14.Location = new System.Drawing.Point(6, 80);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(386, 57);
            this.groupControl14.TabIndex = 15;
            this.groupControl14.Text = "Nơi ở";
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl73.Location = new System.Drawing.Point(5, 27);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(39, 15);
            this.labelControl73.TabIndex = 9;
            this.labelControl73.Text = "Địa chỉ";
            // 
            // txtPg5DiaChi
            // 
            this.txtPg5DiaChi.Location = new System.Drawing.Point(53, 24);
            this.txtPg5DiaChi.Name = "txtPg5DiaChi";
            this.txtPg5DiaChi.Properties.AllowFocused = false;
            this.txtPg5DiaChi.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg5DiaChi.Properties.Appearance.Options.UseFont = true;
            this.txtPg5DiaChi.Size = new System.Drawing.Size(328, 22);
            this.txtPg5DiaChi.TabIndex = 8;
            // 
            // txtPg5GhiChu
            // 
            this.txtPg5GhiChu.Location = new System.Drawing.Point(768, 48);
            this.txtPg5GhiChu.Name = "txtPg5GhiChu";
            this.txtPg5GhiChu.Properties.AllowFocused = false;
            this.txtPg5GhiChu.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg5GhiChu.Properties.Appearance.Options.UseFont = true;
            this.txtPg5GhiChu.Size = new System.Drawing.Size(108, 89);
            this.txtPg5GhiChu.TabIndex = 7;
            // 
            // txtPg5DacDiemLS
            // 
            this.txtPg5DacDiemLS.Location = new System.Drawing.Point(538, 48);
            this.txtPg5DacDiemLS.Name = "txtPg5DacDiemLS";
            this.txtPg5DacDiemLS.Properties.AllowFocused = false;
            this.txtPg5DacDiemLS.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg5DacDiemLS.Properties.Appearance.Options.UseFont = true;
            this.txtPg5DacDiemLS.Size = new System.Drawing.Size(224, 89);
            this.txtPg5DacDiemLS.TabIndex = 7;
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl69.Location = new System.Drawing.Point(768, 27);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(42, 15);
            this.labelControl69.TabIndex = 6;
            this.labelControl69.Text = "Ghi chú";
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl68.Location = new System.Drawing.Point(541, 27);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(96, 15);
            this.labelControl68.TabIndex = 6;
            this.labelControl68.Text = "Đặc điểm lịch sử";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl65.Location = new System.Drawing.Point(291, 27);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(77, 15);
            this.labelControl65.TabIndex = 0;
            this.labelControl65.Text = "Loại quan hệ*";
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl67.Location = new System.Drawing.Point(310, 55);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(58, 15);
            this.labelControl67.TabIndex = 0;
            this.labelControl67.Text = "Năm sinh*";
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl72.Location = new System.Drawing.Point(6, 55);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(70, 15);
            this.labelControl72.TabIndex = 0;
            this.labelControl72.Text = "Nghề nghiệp";
            // 
            // labelControl78
            // 
            this.labelControl78.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl78.Location = new System.Drawing.Point(35, 27);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(41, 15);
            this.labelControl78.TabIndex = 0;
            this.labelControl78.Text = "Họ tên*";
            // 
            // txtPg5NgheNghiep
            // 
            this.txtPg5NgheNghiep.Location = new System.Drawing.Point(82, 52);
            this.txtPg5NgheNghiep.Name = "txtPg5NgheNghiep";
            this.txtPg5NgheNghiep.Properties.AllowFocused = false;
            this.txtPg5NgheNghiep.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg5NgheNghiep.Properties.Appearance.Options.UseFont = true;
            this.txtPg5NgheNghiep.Size = new System.Drawing.Size(205, 22);
            this.txtPg5NgheNghiep.TabIndex = 0;
            // 
            // txtPg5HoTen
            // 
            this.txtPg5HoTen.Location = new System.Drawing.Point(82, 24);
            this.txtPg5HoTen.Name = "txtPg5HoTen";
            this.txtPg5HoTen.Properties.AllowFocused = false;
            this.txtPg5HoTen.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPg5HoTen.Properties.Appearance.Options.UseFont = true;
            this.txtPg5HoTen.Size = new System.Drawing.Size(205, 22);
            this.txtPg5HoTen.TabIndex = 0;
            // 
            // ucNVQuanHeGiaDinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "ucNVQuanHeGiaDinh";
            this.Size = new System.Drawing.Size(1282, 510);
            this.Load += new System.EventHandler(this.ucNVQuanHeGiaDinh_Load);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPg5QuanHeGiaDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewQuanHeGiaDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            this.groupControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbTNBenVoChong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPg5NgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPg5LoaiQH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            this.groupControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5DiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5GhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5DacDiemLS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5NgheNghiep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPg5HoTen.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XtraScrollableControl xtraScrollableControl1;
        private GridControl gvPg5QuanHeGiaDinh;
        private GridView gridviewQuanHeGiaDinh;
        private GridColumn gridColQHGDID;
        private GridColumn gridColQHGDHoTen;
        private GridColumn gridColQHGDLoaiQuanHe;
        private GridColumn gridColQHGDCongViec;
        private RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditXoa;
        private GridColumn gridColQHGDNgaySinh;
        private GridColumn gridColQHGDNoiO;
        private GridColumn gridColQHGDDacDiem;
        private GridColumn gridColQHGDGhiChu;
        private GroupControl groupControl13;
        private SimpleButton btnPg5Xoa;
        private LookUpEdit cbbPg5LoaiQH;
        private SimpleButton btnPg5Them;
        private SimpleButton btnPg5TaoMoi;
        private GroupControl groupControl14;
        private LabelControl labelControl73;
        private TextEdit txtPg5DiaChi;
        private MemoEdit txtPg5GhiChu;
        private MemoEdit txtPg5DacDiemLS;
        private LabelControl labelControl69;
        private LabelControl labelControl68;
        private LabelControl labelControl65;
        private LabelControl labelControl67;
        private LabelControl labelControl72;
        private LabelControl labelControl78;
        private TextEdit txtPg5NgheNghiep;
        private TextEdit txtPg5HoTen;
        private SimpleButton btnPg5Sua;
        private TextEdit dtPg5NgaySinh;
        private GridColumn gridColTNVoChong;
        private RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private CheckEdit chbTNBenVoChong;

    }
}
