﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Text;
using System.Security.Cryptography;
using HerculesCTL;
using HerculesDTO;

namespace HerculesHRMT
{
    public partial class ucDangNhap : XtraUserControl
    {
        TaiKhoanCTL taikhoanCTL = new TaiKhoanCTL();
        public ucDangNhap()
        {
            InitializeComponent();
        }
        public delegate void SendInfor(bool isDaDangNhap);
        public SendInfor dnSender;

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (txtTaikhoan.Text.Trim() == "" || txtMatKhau.Text == "")
            {
                XtraMessageBox.Show("Vui lòng nhập tài khoản và mật khẩu", "Tài khoản hoặc mật khẩu trống", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                //Mã hóa mật khẩu MD5
                string strMD5 = Common.TaoChuoiMD5(txtMatKhau.Text);
                TaiKhoanDTO tkdangnhap = taikhoanCTL.LayTaiKhoanUserDangNhap(txtTaikhoan.Text, strMD5);
                if (tkdangnhap != null)
                {
                    Program.herculesUser = tkdangnhap;
                    dnSender(true);
                }
                else
                {
                    XtraMessageBox.Show("Tài khoản hoặc mật khẩu không đúng", "Đăng nhập", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        
    }
}
