﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace HerculesHRMT
{
    partial class ucQuanHuyen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTenVT = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.meGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.lueTinhThanh = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.radLoaiQH = new DevExpress.XtraEditors.RadioGroup();
            this.txtTenDD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.txtQuanHuyen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcQuanHuyen = new DevExpress.XtraGrid.GridControl();
            this.gridViewDSQuanHuyen = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColMaQH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenQH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTinhThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColLoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTenDayDu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridColTenVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhThanh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoaiQH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuanHuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuanHuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSQuanHuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTenVT
            // 
            this.txtTenVT.Location = new System.Drawing.Point(503, 88);
            this.txtTenVT.Name = "txtTenVT";
            this.txtTenVT.Size = new System.Drawing.Size(214, 20);
            this.txtTenVT.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.meGhiChu);
            this.panelControl1.Controls.Add(this.lueTinhThanh);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtTenVT);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.radLoaiQH);
            this.panelControl1.Controls.Add(this.txtTenDD);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnXoa);
            this.panelControl1.Controls.Add(this.btnLuu);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThem);
            this.panelControl1.Controls.Add(this.txtQuanHuyen);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 224);
            this.panelControl1.TabIndex = 5;
            // 
            // meGhiChu
            // 
            this.meGhiChu.Location = new System.Drawing.Point(105, 124);
            this.meGhiChu.Name = "meGhiChu";
            this.meGhiChu.Size = new System.Drawing.Size(259, 93);
            this.meGhiChu.TabIndex = 4;
            // 
            // lueTinhThanh
            // 
            this.lueTinhThanh.Location = new System.Drawing.Point(105, 56);
            this.lueTinhThanh.Name = "lueTinhThanh";
            this.lueTinhThanh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTinhThanh.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaTinhThanh", "Ma Tinh Thanh", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenTinhThanh", "Tỉnh/Thành")});
            this.lueTinhThanh.Properties.DisplayMember = "TenTinhThanh";
            this.lueTinhThanh.Properties.NullText = "";
            this.lueTinhThanh.Properties.ValueMember = "MaTinhThanh";
            this.lueTinhThanh.Size = new System.Drawing.Size(259, 20);
            this.lueTinhThanh.TabIndex = 0;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(30, 56);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Tỉnh/Thành:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(397, 91);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Tên viết tắt:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(30, 126);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(39, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Ghi chú:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(397, 126);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(23, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Loại:";
            // 
            // radLoaiQH
            // 
            this.radLoaiQH.EditValue = "0";
            this.radLoaiQH.Location = new System.Drawing.Point(503, 126);
            this.radLoaiQH.Name = "radLoaiQH";
            this.radLoaiQH.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Quận"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Huyện"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Thị xã/Thành phố")});
            this.radLoaiQH.Size = new System.Drawing.Size(214, 48);
            this.radLoaiQH.TabIndex = 5;
            // 
            // txtTenDD
            // 
            this.txtTenDD.Location = new System.Drawing.Point(105, 88);
            this.txtTenDD.Name = "txtTenDD";
            this.txtTenDD.Size = new System.Drawing.Size(259, 20);
            this.txtTenDD.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(30, 91);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(58, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Tên đầy đủ:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(675, 194);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(582, 194);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(87, 23);
            this.btnLuu.TabIndex = 8;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(489, 194);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 23);
            this.btnCapNhat.TabIndex = 7;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(396, 194);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(87, 23);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtQuanHuyen
            // 
            this.txtQuanHuyen.Location = new System.Drawing.Point(503, 56);
            this.txtQuanHuyen.Name = "txtQuanHuyen";
            this.txtQuanHuyen.Size = new System.Drawing.Size(214, 20);
            this.txtQuanHuyen.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(397, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên Quận/Huyện:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(297, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(186, 33);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Quận - Huyện";
            // 
            // gcQuanHuyen
            // 
            this.gcQuanHuyen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcQuanHuyen.Location = new System.Drawing.Point(2, 2);
            this.gcQuanHuyen.MainView = this.gridViewDSQuanHuyen;
            this.gcQuanHuyen.Name = "gcQuanHuyen";
            this.gcQuanHuyen.Size = new System.Drawing.Size(781, 293);
            this.gcQuanHuyen.TabIndex = 3;
            this.gcQuanHuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDSQuanHuyen});
            // 
            // gridViewDSQuanHuyen
            // 
            this.gridViewDSQuanHuyen.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColMaQH,
            this.gridColTenQH,
            this.gridColTinhThanh,
            this.gridColLoai,
            this.gridColTenDD,
            this.gridColTenVT,
            this.gridColGhiChu});
            this.gridViewDSQuanHuyen.GridControl = this.gcQuanHuyen;
            this.gridViewDSQuanHuyen.GroupPanelText = " ";
            this.gridViewDSQuanHuyen.Name = "gridViewDSQuanHuyen";
            this.gridViewDSQuanHuyen.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDSQuanHuyen_FocusedRowChanged);
            // 
            // gridColMaQH
            // 
            this.gridColMaQH.Caption = "Mã Quận Huyện";
            this.gridColMaQH.FieldName = "MaQuanHuyen";
            this.gridColMaQH.Name = "gridColMaQH";
            this.gridColMaQH.OptionsColumn.AllowEdit = false;
            this.gridColMaQH.OptionsColumn.ReadOnly = true;
            // 
            // gridColTenQH
            // 
            this.gridColTenQH.Caption = "Tên Quận/Huyện";
            this.gridColTenQH.FieldName = "TenQuanHuyen";
            this.gridColTenQH.Name = "gridColTenQH";
            this.gridColTenQH.OptionsColumn.AllowEdit = false;
            this.gridColTenQH.OptionsColumn.ReadOnly = true;
            this.gridColTenQH.Visible = true;
            this.gridColTenQH.VisibleIndex = 0;
            // 
            // gridColTinhThanh
            // 
            this.gridColTinhThanh.Caption = "Tỉnh/Thành";
            this.gridColTinhThanh.FieldName = "TenTinhThanh";
            this.gridColTinhThanh.Name = "gridColTinhThanh";
            this.gridColTinhThanh.OptionsColumn.AllowEdit = false;
            this.gridColTinhThanh.OptionsColumn.ReadOnly = true;
            this.gridColTinhThanh.Visible = true;
            this.gridColTinhThanh.VisibleIndex = 1;
            // 
            // gridColLoai
            // 
            this.gridColLoai.Caption = "Loại Quận/Huyện";
            this.gridColLoai.FieldName = "TenLoaiQuanHuyen";
            this.gridColLoai.Name = "gridColLoai";
            this.gridColLoai.OptionsColumn.AllowEdit = false;
            this.gridColLoai.OptionsColumn.ReadOnly = true;
            this.gridColLoai.Visible = true;
            this.gridColLoai.VisibleIndex = 2;
            // 
            // gridColTenDD
            // 
            this.gridColTenDD.Caption = "Tên Đầy Đủ";
            this.gridColTenDD.FieldName = "TenDayDu";
            this.gridColTenDD.Name = "gridColTenDD";
            this.gridColTenDD.OptionsColumn.AllowEdit = false;
            this.gridColTenDD.OptionsColumn.ReadOnly = true;
            this.gridColTenDD.Visible = true;
            this.gridColTenDD.VisibleIndex = 3;
            // 
            // gridColTenVT
            // 
            this.gridColTenVT.Caption = "Tên Viết Tắt";
            this.gridColTenVT.FieldName = "TenVietTat";
            this.gridColTenVT.Name = "gridColTenVT";
            this.gridColTenVT.OptionsColumn.AllowEdit = false;
            this.gridColTenVT.OptionsColumn.ReadOnly = true;
            this.gridColTenVT.Visible = true;
            this.gridColTenVT.VisibleIndex = 4;
            // 
            // gridColGhiChu
            // 
            this.gridColGhiChu.Caption = "Ghi Chú";
            this.gridColGhiChu.FieldName = "GhiChu";
            this.gridColGhiChu.Name = "gridColGhiChu";
            this.gridColGhiChu.Visible = true;
            this.gridColGhiChu.VisibleIndex = 5;
            // 
            // gridColTenDayDu
            // 
            this.gridColTenDayDu.Caption = "Tên đầy đủ";
            this.gridColTenDayDu.FieldName = "TENDAYDU";
            this.gridColTenDayDu.Name = "gridColTenDayDu";
            this.gridColTenDayDu.OptionsColumn.AllowEdit = false;
            this.gridColTenDayDu.OptionsColumn.ReadOnly = true;
            this.gridColTenDayDu.Visible = true;
            this.gridColTenDayDu.VisibleIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.AutoSize = true;
            this.panelControl3.Controls.Add(this.gcQuanHuyen);
            this.panelControl3.Location = new System.Drawing.Point(0, 218);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(785, 297);
            this.panelControl3.TabIndex = 4;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl1);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(785, 518);
            this.panelControl2.TabIndex = 3;
            // 
            // gridColTenVietTat
            // 
            this.gridColTenVietTat.Caption = "Tên viết tắt";
            this.gridColTenVietTat.Name = "gridColTenVietTat";
            this.gridColTenVietTat.OptionsColumn.AllowEdit = false;
            this.gridColTenVietTat.OptionsColumn.ReadOnly = true;
            this.gridColTenVietTat.Visible = true;
            this.gridColTenVietTat.VisibleIndex = 3;
            // 
            // ucQuanHuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Name = "ucQuanHuyen";
            this.Size = new System.Drawing.Size(785, 518);
            this.Load += new System.EventHandler(this.ucQuanHuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhThanh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLoaiQH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuanHuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuanHuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDSQuanHuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TextEdit txtTenVT;
        private PanelControl panelControl1;
        private LabelControl labelControl6;
        private LabelControl labelControl5;
        private LabelControl labelControl4;
        private RadioGroup radLoaiQH;
        private TextEdit txtTenDD;
        private LabelControl labelControl3;
        private SimpleButton btnXoa;
        private SimpleButton btnLuu;
        private SimpleButton btnCapNhat;
        private SimpleButton btnThem;
        private TextEdit txtQuanHuyen;
        private LabelControl labelControl2;
        private LabelControl labelControl1;
        private GridControl gcQuanHuyen;
        private GridView gridViewDSQuanHuyen;
        private GridColumn gridColTenDayDu;
        private PanelControl panelControl3;
        private PanelControl panelControl2;
        private GridColumn gridColTenVietTat;
        private GridColumn gridColMaQH;
        private GridColumn gridColTenQH;
        private GridColumn gridColTinhThanh;
        private GridColumn gridColLoai;
        private GridColumn gridColTenDD;
        private GridColumn gridColTenVT;
        private LookUpEdit lueTinhThanh;
        private LabelControl labelControl7;
        private MemoEdit meGhiChu;
        private GridColumn gridColGhiChu;
    }
}
