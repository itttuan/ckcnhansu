﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HerculesCTL;
using HerculesDTO;
//vdtoan
namespace HerculesHRMT
{
    public partial class ucNVDotTuyen : DevExpress.XtraEditors.XtraUserControl
    {
        PhongKhoaCTL phongkhoaCTL = new PhongKhoaCTL();
        BoMonBoPhanCTL bomonbophanCTL = new BoMonBoPhanCTL();
        NVDotTuyenCTL nhanvienCTL = new NVDotTuyenCTL();
        HinhAnhCTL hinhanhCTL = new HinhAnhCTL();
        List<byte[]> listImgNV = new List<byte[]>();
        List<NVDotTuyenDTO> listNhanVien = new List<NVDotTuyenDTO>();
        string id = "";
        string strMaNV = "";
        Common.STATUS _statusTNX;
       
       //vdtoan add 04102016
        static Boolean chaylandau2 = true;

        public ucNVDotTuyen()
        {
            InitializeComponent();
        }

        public void LoadDuLieuTatCaLenUC()
        {

            LoadData();
            //LoadDanhSachNhanVien();
        }
        private void ucNVDotTuyenDung_Load(object sender, EventArgs e)
        {
        }

        private void LoadData()
        {
            List<PhongKhoaDTO> listPhongKhoa = Program.listPhongKhoa_UV;

            //listPhongKhoa.Insert(0, new PhongKhoaDTO());
            if (chaylandau2 == true)
                listPhongKhoa.Insert(0, new PhongKhoaDTO());

            List<BoMonBoPhanDTO> listBPBM = Program.listBMBP_UV;
            //listBPBM.Insert(0, new BoMonBoPhanDTO());
            if (chaylandau2 == true)
                listBPBM.Insert(0, new BoMonBoPhanDTO());
            chaylandau2 = false;

            lueTKPhongKhoa.Properties.DataSource = listPhongKhoa;
            lueTKBPBM.Properties.DataSource = listBPBM;
            if (listPhongKhoa.Count > 0)
            {
                lueTKPhongKhoa.EditValue = listPhongKhoa[0].MaPhongKhoa;
            }

            ////vdtoan upd
            ////load luePhongKhoa
            //List<PhongKhoaDTO> listpk = new List<PhongKhoaDTO>();
            //PhongKhoaCTL pkCTL = new PhongKhoaCTL();
            //listpk = pkCTL.GetAll();
            //lueTKPhongKhoa.Properties.DataSource = listpk;

            ////load lueBMBP
            //List<BoMonBoPhanDTO> listbmbp = new List<BoMonBoPhanDTO>();
            //BoMonBoPhanCTL bmbpCTL = new BoMonBoPhanCTL();
            //listbmbp = bmbpCTL.GetAll();
            //lueTKBPBM.Properties.DataSource = listbmbp;


            // vdtoan
            //List<String> listMaDot = new List<string> () {null,"201601","201602"};
            //lueTKMaDotTuyenDung.Properties.DataSource = listMaDot;
            
            
            //cbbeTKMaDotTuyenDung.Properties.Items.Add("");
            //cbbeTKMaDotTuyenDung.Properties.Items.Add("201601");
            //cbbeTKMaDotTuyenDung.Properties.Items.Add("201602");

            //load lueMaDot
            List<DotTuyenDTO> listdt = new List<DotTuyenDTO>();
            DotTuyenCTL dtCTL = new DotTuyenCTL();
            listdt = dtCTL.GetAll();
            listdt.Insert(0, new DotTuyenDTO());
            lueTKMaDotTuyenDung.Properties.DataSource = listdt;
            if (listdt.Count > 0)
            {
                lueTKMaDotTuyenDung.EditValue = listdt[0].MaDot;
            }

            listNhanVien = nhanvienCTL.LayDanhSachTatCaNhanVien();
            gcDSNhanVien.DataSource = listNhanVien;
            gridViewDSNhanVien.BestFitColumns();
 
        }
        private void LoadDanhSachNhanVien()
        {
          
            //string strPK, strBMBP, strMaDot;

            //if (lueTKPhongKhoa.EditValue == null)
            //    strPK = "";
            //else
            //    strPK= lueTKPhongKhoa.EditValue.ToString();

            //if (lueTKBPBM.EditValue == null)
            //    strBMBP = "";
            //else
            //    strBMBP = lueTKBPBM.EditValue.ToString();

            //if (lueTKMaDotTuyenDung.EditValue == null)
            //    strMaDot = "";
            //else
            //    strMaDot = lueTKMaDotTuyenDung.EditValue.ToString();
            //listNhanVien = nhanvienCTL.TimNhanVienTheoNhieuTieuChi(strPK, strBMBP, strMaDot);
            //gcDSNhanVien.DataSource = listNhanVien;
            //gridViewDSNhanVien.BestFitColumns();

            if (lueTKPhongKhoa.EditValue == null && lueTKBPBM.EditValue == null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachTatCaNhanVien();
            }
            else if (lueTKPhongKhoa.EditValue != null && lueTKBPBM.EditValue == null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
            }
            else if (lueTKBPBM.EditValue != null)
            {
                listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoBoMon(lueTKBPBM.EditValue.ToString().Trim());
            }
            gcDSNhanVien.DataSource = listNhanVien;

            ////vdtoan upd
            //if (lueTKPhongKhoa.Text == string.Empty && lueTKBPBM.Text == string.Empty)
            //{
            //    listNhanVien = nhanvienCTL.LayDanhSachTatCaNhanVien();
            //}
            //else if (lueTKPhongKhoa.Text != string.Empty && lueTKBPBM.Text == string.Empty)
            //{
            //    listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
            //}
            //else if (lueTKBPBM.Text != string.Empty)
            //{
            //    listNhanVien = nhanvienCTL.LayDanhSachNhanVienTheoBoMon(lueTKBPBM.EditValue.ToString().Trim());
            //}
            //gcDSNhanVien.DataSource = listNhanVien;
        }

        private void grcTimKiem_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tabUngVien_Click(object sender, EventArgs e)
        {

        }

        private void lueTKPhongKhoa_EditValueChanged(object sender, EventArgs e)
        {
            if (lueTKPhongKhoa.EditValue == null)
            {
                lueTKBPBM.Properties.DataSource = null;
                List<BoMonBoPhanDTO> listBPBM = Program.listBMBP_UV;
                lueTKBPBM.Properties.DataSource = listBPBM;
                if (listBPBM.Count > 0)
                {
                    lueTKBPBM.EditValue = listBPBM[0].MaBMBP;
                }
            }

            if (lueTKPhongKhoa.EditValue != null)
            {
                List<BoMonBoPhanDTO> listBPBM = bomonbophanCTL.GetListByMaPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
                listBPBM.Insert(0, new BoMonBoPhanDTO());
                // if (chaylandau2 == true)
                //         listBPBM.Insert(0, new BoMonBoPhanDTO());
                //chaylandau2 = false;

                lueTKBPBM.Properties.DataSource = listBPBM;
                if (listBPBM.Count > 0)
                {
                    lueTKBPBM.EditValue = listBPBM[0].MaBMBP;
                }
            }
            LoadDanhSachNhanVien();

            ////vdtoan upd
            //if (lueTKPhongKhoa.EditValue != string.Empty)
            //{
            //    BoMonBoPhanCTL bomonbophanCTL = new BoMonBoPhanCTL();
            //    List<BoMonBoPhanDTO> listBPBM = bomonbophanCTL.GetListByMaPhongKhoa(lueTKPhongKhoa.EditValue.ToString().Trim());
            //    //listBPBM.Insert(0, new BoMonBoPhanDTO());
            //    lueTKBPBM.Properties.DataSource = listBPBM;
            //    //if (listBPBM.Count > 0)
            //    //{
            //    //lueBMBP.EditValue = listBPBM[0].MaBMBP;
            //    //}
            //}
            //LoadDanhSachNhanVien();
        }

        private void lueTKBPBM_EditValueChanged(object sender, EventArgs e)
        {
            LoadDanhSachNhanVien();
        }

        private void btnTimKiemMaNV_Click(object sender, EventArgs e)
        {
            if (txtTKMaNV.Text.Trim() != "")
            {
                listNhanVien = nhanvienCTL.TimNhanVienTheoMaNV(txtTKMaNV.Text.Trim());
                gcDSNhanVien.DataSource = listNhanVien;
                gridViewDSNhanVien.BestFitColumns();
            }
            else
                LoadDanhSachNhanVien();
        }

        private void btnTimKiemCMND_Click(object sender, EventArgs e)
        {
          
        }

        private void tabNhanVien_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnTimKiemMaDotTuyenDung_Click(object sender, EventArgs e)
        {
            
        }

       
        private void lueTKMaDotTuyenDung_EditValueChanged_1(object sender, EventArgs e)
        {
            
           
        }

        private void labelControl5_Click(object sender, EventArgs e)
        {

        }

        private void cbbeTKMaDotTuyenDung_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void gcDSNhanVien_Click(object sender, EventArgs e)
        {

        }

        private void lueTKMaDotTuyenDung_EditValueChanged(object sender, EventArgs e)
        {

            //LoadDanhSachNhanVien();
        }

        private void txtTKMaNV_Enter(object sender, EventArgs e)
        {
        
        }

        private void txtTKMaNV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnTimKiemMaNV_Click(sender,e);
            }
        }

        private void btnTimKiemTheoDotTuyenDung_Click(object sender, EventArgs e)
        {

            if (lueTKMaDotTuyenDung.EditValue == null)
            {
                XtraMessageBox.Show("Vui lòng nhập Đợt tuyển dụng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gcDSNhanVien.DataSource = listNhanVien;

            }
            else
            {
                //gridViewNhacNho.Columns["Id"].FilterInfo = new ColumnFilterInfo("[Id] = " + txtID.Text);
                //gcDSNhanVien.DataSource = listNhanVien.FindAll(onn => onn.MaDot == lueTKMaDotTuyenDung.EditValue.ToString());
                //vdtoan upd
                listNhanVien = nhanvienCTL.TimNhanVienTheoMaDot(lueTKMaDotTuyenDung.EditValue.ToString());
                gcDSNhanVien.DataSource = listNhanVien;
                gridViewDSNhanVien.BestFitColumns();


            }
        }

        private void txtTKMaNV_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btnTimKiemMaUV_Click(object sender, EventArgs e)
        {
            if (txtTKMaUV.Text.Trim() != "")
            {
                listNhanVien = nhanvienCTL.TimNhanVienTheoMaUV(txtTKMaUV.Text.Trim());
                gcDSNhanVien.DataSource = listNhanVien;
                gridViewDSNhanVien.BestFitColumns();
            }
            else
                LoadDanhSachNhanVien();
        }

    }
}
