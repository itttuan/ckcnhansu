﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using HerculesCTL;
using HerculesDTO;

namespace HerculesHRMT
{
    public partial class FrmThemNhanVien : XtraForm
    {
        #region USER CONTROLS
        private readonly ucNVSoLuocLyLich _ucNvSoLuocLyLich = new ucNVSoLuocLyLich(false);
        private readonly ucNVQuaTrinhCongTac _ucNvQuaTrinhCongTac = new ucNVQuaTrinhCongTac(false);
        private readonly ucNVTrinhDo _ucNvTrinhDo = new ucNVTrinhDo(false);
        private readonly ucNVKhenThuongKyLuat _ucNvKhenThuongKyLuat = new ucNVKhenThuongKyLuat(false);
        private readonly ucNVQuanHeGiaDinh _ucNvQuanHeGiaDinh = new ucNVQuanHeGiaDinh(false);
        #endregion

        #region PARAMETERS

        private string _maNv = string.Empty;

        private readonly NhanVienCTL _nhanVienCtl = new NhanVienCTL();
        private readonly HinhAnhCTL _hinhAnhCtl = new HinhAnhCTL();
        private readonly QuaTrinhCongTacCTL _quaTrinhCongTacCtl = new QuaTrinhCongTacCTL();
        private readonly TrinhDoCTL _trinhDoCtl = new TrinhDoCTL();
        private readonly KhenThuongKyLuatCTL _khenThuongKyLuatCtl = new KhenThuongKyLuatCTL();
        private readonly QuanHeGiaDinhCTL _quanHeGiaDinhCtl = new QuanHeGiaDinhCTL();
        #endregion

        // create list of tab's info to handle its status
        private readonly List<AddStaffTabDTO> _dsTab = new List<AddStaffTabDTO>
        {
            new AddStaffTabDTO(),
            new AddStaffTabDTO(),
            new AddStaffTabDTO(),
            new AddStaffTabDTO(),
            new AddStaffTabDTO(),
            new AddStaffTabDTO()
        };

        public FrmThemNhanVien()
        {
            InitializeComponent();

            // ucNVSoLuocLyLich
            _ucNvSoLuocLyLich.SoYeuLyLichSender = GetSoYeuLyLichSender;
            _ucNvSoLuocLyLich.MainInfoSender = GetMainInfoSender;
            _ucNvSoLuocLyLich.Dock = DockStyle.Fill;
            



            _ucNvQuaTrinhCongTac.Dock = DockStyle.Fill;
            _ucNvTrinhDo.Dock = DockStyle.Fill;
            _ucNvKhenThuongKyLuat.Dock = DockStyle.Fill;
            _ucNvQuanHeGiaDinh.Dock = DockStyle.Fill;

            pnl_NVSoLuocLyLich.Controls.Add(_ucNvSoLuocLyLich);
            pnl_NVQuaTrinhCongTac.Controls.Add(_ucNvQuaTrinhCongTac);
            pnl_NVTrinhDo.Controls.Add(_ucNvTrinhDo);
            pnl_NVKhenThuongKyLuat.Controls.Add(_ucNvKhenThuongKyLuat);
            pnl_NVQuanHeGiaDinh.Controls.Add(_ucNvQuanHeGiaDinh);
        }

        #region GET INFO: SOYEULYLICH, TRINHDO, QUATRINHCONGTAC, QUANHEGIADINH

        public void GetSoYeuLyLichSender(string maNv)
        {
            _maNv = maNv;

            txtPg1MaNV.Text = _maNv;
            _ucNvQuaTrinhCongTac.SetMaNV(_maNv, Common.STATUS.NEW);
            _ucNvTrinhDo.SetMaNV(_maNv, Common.STATUS.NEW);
            _ucNvKhenThuongKyLuat.setMaNV(_maNv, Common.STATUS.NEW);
            _ucNvQuanHeGiaDinh.SetMaNV(_maNv, Common.STATUS.NEW);
        }

        public void GetMainInfoSender(string hoLot, string ten, string phongKhoa, string boMonBoPhan)
        {
            txtHoTen.Text = string.Format("{0} {1}", hoLot, ten);
            txtTenPhongKhoa.Text = phongKhoa;
            txtTenBMBP.Text = boMonBoPhan;
        }
        #endregion

        #region FORM EVENTS

        private void frmThemNhanVien_Load(object sender, EventArgs e)
        {
            // set first tab is active default
            xtraTabControl.AppearancePage.HeaderActive.Font = new Font(xtraTabControl.AppearancePage.HeaderActive.Font, FontStyle.Bold);
        }
        #endregion

        #region TAB EVENTS
        private void xtraTabControl_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            SetTabFormat(xtraTabControl.SelectedTabPageIndex, false);
        }

        private void xtraTabControl_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            SetTabFormat(xtraTabControl.SelectedTabPageIndex, true);
        }
        #endregion

        #region TAB METHODS

        // set name of form follow active tab
        private void SetFormName(int index)
        {
            switch (index)
            {
                case 0:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.SoLuocLyLich);
                    break;
                case 1:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.LichSuBanThan);
                    break;
                case 2:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.TrinhDoChuyenMon);
                    break;
                case 3:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.KhenThuongKyLuat);
                    break;
                case 4:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.QuanHeGiaDinh);
                    break;
                case 5:
                    Text = string.Format(Constants.FrmThemNvFormat, Constants.FrmThemNv,
                        Constants.TuNhanXet);
                    break;
            }
        }
        #endregion

        #region METHODS
       // handle status of progress bar
        private void SetTabFormat(int index, bool isPrevious)
        {
            // check tab's info for set format
            //CheckTab(index);

            // get tab by index
            var tabDto = _dsTab[index];

            // tab not start 
            // -> set tab is normal status
            if (!tabDto.IsStart && isPrevious && !tabDto.IsError)
            {
                ucProgressBarAddStaff.SetFormat(index, Constants.NormalStatus);
                return;
            }

            // tab have errors or started but not finished 
            // -> set tab is error status
            if (tabDto.IsError || (tabDto.IsStart && !tabDto.IsComplete && isPrevious))
            {
                ucProgressBarAddStaff.SetFormat(index, Constants.ErrorStatus);
                return;
            }

            // tab complete and has not any errors
            // -> set tab is checked
            if (tabDto.IsComplete && !tabDto.IsError && isPrevious)
            {
                ucProgressBarAddStaff.SetFormat(index, Constants.CheckedStatus);
                return;
            }

            ucProgressBarAddStaff.SetFormat(index, Constants.CurrentStatus);
        }

        #endregion

        #region BUTTON EVENTS
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_maNv))
            {
                XtraMessageBox.Show(Constants.ErrLuuSoLuocLyLich, Constants.Notify, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            btnBack.Enabled = true;
            if (xtraTabControl.SelectedTabPageIndex == 4)
            {
                btnNext.Text = Constants.AddNewStaffFinish;
            }

            if (xtraTabControl.SelectedTabPageIndex == 5)
            {
                Close();
            }

            SetTabFormat(xtraTabControl.SelectedTabPageIndex, true);

            if (xtraTabControl.SelectedTabPageIndex == 5)
                xtraTabControl.SelectedTabPageIndex = 0;
            else
            {
                xtraTabControl.SelectedTabPageIndex++;
            }

            SetTabFormat(xtraTabControl.SelectedTabPageIndex, false);
            SetFormName(xtraTabControl.SelectedTabPageIndex);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (xtraTabControl.SelectedTabPageIndex == 1)
            {
                btnBack.Enabled = false;
            }
            else
            {
                btnNext.Text = Constants.AddNewStaffNext;
            }

            SetTabFormat(xtraTabControl.SelectedTabPageIndex, true);

            if (xtraTabControl.SelectedTabPageIndex == 0)
                xtraTabControl.SelectedTabPageIndex = 6;
            else
            {
                xtraTabControl.SelectedTabPageIndex--;
            }

            SetTabFormat(xtraTabControl.SelectedTabPageIndex, false);
            SetFormName(xtraTabControl.SelectedTabPageIndex);
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            //var rpt = new rptNhanVienPreview(GetData());
            //rpt.ShowPreview();
            
            rptLyLich ll = new rptLyLich(GetData());
            ll.ShowPreview();
        }
        #endregion

        #region TEXT EVENTS
        #endregion
    
        #region IMAGE EVENTS
        private void imgHinhAnh_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_maNv))
            {
                XtraMessageBox.Show(Constants.ErrLuuSoLuocLyLich, Constants.Notify, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //var open = new OpenFileDialog
            //{
            //    Filter = Constants.ImageFilter
            //};

            //if (open.ShowDialog() != DialogResult.OK) return;

            //var img = (Bitmap)Image.FromFile(open.FileName);
            //{
            //    var stream = new MemoryStream();

            //    ((Image)img).Save(stream, ImageFormat.Jpeg);

            //    var imageBytes = stream.ToArray();

            //    //_staff.HinhAnh = imageBytes;
            //    imgHinhAnh.Image = img;
            //}

            var open = new OpenFileDialog
            {
                Filter = Constants.ImageFilter
            };

            if (open.ShowDialog() != DialogResult.OK) return;

            var img = (Bitmap)Image.FromFile(open.FileName);
            {
                var stream = new MemoryStream();

                img.Save(stream, ImageFormat.Jpeg);

                var imageBytes = stream.ToArray();

                var hinhAnhDto = new HinhAnhDTO {MaNV = _maNv, HinhAnh = imageBytes};
                if (_hinhAnhCtl.InsertImage(hinhAnhDto))
                {
                    imgHinhAnh.Image = img;
                }
                else
                {
                    XtraMessageBox.Show(Constants.MsgLuuThatBai, Constants.Notify, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        private void btnPg6Luu_Click(object sender, EventArgs e)
        {
            var strNhanXet = txtPg6TuNhanXet.EditValue as String;
            if (!string.IsNullOrEmpty(_maNv))
            {
                var staff = _nhanVienCtl.TimNhanVienTheoMaNV(_maNv).FirstOrDefault();
                if (staff == null) return;

                if (_nhanVienCtl.CapNhatTuNhanXet(strNhanXet, staff.Id.ToString()))
                {
                    XtraMessageBox.Show(Constants.MsgLuuThanhCong, Constants.Notify, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    XtraMessageBox.Show(Constants.MsgLuuThatBai, Constants.Notify, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private ThongTinNhanVienDTO GetData()
        {
            var nhanVienDto = _ucNvSoLuocLyLich.GetData();
            var quaTrinhCongTacDto = _ucNvQuaTrinhCongTac.GetData();
            var khenThuongKyLuatDto = _ucNvKhenThuongKyLuat.GetData();
            var quanHeGiaDinhDto = _ucNvQuanHeGiaDinh.GetData();
            var tuNhanXet = txtPg6TuNhanXet.EditValue as String;

            // so luoc ly lich

            //
            var quaTrinhCongTac = _quaTrinhCongTacCtl.LayDanhSachQuaTrinhCongTacNhanVien(_maNv);
            var quaTrinhCongTacXaHoi = _quaTrinhCongTacCtl.LayDanhSachToChucXaHoiNhanVien(_maNv);
            var trinhDoChuyenMonCaoNhat = _trinhDoCtl.LayDanhSachTrinhDo(_maNv, Constants.LoaiTrinhDoCaoNhat); // cao nhat: 0 || 1
            var trinhDoChuyenMonNghiepVu = _trinhDoCtl.LayDanhSachTrinhDo(_maNv, Constants.LoaiTrinhDoNghiepVu); // nghiep vu: 2
            var trinhDoChuyenMonLyLuanChinhTri = _trinhDoCtl.LayDanhSachTrinhDo(_maNv, Constants.LoaiTrinhDoLyLuanChinhTri); // chinh tri: 3
            var trinhDoChuyenMonNgoaiNgu = _trinhDoCtl.LayDanhSachTrinhDo(_maNv, Constants.LoaiTrinhDoNgoaiNgu); // ngoai ngu: 4
            var trinhDoChuyenMonTinHoc = _trinhDoCtl.LayDanhSachTrinhDo(_maNv, Constants.LoaiTrinhDoTinHoc); // tin hoc: 5
            var khenThuong = _khenThuongKyLuatCtl.LayDanhSachKhenThuongNhanVien(_maNv);
            var kyLuat = _khenThuongKyLuatCtl.LayDanhSachKyLuatNhanVien(_maNv);
            var quanHeGiaDinh = _quanHeGiaDinhCtl.LayDanhSachThanNhanCuaNhanVien(_maNv);

            var thongTinNhanVienDto = new ThongTinNhanVienDTO {lylichsoluoc = nhanVienDto};

            if(trinhDoChuyenMonCaoNhat.Count != 0)
                thongTinNhanVienDto.trinhdochuyenmon.AddRange(trinhDoChuyenMonCaoNhat);

            if (trinhDoChuyenMonNghiepVu.Count != 0)
                thongTinNhanVienDto.trinhdochuyenmon.AddRange(trinhDoChuyenMonNghiepVu);

            if (trinhDoChuyenMonLyLuanChinhTri.Count != 0)
                thongTinNhanVienDto.trinhdochuyenmon.AddRange(trinhDoChuyenMonLyLuanChinhTri);

            if (trinhDoChuyenMonNgoaiNgu.Count != 0)
                thongTinNhanVienDto.trinhdochuyenmon.AddRange(trinhDoChuyenMonNgoaiNgu);

            if (trinhDoChuyenMonTinHoc.Count != 0)
                thongTinNhanVienDto.trinhdochuyenmon.AddRange(trinhDoChuyenMonTinHoc);

            thongTinNhanVienDto.khenthuong = khenThuong;
            thongTinNhanVienDto.kyluat = kyLuat;

            if (quanHeGiaDinh.Count != 0)
                thongTinNhanVienDto.quanhegiadinh.AddRange(quanHeGiaDinh);

            if (quaTrinhCongTac.Count != 0)
            {
                thongTinNhanVienDto.truockhituyendung =
                    quaTrinhCongTac.AsEnumerable().Where(u => u.GiaiDoan == Constants.GiaDoanTruocTuyenDung).ToList();

                thongTinNhanVienDto.khituyendung =
                    quaTrinhCongTac.AsEnumerable().Where(u => u.GiaiDoan == Constants.GiaDoanSauTuyenDung).ToList();
            }

            if (quaTrinhCongTacXaHoi.Count != 0)
            {
                thongTinNhanVienDto.congtacxahoi = quaTrinhCongTac.AsEnumerable().Where(u => u.GiaiDoan == Constants.GiaDoanCongTacXaHoi).ToList();
            }

            return thongTinNhanVienDto;
        }
    }
}